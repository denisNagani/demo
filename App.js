import React, { Component } from 'react';
import { Button, Container, Content, Footer, Item, Root, StyleProvider, Text } from 'native-base';
import getTheme from './native-base-theme/components';
import platform from './native-base-theme/variables/platform';
import { Alert, Image, Platform, SafeAreaView, StyleSheet, YellowBox } from 'react-native'
import { store } from "./src/store";
import { Provider } from "react-redux";
import StartScreen from "./src/components/startScreen";
import messaging from '@react-native-firebase/messaging';
import withCodePush from './codepush'
import Images from "./src/assets/images";
import NavigationService from "./src/components/startScreen/RootNavigation";
import { getRole, setOfferFlag, setOfferId } from './src/services/auth';
import { setJoinCallData } from './src/actions/appointment';
import { isValid } from './src/utils/ifNotValid';
import { acceptCallAction } from './src/actions/start-consultation';
import { setTcModal } from './src/actions/modal';
import SpInAppUpdates, {
    IAUInstallStatus,
    IAUUpdateKind
} from 'sp-react-native-in-app-updates';
import firestore from '@react-native-firebase/firestore'
import { appName, appStoreAppLink, playStoreAppLink } from './src/config';

console.disableYellowBox = true;
YellowBox.ignoreWarnings([
    'Animated: `useNativeDriver` was not specified. This is a required option and must be explicitly set to `true` or `false`',
])
// To see all the requests in the chrome Dev tools in the network tab.
// XMLHttpRequest = GLOBAL.originalXMLHttpRequest ?
//     GLOBAL.originalXMLHttpRequest :
//     GLOBAL.XMLHttpRequest;

// fetch logger
global._fetch = fetch;
global.fetch = function (uri, options, ...args) {
    return global._fetch(uri, options, ...args).then((response) => {
        console.log('Fetch', { request: { uri, options, ...args }, response });
        return response;
    });
};


class App extends Component {


    constructor(props) {
        super(props)
        this.state = {
            isLatestApp: true,
            message: `The new version of ${appName} App is available`,
            iosLink: appStoreAppLink,
            androidLink: playStoreAppLink,
            notificationObj: {},
            role: null,
            needsUpdate: false,
            otherData: null,
        }
        this.inAppUpdates = new SpInAppUpdates();
    }


    async componentDidMount() {
        if (Platform.OS === "ios") {
            this.checkForAppUpdate('App Store');
        } else {
            this.checkForAppUpdate('Play Store')
        }

        let role = await getRole();
        this.setState({
            role: role
        })

        // Assume a message-notification contains a "type" property in the data payload of the screen to open
        messaging().onNotificationOpenedApp(remoteMessage => {
            if (remoteMessage?.data?.type === "OFFER") {
                setOfferFlag(true)
                setOfferId(remoteMessage?.data?.id)
            }
            else if (Platform.OS == 'ios') {
                if (remoteMessage?.data?.type === "JOINCALL") {
                    if (isValid(remoteMessage?.data)) {
                        // store data in redux

                        const aData = JSON.parse(remoteMessage?.data?.appointmentData)
                        const uData = JSON.parse(remoteMessage?.data?.userData)
                        const jData = JSON.parse(remoteMessage?.data?.joinCallData)

                        store.dispatch(setJoinCallData({
                            userData: uData,
                            appointmentData: aData,
                            joinCallData: {
                                ...jData,
                                id: jData?._id
                            },
                        }))
                        store.dispatch(acceptCallAction(aData, 'APPROVED', uData, () => {
                            NavigationService.navigate('JoinCall')
                        }))
                    }
                }
            }
        });

        // Check whether an initial notification is available
        messaging()
            .getInitialNotification()
            .then(remoteMessage => {
                if (remoteMessage) {
                    if (remoteMessage?.data?.type === "OFFER") {
                        setOfferFlag(true)
                        setOfferId(remoteMessage?.data?.id)
                    } else if (Platform.OS == 'ios') {
                        if (remoteMessage?.data?.type === "JOINCALL") {
                            if (isValid(remoteMessage?.data)) {
                                // store data in redux

                                const aData = JSON.parse(remoteMessage?.data?.appointmentData)
                                const uData = JSON.parse(remoteMessage?.data?.userData)
                                const jData = JSON.parse(remoteMessage?.data?.joinCallData)

                                store.dispatch(setJoinCallData({
                                    userData: uData,
                                    appointmentData: aData,
                                    joinCallData: {
                                        ...jData,
                                        id: jData?._id
                                    },
                                }))
                                store.dispatch(acceptCallAction(aData, 'APPROVED', uData, () => {
                                    NavigationService.navigate('JoinCall')
                                }))
                            }
                        }
                    }
                }
            });

        messaging().onMessage(async remoteMessage => {
            console.log("redirect to incoming call screen here ...", remoteMessage);

            const patientOnWaitingScreen = store.getState().bookings.patientOnWaitingScreen

            if (remoteMessage.data.type === "ACCEPT" && this.state.role === 'PATIENT') {
                if (patientOnWaitingScreen == false) {
                    console.log('datatata', remoteMessage?.data);
                    if (isValid(remoteMessage?.data?.appointmentId)) {
                        this.showAlert(remoteMessage?.notification?.body, () => {

                            const _id = JSON.parse(remoteMessage?.data?.appointmentId)
                            const appointmentDate = JSON.parse(remoteMessage?.data?.appointmentDate)
                            const doctorId = JSON.parse(remoteMessage?.data?.doctor)

                            store.dispatch(setTcModal({
                                visible: true,
                                appointmentId: _id,
                                appointmentDate: appointmentDate,
                                doctor: doctorId,
                            }))
                        }, 'Connect')
                    }

                }
            } else if (remoteMessage.data.type === "REJECT" && this.state.role === 'PATIENT') {
                this.showAlert('Your request to join is declined by doctor ')
            } else if (remoteMessage.data.type === "BOOKING" && this.state.role === 'DOCTOR') {
                this.showAlert('New Appointment Booking from patient...check in my appointments')
            } else if (remoteMessage.data.type === "REMINDER" && this.state.role === 'PATIENT') {
                this.showAlert('Appointment Alert! Check your appointments 1 hour left for new appointment')
            } else if (remoteMessage.data.type === "JOINCALL" && this.state.role === 'DOCTOR') {
                if (patientOnWaitingScreen == false) {
                    this.showAlert('Patient requesting call...please check waiting list', () => {

                        const aData = JSON.parse(remoteMessage?.data?.appointmentData)
                        const uData = JSON.parse(remoteMessage?.data?.userData)
                        const jData = JSON.parse(remoteMessage?.data?.joinCallData)

                        store.dispatch(setJoinCallData({
                            userData: uData,
                            appointmentData: aData,
                            joinCallData: {
                                ...jData,
                                id: jData?._id
                            },
                        }))
                        store.dispatch(acceptCallAction(aData, 'APPROVED', uData, () => {
                            NavigationService.navigate('JoinCall')
                        }))
                    }, 'Connect')
                }
            } else {
                console.log("else notification " + JSON.stringify(remoteMessage));
            }
        });
    }

    showAlert(subtitle, onPress, btnText) {
        Alert.alert(
            '',
            subtitle,
            [
                { text: isValid(btnText) ? btnText : 'OK', onPress: onPress },
            ]
        )
    }

    checkForAppUpdate = (type) => {
        // curVersion is optional if you don't provide it will automatically take from the app using react-native-device-info
        this.inAppUpdates.checkNeedsUpdate()
            .then((result) => {
                console.log('checkForAppUpdate result => ', result);
                let message = `The new version of ${appName} is available on ${type}, we recommend you to update.`
                if (result?.shouldUpdate == true) {
                    this.setState({
                        needsUpdate: result?.shouldUpdate,
                        otherData: result,
                        isLatestApp: false,
                        message
                    })
                }
            })
            .catch(err => {
                console.log('error ', err);
            })
    }

    updateApp = () => {
        if (this.state.needsUpdate) {
            const updateOptions = Platform.select({
                ios: {
                    title: 'Update available',
                    message: "There is a new version of the app available on the App Store, do you want to update it?",
                    buttonUpgradeText: 'Update',
                    buttonCancelText: 'Cancel',
                },
                android: {
                    updateType: IAUUpdateKind.IMMEDIATE,
                },
            });

            this.inAppUpdates.addStatusUpdateListener(downloadStatus => {
                console.log('download status', downloadStatus);
                if (downloadStatus.status === IAUInstallStatus.DOWNLOADED) {
                    console.log('downloaded');
                    this.inAppUpdates.installUpdate();
                    this.setState({
                        needsUpdate: false,
                        otherData: null,
                        isLatestApp: true,
                    })
                    this.inAppUpdates.removeStatusUpdateListener(finalStatus => {
                        console.log('final status', finalStatus);
                    });
                } else if (downloadStatus.status === IAUInstallStatus.INSTALLED) {
                    console.log('installed');
                    this.setState({
                        needsUpdate: false,
                        otherData: null,
                        isLatestApp: true,
                    })
                    this.inAppUpdates.removeStatusUpdateListener(finalStatus => {
                        console.log('final status', finalStatus);
                    });
                }
            });

            this.inAppUpdates.startUpdate(updateOptions)
        }
    }

    getAppStoreVersion = async (docName) => {
        const snap = await firestore().collection("support").doc(docName).get();
        return snap.data()
    }

    renderUpdateAppView() {
        return (<Container>
            <SafeAreaView style={{ flex: 1 }}>
                <Image resizeMode="contain" source={Images.home_logo} style={styles.headerLogo} />

                <Content>
                    <Item noLine style={styles.title}>
                        <Text style={styles.titleText}>Update Required</Text>
                    </Item>
                    <Item noLine style={styles.body}>
                        <Text style={styles.bodyText}>{this.state.message}</Text>
                    </Item>
                </Content>
            </SafeAreaView>
            <Footer>
                <Button success loginBtn style={styles.footerBtn} onPress={this.updateApp}>
                    <Text btnText> Update Now </Text>
                </Button>
            </Footer>
        </Container>)
    }


    render() {
        return <Provider store={store}>
            <StyleProvider style={getTheme(platform)}>
                <Root>
                    {this.state.isLatestApp ?
                        <StartScreen /> :
                        this.renderUpdateAppView()}
                </Root>
            </StyleProvider>
        </Provider>;
    }

}


const styles = StyleSheet.create({
    header: {
        backgroundColor: '#f9f9f9',
        height: 140,
    },
    headerLogo: {
        justifyContent: 'center',
        alignSelf: 'center',
        height: 130,
        width: 185
    },
    title: {
        justifyContent: 'center',
        padding: 20
    },
    titleText: {
        fontSize: 20,
    },
    body: {
        justifyContent: 'center',
        padding: 20,
        paddingTop: 10
    },
    bodyText: {
        textAlign: 'center'
    },
    footerBtn: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100%'
    }
});


export default withCodePush(App);
