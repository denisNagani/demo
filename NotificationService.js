import notifee, { AndroidImportance } from '@notifee/react-native';
import { Platform } from 'react-native';
import { setIncomingCallScreen } from './src/services/auth';

export default class NotificationService {
    async incomingCallNotification(remoteMessage) {
        const channelId = await notifee.createChannel({
            id: 'default',
            name: 'sound Channel',
            // vibration: true,
            sound: 'telephone',
            importance: AndroidImportance.HIGH,
        });

        if (Platform.OS === 'ios') {
            await notifee.setNotificationCategories([
                {
                    id: 'post',
                    actions: [
                        {
                            id: 'accept',
                            title: 'ACCEPT',
                            foreground: true,
                        },
                        {
                            id: 'not_now',
                            title: 'Not Now',
                        },
                    ],
                },
            ]);
        }

        // Display a notification
        await notifee.displayNotification({
            id: remoteMessage?.messageId,
            title: 'Incoming Video Call',
            body: `${remoteMessage?.data?.patientName} is Calling...`,
            android: {
                ongoing: true,
                sound: 'telephone',
                importance: AndroidImportance.HIGH,
                channelId,
                pressAction: {
                    id: 'default',
                    launchActivity: 'default'
                },
                actions: [
                    {
                        title: 'ACCEPT',
                        pressAction: {
                            id: 'accept',
                            launchActivity: 'default',
                        },
                    },
                    {
                        title: 'Not Now',
                        pressAction: {
                            id: 'not_now',
                        },
                    },
                ],
            },
            data: remoteMessage?.data,
            ios: {
                sound: 'telephone.wav',
                categoryId: 'post'
            }
        });
    }
}