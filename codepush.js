import React from 'react';
import CodePush from 'react-native-code-push';
import CodePushUpdate from './src/components/CodePushUpdate';

const CODE_PUSH_OPTIONS = {
    checkFrequency: CodePush.CheckFrequency.ON_APP_RESUME,
    installMode: CodePush.InstallMode.ON_NEXT_RESUME,
};

const withCodePush = WrappedComponent => {
    class WrappedApp extends React.PureComponent {
        state = {
            dowloadProgresss: 0,
            totalDownload: 0,
            showAlert: false
        }
        componentDidMount() {
            CodePush.sync({
                updateDialog: {
                    title: "An update is available!",
                    appendReleaseDescription: true,
                    optionalIgnoreButtonLabel: ""
                }, installMode: CodePush.InstallMode.IMMEDIATE
            }, this.syncWithCodePush, this.dowloadProgresss);
        }

        syncWithCodePush = (status) => {
            console.log('status codepush', status);
            if (status == CodePush.SyncStatus.DOWNLOADING_PACKAGE) {
                this.setState({ showAlert: true })
            } else if (status == CodePush.SyncStatus.INSTALLING_UPDATE) {
                this.setState({ showAlert: false })
            }
        };

        dowloadProgresss = (progress) => {
            this.setState({
                dowloadProgresss: progress.receivedBytes,
                totalDownload: progress.totalBytes
            })
        };

        showAlertDialog = () => {
            return <CodePushUpdate total={this.state.totalDownload} downloaded={this.state.dowloadProgresss} />
        }

        render() {
            return (
                <>
                    {this.state.showAlert === true && this.showAlertDialog()}
                    <WrappedComponent />
                </>
            );
        }
    }

    return CodePush(CODE_PUSH_OPTIONS)(WrappedApp);
};
export default withCodePush;
