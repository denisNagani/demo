/**
 * @format
 */
////////////////////////////
///////////////
///////
///
//

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import messaging from '@react-native-firebase/messaging';
import { setIncomingCallScreen, setOfferFlag, setOfferId } from './src/services/auth';
import NotificationService from './NotificationService';
import { store } from './src/store';
import { setJoinCallData } from './src/actions/appointment';
import notifee, { EventType } from '@notifee/react-native'
import { isValid } from './src/utils/ifNotValid';
import { setPrescriptionData } from './src/actions/presModel';

// Register background handler
messaging().setBackgroundMessageHandler(async remoteMessage => {
    console.log('Message handled in the background!', remoteMessage);
    if (remoteMessage?.data?.type === "JOINCALL") {
        //trigger notification with ring
        const notification = new NotificationService();
        notification.incomingCallNotification(remoteMessage)
    }
});

notifee.onBackgroundEvent(async ({ type, detail }) => {
    const { notification, pressAction } = detail;
    console.log('pressAction ', pressAction);

    if (type === EventType.ACTION_PRESS && pressAction.id === 'accept') {
        if (isValid(notification?.data)) {
            // set incoming call flag
            setIncomingCallScreen("true")
            // store data in redux
            const appData = JSON.parse(notification?.data?.appointmentData)
            const userData = JSON.parse(notification?.data?.userData)
            const joinData = JSON.parse(notification?.data?.joinCallData)

            store.dispatch(setJoinCallData({
                userData: userData,
                appointmentData: appData,
                joinCallData: {
                    ...joinData,
                    id: joinData?._id
                },
                setIncomingCallScreen: true
            }))
            const presdata = {
                ...appData,
                doctorId: {
                    fullName: userData?.fullName,
                    image: userData?.image,
                }

            }
            store.dispatch(setPrescriptionData(presdata, appData?._id))
        }
    } else if (type === EventType.ACTION_PRESS && pressAction.id === 'not_now') {
        setIncomingCallScreen("false")
    } else {
        setIncomingCallScreen("true")
    }
});

AppRegistry.registerComponent(appName, () => App);
