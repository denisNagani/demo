import {
    DIRECT_LOGIN,
    DOCTOR_SLOT,
    SELECTED_DOCTOR,
    SLOT_TIMEZONE,
    START_RECORDING,
    STOP_RECORDING, SUMMARY_DATE_N_TIME, SET_MEMBERSHIP_ID, SET_JOIN_CALL_DATA, SET_APPOINTMENT_TYPE
} from "../utils/constant";
import http from "../services/http";
import { hide, show } from "../utils/loader/action";
import { setCallHeader, } from "./bookings";
import { errorLog, successLog } from "../utils/fireLog";
import { selectedSlotObj } from "../scenes/patient/book-appointment/action";
import { getDocProfile } from "./sehet/user-action";
import { ifValid, isValid } from "../utils/ifNotValid";
import { CoupnApplied, UpdatePrice, setDisValue } from "./coupn";
import { selectedSpecialityObj } from "./sehet/speciality-action";
import { getSkipped, setFromCheckout } from "../services/auth";
import { strings } from "../utils/translation";
import { showModal } from "./modal";
import NavigationService from "../components/startScreen/RootNavigation";
import { calculateDrConsultationFee } from "../utils/helper";
import { CONSULT_FEE } from "../config";

export const getDoctorsSlot = (slotId, date, consultType) => {
    return (dispatch) => {
        dispatch({ type: DOCTOR_SLOT, payload: [] });
        dispatch(show());
        http
            .post(`doctors/slots`, {
                doctorId: slotId,
                date: date,
                consultType: consultType
            }, dispatch)
            .then((res) => {
                successLog("get doctor slots", res.status)
                dispatch({ type: DOCTOR_SLOT, payload: res.data });
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("get doctor slot", o)
                dispatch(hide());
            });
    };
};

export const getSlotTimeZone = () => {
    return (dispatch) => {
        dispatch(show());
        http.get(`organization/timezone`, dispatch)
            .then((res) => {
                successLog("get slot timezone ", res.status)
                dispatch({ type: SLOT_TIMEZONE, payload: res.data });
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("get slot time zone", o)
                dispatch(hide());
            });
    };
};

export const setSelectedDoctor = (data) => {
    console.log("selcted doctor is ", data);
    return {
        type: SELECTED_DOCTOR,
        payload: data
    }
};

export const emptySlot = () => {
    return {
        type: DOCTOR_SLOT,
        payload: [],
    };
};


export const setSummaryDateNTime = (value) => {
    return {
        type: SUMMARY_DATE_N_TIME,
        payload: value,
    };
};

export const postDoctorSlot = (payload, navigation, get_price) => {
    console.log("api/appointment/book payload ", JSON.stringify(payload));
    return (dispatch) => {
        dispatch(show());
        dispatch(selectedSlotObj(payload));
        // dispatch(setIsBookingFlow1or2(flowBooking1or2));
        // if (flowBooking1or2 === 1) {
        // navigation.navigate('ApplyCoupon');
        // navigation.navigate('BookingSummaryScreen');
        dispatch(CoupnApplied(null))
        dispatch(UpdatePrice(get_price?.m_price))
        navigation.navigate('BookingSummaryScreen', {
            dis_value: 0
        })
        dispatch(setDisValue(0))
        // } else {
        //     navigation.navigate('BookingSummaryScreenFlow2');
        // }
        // navigation.navigate("BookingSummaryScreenFlow2");
        if (ifValid(payload.doctorId))
            dispatch(getDocProfile(payload.doctorId))
        else console.log("id not found")
    };
};

export const postDirectLogin = (body, props, roomUrl) => {
    return (dispatch) => {
        dispatch(show());
        http
            .post(`users/patients`, body, dispatch)
            .then((res) => {
                successLog("post direct login", res.status)
                dispatch(hide());
                dispatch({ type: DIRECT_LOGIN, payload: res.data });
                dispatch(setCallHeader(res.data.doctorUrl));
                props.navigation.navigate("WaitingRoom", {
                    channel: roomUrl,
                    user: "PATIENT",
                    doctor: roomUrl,
                    bookingId: res.data._id,
                    guestName: body.fullName + "(guest)",
                });
            })
            .catch((o) => {
                errorLog("post direct login", o)
                dispatch(hide());
            });
    };
};

export const startRecording = (channel, uid, appointmentId) => {
    successLog(appointmentId + 'appointmentId')
    return (dispatch) => {
        dispatch(show());
        http
            .post(`api/appointments/recording/start`, {
                cname: channel,
                uid: uid.toString(),
                appointmentId: appointmentId,
                clientRequest: {
                    resourceExpiredHour: 24,
                },
            }, dispatch)
            .then((res) => {
                successLog("start recording", res.status)
                dispatch({ type: START_RECORDING, payload: res.data });
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("start recording ", o)
                dispatch(hide());
            });
    };
};
export const stopRecording = (body) => {
    return (dispatch) => {
        dispatch(show());
        http
            .post(`api/appointments/recording/stop`, body, dispatch)
            .then((res) => {
                successLog("stop recording ", res.status)
                dispatch({ type: STOP_RECORDING, payload: res.data });
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("stop recording", o)
                dispatch(hide());
            });
    };
};

export const followUpPost = (body, navigation) => {
    return (dispatch) => {
        dispatch(show());
        http
            .post(`api/follow-up`, body, dispatch)
            .then((res) => {
                successLog("Follow Up ", res.status)
                dispatch(hide());
                navigation.navigate("AcknowledgmentScreen")
            })
            .catch((o) => {
                errorLog("Follow Up", o)
                dispatch(hide());
            });
    };
};

export const reschedulePost = (body, navigation) => {
    return (dispatch) => {
        dispatch(show());
        http
            .put(`api/resheduleAppointment`, body, dispatch)
            .then((res) => {
                dispatch(hide());
                navigation.navigate("AcknowledgmentScreen")
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};

export const handleBookNow = (doc) => {
    return async dispatch => {
        console.log("passed doc ", doc);
        dispatch(setSelectedDoctor(doc))

        const specialityArray = doc.specialityId;
        const item = calculateDrConsultationFee(specialityArray)
        if (isValid(doc)) {
            // let m_price = parseInt(item?.price)
            // let m_specialPrice = parseInt(item?.specialPrice)
            // let percentage = (m_price / 100) * m_specialPrice;
            // m_price = m_price + percentage;
            // dispatch(selectedSpecialityObj({ ...item, price: m_price, m_price: m_price, dateTime: new Date() }))

            let m_price = isValid(doc?.fee) ? doc?.fee : CONSULT_FEE
            dispatch(selectedSpecialityObj({ ...doc, price: m_price, m_price: m_price, dateTime: new Date() }))

            const skipped = await getSkipped()
            const isSkipped = JSON.parse(skipped)
            if (isSkipped) {
                const payload = {
                    text: `${strings.please_login_to_continue}`,
                    subtext: "",
                    iconName: "closecircleo",
                    modalVisible: true,
                }
                setFromCheckout(true)
                NavigationService.navigate('PatientLogin')
                dispatch(showModal(payload))
            } else {
                NavigationService.navigate("BookAppointmentScreenFlow2", {
                    doc
                })
            }
        } else {
            console.log("invalid doc");
        }
    }

}

export const setMembershipId = (id) => {
    return dispatch => {
        dispatch({
            type: SET_MEMBERSHIP_ID,
            payload: id
        })
    }
}

export const setJoinCallData = (id) => {
    return dispatch => {
        dispatch({
            type: SET_JOIN_CALL_DATA,
            payload: id
        })
    }
}

export const setAppointmentType = (type) => {
    return dispatch => {
        dispatch({
            type: SET_APPOINTMENT_TYPE,
            payload: type
        })
    }
}