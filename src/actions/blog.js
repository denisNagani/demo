import { show, hide } from "../utils/loader/action";
import http from "../services/http";
import { errorLog, successLog } from "../utils/fireLog";
import { GET_ALL_BLOGS } from "../utils/constant";

export const getAllBlogs = () => {
    return (dispatch) => {
        dispatch(show());
        http.get(`blog`, dispatch)
            .then((res) => {
                successLog("get all blogs ", res.status)
                dispatch({ type: GET_ALL_BLOGS, payload: res.data });
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("get all blogs ", o)
                dispatch(hide());
            });
    };
};