import {
	BOOKING_HISTORY,
	DOCTOR_IMAGE,
	DOCTOR_NAME,
	DOCTOR_UPDATE_ROOM,
	DOCTOR_WAITING_LIST,
	IO_EVENTS,
	PAST_BOOKINGS,
	PATIENT_ON_WAITING_SCREEN,
	SELECTED_APPOINTMENT,
	SELECTED_HEADER,
	SELECTED_SLOT,
	UPCOMING_BOOKINGS,
	UPCOMING_BOOKINGS_BY_DATE,
	UPDATE_BOOKING
} from "../utils/constant";
import http from "../services/http";
import { showModal } from "./modal";
import moment from "moment";
import { errorLog, successLog } from "../utils/fireLog";
import { ifValid } from "../utils/ifNotValid";
import NavigationService from '../components/startScreen/RootNavigation'
import { socket } from "../config";

export const getBookingHistory = () => {
	return (dispatch) => {
		http
			.get("api/past/appointments", dispatch)
			.then((res) => {
				successLog("get past bookings", res.status)
				dispatch({ type: BOOKING_HISTORY, payload: res.data });
			})
			.catch((o) => {
				errorLog("getBookingHistory", o)
			});
	};
};

export const getUpcomingBookings = () => {
	let date = moment().format("YYYY-MM-DD")
	return (dispatch) => {
		http.get(`api/upcoming/appointments?date=${date}`, dispatch)
			.then((res) => {
				successLog("get upcoming bookings", res.status)
				dispatch({ type: UPCOMING_BOOKINGS, payload: res.data });
			})
			.catch((o) => {
				errorLog("getUpcomingBookings", o)
			});
	};
};

export const getPastBookings = () => {
	return (dispatch) => {
		http.get(`api/past/appointments`, dispatch)
			.then((res) => {
				successLog("get past bookings", res.status)
				// console.log("past data ", res.data)
				dispatch({ type: PAST_BOOKINGS, payload: res.data });
			})
			.catch((o) => {
				errorLog("getpastBookings", o)
			});
	};
};

export const getUpcomingBookingsByDate = (date, callMethod) => {
	return (dispatch) => {
		http.get(`api/booked/appointments?appointmentDate=${date}`, dispatch)
			.then((res) => {
				successLog("get upcoming bookings by date", res.status)
				dispatch({ type: UPCOMING_BOOKINGS_BY_DATE, payload: res.data });
				callMethod(true)
			})
			.catch((o) => {
				errorLog("getUpcomingBookingsByDate", o)
			});
	};
};

export const setDoctorName = (payload) => {
	return {
		type: DOCTOR_NAME,
		payload: payload,
	};
};

export const setDoctorImage = (payload) => {
	return {
		type: DOCTOR_IMAGE,
		payload: payload,
	};
};

export const updateAppointment = (id, navigation, status, roomName, doctor) => {
	return (dispatch) => {
		http
			.put("api/status/appointments", {
				id: id,
				status: status,
			}, dispatch)
			.then((res) => {
				successLog(`update appointment status passed${status}`, res.status)
				if (status === "REJECT") {
					const payload = {
						text: "Appointment Status updated",
						subText: "",
						iconName: "checkcircleo",
						modalVisible: true,
					};
					dispatch(showModal(payload));
				} else if (status === "REJECTED") {
					const payload = {
						text: "Appointment Status updated",
						subText: "",
						iconName: "checkcircleo",
						modalVisible: true,
					};
					dispatch(showModal(payload));
				} else
					navigation.navigate("WaitingRoom", {
						roomName: roomName,
						doctor: doctor,
						bookingId: id,
					});
				dispatch({ type: UPDATE_BOOKING, payload: res.data });
				dispatch(getWaitingListDoctor());
			})
			.catch((o) => {
				errorLog("updateAppointment", o)
			});
	};
};

export const updateAppointmentPatient = (
	id,
	navigation,
	status,
	roomName,
	doctor
) => {
	return (dispatch) => {
		http
			.put("api/status/appointments", {
				id: id,
				status: status,
			}, dispatch)
			.then((res) => {
				successLog(`update appointment patient status passed${status}`, res.status)
				if (status === "REJECT") {
					const payload = {
						text: "Appointment Status updated",
						subText: "",
						iconName: "checkcircleo",
						modalVisible: true,
					};
					dispatch(showModal(payload));
				} else if (status === "REJECTED") {
					const payload = {
						text: "Appointment Status updated",
						subText: "",
						iconName: "checkcircleo",
						modalVisible: true,
					};
					dispatch(showModal(payload));
				} else
					NavigationService.navigate("WaitingRoom", {
						roomName: roomName,
						doctor: doctor,
						bookingId: id,
					});

				// emit STATUS_UPDATED event
				socket().emit(IO_EVENTS.CALL_JOINED, doctor?._id)

				dispatch({ type: UPDATE_BOOKING, payload: res.data });
			})
			.catch((o) => {
				errorLog("updateAppointmentPatient", o)
			});
	};
};

export const getWaitingListDoctor = (callBack) => {
	let date = moment().format("YYYY-MM-DD")
	return (dispatch) => {
		http
			.get(`api/waiting/appointments?status=WAITING&date=${date}`, dispatch)
			.then((res) => {
				successLog(`waiting list doctor`, res.status)
				let data = ifValid(res.data) ? res.data : [];
				dispatch({ type: DOCTOR_WAITING_LIST, payload: data });
				callBack();
			})
			.catch((o) => {
				errorLog("getWaitingListDoctor", o)
			});
	};
};

export const checkRoomUrlExist = (url) => {
	return (dispatch) => {
		http
			.get(`api/doctors/rooomUrl/${url}`, dispatch)
			.then((res) => {
				successLog(`check room url exist`, res.status)
				// console.log(JSON.stringify(res))
				if (res.status === 204) {
					dispatch(updateRoomUrl(url));
				} else {
					const payload = {
						text: "Room already exists. Please choose different one",
						subText: "",
						iconName: "closecircleo",
						modalVisible: true,
						iconColor: "#C70315",
					};
					dispatch(showModal(payload));
				}
			})
			.catch((o) => {
				errorLog("checkRoomUrlExist", o)
			});
	};
};

export const updateRoomUrl = (url) => {
	return (dispatch) => {
		http
			.put(`api/doctors/updateRoomUrl`, {
				roomUrl: url,
			}, dispatch)
			.then((res) => {
				successLog(`update room url`, res.status)
				dispatch({ type: DOCTOR_UPDATE_ROOM, payload: res });
			})
			.catch((o) => {
				errorLog("updateRoomUrl", o)
			});
	};
};

export const completeAppointment = (
	id,
	navigation,
	status,
	roomName,
	doctor
) => {
	return (dispatch) => {
		http
			.put("api/status/appointments", { id: id, status: status }, dispatch)
			.then((res) => {
				successLog(`complete appointment`, res.status)
				dispatch({ type: UPDATE_BOOKING, payload: res.data });
				dispatch(getWaitingListDoctor());
			})
			.catch((o) => {
				errorLog("completeAppointment", o)
			});
	};
};

export const updateAppointmentDirect = (id, navigation, status) => {
	return (dispatch) => {
		http
			.put("api/directLoginUsers/status", { id: id, status: status }, dispatch)
			.then((res) => {
				successLog(`update appointment direct`, res.status)
				if (status === "REJECTED") {
					const payload = {
						text: "Appointment Status updated",
						subText: "",
						iconName: "checkcircleo",
						modalVisible: true,
					};
					dispatch(showModal(payload));
				}
				dispatch({ type: UPDATE_BOOKING, payload: res.data });
				dispatch(getWaitingListDoctor());
			})
			.catch((o) => {
				errorLog("updateAppointmentDirect", o)
			});
	};
};

export const setSelectedSlot = (payload) => {
	return {
		type: SELECTED_SLOT,
		payload: payload,
	};
};

export const setSelectedAppointment = (payload) => {
	return {
		type: SELECTED_APPOINTMENT,
		payload: payload,
	};
};

export const setCallHeader = (payload) => {
	return {
		type: SELECTED_HEADER,
		payload: payload,
	};
};

export const setPatientIsOnWaiting = (payload) => {
	return {
		type: PATIENT_ON_WAITING_SCREEN,
		payload: payload,
	};
};