import {
    BOOKING_HISTORY,
    DOCTOR_NAME,
    DOCTOR_UPDATE_ROOM,
    DOCTOR_WAITING_LIST,
    UPCOMING_BOOKINGS_BY_DATE,
    UPDATE_BOOKING,
} from "../utils/constant";
import http from "../services/http";
import {showModal} from "./modal";
import moment from "moment";
import {errorLog, successLog} from "../utils/fireLog";

export const getBookingHistory = () => {
    return (dispatch) => {
        http
            .get("api/past/appointments", dispatch)
            .then((res) => {
                successLog("getBookingHistory", res.status)
                dispatch({type: BOOKING_HISTORY, payload: res.data});
            })
            .catch((o) => {
                errorLog("getBookingHistory", o)
            });
    };
};

export const getUpcomingBookingsAdmin = () => {
    let date = moment().format("YYYY-MM-DD")
    return (dispatch) => {
        http
            .get(`api/admin/appointments?date=${date}`, dispatch)
            .then((res) => {
                successLog("getUpcomingBookingsAdmin", res.status)
                dispatch({type: UPCOMING_BOOKINGS_BY_DATE, payload: res.data});
            })
            .catch((o) => {
                errorLog("getUpcomingBookingsAdmin", o)
            });
    };
};

export const getUpcomingBookingsByDate = (date) => {
    return (dispatch) => {
        http
            .get(
                `api/booked/appointments?appointmentDate=${date}&appointmentType=VIDEO`, dispatch)
            .then((res) => {
                successLog("getUpcomingBookingsByDate", res.status)
                dispatch({type: UPCOMING_BOOKINGS_BY_DATE, payload: res.data});
            })
            .catch((o) => {
                errorLog("getUpcomingBookingsByDate", o)
            });
    };
};

export const setDoctorName = (payload) => {
    return {
        type: DOCTOR_NAME,
        payload: payload,
    };
};

export const updateAppointment = (id, navigation, status, roomName, doctor) => {
    return (dispatch) => {
        http
            .put("api/admin/status/appointments", {
                id: id,
                status: status,
            }, dispatch)
            .then((res) => {
                successLog("updateAppointment", res.status)
                if (status === "REJECTED") {
                    const payload = {
                        text: "Appointment Status updated",
                        subText: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(showModal(payload));
                }
                dispatch({type: UPDATE_BOOKING, payload: res.data});
                dispatch(getWaitingListAdmin());
            })
            .catch((o) => {
                errorLog("updateAppointment", o)
            });
    };
};

export const updateAppointmentDirectNurse = (id, navigation, status) => {
    return (dispatch) => {
        http
            .put("api/directLoginUsers/status", {id: id, status: status}, dispatch)
            .then((res) => {
                successLog("updateAppointmentDirectNurse", res.status)
                if (status === "REJECTED") {
                    const payload = {
                        text: "Appointment Status updated",
                        subText: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(showModal(payload));
                }
                dispatch({type: UPDATE_BOOKING, payload: res.data});
                dispatch(getWaitingListAdmin());
            })
            .catch((o) => {
                errorLog("updateAppointmentDirectNurse", o)
            });
    };
};

export const getWaitingListAdmin = () => {
    let date = moment().format("YYYY-MM-DD")
    return (dispatch) => {
        http
            .get(`api/admin/waitingAppointments?date=${date}&status=WAITING`, dispatch)
            .then((res) => {
                successLog("getWaitingListAdmin", res.status)
                http
                    .get("api/admin/directLoginUsers/appointments")
                    .then((response) => {
                        successLog("getWaitingListAdmin", response.status)
                        let data =
                            res.data !== undefined && res.data != null ? res.data : [];
                        let final,
                            addingStatus = [];
                        if (response.data) {
                            addingStatus = response.data.map((obj) => {
                                return {...obj, directLogin: "true"};
                            });
                        }
                        final = [...data, ...addingStatus];
                        dispatch({type: DOCTOR_WAITING_LIST, payload: final});
                    })
                    .catch((o) => {
                        console.log(" err " + JSON.stringify(o));
                    });
            })
            .catch((o) => {
                errorLog("getWaitingListAdmin", o)
            });
    };
};

export const checkRoomUrlExist = (url) => {
    return (dispatch) => {
        http
            .get(`api/doctors/rooomUrl/${url}`, dispatch)
            .then((res) => {
                successLog("checkRoomUrlExist", res.status)
                if (res.status === 204) {
                    dispatch(updateRoomUrl(url));
                } else {
                    const payload = {
                        text: "Room already exists. Please choose different one",
                        subText: "",
                        iconName: "closecircleo",
                        modalVisible: true,
                        iconColor: "#C70315",
                    };
                    dispatch(showModal(payload));
                }
            })
            .catch((o) => {
                errorLog("checkRoomUrlExist", o)
            });
    };
};

export const updateRoomUrl = (url) => {
    return (dispatch) => {
        http
            .put(`api/doctors/updateRoomUrl`, {
                roomUrl: url,
            }, dispatch)
            .then((res) => {
                successLog("updateRoomUrl", res.status)
                dispatch({type: DOCTOR_UPDATE_ROOM, payload: res});
            })
            .catch((o) => {
                errorLog("updateRoomUrl", o)
            });
    };
};

export const completeAppointment = (
    id,
    navigation,
    status,
    roomName,
    doctor
) => {
    return (dispatch) => {
        http
            .put("api/status/appointments", {id: id, status: status}, dispatch)
            .then((res) => {
                successLog("completeAppointment", res.status)
                dispatch({type: UPDATE_BOOKING, payload: res.data});
                dispatch(getWaitingListAdmin());
            })
            .catch((o) => {
                errorLog("completeAppointment", o)
            });
    };
};
