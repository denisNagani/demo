import {
    SET_FILE_URL,
    CLEAR_FILE_URL,
    SET_DOC_URL,
    CLEAR_DOC_URL
} from "../utils/constant"
import { errorLog, successLog } from "../utils/fireLog"
import http from "../services/http"

export const setFileUrl = (url, type) => {
    return dispatch => {
        dispatch({
            type: SET_FILE_URL,
            payload: {
                url: url,
                type: type
            }
        })
    }
}

export const clearFileUrl = () => {
    return dispatch => {
        dispatch({
            type: CLEAR_FILE_URL,
            payload: null
        })
    }
}

export const setDocUrl = url => {
    return dispatch => {
        dispatch({
            type: SET_DOC_URL,
            payload: url
        })
    }
}

export const clearDoceUrl = () => {
    return dispatch => {
        dispatch({
            type: CLEAR_DOC_URL,
            payload: null
        })
    }
}

export const UpdateChatStatus = (payload) => {
    successLog('chat status api calling.')
    return (dispatch) => {
        http.get(`api/chatStatus?status=${payload}`, dispatch)
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    console.log("chat status updated");
                }
            })
            .catch((err) => {
                errorLog("update Profile Patient", err)
            });
    };
};