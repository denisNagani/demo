import http from "../services/http";
import { show, hide } from "../utils/loader/action";
import { errorPayload } from "../utils/helper";
import { ifValid } from "../utils/ifNotValid";
import { successLog, errorLog } from "../utils/fireLog";
import { showModal } from "./modal";
import { UPDATE_PRICE, COUPON_APPLIED, SET_DISCOUNT, SUB_PLANS } from "../utils/constant";
import NavigationService from "../components/startScreen/RootNavigation";
import { setApplyCouponModal, setSelectPlanModal } from "./sehet/speciality-action";

export const CheckCoupon = (couponName, user_id, appoint_id, ogPrice, deviceId) => {
    return dispatch => {
        dispatch(show());
        http
            .post(`api/fetchCoupon`, {
                couponName
            }, dispatch)
            .then((res) => {
                successLog("api/fetchCoupon == ", res.status)
                if (res.status === 200) {
                    if (ifValid(res.data)) {
                        const coupon_id = res.data;
                        console.log("coupon res " + coupon_id);
                        const isEnabled = coupon_id.isEnabled;
                        if (isEnabled) {
                            dispatch(CheckUserCoupon(coupon_id, user_id, appoint_id, ogPrice, deviceId))
                        } else {
                            dispatch(showModal(errorPayload('Invalid Coupon Code', "")))
                            dispatch(hide());
                        }
                    } else {
                        dispatch(hide());
                        dispatch(showModal(errorPayload("Invalid Coupon Code")))
                    }
                } else {
                    dispatch(showModal(errorPayload("Invalid Coupon Code")))
                    dispatch(hide());
                }
            })
            .catch((o) => {
                errorLog("api/fetchCoupon == ", o)
                dispatch(hide());
            });
    }
}

export const CheckUserCoupon = (coupon_id, user_id, appoint_id, ogPrice, deviceId) => {
    console.log("inside check user coupon ");
    return dispatch => {
        dispatch(show());
        http
            .post(`api/fetchUserCoupon`, {
                userId: user_id,
                couponId: coupon_id._id,
                deviceId: deviceId
            }, dispatch)
            .then((res) => {
                successLog("api/fetchUserCoupon == ", res.status)
                if (res.status === 200) {
                    if (ifValid(res.data)) {
                        if (res.data.length > 0) {
                            //coupon already applied
                            dispatch(showModal(errorPayload("Coupon already used")))
                            dispatch(hide());
                        } else {

                            let final_price;
                            let dis_value;
                            const price = coupon_id.price;
                            const type = coupon_id.couponType;

                            if (type == "PERCENTAGE") {
                                dis_value = ((price / 100) * ogPrice)
                                final_price = ogPrice - dis_value
                            } else {
                                dis_value = price
                                final_price = ogPrice - dis_value
                            }

                            dispatch(UpdatePrice(final_price))
                            //apply coupon
                            const body = {
                                couponId: coupon_id._id,
                                userId: user_id,
                                appointmentId: appoint_id,
                                isApplied: true,
                                deviceId: deviceId
                            }
                            dispatch(CoupnApplied(body))
                            const payload = {
                                text: "Coupon Applied !!",
                                subtext: "",
                                iconName: "checkcircleo",
                                modalVisible: true,
                            };
                            dispatch(showModal(payload))
                            // NavigationService.navigate('BookingSummaryScreen', {
                            //     dis_value: dis_value
                            // })
                            dispatch(setDisValue(dis_value))
                            dispatch(setApplyCouponModal(false))
                            dispatch(hide());
                        }
                    }
                } else {
                    dispatch(showModal(errorPayload("Invalid Coupon Code")))
                    dispatch(hide())
                }
            })
            .catch((o) => {
                errorLog("api/fetchUserCoupon == ", o)
                dispatch(hide());
            });
    }
}

export const ApplyCoupon = (body) => {
    console.log("inside check user coupon ");
    return dispatch => {
        dispatch(show());
        http
            .post(`api/userCoupon`, body, dispatch)
            .then((res) => {
                successLog("api/userCoupon == ", res.status)
                if (res.status === 200) {
                    if (res.data !== null) {
                        dispatch(hide());
                    } else {
                        const _msg = ifValid(res.message) ? res.message : "Invalid Coupon Code"
                        console.log("err " + _msg);
                        dispatch(hide());
                    }
                } else {
                    dispatch(hide());
                }
            })
            .catch((o) => {
                const mg = ifValid(error) ? error : "Something went wrong"
                errorLog("api/userCoupon == ", mg)
                dispatch(hide());
            });
    }
}

export const UpdatePrice = (value) => {
    return {
        type: UPDATE_PRICE,
        payload: value,
    };
};

export const CoupnApplied = (obj) => {
    return {
        type: COUPON_APPLIED,
        payload: obj
    }
}

export const setDisValue = (val) => {
    return {
        type: SET_DISCOUNT,
        payload: val
    }
}


export const CheckMemberShipCard = (couponName, user_id, appoint_id, ogPrice, deviceId, doctorId) => {
    return dispatch => {
        dispatch(show());
        http
            .get(`api/user-membership?subId=${couponName}`, dispatch)
            .then((res) => {
                if (res?.status === 200) {
                    const body = {
                        couponName: couponName,
                        doctorId: doctorId,
                        userId: user_id,
                        appointmentId: appoint_id,
                        isApplied: true,
                        deviceId: deviceId,
                    }
                    dispatch({
                        type: SUB_PLANS,
                        payload: {
                            plans: res?.data?.plans,
                            couponData: body
                        }
                    })
                    dispatch(setApplyCouponModal(false))
                    dispatch(setSelectPlanModal(true))
                    dispatch(hide());
                } else {
                    dispatch(hide());
                }
            })
            .catch((o) => {
                console.log('Membership-card api catch', o)
                errorLog("api/fetchCoupon == ", o)
                dispatch(hide());
            });
    }
}

export const checkConsulCount = (couponData, plan_id) => {
    return dispatch => {
        dispatch(show());
        http
            .get(`api/consult-count?subId=${couponData?.couponName}&planId=${plan_id}&doctorId=${couponData?.doctorId}`, dispatch)
            .then((res) => {
                if (res?.status === 200) {
                    const couponPrice = 0;
                    dispatch(UpdatePrice(couponPrice))

                    //apply coupon
                    const body = {
                        couponName: couponData?.couponName,
                        doctorId: couponData?.doctorId,
                        userId: couponData?.userId,
                        appointmentId: couponData?.appointmentId,
                        isApplied: couponData?.isApplied,
                        deviceId: couponData?.deviceId,
                        planId: plan_id
                    }
                    dispatch(CoupnApplied(body))

                    const payload = {
                        text: "Membership Code applied successfully !",
                        subtext: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(showModal(payload))

                    dispatch(setDisValue(couponPrice))
                    dispatch(setApplyCouponModal(false))
                    dispatch(setSelectPlanModal(false))
                    dispatch(hide());
                } else {
                    dispatch(hide());
                }
            })
            .catch((o) => {
                dispatch(hide());
            });
    }
}

export const ApplyMembershipCoupon = (body) => {
    return dispatch => {
        dispatch(show());
        http
            .post(`api/user-membership`, body, dispatch)
            .then((res) => {
                if (res.status === 200) {
                    if (res.data !== null) {
                        dispatch(hide());
                    } else {
                        const _msg = ifValid(res.message) ? res.message : "Invalid Coupon Code"
                        console.log("err " + _msg);
                        dispatch(hide());
                    }
                } else {
                    dispatch(hide());
                }
            })
            .catch((o) => {
                const mg = ifValid(error) ? error : "Something went wrong"
                errorLog("api/userCoupon == ", mg)
                dispatch(hide());
            });
    }
}