import {DIRECT_MODEL, INCOMING_URL, PATIENT_GUEST_RESPONSE} from "../utils/constant";
import http from "../services/http";
import {setCallHeader} from "./bookings";
import {showModal} from "./modal";
import {errorLog, successLog} from "../utils/fireLog";

export const setIncomingUrl = (url) => {
    return {
        type: INCOMING_URL,
        payload: url,
    };
};


const getUrlParams = (url) => {
    let regex = /[?&]([^=#]+)=([^&#]*)/g,
        _params = {},
        match;
    while (match = regex.exec(url))
        _params[match[1]] = match[2];
    return _params
}

export const checkUrlGuestPatient = (email, dob, appointmentId, navigation, room, token) => {
    return (dispatch) => {
        http.post("users/verifyPatient", {
            email: email,
            dob: dob,
            token: token,
        }, dispatch)
            .then((res) => {
                successLog("verifyPatient", res.status)
                if (res.status === 204) {
                    const payload = {
                        text: "Verification failed",
                        iconName: "closecircleo",
                        modalVisible: true,
                        subText: "",
                    };
                    dispatch(showModal(payload));
                }

                dispatch({type: PATIENT_GUEST_RESPONSE, payload: res.data});
                console.log("User exist....")
                dispatch(updateAppointmentStatus(appointmentId, res.data._id, navigation, room, res.data.fullName))
            })
            .catch(o => {
                errorLog("verifyPatient", o)
            });
    };
};
export const updateAppointmentStatus = (appointmentId, patientId, navigation, room, fullName) => {
    let appointment = appointmentId.replace('_PATIENT', '')
    return (dispatch) => {
        http.post("users/appointments", {
            appointmentId: appointment,
            patientId: patientId,
            status: "WAITING"
        }, dispatch)
            .then((res) => {
                successLog("updateAppointmentStatus", res.status)
                dispatch({type: PATIENT_GUEST_RESPONSE, payload: res.data});
                dispatch(setCallHeader(fullName))
                navigation.navigate("WaitingRoom", {
                    channel: room,
                    user: "PATIENT",
                    doctor: room,
                    bookingId: appointment,
                    guestName: fullName
                });
            })
            .catch(o => {
                errorLog("updateAppointmentStatus", o)
            });
    };
};

export const setModelVisible = (payload) => {
    return {
        type: DIRECT_MODEL,
        payload: payload,
    };
};

