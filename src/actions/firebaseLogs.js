import http from "../services/http";
import {errorLog, successLog} from "../utils/fireLog";

export const postAttendeesLogs = (id, payload) => {
    return (dispatch) => {
        http.post("api/attendees/appointments", {
            appointmentId : id,
            attendees : payload
        }, dispatch)
            .then((res) => {
                successLog("postAttendeesLogs", res.status)
            })
            .catch((o) => {
                errorLog("postAttendeesLogs", o)
            });
    };
};

