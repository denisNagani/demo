import { SET_APP_LANGUAGE, GET_APP_LANGUAGE } from "../utils/constant"

export const setLanguage = (lang) => {
    return dispatch => {
        dispatch({
            type: SET_APP_LANGUAGE,
            payload: lang
        })
    }
}

export const getLanguage = (lang) => {
    return dispatch => {
        dispatch({
            type: GET_APP_LANGUAGE,
            payload: lang
        })
    }
}