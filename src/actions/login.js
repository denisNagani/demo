import http from "../services/http";
import { getFromCheckout, setSkipped, setLoggedIn, getRole, setToken, setFromCheckout } from "../services/auth";
import { showModal } from "./modal";
import crashlytics from '@react-native-firebase/crashlytics';
import { errorLog, successLog } from "../utils/fireLog";
import NavigationService from "../components/startScreen/RootNavigation";
import { getUserDetails } from "./profile";
import { loggedInAnalytics } from "../services";
import { getStore } from "../store/index";
import firestore from "@react-native-firebase/firestore";

export const callLoginApi = (payload, navigation, bookingFlow1or2, payEmail) => {
    console.log("login body ", payload);
    return (dispatch) => {
        http.postLogin("oauth/token", payload, dispatch).then(async (response) => {

            if (response.status === 400) {
                const payload = {
                    text: response.message,
                    subtext: "",
                    iconName: "closecircleo",
                    modalVisible: true,
                };
                this.props.showModal(payload);
            } else if (response.status === 200) {
                successLog("logged in user", response.status);
                setToken(response.data);
                setSkipped(false)
                const payload = {
                    text: "You are logged in successfully!!",
                    subText: "",
                    iconName: "checkcircleo",
                    modalVisible: true,
                };
                if (response.data.user.role === "DOCTOR") {
                    successLog("logged in as DOCTOR")
                    // dispatch(showModal(payload));
                    navigation.navigate("DashboardScreen");

                } else if (response.data.user.role === "PATIENT") {
                    successLog("logged in as PATIENT")
                    // dispatch(showModal(payload));
                    const sehetCardFlow = getStore().getState().directLogin.sehetCardFlow
                    if (sehetCardFlow) {
                        NavigationService.navigate('PlanDetails');
                    } else {
                        const checkFlag = await getFromCheckout()
                        const checkoutfrom = JSON.parse(checkFlag)
                        if (checkoutfrom) {
                            if (bookingFlow1or2 === 1) {
                                NavigationService.navigate('BookAppointmentScreen');
                                setSkipped(false)
                                setFromCheckout(false)
                            } else {
                                NavigationService.navigate('BookAppointmentScreenFlow2');
                                setSkipped(false)
                                setFromCheckout(false)
                            }
                        } else {
                            NavigationService.navigate('Home');
                            loggedInAnalytics("LOGGED_IN", payEmail)
                        }
                    }
                    dispatch(getUserDetails())

                } else if (response.data.user.role === "ADMIN") {
                    successLog("logged in as ADMIN")
                    // dispatch(showModal(payload));
                    navigation.navigate("Nurse");
                } else {
                    successLog("user role not found")
                    const payload = {
                        text:
                            "Access denied, you are not authorized to access this device.",
                        iconName: "closecircleo",
                        modalVisible: true,
                        subText: "",
                    };
                    dispatch(showModal(payload));
                }
            }
        }).catch(error => {
            errorLog("login api", error)
            crashlytics().recordError(error);
            reset()
        });
    };
};

export const signUpCall = (payload, navigation, _resetData) => {
    return (dispatch) => {
        http.post("users/signup", payload, dispatch).then((response) => {
            successLog("user sign up api", response.status)
            if (response.status === 200) {
                const payload = {
                    text: "You have successfully signed up",
                    subText: "Start booking appointments now!!",
                    iconName: "checkcircleo",
                    modalVisible: true,
                };
                dispatch(showModal(payload));
                _resetData()
                navigation.navigate("UserLogin");
            }
        }).catch((error) => {
            errorLog("sign up error", error)
        });
    };
};

export const getCountries = () => {
    return firestore()
        .collection("countries")
        .get()
        .then((snapshot) => {
            let data = []
            snapshot.docs.map((itm) => {
                data.push(itm.data())
            })
            return data
        })
        .catch(err => {
            console.log('countries ', err);
        })
}