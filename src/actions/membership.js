import { show, hide } from "../utils/loader/action";
import http from "../services/http";
import { GET_ALL_MEMBERSHIP_PLANS, SUBSCRIPTION_OBJ, SET_PLAN_DETAILS, SET_SEHET_CARD_FLOW, SET_FIREBASE_MEM_DATA, SET_MY_HEALTH_CARD } from "../utils/constant";
import { showModal } from "./modal";
import { base_url, paytm_mid, staging } from "../config";
import AllInOneSDKManager from 'paytm_allinone_react-native';
import { ifValid, isValid } from "../utils/ifNotValid";
import { strings } from "../utils/translation";
import { errorPayload } from "../utils/helper";
import NavigationService from "../components/startScreen/RootNavigation";
import moment from "moment";

export const getAllMebershipPlans = () => {
    return (dispatch) => {
        dispatch(show());
        http.get(`plan`, dispatch)
            .then((res) => {
                dispatch({ type: GET_ALL_MEMBERSHIP_PLANS, payload: res?.data });
                dispatch(hide());
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};

export const createPaytmTxnTokenApi = (payload, planData) => {
    return (dispatch) => {
        dispatch(show());
        let random = Math.random()
        let orderId = JSON.stringify(random)
        let price = payload?.price
        // axios.post()
        http
            .post("createPaytmTxnToken", {
                custId: "123SSS",
                custMobile: "446645465",
                orderId: orderId,
                txnAmount: price,
                currency: "INR"
            }, dispatch)
            .then((res) => {
                console.log({ paytmRes: res });
                AllInOneSDKManager.startTransaction(
                    orderId,
                    paytm_mid,
                    res?.data?.body?.txnToken,
                    price.toString(),
                    "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId,
                    staging,
                    true
                ).then(result => {
                    console.log({ result });
                    dispatch(paymentStatusUpdate(result, orderId, planData))
                }).catch(err => {
                    console.log("AllInOneSDKManager.startTransaction catch = ", JSON.stringify(err));
                    dispatch(showModal(errorPayload("Transaction cancelled")))
                    dispatch(hide())
                })

            })
            .catch((err) => {
                dispatch(hide())
                dispatch(showModal(errorPayload('err ' + err)))
                console.log(JSON.stringify(err));
            });
    };
};

export const paymentStatusUpdate = (result, orderId, planData) => {
    return (dispatch) => {
        dispatch(show());
        http
            .post("paymentStatus", { orderId }, dispatch)
            .then((res) => {
                console.log(" payment success res " + JSON.stringify(res.data))
                dispatch(hide())
                let _msgRes = res?.data?.body?.resultInfo?.resultMsg
                if (ifValid(_msgRes)) {
                    let _resMsg = _msgRes;
                    let paidAmount = res?.data?.body?.txnAmount;
                    if (_resMsg == "Txn Success") {
                        NavigationService.navigate('Success', {
                            planData
                        })

                        const activatedDate = moment(new Date()).format('YYYY-MM-DD')
                        if (planData?.newCard == false) {
                            const bdy = {
                                planId: {
                                    id: planData?._id,
                                    paidAmount: paidAmount,
                                    activated: activatedDate
                                },
                                subId: planData?.subId
                            }
                            dispatch(addPlanToSubscriptionApi(bdy))
                        } else {
                            const bdy = {
                                planId: {
                                    id: planData?._id,
                                    paidAmount: paidAmount,
                                    activated: activatedDate,
                                },
                                dateTime: new Date(),
                                referenceCode: planData?.referenceCode
                            }
                            dispatch(subscribeUserApi(bdy))
                        }
                    } else {
                        dispatch(showModal(errorPayload(strings.transaction_cancelled)))
                    }
                } else {
                    console.log(JSON.stringify(res))
                }
            })
            .catch((err) => {
                console.log("paymentStatus err catch " + err);
                dispatch(hide())
            });
    };
};

export const subscribeUserApi = (body) => {
    return (dispatch) => {
        dispatch(show());
        http.post(`api/subscription`, body, dispatch)
            .then((res) => {
                console.log('res=> ', res);
                dispatch(hide());
                dispatch(setSubcriptionObj(res?.data))
                dispatch(getMyCardDetails())
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};

export const addPlanToSubscriptionApi = (body) => {
    return (dispatch) => {
        dispatch(show());
        http.put(`api/addPlanToSubscription`, body, dispatch)
            .then((res) => {
                console.log('res=> ', res);
                dispatch(hide());
                dispatch(setSubcriptionObj(res?.data))
                dispatch(getMyCardDetails())
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};

export const getMyCardDetails = () => {
    return (dispatch) => {
        dispatch(show());
        http.get(`api/plan-details`, dispatch)
            .then((res) => {
                dispatch({ type: SET_MY_HEALTH_CARD, payload: res?.data });
                dispatch(hide());
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};

export const setSubcriptionObj = (data) => {
    return dispatch => {
        dispatch({
            type: SUBSCRIPTION_OBJ,
            payload: data
        })
    }
}

export const setPlanDetails = (data) => {
    return dispatch => {
        dispatch({
            type: SET_PLAN_DETAILS,
            payload: data
        })
    }
}

export const setSehetCardFlow = (data) => {
    return dispatch => {
        dispatch({
            type: SET_SEHET_CARD_FLOW,
            payload: data
        })
    }
}

export const setFirebaseMemData = (data) => {
    return dispatch => {
        dispatch({
            type: SET_FIREBASE_MEM_DATA,
            payload: data
        })
    }
}