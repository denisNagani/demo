import { CLOCK_TIME, HIDE_APPOINTMENT_MODAL, HIDE_MODAL, SHOW_APPOINTMENT_MODAL, SHOW_MODAL, SHOW_MEDIA_MODAL, HIDE_MEDIA_MODAL, TC_MODAL, SET_COUNTRY, SET_PHOTO_PICKER_MODAL } from "../utils/constant";


export const showModal = (payload) => {
    return (dispatch) => {
        dispatch(hideAppointmentModal());
        dispatch(hideModal());
        dispatch(setClockTime(3));
        dispatch({ type: SHOW_MODAL, payload: payload })
    };
};

export const setClockTime = (payload) => {
    return {
        type: CLOCK_TIME,
        payload: payload,
    };
};

export const hideModal = () => {
    return {
        type: HIDE_MODAL,
        payload: false,
    };
};

export const showAppointmentModal = (payload) => {
    return (dispatch) => {
        dispatch({ type: SHOW_APPOINTMENT_MODAL, payload: payload })
    };
};
export const hideAppointmentModal = () => {
    return {
        type: HIDE_APPOINTMENT_MODAL,
        payload: false,
    };
};

export const showMediaModal = () => {
    return {
        type: SHOW_MEDIA_MODAL,
        payload: true,
    };
};
export const hideMediaModal = () => {
    return {
        type: HIDE_MEDIA_MODAL,
        payload: false,
    };
};
export const setTcModal = (obj) => {
    return {
        type: TC_MODAL,
        payload: obj,
    };
};
export const setCountryModal = (obj) => {
    return {
        type: SET_COUNTRY,
        payload: obj,
    };
};
export const setPhotoPickerModal = (obj) => {
    return {
        type: SET_PHOTO_PICKER_MODAL,
        payload: obj,
    };
};
