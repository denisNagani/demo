import { show, hide } from "../utils/loader/action";
import http from "../services/http";

export const sendNotification = (body) => {
    console.log("inside notification api ", JSON.stringify(body))
    return (dispatch) => {
        dispatch(show());
        http
            .post(`api/notification`, body, dispatch)
            .then((res) => {
                console.log("notification resposne", res)
                dispatch(hide());
            })
            .catch((o) => {
                console.log(" err notification resposne", o)
                dispatch(hide());
            });
    };
};

export const sendReminderNotification = (body) => {
    console.log("inside notification api ", JSON.stringify(body))
    return (dispatch) => {
        dispatch(show());
        http
            .post(`/api/reminderNotification`, body, dispatch)
            .then((res) => {
                console.log("reminder notification resposne", res)
                dispatch(hide());
            })
            .catch((o) => {
                console.log(" err reminder  notification resposne", o)
                dispatch(hide());
            });
    };
};
