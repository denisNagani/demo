import { GET_SLOTS, PRESCRIPTION_DATA, SHOW_PRESCRIPTION_MODEL } from "../utils/constant";

export const setPrescriptionModel = (value) => {
	return {
		type: SHOW_PRESCRIPTION_MODEL,
		payload: value,
	};
};

export const setPrescriptionData = (selectedPatient, addMedicineId) => {
	return {
		type: PRESCRIPTION_DATA,
		payload: {
			selectedPatient,
			selectedAppointment: selectedPatient,
			addMedicineId,
		},
	};
};
