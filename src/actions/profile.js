import { GET_SPECIALITIES, USER_PROFILE, SELECTED_LANGUAGE, SET_FROM_DYNAMIC_LINK, QUICK_DOC, GET_DOC_LIST, SET_MEMBERS, SET_MEMBERS_EXTRADATA } from "../utils/constant";
import http from "../services/http";
import { hide, show } from "../utils/loader/action";
import { showModal } from "./modal";
import { base_url } from "../config";
import { getBookingHistory, getUpcomingBookings, getUpcomingBookingsByDate, setDoctorName, setDoctorImage, } from "./bookings";
import moment from "moment-timezone";
import axios from "axios";
import { errorLog, successLog } from "../utils/fireLog";
import { strings } from "../utils/translation";
import { setSummaryDateNTime, postDoctorSlot, setSelectedDoctor, setAppointmentType, getSlotTimeZone } from "./appointment";
import { setIsBookingFlow1or2 } from "../scenes/patient/book-appointment/action";
import { ifValid, ifNotValid } from "../utils/ifNotValid";
import { setFollowUp } from "../scenes/patient/upcoming-past-bookings/components/action";
import { selectedSpecialityObj } from "./sehet/speciality-action";
import { calculateDrConsultationFee, errorPayload } from "../utils/helper";
import { setNearbyDoctors } from "./sehet/user-action";
import { getAllMebershipPlans } from "./membership";

export const getUserDetails = () => {
    return (dispatch) => {
        dispatch(show());
        http
            .get("api/user/profile", dispatch)
            .then((res) => {
                successLog("find doctors get", res.status)
                dispatch(hide());
                dispatch({ type: USER_PROFILE, payload: res.data });
            })
            .catch((o) => {
                errorLog("user profile get", o)
                dispatch(hide());
            });
    };
};

export const findDoctors = () => {
    return (dispatch) => {
        dispatch(show());
        http
            .get(`api/doctors`, dispatch)
            .then((res) => {
                successLog("find GET_SPECIALITIES get", res.status)
                dispatch(hide());
                dispatch({ type: GET_SPECIALITIES, payload: res.data });
            })
            .catch((o) => {
                errorLog("find doctors get", o)
                dispatch(hide());
            });
    };
};

export const getRefreshTokenHome = (refToken, navigation) => {
    return (dispatch) => {
        // let payload = `refresh_token=${refToken}&grant_type=refresh_token&offset=${moment(
        //     new Date()
        // ).format()}&client_id=${client_id}&client_secret=${client_secret}`;
        // http.postLogin("oauth/token", payload, dispatch, navigation).then((response) => {
        //     if (response.status === 200) {
        //         dispatch({type: LOGIN_REFRESH, payload: response.data});
        //         setToken(response.data);
        dispatch(getUserDetails());
        dispatch(getBookingHistory());
        dispatch(getUpcomingBookings());
        //     }
        //
        // }).catch(o => {
        //
        // });
    };
};
export const getRefreshTokenDash = (refToken) => {
    return (dispatch) => {
        // let payload = `refresh_token=${refToken}&grant_type=refresh_token&offset=${moment(
        //     new Date()
        // ).format()}&client_id=${client_id}&client_secret=${client_secret}`;
        // http.postLogin("oauth/token", payload).then((response) => {
        //     if (response.status === 400) {
        //         const payload = {
        //             text: response.message,
        //             subText: "",
        //             iconName: "closecircleo",
        //             modalVisible: true,
        //             iconColor: "#C70315",
        //         };
        //         dispatch(showModal(payload));
        //     } else if (response.status === 200) {
        //         dispatch({type: LOGIN_REFRESH, payload: response.data});
        //         setToken(response.data).then((r) => {
        dispatch(getUserDetails());
        dispatch(getUpcomingBookingsByDate(moment(new Date()).format("YYYY-MM-DD")));
        //         });
        //     }
        // });
    };
};

export const image_Upload = (data, token, payload, navigation) => {
    const request = {
        method: "POST",
        url: base_url + "api/user/uploadProfile",
        data: data,
        headers: {
            Authorization: "Bearer " + token,
            "Content-Type": "multipart/form-data",
        },
    };
    // return console.log(JSON.stringify(data) + token + JSON.stringify(payload));

    return (dispatch) => {
        dispatch(show());
        axios(request)
            .then((response) => {
                successLog("upload image successful ", response.status)
                dispatch(hide());
                dispatch(updateProfilePatient(payload, navigation))
            })
            .catch((error) => {
                const payload = {
                    text: JSON.stringify(error),
                    subText: "",
                    iconName: "closecircleo",
                    modalVisible: true,
                    iconColor: "#C70315",
                };
                dispatch(hide());
                dispatch(updateProfilePatient(payload, navigation))
                errorLog("upload image error", error)
                dispatch(showModal(payload));
            });
    };
};


export const updateProfilePatient = (payload, navigation, fromCard) => {
    successLog('calling api.')
    return (dispatch) => {
        http.put("api/users", payload, dispatch)
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    successLog("update patient profile ", res.status)
                    const payload = {
                        text: `${strings.profile_updated}`,
                        subText: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(getUserDetails())
                    if (fromCard == true) {
                        navigation.navigate('Membership');
                        dispatch(getAllMebershipPlans())
                    } else {
                        navigation.goBack();
                    }
                    dispatch(showModal(payload))

                }
            })
            .catch((err) => {
                errorLog("update Profile Patient", err)
            });
    };
};

export const updateDoctorProfile = (payload, navigation) => {
    successLog('calling api.')
    return (dispatch) => {
        http.post("api/doctors/updateDoctor", payload, dispatch)
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    successLog("update updateDoctorProfile profile ", res.status)
                    const payload = {
                        text: `${strings.profile_updated}`,
                        subText: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(getUserDetails())
                    navigation.goBack();
                    dispatch(showModal(payload))

                }
            })
            .catch((err) => {
                errorLog("updateDoctorProfile", err)
            });
    };
};



export const image_UploadDoctor = (data, token, payload, navigation) => {
    const request = {
        method: "POST",
        url: base_url + "api/user/uploadProfile",
        data: data,
        headers: {
            Authorization: "Bearer " + token,
            "Content-Type": "multipart/form-data",
        },
    };

    return (dispatch) => {
        dispatch(show());
        axios(request)
            .then((response) => {
                successLog("upload image successful ", response.status)
                dispatch(hide());
                dispatch(updateDoctorProfile(payload, navigation))
            })
            .catch((error) => {
                const payload = {
                    text: JSON.stringify(error),
                    subText: "",
                    iconName: "closecircleo",
                    modalVisible: true,
                    iconColor: "#C70315",
                };
                dispatch(hide());
                dispatch(updateDoctorProfile(payload, navigation))
                errorLog("upload image error", error)
                dispatch(showModal(payload));
            });
    };
};

export const Set_App_Language = (lang) => {
    return dispatch => {
        dispatch({
            type: SELECTED_LANGUAGE,
            payload: lang
        })
    }
}

export const setFromDynamicLink = (value, data) => {
    return dispatch => {
        dispatch({
            type: SET_FROM_DYNAMIC_LINK,
            payload: {
                value,
                data
            }
        })
    }
}

export const BookFlow2Action = (doc, datee, selectedObject, slotTimeZone, props, get_price, selectedCosultMode) => {
    return dispatch => {
        let data = {
            doctorId: doc?._id,
            appointmentDate: datee,
            appointmentType: "VIDEO",
            slotId: selectedObject?._id,
            status: "BOOKED",
            timeZone: slotTimeZone?.timeZone,
            type: "BOOKING"
        };
        if (selectedCosultMode == "OFFLINE") {
            data.callType = "Offline"
        }
        dispatch(setSummaryDateNTime({
            appointmentDate: datee,
            appointmentTime: ifValid(selectedObject.startTime) ? selectedObject.startTime : ''
        }));
        console.log("BookFlow2Action ", get_price);
        dispatch(postDoctorSlot(data, props.navigation, get_price));
        dispatch(setDoctorName(doc.fullName));
        dispatch(setDoctorImage(doc.imageUrl));
        dispatch(setIsBookingFlow1or2(2));
    }
}

export const SetSpecialityDetails = (doc) => {
    return dispatch => {
        dispatch(setFollowUp(null))
        dispatch(setSelectedDoctor(doc))

        // const specialityArray = doc.specialityId;
        // const item = calculateDrConsultationFee(specialityArray)
        // if (ifNotValid(item) || ifNotValid(item.price))
        //     return;
        // let _price = parseInt(item.price)
        // let m_price = parseInt(item.price)
        // let m_specialPrice = parseInt(item?.specialPrice)
        // let percentage = (m_price / 100) * m_specialPrice;
        // m_price = m_price + percentage;

        let m_price = doc?.fee
        dispatch(selectedSpecialityObj({ ...doc, price: m_price, m_price: m_price, dateTime: new Date() }))
    }
}

export const getQuickDoctor = (payload, navigation) => {
    return (dispatch) => {
        dispatch(show());
        http.get(`api/quickDoctors/${payload?.date}/${payload?.time}`, dispatch)
            .then((res) => {
                if (res.status === 200) {
                    dispatch({
                        type: QUICK_DOC,
                        payload: res.data
                    })
                    navigation.navigate("QuickDoc", {
                        ...payload,
                        available: true
                    })
                    dispatch(setAppointmentType('quickCall'))
                    dispatch(getSlotTimeZone())
                } else if (res.status === 204) {
                    navigation.navigate("QuickDoc", {
                        ...payload,
                        available: false
                    })
                    dispatch({
                        type: QUICK_DOC,
                        payload: null
                    })
                    dispatch(hide())
                }
                else {
                    dispatch({
                        type: QUICK_DOC,
                        payload: null
                    })
                }
                dispatch(hide());
            })
            .catch((err) => {
                errorLog("getQuickDoctor", err)
            });

    }
}

export const saveLatLongOfDoctors = (cords) => {
    return (dispatch) => {
        dispatch(show());
        http
            .put("api/user/saveLatLongOfDoctors", {
                latitude: cords?.latitude,
                longitude: cords?.longitude
            }, dispatch)
            .then((res) => {
                console.log('saveLatLongOfDoctors res ', res);
                dispatch(hide())
            })
            .catch((o) => {
                console.log(o);
                dispatch(hide())
            });
    };
};

export const getNearbyDoctors = (cords) => {
    return (dispatch) => {
        dispatch(show());
        http
            .get(`user/getNearbyDoctors?lat=${cords?.latitude}&long=${cords?.longitude}`, dispatch)
            .then((res) => {
                dispatch(setNearbyDoctors(res?.data))
                dispatch(hide())
            })
            .catch((o) => {
                console.log(o);
                dispatch(hide())
            });
    };
};

export const setMembersData = (data) => {
    return dispatch => {
        dispatch({
            type: SET_MEMBERS,
            payload: data
        })
    }
}

export const setMembersExtraData = (data) => {
    return dispatch => {
        dispatch({
            type: SET_MEMBERS_EXTRADATA,
            payload: data
        })
    }
}