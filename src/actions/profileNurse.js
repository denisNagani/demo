import {LOGIN_REFRESH, USER_PROFILE} from "../utils/constant";
import http from "../services/http";
import {hide, show} from "../utils/loader/action";
import {setToken} from "../services/auth";
import {showModal} from "./modal";
import {client_id, client_secret} from "../config";
import {getBookingHistory, getUpcomingBookings,} from "./bookings";
import {getUpcomingBookingsAdmin} from "./bookingsNurse";
import {errorLog, successLog} from "../utils/fireLog";

export const getUserDetails = () => {
	return (dispatch) => {
		dispatch(show());
		http
			.get("/api/user/token", dispatch)
			.then((res) => {
				successLog("getUserDetailsNurse", res.status)
				dispatch(hide());
				dispatch({ type: USER_PROFILE, payload: res.data });
			})
			.catch((o) => {
				dispatch(hide());
				errorLog("getUserDetails nurse", o)
			});
	};
};


export const getRefreshTokenHome = (refToken) => {
	return (dispatch) => {
		let payload = `refresh_token=${refToken}&grant_type=refresh_token&client_id=${client_id}&client_secret=${client_secret}`;
		http.postLogin("oauth/token", payload).then((response) => {
			successLog("getRefreshTokenHome", response.status)
			if (response.status === 400) {
				const payload = {
					text: response.message,
					subText: "",
					iconName: "closecircleo",
					modalVisible: true,
					iconColor: "#C70315",
				};
				dispatch(showModal(payload));
			} else if (response.status === 200) {
				dispatch({ type: LOGIN_REFRESH, payload: response.data });
				setToken(response.data);
				dispatch(getUserDetails());
				dispatch(getBookingHistory());
				dispatch(getUpcomingBookings());
			}
		});
	};
};
export const getRefreshTokenDash = (refToken) => {
	return (dispatch) => {
		// let timezone = new Date().getTimezoneOffset();
		// let result = moment(timezone).format("HH:mm");
		// let payload = `refresh_token=${refToken}&grant_type=refresh_token&offset=${result}&client_id=${client_id}&client_secret=${client_secret}`;
		// http.postLogin("oauth/token", payload).then((response) => {
		// 	if (response.status === 400) {
		// 		const payload = {
		// 			text: response.message,
		// 			subText: "",
		// 			iconName: "closecircleo",
		// 			modalVisible: true,
		// 			iconColor: "#C70315",
		// 		};
		// 		dispatch(showModal(payload));
		// 	} else if (response.status === 200) {
		// 		dispatch({ type: LOGIN_REFRESH, payload: response.data });
		// 		setToken(response.data).then((r) => {
					dispatch(getUserDetails());
					dispatch(getUpcomingBookingsAdmin());
		// 		});
		// 	}
		// });
	};
};
