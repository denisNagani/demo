import { errorLog, successLog } from "../../utils/fireLog";
import { hide, show } from "../../utils/loader/action";
import http from "../../services/http";
import { DOCTOR_SLOT } from "../../utils/constant";
import { ifValid } from "../../utils/ifNotValid";

export const getDoctorsSlotFlow1 = (slotId, date) => {
    // 2020-09-21
    return (dispatch) => {
        dispatch({ type: DOCTOR_SLOT, payload: [] });
        dispatch(show());
        http.get(`api/slots/${slotId}/${date}`, dispatch)
            .then((res) => {
                successLog("get doctor slots", res.status)
                if (res.status != 204) {
                    if (ifValid(res.data))
                        dispatch({ type: DOCTOR_SLOT, payload: res.data });
                }
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("get Doctors Slot", o)
                dispatch(hide());
            });
    };
};
