import { POST_DOCTOR_SLOT } from "../../utils/constant";
import { errorLog, successLog } from "../../utils/fireLog";
import { ifValid } from "../../utils/ifNotValid";
import http from '../../services/http'
import { hide, show } from "../../utils/loader/action";
import { showModal } from "../modal";
import { errorPayload } from "../../utils/helper";


export const postPatientDetails = (body, navigation, showPaymentScreen) => {
    return (dispatch) => {
        dispatch(show());
        http.put(`users/updateProfile`, body, dispatch)
            .then((res) => {
                successLog("postPatientDetails", res.status)
                if (res.status === 200) {
                    if (ifValid(res.data)) {
                        // dispatch(setPatientAddSuccess(true))
                    }
                    showPaymentScreen()
                    successLog("showPaymentScreen")
                }
                dispatch(hide());
            })
            .catch((o) => {
                showPaymentScreen()
                dispatch(hide());
                errorLog("postPatientDetails  ", o)
            });
    };
};

export const postDoctorSlotFlow1 = (payload, navigation) => {
    return (dispatch) => {
        dispatch(show());
        http.post(`api/appointments/book`, payload, dispatch)
            .then((res) => {
                dispatch(hide());
                successLog("post doctor slot", res.status)
                if (res.status === 200) {
                    if (ifValid(res.data)) {
                        dispatch({ type: POST_DOCTOR_SLOT, payload: res.data });
                        navigation.navigate("AcknowledgmentScreen")
                    }
                } else {
                    dispatch(showModal(errorPayload('Something went wrong')));
                }
            })
            .catch((o) => {
                errorLog("postDoctorSlot", o)
                dispatch(showModal(errorPayload(JSON.stringify(o))));
                dispatch(hide());
            });
    };
};

export const uploadAttachment = (body, user_id, callback) => {
    return (dispatch) => {
        dispatch(show());
        http.post(`api/users/uploadDocument/${user_id}`, body, dispatch)
            .then((res) => {
                if (res.status === 200) {
                    console.log('uploaded image url data ', res?.data);
                    callback(res?.data)
                }
            })
            .catch((o) => {
                callback()
                dispatch(hide());
            });
    };
}

export const uploadPrescriptionImage = (body, appointment_id, callback) => {
    return (dispatch) => {
        dispatch(show());
        http.post(`api/uploadPrescriptionImage/${appointment_id}`, body, dispatch)
            .then((res) => {
                if (res.status === 200) {
                    console.log('uploaded image url data ', res?.data);
                    callback(res?.data)
                }
            })
            .catch((o) => {
                callback()
                dispatch(hide());
            });
    };
}

export const postDoctorQuickBook = (payload, navigation) => {
    return (dispatch) => {
        dispatch(show());
        http.post(`api/appointments/quickBook`, payload, dispatch)
            .then((res) => {
                dispatch(hide());
                if (res.status === 200) {
                    if (ifValid(res.data)) {
                        // connect to viceo call
                        navigation.navigate("UpcomingPastBookings")
                    }
                } else {
                    dispatch(showModal(errorPayload('Something went wrong')));
                }
            })
            .catch((o) => {
                errorLog("postDoctorSlot", o)
                dispatch(showModal(errorPayload(JSON.stringify(o))));
                dispatch(hide());
            });
    };
};