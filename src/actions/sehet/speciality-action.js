import { hide, show } from "../../utils/loader/action";
import http from "../../services/http";
import { errorLog, successLog } from "../../utils/fireLog";
import { GET_SPECIALITIES, GET_TOP_SPECIALITIES, INCOMING_URL, SELECTED_SPECIALITY, APPLY_COUPOUN_MODAL, CITY_FILTER_LIST, SELECT_PLAN_MODEL, MIC_MODEL, GET_HOSPITALS } from "../../utils/constant";
import { setSpecialityFilter } from "../../scenes/patient/select-and-book/action";

export const getSpeciality = () => {
    return (dispatch) => {
        dispatch(show());
        http
            .get(`topSpecialities`, dispatch)
            .then((res) => {
                successLog("getSpeciality get", res.status)
                dispatch(hide());
                if (res.data === undefined || res.data == null) {
                    successLog(JSON.stringify(res.data) + "wrong data ")
                    return
                }
                dispatch({ type: GET_SPECIALITIES, payload: res.data });
            })
            .catch((o) => {
                errorLog("getSpeciality", o)
                dispatch(hide());
            });
    };
};

export const getHospitals = () => {
    return (dispatch) => {
        dispatch(show());
        http
            .get(`allHospitals`, dispatch)
            .then((res) => {

                console.log('res?.data ', res?.data);
                dispatch(hide());
                if (res?.data?.length > 0) {
                    dispatch({ type: GET_HOSPITALS, payload: res.data });
                }
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};

export const getCities = () => {
    return (dispatch) => {
        dispatch(show());
        http
            .get(`city`, dispatch)
            .then((res) => {
                dispatch(hide());
                if (res.data === undefined || res.data == null) {
                    successLog(JSON.stringify(res.data) + "wrong data ")
                    return
                }
                const cityArray = []
                res?.data?.map((item, index) => {
                    let sp = {
                        _id: item._id,
                        name: item.city,
                        value: false
                    }
                    cityArray.push(sp)
                })
                dispatch(setSpecialityFilter(CITY_FILTER_LIST, cityArray))
                dispatch(setSpecialityFilter(CITY_FILTER_SELECTED_LIST, cityArray))
            })
            .catch((o) => {
                errorLog("getCities", o)
                dispatch(hide());
            });
    };
};


export const selectedSpecialityObj = (obj) => {
    return {
        type: SELECTED_SPECIALITY,
        payload: obj,
    };
};

export const setApplyCouponModal = (value) => {
    return {
        type: APPLY_COUPOUN_MODAL,
        payload: value,
    }
}

export const setSelectPlanModal = (value) => {
    return {
        type: SELECT_PLAN_MODEL,
        payload: value,
    }
}

export const setMicModal = (value) => {
    return {
        type: MIC_MODEL,
        payload: value,
    }
}