import { hide, show } from "../../utils/loader/action";
import http from "../../services/http";
import { errorLog, successLog } from "../../utils/fireLog";
import {
    DOC_PROFILE,
    GET_DOC_LIST,
    OTP_RESPONSE_GOOGLE,
    OTP_SESSION,
    VERIFIED_EMAIL,
    VERIFIED_MOBILE,
    GET_OFFERS,
    GET_BANNERS,
    GET_DOC_LIST_TOP,
    GET_DOC_LIST_TEMP,
    OTP_USING,
    GET_FAQS,
    KEYWORD_SUGGESTION,
    SET_LOCATION_DATA,
    GET_NEARBY_DOC,
    GET_DOCTOR_BY_SPECIALITY
} from "../../utils/constant";
import { showModal } from "../modal";
import { ifNotValid, ifValid, isValid } from "../../utils/ifNotValid";
import { successPayload, errorPayload } from "../../utils/helper";
import NavigationService from "../../components/startScreen/RootNavigation";
import { client_id, client_secret } from "../../config";
import { getFromCheckout, getSkipped, setFromCheckout, setSkipped, setToken } from "../../services/auth";
import moment from "moment";
import { getUserDetails, BookFlow2Action } from "../profile";
import database from '@react-native-firebase/database'
import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'
import messaging from '@react-native-firebase/messaging'
import { fireReport, loggedInAnalytics } from "../../services";
import { signInWithPhoneNumber } from "../../scenes/patient/login-whatsapp/action";
import { strings } from "../../utils/translation";
import { getStore, store } from "../../store/index";
import DeviceInfo from 'react-native-device-info'
import { Platform } from "react-native";

export const getDocList = (url) => {
    return (dispatch) => {
        dispatch(show());
        http
            .get(url, dispatch)
            .then((res) => {
                successLog("getDocList get", res.status)
                dispatch(hide());
                if (res.data == undefined || res.data == null) {
                    successLog(JSON.stringify(res.data) + "wrong data ")
                    return
                }
                // dispatch({ type: GET_DOC_LIST_TOP, payload: res.data });
                // dispatch({ type: GET_DOC_LIST_TEMP, payload: res.data });
                dispatch({ type: GET_DOC_LIST, payload: res?.data });
            })
            .catch((o) => {
                errorLog("getSpeciality", o)
                dispatch(hide());
            });
    };
};

export const getTopDocList = (url) => {
    return (dispatch) => {
        dispatch(show());
        http
            .get(url, dispatch)
            .then((res) => {
                dispatch(hide());
                dispatch({ type: GET_DOC_LIST_TOP, payload: isValid(res?.data) ? res?.data : [] });
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};



export const getDocProfile = (id) => {
    return (dispatch) => {
        dispatch(show());
        http
            .get(`users/user/${id}`, dispatch)
            .then((res) => {
                successLog("getDocProfile get", res.status)
                dispatch(hide());
                if (res.data == undefined || res.data == null) {
                    successLog(JSON.stringify(res.data) + "wrong data ")
                    return
                }
                dispatch({ type: DOC_PROFILE, payload: res.data });
            })
            .catch((o) => {
                errorLog("getDocProfile", o)
                dispatch(hide());
            });
    };
};


export const sendOtpAction = (body, dispatch, navigation, send = true) => {
    successLog("sendOtpAction post ", body)
    return (dispatch) => {
        dispatch(show());
        http.post(`auth/sendOTP`, body, dispatch)
            .then((res) => {
                successLog("sendOtpAction post ", res.status)
                dispatch(hide());
                if (res.data == undefined || res.data == null) {
                    successLog(JSON.stringify(res.data) + "wrong data ")
                } else {
                    successLog(JSON.stringify(res.data))
                    successLog('after 200')
                    console.log("_id session ", res.data._id);
                    console.log("mobileAndMedia ", body);
                    /// send otp
                    if (send === true && ifValid(res.data)) {
                        dispatch(setOtpSession(res.data._id))
                        navigation.navigate("OTPScreen", { mobileAndMedia: body })
                    } else {
                        ///resend otp
                        dispatch(setOtpSession(res.data._id))
                        dispatch(showModal(successPayload("Otp has been shared again")))
                    }
                }
            })
            .catch((o) => {
                errorLog("sendOtpAction", o)
                dispatch(hide());
            });
    };
};


export const verifyOtp = (body, navigation, bookingFlow1or2, fromDynamicLink) => {
    console.log("verifyOtp post ", body)
    return (dispatch) => {
        dispatch(show());
        http.post(`auth/verifyOTPAndSignup`, body, dispatch)
            .then((res) => {
                successLog("verifyOtp post ", res.status)
                dispatch(hide());

                if (ifNotValid(res.message)) {
                    successLog(JSON.stringify(res.message) + "wrong data ")
                } else {
                    successLog(JSON.stringify(res.message))
                    if (ifNotValid(bookingFlow1or2)) {
                        errorLog("invalid booking flow..!")
                        return
                    }
                    if (res.status === 200)
                        userLogin(navigation, bookingFlow1or2, body, dispatch, fromDynamicLink)
                }
            })
            .catch((o) => {
                errorLog("verifyOtp", o)
                dispatch(hide());
            });
    };
};

export const verifyOtpFirebase = (otpResponseGoogle, body, navigation, bookingFlow1or2, fromDynamicLink) => {
    console.log("verifyOtp post ", body)
    return async (dispatch) => {
        dispatch(show());
        const creds = auth.PhoneAuthProvider.credential(
            otpResponseGoogle?.verificationId,
            body?.otp
        )
        auth()
            .signInWithCredential(creds)
            .then(confirmed => {
                console.log({ confirmed });
                if (ifValid(confirmed)) {
                    //now add / update user to db
                    http.post(`auth/verifyOTPWithFirebase`, body, dispatch)
                        .then((res) => {
                            successLog("verifyOtp post ", res.status)
                            if (res.status === 200) {
                                fireReport(body.mobileNo)
                                userLogin(navigation, bookingFlow1or2, body, dispatch, fromDynamicLink)
                            }
                        })
                        .catch((o) => {
                            dispatch(hide());
                        });
                } else {
                    dispatch(showModal(errorPayload("invalid OTP")));
                    dispatch(hide());
                }
            })
            .catch(error => {
                if (error.code == "auth/invalid-verification-code") {
                    dispatch(showModal(errorPayload("Invalid OTP")));
                } else if (error.code == "auth/session-cookie-expired") {
                    //resend opt using api
                    dispatch(sendOtpAction({
                        mobileNo: body.mobileNo,
                        msgMode: "MOBILE"
                    }, dispatch, navigation, true))
                    dispatch(setOptUsing('api'))
                    dispatch(showModal(errorPayload("OTP Expired...Otp has been shared again")))
                } else {
                    dispatch(showModal(errorPayload(error?.code)))
                }
                dispatch(hide());
            })
    };
};

export const userLogin = async (navigation, bookingFlow1or2, body, dispatch, fromDynamicLink) => {
    successLog("inside login api")
    const email = body.mobileNo;
    const password = body.otp;
    const fcmToken = body.fcmToken;
    const msgMode = body.msgMode;
    const deviceId = DeviceInfo.getUniqueId()
    if (email.includes('@')) dispatch(setVerifiedEmail(email))
    else dispatch(setVerifiedMobile(email))
    const myToken = await messaging().getToken()
    const appPlatform = Platform.OS
    const tokenObj = {
        token: isValid(fcmToken) ? fcmToken : myToken,
        deviceId: deviceId,
        platform: appPlatform
    }
    let payload = `username=${email}&password=${password}&fcmToken=${JSON.stringify(tokenObj)}&offset=${moment(new Date())
        .format()}&grant_type=password&client_id=${client_id}&client_secret=${client_secret}&msgMode=${msgMode}`;
    successLog('inside userLogin')
    try {
        http.postLogin("oauth/token", payload, dispatch)
            .then(async (response) => {

                if (response.status === 400) {
                    const payload = {
                        text: response.message,
                        subtext: "",
                        iconName: "closecircleo",
                        modalVisible: true,
                    };
                    this.props.showModal(payload);
                    dispatch(hide())
                } else if (response.status === 200) {
                    successLog("logged in user", response.status)
                    setToken(response.data).then(r => {
                    });

                    dispatch(getUserDetails())

                    const sehetCardFlow = getStore().getState().directLogin.sehetCardFlow
                    if (sehetCardFlow) {
                        NavigationService.navigate('PlanDetails');
                        setSkipped(false)
                    } else {
                        const checkFlag = await getFromCheckout()
                        const checkoutfrom = JSON.parse(checkFlag)
                        if (checkoutfrom) {
                            if (bookingFlow1or2 === 1) {
                                NavigationService.navigate('BookAppointmentScreen');
                                setSkipped(false)
                                setFromCheckout(false)
                            } else {
                                if (fromDynamicLink?.value) {
                                    //redirect to summary page
                                    setSkipped(false)
                                    setFromCheckout(false)
                                    const {
                                        doc,
                                        datee,
                                        selectedObject,
                                        slotTimeZone,
                                        props,
                                        get_price
                                    } = fromDynamicLink?.data;
                                    dispatch(BookFlow2Action(doc, datee, selectedObject, slotTimeZone, props, get_price))
                                } else {
                                    NavigationService.navigate('BookAppointmentScreenFlow2');
                                    setSkipped(false)
                                    setFromCheckout(false)
                                }

                            }
                        } else {
                            NavigationService.navigate('Home');
                            setSkipped(false)
                            loggedInAnalytics("LOGGED_IN", email)
                        }
                    }

                }
            }).catch(error => {
                dispatch(hide())
                errorLog("login api", error)
            });
    } catch (e) {
        errorLog(e)
    }

}

export const ClearFcmToken = (body) => {
    return (dispatch) => {
        dispatch(show());
        http.put(`removeFcmToken`, body, dispatch)
            .then((res) => {
                successLog("logout status", res.status)
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("logout ", o)
                dispatch(hide());
            });
    };
};


export const setOtpGoogleResponse = (value) => {
    return {
        type: OTP_RESPONSE_GOOGLE,
        payload: value,
    };
};


export const setVerifiedMobile = (value) => {
    return {
        type: VERIFIED_MOBILE,
        payload: value,
    };
};
export const setVerifiedEmail = (value) => {
    return {
        type: VERIFIED_EMAIL,
        payload: value,
    };
};

export const setOtpSession = (value) => {
    return {
        type: OTP_SESSION,
        payload: value,
    };
};

export const setOptUsing = (value) => {
    return {
        type: OTP_USING,
        payload: value,
    };
};

export const getOffers = () => {
    return async dispatch => {
        await database().ref('/offers').on('value', snapshot => {
            const urlsData = snapshot.val();
            const urls = Object.values(urlsData)
            console.log("urls offer", urls);
            dispatch({
                type: GET_OFFERS,
                payload: urls
            })
        })
    }
}

export const getBanners = () => {
    return async dispatch => {
        await database().ref('/doctor_banners').on('value', snapshot => {
            const urlsData = snapshot.val();
            const urls = Object.values(urlsData)
            dispatch({
                type: GET_BANNERS,
                payload: urls
            })
        })
    }
}

export const getFAQ = () => {
    return async dispatch => {
        await firestore()
            .collection("FAQ")
            .orderBy('srno', 'asc')
            .onSnapshot(snapshot => {
                const Faqs = []
                if (isValid(snapshot)) {
                    snapshot?.forEach(doc => {
                        const item = doc.data()
                        Faqs.push(item)
                    })
                    dispatch({
                        type: GET_FAQS,
                        payload: Faqs
                    })
                }
            })
    }
}

export const checkIfUserDoctor = (type, emailOrMobile, navigation, countryCode) => {
    return dispatch => {
        dispatch(show());
        http
            .get(`check-user?type=${type}&emailOrMobile=${emailOrMobile}`, dispatch)
            .then((res) => {
                successLog("checkIfDoctor get", res.status)
                dispatch(hide());
                if (res.data) {
                    signInWithPhoneNumber(emailOrMobile, dispatch, navigation, countryCode)
                } else {
                    dispatch(showModal(errorPayload(strings.doctors_number)))
                }
            })
            .catch((o) => {
                errorLog("getDocProfile", o)
                dispatch(hide());
            });
    }
}

export const getDocListByKeyword = (url) => {
    return (dispatch) => {
        dispatch(show());
        http
            .get(url, dispatch)
            .then((res) => {
                if (isValid(res?.data)) {
                    dispatch(hide());
                    dispatch({ type: GET_DOC_LIST, payload: res?.data });
                    dispatch({ type: GET_DOC_LIST_TEMP, payload: res?.data });
                } else {
                    dispatch({ type: GET_DOC_LIST, payload: [] });
                    dispatch({ type: GET_DOC_LIST_TEMP, payload: [] });
                    dispatch(hide());
                }
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};

export const setKeywordSuggestions = (data) => {
    return dispatch => {
        dispatch({
            type: KEYWORD_SUGGESTION,
            payload: data
        })
    }
}

export const setLocationData = (obj) => {
    return dispatch => {
        dispatch({
            type: SET_LOCATION_DATA,
            payload: obj
        })
    }
}

export const setNearbyDoctors = (obj) => {
    return dispatch => {
        dispatch({
            type: GET_NEARBY_DOC,
            payload: obj
        })
    }
}

export const setDoctorsBySpeciality = (data, search) => {
    return dispatch => {
        dispatch({
            type: GET_DOCTOR_BY_SPECIALITY,
            payload: {
                data,
                search
            }
        })
    }
}

export const getSpecialityDoctors = (id, itemsPerPage, page, fees) => {
    return async (dispatch) => {
        let skip = await getSkipped();
        let auth = skip == 'true' ? '' : 'api/';
        let api_url;
        if (fees) {
            api_url = `${auth}allDoctorsInHospital/${id}?itemsPerPage=${itemsPerPage}&page=${page}&fee=${fees}`
        } else {
            api_url = `${auth}allDoctorsInHospital/${id}?itemsPerPage=${itemsPerPage}&page=${page}`
        }
        dispatch(show());
        http
            .get(api_url, dispatch)
            .then((res) => {
                const curr_data = store.getState().userReducer.doctorBySpeciality
                const resData = isValid(res?.data) ? res?.data : []
                if (page > 1) {
                    if (res?.data != null) {
                        dispatch(setDoctorsBySpeciality([...curr_data, ...resData]))
                    } else if (res?.data == null) {
                        dispatch(setDoctorsBySpeciality(curr_data))
                    }
                } else {
                    dispatch(setDoctorsBySpeciality(resData))
                }
                dispatch(hide())
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};