import {GET_GUEST_URL, GET_SLOTS, POST_SHARE_ROOM,} from "../utils/constant";
import http from "../services/http";
import {hide, show} from "../utils/loader/action";
import {showModal} from "./modal";
import {Clipboard} from 'react-native';
import {getUpcomingBookingsByDate} from "./bookings";
import moment from "moment-timezone";
import branch from 'react-native-branch'
import {errorLog, successLog} from "../utils/fireLog";

export const postShareRoom = (object, roomUrl) => {
    return (dispatch) => {
        dispatch({type: GET_SLOTS, payload: []});
        dispatch(show());
        http.post(`api/walking/appointments`, object, dispatch)
            .then((res) => {
                successLog("postShareRoom", res.status)
                dispatch({type: POST_SHARE_ROOM, payload: res.data});
                dispatch(getUpcomingBookingsByDate(moment(new Date()).format("YYYY-MM-DD")));
                dispatch(hide());
                if (res.status == 200) {
                    const payload = {
                        text: res.message,
                        subtext: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(showModal(payload))
                    if(res.data !== undefined && res.data !== null){
                        const _data = res.data;
                        crateLink(dispatch, _data._id, roomUrl, 'PATIENT')
                    }
                }
            })
            .catch((o) => {
                errorLog("postShareRoom api/walking/appointments", o)
                dispatch(hide());
            });
    };
};


const crateLink = async (dispatch, id , roomUrl, user) => {
    console.log(url + "in shareAppointmentUrl " + id , roomUrl, user)
    let branchUniversalObject = await branch.createBranchUniversalObject('canonicalIdentifier', {
        locallyIndex: true,
        title: 'Pathway healthcare',
        contentDescription: '',
        contentMetadata: {
            ratingAverage: 4.2,
            customMetadata: {
                appointmentId: id,
                room: roomUrl,
                token: 'ABCD',
                user: user
            }
        }
    })

    let linkProperties = {feature: 'share', channel: 'facebook'}
    let controlParams = {}

    let {url} = await branchUniversalObject.generateShortUrl(linkProperties, controlParams);
    shareAppointmentUrl(dispatch, url)
}


export const shareAppointmentUrl = (dispatch, url) => {
    console.log(url + "in shareAppointmentUrl")
    Clipboard.setString(url)
    return (dispatch) => {
        dispatch(show());
        http.post(` `, {
            directUrl: url,
        }, dispatch)
            .then((res) => {
                if (res.data !== undefined && res.data !== null) {

                }
                dispatch(hide());
            })
            .catch(_ => {
                dispatch(hide());
            });
    };
};





export const emptySlot = () => {
    return {
        type: GET_SLOTS,
        payload: [],
    };
};


export const getShareRoomSlots = (doctorId, date) => {
    return (dispatch) => {
        dispatch({type: GET_SLOTS, payload: []});
        dispatch(show());
        http.post(`api/doctors/slots`, {
            doctorId: doctorId,
            date: date,
        }, dispatch)
            .then((res) => {
                successLog("getShareRoomSlots", res.status)
                if (res.data !== undefined && res.data !== null) {
                    let _response = res.data;
                    let result = _response.filter(item => {
                        return item.status != "INACTIVE" && item.isTimeout != true
                    })
                    dispatch({type: GET_SLOTS, payload: result});
                }
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("getShareRoomSlots", o)
                dispatch(hide());
            });
    };
};


export const getRoomURLForGuest = (appointmentId, channelName) => {
    return (dispatch) => {
        dispatch(show());
        http.post(`api/shareUrl`, {
            appointmentId: appointmentId,
            userType: "GUEST"
        }, dispatch)
            .then((res) => {
                successLog("getRoomURLForGuest", res.status)
                dispatch({type: GET_GUEST_URL, payload: res.data});
                if (res.data !== undefined && res.data !== null)
                    Clipboard.setString(res.data)
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("getRoomURLForGuest", o)
                dispatch(hide());
            });
    };
};