import http from "../services/http";
import { UPCOMING_BOOKINGS } from "../utils/constant";
import moment from "moment";
import { showModal } from "./modal";
import database from "@react-native-firebase/database";
import { hide, show } from "../utils/loader/action";
import { setCallHeader, setSelectedAppointment, updateAppointmentPatient } from "./bookings";
import { addUserLogToFirebase, checkUserProfile } from "../utils/helper";
import { errorLog, methodLog, recordError, successLog } from "../utils/fireLog";
import { setSelectedPatient } from "../scenes/doctor/perscription-form/components/action";
import { ifValid, isValid } from "../utils/ifNotValid";
import NavigationService from '../components/startScreen/RootNavigation'
import { setPrescriptionData } from "./presModel";

export const getUpcomingBookings = (navigation, userProfile) => {
    let date = moment().format("YYYY-MM-DD")
    return (dispatch) => {
        http.get(`api/upcoming/appointments?date=${date}`, dispatch)
            .then((res) => {
                successLog("getRoomURLForGuest", res.status)
                const response = res.data;
                if (response !== undefined && response !== null && response.length > 0) {
                    dispatch({ type: UPCOMING_BOOKINGS, payload: res.data });
                    let format = "hh:mm:ss"
                    const data = response.filter(o => {
                        let slotTime = moment(o.startTime, 'hh:mm:ss')
                        if (o.startTime !== undefined && o.startTime !== null) {
                            let currentTime = moment()
                            let afterTime = moment().add(15, 'minutes');
                            if (slotTime.isBetween(currentTime, afterTime)) {
                                console.log('is between')
                                return o;
                            } else {
                                console.log('is not between')
                            }
                        }
                    })
                    if (data === undefined || data === null)
                        return;
                    console.log(data.length)
                    console.log(JSON.stringify(data))
                    if (data.length === 0) {
                        showMsg(dispatch, "No available booking found in coming 15 minutes..!")
                        navigation.navigate('UpcomingBookings')
                    } else if (data.length === 1) {
                        sendUserToAppointment(data[0], dispatch, navigation, userProfile)
                    } else if (data.length > 1) {
                        sendUserToAppointment(data[0], dispatch, navigation, userProfile)
                    }
                } else {
                    showMsg(dispatch, "No booked appointments found")
                }
            })
            .catch((o) => {
                errorLog("getUpcomingBookings", o)
            });
    };
};


const sendUserToAppointment = (object, dispatch, navigation, userProfile) => {
    console.log(object.startTime)
    if (moment().format("YYYY-MM-DD") !== moment(object.appointmentDate).format("YYYY-MM-DD")) {
        showMsg(dispatch, "No available booking found in coming 15 minutes..!")
        return;
    }

    let id = object._id;
    if (id === undefined || id === null)
        return;

    let doctor = object.doctorId;
    if (doctor === undefined || doctor === null)
        return;

    let roomName = object.doctorId.roomName;
    if (roomName === undefined || roomName === null)
        return;


    if (userProfile === undefined || userProfile === null) {
        console.log("userProfile empty ")
        return;
    }

    if (navigation === undefined || navigation === null) {
        console.log("navigation empty ")
        return;
    }


    dispatch(show())
    dispatch(setSelectedAppointment(id))
    dispatch(setCallHeader(`${doctor.fullName}`))

    let obj = { [id]: { status: "WAITING" } };
    //// put user in waiting list.
    database()
        .ref("/doctor")
        .update(obj)
        .then(_ => {
            console.log("status added")
            //// put user details in list.
            addUserInList(id, userProfile).then((_) => {
                console.log("added in list..")
                dispatch(hide())
                dispatch(updateAppointmentPatient(id, navigation, "WAITING", roomName, doctor))
            });
        });

}

const addUserInList = async (id, userProfile) => {
    let { fullName, image } = checkUserProfile(userProfile);
    console.log(fullName + image);
    await database()
        .ref(`/users/${id}`)
        .update({ patient: { fullName: fullName, image: image } })
        .then((obj) => {

        });
};


const showMsg = (dispatch, msg) => {
    const payload = {
        text: msg,
        iconName: "closecircleo",
        modalVisible: true,
        subText: "",
    };
    dispatch(showModal(payload));
}

export const acceptCallAction = (item, status, userProfileData, cb) => {
    return dispatch => {
        dispatch(setSelectedPatient(item))
        let pfullName = ifValid(item?.patientId?.fullName) ? item?.patientId?.fullName : "-";

        const presdata = {
            ...item,
            doctorId: {
                fullName: userProfileData?.fullName,
                image: userProfileData?.image,
            }

        }
        dispatch(setPrescriptionData(presdata, item?._id))

        try {
            dispatch(setCallHeader(pfullName))
            dispatch(setSelectedAppointment(item?._id))
            let obj = { [item?._id]: { status: status } };
            const newReference = database()
                .ref("/doctor")
                .update(obj)
                .then(async (obj) => {
                    if (status == "APPROVED") {
                        //adding user
                        let fullName = isValid(userProfileData?.fullName) ? userProfileData?.fullName : '-'
                        let image = isValid(userProfileData?.image) ? userProfileData?.image : '-'

                        await addUserLogToFirebase(item?._id, { event: status, name: `${fullName} (Doctor)` }).then(r => {
                        })

                        /// adding first user in list.
                        const users = database()
                            .ref(`/users/${[item?._id]}`)
                            .update({ doctor: { fullName: `${fullName}`, image: image } })
                            .then((obj) => {
                                cb()
                            });
                    } else {
                        cb()
                    }
                });
        } catch (e) {
            cb()
            console.log(e)
        }
    }
}