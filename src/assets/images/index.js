const Images = {
	bookinDr: require("./booking-dr.png"),
	doctor1: require("./dashboard-doctor1.png"),
	doctor2: require("./dashboard-doctor2.png"),
	doctor3: require("./dashboard-doctor3.png"),
	doctor6: require("./doctor6.png"),
	doctor7: require("./doctor7.png"),
	doctor8: require("./doctor8.png"),
	doctor9: require("./doctor9.png"),
	user: require("./user.png"),
	doctor4: require("./dashboard-doctor4.png"),
	ackDoctor: require("./acknowledgment-doctor.png"),
	icon_plus: require("./icon_plus.png"),
	icon_calender: require("./calendar.png"),
	icon_doctor: require("./icon_doctor.png"),
	icon_help: require("./icon_helpCenter.png"),
	icon_setting: require("./icon_setting.png"),
	icon_down: require("./icon_down.png"),
	userProfile: require("./userProfile.png"),
	patient1: require("./patient-1.png"),
	patient2: require("./patient-2.png"),
	patient3: require("./patient-3.png"),
	patient4: require("./patient-4.png"),
	mic_off_icon: require("./mic_off.png"),
	call_end: require("./call_end.png"),
	video_off_icon: require("./video_off.png"),
	people_icon: require("./people_2.png"),
	mic_disabled: require("./mic_disabled.png"),
	video_disabled: require("./video_diabled.png"),
	network: require("./network.png"),
	three_dots: require("./three_dots.png"),
	network_no_circle: require("./networkFull.png"),
	three_dots_no_circle: require("./threeDots.png"),
	profile_male: require("./profile_male.png"),
	camera: require("./camera.png"),
	no_bookings: require("./no-bookings.png"),
	userImage: require("./userImage.png"),
	phoneOff: require("./phoneOff.png"),
	call: require("./call.png"),
	logout: require("./logout.png"),
	notFound: require("./nofound.png"),
	start_consult: require("./home_banner.png"),
	coupen_code: require("./coupen_code.png"),
	bell_icon: require("./bell_icon.png"),
	icon_coupen: require("./coupen.png"),
	icon_clock: require("./clock.png"),
	icon_arrow_right: require("./arrow-right.png"),
	left_arrow: require("./left_icon.png"),
	rightArrow: require("./right_icon.png"),
	loginHalf: require("./loginHalf.png"),
	google: require("./google.png"),
	ayurveda: require("./ayurveda.png"),
	hat: require("./hat.png"),
	policy: require("./policy.png"),
	mapPin: require("./mapPin.png"),
	thumb: require("./thumb.png"),
	checkCircle: require("./checkCircle.png"),
	left_arrow_enabled: require("./left_arrow_enabled.png"),
	right_arrow_enabled: require("./right_arrow_enabled.png"),
	edit: require("./edit.png"),
	calendar: require("./calendar.png"),
	plusWhite: require("./plusWhite.png"),
	pin: require("./pin.png"),
	phone: require("./phone.png"),
	pinRed: require("./pinRed.png"),
	plusRed: require("./plusRed.png"),
	watchRed: require("./watchRed.png"),
	userRed: require("./userRed.png"),
	dollarRed: require("./dollerRed.png"),
	starWhite: require("./starWhite.png"),
	usersWhite: require("./usersWhite.png"),
	rewardWhite: require("./rewardWhite.png"),
	calenderBlue: require("./calendarBlue.png"),
	dropDown: require("./dropDown.png"),
	login_banner: require("./login_banner.png"),
	login_banner2: require("./login_banner2.png"),
	calendarGray: require("./calender.png"),
	splash: require("./splash.png"),
	doc_banner: require("./Banner_1.jpg"),
	facebook: require("./facebook.png"),
	instagram: require("./instagram.png"),
	twitter: require("./twitter.png"),
	logo_sehet: require("./sehet_logo_new.png"),
	logo_sehet_new: require("./logo_sehet_new.png"),
	about: require("./about_us.png"),
	uploadImage: require("./uploadPlacehoder.jpg"),
	filter: require("./filter_icon.png"),
	male_doctor: require("./male_doctor.png"),
	female_doctor: require("./female_doctor.png"),
	video_splash: require("./sehet_splash_screen.mp4"),
	g1: require("./g1.png"),
	g2: require("./g2.png"),
	g3: require("./g3.png"),
	back_screen: require("./back_screen.png"),
	background_theme: require("./background_theme.png"),
	sehet_logo_login: require("./sehet_logo_2.png"),
	header_logo: require("./header_logo.png"),
	pencil_edit: require("./pencil_edit.png"),
	calendar_2: require("./calendar_2.png"),
	pill_capsule: require("./pill_capsule.png"),
	stethoscope: require("./stethoscope.png"),
	video_calling_app: require("./video-calling-app.png"),
	approved_doctors: require("./approved_doctors.png"),
	FAQ: require("./FAQ.png"),
	BLOG: require("./BLOG.png"),
	covid_logo: require("./covid.png"),
	share_icon: require("./share_icon.png"),
	fight_corona: require("./fight_corona.png"),
	incoming_bg: require("./incoming_bg.png"),
	end_call: require("./end_call.png"),
	video_message: require("./message.png"),
	video_call: require("./video_call.png"),
	sehet_card: require("./sehet_card.png"),
	sehet_card_bg: require("./sehet_card_bg.png"),
	appIcon: require("./appIcon.png"),
	about_us_banner: require("./about_us_banner.png"),
	my_health_card_icon: require("./my_health_card_icon.png"),
	bellicon: require("./bellicon.png"),
	chatIcon: require("./chatIcon.png"),
	timeIcon: require("./time_icon.png"),
	calendar_icon: require("./calendar_icon.png"),
	speech_mic: require("./speech_mic.png"),
	whatsapp: require("./whatsapp.png"),
	lab_test: require("./lab_test.png"),
	order_medicine: require("./order_medicine.png"),
	upload_doc_icon: require("./upload_doc_icon.png"),
	amar_health_card: require("./amar_health_card.png"),
	search_by_hospitals: require("./search_by_hospitals.png"),
	home_logo: require("./home_logo.png"),
	amar_logo: require("./amar_logo.png"),

	AboutUsRed: require("./AboutUsRed.png"),
	BlogsRed: require("./BlogsRed.png"),
	facebookred: require("./facebookred.png"),
	FAQRed: require("./FAQRed.png"),
	instargamred: require("./instargamred.png"),
	LogoutRed: require("./LogoutRed.png"),
	MyAppointments: require("./MyAppointments.png"),
	twitterred: require("./twitterred.png"),
	ShareRed: require("./ShareRed.png"),
	MyHealthcard: require("./MyHealthcard.png"),
	NotificationRed: require("./NotificationRed.png"),
	whatsappred: require("./whatsappred.png"),
	home_logo2: require("./home_logo2.png"),
	Call_Ambulance: require("./Call_Ambulance.png"),
	nearby_doc_banner: require("./nearby_doc_banner.png"),
	powered_by: require("./powered_by.png"),
	lab_request: require("./lab_request.png"),
	order_request: require("./order_request.png"),
	chat_new: require("./chat_new.png"),
};

export default Images;
