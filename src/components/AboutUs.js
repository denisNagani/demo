import React, { useEffect, useState } from 'react'
import { Image, Linking, ScrollView, StyleSheet, Text, View } from 'react-native'
import HeaderSehet from './header-common'
import Images from '../assets/images'
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_SF_PRO_SemiBold } from "../styles/typography";
import { strings } from "../utils/translation";
import { getContactMobileNumbers } from '../services';
import { callNumber } from '../utils/helpline-no';
import { supportEmailId } from '../config';

const AboutUs = (props) => {

    const [number, setNumber] = useState("")
    const [contactNo, setContactnno] = useState([])

    useEffect(() => {
        getContact()
    }, [])

    const getContact = async () => {
        try {
            const number = await getContactMobileNumbers()
            setContactnno(number)
        } catch (error) {
            setContactnno(["08047166558"])
        }
    }

    return (
        <View style={{ flex: 1, }}>
            <HeaderSehet headerText={strings.about_us} navigation={props.navigation} changeTheme={true} />
            <ScrollView>
                <View style={{ flex: 1 }}>

                    <Image
                        source={Images.about_us_banner}
                        style={{ height: 250, width: '100%' }}
                        resizeMode="cover"
                    />

                    <View style={styles.cardContainer}>
                        <View style={styles.card}>
                            <Image
                                source={Images.amar_logo}
                                style={{ height: 90, width: 90 }}
                                resizeMode="contain"
                            />
                            <View style={{ marginHorizontal: 5, marginVertical: 5 }}>
                                <Text style={styles.textStyle}>{strings.aboutText1} {strings.aboutText2} {strings.aboutText3}</Text>
                            </View>
                        </View>
                    </View>

                    <View style={{
                        marginHorizontal: 40,
                        marginVertical: 10
                    }}>
                        <Text style={{
                            fontSize: 20,
                            fontFamily: FONT_FAMILY_SF_PRO_SemiBold
                        }}>{strings.help_center}</Text>

                        <Text style={{
                            fontSize: 16,
                            textAlign: 'justify',
                            fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
                            marginVertical: 10,
                            lineHeight: 20
                        }}
                        >
                            {strings.we_at_sehet}
                            <Text
                                style={{ color: 'blue', textDecorationLine: 'underline' }}
                                onPress={() => Linking.openURL(`mailto:${supportEmailId}`)}
                            > {supportEmailId}</Text> {strings.or_call_us} {
                                contactNo.map(item => (
                                    <Text
                                        style={{
                                            color: 'blue',
                                            textDecorationLine: 'underline'
                                        }}
                                        onPress={() => {
                                            callNumber(`${item}`)
                                        }}
                                    >{item} </Text>
                                ))
                            } {strings.our_customer}
                        </Text>

                    </View>

                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        paddingVertical: 10,
        paddingHorizontal: 25,
        marginVertical: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: 'red',
        // shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 5
    },
    cardContainer: {
        marginHorizontal: 30,
        marginTop: -50
    },
    textStyle: {
        fontSize: 16,
    }
})

export default AboutUs
