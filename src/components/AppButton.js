import React from 'react'
import { Text, TouchableOpacity, StyleSheet, ActivityIndicator } from 'react-native'

const AppButton = ({ title, onPress, style, textStyle, loading, successText, loaderColor, disable }) => {
    let newStyle = disable == true ? { backgroundColor: '#A4A9C2' } : { backgroundColor: '#FF5E38' }
    return (
        <TouchableOpacity
            style={[styles.btnStyle, newStyle, style]}
            onPress={onPress}
            disabled={disable}
        >
            {loading
                ? <ActivityIndicator color={loaderColor ? loaderColor : '#FF5E38'} />
                : <Text style={[styles.txtStyle, textStyle]}>{
                    loading == false
                        ? successText
                        : title
                }</Text>
            }
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    btnStyle: {
        backgroundColor: '#FF5E38',
        borderRadius: 5,
        padding: 10
    },
    txtStyle: {
        textAlign: 'center',
        fontSize: 16,
        color: '#FFF',
    }
})

export default AppButton