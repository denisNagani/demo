import React, { useRef, useState, useEffect } from "react";
import { AppState, StyleSheet, Text, View } from "react-native";
import { useDispatch } from "react-redux";
import { UpdateChatStatus } from "../actions/chat";
import moment from 'moment'

const AppStateExample = () => {
    const appState = useRef(AppState.currentState);
    const [appStateVisible, setAppStateVisible] = useState(appState.current);

    const dispatch = useDispatch()
    useEffect(() => {
        AppState.addEventListener("change", _handleAppStateChange);

        return () => {
            AppState.removeEventListener("change", _handleAppStateChange);
        };
    }, []);

    const _handleAppStateChange = (nextAppState) => {
        if (appState.current.match(/inactive|background/) && nextAppState === "active") {
            console.log("App has come to the foreground!");
        }

        appState.current = nextAppState;
        setAppStateVisible(appState.current);
        console.log("AppState", appState.current);
        if (appState.current === "active") {
            //online
            const payload = {
                chatStatus: "Online"
            }
            // dispatch(UpdateChatStatus(payload))
        } else {
            //last seen
            const date = moment.utc(new Date())
            const payload = {
                chatStatus: date
            }
            console.log(payload);
            // dispatch(UpdateChatStatus(payload))

            // to convert to last seen
            // const date = moment.utc("2021-05-05T07:43:27.556Z").local().startOf('seconds').fromNow()
        }
    };

    // return (
    //     <View style={styles.container}>
    //         <Text>Current state is: {appStateVisible}</Text>
    //     </View>
    // );
    return null
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
});

export default AppStateExample;