import React, { useState, useEffect } from 'react'
import { TextInput, Text, ScrollView, Image, KeyboardAvoidingView, View } from 'react-native'
import { Button } from "native-base";
import { useDispatch, useSelector } from 'react-redux'
import { CheckCoupon, CheckMemberShipCard } from '../actions/coupn'
import Images from '../assets/images'
import Modal, { ModalContent } from "react-native-modals";
import { showModal } from '../actions/modal'
import { errorPayload } from '../utils/helper'
import { FONT_FAMILY_SF_PRO_BOLD } from '../styles/typography'
import DeviceInfo from "react-native-device-info";
import Loader from "../utils/loader";
import { setApplyCouponModal } from '../actions/sehet/speciality-action';
import { strings } from '../utils/translation';
import firestore from '@react-native-firebase/firestore'
import { isValid } from '../utils/ifNotValid';

const ApplyCouponModal = ({ visible }) => {
    const dispatch = useDispatch()
    const user = useSelector(state => state.userReducer.userProfile.userId)
    const selected_slot = useSelector(state => state.appointment.selectedSlotObj)
    const get_price = useSelector(state => state.specialitiesReducer.selectedSpeciality)
    const membershipId = useSelector(state => state.appointment.membershipId)
    const [couponCode, setCouponCode] = useState('')
    const [CouponImage, setCouponImage] = useState(null)

    useEffect(() => {
        getCouponImage()
    }, [])

    useEffect(() => {
        if (visible == false) {
            setCouponCode('')
        }
        if (isValid(membershipId)) {
            setCouponCode(membershipId)
        }
    }, [visible, membershipId])

    const getCouponImage = async () => {
        try {
            const data = await firestore().collection('support').doc('couponImage').get()
            const { imageUrl } = data.data()
            setCouponImage(imageUrl)
        } catch (error) {
            console.log(error);
        }
    }


    const handleCoupon = () => {
        if (couponCode == null || couponCode == '' || couponCode == undefined) {
            dispatch(showModal(errorPayload('Enter membership Code')))
        } else {
            // if (selected_slot?.callType == "Offline") {
            //     dispatch(setApplyCouponModal(false))
            //     dispatch(showModal(errorPayload('Memmbership is not valid for clinic visit')))
            // } else {
            const slot_id = selected_slot.slotId
            const user_id = user._id
            const og_price = get_price.m_price
            const deviceId = DeviceInfo.getUniqueId()
            // dispatch(CheckCoupon(couponCode, user_id, slot_id, og_price, deviceId))
            const cName = couponCode?.trim()
            console.log(couponCode, cName);
            const doctorId = selected_slot.doctorId
            dispatch(CheckMemberShipCard(cName, user_id, slot_id, og_price, deviceId, doctorId))
            // }
        }
    }

    return (
        <Modal
            width={0.9}
            style={{ flex: 1 }}
            visible={visible}
            onTouchOutside={() => {
                dispatch(setApplyCouponModal(false))
            }}
            rounded
            actionsBordered
            onSwipeOut={() => {
                dispatch(setApplyCouponModal(false))
            }}
            onHardwareBackPress={() => {
                dispatch(setApplyCouponModal(false))
                return true
            }}
        >
            <ModalContent style={{ backgroundColor: '#fff', }}>
                <View>

                    <Image
                        resizeMode="stretch"
                        style={{ height: 200, width: '100%' }}
                        source={{ uri: CouponImage }}
                    // defaultSource={Images.fight_corona}
                    />

                    <Text style={{ marginHorizontal: 10, marginTop: 20, color: '#041A32', fontSize: 16, fontFamily: FONT_FAMILY_SF_PRO_BOLD }}>{strings.coupon_code} *</Text>
                    <TextInput
                        value={couponCode}
                        placeholder={strings.enter_coupon_code}
                        placeholderTextColor="#484F5F"
                        style={{
                            margin: 10,
                            borderRadius: 8,
                            height: 50,
                            borderColor: '#E5ECF9',
                            borderWidth: 1,
                            paddingLeft: 10
                        }}
                        autoCorrect={false}
                        autoCapitalize="none"
                        onChangeText={text => setCouponCode(text)}
                    />

                    {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: '10%' }}> */}
                    <Button danger style={[{
                        marginHorizontal: 10,
                        alignSelf: 'center',
                        marginVertical: 25,
                        backgroundColor: '#FF5E38'
                    }]}
                        onPress={handleCoupon}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Text uppercase={false}
                                style={[{ fontSize: 16, fontWeight: 'bold', textAlign: 'center', color: 'white', flex: 1, paddingHorizontal: 15 }]}>{strings.apply_code} </Text>

                        </View>
                    </Button>
                    {/* </View> */}

                    {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: '10%' }}> */}
                    <Button style={[{
                        marginHorizontal: 10,
                        alignSelf: 'center',
                        backgroundColor: '#D5D5D5'
                    }]}
                        onPress={() => {
                            dispatch(setApplyCouponModal(false))
                            // dispatch(CoupnApplied(null))
                            // dispatch(UpdatePrice(get_price.m_price))
                            // props.navigation.navigate('BookingSummaryScreen', {
                            //     dis_value: 0
                            // })
                        }}>

                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Text uppercase={false}
                                style={[{ fontSize: 16, fontWeight: 'bold', textAlign: 'center', color: 'white', flex: 1, paddingHorizontal: 15 }]}>{strings.close}</Text>

                        </View>
                    </Button>
                    {/* </View> */}

                </View>
            </ModalContent>
        </Modal>
    )
}

export default ApplyCouponModal
