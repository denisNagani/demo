import {Text, View} from "react-native";
import styles from "../styles/common";
import {Thumbnail} from "native-base";
import Images from "../assets/images";
import Line from "./line";
import React from "react";
import {ifValid} from "../utils/ifNotValid";
import {successLog} from "../utils/fireLog";

export const AttendeesItem = ({index, usersListFirebase, item}) => {
    successLog(JSON.stringify(item))
    return <>
        <View style={styles.videoListItemContainer}>
            {
                item.image === "" ?
                    <Thumbnail
                        source={Images.profile_male}
                        style={styles.videoList}
                    /> : <Thumbnail
                        source={{uri: item.image}}
                        style={styles.videoList}
                    />
            }
            <View style={{marginLeft: 15, flex: 1, justifyContent:'center'}}>
                <Text style={styles.drawerProfileName}>{ifValid(item.fullName)
                && item.fullName === '' ? '-' : item.fullName}</Text>
                {/*<View style={styles.videoUserButtons}>*/}
                {/*<TouchableOpacity>*/}
                {/*    <Image source={Images.mic_disabled} style={styles.videoUsersButtons}/>*/}
                {/*</TouchableOpacity>*/}
                {/*<TouchableOpacity>*/}
                {/*    <Image source={Images.video_disabled} style={styles.videoUsersButtons}/>*/}
                {/*</TouchableOpacity>*/}
                {/*<TouchableOpacity>*/}
                {/*    <Image source={Images.network} style={styles.videoUsersButtons}/>*/}
                {/*</TouchableOpacity>*/}
                {/*</View>*/}
            </View>
            <View>
                {/*<TouchableOpacity>*/}
                {/*    <Image source={Images.three_dots} style={styles.videoUsersButtons}/>*/}
                {/*</TouchableOpacity>*/}
            </View>
        </View>
        <Line style={{marginTop: 15, opacity: 0.13}}/>
    </>
}

