import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { ifNotValid } from '../utils/ifNotValid';
import Images from '../assets/images';
import { FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_BOLD, FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_SemiBold } from '../styles/typography';
import { useSelector } from 'react-redux';
import moment from 'moment'
import { Icon } from 'native-base';

const ChatItem = ({ item, navigation }) => {

    let user = useSelector(state => state.userReducer.userProfile.userId)
    let role = user.role

    let imageUrl = item?.doctorId?.imageUrl;
    if (ifNotValid(imageUrl)) imageUrl = Images.user
    else imageUrl = { uri: imageUrl }

    let _name = role === 'DOCTOR' ? item?.patientId?.fullName : item?.doctorId?.fullName;
    let _status = role === 'DOCTOR' ? item?.patientId?.chatStatus : item?.doctorId?.chatStatus;

    // to convert to last seen
    const convertToLastSeen = (dt) => {
        const IsoDateTo = dt;
        const date = moment(IsoDateTo, "YYYY-MM-DD[T]HH:mm:ss").fromNow()
        return date
    }

    let currentStatus = _status == "Online" || _status == "Offline" ? _status : convertToLastSeen(_status)

    return (
        <View style={{
            marginHorizontal: 5
        }}>
            <TouchableOpacity style={{
                paddingHorizontal: 8,
                paddingVertical: 8,
                margin: 10,
                flex: 1,
                flexDirection: 'row',
                alignItems: 'center'
            }} onPress={() => {
                role === "DOCTOR"
                    ? navigation.navigate('DoctorChatScreen', { data: item, status: currentStatus })
                    : navigation.navigate('PatientChatScreen', { data: item, status: currentStatus })
            }}>
                <Image
                    source={imageUrl}
                    style={{
                        height: 50,
                        width: 50,
                        borderRadius: 50
                    }}
                />
                <View style={{
                }}>
                    <Text style={{
                        marginLeft: 10,
                        fontSize: 16,
                        fontFamily: FONT_FAMILY_SF_PRO_SemiBold
                    }}>{_name}</Text>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            marginLeft: 8,
                            fontSize: 10,
                            marginVertical: 3,
                            color: currentStatus === "Online"
                                ? 'green'
                                : currentStatus === "Offline"
                                    ? 'red'
                                    : 'grey'
                        }}>{'\u2B24'}</Text>

                        <Text style={{
                            marginLeft: 3,
                            fontSize: 14,
                        }}>{currentStatus}</Text>
                    </View>
                </View>
            </TouchableOpacity>
            <Text style={{
                borderColor: 'lightgrey',
                borderWidth: 1,
                height: .5
            }}></Text>
        </View>
    )
}

export default ChatItem
