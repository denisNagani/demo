import React, { useEffect, useState } from 'react'
import { View, Text, Image, Modal, SafeAreaView, ActivityIndicator } from 'react-native'
import Images from '../../../assets/images'
import { Icon } from 'native-base'
import { useDispatch } from 'react-redux'
import { hideMediaModal } from '../../../actions/modal'
import Video from 'react-native-video'
import { WebView } from 'react-native-webview'


const MediaPreview = ({ type, url, visible }) => {

    const dispatch = useDispatch()
    const [loading, setLoading] = useState(true)

    const renderVideo = () => (
        <>
            {loading && <ActivityIndicator
                style={{
                    position: 'absolute',
                    top: '45%',
                    left: '45%'
                }}
                size="large"
                color="white"
            />}
            <Video source={{ uri: url }}
                // ref={(ref) => {
                //     this.player = ref
                // }}
                style={{
                    height: '40%',
                    width: '100%'
                }}
                resizeMode={"cover"}
                repeat={false}
                paused={false}
                muted={false}
                playInBackground={false}
                playWhenInactive={false}
                onLoad={() => setLoading(false)}
            />
        </>
    )

    const renderDocument = () => {
        return (
            <WebView
                ignoreSslError={true}
                scalesPageToFit={true}
                useWebKit
                allowUniversalAccessFromFileURLs
                mixedContentMode="always"
                originWhitelist={['*']}
                javaScriptEnabled={true}
                domStorageEnabled={true}
                source={{ uri: `https://docs.google.com/gview?embedded=true&url=${url}` }}
            // source={{ uri: `https://docs.google.com/gview?embedded=true&url=https://firebasestorage.googleapis.com/v0/b/sehet-dev.appspot.com/o/chats%2FSehet%20Covid%20fight%20requirnments.docx?alt=media&token=a2f9e2a5-3974-412e-81b1-2b0d027d4adb` }}
            />
        )
    }


    return (
        <Modal
            visible={visible}
            animationType="slide"
            onRequestClose={() => dispatch(hideMediaModal())}
        >
            <SafeAreaView style={{
                flex: 1,
                backgroundColor: 'black',
            }}>
                <>
                    <Icon
                        name="close"
                        type="AntDesign"
                        style={{
                            color: 'white',
                            padding: 8,
                            fontSize: 40
                        }}
                        onPress={() => dispatch(hideMediaModal())}
                    />
                    <View style={{
                        flex: 1,
                        justifyContent: 'center'
                    }}>
                        {
                            type === "image"
                                ? <Image
                                    style={{
                                        height: '100%',
                                        width: '100%',
                                    }}
                                    source={{ uri: url }}
                                    resizeMode="contain"
                                />
                                : type === "video"
                                    ? renderVideo()
                                    // : renderDocument()
                                    : null
                        }
                    </View>
                </>
            </SafeAreaView>
        </Modal>
    )
}

export default MediaPreview
