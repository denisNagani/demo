import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import RNFS from 'react-native-fs';
import FileViewer from 'react-native-file-viewer';
import { useDispatch } from 'react-redux';
import { clearDoceUrl } from '../../../actions/chat';

const RenderDocument = ({ url, visible }) => {

    const dispatch = useDispatch()

    useEffect(() => {
        if (url !== null) {
            LoadDocument()
        }
    }, [url])

    const LoadDocument = () => {
        const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.pdf`;

        const options = {
            fromUrl: url,
            toFile: localFile
        };
        RNFS.downloadFile(options).promise
            .then(() => FileViewer.open(localFile))
            .then(() => {
                // success
                dispatch(clearDoceUrl())
            })
            .catch(error => {
                console.log("LoadDocument " + error);
                // error
                dispatch(clearDoceUrl())
            });
    }

    return (
        <View>
            <Text></Text>
        </View>
    )
}

export default RenderDocument