import React, { useEffect, useState } from 'react'
import { View, Text, Platform, Image } from 'react-native'

import SoundPlayer from 'react-native-sound-player'
import { Icon } from 'native-base'
import moment from 'moment'
import { formatDuration } from '../../../utils/core'

import Slider from 'react-native-slider';
import Images from '../../../assets/images'
import { useDispatch, useSelector } from 'react-redux'
import { setFileUrl } from '../../../actions/chat'

const SoundPlayerComp = ({ url, type }) => {
    const dispatch = useDispatch()
    const fileUrl = useSelector(state => state.chat.url)


    // let _onFinishedPlayingSubscription = null
    // let _onFinishedLoadingSubscription = null
    // let _onFinishedLoadingFileSubscription = null
    // let _onFinishedLoadingURLSubscription = null

    const [play, setPlay] = useState(true)
    const [audioPlaying, setAudioPlaying] = useState(false)
    const [duration, setDuration] = useState(0)
    const [durationSlider, setDurationSlider] = useState(0)

    const [sliderValue, setSliderValue] = useState(0);
    const [loadingFile, setLoadingFile] = useState(null)

    const [soundTime, setSoundTime] = useState(0)

    useEffect(() => {
        const _onFinishedPlayingSubscription = SoundPlayer.addEventListener('FinishedPlaying', ({ success }) => {
            console.log('finished playing', success)
            setPlay(true)
            setSliderValue(0)
            setDurationSlider(0)
            setAudioPlaying(false)
        })
        const _onFinishedLoadingSubscription = SoundPlayer.addEventListener('FinishedLoading', ({ success }) => {
            console.log('finished loading', success)
        })
        const _onFinishedLoadingFileSubscription = SoundPlayer.addEventListener('FinishedLoadingFile', ({ success, name, type }) => {
            console.log('finished loading file', success, name, type)
        })
        const _onFinishedLoadingURLSubscription = SoundPlayer.addEventListener('FinishedLoadingURL', ({ success, url }) => {
            console.log('finished loading url', success, url)
            getSoundTime()
        })

        return () => {
            SoundPlayer.stop()
            _onFinishedPlayingSubscription.remove()
            _onFinishedLoadingSubscription.remove()
            _onFinishedLoadingURLSubscription.remove()
            _onFinishedLoadingFileSubscription.remove()
        }
    }, [])

    const getSoundTime = async () => {
        setInterval(async () => {
            const data = await SoundPlayer.getInfo()
            const currentplaingTime = data?.currentTime
            setSoundTime(currentplaingTime)
        }, 1000);
    }

    const loadAudioFromUrl = () => {
        try {
            // // play the file tone.mp3
            // SoundPlayer.playSoundFile('tone', 'mp3')
            // or play from url
            SoundPlayer.loadUrl(url)
        } catch (e) {
            console.log(`cannot play the sound file`, e)
        }
    }

    const playAudioFromUrl = async () => {
        // cleaner()
        try {
            if (audioPlaying) {
                SoundPlayer.resume()
                setPlay(false)
            } else {
                setPlay(!play)
                setLoadingFile(false)
                await SoundPlayer.playUrl(url)
                await getInfo()
                setAudioPlaying(true)
                setLoadingFile(true)
                dispatch(setFileUrl(url, "audio"))
            }


        } catch (e) {
            console.log(`cannot play the sound file`, e)
        }
    }

    const cleaner = () => {
        SoundPlayer.stop()
        setPlay(true)
    }

    const getInfo = async () => { // You need the keyword `async`
        try {
            const info = await SoundPlayer.getInfo() // Also, you need to await this because it is async
            console.log('getInfo', info) // {duration: 12.416, currentTime: 7.691}
            const durations = info?.duration
            const ftime = formatDuration(durations)
            setDurationSlider(durations)
            setDuration(ftime)
        } catch (e) {
            console.log('There is no song playing', e)
        }
    }
    const pauseAudio = () => {
        setLoadingFile(true)
        SoundPlayer.pause()
        setPlay(!play)
    }
    const seekTo = (num) => {
        SoundPlayer.seek(num)
    }
    const onPregress = (e) => {
        console.log("onProgress " + e);
    }

    const checkUrl = () => {
        return url === fileUrl
    }

    return (
        <View style={{}}>
            <View style={{
                // flex: 1,
                flexDirection: 'row',
                alignItems: 'center',
            }}>
                {
                    play ?
                        <Icon
                            name="play"
                            type="AntDesign"
                            onPress={playAudioFromUrl}
                            style={{ marginHorizontal: 5, fontSize: 34 }}
                        />
                        : <Icon
                            name="pausecircle"
                            type="AntDesign"
                            onPress={pauseAudio}
                            style={{ marginHorizontal: 5, fontSize: 34 }}
                        />
                }
                {
                    checkUrl() && loadingFile === true && (
                        <Slider
                            maximumValue={durationSlider}
                            minimumValue={0}
                            minimumTrackTintColor="#307ecc"
                            maximumTrackTintColor="#000000"
                            // step={1}
                            style={{
                                width: 100,
                                marginHorizontal: 5
                            }}
                            value={soundTime}
                            onValueChange={
                                (sliderValue) => {
                                    if (sliderValue === 0) {
                                        setPlay(true)
                                        SoundPlayer.pause()
                                    }
                                    setSliderValue(sliderValue)
                                    seekTo(sliderValue)
                                }
                            }
                            onProgress={(e) => onPregress(e)}
                            // onSlidingStart={}
                            thumbTintColor="#e67e22"
                            maximumTrackTintColor="grey"
                            minimumTrackTintColor="#e67e22"
                        />)
                }
                {loadingFile === false && <Text style={{
                    marginHorizontal: 5
                }}>Loading...</Text>}

            </View>

            {checkUrl() && <View style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginHorizontal: 5
            }}>
                {
                    play && loadingFile && <Text style={{
                        paddingHorizontal: 5,
                        fontSize: 12
                    }}>{duration}</Text>
                }
                {!play && <Text style={{ fontSize: 12 }}> {formatDuration(soundTime)} </Text>}
            </View>}
        </View>
    )
}

export default SoundPlayerComp
