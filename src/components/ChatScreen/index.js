import React, { useState, useCallback, useEffect, useRef } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, Clipboard, TextInput, ActivityIndicator, Platform, Linking, ToastAndroid } from 'react-native'
import { GiftedChat, Actions, InputToolbar, Send, Bubble } from 'react-native-gifted-chat'

import HeaderSehet from '../header-common'
import firestore from '@react-native-firebase/firestore'
import storage from '@react-native-firebase/storage'
import { useSelector, useDispatch } from 'react-redux'
import RNFS from 'react-native-fs'
import { ifValid } from '../../utils/ifNotValid'
import Images from '../../assets/images'
import Video from 'react-native-video'
import { Icon } from 'native-base'
import { useStorage } from '../../hooks/useStorage'
import DocumentPicker from 'react-native-document-picker'
import MediaPreview from './component/MediaPreview'
import { showMediaModal, showModal } from '../../actions/modal'
import { setFileUrl, setDocUrl } from '../../actions/chat'
import SoundPlayer from './component/SoundPlayer'

import AudioRecorderPlayer, { AudioSourceAndroidType, AVEncoderAudioQualityIOSType, AVEncodingOption } from 'react-native-audio-recorder-player';
import { FONT_FAMILY_SF_PRO_MEDIUM } from '../../styles/typography'
import { formatDuration } from '../../utils/core'
import { CheckReadWritePermission, CheckAudioPermission, AskReadWritePermissions } from '../../utils/permissions'
import RNFetchBlob from 'rn-fetch-blob'
import FileViewer from 'react-native-file-viewer';
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import ActionSheet from 'react-native-actionsheet'
import moment from "moment";
import { strings } from "../../utils/translation";
import { CHAT_AVAILABLE_DAYS } from '../../utils/constant'
import { useFileUpload } from '../../hooks/useFileUpload'
import { accessToken } from '../../services/auth'
import { errorPayload } from '../../utils/helper'

const audioRecorderPlayer = new AudioRecorderPlayer();
audioRecorderPlayer.setSubscriptionDuration(0.9);

const index = (props) => {
    const userRole = useSelector(state => state.userReducer.userProfile.userId.role)
    const modalVisible = useSelector(state => state.modal.mediaModal)
    const fileUrl = useSelector(state => state.chat.url)
    const fileType = useSelector(state => state.chat.type)
    const docUrl = useSelector(state => state.chat.docUrl)

    const dispatch = useDispatch()

    let player = useRef(null)
    let actionSheetRightRef = useRef(null)
    let actionSheetLeftRef = useRef(null)

    const appointmentData = props.navigation.state.params.data;
    const currentStatus = props.navigation.state.params.status;
    const { doctorId, patientId, _id } = appointmentData;

    const appoinment_id = _id;
    const doctor_id = doctorId?._id;
    const patient_id = patientId?._id;

    const patientuser = {
        _id: patientId?._id,
        avatar: ifValid(patientId?.imageUrl) ? patientId?.imageUrl : Images.user,
        fcmToken: patientId?.fcmToken,
        name: patientId?.fullName,
    }

    const doctoruser = {
        _id: doctorId?._id,
        avatar: ifValid(doctorId?.imageUrl) ? doctorId?.imageUrl : Images.user,
        fcmToken: doctorId?.fcmToken,
        name: doctorId?.fullName,
    }

    const [messages, setMessages] = useState([]);
    const [selectedFile, setSelectedFile] = useState(null);
    const [firebaseDoc, setFirebaseDoc] = useState(null);
    const [currentUri, setCurrentUri] = useState(null);
    const [token, setToken] = useState(null);

    const [isText, setIsText] = useState("");
    const [recordingState, setRecordingState] = useState(false);

    const [currentSelectedMessage, setCurrentSelectedMessage] = useState(null);

    //audio recorder stuff
    const [audioRedocoderdata, setAudioRedocoderdata] = useState({
        recordSecs: 0,
        recordTime: '00:00:00',
        currentPositionSec: 0,
        currentDurationSec: 0,
        playTime: '00:00:00',
        duration: '00:00:00',
    });

    // const { progress, url, error } = useStorage(selectedFile)
    const { progress, url, error } = useFileUpload(selectedFile, token)
    // const { progress, url, error } = useStorage(null)

    var app_date = appointmentData?.appointmentDate;
    var today = moment().format("YYYY-MM-DD");
    var diff = moment(today).diff(app_date, 'days')
    let chatAvailable = (diff > CHAT_AVAILABLE_DAYS) ? true : false

    useEffect(() => {
        getChatMessages()
        CheckReadWritePermission()
        getToken()
        return () => {
        }
    }, [])

    useEffect(() => {
        if (progress === 100 && url !== null) {
            const doc = {
                ...firebaseDoc,
                [
                    selectedFile.type === 'video/mp4'
                        ? "video"
                        : selectedFile.type === "image/jpg" || selectedFile.type === "image/jpeg" || selectedFile.type === "image/png"
                            ? "image"
                            : selectedFile.type === 'audio/mp3'
                                ? "audio"
                                : "document"
                ]: url
            }
            sendTofirebase(doc)
        }
    }, [url])

    const getToken = async () => {
        const mytoken = await accessToken()
        setToken(mytoken)
    }

    const onSend = useCallback((messages) => {
        const msg = messages[0]
        let sendMsg = {
            ...msg,
            sendby: userRole === 'PATIENT' ? patient_id : doctor_id,
            sendTo: userRole === 'DOCTOR' ? patient_id : doctor_id,
        }
        setMessages(previousMessages => GiftedChat.append(previousMessages, sendMsg))
        sendTofirebase(sendMsg)
    }, [])

    const sendTofirebase = async (msg) => {
        try {
            await firestore()
                .collection('chats')
                .doc(appoinment_id)
                .collection("messages")
                .add({
                    ...msg,
                    createdAt: Date.parse(msg.createdAt),
                })

            console.log("message added to firebase")
        } catch (error) {
            console.log(error)
        }
    }

    const getChatMessages = async () => {
        try {
            await firestore()
                .collection("chats")
                .doc(appoinment_id)
                .collection("messages")
                .orderBy('createdAt', 'desc')
                .onSnapshot(snapshot => {
                    const data = []
                    const documents = snapshot?.docs;
                    if (documents?.length > 0) {
                        documents.map(item => {
                            const documentId = item.id;
                            const singleDoc = {
                                ...item.data(),
                                documentId
                                // createdAt: item.data().createdAt.toDate()
                            };
                            data.push(singleDoc);
                            // data.push(item.data());
                        })
                    }
                    setMessages(data)
                })

        } catch (error) {

        }
    }

    const renderbubble = (props) => {
        return <Bubble
            {...props}
            textStyle={{
                // right: {
                //     color: 'yellow',
                // },
            }}
            wrapperStyle={{
                right: {
                    backgroundColor: '#3498db',
                },
            }}
        />
    }

    const renderVideo = (props) => {
        const { currentMessage } = props;
        return (
            <TouchableOpacity
                activeOpacity={1}
                onPress={() => {
                    dispatch(setFileUrl(currentMessage.video, "video"))
                    dispatch(showMediaModal())
                }}
                onLongPress={() => {
                    onLongPress(null, currentMessage)
                }}
            >
                <Video source={{ uri: currentMessage.video }}
                    style={{
                        height: 200,
                        width: 200,
                        borderRadius: 8,
                        margin: 8
                    }}
                    ref={(ref) => { player = ref }}
                    onLoad={() => { player.seek(0) }}
                    // resizeMode="contain"
                    repeat={false}
                    paused={true}
                    muted={true}
                    playInBackground={false}
                    playWhenInactive={false}
                />
                {renderMediaLoading()}
                <View style={{
                    position: 'absolute',
                    bottom: 10,
                    left: 15
                }}>
                    <Icon
                        name="video-camera"
                        type="Entypo"
                    />
                </View>
            </TouchableOpacity>
        )
    }

    const renderImage = (props) => {
        const { context, currentMessage } = props;
        if (typeof currentMessage?.image !== "string") {
            return null
        }
        return (
            <TouchableOpacity
                onPress={() => {
                    dispatch(setFileUrl(currentMessage.image, "image"))
                    dispatch(showMediaModal())
                }}
                activeOpacity={1}
                onLongPress={() => {
                    onLongPress(null, currentMessage)
                }}
            >
                <Image
                    source={{ uri: currentMessage?.image }}
                    style={{
                        height: 200,
                        width: 200,
                        borderRadius: 8,
                        margin: 8
                    }}
                    resizeMode={"cover"}
                />
                {
                    renderMediaLoading()
                }
                <View style={{
                    position: 'absolute',
                    bottom: 10,
                    left: 15
                }}>
                    <Icon
                        name="image"
                        type="FontAwesome"
                    />
                </View>
            </TouchableOpacity>
        )
    }

    const renderMediaLoading = () => {
        return (
            <>
                {selectedFile != null && progress !== 100 && selectedFile?.uri === currentUri
                    && <View style={styles.loadinContainer}>
                        {/* <View style={}> */}
                        <ActivityIndicator
                            style={{
                                padding: 8,
                            }}
                            color="white"
                            size="large"
                        >
                            {/* <Icon
                        name="close"
                        type="AntDesign"
                        style={{ fontSize: 30 }}
                    /> */}
                        </ActivityIndicator>
                        {/* </View> */}
                    </View>
                }
            </>
        )
    }

    const renderAudio = (props) => {
        const { currentMessage } = props;
        const audioUrl = currentMessage?.audio
        return <TouchableOpacity
            activeOpacity={1}
            style={{
                paddingVertical: 10,
                paddingHorizontal: 5
            }}
            onLongPress={() => {
                onLongPress(null, currentMessage)
            }}
        >
            <SoundPlayer url={audioUrl} type={"audio"} />
        </TouchableOpacity>
    }

    const renderActions = () => {
        return <TouchableOpacity
            onPress={async () => {
                // const check = await CheckAudioPermission()
                // // alert(check)
                // if (check) {
                ChooseFile()
                // }
            }}
            style={{
                padding: 10,
                // borderRightWidth: 1
                // borderWidth: 1,
                // borderColor: 'lightgrey'
            }}
        >
            <Icon
                name="attachment"
                type="Entypo"
                style={{
                    fontSize: 26
                }}
            />
        </TouchableOpacity>
    }

    const ChooseFile = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.images, DocumentPicker.types.video, DocumentPicker.types.pdf],
            });
            console.log("select image in chat " +
                res.uri,
                res.type, // mime type
                res.name,
                res.size
            );
            // return alert(res.type)
            // const mainUri = await getPathForFirebaseStorage(res.uri)

            if (res.size < 5000000) {
                const mainUri = res.uri
                const resObj = {
                    uri: mainUri,
                    type: res.type, // mime type
                    name: res.name,
                    size: res.size
                }
                setSelectedFile(resObj)
                handleOnSelectMedia(resObj)
                setCurrentUri(mainUri)
            } else {
                dispatch(showModal(errorPayload("File size must be less than 5 MB")))
            }
        } catch (error) {
            console.log("error " + error);
        }
    }

    const getPathForFirebaseStorage = async (uri) => {
        if (Platform.OS === 'ios') {
            return uri
        }
        const stat = await RNFetchBlob.fs.stat(uri)
        return stat.path
    }

    const handleOnSelectMedia = (res) => {
        const type = res.type;
        // alert(JSON.stringify(mediDoc))
        let newMsg = {
            _id: new Date().getTime(),
            createdAt: new Date(),
            sendby: userRole === 'PATIENT' ? patient_id : doctor_id,
            sendTo: userRole === 'DOCTOR' ? patient_id : doctor_id,
            user: userRole === 'PATIENT' ? patientuser : doctoruser,
            [
                type === 'video/mp4'
                    ? "video"
                    : type === "image/jpg" || type === "image/jpeg" || type === "image/png"
                        ? "image"
                        : type === 'audio/mp3'
                            ? "audio"
                            : "document"
            ]: res.uri,
            file_name: res.name
        }
        setMessages(previousMessages => GiftedChat.append(previousMessages, newMsg))
        setFirebaseDoc(newMsg)
    }

    const renderEmptyChatComponent = () => {
        if (chatAvailable) return null
        return <TouchableOpacity
            activeOpacity={1}
            style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
                transform: [{ scaleY: -1 }],
            }}
        >
            <Text style={{
                color: '#1B80F3',
                fontSize: 16
            }}
                onPress={() => {
                    let newMsg = {
                        _id: new Date().getTime(),
                        createdAt: new Date(),
                        sendby: userRole === 'PATIENT' ? patient_id : doctor_id,
                        sendTo: userRole === 'DOCTOR' ? patient_id : doctor_id,
                        user: userRole === 'PATIENT' ? patientuser : doctoruser,
                        text: "Hello!"
                    }
                    setMessages(previousMessages => GiftedChat.append(previousMessages, newMsg))
                    sendTofirebase(newMsg)
                }}
            >Say "Hello! 👋"</Text>
        </TouchableOpacity>
    }

    function onLongPress(context, message) {
        const user_id = userRole === "PATIENT" ? patientId._id : doctorId._id
        if (message?.sendby === user_id) {
            setCurrentSelectedMessage(message)
            actionSheetRightRef.current.show()
        } else {
            if (message?.text) {
                setCurrentSelectedMessage(message)
                actionSheetLeftRef.current.show()
            }
        }
    }

    const renderInputToolbar = (props) => {
        return <>
            {
                chatAvailable
                    ? <View style={{
                    }}>
                        <Text style={{
                            textAlign: 'center',
                            fontSize: 16,
                            paddingHorizontal: 10,
                            fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
                        }}>
                            {strings.chat_footer}
                        </Text>
                    </View>
                    : <InputToolbar
                        {...props}
                        containerStyle={{
                            borderRadius: 30,
                            backgroundColor: 'lightgrey',
                            marginHorizontal: 10,
                            // position: 'absolute',
                            // bottom: 10
                            // marginTop: 20,
                            // marginBottom: 10
                            // borderColor: 'white',
                            // borderWidth: 0
                        }}
                    />
            }
        </>
    }

    const renderSend = (props) => {
        return <View
            style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 8 }}>
            <Send
                {...props}
            >
                <>
                    <Icon
                        name="send"
                        type="Feather"
                        style={{ fontSize: 28, marginRight: 5 }}
                    />
                </>
            </Send>
            {
                isText == "" && (
                    recordingState === false ? <TouchableOpacity
                        style={{ marginRight: 8 }}
                    >
                        <Icon
                            name="mic"
                            type="Feather"
                            style={{ fontSize: 30 }}
                            onPress={async () => {
                                // const audioReady = await CheckAudioPermission()
                                if (Platform.OS === 'android') {
                                    const readWriteReady = await CheckReadWritePermission()
                                    if (readWriteReady) {
                                        setRecordingState(true)
                                        onStartRecord()
                                    }
                                } else {
                                    setRecordingState(true)
                                    onStartRecord()
                                }
                            }}
                        />
                    </TouchableOpacity>
                        : <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={{ marginHorizontal: 8, fontSize: 16, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, }}>
                                {/* {formatDuration(audioRedocoderdata.recordSecs)} */}
                                recording...
                            </Text>
                            <Icon
                                name="stop-circle"
                                type="Feather"
                                style={{ fontSize: 32, color: 'red', marginRight: 8 }}
                                onPress={() => {
                                    setRecordingState(false)
                                    onStopRecord()
                                }}
                            />
                        </TouchableOpacity>
                )}
        </View>
    }

    const onStartRecord = async () => {
        try {
            const path = RNFS.DocumentDirectoryPath + `/${new Date().getTime()}_audio.mp3`;
            const audioSet = {
                AudioEncoderAndroid: AudioSourceAndroidType.AAC,
                AudioSourceAndroid: AudioSourceAndroidType.MIC,
                AVEncoderAudioQualityKeyIOS: AVEncoderAudioQualityIOSType.high,
                AVNumberOfChannelsKeyIOS: 2,
                AVFormatIDKeyIOS: AVEncodingOption.aac,
            };
            const result = await audioRecorderPlayer.startRecorder(path);
            // const result = await audioRecorderPlayer.startRecorder();
            audioRecorderPlayer.addRecordBackListener((e) => {
                setAudioRedocoderdata({
                    ...audioRedocoderdata,
                    recordSecs: e.current_position,
                    recordTime: audioRecorderPlayer.mmssss(
                        // Math.floor(e.current_position),
                        e.current_position,
                    ),
                });
                return;
            });
            console.log("onStartRecord " + result);
        } catch (error) {
            console.log("onStartRecord " + error);
        }
    };

    const onStopRecord = async () => {
        try {
            const result = await audioRecorderPlayer.stopRecorder();
            audioRecorderPlayer.removeRecordBackListener();
            setAudioRedocoderdata({
                ...audioRedocoderdata,
                recordSecs: 0,
            });
            console.log(result);
            let ress = {
                type: "audio/mp3",
                uri: result,
                name: `${new Date().getTime()}_audio.mp3`
            }
            setSelectedFile(ress)
            handleOnSelectMedia(ress)
        } catch (error) {
            console.log("onStopRecord " + error);
        }
    };

    const onDelete = async (id) => {
        try {
            await firestore()
                .collection('chats')
                .doc(appoinment_id)
                .collection("messages")
                .doc(id)
                .delete()
            await deleteFile()
        } catch (error) {
            console.log(error);
        }
    }

    const deleteFile = async () => {
        try {
            const url = currentSelectedMessage?.image || currentSelectedMessage?.video || currentSelectedMessage?.audio || currentSelectedMessage?.document
            console.log(url);
            const delRef = await storage().refFromURL(url)
            delRef.delete()
        } catch (error) {
            console.log(error);
        }
    }

    const renderCustomView = (props) => {
        const { currentMessage } = props
        return <>
            {
                currentMessage?.document && <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => {
                        dispatch(setDocUrl(currentMessage.document))
                        LoadDocument()
                    }}
                    onLongPress={() => {
                        onLongPress(null, currentMessage)
                    }}
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        padding: 10
                    }}
                >
                    <Icon
                        name="filetext1"
                        type="AntDesign"
                        style={{
                            // color: '#FFF',
                            fontSize: 24
                        }}
                    />
                    <Text
                        numberOfLines={1}
                        style={{ marginRight: 10, marginLeft: 1, fontSize: 16, width: 100 }}
                    >
                        {currentMessage?.file_name}
                    </Text>

                    <Icon
                        name={Platform.OS === 'android' ? "download-circle-outline" : 'download'}
                        type={Platform.OS === 'android' ? "MaterialCommunityIcons" : "Entypo"}
                        style={{
                            // color: '#FFF',
                            fontSize: Platform.OS === 'android' ? 34 : 26
                        }}
                        onPress={() => Linking.openURL(currentMessage.document)}
                    />
                </TouchableOpacity>
            }
        </>
    }

    const LoadDocument = () => {
        if (docUrl !== null) {
            { Platform.OS === 'android' && ToastAndroid.show('Loading file....', 2); }
            const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.pdf`;

            const options = {
                fromUrl: docUrl,
                toFile: localFile
            };
            RNFS.downloadFile(options).promise
                .then(() => FileViewer.open(localFile))
                .then(() => {
                    // success
                    { Platform.OS === 'android' && ToastAndroid.show('File loaded', 2); }
                })
                .catch(error => {
                    console.log("LoadDocument " + error);
                    // error
                });
        }
    }

    const onRightActionPress = (index) => {
        switch (index) {
            case 0:
                const messageIdToDelete = currentSelectedMessage?.documentId
                onDelete(messageIdToDelete)
                break;
            case 1:
                break;
        }
    }

    const onLeftActionPress = (index) => {
        switch (index) {
            case 0:
                Clipboard.setString(currentSelectedMessage?.text);
                break;
            case 1:
                break;
        }
    }

    return (
        <View style={styles.container}>
            <View style={{
                height: Platform.OS === 'ios' ? hp("11%") : hp("10%"),
                backgroundColor: "#FFFFFF",
                // justifyContent: "center",
                shadowOffset: { width: 0, height: 1 },
                shadowOpacity: 0.8,
                shadowRadius: 2,
                shadowColor: '#0000000B',
                elevation: 5,
                flexDirection: 'row',
                alignItems: 'center',
            }}>
                <Icon
                    name="arrowleft"
                    type="AntDesign"
                    style={{
                        fontSize: hp("3.5%"),
                        color: "#041A32",
                        marginHorizontal: 10,
                        marginBottom: 10,
                        marginTop: Platform.OS === 'ios' ? 25 : 0
                    }}
                    onPress={() => props.navigation.goBack()}
                />
                <View style={{
                    flex: 1,
                    marginLeft: 5,
                    marginTop: Platform.OS === 'ios' ? 25 : 0
                }}>
                    <Text style={{
                        color: "#041A32",
                        fontSize: 18,
                        // marginLeft: wp("4%"),
                        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
                    }}>{userRole === "PATIENT" ? doctorId?.fullName : patientId?.fullName}</Text>

                    <Text>{currentStatus}</Text>
                </View>

            </View>
            <GiftedChat
                messages={messages}
                onSend={messages => onSend(messages)}
                user={userRole === 'PATIENT' ? patientuser : doctoruser}
                scrollToBottom={true}
                renderInputToolbar={renderInputToolbar}
                renderMessageVideo={renderVideo}
                renderMessageImage={renderImage}
                renderActions={renderActions}
                renderChatEmpty={renderEmptyChatComponent}
                renderMessageAudio={renderAudio}
                renderSend={renderSend}
                onInputTextChanged={(txt) => setIsText(txt)}
                textInputProps={{
                    autoCorrect: false,
                    autoCapitalize: "none"
                }}
                renderBubble={renderbubble}
                onLongPress={onLongPress}
                renderCustomView={renderCustomView}
            // alwaysShowSend
            />

            <MediaPreview
                url={fileUrl}
                type={fileType}
                visible={modalVisible}
            />

            <ActionSheet
                ref={o => actionSheetRightRef.current = o}
                title={'Select an option'}
                options={['Delete', 'Cancel']}
                cancelButtonIndex={1}
                // destructiveButtonIndex={1}
                onPress={(index) => onRightActionPress(index)}
            />
            <ActionSheet
                ref={o => actionSheetLeftRef.current = o}
                title={'Select an option'}
                options={['Copy text', 'Cancel']}
                cancelButtonIndex={1}
                // destructiveButtonIndex={1}
                onPress={(index) => onLeftActionPress(index)}
            />
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F9F9F9',
        paddingBottom: 8,
    },
    loadinContainer: {
        position: 'absolute',
        left: '35%',
        top: '45%',
    },
    loadinClose: {
        color: 'red',
        backgroundColor: 'transparent',
        borderRadius: 50,
        padding: 5,
        borderWidth: 5,
        borderColor: 'red'
    }
})
export default index
