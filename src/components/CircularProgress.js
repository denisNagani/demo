import React from "react";
import { View } from "react-native";
import { Svg, Circle, Text as SVGText } from 'react-native-svg'

const CircularProgress = ({ size, strokeWidth, text, progressPercent, bgColor, pgColor, totalPercent }) => {

    const radius = (size - strokeWidth) / 2;
    const circum = radius * 2 * Math.PI;
    const svgProgress = totalPercent - progressPercent;

    return (
        <View style={{ margin: 10 }}>
            <Svg width={`${size}`} height={`${size}`}>
                {/* Background Circle */}
                <Circle
                    stroke={"#FBDAC7"}
                    fill="none"
                    cx={`${size / 2}`}
                    cy={`${size / 2}`}
                    r={`${radius}`}
                    {...{ strokeWidth: "10" }}
                />

                {/* Progress Circle */}
                <Circle
                    stroke={"#FF5E38"}
                    fill="none"
                    cx={`${size / 2}`}
                    cy={`${size / 2}`}
                    r={`${radius}`}
                    strokeDasharray={`${circum} ${circum}`}
                    strokeDashoffset={`${radius * Math.PI * 2 * (svgProgress / 100)}`}
                    strokeLinecap="round"
                    transform={`rotate(-90, ${`${size / 2}`}, ${`${size / 2}`})`}
                    {...{ strokeWidth: "10" }}
                />

                {/* Text */}
                <SVGText
                    fontSize={"18"}
                    x={"75"}
                    fontWeight="bold"
                    y={"82"}
                    textAnchor="middle"
                    fill={"#333333"}
                >
                    {text}
                </SVGText>
                <SVGText
                    fontSize={"12"}
                    x={"75"}
                    y={"100"}
                    textAnchor="middle"
                    fill={"#333333"}
                >
                    Consultation
                </SVGText>

            </Svg>
        </View>
    )
}

export default CircularProgress;