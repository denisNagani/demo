import { View, Text, FlatList, StyleSheet, TouchableOpacity, Platform, SafeAreaView, Image, TextInput } from 'react-native'
import React, { useEffect, useState } from 'react'
import { BottomModal } from 'react-native-modals'
import { DEVICE_HEIGHT } from '../styles/typography'
import { useSelector } from 'react-redux'
import { Button, Icon } from 'native-base'
import { store } from '../store'
import { filterListByCity, updateCitySelected } from '../scenes/patient/select-and-book/action'

const CityModal = ({ visible, onCloseModal }) => {
    const cityFilterSelected = useSelector((state) => state.doctorFilterReducer.cityFilterSelected);
    const cities = useSelector((state) => state.doctorFilterReducer.cityFilter);

    const [searchCity, setSearchCity] = useState('');

    useEffect(() => {
        store.dispatch(filterListByCity(searchCity, cities))
    }, [searchCity])

    const onSelectedCityChange = (item) => {
        let result = cityFilterSelected.map(x => {
            return x._id === item._id ? { ...x, value: !x.value } : x;
        })
        store.dispatch(updateCitySelected(result))
    }

    const onTextSearch = (text) => {
        setSearchCity(text)
    }

    return (
        <BottomModal
            visible={visible}
            onTouchOutside={onCloseModal}
            width={1}
            height={.8}
        >

            <SafeAreaView style={{ flex: 1 }}>

                <View style={{ flex: 1, marginHorizontal: 15, marginVertical: 10 }}>

                    <Button block style={{
                        backgroundColor: '#FBE7E3',
                        borderRadius: 8,
                        paddingHorizontal: 15,
                        elevation: 0,
                        alignSelf: 'flex-end'
                    }} onPress={onCloseModal}>
                        <Text style={{
                            color: '#FF5E38',
                            fontSize: 16
                        }}>{"Done"}</Text>
                    </Button>
                    <TextInput
                        style={{
                            borderWidth: 1,
                            marginVertical: 10,
                            paddingHorizontal: 15,
                            paddingVertical: 8,
                            borderRadius: 8,
                            borderColor: 'grey',
                            height: 45,
                            fontSize: 16
                        }}
                        placeholder={"search city"}
                        value={searchCity}
                        onChangeText={text => onTextSearch(text)}
                        autoCapitalize="none"
                        autoCorrect={false}
                    />

                    <FlatList
                        scrollEnabled
                        showsVerticalScrollIndicator={true}
                        style={{ flex: 1, marginLeft: 20, }}
                        keyExtractor={(item, index) => index.toString()}
                        data={cityFilterSelected}
                        keyboardShouldPersistTaps={true}
                        ListEmptyComponent={() => <View><Text>No cities found</Text></View>}
                        renderItem={({ item }) =>
                            <TouchableOpacity onPress={() => onSelectedCityChange(item)}
                                style={{
                                    flex: 1, flexDirection: 'row', paddingVertical: 5,
                                    paddingRight: 20
                                }}>
                                <Text style={{ flex: 8, color: item.value ? '#038BEF' : 'black' }}>{item.name}</Text>
                                <Icon type={"AntDesign"} name={"check"} style={{ color: item.value ? '#038BEF' : 'white' }} />
                            </TouchableOpacity>}
                    />
                </View>
            </SafeAreaView>

        </BottomModal>
    )
}
const styles = StyleSheet.create({
})

export default CityModal