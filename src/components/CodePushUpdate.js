import React from 'react'
import { View, Text } from 'react-native'
import Dialog, { DialogContent, SlideAnimation, } from "react-native-popup-dialog";
import { bytesToSize } from '../utils/helper';
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_REGULAR } from '../styles/typography';

const CodePushUpdate = ({ total, downloaded }) => {
    const totalSize = bytesToSize(total)
    const downloadedSize = bytesToSize(downloaded)
    return (
        <View>
            <Dialog
                visible={true}
                dialogAnimation={
                    new SlideAnimation({
                        slideFrom: "bottom",
                    })
                }
                width={325}
                // height={250}
                overlayOpacity={0.5}
            >
                <DialogContent style={{ width: '100%', }}>
                    <View style={{
                        marginTop: 30,
                        marginBottom: 20
                    }}>
                        <Text style={{
                            fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
                            fontSize: 20,
                            marginBottom: 10
                        }}>Downloading update...</Text>
                        <Text style={{
                            fontSize: 16,
                        }}>{`${downloadedSize} / ${totalSize}`}</Text>
                    </View>
                </DialogContent>
            </Dialog>
        </View>
    )
}

export default CodePushUpdate