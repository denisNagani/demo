import React, { useEffect, useState } from 'react'
import { Image, Linking, ScrollView, Text, View } from 'react-native'
import HeaderSehet from './header-common'
import Images from '../assets/images'
import { FONT_FAMILY_SF_PRO_REGULAR } from "../styles/typography";
import { strings } from "../utils/translation";
import { getContactNumber, getContactMobileNumbers } from '../services';
import { callNumber } from '../utils/helpline-no';
import { appName, supportEmailId } from '../config';

const ContactUs = (props) => {
    const [number, setNumber] = useState("")
    const [contactNo, setContactnno] = useState([])

    useEffect(() => {
        // getno()
        getContact()
    }, [])

    const getno = async () => {
        try {
            const number = await getContactNumber()
            setNumber(number)
            // callNumber(`+91 ${number}`);
        } catch (error) {
            setNumber("9983497088")
            // callNumber();
        }
    }
    const getContact = async () => {
        try {
            const number = await getContactMobileNumbers()
            setContactnno(number)
            // callNumber(`+91 ${number}`);
        } catch (error) {
            setContactnno(["08047166558"])
            // callNumber();
        }
    }
    return (
        <View style={{ flex: 1, }}>
            <HeaderSehet headerText={strings.help_center} navigation={props.navigation} changeTheme={true} />
            <ScrollView>
                <View
                    style={{
                        flex: 1,
                        padding: 20,
                        flexDirection: 'column'
                    }}>

                    <Image
                        source={Images.amar_logo}
                        style={{ height: 150, width: 150, alignSelf: 'center', color: '#000' }}
                        resizeMode="contain"
                    />

                    <Text style={{
                        fontSize: 16,
                        textAlign: 'justify',
                        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
                        lineHeight: 25
                    }}>
                        We at {appName} Private Limited put maximum Focus to resolve our Customer Doubts.
                    </Text>
                    <Text style={{
                        fontSize: 16,
                        textAlign: 'justify',
                        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
                        lineHeight: 25,
                        marginTop: 10
                    }}
                    >
                        In case if you have any queries. Reach out to us by emailing us at <Text style={{
                            color: 'blue',
                            textDecorationLine: 'underline'
                        }}
                            onPress={() => Linking.openURL(`mailto:${supportEmailId}`)}
                        >{supportEmailId}</Text> or call us on {
                            contactNo.map(item => (
                                <Text
                                    style={{
                                        color: 'blue',
                                        textDecorationLine: 'underline'
                                    }}
                                    onPress={() => {
                                        callNumber(`${item}`)
                                    }}
                                >{item} </Text>
                            ))
                        } our customer care executive will be happy to help you.
                    </Text>


                </View>
            </ScrollView>
        </View>
    )
}

export default ContactUs
