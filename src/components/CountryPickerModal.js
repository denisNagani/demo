import React, { useEffect, useState } from 'react'
import { View, Text, Modal, SafeAreaView, ScrollView, FlatList, Image, TextInput, TouchableOpacity } from 'react-native'
import { Icon, Button } from 'native-base'
import styles from "../scenes/patient/upcoming-past-bookings/style";
import { useDispatch, useSelector } from 'react-redux';
import { setCountryModal, setTcModal } from '../actions/modal';
import Line from './line';
import { getCountries } from '../actions/login';
import { isValid } from '../utils/ifNotValid';

const CountryPickerModal = ({ data, callback }) => {
    const dispatch = useDispatch()

    const country = useSelector(state => state.modal.country)
    const [contData, setContData] = useState(data)
    const [txtValue, setTxtValue] = useState('')

    const onClosePress = () => {
        dispatch(setCountryModal(false))
    }

    useEffect(() => {
        if (isValid(txtValue)) {
            const filterData = data?.filter(item => item?.name?.toLowerCase()?.includes(txtValue?.toLowerCase()));
            setContData(filterData)
        }
        if (txtValue?.length == 0) {
            setContData(data)
        }
    }, [txtValue])

    useEffect(() => {
        setContData(data)
    }, [data])

    return (
        <Modal visible={country} animationType="slide">
            <SafeAreaView style={styles.modalContainer}>
                <View style={[styles.modalheader, { marginHorizontal: 10 }]}>
                    <TextInput
                        style={{
                            height: 45,
                            borderWidth: 1,
                            flex: 1,
                            borderColor: '#DCDCDC',
                            paddingLeft: 10,
                            fontSize: 16,
                            borderRadius: 5,
                        }}
                        placeholder="Search Country"
                        value={txtValue}
                        onChangeText={val => setTxtValue(val)}
                        autoCorrect={false}
                        autoCapitalize='none'
                    />
                    <Icon
                        name="close"
                        type="AntDesign"
                        style={{
                            // fontSize: 22,
                            marginLeft: 10,
                        }}
                        onPress={onClosePress}
                    />
                </View>
                <View style={{ marginHorizontal: 10 }}>
                    <FlatList
                        data={contData}
                        keyboardShouldPersistTaps='always'
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={({ item }) => {
                            return <TouchableOpacity onPress={() => {
                                onClosePress()
                                callback(item)
                            }}>
                                <View style={{
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    paddingVertical: 10,
                                }}>
                                    <Image
                                        style={{
                                            height: 20,
                                            width: 30,
                                            borderRadius: 3,
                                            marginHorizontal: 10
                                        }}
                                        source={{ uri: item?.flag }}
                                    />
                                    <Text style={{
                                        color: '#000',
                                        fontSize: 18,
                                    }}>{` ${item?.name}`}</Text>
                                </View>
                                <Line />
                            </TouchableOpacity>
                        }}
                    />
                </View>
            </SafeAreaView>
        </Modal>
    )
}

export default CountryPickerModal