import React, { useEffect } from 'react'
import NavigationService from "../components/startScreen/RootNavigation";
import dynamicLinks from '@react-native-firebase/dynamic-links'
import firestore from '@react-native-firebase/firestore'
import { ifValid } from "../utils/ifNotValid";
import { setFromDynamicLink, SetSpecialityDetails } from "../actions/profile";
import { useDispatch } from 'react-redux';

const DynamicLink = () => {

    const dispatch = useDispatch()

    useEffect(() => {
        const unsubscribe = dynamicLinks().onLink(handleDynamicLink);
        // When the component is unmounted, remove the listener
        return () => unsubscribe();
    }, []);

    const handleDynamicLink = link => {
        console.log('App In Foreground handleDynamicLink');
        // Handle dynamic link inside your own application
        if (ifValid(link?.url)) {

            firestore()
                .collection("dynamic_links")
                .get()
                .then(async querySnapshot => {
                    if (querySnapshot.size > 0) {
                        let arr = []
                        await querySnapshot.forEach(documentSnapshot => {
                            let document = documentSnapshot.data()
                            arr.push(document)
                        });

                        console.log(arr);

                        arr?.map(item => {
                            console.log("item link ", item.link);
                            if (item?.link == link?.url) {
                                // ...navigate to your offers screen
                                dispatch(setFromDynamicLink(true, null))
                                dispatch(SetSpecialityDetails(item?.doc))
                                NavigationService.navigate("BookAppointmentScreenFlow2", {
                                    doc: item?.doc
                                })
                            }
                        })
                    }
                })
                .catch(err => console.log(err))
        }
    };
    return null
}

export default DynamicLink
