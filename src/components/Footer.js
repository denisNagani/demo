import React from 'react'
import { View, Text } from 'react-native'

const Footer = ({ children }) => {
    return (
        <View style={{
            backgroundColor: '#FFFFFF',
            shadowColor: '#0000000B',
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 1,
            shadowRadius: 2,
            elevation: 8,
            borderWidth: 1,
            borderColor: '#0000000B',
            padding: 15
        }}>
            {children}
        </View>
    )
}

export default Footer
