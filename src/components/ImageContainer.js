import React from 'react'
import { View, Text, ImageBackground } from 'react-native'
import Images from '../assets/images'

const ImageContainer = ({ children }) => {
    return (
        <ImageBackground
            resizeMode="cover"
            source={Images.background_theme}
            style={{ flex: 1 }}
        >
            {children}
        </ImageBackground>
    )
}

export default ImageContainer
