import React, { useEffect, useState } from 'react'
import { View, Text, SafeAreaView, Image, TouchableOpacity, ImageBackground, StyleSheet, Platform } from 'react-native'
import Images from '../../assets/images'
import { FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_SemiBold } from '../../styles/typography'
import NavigationService from "../../components/startScreen/RootNavigation";
import { setIncomingCallScreen, getCallerData, setCallerData } from '../../services/auth'
import { useDispatch } from 'react-redux'

const index = (props) => {

    const dispatch = useDispatch()

    const [callerImageUrl, setCallerImageUrl] = useState(null)
    const [callerName, setCallerName] = useState("")

    useEffect(() => {
        getCallerDetails()
        return () => {
            setIncomingCallScreen("false")
            setCallerData(null)
        }
    }, [])

    const getCallerDetails = async () => {
        const callerData = await getCallerData()
        const mainData = JSON.parse(callerData)
        const { fullName, imageUrl } = mainData
        setCallerName(fullName)
        setCallerImageUrl(imageUrl)
    }

    const receiveCall = () => {

        //navigate to video calling screen
        console.log("redirect to video calling screen ");
        NavigationService.navigate("DashboardWaitingScreen")
        // setIsVideoNotification("false")
        setIncomingCallScreen("false")
    }

    const rejectCall = () => {

        //navigate to home screen
        console.log("redirect to home ");
        NavigationService.navigate("DashboardWaitingScreen")
        // setIsVideoNotification("false")
        setIncomingCallScreen("false")
    }
    return (

        <ImageBackground
            resizeMode="cover"
            source={Images.incoming_bg}
            style={{ flex: 1 }}
        >
            <SafeAreaView style={{ flex: 1 }}>
                <View style={styles.mainContainer}>
                    <Image
                        style={styles.imageStyle}
                        source={
                            callerImageUrl == undefined || callerImageUrl == "" || callerImageUrl == null
                                ? Images.male_doctor
                                : { uri: callerImageUrl }
                        }
                        defaultSource={Images.male_doctor}
                    />
                    <Text style={[styles.cText, { fontSize: 30, marginBottom: 5 }]}>
                        {callerName}
                    </Text>

                    <Text style={[styles.cText, { fontSize: 18, fontFamily: FONT_FAMILY_SF_PRO_REGULAR }]}>Incoming Video Call</Text>
                    <Text style={[styles.cText, { fontSize: 18, fontFamily: FONT_FAMILY_SF_PRO_REGULAR }]}>Your patient is waiting for you</Text>
                </View>

                <View style={{
                    marginHorizontal: 20,
                    marginVertical: 20
                }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            justifyContent: 'space-between'
                        }}
                    >
                        <TouchableOpacity
                            onPress={rejectCall}
                        >
                            <Image
                                source={Images.end_call}
                                style={styles.btnStyle}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => receiveCall(item)}
                        >
                            <Image
                                source={Images.video_call}
                                style={styles.btnStyle}
                            />
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <Image
                                source={Images.video_message}
                                style={styles.btnStyle}
                            />
                        </TouchableOpacity>
                    </View>
                    <Text style={{ marginVertical: 8, fontFamily: FONT_FAMILY_SF_PRO_REGULAR, color: '#FFF', textAlign: 'center' }}>Swipe up to accept</Text>
                </View>
            </SafeAreaView>
        </ImageBackground>
    )
}
const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        alignItems: 'center',
        // justifyContent: 'center',
    },
    callBtn: {
        height: 80,
        width: 80,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 80 / 2,
    },
    imageStyle: {
        height: 100,
        width: 100,
        borderRadius: 100 / 2,
        marginTop: 50,
        marginBottom: 8
    },
    cText: { fontFamily: FONT_FAMILY_SF_PRO_SemiBold, color: 'white' },
    btntext: { fontSize: 14, fontFamily: FONT_FAMILY_SF_PRO_REGULAR },
    btnStyle: {
        height: 55,
        width: 55
    }
})
export default index
