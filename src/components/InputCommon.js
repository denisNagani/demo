import React from "react";
import { StyleSheet, Text, TextInput, View } from "react-native";
import common from "../styles/common";

const InputCommon = ({ styleInput, style, title, titleStyle, ...props }) => (
    <View style={[style, {}]}>
        <Text style={[common.titleStyle_, titleStyle]}>{title}</Text>
        <TextInput
            style={[styles.boxStyleAmount, { ...styleInput }]}
            {...props} />
    </View>
);
export default InputCommon;

const styles = StyleSheet.create({

    boxStyleAmount: {
        paddingHorizontal: 9, paddingVertical: 5, width: '100%',
        marginTop: 4, alignItems: 'center', borderRadius: 9, borderColor: '#B6BECB', borderWidth: 0.6,
        color: 'black'
    },
})
