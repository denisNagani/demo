import React, { useState, useEffect } from 'react'
import { TextInput, Text, ScrollView, Image, KeyboardAvoidingView, View, TouchableOpacity } from 'react-native'
import { Button, Icon } from "native-base";
import { useDispatch, useSelector } from 'react-redux'
import Modal, { ModalContent } from "react-native-modals";
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_REGULAR } from '../styles/typography'
import { strings } from '../utils/translation';
import { setMicModal } from '../actions/sehet/speciality-action';
import Voice from '@react-native-voice/voice';
import { isValid } from '../utils/ifNotValid';
import { setSearchDocName } from '../scenes/patient/select-and-book/action';
import NavigationService from './startScreen/RootNavigation'
import Images from '../assets/images';
import { EMPTY_DOC_LIST } from '../utils/constant';

const MicModal = ({ visible }) => {
    const dispatch = useDispatch()
    const selected_language = useSelector(state => state.profile.selected_language)

    const [startedRecording, setStartedRecording] = useState(false);
    const [error, setError] = useState(null);
    const [errorMsg, setErrorMsg] = useState('');
    const [results, setResults] = useState([]);

    useEffect(() => {
        //Setting callbacks for the process status
        Voice.onSpeechStart = onSpeechStart;
        Voice.onSpeechEnd = onSpeechEnd;
        Voice.onSpeechError = onSpeechError;
        Voice.onSpeechResults = onSpeechResults;
        Voice.onSpeechPartialResults = onSpeechPartialResults;
        Voice.onSpeechVolumeChanged = onSpeechVolumeChanged;

        return () => {
            //destroy the process after switching the screen
            Voice.destroy().then(Voice.removeAllListeners);
        };
    }, []);

    useEffect(() => {
        if (results?.length > 0) {
            setTimeout(() => {
                console.log('redirect to doctors list');
                closeModal()
                NavigationService.navigate("SelectAndBook", {
                    searchText: results[0]
                })
                dispatch(setSearchDocName(""))
                dispatch({ type: EMPTY_DOC_LIST })
            }, 1000);
        }
    }, [results])

    const checkSpeechAvaible = async () => {
        try {
            console.log("inside checkSpeechAvaible");
            const isavailable = await Voice.isAvailable()
            const services = await Voice.getSpeechRecognitionServices()
            return {
                isavailable,
                services
            }
        } catch (error) {
            console.log('error ', error);
            return null
        }
    }

    const onSpeechStart = (e) => {
        //Invoked when .start() is called without error
        console.log('onSpeechStart: ', e);
    };

    const onSpeechEnd = (e) => {
        //Invoked when SpeechRecognizer stops recognition
        console.log('onSpeechEnd: ', e);
    };

    const onSpeechError = (e) => {
        //Invoked when an error occurs.
        console.log('onSpeechError: ', e);
        setError(JSON.stringify(e.error));
    };

    const onSpeechResults = (e) => {
        //Invoked when SpeechRecognizer is finished recognizing
        console.log('onSpeechResults: ', e);
        setResults(e.value);
    };

    const onSpeechPartialResults = (e) => {
        //Invoked when any results are computed
        console.log('onSpeechPartialResults: ', e);
    };

    const onSpeechVolumeChanged = (e) => {
        //Invoked when pitch that is recognized changed
        console.log('onSpeechVolumeChanged: ', e);
    };

    const startRecognizing = async () => {
        //Starts listening for speech for a specific locale
        try {
            const lang = selected_language == 'hin' ? 'hi-IN' : 'en-US'
            await Voice.start(lang);
            setError('');
            setResults([]);
        } catch (e) {
            //eslint-disable-next-line
            setErrorMsg(isValid(e) ? e : 'Unable to start recognizing..')
            console.log(e);
        }
    };

    const stopRecognizing = async () => {
        //Stops listening for speech
        try {
            await Voice.stop();
        } catch (e) {
            //eslint-disable-next-line
            console.error(e);
        }
    };

    const closeModal = () => {
        dispatch(setMicModal(false))
        setResults([])
    }

    return (
        <Modal
            width={0.9}
            style={{ flex: 1 }}
            visible={visible}
            onTouchOutside={closeModal}
            rounded
            actionsBordered
            onSwipeOut={closeModal}
            onHardwareBackPress={() => {
                closeModal()
                return true
            }}
        >
            <ModalContent style={{ backgroundColor: '#fff', }}>
                <View>
                    <TouchableOpacity
                        style={{ alignSelf: 'flex-end' }}
                        onPress={closeModal}
                    >
                        <Icon
                            name='close'
                            type='AntDesign'
                        />
                    </TouchableOpacity>
                    <Text style={{ marginHorizontal: 10, marginTop: 20, color: '#041A32', fontSize: 18, fontFamily: FONT_FAMILY_SF_PRO_BOLD }}>{
                        startedRecording
                            ? "Listening....."
                            : "Press and hold mic to speak"
                    }</Text>

                    <TouchableOpacity
                        style={{ alignItems: 'center', marginVertical: 20 }}
                        onPressIn={() => {
                            checkSpeechAvaible()
                                .then((res) => {
                                    if (res?.isavailable == false && res?.services?.length === 0) {
                                        setErrorMsg("No Speech recognition service/engines available on this device")
                                    } else if (res?.isavailable == false) {
                                        setErrorMsg("Speech recognition service is not available on this device")
                                    } else if (res?.services?.length === 0) {
                                        setErrorMsg("No speech recognition engines found on this device")
                                    } else {
                                        setStartedRecording(true)
                                        startRecognizing()
                                    }
                                })
                        }}
                        onPressOut={() => {
                            setStartedRecording(false)
                            stopRecognizing()
                        }}
                    >
                        <Image
                            style={{
                                height: 100,
                                width: 100,
                                borderRadius: 100,
                                tintColor: 'red'
                            }}
                            source={Images.speech_mic}
                        />
                    </TouchableOpacity>

                    {
                        isValid(errorMsg)
                            ? <Text style={{ textAlign: 'center', marginHorizontal: 10, marginTop: 20, color: 'red', fontSize: 16, fontFamily: FONT_FAMILY_SF_PRO_REGULAR }}>{errorMsg}</Text>
                            : isValid(error)
                                ? <Text style={{ textAlign: 'center', marginHorizontal: 10, marginTop: 20, color: '#041A32', fontSize: 16, fontFamily: FONT_FAMILY_SF_PRO_REGULAR }}>{"Try Again"}</Text>
                                : <Text style={{ textAlign: 'center', marginHorizontal: 10, marginTop: 20, color: '#041A32', fontSize: 16, fontFamily: FONT_FAMILY_SF_PRO_REGULAR }}>{results[0]}</Text>
                    }

                </View>
            </ModalContent>
        </Modal>
    )
}

export default MicModal
