import React, { useEffect } from 'react'
import { Picker, Icon } from 'native-base'
import { changeLaguage } from '../utils/translation'
import { Set_App_Language } from '../actions/profile'
import { useDispatch, useSelector } from 'react-redux'
import { setAppLangugae, getAppLangugae } from '../services/auth'
import { ifValid } from '../utils/ifNotValid'

const MultiLanguage = () => {
    const dispatch = useDispatch()
    const selected_language = useSelector(state => state.profile.selected_language)

    useEffect(() => {
        getData()
    }, [])

    const getData = async () => {
        const lan = await getAppLangugae()
        // alert(lan)
        console.log("lan ^^" + lan);
        if (ifValid(lan)) {
            changeLang(lan)
        } else {
            changeLang("en")
        }
    }

    const changeLang = async (value) => {
        dispatch(Set_App_Language(value))
        changeLaguage(value)
        await setAppLangugae(value)
    }

    return (
        <Picker
            mode="dropdown"
            // iosIcon={<Icon name="keyboard-arrow-down" type="MaterialIcons" />}
            selectedValue={selected_language}
            onValueChange={(value) => {
                if (value == "en") {
                    changeLang(value)
                } else if (value == "hin") {
                    changeLang(value)
                } else if (value == "ma") {
                    changeLang(value)
                }
            }}
            textStyle={{ color: '#FFF' }}
        >
            <Picker.Item label="English" value="en" />
            <Picker.Item label="Hindi" value="hin" />
            <Picker.Item label="Ma" value="ma" />
        </Picker>
    )
}

export default MultiLanguage
