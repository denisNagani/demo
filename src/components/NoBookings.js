import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import Images from '../assets/images'
import { strings } from '../utils/translation'

const NoBookings = () => {

    return (
        <View style={styles.noBookingsBlock}>
            <Image source={Images.no_bookings} style={styles.noBookingsImage} />
            <Text style={styles.noBookingsTitle}>{strings.No_appointments}</Text>
            <Text style={styles.noBookingsSubTitle}>
                {strings.No_appointments_chat}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    noBookingsBlock: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    noBookingsImage: {
        margin: 30,
        height: 200,
        width: 200,
    },
    noBookingsTitle: {
        fontSize: 20,
        fontWeight: "bold",
        marginVertical: 5,
    },
    noBookingsSubTitle: {
        fontSize: 15,
        fontWeight: "normal",
        color: "gray",
        textAlign: 'center',
        marginHorizontal: 10,
    },
    noBookingsBtnText: {
        color: "#fff",
    },
    noBookingsBtn: {
        margin: 20,
    },
})

export default NoBookings
