import { Linking, Modal, SafeAreaView, StyleSheet, Text, View } from "react-native";
import { Icon } from "native-base";
import { ScrollView } from "react-native-gesture-handler";
import React from "react";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const PatientConsent = ({ onPress, visible }) => {
    const data1 = [
        {
            title: "PURPOSE",
            body: "The purpose of Telemedicine Consent Form is to get the patient's consent in order to participate in appointments/consultation of telemedicine cares."
        },
        {
            title: "RECORDS",
            body: "Telecommunications with patients might be recorded and stored as per guidelines provided by moHFW. Patients' medical information obtained by the diagnosis and analysis can be used anonymously for further improvements in scientific studies."
        },
        {
            title: "TELEMEDICINE INFORMATION",
            body: "The medical information related to history, records and tests of the patient will be discussed during the telemedicine appointment with video and audio."
        },
        {
            title: "ACCESS",
            body: "The patient accepts that he/she needs access to PC, laptop, or mobile device and a good internet connection in order to have an efficient telemedicine appointment."
        },
        {
            title: "PATIENT RIGHTS",
            body: "The patient can withdraw his/her consent at any time and can ask the questions related to telemedicine appointments and technical requirements for telecommunication."
        },
    ]
    const data2 = [
        { title: 'I understand that my Doctor recommends engaging in Telemedicine services with me to provide treatment.' },
        { title: 'I understand this is out of necessity and an abundance of caution and has originated due to the Coronavirus (Covid-19) pandemic ' },
        { title: 'I understand that Telemedicine treatment has potential benefits including, but not limited to, easier access to care. ' },
        { title: 'I understand that Telemedicine has been found to be effective in treating a wide range of disorders, and there are potential benefits including, but not limited to easier access to care.  I understand; however, there is no guarantee that all treatment of all patients will be effective.' },
        { title: 'I understand that it is my obligation to notify my Doctor of my location at the beginning of each treatment session.  If for some reason, I change locations during the session, it is my obligation to notify my Doctor of the change in location.  ' },
        { title: 'I understand that it is my obligation to notify my Doctor of any other persons in the location, either on or off camera and who can hear or see the session.  I understand that I am responsible to ensure privacy at my location. I will notify my Doctor at the outset of each session and am aware that confidential information may be discussed.' },
        { title: 'I understand that it is my obligation to ensure that any virtual assistant artificial intelligence devices, including but not limited to Alexa or Echo, will be disabled or will not be in the location where information can be heard.' },
        { title: 'I agree that I will not record either through audio or video any of the session, unless I notify my Doctor and this is agreed upon. ' },
        { title: 'I understand there are potential risks to using Telemedicine technology, including but not limited to, interruptions, unauthorized access, and technical difficulties.  I understand some of these technological challenges include issues with software, hardware, and internet connection which may result in interruption.' },
        { title: 'I understand that my Doctor is not responsible for any technological problems of which my Doctor has no control over. I further understand that my Doctor does not guarantee that technology will be available or work as expected.' },
        { title: 'I understand that I am responsible for information security on my device, including but not limited to, computer, tablet, or phone, and in my own location.' },
        { title: 'I understand that my Doctor or I (or, if applicable, my guardian or conservator), can discontinue the Telemedicine consult/visit if it is determined by either me or my Doctor that the videoconferencing connections or protections are not adequate for the situation.' },
        { title: 'I have had a conversation with my Doctor, during which time I have had the opportunity to ask questions concerning services via Telemedicine.  My questions have been answered, and the risks, benefits, and any practical alternatives have been discussed with me.' },
        {
            title: 'Amar e-clinic mobile app will be used to conduct Telemedicine videoconferencing appointments. The Doctors have discussed the use of this platform.  Prior to each session, “Join Call” button will be available for me to start the call and soon the doctor will accept the call which will result in starting the video consultation.',
            subArr: [
                { text: "Amar e-clinic is NOT an emergency service.  In the event of an emergency, I will use a phone to call 108 and/or other appropriate emergency contact." },
                { text: "Amar e-clinic is NOT an emergency service.  In the event of an emergency, I will use a phone to call 108 and/or other appropriate emergency contact." },
            ]
        },
    ]

    const data3 = [
        { text: "1.	Amar e-clinic is NOT an emergency service.  In the event of an emergency, I will use a phone to call 108 and/or other appropriate emergency contact" },
        { text: "2.	I recognize my Doctor may need to notify emergency personnel in the event he/she feels there is a safety concern, including but not limited to, a risk to self/others or my Doctor is concerned that immediate medical attention is needed." },
        { text: "3.	Though my Doctor and I may be in virtual contact through Telemedicine services, neither Amar e-clinic nor my Doctor provides any medical or emergency or urgent healthcare services or advice.  I understand should medical services be required, I will contact my physician.  If emergency services are needed, I understand I should call 108." },
        { text: "4.	The Amar e-clinic facilitates videoconferencing and this technology platform is not, itself, a source of healthcare, medical advice, or care." },
        { text: "5.	I understand that the same fee rates may apply for Telemedicine as apply for in-person treatment.  Some insurers are waiving co-pays during this time.  It is my obligation to contact my insurer before engaging in Telemedicineto determines if there are applicable co-pays or fees which I am responsible for.  Insurance or other managed care providers may not cover Telemedicine sessions.  I understand that if my insurance, HMO, third-party pay or, or other managed care provider do not cover the Telemedicine sessions, I will be solely responsible for the entire fee of the session." },
        { text: "6.	To maintain confidentiality, I will not share my Telemedicine appointment link or information with anyone not authorized to attend the session." },
        { text: "7.	I understand that either I or my Doctor can discontinue the Telemedicine services if those services do not appear to benefit me therapeutically or for other reasons which will be explained to me.  I understand there may be no other treatment alternative available." }
    ]
    return (
        <Modal visible={visible} animationType="slide" onRequestClose={onPress}>
            <SafeAreaView style={styles.modalContainer}>
                <View style={styles.modalheader}>
                    <Icon
                        name="close"
                        type="AntDesign"
                        style={styles.modalheaderBackIcon}
                        onPress={onPress}
                    />
                    <Text style={styles.modalheading}>
                        Patient Consent
                    </Text>
                </View>
                <ScrollView contentContainerStyle={styles.modal}>

                    <Text style={[styles.headerCenter]}>PATIENT CONSENT</Text>
                    <Text style={[styles.headerCenter, { fontSize: 18 }]}>TELEMEDICINE PATIENT CONSENT FORM</Text>

                    {
                        data1.map((i, index) => (
                            <Text style={{ paddingVertical: 10 }} key={index}>
                                <Text style={styles.boldStyle}>{i.title} :</Text>
                                <Text style={styles.boldStylebody}> {i.body} </Text>
                            </Text>
                        ))
                    }

                    <Text>[I refer to the patient here.]</Text>

                    {
                        data2.map((i, index) => (
                            <Text style={{ paddingVertical: 10 }} key={index}>
                                <Text style={styles.boldStylebody}>{index + 1} .</Text>
                                <Text style={styles.boldStylebody}> {i.title} </Text>
                            </Text>
                        ))
                    }
                    <Text style={styles.boldStylebody}>15.
                        <Text style={styles.boldStylebody}>By signing this document, I acknowledge:</Text>
                    </Text>

                    {
                        data3.map((i, index) => (
                            <Text style={{ paddingVertical: 10, paddingHorizontal: 10 }} key={index}>
                                <Text style={styles.boldStylebody}> {i.text} </Text>
                            </Text>
                        ))
                    }

                    <View>
                        <Text style={{ marginVertical: 1, fontSize: 16 }}>I have read and understand the information provided above regarding Telemedicine, have discussed it with my Doctor, and I hereby give informed consent to the use of Telemedicine.</Text>
                        <Text style={{ marginVertical: 1, fontSize: 16 }}>I understand that I can withdraw the consent at any time and that will not affect any of my future treatment procedures.</Text>
                        <Text style={{ marginVertical: 1, fontSize: 16 }}>I understand that all the laws that are protecting my privacy of medical history or information are also applied to telemedicine practices.</Text>
                        <Text style={{ marginVertical: 1, fontSize: 16 }}>I understand that I can be charged the additional fees that my insurance does not cover.</Text>
                        <Text style={{ marginVertical: 1, fontSize: 16 }}>I accept that I authorize health care professionals and use telemedicine for my treatment and diagnosis.</Text>
                    </View>

                </ScrollView>
            </SafeAreaView>
        </Modal>
    )
}

export default PatientConsent

const styles = StyleSheet.create({
    headerCenter: {
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center',
        textDecorationLine: 'underline',
        paddingVertical: 5
    },
    boldStylebody: {
        fontSize: 16
    },
    modal: {
        justifyContent: "center",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("2%"),
    },
    boldStyle: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 16,
        paddingVertical: 5
    },
    modalContainer: {
        flex: 1,
    },
    modalTextHeader: {
        fontSize: 24,
        marginTop: 27,
        marginBottom: 6,
        color: "#041A32",
    },

    modalTextSubHeader: {
        fontSize: 14,
        color: "#041A32",
        marginTop: 20,
        marginBottom: 6,
    },
    modalText: {
        fontSize: 13,
        color: "#4E5C76",
    },
    modal1Close: {
        alignItems: "center",
        justifyContent: "center",
    },
    modal2Close: {
        alignItems: "center",
        justifyContent: "center",
    },
    modalheader: {
        height: hp("9%"),
        flexDirection: "row",
        alignItems: "center",
    },
    modalheaderBackIcon: {
        flex: 1,
        fontSize: hp("3.5%"),
        color: "#1B80F3",
        marginLeft: wp("5%"),
    },
    modalheading: {
        flex: 8,
        color: "#041A32",
        fontSize: hp("2.9%"),
    },

})
