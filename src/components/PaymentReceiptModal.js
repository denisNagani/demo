import { View, Text, FlatList, StyleSheet, TouchableOpacity, Platform, SafeAreaView, Image, TextInput } from 'react-native'
import React, { useEffect, useState } from 'react'
import WebView from 'react-native-webview'
import { useDispatch } from 'react-redux'
import { hide, show } from '../utils/loader/action'
import { isValid } from '../utils/ifNotValid'
import HeaderSehet from '../components/header-common'
import { downloadPdfFromBase64, successPayload } from '../utils/helper'
import { showModal } from '../actions/modal'
import { requestStoragePermission } from '../scenes/patient/upcoming-past-bookings/components/checkPermission'

const PaymentReceiptModal = (props) => {
    const dispatch = useDispatch()

    let url = props?.navigation?.state?.params?.url

    useEffect(() => {
        if (isValid(url)) {
            dispatch(show())
        }
    }, [url])

    useEffect(() => {
        requestStoragePermission(status => console.log('permission status ', status))
    }, [])


    return (
        <SafeAreaView style={{ flex: 1 }}>
            <HeaderSehet headerText={"Payment Receipt"} navigation={props?.navigation} changeTheme={true} />
            <WebView
                style={{ flex: 1 }}
                source={{ uri: url }}
                onLoadEnd={() => dispatch(hide())}
                onError={() => dispatch(hide())}

                javaScriptEnabled={true}
                domStorageEnabled={true}
                allowFileAccess={true}
                allowUniversalAccessFromFileURLs={true}
                allowFileAccessFromFileURLs={true}
                allowingReadAccessToURL={true}
                mixedContentMode={'always'}
                javaScriptCanOpenWindowsAutomatically={true}
                onMessage={(event) => {
                    const data = JSON.parse(event?.nativeEvent?.data);
                    console.log('data ', data);
                    if (data) {
                        let uri = data?.base64
                        let baseUriSplit = uri?.split("data:application/pdf;base64,")
                        let baseUri = baseUriSplit?.[1]

                        downloadPdfFromBase64(baseUri)
                            .then((result) => {
                                if (isValid(result)) {
                                    dispatch(showModal(successPayload("PDF Downloaded Successfully")));
                                }
                            })
                            .catch(err => {
                                console.log('err ', err);
                            })
                    }
                }}
            />
        </SafeAreaView>
    )
}
const styles = StyleSheet.create({
})

export default PaymentReceiptModal