import { View, Text, FlatList, StyleSheet, TouchableOpacity, Platform, SafeAreaView } from 'react-native'
import React, { useState } from 'react'
import { BottomModal } from 'react-native-modals'
import { useBackHandler } from '../hooks/usebackhandler'
import { store } from '../store'
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_SF_PRO_SemiBold } from '../styles/typography'
import { setPhotoPickerModal } from '../actions/modal'

const PhotoPickerModal = ({ filter, visible, onPress }) => {

    const closeModal = () => {
        store.dispatch(setPhotoPickerModal(false))
    }

    if (Platform.OS == 'android') {
        useBackHandler(closeModal, visible)
    }

    const FilterItem = ({ item }) => {
        const onPressFilterItem = () => {
            closeModal()
            onPress(item)
        }

        return <TouchableOpacity
            style={styles.filterCont}
            onPress={onPressFilterItem}
        >
            <Text style={styles.filterText}>{item?.title}</Text>
        </TouchableOpacity>
    }

    return (
        <BottomModal
            visible={visible}
            onTouchOutside={closeModal}
            width={1}
        >
            <View style={styles.rowStyle} />

            <Text style={styles.sortByStyle}>Select Method</Text>
            <SafeAreaView>
                <FlatList
                    style={{ marginVertical: 3, marginBottom: 10 }}
                    keyExtractor={(item, index) => index.toString()}
                    data={filter}
                    renderItem={({ item }) => {
                        return <FilterItem item={item} />
                    }}
                />
            </SafeAreaView>

        </BottomModal>
    )
}
const styles = StyleSheet.create({
    rowStyle: {
        paddingHorizontal: 25,
        paddingVertical: 3,
        backgroundColor: '#000',
        alignSelf: 'center',
        borderRadius: 5,
        marginVertical: 5
    },
    sortByStyle: {
        fontSize: 18,
        fontFamily: FONT_FAMILY_SF_PRO_BOLD,
        color: '#000',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    filterText: {
        fontSize: 16,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        color: '#000',
        flex: 1,
    },
    filterCont: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 15,
    },
    radioStyle: {
        borderRadius: 50,
        borderWidth: 1,
        borderColor: 'red',
    }
})

export default PhotoPickerModal