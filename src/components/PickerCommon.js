import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { Icon, Picker } from "native-base";
import Images from "../assets/images";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_14 } from "../styles/typography";

const PickerCommon = ({ style, title, value = "", data = [], onDropValueChange, key }) => (
    <View style={[style]}>
        <Text style={styles.titleStyle_}>{title}</Text>
        <View style={[styles.boxStyleAmount, { height: 45 }]}>
            <Picker
                mode="dropdown"
                iosIcon={<Icon type="MaterialIcons" name="keyboard-arrow-down" />}
                selectedValue={value}
                placeholder={"select"}
                onValueChange={(value) => {
                    if (value != "0")
                        onDropValueChange(value)
                }}>
                <Picker.Item label="Select" value="0" />
                {data.map((obj) => <Picker.Item label={obj.label} value={obj.value} />)}
            </Picker>
            {Platform.OS === 'android' ?
                <Image source={Images.dropDown}
                    style={[{ right: 16, top: 13, width: 10, height: 20, position: 'absolute' }]}
                /> : null}
        </View>
    </View>
);
export default PickerCommon;

const styles = StyleSheet.create({
    titleStyle_: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: 'black',
        fontSize: FONT_SIZE_14
    },
    boxStyleAmount: {
        paddingHorizontal: 1, paddingVertical: 1, width: '100%', justifyContent: 'center',
        marginTop: 4, borderRadius: 9, borderColor: '#B6BECB', borderWidth: 0.6,
    },
})
