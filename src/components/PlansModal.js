import React, { useState, useEffect } from 'react'
import { TextInput, Text, ScrollView, Image, KeyboardAvoidingView, View, TouchableOpacity } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import Images from '../assets/images'
import Modal, { ModalContent } from "react-native-modals";
import { setApplyCouponModal, setSelectPlanModal } from '../actions/sehet/speciality-action';
import { FONT_FAMILY_REGULAR, FONT_FAMILY_SF_PRO_BOLD } from '../styles/typography';
import { checkConsulCount } from '../actions/coupn';
import { isValid } from '../utils/ifNotValid';
import { showModal } from '../actions/modal';
import { errorPayload } from '../utils/helper';

const PlansModal = ({ visible }) => {
    const dispatch = useDispatch()
    const subPlans = useSelector(state => state.appointment.subPlans)
    const plans = subPlans?.plans

    const [selectedPlanId, setSelectedPlanId] = useState('')
    const [allPlans, setAllPlans] = useState([])

    useEffect(() => {
        setAllPlans(plans)
    }, [plans])

    const onpresssubmit = () => {
        if (isValid(selectedPlanId)) {
            dispatch(checkConsulCount(subPlans?.couponData, selectedPlanId))
        } else {
            dispatch(showModal(errorPayload("Please select any plan")))
        }
    }

    const onPressPlan = (item) => {
        setSelectedPlanId(item?.id?._id)
        const newPlans = plans?.map(plan => {
            if (plan?.id?._id == item?.id?._id) {
                plan.selected = true
            } else {
                plan.selected = false
            }
            return plan
        })
        setAllPlans(newPlans)
    }

    return (
        <Modal
            width={0.9}
            style={{ flex: 1 }}
            visible={visible}
            onTouchOutside={() => {
                dispatch(setSelectPlanModal(false))
            }}
            rounded
            actionsBordered
            onSwipeOut={() => {
                dispatch(setSelectPlanModal(false))
            }}
            onHardwareBackPress={() => {
                dispatch(setSelectPlanModal(false))
                return true
            }}
        >
            <ModalContent style={{ backgroundColor: '#fff', }}>
                <View>

                    <Image
                        resizeMode="stretch"
                        style={{ height: 200, width: '100%' }}
                        source={Images.amar_health_card}
                    />
                    <Text style={{ marginHorizontal: 10, marginTop: 20, color: '#041A32', fontSize: 16, fontFamily: FONT_FAMILY_SF_PRO_BOLD }}>Select Your Membership Plan *</Text>
                    {
                        allPlans.map((item, index) => (
                            <TouchableOpacity
                                style={{
                                    flexDirection: 'row',
                                    alignItems: 'center'
                                }}
                                onPress={() => onPressPlan(item)}
                            >
                                <View style={{
                                    padding: 10,
                                    borderRadius: 50,
                                    marginHorizontal: 10,
                                    marginVertical: 10,
                                    borderWidth: 2,
                                    borderColor: '#038BEF',
                                    backgroundColor: item?.selected ? '#038BEF' : 'white'
                                }}></View>
                                <Text>{item?.id?.name}</Text>
                            </TouchableOpacity>
                        ))
                    }

                    <TouchableOpacity
                        style={{
                            backgroundColor: 'red',
                            padding: 10,
                            borderRadius: 8,
                            marginVertical: 10
                        }}
                        onPress={onpresssubmit}
                    >
                        <Text
                            style={{
                                color: 'white',
                                fontSize: 16,
                                textAlign: 'center',
                                fontFamily: FONT_FAMILY_REGULAR,
                                fontWeight: 'bold'
                            }}
                        >Submit</Text>
                    </TouchableOpacity>
                </View>
            </ModalContent>
        </Modal>
    )
}

export default PlansModal
