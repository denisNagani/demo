import React from 'react'
import { Text } from 'react-native'
import { useSelector } from 'react-redux'

const PowerTranslator = ({ style, enText, hinText, numberOfLines }) => {
    const lang = useSelector(state => state.profile.selected_language)

    return (
        <Text
            numberOfLines={numberOfLines}
            style={[{}, style]}
        >
            {lang == 'hin' ? hinText : enText}
        </Text>
    )
}

export default PowerTranslator
