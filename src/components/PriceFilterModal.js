import React, { useState } from 'react'
import { View, Text, FlatList, StyleSheet, TouchableOpacity, Platform, SafeAreaView } from 'react-native'
import { BottomModal } from 'react-native-modals'
import { store } from '../store/index'
import { Icon } from 'native-base'

const PriceFilterModal = ({ visible, data, closeModal, onItemPressCb }) => {

    const FilterItem = ({ item }) => {

        const onPressFilterItem = () => {
            onItemPressCb(item)
        }

        return <TouchableOpacity style={styles.filterCont} onPress={onPressFilterItem}>
            <Text style={styles.filterText}>{item?.title}</Text>
            {item?.selected && <Icon type='Feather' name='check' style={{ color: 'red', fontSize: 24, padding: 3 }} />}
        </TouchableOpacity>
    }

    return (
        <BottomModal
            visible={visible}
            onTouchOutside={closeModal}
            width={1}
        >
            <View style={styles.rowStyle} />

            <Text style={styles.sortByStyle}>Filter By Price</Text>
            <SafeAreaView>
                <FlatList
                    style={{ marginVertical: 3 }}
                    keyExtractor={(item, index) => index.toString()}
                    data={data}
                    renderItem={({ item }) => {
                        return <FilterItem item={item} />
                    }}
                />
            </SafeAreaView>

        </BottomModal>
    )
}
const styles = StyleSheet.create({
    rowStyle: {
        paddingHorizontal: 25,
        paddingVertical: 3,
        backgroundColor: '#000',
        alignSelf: 'center',
        borderRadius: 5,
        marginVertical: 5
    },
    sortByStyle: {
        fontSize: 18,
        color: '#000',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    filterText: {
        fontSize: 16,
        color: '#000',
        flex: 1,
    },
    filterCont: {

        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 10,
        paddingHorizontal: 15,
    },
    radioStyle: {
        borderRadius: 50,
        borderWidth: 1,
    }
})

export default PriceFilterModal