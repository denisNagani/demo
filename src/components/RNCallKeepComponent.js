// import React, { useEffect } from 'react'
// import RNCallKeep from 'react-native-callkeep';
// import NavigationService from "../components/startScreen/RootNavigation";

// RNCallKeep.setup({
//     ios: {
//         appName: "App name",
//     },
//     android: {
//         alertTitle: "Permissions required",
//         alertDescription: "This application needs to access your phone accounts",
//         cancelButton: "Cancel",
//         okButton: "ok",
//     },
// }).then((accepted) => {
//     if (Platform.android) {
//         RNCallKeep.setActive(true);
//     }
// }).catch(err => {
//     console.log(err);
// });

// const RNCallKeepComponent = () => {

//     useEffect(() => {

//         startCalling()

//         RNCallKeep.addEventListener("answerCall", ({ callUUID, ...o }) => {
//             // to hide the native "accepted call" screen 
//             console.log("call answered.", callUUID);
//             RNCallKeep.rejectCall(callUUID);
//             NavigationService.navigate('Blog')
//         });
//         RNCallKeep.addEventListener("endCall", ({ callUUID }) => {
//             console.log("endCall.", callUUID);
//         });
//     }, [])

//     // const setup = () => {

//     //     const options = {
//     //         ios: {
//     //             appName: 'My app name',
//     //         },
//     //         android: {
//     //             alertTitle: 'Permissions required',
//     //             alertDescription: 'This application needs to access your phone accounts',
//     //             cancelButton: 'Cancel',
//     //             okButton: 'ok',
//     //             imageName: 'phone_account_icon',
//     //             // additionalPermissions: [PermissionsAndroid.PERMISSIONS.example],
//     //             // Required to get audio in background when using Android 11
//     //             foregroundService: {
//     //                 channelId: 'com.company.my',
//     //                 channelName: 'Foreground service for my app',
//     //                 notificationTitle: 'My app is running on background',
//     //                 notificationIcon: 'Path to the resource icon of the notification',
//     //             },
//     //         }
//     //     };

//     //     RNCallKeep
//     //         .setup(options)
//     //         .then(accepted => {
//     //             console.log("setup RNCALLKEEP " + accepted);
//     //         })
//     //         .catch(err => console.log("error in setup RNCALLKEEP" + err));
//     //     RNCallKeep.setAvailable(true); // Only used for Android, see doc above.

//     // }

//     const startCalling = () => {
//         displayIncomingCall("dr vaishnavi deskhmukh");
//         // alert(JSON.stringify(nav))
//     }

//     const displayIncomingCall = (number) => {
//         const callUUID = "123";
//         RNCallKeep.displayIncomingCall(callUUID, number, number, 'number', true);
//     };


//     return (
//         null
//     )
// }

// export default RNCallKeepComponent
