import React from 'react'
import { Modal, Text, StyleSheet, SafeAreaView, ScrollView, View } from 'react-native'
import { FONT_FAMILY_SF_PRO_BOLD } from './../styles/typography'
import { Icon } from 'native-base'

const Refund = ({ visible, onPress }) => {
    return (
        <Modal visible={visible} animationType="slide" onRequestClose={onPress}>
            <SafeAreaView style={styles.container}>
                <View style={{ height: '9%', flexDirection: 'row', alignItems: 'center' }}>
                    <Icon
                        name="close"
                        type="AntDesign"
                        style={{
                            // flex: 1,
                            fontSize: 28,
                            color: "#1B80F3",
                            marginRight: 5,
                        }}
                        onPress={onPress}
                    />
                    <Text style={styles.header}>Refund and Cancellation Policy</Text>
                </View>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={styles.bodyText}>Our focus is complete customer satisfaction. In the event, if you are displeased with the services provided, we will refund back the money, provided the reasons are genuine and proved after investigation. Please read the fine prints of each deal before buying it, it provides all the details about the services or the product you purchase.</Text>
                    <Text style={styles.bodyText}>In case of dissatisfaction from our services, clients have the liberty to cancel their projects and request a refund from us. Our Policy for the cancellation and refund will be as follows:</Text>

                    <Text style={styles.subHeader}>Cancellation Policy</Text>
                    <Text style={styles.bodyText}>For Cancellations please contact the us via contact us link.</Text>
                    <Text style={styles.bodyText}>Requests received later than 7 business days prior to the end of the current service period will be treated as cancellation of services for the next service period.</Text>

                    <Text style={styles.subHeader}>Refund Policy</Text>

                    <Text style={styles.bodyText}>We will try our best to create the suitable design concepts for our clients.</Text>

                    <Text style={styles.bodyText}>In case any client is not completely satisfied with our products we can provide a refund. </Text>

                    <Text style={styles.bodyText}>If paid by credit card, refunds will be issued to the original credit card provided at the time of purchase and in case of payment gateway name payments refund will be made to the same account.</Text>
                </ScrollView>
            </SafeAreaView>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 15
    },
    header: {
        fontSize: 21,
        // fontFamily: FONT_FAMILY_SF_PRO_BOLD,
        fontWeight: 'bold',
        color: '#000',
        textAlign: 'center',
        marginVertical: 20
    },
    subHeader: {
        fontSize: 18,
        // fontFamily: FONT_FAMILY_SF_PRO_BOLD,
        fontWeight: 'bold',
        color: '#000',
    },
    bodyText: {
        fontSize: 14,
        marginVertical: 10
    }
})

export default Refund
