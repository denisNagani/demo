import React from "react";
import { StyleSheet, Text, TouchableOpacity } from "react-native";
import { Footer } from "native-base";
import { FONT_SIZE_15 } from "../styles/typography";
import { strings } from "../utils/translation";

const SaveCancelFooter = ({ style, disableSave = false, onSaveClick, onCancelClick, leftText, rightText }) => (
    <Footer style={[styles.buttonContainer, style]}>
        <TouchableOpacity onPress={onSaveClick} disabled={disableSave}
            style={[styles.buttonStyle, { backgroundColor: disableSave ? '#b5b5b5' : '#FF5E38' }]}>
            <Text uppercase={false} style={[styles.buttonTxtStyle, { color: 'white' }]}>{leftText ? leftText : `${strings.save}`}</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={onCancelClick} style={styles.buttonStyle}>
            <Text uppercase={false} style={styles.buttonTxtStyle}>{rightText ? rightText : strings.cancel}</Text>
        </TouchableOpacity>
    </Footer>
);
export default SaveCancelFooter;
const styles = StyleSheet.create({
    buttonTxtStyle: {
        color: '#A0A9BE',
        padding: 0,
        margin: 0,
        textAlign: 'center',
        fontSize: FONT_SIZE_15
    },
    footer: {
        backgroundColor: 'white',
        margin: 10,
    },
    buttonContainer: {
        backgroundColor: 'white',
        marginTop: 10,
        paddingHorizontal: 10,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    buttonStyle: {
        borderRadius: 6,
        backgroundColor: '#EBEFFA',
        padding: 10,
        flex: 1,
        marginTop: 5,
        marginBottom: 8,
        marginHorizontal: 5,
    },
})
