import React, { useState } from 'react'
import { TouchableOpacity, Image, Share, Platform, Text } from 'react-native'
import Images from '../assets/images'
import { Icon } from 'native-base'
import { strings } from '../utils/translation'
import { appName, appStoreAppLink, playStoreAppLink } from '../config'

const ShareCommon = () => {

    const [appLinks, setAppLinks] = useState({
        iosLink: appStoreAppLink,
        androidLink: playStoreAppLink
    })

    const shareApp = async () => {

        let url = Platform.OS === "ios" ? appLinks.iosLink : appLinks.androidLink;
        // Linking.openURL(url)
        try {
            const result = await Share.share({
                title: 'App link',
                message: `${appName} ${url}`,
                url: url
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                    console.log("activityType " + activityType);
                } else {
                    // shared
                    console.log("shared");
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
                console.log("dismissed");
            }
        } catch (error) {
            console.log(error.message);
        }
    }

    return (
        <TouchableOpacity
            // activeOpacity={1}
            onPress={shareApp}
            style={{
                width: '100%',
                flexDirection: 'row',
                alignItems: 'center'
            }}
        >
            <Image style={{
                height: 20,
                width: 20,
                resizeMode: 'contain'
            }}
                source={Images.ShareRed}

            />
            {/* <Icon
                name="sharealt"
                type="AntDesign"
                style={{
                    fontSize: 25,
                    color: '#68a2f9'
                }}
            /> */}
            <Text style={{ color: "#484F5F", fontSize: 15, marginLeft: 8 }}>{strings.Share}</Text>
        </TouchableOpacity>
    )
}

export default ShareCommon
