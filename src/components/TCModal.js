import { View, Text, Modal, SafeAreaView, ScrollView } from 'react-native'
import React from 'react'
import { Icon, Button } from 'native-base'
import styles from "../scenes/patient/upcoming-past-bookings/style";
import { useDispatch, useSelector } from 'react-redux';
import { setTcModal, showModal } from '../actions/modal';
import { methodLog, recordError } from '../utils/fireLog';
import { hide, show } from '../utils/loader/action';
import { setCallHeader, setSelectedAppointment, updateAppointmentPatient } from '../actions/bookings';
import database from "@react-native-firebase/database";
import { addUserLogToFirebase, checkUserProfile } from '../utils/helper';
import { JOINED, WAITING } from '../utils/constant';
import { ifValid } from '../utils/ifNotValid';
import { useNavigation } from '@react-navigation/native';
import NavigationService from '../components/startScreen/RootNavigation'
import { appName } from '../config';

const TCModal = () => {
    const dispatch = useDispatch()

    const tcModal = useSelector(state => state.modal.tcModal)
    const userProfile = useSelector(state => state.profile.userProfile)

    const onClosePress = () => {
        dispatch(setTcModal({
            visible: false
        }))
    }

    const _tAndc = (id, roomName, doctor, appointmentDate, intakeInfo) => {
        methodLog("join call")
        let payload;
        if (roomName === null || roomName === undefined) {
            payload = { ...payload, text: "room url not found" }
            dispatch(showModal(payload));
            return;
        }
        dispatch(show());
        dispatch(setSelectedAppointment(id))
        dispatch(setCallHeader(
            doctor !== undefined ? `${doctor.fullName}` : ""
        ))

        let connect = true;
        try {
            database()
                .ref(`/doctor/${id}`)
                .on("value", (snapshot) => {
                    console.log(JSON.stringify(snapshot));
                    if (snapshot.val() !== null && snapshot.val().status === "APPROVED") {
                        if (connect) {
                            connect = false;
                            dispatch(hide());
                            dispatch(setTcModal({
                                ...tcModal,
                                visible: false
                            }))
                            // let { fullName, image } = checkUserProfile(userProfile);
                            let fullName = intakeInfo?.name
                            // add status joined
                            addUserLogToFirebase(id, { event: JOINED, name: `${fullName} (Patient)` }).then(r => {
                                // move if already approved to call.
                                addUserInList(id).then((r) => redirect(roomName, id));
                            })

                        }
                    } else {
                        if (connect) {
                            connect = false;
                            let obj = { [id]: { status: "WAITING" } };
                            //// put user in waiting list.
                            database()
                                .ref("/doctor")
                                .update(obj)
                                .then((obj) => {
                                    let { fullName, image } = checkUserProfile(userProfile);
                                    /// put user log in array
                                    addUserLogToFirebase(id, {
                                        event: WAITING,
                                        name: `${fullName} (Patient)`
                                    }).then(r => {
                                        //// put user details in list.
                                        addUserInList(id).then((_) => {
                                            dispatch(hide());
                                            dispatch(setTcModal({
                                                ...tcModal,
                                                visible: false
                                            }))
                                            dispatch(updateAppointmentPatient(
                                                id,
                                                null,
                                                "WAITING",
                                                roomName,
                                                doctor
                                            ));
                                        });
                                    })

                                });
                        }
                    }
                });
        } catch (e) {
            recordError(e)
        }
    };

    const addUserInList = async (id) => {
        methodLog("addUserInList")
        // let { fullName, image } = checkUserProfile(userProfile);
        let fullName = tcModal?.intakeInfo?.name
        let image = ""
        console.log(fullName + image);
        await database()
            .ref(`/users/${id}`)
            .update({ patient: { fullName: ifValid(fullName) ? fullName : '', image: ifValid(image) ? image : '' } })
            .then((obj) => {
            });
    };

    const redirect = (roomName, id) => {
        methodLog("redirect")
        NavigationService.navigate("JoinCall", {
            channel: roomName,
            user: "PATIENT_DIRECT",
            bookingId: id,
        });
    };

    return (
        <Modal visible={tcModal?.visible} animationType="slide">
            <SafeAreaView style={styles.modalContainer}>
                <View style={styles.modalheader}>
                    <Icon
                        name="close"
                        type="AntDesign"
                        style={styles.modalheaderBackIcon}
                        onPress={onClosePress}
                    />
                    <Text style={styles.modalheading}>
                        Consent For Telehealth Consultation
                    </Text>
                </View>
                <ScrollView contentContainerStyle={styles.modal}>
                    <Text style={styles.modalText}>1. I understand that my Physical recommends engaging in
                        telehealth services with me to provide treatment</Text>
                    <Text style={styles.modalText}>2. I understand this is out of necessity and an abundance of
                        caution and has originated due to the Coronavirus (Covid-19) pandemic. This will
                        continue until such time that we are able to meet in person, or could continue,
                        depending on the particular circumstance.</Text>
                    <Text style={styles.modalText}>3. I understand that telehealth treatment has potential
                        benefits including, but not limited to, easier access to care.</Text>
                    <Text style={styles.modalText}>4. I understand that telehealth has been found to be
                        effective in treating a wide range of disorders, and there are potential benefits
                        including, but not limited to easier access to care. I understand; however, there is no
                        guarantee that all treatment of all patients will be effective.</Text>
                    <Text style={styles.modalText}>5. I understand that it is my obligation to notify my
                        Physical of my location at the beginning of each treatment session. If for some reason,
                        I change locations during the session, it is my obligation to notify my [insert
                        discipline] of the change in location.</Text>
                    <Text style={styles.modalText}>6. I understand that it is my obligation to notify my
                        Physical of any other persons in the location, either on or off camera and who can hear
                        or see the session. I understand that I am responsible to ensure privacy at my location.
                        I will notify my [insert discipline] at the outset of each session and am aware that
                        confidential information may be discussed.</Text>
                    <Text style={styles.modalText}>7. I understand that it is my obligation to ensure that any
                        virtual assistant artificial intelligence devices, including but not limited to Alexa or
                        Echo, will be disabled or will not be in the location where information can be
                        heard.</Text>
                    <Text style={styles.modalText}>8. I agree that I will not record either through audio or
                        video any of the session, unless I notify my Physical and this is agreed upon.</Text>
                    <Text style={styles.modalText}>9. I understand there are potential risks to using telehealth
                        technology, including but not limited to, interruptions, unauthorized access, and
                        technical difficulties. I understand
                        some of these technological challenges include issues with software, hardware, and
                        internet connection which may result in interruption.</Text>
                    <Text style={styles.modalText}>10. I understand that my [insert discipline] is not
                        responsible for any technological problems of which my Physical has no control over. I
                        further understand that my [insert discipline] does not guarantee that technology will
                        be available or work as expected.</Text>
                    <Text style={styles.modalText}>11. I understand that I am responsible for information
                        security on my device, including but not limited to, computer, tablet, or phone, and in
                        my own location.</Text>
                    <Text style={styles.modalText}>12. I understand that my [insert discipline] or I (or, if
                        applicable, my guardian or conservator), can discontinue the telehealth consult/visit if
                        it is determined by either me or my [insert discipline] that the videoconferencing
                        connections or protections are not adequate for the situation.</Text>
                    <Text style={styles.modalText}>13. I have had a conversation with my [insert discipline],
                        during which time I have had the opportunity to ask questions concerning services via
                        telehealth. My questions have been answered, and the risks, benefits, and any practical
                        alternatives have been discussed with me.</Text>
                    <Text style={styles.modalText}>14. {appName} is the technology service we will
                        use to conduct telehealth videoconferencing appointments. My [insert discipline] has
                        discussed the use of this platform. Prior to each session, I will receive an email link
                        to enter the “waiting room” until the session begins. There are no passwords or log in
                        required.</Text>

                    <Text style={styles.modalTextSub}>By signing this document, I acknowledge:</Text>

                    <Text style={styles.modalText}>1. Pathway Healthcare App is NOT an emergency service. In the
                        event of an emergency, I will use a phone to call 9-1-1 and/or other appropriate
                        emergency contact.</Text>
                    <Text style={styles.modalText}>2. I recognize my [insert discipline] may need to notify
                        emergency personnel in the event he/she feels there is a safety concern, including but
                        not limited to, a risk to self/others or my Physical is concerned that immediate medical
                        attention is needed.</Text>
                    <Text style={styles.modalText}>3. Though my [insert discipline] and I may be in virtual
                        contact through telehealth services, neither [Name of the Telehealth Service to be used]
                        or my [insert discipline] provides any medical or emergency or urgent healthcare
                        services or advice. I understand should medical services be required, I will contact my
                        physician. If emergency services are needed, I understand I should call 9-1-1.</Text>
                    <Text style={styles.modalText}>4. The {appName} facilitates videoconferencing
                        and this technology platform is not, itself, a source of healthcare, medical advice, or
                        care.</Text>
                    <Text style={styles.modalText}>5. I understand that the same fee rates apply for telehealth
                        as apply for in-person treatment. Some insurers are waiving co-pays during this time. It
                        is my obligation to contact my insurer before engaging in telehealth to determine if
                        there are applicable co-pays or fees which I am responsible for. Insurance or other
                        managed care providers may not cover telehealth sessions. I understand that if my
                        insurance, HMO, third-party payor, or other managed care provider do not cover the
                        telehealth sessions, I will be solely responsible for the entire fee of the
                        session.</Text>
                    <Text style={styles.modalText}>6. During these times of the impact of Coronavirus (Covid-19)
                        my [insert discipline] may not have access to all of my medical/treatment records. My
                        [insert discipline] has made reasonable efforts to obtain records, but I understand and
                        agree this may not be reasonably possible.</Text>
                    <Text style={styles.modalText}>7. To maintain confidentiality, I will not share my
                        telehealth appointment link or information with anyone not authorized to attend the
                        session.</Text>
                    <Text style={styles.modalText}>8. I understand that either I or my [insert discipline] can
                        discontinue the telehealth services if those services do not appear to benefit me
                        therapeutically or for other reasons which will be explained to me. I understand there
                        may be no other treatment alternative available.</Text>
                    <Text style={styles.modalText}>6. During these times of the impact of Coronavirus (Covid-19)
                        my [insert discipline] may not have access to all of my medical/treatment records. My
                        [insert discipline] has made reasonable efforts to obtain records, but I understand and
                        agree this may not be reasonably possible.</Text>
                    <Text style={styles.modalTextSub}>I have read and understand the information provided above
                        regarding telehealth, have discussed it with my physician, and I hereby give informed
                        consent to the use of telehealth.</Text>

                </ScrollView>
                <View style={{ width: '100%' }}>
                    <Button
                        primary
                        onPress={() =>
                            _tAndc(
                                tcModal?.appointmentId,
                                tcModal?.doctor?.roomName,
                                tcModal?.doctor,
                                tcModal?.appointmentDate,
                                tcModal?.intakeInfo
                            )
                        }
                        style={styles.modal1Close}
                    >
                        <Text uppercase={false} style={{ color: "#fff" }}>
                            Accept & Continue
                        </Text>
                    </Button>
                </View>
            </SafeAreaView>
        </Modal>
    )
}

export default TCModal