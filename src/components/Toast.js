import { Toast } from "native-base";

const _successToast = (msg) => {
	Toast.show({
		text: msg,
		type: "success",
		position: "top",
		duration: 3000,
		style: {
			width: "97%",
			alignSelf: "center",
			borderRadius: 10,
			marginTop: 10,
		},
	});
};

const _failureToast = (msg) => {
	Toast.show({
		text: msg,
		type: "danger",
		position: "top",
		duration: 3000,
		style: {
			width: "97%",
			alignSelf: "center",
			borderRadius: 10,
			marginTop: 10,
		},
	});
};

const _failureToastWithoutOk = (msg) => {
	Toast.show({
		text: msg,
		type: "danger",
		position: "top",
		duration: 3000,
		style: {
			width: "97%",
			alignSelf: "center",
			borderRadius: 10,
			// marginTop: 10,
		},
	});
};

export { _successToast, _failureToast, _failureToastWithoutOk };
