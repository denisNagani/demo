import {TouchableOpacity} from "react-native";
import React from "react";
import {Icon} from "native-base";

const TouchableIcon = ({icon, style, onPress}) => {
    return <TouchableOpacity onPress={onPress}>
        <Icon name={icon} type="AntDesign" style={style}/>
    </TouchableOpacity>
}
export default TouchableIcon;
