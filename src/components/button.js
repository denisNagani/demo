import { Button, Text } from 'native-base';
export default ButtonComponent = () => (
    <Button block danger>
        <Text>Danger</Text>
    </Button>
);
