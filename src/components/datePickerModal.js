import DateTimePickerModal from "react-native-modal-datetime-picker";
import React from "react";

const DatePickerModal = ({isVisible, onConfirm, onCancel, maximumDate, minimumDate}) => (
    <DateTimePickerModal
        isVisible={isVisible}
        mode="date"
        onConfirm={(date) => onConfirm(date)}
        onCancel={onCancel}
        maximumDate={maximumDate}
        minimumDate={minimumDate}
    />
);

export default DatePickerModal;