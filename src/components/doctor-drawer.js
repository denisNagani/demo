import React, { useState } from "react";
import { Text } from "native-base";
import { Alert, Image, Linking, SafeAreaView, ScrollView, TouchableOpacity, View } from "react-native";
import Line from "../components/line";
import Images from "../assets/images";
import styles from "../styles/common";
import common from "../styles/common";
import { useDispatch, useSelector } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import ProfileNameImg from "./profile-name-img";
import { showModal } from "../actions/modal";
import { callNumber } from "../utils/helpline-no";
import { successLog } from "../utils/fireLog";
import { setUpcomingPastActive } from "../scenes/patient/upcoming-past-bookings/components/action";
import { USER_LOGOUT } from "../utils/constant";
import { facebookLink, instagramLink, twitterLink } from "../utils/links";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { ClearFcmToken } from "../actions/sehet/user-action";
import { ifValid } from "../utils/ifNotValid";
import { setGetStarted, setAppLangugae } from "../services/auth";
import { getContactNumber } from "../services";
import { strings, changeLaguage } from "../utils/translation";
import { Set_App_Language } from "../actions/profile";
import { Switch } from "react-native-switch";
import ShareCommon from "./ShareCommon";
import moment from 'moment'
import { UpdateChatStatus } from "../actions/chat";
import DeviceInfo from 'react-native-device-info'

const DoctorDrawerBar = ({ navigation }) => {
    const dispatch = useDispatch();
    let userProfile = useSelector((state) => state.profile.userProfile);
    const lang = useSelector(state => state.profile.selected_language)

    const statelang = lang == "hin" ? false : true
    const [switchState, setSwitchState] = useState(statelang)

    let doctorMenu = [
        {
            item: `${strings.dash_board}`,
            navigateTo: "DashboardScreen",
            image: Images.icon_plus,
        },
        {
            item: `${strings.my_appointments}`,
            navigateTo: "UpcomingPastBookingsDoctor",
            image: Images.MyAppointments,
        },
        {
            item: `${strings.Appointmentslots}`,
            navigateTo: "Appointmentslots",
            image: Images.calenderBlue,
        },
        {
            item: `${strings.help_center}`,
            navigateTo: "contact",
            image: Images.icon_help,
        },
        {
            item: `${strings.about_us}`,
            navigateTo: "about",
            image: Images.AboutUsRed,
        },
        {
            item: `${strings.FAQ}`,
            navigateTo: "FAQ_doctor",
            image: Images.FAQRed,
        },
        { item: "Share" },
        { item: `${strings.logout}`, navigateTo: "Land", image: Images.LogoutRed },
        { item: "Line" },
    ];

    const navigateToScreenPress = async (data) => {
        successLog(`navigateToScreenPress pressed ${data}`)
        const payload = {
            text: "You have logged out successfully!!",
            iconName: "checkcircleo",
            modalVisible: true,
            subText: "",
        };
        if (data === "Land") {
            dispatch(showModal(payload));
            AsyncStorage.clear();
            setGetStarted(true)
            setAppLangugae(lang)
        } else if (data === "UpcomingPastBookingsDoctor") {
            dispatch(setUpcomingPastActive("UPCOMING"))
        } else if (data === "help") {
            try {
                const number = await getContactNumber()
                callNumber(`+91 ${number}`);
            } catch (error) {
                callNumber("+91 9983497088");
            }
        }
        navigation.navigate(data);
        navigation.closeDrawer();
    };

    const checkLogout = () => {
        successLog(`logout pressed`);
        const deviceId = DeviceInfo.getUniqueId()
        Alert.alert(strings.logout, strings.are_you_sure, [
            {
                text: "Cancel",
                onPress: () => navigateToScreenPress("DashboardScreen"),
            },
            {
                text: "OK",
                onPress: async () => {
                    let dd = "Offline"
                    await dispatch(UpdateChatStatus(dd))
                    navigateToScreenPress("Land");
                    dispatch({ type: USER_LOGOUT });
                    const body = {
                        userId: ifValid(userProfile?.userId) ? userProfile?.userId?._id : null,
                        deviceId: deviceId
                    }
                    dispatch(ClearFcmToken(body))
                }
            },
        ]);
    };

    const changeLang = async (value) => {
        changeLaguage(value)
        dispatch(Set_App_Language(value))
        await setAppLangugae(value)
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <ProfileNameImg
                userProfile={userProfile}
                navigation={navigation}
                editNavigation={"ShowDoctorProfile"}
            />

            <View style={{ flex: 8 }}>
                <ScrollView>
                    {doctorMenu.map((o) => {
                        return o.item !== "Line" ? (
                            <TouchableOpacity
                                style={styles.drawerItemContainer}
                                onPress={
                                    o.navigateTo == "Land"
                                        ? checkLogout
                                        : () => navigateToScreenPress(o.navigateTo)
                                }
                            >
                                {
                                    o.item !== 'Share' ? <Image style={[styles.drawerImage, { resizeMode: 'contain', tintColor: '#D9251D' }]} source={o.image} />
                                        : <ShareCommon />
                                }
                                {o.item !== "Share" && <Text style={styles.drawerItemText}>{o.item}</Text>}
                            </TouchableOpacity>
                        ) : (
                            <Line style={styles.lineStyleDrawer} />
                        );
                    })}
                </ScrollView>
            </View>

            <View
                style={common.drawerFollowUsContainer}>

                <View style={{
                    flex: 1,
                    // backgroundColor: 'red',
                    position: 'absolute',
                    top: -30,
                    left: 0
                }}>
                    <Switch
                        value={switchState}
                        onValueChange={(val) => {
                            setSwitchState(val)
                            let lanToChange = val === true ? 'en' : 'hin'
                            changeLang(lanToChange)
                            // console.log(val)
                        }}
                        disabled={false}
                        activeText={'English'}
                        inActiveText={'हिंदी'}
                        activeTextStyle={{ color: 'white', fontWeight: 'bold', width: 60, textAlign: 'center' }}
                        inactiveTextStyle={{ color: 'white', fontWeight: 'bold', width: 60 }}
                        circleSize={40}
                        containerStyle={{ borderRadius: 0 }}
                        // barHeight={50}
                        circleBorderWidth={1}
                        backgroundActive={'#1B80F3'}
                        backgroundInactive={'#1B80F3'}
                        circleActiveColor={'white'}
                        circleInActiveColor={'white'}
                        renderInsideCircle={() => { }} // custom component to render inside the Switch circle (Text, Image, etc.)
                        changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
                        innerCircleStyle={{
                            alignItems: "center",
                            justifyContent: "center",

                        }} // style for inner animated circle for what you (may) be rendering inside the circle
                        outerCircleStyle={{
                            borderRadius: 25,
                            backgroundColor: '#1B80F3',
                            // borderWidth: 1
                        }} // style for outer animated circle
                        renderActiveText={true}
                        renderInActiveText={true}
                        switchLeftPx={2} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
                        switchRightPx={2} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
                        switchWidthMultiplier={2} // multipled by the `circleSize` prop to calculate total width of the Switch
                        switchBorderRadius={30} // Sets the border Rad
                    />
                </View>


                <Text style={{}}>{strings.follow_us} </Text>
                <View style={common.drawerFollowIcons}>
                    <TouchableOpacity style={{ marginLeft: -70 }} onPress={() => Linking.openURL(facebookLink)}>
                        <Image style={[styles.drawerImageSocial, { resizeMode: 'contain' }]} source={Images.facebookred} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => Linking.openURL(twitterLink)}>
                        <Image style={[styles.drawerImageSocial, { width: hp("4.6%"), resizeMode: 'contain' }]} source={Images.twitterred} />
                    </TouchableOpacity>
                    <TouchableOpacity style={{ marginRight: -70 }} onPress={() => Linking.openURL(instagramLink)}>
                        <Image style={[styles.drawerImageSocial, { resizeMode: 'contain' }]} source={Images.instargamred} />
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    );
};

export default DoctorDrawerBar;
