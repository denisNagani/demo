import { SafeAreaView, StyleSheet, Text, TouchableOpacity, View, Platform } from "react-native";
import React from "react";
import { Icon } from "native-base";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_18 } from "../styles/typography";
import { errorLog } from "../utils/fireLog";
import { ifNotValid } from "../utils/ifNotValid";
import { rowStyle } from "../styles/commonStyle";

const HeaderSehet = ({ navigation, navigateTo, headerText, rightIcon, onPress, onPressCustom, onRightIconPress, rightIconStyle, hideshadow, rightIconText, changeTheme }) => {
    let bgColor = '#141E46'
    let iconColor = '#FFF'

    return <SafeAreaView style={[styles.header,
    {
        shadowOffset: hideshadow ? { width: 0, height: 0 } : { width: 0, height: 1 },
        shadowOpacity: hideshadow ? 0 : 0.8,
        shadowRadius: hideshadow ? 0 : 2,
        shadowColor: hideshadow ? '#FFFFFF' : '#0000000B',
        elevation: hideshadow ? 0 : 5
    },
    {
        backgroundColor: changeTheme ? bgColor : "#FFFFFF",
    }
    ]}>
        <View style={styles.heading}>
            <Icon
                name="arrowleft"
                type="AntDesign"
                style={[styles.headerBackIcon, { color: changeTheme ? iconColor : "#041A32" }]}
                onPress={() => {
                    if (ifNotValid(onPressCustom)) {
                        if (navigateTo) {
                            navigation.navigate(navigateTo)
                        } else {
                            navigation.goBack()
                        }
                    } else {
                        onPressCustom()
                    }
                }}
            />
            <Text style={[styles.headerText, { color: changeTheme ? iconColor : "#041A32", }]}>{headerText}</Text>

            {rightIcon ?
                <TouchableOpacity
                    style={rowStyle}
                    onPress={() => {
                        try {
                            onRightIconPress()
                        } catch (e) {
                            errorLog(e)
                        }
                    }}>
                    <Icon name={rightIcon} type="AntDesign" style={rightIconStyle}
                        onPress={onRightIconPress} />
                    {rightIconText && <Text style={{ color: "#FF5E38", fontSize: 16, fontWeight: 'bold' }}>{rightIconText}</Text>}
                </TouchableOpacity> : null
            }
        </View>
    </SafeAreaView >
}
export default HeaderSehet;


const styles = StyleSheet.create({

    header: {
        height: Platform.OS === 'ios' ? hp("11%") : hp("10%"),
        justifyContent: "center",
    },
    heading: {
        width: '92%',
        flexDirection: "row",
        marginLeft: 18,
        alignItems: "center",
        justifyContent: "flex-start",
    },
    headerText: {
        flex: 1,
        fontSize: FONT_SIZE_18,
        marginLeft: wp("4%"),
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    headerBackIcon: {
        fontSize: hp("3.5%"),
    },

})
    ;
