import { SafeAreaView, ScrollView, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import React from "react";
import { Icon, Input } from "native-base";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_14, FONT_SIZE_18 } from "../styles/typography";
import Images from "../assets/images";

const HeaderSearchBar = ({ navigation, headerText, onChange, inputTextValue, inputTextPlaceholder, showFilter, showFilterCb }) => {
    if (headerText === undefined || headerText == null)
        headerText = ".."

    if (inputTextValue === undefined || inputTextValue === null)
        inputTextValue = ""

    if (inputTextPlaceholder === undefined || inputTextPlaceholder === null)
        inputTextPlaceholder = ""

    return <SafeAreaView style={styles.header}>
        <View style={styles.heading}>
            <TouchableOpacity onPress={() => navigation && navigation.goBack()}>
                <Icon
                    name="arrowleft"
                    type="AntDesign"
                    style={styles.headerBackIcon}
                />
            </TouchableOpacity>
            <Text style={[styles.headerText, showFilter && { flex: 1 }]}>{headerText}</Text>
            {showFilter && <TouchableOpacity onPress={showFilterCb} style={{ marginHorizontal: 10, }}>
                <Image source={Images.filter} resizeMode="contain" style={{ width: 23, height: 23, marginHorizontal: 10 }} />
            </TouchableOpacity>}
        </View>
        <View style={styles.headerSearch}>
            {/* <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}> */}
            <Input
                value={inputTextValue}
                placeholder={inputTextPlaceholder}
                placeholderTextColor="#041A32"
                style={[styles.searchBarText, {}]}
                // pattern={"[A-Za-z]"}
                onChangeText={(name) => onChange(name)}
            />
            {/* </ScrollView> */}
        </View>
    </SafeAreaView>
}
export default HeaderSearchBar;


const styles = StyleSheet.create({
    header: {
        height: hp("18%"),
        backgroundColor: "#141E46",
        justifyContent: "center",
        paddingHorizontal: 8,
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5,
    },
    heading: {
        flexDirection: "row",
        marginLeft: 18,
        alignItems: "center",
        justifyContent: "flex-start",
    },
    headerText: {
        color: "#FFF",
        fontSize: FONT_SIZE_18,
        marginLeft: wp("4%"),
    },
    headerBackIcon: {
        fontSize: hp("3.5%"),
        color: "#FFF",
    },
    headerSearch: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: wp("4%"),
        marginTop: 20,
        opacity: 0.80,
        marginHorizontal: "4%",
        backgroundColor: "#E5F3FE",
        borderRadius: hp("0.8%"),
    },
    searchBarText: {
        // backgroundColor: '#E5F3FE',
        color: "#041A32",
        height: 45,
        width: '100%',
        fontSize: FONT_SIZE_14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        borderRadius: 5
    },
    headerSearchIcon: {
        fontSize: 24,
        marginRight: 10,
        color: "white",
    },

})
    ;
