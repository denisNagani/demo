import {SafeAreaView, Text, TouchableOpacity, View} from "react-native";
import React from "react";
import styles from "../styles/common"
import {Icon} from "native-base";

const HeaderCommon = ({onPress}) => {
    return <TouchableOpacity onPress={onPress}>
        <SafeAreaView style={styles.header}>
            <Icon
                name="arrowleft"
                type="AntDesign"
                style={styles.headerBackIcon}
            />
            <Text style={styles.heading}>Attendees</Text>
        </SafeAreaView>
    </TouchableOpacity>
}
export default HeaderCommon;
