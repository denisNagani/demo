import ButtonComponent from './button';
import InputComponent from './input';

export default {
    ButtonComponent,
    InputComponent
}
