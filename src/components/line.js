import {View} from "react-native";
import React from "react";
import styles from "../styles/common"

const Line = ({lineColor, style}) => {
    return <View style={[styles.lineStyle, {borderBottomColor: lineColor}, style]}/>
}
export default Line;
