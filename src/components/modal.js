import React, {useEffect} from "react";
import Dialog, {DialogContent, SlideAnimation,} from "react-native-popup-dialog";
import {heightPercentageToDP as hp} from "react-native-responsive-screen";
import {hideModal, setClockTime} from "../actions/modal";
import {useDispatch, useSelector} from "react-redux";
import {Button, Icon, Text} from "native-base";
import {Dimensions, StyleSheet, View} from "react-native";
import {FONT_SIZE_15} from "../styles/typography";
import {successLog} from "../utils/fireLog";

let deviceWidth = Dimensions.get('window').width

const ModalComponent = (props) => {
    const modelState = useSelector(state => state.modal);
    const dispatch = useDispatch();
    let clockTime = modelState.clockTime;

    useEffect(() => {
        successLog('modelStatclockTime ' + clockTime)

        if (modelState.modalVisible === true){
            let token;
            if (clockTime === 0) {
                clearInterval(token)
                dispatch(hideModal())
                successLog('clockTime' + 'interval cleared')
                return;
            }

            successLog('running ' + clockTime)
            token = setTimeout(updateTime, 1000)
            return () => {
                // clearInterval(token)
                // dispatch(hideModal())
            };
        }
    }, [clockTime,modelState.modalVisible])


    function updateTime() {
        if (clockTime === 0) {
            //reset
            successLog('timer is zero')
        } else {
            successLog('modelState.clockTime ' + clockTime)
            dispatch(setClockTime(clockTime - 1));
        }
    }

    return (
        <View>
            <Dialog
                visible={modelState.modalVisible}
                dialogAnimation={
                    new SlideAnimation({
                        slideFrom: "bottom",
                    })
                }
                width={325}
                // height={250}
                overlayOpacity={0.5}
                onTouchOutside={() => dispatch(hideModal())}>
                <DialogContent style={{width: '100%',}}>
                    {modelState.isIcon !== false ? (
                        <Icon
                            name={modelState.iconName}
                            type="AntDesign"
                            onPress={() => dispatch(hideModal())}
                            style={[
                                styles.iconStyle,
                                {
                                    color:
                                        modelState.iconColor !== undefined
                                            ? modelState.iconColor
                                            : "#1B80F3",
                                },
                            ]}
                        />
                    ) : null}
                    <Text style={styles.text}>{modelState.text}</Text>
                    <Text note style={styles.subText}> {modelState.subText}</Text>

                    {modelState.button ?
                        <View style={{
                            marginLeft: deviceWidth / 4,
                            marginTop: 20,
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            <Button rounded bordered info onPress={() => dispatch(hideModal())}>
                                <Text uppercase={false}
                                      style={{color: '#00A2FF', padding: 0, margin: 0, fontSize: FONT_SIZE_15}}>Ok,
                                    Thanks!</Text>
                            </Button>
                        </View> : null}
                </DialogContent>
            </Dialog>
        </View>
    );
}

const styles = StyleSheet.create({
    iconStyle: {
        marginTop: 30,
        alignSelf: "center",
        fontSize: hp("10%"),
    },
    text: {
        color: "#041A32",
        marginTop: 30,
        width: "100%",
        alignSelf: "center",
        textAlign: "center",
    },
    subText: {
        alignSelf: "center",
        textAlign: 'center'
    },
});


export default ModalComponent;
