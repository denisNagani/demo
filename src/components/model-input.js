import Modal, {ModalContent} from "react-native-modals";
import {Button, Icon, Input, Item, Label, Text} from "native-base";
import styles from "../styles/common";
import {TextInput, View} from "react-native";
import React from 'react';
import Line from "../components/line";
import moment from "moment";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const InputModel = ({user, visible, onTouchOutside, onSwipeOut, value, onBackdropPress, text, desc, onContinue, onCancel, onChangeText, togglePicker, dob, handleConfirm, isDatePickerVisible}) => {

    return <View>
        <Modal
            width={0.9}
            visible={visible}
            onTouchOutside={onTouchOutside}
            rounded
            actionsBordered
            onBackdropPress={onBackdropPress}
            onSwipeOut={onSwipeOut}>
            <ModalContent style={{backgroundColor: '#fff'}}>
                <View style={{width: '100%', marginTop: 5}}>
                    {user === "GUEST" ?
                        <View>
                            <Text style={styles.successModalMainTxt}>Please enter your full name</Text>
                            <TextInput
                                style={{marginBottom: 1, color: 'black', fontSize: 17, paddingVertical: 0}}
                                autoFocus={true}
                                value={value}
                                onChangeText={onChangeText}
                            />
                        </View> : null
                    }
                    {
                        user === "PATIENT" ?
                            <>
                                <Text style={styles.successModalMainTxt}>Please enter your email</Text>
                                <TextInput
                                    style={{marginBottom: 1, color: 'black', fontSize: 17, paddingVertical: 0}}
                                    autoFocus={true}
                                    value={value}
                                    keyboardType='email-address'
                                    onChangeText={onChangeText}
                                />
                                <Line style={{marginBottom: 15}}/>

                                <Item
                                    onPress={(dob) => {
                                        togglePicker(dob)
                                    }}>
                                    <Label>Date of birth</Label>
                                    <Input
                                        style={{}}
                                        value={dob === "" ? "" : moment(dob).format("YYYY-MM-DD").toString()}
                                        disabled
                                    />
                                    <Icon
                                        name="calendar"
                                        type="Feather"
                                        style={styles.calIcon}
                                        onPress={togglePicker}
                                    />
                                </Item>

                            </> : null
                    }
                    <View>
                        {
                            user === "GUEST" ?
                                <Button disabled={value.length <= 0 } success block
                                                       style={{
                                                           backgroundColor: value.length > 0  ? "red" : '#A0A9BE',
                                                           marginTop: 20
                                                       }}
                                                       onPress={onContinue}>
                                    <Text createAccountText>Continue</Text>
                                </Button> :


                                <Button disabled={value.length <= 0 || dob === ""} success block
                                        style={{
                                            backgroundColor: value.length > 0 && dob !== "" ? "red" : '#A0A9BE',
                                            marginTop: 20
                                        }}
                                        onPress={onContinue}>
                                    <Text createAccountText>Continue</Text>
                                </Button>
                        }
                    </View>
                </View>
            </ModalContent>
        </Modal>

        <DateTimePickerModal
            isVisible={isDatePickerVisible}
            mode="date"
            onConfirm={handleConfirm}
            onCancel={togglePicker}
            maximumDate={new Date()}
        />
    </View>
}
export default InputModel;
