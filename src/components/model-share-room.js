import {Button, Container, Form, Icon, Input, Item, Label, Picker, Text} from "native-base";
import styles from "../styles/common";
import {ScrollView, View} from "react-native";
import React from 'react';
import moment from "moment";
import DatePickerModal from "./datePickerModal";
import Modal from 'react-native-modal';

// const ModelShareRoom = ({visible, gender, mobileNoError, onTouchOutside, value, text, desc, onContinue, onCancel, onChangeText, togglePicker, dob, handleConfirm, isDatePickerVisible}) => {
const ModelShareRoom = ({
                            name, onNameChange, email, onEmailChange, mobile, onMobileChange, dob = "", togglePickerDob, appointmentDate = "",
                            handleAppointmentConfirm, slots = [], selectedSlot, onSlotChange, isPickerVisibleDob, isPickerVisibleAppointment,
                            onContinue, onTouchOutside, visible, handleDobConfirm, togglePickerAppointment, onCancel,
                            selectedGender, onGenderChange
                        }) => {

    const validate = () => {

    }
    if (selectedSlot === null || selectedSlot === undefined)
        selectedSlot = ""

    return (
        <Container>
            <Modal
                isVisible={visible}
                onBackButtonPress={onTouchOutside}
                onBackdropPress={onTouchOutside}>
                <View style={{backgroundColor: '#fff', flex: 1, borderRadius: 10, paddingRight: 8}}>
                    <ScrollView
                        showsVerticalScrollIndicator={false}>
                        <Form style={{width: '100%', marginTop: 5}}>

                            <Item floatingLabel style={styles.paddingVertical4}>
                                <Label style={styles.labelColorModal}>Enter patient name</Label>
                                <Input
                                    value={name}
                                    onChangeText={(v) => onNameChange(v)}/>
                            </Item>

                            <Item floatingLabel style={styles.paddingVertical4}>
                                <Label style={styles.labelColorModal}>Enter patient email</Label>
                                <Input
                                    keyboardType={"email-address"}
                                    value={email}
                                    onChangeText={(v) => onEmailChange(v)}/>
                            </Item>

                            {/*error={mobileNoError !== null}>*/}
                            <Item floatingLabel style={styles.paddingVertical4}>
                                <Label style={styles.labelColorModal}>Enter patient mobile</Label>
                                <Input
                                    maxLength={10}
                                    keyboardType={"numeric"}
                                    value={mobile}
                                    onChangeText={(value) => onMobileChange(value)}
                                />
                            </Item>


                            <Item style={{marginTop: 15}}>
                                <Label style={[{flex: 1}, styles.labelColorModal]}>Select gender</Label>
                                <Picker mode="dropdown"
                                        iosIcon={<Icon name="arrow-down"/>}
                                        style={{width: undefined}}
                                        placeholder="Select your SIM"
                                        placeholderStyle={{color: "#bfc6ea"}}
                                        placeholderIconColor="#007aff"
                                        selectedValue={selectedGender}
                                        onValueChange={v => {
                                            if (v != "0")
                                                onGenderChange(v)
                                        }}>
                                    <Picker.Item label="Select" value="0"/>
                                    <Picker.Item label="MALE" value="MALE"/>
                                    <Picker.Item label="FEMALE" value="FEMALE"/>
                                </Picker>
                            </Item>


                            <Item
                                floatingLabel style={styles.paddingVertical4}>
                                <Label style={styles.labelColorModal}>Date of birth</Label>
                                <Input
                                    value={dob === "" ? "" : moment(dob).format("DD MMMM YYYY")}
                                    disabled
                                />
                                <Icon
                                    name="calendar"
                                    type="Feather"
                                    onPress={togglePickerDob}
                                    style={styles.calIcon}
                                />
                            </Item>


                            <Item
                                style={styles.paddingVertical4}
                                floatingLabel>
                                <Label style={styles.labelColorModal}>Appointment date</Label>
                                <Input
                                    value={appointmentDate === "" ? "" : moment(appointmentDate).format("DD MMMM YYYY")}
                                    disabled
                                />
                                <Icon
                                    name="calendar"
                                    type="Feather"
                                    style={styles.calIcon}
                                    onPress={togglePickerAppointment}
                                />
                            </Item>

                            <Item style={{marginTop: 15,}}>
                                <Label style={[{flex: 1}, styles.labelColorModal]}>Select Slot</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="arrow-down"/>}
                                    style={{width: undefined}}
                                    placeholderStyle={{color: "#bfc6ea"}}
                                    placeholderIconColor="#007aff"
                                    selectedValue={selectedSlot}
                                    onValueChange={v => onSlotChange(v)}>
                                    {
                                        slots.map((obj, key) => {
                                            return <Picker.Item key={key}
                                                                label={moment(obj.startTime, "HHmmss").format("hh:mm")}
                                                                value={obj._id}/>
                                        })
                                    }

                                </Picker>
                            </Item>


                            <DatePickerModal
                                isVisible={isPickerVisibleDob}
                                onConfirm={(date) => handleDobConfirm(date)}
                                onCancel={onCancel}
                                maximumDate={new Date()}
                            />
                            <DatePickerModal
                                isVisible={isPickerVisibleAppointment}
                                onCancel={onCancel}
                                onConfirm={(date) => handleAppointmentConfirm(date)}
                                minimumDate={new Date()}
                            />


                            <View style={{marginHorizontal: 15}}>
                                <Button
                                    disabled={!(name.length > 0 && email.includes('@') && email.includes('.') && mobile.length > 0 && selectedGender !== "Select" && dob.length > 0 && appointmentDate.length > 0 && selectedSlot.length > 0)}
                                    success block
                                    style={{
                                        backgroundColor: (name.length > 0 && email.includes('@') && email.includes('.') && mobile.length > 0 && selectedGender !== "Select" && dob.length > 0 && appointmentDate.length > 0 && selectedSlot.length > 0) ? "red" : '#A0A9BE',
                                        marginTop: 20,
                                        // backgroundColor: 'red'
                                    }}
                                    onPress={() => {
                                        onContinue()
                                    }}>
                                    <Text createAccountText>Continue</Text>
                                </Button>
                            </View>
                        </Form>
                    </ScrollView>
                </View>
            </Modal>
        </Container>
    )
}
export default ModelShareRoom;
