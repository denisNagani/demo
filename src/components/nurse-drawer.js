import React from "react";
import { Container, Text } from "native-base";
import { Image, TouchableOpacity, SafeAreaView, Alert } from "react-native";
import Line from "../components/line";
import Images from "../assets/images";
import styles from "../styles/common";
import { useDispatch, useSelector } from "react-redux";
import AsyncStorage from "@react-native-community/async-storage";
import ProfileNameImg from "./profile-name-img";
import { successLog } from "../utils/fireLog";
import { setGetStarted } from "../services/auth";

const NurseDrawerBar = ({ navigation }) => {
	const dispatch = useDispatch();
	let userProfile = useSelector((state) => state.profile.userProfile);

	let patientMenu = [
		{
			item: "Dashboard",
			navigateTo: "NurseDashBoard",
			image: Images.icon_plus,
		},
		{ item: "Line" },
		{ item: "Logout", navigateTo: "Land", image: Images.logout },
	];

	const navigateToScreenPress = (data) => {
		successLog(`navigateToScreenPress pressed ${data}`)
		if (data === "Land") {
			console.log("CLEARED");
			AsyncStorage.clear();
			setGetStarted(true)
		}
		navigation.navigate(data);
		navigation.closeDrawer();
	};

	const checkLogout = () => {
		successLog(`logout pressed`)
		Alert.alert("Logout!!", "Are you sure you want to logout?", [
			{
				text: "Cancel",
				onPress: () => navigateToScreenPress("NurseDashBoard"),
			},
			{
				text: "OK",
				onPress: () => navigateToScreenPress("Land"),
			},
		]);
	};

	return (
		<Container>
			<SafeAreaView>
				<ProfileNameImg
					edit={false}
					userProfile={{ userId: userProfile }}
					navigation={navigation}
					editNavigation={"NurseProfile"}
				/>

				{patientMenu.map((o) => {
					return o.item !== "Line" ? (
						<TouchableOpacity
							style={styles.drawerItemContainer}
							onPress={
								o.navigateTo == "Land"
									? checkLogout
									: () => navigateToScreenPress(o.navigateTo)
							}
						>
							<Image style={styles.drawerImage} source={o.image} />
							<Text style={styles.drawerItemText}>{o.item}</Text>
						</TouchableOpacity>
					) : (
							<Line style={styles.lineStyleDrawer} />
						);
				})}
			</SafeAreaView>
		</Container>
	);
};

export default NurseDrawerBar;
