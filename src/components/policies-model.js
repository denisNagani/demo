import { Button, Icon, Text } from "native-base";
import { Modal, SafeAreaView, ScrollView, View } from "react-native";
import styles from "../styles/common";
import React from "react";

const PoliciesModel = ({ visible = false, onPress }) => {
	return (
		<Modal visible={visible} animationType="slide">
			<SafeAreaView style={styles.modalContainer}>
				<View style={styles.modalheader}>
					<Icon
						name="close"
						type="MaterialCommunityIcons"
						style={styles.modalheaderBackIcon}
						onPress={onPress}
					/>
					<Text style={styles.modalheading}>Video consultation policies</Text>
				</View>
				<ScrollView contentContainerStyle={styles.modal}>
					<Text style={styles.modalText}>
						This policy was last updated Date (dd/mm/yyyy)- Inquiries at
						(legal@sehet.com)
					</Text>
					<Text style={styles.modalTextHeader}>1. Header</Text>
					<Text style={styles.modalText}>
						By accessing the Sehet app, you are agreeing to be
						bound by these terms of service, all applicable laws and
						regulations, and agree that you are responsible for compliance with
						any applicable local laws.
					</Text>
					<Text style={styles.modalTextSubHeader}>Subheader</Text>
					<Text style={styles.modalText}>
						If you do not agree with any of these terms, you are prohibited from
						using or accessing this site. The materials contained in this
						website are protected by applicable copyright and trademark law.
						ornare
					</Text>
					<Text style={styles.modalTextHeader}>2. Header</Text>
					<Text style={styles.modalText}>
						Permission is granted to temporarily download one copy of the
						materials (information or software) website for personal,
						non-commercial transitory viewing only. This is the grant of a
						license, not a transfer of title, and under this license you may
						not:
					</Text>
					<Text style={styles.modalText}>
						• modify or copy the materials;{"\n"}• use the materials for any
						commercial purpose, or for any public display (commercial or
						non-commercial);{"\n"}• remove any copyright or other proprietary
						notations from the materials; or{"\n"}• transfer the materials to
						another person or "mirror" the materials on any other server.
					</Text>
				</ScrollView>
			</SafeAreaView>
		</Modal>
	);
};
export default PoliciesModel;
