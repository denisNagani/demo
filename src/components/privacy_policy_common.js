import { Linking, Modal, SafeAreaView, StyleSheet, Text, View } from "react-native";
import { Icon } from "native-base";
import { ScrollView } from "react-native-gesture-handler";
import React from "react";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appName, supportEmailId } from '../config/index'

const PrivacyPolicyCommon = ({ onPress, visible }) => {


    return <Modal visible={visible} animationType="slide" onRequestClose={onPress}>
        <SafeAreaView style={styles.modalContainer}>
            <View style={styles.modalheader}>
                <Icon
                    name="close"
                    type="AntDesign"
                    style={styles.modalheaderBackIcon}
                    onPress={onPress}
                />
                <Text style={styles.modalheading}>Privacy Policy</Text>
            </View>
            <ScrollView contentContainerStyle={styles.modal}>
                <Text style={styles.modalText}>The terms "We" / "Us" / "Our"/”Company” individually and collectively refer to {appName} Private Limited and the terms "You" /"Your" / "Yourself" refer to the users. </Text>
                <Text style={styles.modalText}>This Privacy Policy is an electronic record in the form of an electronic contract formed under the information Technology Act, 2000 and the rules made thereunder and the amended provisions pertaining to electronic documents / records in various statutes as amended by the information Technology Act, 2000. This Privacy Policy does not require any physical, electronic or digital signature.</Text>
                <Text style={styles.modalText}>This Privacy Policy is a legally binding document between you ans {appName} Private Limited (both terms defined below). The terms of this Privacy Policy will be effective upon your acceptance of the same (directly or indirectly in electronic form, by clicking on the I accept tab or by use of the website or by other means) and will govern the relationship between you and {appName} Private Limited for your use of the website “www.sehet.in” (defined below).</Text>
                <Text style={styles.modalText}>This document is published and shall be construed in accordance with the provisions of the Information Technology (reasonable security practices and procedures and sensitive personal data of information) rules, 2011 under Information Technology Act, 2000; that require publishing of the Privacy Policy for collection, use, storage and transfer of sensitive personal data or information.</Text>
                <Text style={styles.modalText}>Please read this Privacy Policy carefully by using the Website, you indicate that you understand, agree and consent to this Privacy Policy. If you do not agree with the terms of this Privacy Policy, please do not use this Website. </Text>
                <Text style={styles.modalText}>By providing us your Information or by making use of the facilities provided by the Website, You hereby consent to the collection, storage, processing and transfer of any or all of Your Personal Information and Non-Personal Information by us as specified under this Privacy Policy. You further agree that such collection, use, storage and transfer of Your Information shall not cause any loss or wrongful gain to you or any other person.</Text>
                <Text style={styles.modalTextSubHeader}>USER INFORMATION </Text>
                <Text style={styles.modalText}>To avail certain services on our Websites, users are required to provide certain information for the registration process namely: - a) your name, b) email address, c) sex, d) age, e) PIN code, f) credit card or debit card details g) medical records and history h) sexual orientation, i) biometric information, j) password etc., and / or your occupation, interests, and the like. The Information as supplied by the users enables us to improve our sites and provide you the most user-friendly experience.</Text>
                <Text style={styles.modalText}>All required information is service dependent and we may use the above said user information to, maintain, protect, and improve its services (including advertising services) and for developing new services</Text>
                <Text style={styles.modalText}>Such information will not be considered as sensitive if it is freely available and accessible in the public domain or is furnished under the Right to Information Act, 2005 or any other law for the time being in force.</Text>
                <Text style={styles.modalTextSubHeader}>COOKIES</Text>
                <Text style={styles.modalText}>To improve the responsiveness of the sites for our users, we may use "cookies", or similar electronic tools to collect information to assign each visitor a unique, random number as a User Identification (User ID) to understand the user's individual interests using the Identified Computer. Unless you voluntarily identify yourself (through registration, for example), we will have no way of knowing who you are, even if we assign a cookie to your computer. The only personal information a cookie can contain is information you supply (an example of this is when you ask for our Personalised Horoscope). A cookie cannot read data off your hard drive. Our advertisers may also assign their own cookies to your browser (if you click on their ads), a process that we do not control. </Text>
                <Text style={styles.modalText}>Our web servers automatically collect limited information about your computer's connection to the Internet, including your IP address, when you visit our site. (Your IP address is a number that lets computers attached to the Internet know where to send you data -- such as the web pages you view.) Your IP address does not identify you personally. We use this information to deliver our web pages to you upon request, to tailor our site to the interests of our users, to measure traffic within our site and let advertisers know the geographic locations from where our visitors come. </Text>
                <Text style={styles.modalTextSubHeader}>LINKS TO THE OTHER SITES</Text>
                <Text style={styles.modalText}>Our policy discloses the privacy practices for our own web site only. Our site provides links to other websites also that are beyond our control. We shall in no way be responsible in way for your use of such sites.5. </Text>
                <Text style={styles.modalTextSubHeader}>INFORMATION SHARING</Text>
                <Text style={styles.modalText}>We shares the sensitive personal information to any third party without obtaining the prior consent of the user in the following limited circumstances:</Text>
                <Text style={styles.modalText}>(a) When it is requested or required by law or by any court or governmental agency or authority to disclose, for the purpose of verification of identity, or for the prevention, detection, investigation including cyber incidents, or for prosecution and punishment of offences. These disclosures are made in good faith and belief that such disclosure is reasonably necessary for enforcing these Terms; for complying with the applicable laws and regulations. </Text>
                <Text style={styles.modalText}>(b) We proposes to share such information within its group companies and officers and employees of such group companies for the purpose of processing personal information on its behalf. We also ensure that these recipients of such information agree to process such information based on our instructions and in compliance with this Privacy Policy and any other appropriate confidentiality and security measures.</Text>
                <Text style={styles.modalTextSubHeader}>INFORMATION SECURITY</Text>
                <Text style={styles.modalText}>We take appropriate security measures to protect against unauthorized access to or unauthorized alteration, disclosure or destruction of data. These include internal reviews of our data collection, storage and processing practices and security measures, including appropriate encryption and physical security measures to guard against unauthorized access to systems where we store personal data.</Text>
                <Text style={styles.modalText}>All information gathered on our Website is securely stored within our controlled database. The database is stored on servers secured behind a firewall; access to the servers is password-protected and is strictly limited. However, as effective as our security measures are, no security system is impenetrable. We cannot guarantee the security of our database, nor can we guarantee that information you supply will not be intercepted while being transmitted to us over the Internet. And, of course, any information you include in a posting to the discussion areas is available to anyone with Internet access. </Text>
                <Text style={styles.modalText}>However the internet is an ever evolving medium. We may change our Privacy Policy from time to time to incorporate necessary future changes. Of course, our use of any information we gather will always be consistent with the policy under which the information was collected, regardless of what the new policy may be. </Text>
                <Text style={styles.modalTextSubHeader}>Grievance Redressal</Text>
                <Text style={styles.modalText}>Redressal Mechanism: Any complaints, abuse or concerns with regards to content and or comment or breach of these terms shall be immediately informed to the designated Grievance Officer as mentioned below via in writing or through email signed with the electronic signature to Mr. Sanjay Pareek ("Grievance Officer"). </Text>

                <Text>www.sehet.in</Text>
                <Text>Company Name & Address    {appName} Private Limited</Text>
                <Text>Email: {supportEmailId}</Text>
            </ScrollView>
        </SafeAreaView>
    </Modal>

}
export default PrivacyPolicyCommon
const styles = StyleSheet.create({

    modal: {
        justifyContent: "center",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("2%"),
    },
    modalContainer: {
        flex: 1,
    },
    modalTextHeader: {
        fontSize: 24,
        marginTop: 27,
        marginBottom: 6,
        color: "#041A32",
    },

    modalTextSubHeader: {
        fontSize: 16,
        // color: "#041A32",
        marginTop: 20,
        marginBottom: 6,
        fontWeight: 'bold'
    },
    modalText: {
        fontSize: 14,
        marginBottom: 10
        // color: "#4E5C76",
    },
    modal1Close: {
        alignItems: "center",
        justifyContent: "center",
    },
    modal2Close: {
        alignItems: "center",
        justifyContent: "center",
    },
    boldStyle: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 14
    },
    modalheader: {
        height: hp("9%"),
        flexDirection: "row",
        alignItems: "center",
    },
    modalheaderBackIcon: {
        flex: 1,
        fontSize: hp("3.5%"),
        color: "#1B80F3",
        marginLeft: wp("5%"),
    },
    modalheading: {
        flex: 8,
        color: "#041A32",
        fontSize: hp("2.9%"),
    },

})

{/* <Text style={styles.modalText}>Last updated: December 23, 2020</Text>
                <Text style={styles.modalTextHeader}>Privacy Policy for Sehet</Text>
                <Text style={styles.modalText}>At Sehet, accessible from <Text onPress={() => Linking.openURL("http://sehet.in/home")} style={{ color: 'blue' }}>http://sehet.in/home</Text>, one of our main
                    priorities is the privacy of our visitors. This Privacy Policy document contains types of
                    information that is collected and recorded by Sehet and how we use it.
                    If you have additional questions or require more information about our Privacy Policy, do not
                    hesitate to contact us.
                    This Privacy Policy applies only to our online activities and is valid for visitors to our website
                    with regards to the information that they shared and/or collect in Sehet. This policy is not
                    applicable to any information collected offline or via channels other than this website.</Text>
                <Text style={styles.modalTextHeader}>Consent</Text>
                <Text style={styles.modalText}>By using our website, you hereby consent to our Privacy Policy and agree
                    to its terms.</Text>

                <Text style={styles.modalTextHeader}>Information we collect</Text>
                <Text style={styles.modalText}>The personal information that you are asked to provide, and the reasons
                why you are asked to provide it, will be made clear to you at the point we ask you to provide your
                personal information.
                If you contact us directly, we may receive additional information about you such as your name, email
                address, phone number, the contents of the message and/or attachments you may send us, and any other
                information you may choose to provide.
                When you register for an Account, we may ask for your contact information, including items such as
                    name, company name, address, email address, and telephone number.</Text>


                <Text style={styles.modalTextHeader}>How we use your information</Text>
                <Text style={styles.modalTextSubHeader}>We use the information we collect in various ways, including
                    to:</Text>
                <Text style={styles.modalText}></Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• </Text> Provide, operate, and maintain
                    our website</Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• </Text> Improve, personalize, and expand
                    our website</Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• </Text> Understand and analyze how you
                    use our website</Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• </Text> Develop new products, services,
                    features, and functionality
                </Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• </Text> Communicate with you, either
                    directly or through one of our partners, including for customer service, to provide you with updates
                    and other information relating to the webste, and for marketing and promotional purposes
                </Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• </Text> Send you emails</Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• </Text> Find and prevent fraud</Text>


                <Text style={styles.modalTextHeader}>Log Files</Text>
                <Text style={styles.modalText}>Sehet follows a standard procedure of using log files. These files log
                visitors when they visit websites. All hosting companies do this and a part of hosting services'
                analytics. The information collected by log files include internet protocol (IP) addresses, browser
                type, Internet Service Provider (ISP), date and time stamp, referring/exit pages, and possibly the
                number of clicks. These are not linked to any information that is personally identifiable. The
                purpose of the information is for analyzing trends, administering the site, tracking users' movement
                on the website, and gathering demographic information.
                </Text>


                <Text style={styles.modalTextHeader}>Advertising Partners Privacy Policies</Text>
                <Text style={styles.modalText}>You may consult this list to find the Privacy Policy for each of the
                advertising partners of Sehet.
                Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons
                that are used in their respective advertisements and links that appear on Sehet, which are sent
                directly to users' browser. They automatically receive your IP address when this occurs. These
                technologies are used to measure the effectiveness of their advertising campaigns and/or to
                personalize the advertising content that you see on websites that you visit.
                Note that Sehet has no access to or control over these cookies that are used by third-party
                    advertisers.</Text>


                <Text style={styles.modalTextHeader}>Advertising Partners Privacy Policies</Text>
                <Text style={styles.modalText}>You may consult this list to find the Privacy Policy for each of the
                advertising partners of Sehet.
                Third-party ad servers or ad networks uses technologies like cookies, JavaScript, or Web Beacons
                that are used in their respective advertisements and links that appear on Sehet, which are sent
                directly to users' browser. They automatically receive your IP address when this occurs. These
                technologies are used to measure the effectiveness of their advertising campaigns and/or to
                personalize the advertising content that you see on websites that you visit.
                Note that Sehet has no access to or control over these cookies that are used by third-party
                    advertisers.</Text>


                <Text style={styles.modalTextHeader}>Third Party Privacy Policies</Text>
                <Text style={styles.modalText}>Sehet's Privacy Policy does not apply to other advertisers or websites.
                Thus, we are advising you to consult the respective Privacy Policies of these third-party ad servers
                for more detailed information. It may include their practices and instructions about how to opt-out
                of certain options.
                You can choose to disable cookies through your individual browser options. To know more detailed
                information about cookie management with specific web browsers, it can be found at the browsers'
                    respective websites.</Text>


                <Text style={styles.modalTextHeader}>CCPA Privacy Rights (Do Not Sell My Personal Information)</Text>
                <Text style={styles.modalText}>Under the CCPA, among other rights, California consumers have the right
                to:
                Request that a business that collects a consumer's personal data disclose the categories and
                specific pieces of personal data that a business has collected about consumers.
                Request that a business delete any personal data about the consumer that a business has collected.
                Request that a business that sells a consumer's personal data, not sell the consumer's personal
                data.
                If you make a request, we have one month to respond to you. If you would like to exercise any of
                    these rights, please contact us.</Text>


                <Text style={styles.modalTextHeader}>GDPR Data Protection Rights</Text>
                <Text style={styles.modalText}>We would like to make sure you are fully aware of all of your data
                protection rights. Every user is entitled to the following:
                The right to access – You have the right to request copies of your personal data. We may charge you
                a small fee for this service.
                The right to rectification – You have the right to request that we correct any information you
                believe is inaccurate. You also have the right to request that we complete the information you
                believe is incomplete.
                The right to erasure – You have the right to request that we erase your personal data, under certain
                conditions.
                The right to restrict processing – You have the right to request that we restrict the processing of
                your personal data, under certain conditions.
                The right to object to processing – You have the right to object to our processing of your personal
                data, under certain conditions.
                The right to data portability – You have the right to request that we transfer the data that we have
                collected to another organization, or directly to you, under certain conditions.
                If you make a request, we have one month to respond to you. If you would like to exercise any of
                    these rights, please contact us.</Text>


                <Text style={styles.modalTextHeader}>Children's Information</Text>
                <Text style={styles.modalText}>Another part of our priority is adding protection for children while
                using the internet. We encourage parents and guardians to observe, participate in, and/or monitor
                and guide their online activity.
                Sehet does not knowingly collect any Personal Identifiable Information from children under the age
                of 13. If you think that your child provided this kind of information on our website, we strongly
                encourage you to contact us immediately and we will do our best efforts to promptly remove such
                    information from our records.</Text>
 */}

