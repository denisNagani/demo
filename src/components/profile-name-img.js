import { TouchableOpacity, View } from "react-native";
import React from "react";
import styles from "../styles/common"
import { Text, Thumbnail } from "native-base";
import Images from "../assets/images";
import Line from "./line";
import { getSkipped } from "../services/auth";
import { useDispatch, useSelector } from 'react-redux'
import { showModal } from "../actions/modal";
import { strings, TranslationConfig } from "../utils/translation";
// import PowerTranslator from "react-native-power-translator";
import PowerTranslator from "./PowerTranslator";
import { ifValid } from "../utils/ifNotValid";

const ProfileNameImg = ({ userProfile, navigation, editNavigation, edit = true }) => {
    const dispatch = useDispatch();
    let fullName = "...", image = null, gender = "", hindiFullName = ""
    // let hindiFullName = "";
    if (userProfile !== undefined && userProfile !== null && userProfile.userId !== undefined && userProfile.userId !== null) {
        const user = userProfile.userId;
        image = user.imageUrl !== null ? user.imageUrl : null;
        fullName = user.fullName;
        gender = user.gender;
        hindiFullName = ifValid(user.hindiFullName) ? user.hindiFullName : fullName;
    }

    const lang = useSelector(state => state.profile.selected_language)
    // TranslationConfig(lang)

    return <>
        <View style={styles.drawerItemContainer}
            onPress={() => this.props.navigation.closeDrawer()}>
            {image ? <Thumbnail
                source={{ uri: image }}
                style={styles.drawerThumbnail}
            /> : <Thumbnail
                source={Images.user}
                style={styles.drawerThumbnail} />}


            <View style={{ marginLeft: 15, marginTop: 10 }}>
                {/* <Text style={styles.drawerProfileName}>{fullName}</Text> */}
                {/* <PowerTranslator style={styles.drawerProfileName} text={fullName} /> */}
                <PowerTranslator
                    style={styles.drawerProfileName}
                    hinText={hindiFullName}
                    enText={fullName}
                    numberOfLines={1}
                />
                {
                    edit ? <TouchableOpacity onPress={async () => {
                        const isSkipped = await getSkipped()
                        if (isSkipped === 'true') {
                            const payload = {
                                text: `${strings.please_login_to_continue}`,
                                subtext: "",
                                iconName: "closecircleo",
                                modalVisible: true,
                            }
                            navigation.navigate('PatientLogin')
                            dispatch(showModal(payload))
                        } else {
                            navigation.navigate(editNavigation)
                        }
                    }}>
                        <Text style={[styles.drawerProfileSmallText, { color: '#11693A' }]}>{strings.view_edit_profile}</Text>
                    </TouchableOpacity> : null
                }
            </View>
        </View>
        <Line style={styles.lineStyleDrawer} />
    </>
}
export default ProfileNameImg;
