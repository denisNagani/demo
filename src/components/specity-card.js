import { Text, TouchableOpacity, View } from "react-native";
import React from "react";
import styles from "../styles/common"
import { Thumbnail } from "native-base";
import Images from "../assets/images";
import { ifNotValid, ifValid } from "../utils/ifNotValid";
import { getSpeciality, selectedSpecialityObj } from "../actions/sehet/speciality-action";
import { useDispatch } from "react-redux";
import { getDoctorsSlotByID } from "../scenes/patient/select-and-book/action";
import { successLog } from "../utils/fireLog";
import { setSelectedDoctor } from "../actions/appointment";
import { getSkipped, setFromCheckout } from "../services/auth";
import { showModal } from "../actions/modal";
import { strings } from "../utils/translation";
import { calculateDrConsultationFee } from "../utils/helper";

const SpecialityCard = ({ navigation, imageUrl, speciality, object }) => {
    const dispatch = useDispatch();
    let imgUrl = imageUrl;

    let gender = ifValid(object.gender) ? object.gender : ""
    if (ifNotValid(imgUrl) || imgUrl === "") imgUrl = gender == "FEMALE" ? Images.female_doctor : Images.male_doctor
    else imgUrl = { uri: imgUrl }

    let _speciality = speciality
    if (_speciality === undefined || _speciality === null)
        _speciality = ""

    const _onSlotSelected = (item) => {
        if (ifNotValid(item) || ifNotValid(item.price))
            return;
        let m_price = parseInt(item.price)
        let _price = parseInt(item.price)
        let percentage = (_price / 100) * 20;
        _price = _price + percentage;
        dispatch(getDoctorsSlotByID(item._id))
        dispatch(selectedSpecialityObj({ ...item, price: _price, m_price: m_price, dateTime: new Date() }))
        navigation.navigate("SelectAndBook", { item: item })
    }

    const handleBookNow = async (doc) => {
        console.log("passed doc ", doc);
        dispatch(setSelectedDoctor(doc))

        const specialityArray = doc.specialityId;
        const item = calculateDrConsultationFee(specialityArray)
        if (ifNotValid(item) || ifNotValid(item.price))
            return;
        let _price = parseInt(item.price)
        let m_price = parseInt(item.price)
        let percentage = (m_price / 100) * 20;
        m_price = m_price + percentage;
        dispatch(selectedSpecialityObj({ ...item, price: m_price, m_price: m_price, dateTime: new Date() }))

        const skipped = await getSkipped()
        const isSkipped = JSON.parse(skipped)
        if (isSkipped) {
            const payload = {
                text: `${strings.please_login_to_continue}`,
                subtext: "",
                iconName: "closecircleo",
                modalVisible: true,
            }
            setFromCheckout(true)
            navigation.navigate('PatientLogin')
            dispatch(showModal(payload))
        } else {
            navigation.navigate("BookAppointmentScreenFlow2", {
                doc
            })
        }

    }

    return <TouchableOpacity
        activeOpacity={1}
        // onPress={() => _onSlotSelected(object)}>
        onPress={() => handleBookNow(object)}>
        <View style={styles.findDoctors}>
            <Thumbnail source={imgUrl} />
            <Text numberOfLines={2} style={styles.specialitiesTitle}>{_speciality}</Text>
        </View>
    </TouchableOpacity>
}
export default SpecialityCard;
