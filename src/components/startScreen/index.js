import React, { Component } from "react";
import { View } from "react-native";
import { connect } from "react-redux";
import Loader from "../../utils/loader";
import ModalComponent from "../../components/modal";
import Router from "../../navigations/";
import VideoRouter from "../../navigations/video-navigator";
import NavigationService from './RootNavigation';
import ApplyCouponModal from "../ApplyCouponModal";
import DynamicLink from "../DynamicLink";
import { getIncomingCallScreen, setIncomingCallScreen } from "../../services/auth";
import { acceptCallAction } from "../../actions/start-consultation";
import { bindActionCreators } from "redux";
import { isValid } from "../../utils/ifNotValid";
import TCModal from "../TCModal";

class StartScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isVideoScreen: false
		}
	}
	async componentDidMount() {
		const val = await getIncomingCallScreen()
		console.log("getIncomingCallScreen === " + val)
		if (val == "true") {
			this.setState({
				isVideoScreen: true
			})
		}
	}

	render() {
		return (
			<View style={{ flex: 1 }}>
				{
					this.state.isVideoScreen
						? <VideoRouter ref={navigationRef => {
							NavigationService.setTopLevelNavigator(navigationRef);
						}}> </VideoRouter>
						: <Router ref={navigationRef => {
							NavigationService.setTopLevelNavigator(navigationRef);
						}}> </Router>
				}
				{this.props.isLoader ? <Loader /> : null}
				<TCModal />
				<ModalComponent />
				<DynamicLink />
			</View>
		);
	}
}

const mapStateToProps = (state) => ({
	isLoader: state.loader.isLoading,
	applyCouponModal: state.specialitiesReducer.applyCouponModal,
	joinCallDoctorData: state.appointment.joinCallDoctorData,
});
const mapDispatchToProps = (dispatch) =>
	bindActionCreators(
		{
			acceptCallAction
		},
		dispatch
	);

export default connect(mapStateToProps, mapDispatchToProps)(StartScreen);
