import { Linking, Modal, SafeAreaView, StyleSheet, Text, View } from "react-native";
import { Icon } from "native-base";
import { ScrollView } from "react-native-gesture-handler";
import React from "react";
import { appName } from '../config/index'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

const TAndCCommon = ({ onPress, visible }) => {

    return <Modal visible={visible} animationType="slide"
        onRequestClose={onPress}
    >
        <SafeAreaView style={styles.modalContainer}>
            <View style={styles.modalheader}>
                <Icon
                    name="close"
                    type="AntDesign"
                    style={styles.modalheaderBackIcon}
                    onPress={onPress}
                />
                <Text style={styles.modalheading}>
                    Terms and Conditions
                </Text>
            </View>
            <ScrollView contentContainerStyle={styles.modal}>

                <Text style={styles.modalText}>The terms "We" / "Us" / "Our"/”Company” individually and collectively refer to {appName} private Limited and the terms "Visitor” ”User” refer to the users. </Text>
                <Text style={styles.modalText}>This page states the Terms and Conditions under which you (Visitor) may visit this website (“www.amar-e-clinc.in”). Please read this page carefully. If you do not accept the Terms and Conditions stated here, we would request you to exit this site. The business, any of its business divisions and / or its subsidiaries, associate companies or subsidiaries to subsidiaries or such other investment companies (in India or abroad) reserve their respective rights to revise these Terms and Conditions at any time by updating this posting. You should visit this page periodically to re-appraise yourself of the Terms and Conditions, because they are binding on all users of this Website.</Text>
                <Text style={styles.modalTextSubHeader}>USE OF CONTENT</Text>
                <Text style={styles.modalText}>All logos, brands, marks headings, labels, names, signatures, numerals, shapes or any combinations thereof, appearing in this site, except as otherwise noted, are properties either owned, or used under licence, by the business and / or its associate entities who feature on this Website. The use of these properties or any other content on this site, except as provided in these terms and conditions or in the site content, is strictly prohibited.</Text>
                <Text style={styles.modalText}>You may not sell or modify the content of this Website  or reproduce, display, publicly perform, distribute, or otherwise use the materials in any way for any public or commercial purpose without the respective organisation’s or entity’s written permission.</Text>
                <Text style={styles.modalTextSubHeader}>ACCEPTABLE WEBSITE USE</Text>

                <Text style={styles.modalTextSubHeader}>(A) Security Rules</Text>
                <Text style={styles.modalText}>Visitors are prohibited from violating or attempting to violate the security of the Web site, including, without limitation, (1) accessing data not intended for such user or logging into a server or account which the user is not authorised to access, (2) attempting to probe, scan or test the vulnerability of a system or network or to breach security or authentication measures without proper authorisation, (3) attempting to interfere with service to any user, host or network, including, without limitation, via means of submitting a virus or "Trojan horse" to the Website, overloading, "flooding", "mail bombing" or "crashing", or (4) sending unsolicited electronic mail, including promotions and/or advertising of products or services. Violations of system or network security may result in civil or criminal liability. The business and / or its associate entities will have the right to investigate occurrences that they suspect as involving such violations and will have the right to involve, and cooperate with, law enforcement authorities in prosecuting users who are involved in such violations.</Text>

                <Text style={styles.modalTextSubHeader}>(B) General Rules</Text>
                <Text style={styles.modalText}>Visitors may not use the Web Site in order to transmit, distribute, store or destroy material (a) that could constitute or encourage conduct that would be considered a criminal offence or violate any applicable law or regulation, (b) in a manner that will infringe the copyright, trademark, trade secret or other intellectual property rights of others or violate the privacy or publicity of other personal rights of others, or (c) that is libellous, defamatory, pornographic, profane, obscene, threatening, abusive or hateful.</Text>

                <Text style={styles.modalTextSubHeader}>INDEMNITY</Text>
                <Text style={styles.modalText}>The User unilaterally agree to indemnify and hold harmless, without objection, the Company, its officers, directors, employees and agents from and against any claims, actions and/or demands and/or liabilities and/or losses and/or damages whatsoever arising from or resulting from their use of www.amar-e-clinic.in or their breach of the terms . </Text>

                <Text style={styles.modalTextSubHeader}>LIABILITY</Text>
                <Text style={styles.modalText}>User agrees that neither Company nor its group companies, directors, officers or employee shall be liable for any direct or/and indirect or/and incidental or/and special or/and consequential or/and exemplary damages, resulting from the use or/and the inability to use the service or/and for cost of procurement of substitute goods or/and services or resulting from any goods or/and data or/and information or/and services purchased or/and obtained or/and messages received or/and transactions entered into through or/and from the service or/and resulting from unauthorized access to or/and alteration of user's transmissions or/and data or/and arising from any other matter relating to the service, including but not limited to, damages for loss of profits or/and use or/and data or other intangible, even if Company has been advised of the possibility of such damages.User further agrees that Company shall not be liable for any damages arising from interruption, suspension or termination of service, including but not limited to direct or/and indirect or/and incidental or/and special consequential or/and exemplary damages, whether such interruption or/and suspension or/and termination was justified or not, negligent or intentional, inadvertent or advertent. User agrees that Company shall not be responsible or liable to user, or anyone, for the statements or conduct of any third party of the service. In sum, in no event shall Company's total liability to the User for all damages or/and losses or/and causes of action exceed the amount paid by the User to Company, if any, that is related to the cause of action.</Text>

                <Text style={styles.modalTextSubHeader}>DISCLAIMER OF CONSEQUENTIAL DAMAGES</Text>
                <Text style={styles.modalText}>In no event shall Company or any parties, organizations or entities associated with the corporate brand name us or otherwise, mentioned at this Website be liable for any damages whatsoever (including, without limitations, incidental and consequential damages, lost profits, or damage to computer hardware or loss of data information or business interruption) resulting from the use or inability to use the Website and the Website material, whether based on warranty, contract, tort, or any other legal theory, and whether or not, such organization or entities were advised of the possibility of such damages.</Text>

            </ScrollView>
        </SafeAreaView>
    </Modal>

}
export default TAndCCommon
const styles = StyleSheet.create({

    modal: {
        justifyContent: "center",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("2%"),
    },
    boldStyle: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: 14
    },
    modalContainer: {
        flex: 1,
    },
    modalTextHeader: {
        fontSize: 24,
        marginTop: 27,
        marginBottom: 6,
        color: "#041A32",
    },

    modalTextSubHeader: {
        fontSize: 16,
        // color: "#041A32",
        marginTop: 20,
        marginBottom: 6,
        fontWeight: 'bold'
    },
    modalText: {
        fontSize: 14,
        marginBottom: 15
        // color: "#3c3c3c",
    },
    modal1Close: {
        alignItems: "center",
        justifyContent: "center",
    },
    modal2Close: {
        alignItems: "center",
        justifyContent: "center",
    },
    modalheader: {
        height: hp("9%"),
        flexDirection: "row",
        alignItems: "center",
    },
    modalheaderBackIcon: {
        flex: 1,
        fontSize: hp("3.5%"),
        color: "#1B80F3",
        marginLeft: wp("5%"),
    },
    modalheading: {
        flex: 8,
        color: "#041A32",
        fontSize: hp("2.9%"),
    },

})

{/* <Text style={styles.modalText}>Last updated: December 23, 2020</Text>
                <Text style={styles.modalTextHeader}>Interpretation and Definitions</Text>
                <Text style={styles.modalTextSubHeader}>Interpretation</Text>
                <Text style={styles.modalText}>The words of which the initial letter is capitalized have meanings
                defined under the following conditions. The following definitions shall have the same meaning
                regardless of whether they appear in singular or in plural.
                </Text>
                <Text style={styles.modalTextSubHeader}>Definitions</Text>
                <Text style={[styles.modalTextSubHeader, { marginTop: 2 }]}>For the purposes of these Terms and
                    Conditions:</Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• Affiliate</Text> means an entity that
                    controls, is controlled by or is under common control with a party,
                    where "control" means ownership of 50% or more of the shares, equity interest or other securities
                    entitled to vote for election of directors or other managing authority.
                </Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• Affiliate</Text> refers to: Rajasthan,
                    India</Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• Country</Text> (referred to as either
                    "the Company", "We", "Us" or "Our" in this Agreement) refers to Sehet.</Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• Company</Text> means any device that can
                    access the Service such as a computer, a cellphone or a digital tablet.
                </Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• Device</Text> refers to the Website.
                </Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• Service</Text> (also referred as
                    "Terms") mean these Terms and Conditions that form the entire agreement between You and the Company
                    regarding the use of the Service. This Terms and Conditions agreement has been created with the help
                    of the Terms and Conditions Generator.
                </Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• Terms and Conditions</Text> means any
                    services or content (including data, information, products or services) provided by a third-party
                    that may be displayed, included or made available by the Service.
                </Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• Website</Text> refers to Sehet,
                    accessible from http://sehet.in/home
                </Text>
                <Text style={styles.modalText}><Text style={styles.boldStyle}>• You</Text> means the individual
                    accessing or using the Service, or the company, or other legal entity on behalf of which such
                    individual is accessing or using the Service, as applicable.
                </Text>

                <Text style={styles.modalTextHeader}>Acknowledgment</Text>
                <Text style={styles.modalText}>These are the Terms and Conditions governing the use of this Service and
                the agreement that operates between You and the Company. These Terms and Conditions set out the
                rights and obligations of all users regarding the use of the Service.
                Your access to and use of the Service is conditioned on Your acceptance of and compliance with these
                Terms and Conditions. These Terms and Conditions apply to all visitors, users and others who access
                or use the Service.
                By accessing or using the Service You agree to be bound by these Terms and Conditions. If You
                disagree with any part of these Terms and Conditions then You may not access the Service.
                You represent that you are over the age of 18. The Company does not permit those under 18 to use the
                Service.
                Your access to and use of the Service is also conditioned on Your acceptance of and compliance with
                the Privacy Policy of the Company. Our Privacy Policy describes Our policies and procedures on the
                collection, use and disclosure of Your personal information when You use the Application or the
                Website and tells You about Your privacy rights and how the law protects You. Please read Our
                    Privacy Policy carefully before using Our Service.</Text>
                <Text style={styles.modalText}></Text>

                <Text style={styles.modalTextHeader}>Links to Other Websites</Text>
                <Text style={styles.modalText}>Our Service may contain links to third-party web sites or services that
                are not owned or controlled by the Company.
                The Company has no control over, and assumes no responsibility for, the content, privacy policies,
                or practices of any third party web sites or services. You further acknowledge and agree that the
                Company shall not be responsible or liable, directly or indirectly, for any damage or loss caused or
                alleged to be caused by or in connection with the use of or reliance on any such content, goods or
                services available on or through any such web sites or services.
                We strongly advise You to read the terms and conditions and privacy policies of any third-party web
                    sites or services that You visit.</Text>


                <Text style={styles.modalTextHeader}>Termination</Text>
                <Text style={styles.modalText}>We may terminate or suspend Your access immediately, without prior notice
                or liability, for any reason whatsoever, including without limitation if You breach these Terms and
                Conditions.
                    Upon termination, Your right to use the Service will cease immediately.</Text>


                <Text style={styles.modalTextHeader}>Limitation of Liability</Text>
                <Text style={styles.modalText}>Notwithstanding any damages that You might incur, the entire liability of
                the Company and any of its suppliers under any provision of this Terms and Your exclusive remedy for
                all of the foregoing shall be limited to the amount actually paid by You through the Service or 100
                USD if You haven't purchased anything through the Service.
                To the maximum extent permitted by applicable law, in no event shall the Company or its suppliers be
                liable for any special, incidental, indirect, or consequential damages whatsoever (including, but
                not limited to, damages for loss of profits, loss of data or other information, for business
                interruption, for personal injury, loss of privacy arising out of or in any way related to the use
                of or inability to use the Service, third-party software and/or third-party hardware used with the
                Service, or otherwise in connection with any provision of this Terms), even if the Company or any
                supplier has been advised of the possibility of such damages and even if the remedy fails of its
                essential purpose.
                Some states do not allow the exclusion of implied warranties or limitation of liability for
                incidental or consequential damages, which means that some of the above limitations may not apply.
                In these states, each party's liability will be limited to the greatest extent permitted by
                    law.</Text>


                <Text style={styles.modalTextHeader}>"AS IS" and "AS AVAILABLE" Disclaimer</Text>
                <Text style={styles.modalText}>The Service is provided to You "AS IS" and "AS AVAILABLE" and with all
                faults and defects without warranty of any kind. To the maximum extent permitted under applicable
                law, the Company, on its own behalf and on behalf of its Affiliates and its and their respective
                licensors and service providers, expressly disclaims all warranties, whether express, implied,
                statutory or otherwise, with respect to the Service, including all implied warranties of
                merchantability, fitness for a particular purpose, title and non-infringement, and warranties that
                may arise out of course of dealing, course of performance, usage or trade practice. Without
                limitation to the foregoing, the Company provides no warranty or undertaking, and makes no
                representation of any kind that the Service will meet Your requirements, achieve any intended
                results, be compatible or work with any other software, applications, systems or services, operate
                without interruption, meet any performance or reliability standards or be error free or that any
                errors or defects can or will be corrected.
                Without limiting the foregoing, neither the Company nor any of the company's provider makes any
                representation or warranty of any kind, express or implied: (i) as to the operation or availability
                of the Service, or the information, content, and materials or products included thereon; (ii) that
                the Service will be uninterrupted or error-free; (iii) as to the accuracy, reliability, or currency
                of any information or content provided through the Service; or (iv) that the Service, its servers,
                the content, or e-mails sent from or on behalf of the Company are free of viruses, scripts, trojan
                horses, worms, malware, timebombs or other harmful components.
                Some jurisdictions do not allow the exclusion of certain types of warranties or limitations on
                applicable statutory rights of a consumer, so some or all of the above exclusions and limitations
                may not apply to You. But in such a case the exclusions and limitations set forth in this section
                shall be applied to the greatest extent enforceable under applicable law.

                </Text>


                <Text style={styles.modalTextHeader}>Governing Law</Text>
                <Text style={styles.modalText}>The laws of the Country, excluding its conflicts of law rules, shall
                govern this Terms and Your use of the Service. Your use of the Application may also be subject to
                    other local, state, national, or international laws.</Text>


                <Text style={styles.modalTextHeader}>Disputes Resolution</Text>
                <Text style={styles.modalText}>If You have any concern or dispute about the Service, You agree to first
                    try to resolve the dispute informally by contacting the Company.</Text>


                <Text style={styles.modalTextHeader}>For European Union (EU) Users</Text>
                <Text style={styles.modalText}>If You are a European Union consumer, you will benefit from any mandatory
                provisions of the law of the country in which you are resident in.
                </Text>


                <Text style={styles.modalTextHeader}>United States Legal Compliance</Text>
                <Text style={styles.modalText}>You represent and warrant that (i) You are not located in a country that
                is subject to the United States government embargo, or that has been designated by the United States
                government as a "terrorist supporting" country, and (ii) You are not listed on any United States
                government list of prohibited or restricted parties.
                </Text>


                <Text style={styles.modalTextHeader}>Severability and Waiver</Text>
                <Text style={styles.modalTextSubHeader}>Severability</Text>
                <Text style={styles.modalText}>If any provision of these Terms is held to be unenforceable or invalid,
                such provision will be changed and interpreted to accomplish the objectives of such provision to the
                greatest extent possible under applicable law and the remaining provisions will continue in full
                force and effect.
                </Text>
                <Text style={styles.modalTextSubHeader}>Waiver</Text>
                <Text style={styles.modalText}>Except as provided herein, the failure to exercise a right or to require
                performance of an obligation under this Terms shall not effect a party's ability to exercise such
                right or require such performance at any time thereafter nor shall be the waiver of a breach
                constitute a waiver of any subsequent breach.

                </Text>


                <Text style={styles.modalTextHeader}>Translation Interpretation</Text>
                <Text style={styles.modalText}>These Terms and Conditions may have been translated if We have made them
                available to You on our Service. You agree that the original English text shall prevail in the case
                of a dispute.These Terms and Conditions may have been translated if We have made them available to
                You on our Service. You agree that the original English text shall prevail in the case of a dispute.
                </Text>

                <Text style={styles.modalTextHeader}>Changes to These Terms and Conditions</Text>
                <Text style={styles.modalText}>We reserve the right, at Our sole discretion, to modify or replace these
                Terms at any time. If a revision is material We will make reasonable efforts to provide at least 30
                days' notice prior to any new terms taking effect. What constitutes a material change will be
                determined at Our sole discretion.
                By continuing to access or use Our Service after those revisions become effective, You agree to be
                bound by the revised terms. If You do not agree to the new terms, in whole or in part, please stop
                    using the website and the Service.</Text>

                <Text style={styles.modalTextHeader}>Contact Us</Text>
                <Text style={styles.modalText}>If you have any questions about these Terms and Conditions, You can
                contact us:
                    By email: <Text onPress={() => Linking.openURL('mailto:info@sehet.in')} style={{ color: 'blue' }}>info@sehet.in</Text></Text> */}

