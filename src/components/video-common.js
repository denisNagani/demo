import React from "react";
import { FlatList, Image, Text, TouchableOpacity, View } from "react-native";
import styles from "../styles/common";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_REGULAR } from "../styles/typography";
import Images from "../assets/images";
import { AgoraView } from "react-native-agora";
import HeaderCommon from "./header";
import { AttendeesItem } from "./AttendeesItem";
import { Container, Icon } from "native-base";
import HeaderSehet from "./header-common";
import KeepAwake from 'react-native-keep-awake';

const VideoCommon = ({ makeFullScreen, copyUrl, fullScreen, peerIdSelected, selectedHeader, usersListFirebase, onPressShowList, endCall, toggleVideo, toggleAudio, peerIds = [], showList, audMute, vidMute, joinSucceed = false, minutes = 0, seconds = 0, switchCamera }) => {

    return <View style={{ flex: 1 }}>
        {!showList ? (
            <View style={styles.joinCallheader}>
                <View>
                    <Text style={styles.joinCallheaderText}>
                        {selectedHeader}
                    </Text>
                    <Text
                        style={{
                            color: "#041A32",
                            fontSize: 13,
                            fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                            marginLeft: 20,
                        }}>
                        {minutes}:{seconds}
                    </Text>
                </View>
                {/*<View style={{flexDirection: "row"}}>*/}
                {/*    <Image*/}
                {/*        source={Images.network_no_circle}*/}
                {/*        style={[styles.iconHeaderCall]}*/}
                {/*    />*/}
                {/*    <Image*/}
                {/*        source={Images.three_dots_no_circle}*/}
                {/*        style={[styles.iconHeaderCall, {marginLeft: 10}]}*/}
                {/*    />*/}
                {/*</View>*/}
            </View>
        ) : null}
        {
            <View style={styles.max}>
                {!joinSucceed ? (
                    <View />
                ) : (
                        fullScreen ?
                            <View style={{ flex: 1 }}>
                                {
                                    peerIdSelected !== -1 ?
                                        <TouchableOpacity activeOpacity={1} style={styles.full}
                                            onPress={() => makeFullScreen(-1)}>
                                            <AgoraView remoteUid={peerIdSelected} style={styles.full} mode={1} />
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity activeOpacity={1} style={styles.full}
                                            onPress={() => makeFullScreen(-1)}>
                                            <AgoraView
                                                style={styles.full}
                                                zOrderMediaOverlay={true}
                                                showLocalVideo={true}
                                                mode={1}
                                            />
                                        </TouchableOpacity>
                                }

                            </View>
                            :
                            <View style={styles.fullView}>
                                {peerIds.length > 3 ? ( //view for five videoStreams
                                    <View style={styles.full}>
                                        <View style={styles.halfViewRow}>
                                            <AgoraView
                                                style={styles.half}
                                                remoteUid={peerIds[0]}
                                                mode={1}
                                            />
                                            <AgoraView
                                                style={styles.half}
                                                remoteUid={peerIds[1]}
                                                mode={1}
                                            />
                                        </View>
                                        <View style={styles.halfViewRow}>
                                            <AgoraView
                                                style={styles.half}
                                                remoteUid={peerIds[2]}
                                                mode={1}
                                            />
                                            <AgoraView
                                                style={styles.half}
                                                remoteUid={peerIds[3]}
                                                mode={1}
                                            />
                                        </View>
                                    </View>
                                ) : peerIds.length > 2 ? ( //view for four videostreams
                                    <View style={styles.full}>
                                        <View style={styles.halfViewRow}>
                                            <TouchableOpacity activeOpacity={1} style={styles.full}
                                                onPress={() => makeFullScreen(peerIds[0])}>
                                                <AgoraView remoteUid={peerIds[0]} style={styles.full} mode={1} />
                                            </TouchableOpacity>

                                            <TouchableOpacity activeOpacity={1} style={styles.full}
                                                onPress={() => makeFullScreen(peerIds[1])}>
                                                <AgoraView remoteUid={peerIds[1]} style={styles.full} mode={1} />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={styles.halfViewRow}>

                                            {!vidMute ?   //view for local video*/}
                                                <TouchableOpacity activeOpacity={1} style={styles.full}
                                                    onPress={() => makeFullScreen(-1)}>
                                                    <AgoraView zOrderMediaOverlay={true} style={styles.full}
                                                        showLocalVideo={true} mode={1} />
                                                </TouchableOpacity> : <View />}


                                            <TouchableOpacity activeOpacity={1} style={styles.full}
                                                onPress={() => makeFullScreen(peerIds[2])}>
                                                <AgoraView remoteUid={peerIds[2]} style={styles.full} mode={1} />
                                            </TouchableOpacity>

                                        </View>
                                    </View>
                                ) : peerIds.length > 1 ? ( //view for three video streams
                                    <View style={styles.full}>
                                        <View style={styles.halfViewRow}>

                                            <TouchableOpacity activeOpacity={1} style={styles.full}
                                                onPress={() => makeFullScreen(peerIds[0])}>
                                                <AgoraView remoteUid={peerIds[0]} style={styles.full} mode={1} />
                                            </TouchableOpacity>

                                            <TouchableOpacity activeOpacity={1} style={styles.full}
                                                onPress={() => makeFullScreen(peerIds[1])}>
                                                <AgoraView remoteUid={peerIds[1]} style={styles.full} mode={1} />
                                            </TouchableOpacity>

                                        </View>
                                        <View style={styles.halfViewRow}>
                                            {!vidMute ?   //view for local video*/}
                                                <TouchableOpacity activeOpacity={1} style={styles.full}
                                                    onPress={() => makeFullScreen(-1)}>
                                                    <AgoraView zOrderMediaOverlay={true} style={styles.full}
                                                        showLocalVideo={true} mode={1} />
                                                </TouchableOpacity> : <View />}
                                        </View>
                                    </View>
                                ) : peerIds.length > 0 ? ( //view for two video stream

                                    <View style={styles.full}>

                                        <TouchableOpacity activeOpacity={1} style={styles.full}
                                            onPress={() => makeFullScreen(peerIds[0])}>
                                            <AgoraView remoteUid={peerIds[0]} style={styles.full} mode={1} />
                                        </TouchableOpacity>

                                        {!vidMute ?   //view for local video*/}
                                            <TouchableOpacity activeOpacity={1} style={styles.full}
                                                onPress={() => makeFullScreen(-1)}>
                                                <AgoraView zOrderMediaOverlay={true} style={styles.full}
                                                    showLocalVideo={true} mode={1} />
                                            </TouchableOpacity> : <View />}

                                    </View>) :
                                                !vidMute ? <AgoraView
                                                    style={styles.full}
                                                    zOrderMediaOverlay={true}
                                                    showLocalVideo={true}
                                                    mode={1}
                                                /> : <View />

                                }

                                {showList ? ( //view for local video
                                    <View style={styles.userList}>
                                        <HeaderCommon
                                            onPress={onPressShowList}
                                        />

                                        <FlatList
                                            keyExtractor={(item, index) => index.toString()}
                                            data={usersListFirebase}
                                            style={{ flex: 1, backgroundColor: "white" }}
                                            renderItem={({ item, index }) => {
                                                return <AttendeesItem index={index} item={item} />;
                                            }}
                                        />
                                    </View>
                                ) : (
                                        <View />
                                    )}
                            </View>
                    )}
            </View>
        }
        <View style={styles.buttonHolder}>
            <TouchableOpacity
                title="Start Call"
                onPress={toggleAudio}
                style={styles.button}
            >
                {!audMute ? (
                    <Icon
                        type="Feather"
                        size={14}
                        name="mic"
                        style={styles.iconStyle}
                        fontSize={12}
                    />
                ) : (
                        <Image source={Images.mic_off_icon} style={styles.iconBottom} />
                    )}
            </TouchableOpacity>

            <TouchableOpacity
                title="Start Call"
                onPress={switchCamera}
                style={styles.button}
            >
                <Icon
                    type="AntDesign"
                    size={16}
                    name="camerao"
                    style={[styles.iconStyle, { fontSize: 36 }]}
                    fontSize={12}
                />
            </TouchableOpacity>

            <TouchableOpacity
                title="End Call"
                style={styles.button}
                onPress={toggleVideo}
            >
                {!vidMute ? (
                    <Icon type="Feather" name="video" style={styles.iconStyle} />
                ) : (
                        <Image source={Images.video_off_icon} style={styles.iconBottom} />
                    )}
            </TouchableOpacity>
            <KeepAwake />
            <TouchableOpacity
                title="Start Call"
                onPress={endCall}
                style={styles.button}
            >
                <Image
                    source={Images.call_end}
                    style={[styles.iconBottom, { width: 35 }]}
                />
            </TouchableOpacity>


            {copyUrl ?
                <TouchableOpacity
                    style={styles.button}
                    onPress={copyUrl}>
                    <Icon type="Feather" name="copy" style={styles.iconStyle} />
                </TouchableOpacity> : null

            }


            {
                onPressShowList ?

                    <TouchableOpacity
                        style={styles.button}
                        onPress={onPressShowList}
                    >
                        <Image source={Images.people_icon} style={styles.iconBottom} />
                        <Text
                            style={[
                                {
                                    fontSize: 11,
                                    fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
                                    color: "black",
                                },
                                styles.userListCount,
                            ]}
                        >
                            {peerIds.length}
                        </Text>
                    </TouchableOpacity> : null


            }
        </View>

    </View>
}
export default VideoCommon
