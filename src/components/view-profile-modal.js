// import Modal, { ModalContent } from "react-native-modals";
import { Text } from "native-base";
import { FlatList, Image, StyleSheet, TouchableOpacity, View, Modal, BackHandler, ScrollView } from "react-native";
import React, { useEffect } from 'react';
import Images from "../assets/images";
import { ifNotValid, ifValid, isValid } from "../utils/ifNotValid";
import ImageContainer from "../components/ImageContainer";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_19
} from "../styles/typography";
import Line from "./line";
import { useSelector } from "react-redux";
import { ImageBackground } from "react-native";
import { TranslationConfig, strings } from "../utils/translation";
import PowerTranslator from "./PowerTranslator";
import { ActivityIndicator } from "react-native";

const ViewProfileModal = ({ visible, onTouchOutside, imageUrl, specialization, experience, name, onHardwareBackPress }) => {
    const isLoading = useSelector(state => state.loader.isLoading)

    // useEffect(() => {
    //     const handleValidateClose = () => {
    //         onTouchOutside()
    //         return true;
    //     };
    //     const handler = BackHandler.addEventListener('hardwareBackPress', handleValidateClose);
    //     return () => handler.remove();
    // }, [])

    const lang = useSelector(state => state.profile.selected_language)
    // TranslationConfig(lang)

    let docProfile = useSelector((state) => state.userReducer.docProfile);

    let docUserId = ifValid(docProfile) && ifValid(docProfile.userId) ? docProfile.userId : {};
    let _name = ifValid(docUserId) ? docUserId.fullName : ''
    if (ifNotValid(_name)) _name = "..."

    let _hindiFullName = ifValid(docUserId) ? docUserId.hindiFullName : ''
    if (ifNotValid(_hindiFullName)) _hindiFullName = _name

    let _speciality = docProfile.specialization;
    if (ifNotValid(_speciality)) _speciality = ""

    let _hindiSpeciality = docProfile.hindiName;
    if (ifNotValid(_hindiSpeciality)) _hindiSpeciality = ""

    let gender = docUserId.gender;
    let _imageUrl = docUserId.imageUrl;
    if (ifNotValid(_imageUrl)) _imageUrl = gender == "FEMALE" ? Images.female_doctor : Images.male_doctor
    else _imageUrl = { uri: _imageUrl }

    let _experience = docProfile.experience;
    if (ifNotValid(_experience)) _experience = "-"
    else _experience = _experience + " Years Experience"

    let _degree = docProfile.education;
    if (ifNotValid(_degree)) _degree = "-"

    let _overview = docProfile.overview;
    if (ifNotValid(_overview)) _overview = "-"

    let _clinicAddess = docProfile.clinicAddess;
    if (ifNotValid(_clinicAddess)) _clinicAddess = "-"

    let _feedbackCount = docProfile.feedbackCount;
    if (ifNotValid(_feedbackCount)) _feedbackCount = "0"

    let _areasOfInterest = docProfile.areasOfInterest;
    if (ifNotValid(_areasOfInterest)) _areasOfInterest = ["no data found"]
    else _areasOfInterest = _areasOfInterest.split(',')

    const _renderItem = (item) => {
        if (isValid(item)) {
            return <View style={{
                justifyContent: 'space-between',
                margin: '2%'
            }}>
                <Text numberOfLines={1} style={{
                    color: '#4E5C76',
                    backgroundColor: '#F5F6F9',
                    paddingHorizontal: 11,
                    paddingVertical: 7,
                    textAlign: 'center'
                }}>{item}</Text>
            </View>
        }
    }
    return <View>
        <Modal
            // width={0.9}
            visible={visible}
            onTouchOutside={onTouchOutside}
            rounded
            actionsBordered
            onHardwareBackPress={onHardwareBackPress}
            onSwipeOut={onTouchOutside}>
            <ImageContainer>
                <View style={{
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginHorizontal: 25,
                }}>
                    {
                        isLoading
                            ? <View>
                                <ActivityIndicator size={'large'} color="#35B3FF" />
                            </View>
                            : <View style={{
                                backgroundColor: '#FFF',
                                marginHorizontal: 25,
                                padding: 10,
                                borderRadius: 10
                            }}>
                                <View style={styles.docDetails}>
                                    <Image source={_imageUrl} style={styles.headerImg} />
                                    <View style={styles.docDesc}>
                                        {/* <Text numberOfLines={1} style={styles.docName}>Dr. {_name}</Text> */}
                                        {/* <PowerTranslator numberOfLines={1} style={styles.docName} text={`Dr. ${_name}`} /> */}
                                        <PowerTranslator
                                            numberOfLines={1}
                                            style={styles.docName}
                                            hinText={`${_hindiFullName}`}
                                            enText={`${_name}`}
                                        />
                                        {/*<Text style={styles.specializationStyle}>{_speciality}</Text>*/}
                                        {/* <Text style={styles.specializationStyle}>{_experience}</Text> */}
                                        {/* <PowerTranslator style={styles.specializationStyle} text={_experience} /> */}
                                        <PowerTranslator
                                            style={styles.specializationStyle}
                                            hinText={_experience}
                                            enText={_experience}
                                        />
                                    </View>
                                </View>

                                <View style={styles.docProfile}>
                                    <Image source={Images.hat} style={{ width: 32, height: 16, marginRight: 0 }} />
                                    {/* <Text style={styles.docProfileText}>{_degree}</Text> */}
                                    {/* <PowerTranslator style={styles.docProfileText} text={_degree} /> */}
                                    <PowerTranslator
                                        style={styles.docProfileText}
                                        hinText={_degree}
                                        enText={_degree}
                                    />
                                </View>
                                <View style={styles.docProfile}>
                                    <Image source={Images.policy} style={{ width: 25, marginLeft: 4, marginRight: 4, height: 22, }} />
                                    {/* <PowerTranslator style={styles.docProfileText} text={_overview} /> */}
                                    <PowerTranslator
                                        style={styles.docProfileText}
                                        hinText={_overview}
                                        enText={_overview}
                                    />
                                </View>
                                <View style={styles.docProfile}>
                                    <Image source={Images.mapPin} style={{ width: 20, height: 24, marginLeft: 5, marginRight: 8 }} />
                                    {/* <Text numberOfLines={2} style={styles.docProfileText}>{_clinicAddess}</Text> */}
                                    {/* <PowerTranslator style={styles.docProfileText} text={_clinicAddess} /> */}
                                    <PowerTranslator
                                        style={styles.docProfileText}
                                        hinText={_clinicAddess}
                                        enText={_clinicAddess}
                                    />
                                </View>
                                <Line style={{ marginTop: 15, borderBottomWidth: 0.2 }} lineColor="#A0A9BE" />

                                <View style={{ width: '100%', marginTop: '3%' }}>
                                    <Text style={styles.areaOfSpecialStyle}>Areas of Special Interest</Text>
                                    <FlatList
                                        keyExtractor={(item, index) => index.toString()}
                                        data={_areasOfInterest}
                                        numColumns={2}
                                        renderItem={({ item }) => _renderItem(item)}
                                    />
                                </View>

                                <Line style={{ marginTop: 15, borderBottomWidth: 0.2, }} lineColor="#A0A9BE" />
                                {/*<View style={styles.docProfile}>*/}
                                {/*    <Text style={styles.areaOfSpecialStyle}>Feedbacks</Text>*/}
                                {/*</View>*/}

                                {/*<View style={styles.docProfile}>*/}
                                {/*    <Image source={Images.thumb} style={{width: 15, height: 15}}/>*/}
                                {/*    <Text style={styles.docProfileText}>{_feedbackCount} people find its helpful</Text>*/}
                                {/*</View>*/}
                                <TouchableOpacity style={styles.buttonStyle} onPress={onTouchOutside}>
                                    <Text uppercase={false} style={styles.buttonTxtStyle}>Ok, Thanks!</Text>
                                </TouchableOpacity>
                            </View>
                    }
                </View>
            </ImageContainer>
        </Modal>
    </View>
}
const styles = StyleSheet.create({
    docDetails: {
        width: '100%',
        flexDirection: "row",
    },
    buttonStyle: {
        borderRadius: 15,
        borderColor: '#00A2FF',
        paddingVertical: 4,
        paddingHorizontal: 8,
        borderWidth: 1,
        alignSelf: 'center',
        marginTop: '5%',
    },
    buttonTxtStyle: {
        color: '#00A2FF',
        textAlign: 'center',
        fontSize: FONT_SIZE_15
    },
    docDesc: {
        flexDirection: "column",
        justifyContent: "space-evenly",
        marginLeft: wp("5%"),
        marginRight: wp("25%"),
    },

    headerImg: { width: 72, height: 72, borderRadius: 72 / 2, },

    docName: { color: "#041A32", fontSize: FONT_SIZE_19, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM },

    specializationStyle: {
        fontSize: FONT_SIZE_14, color: '#484F5F', fontFamily: FONT_FAMILY_SF_PRO_REGULAR
    },
    viewProfileStyle: {
        fontSize: FONT_SIZE_14, color: '#484F5F', fontFamily: FONT_FAMILY_SF_PRO_REGULAR, marginTop: 8
    },

    docProfile: {
        flexDirection: "row", marginTop: '3%', alignItems: 'center'
    },
    docProfileText: {
        fontSize: FONT_SIZE_14, color: '#484F5F', fontFamily: FONT_FAMILY_SF_PRO_REGULAR, marginLeft: '4%', flex: 1,
    },
    areaOfSpecialStyle: {
        fontSize: FONT_SIZE_14, color: '#484F5F', fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, marginVertical: 5
    },

})
export default ViewProfileModal;
