import Config from "react-native-config";
import { io } from "socket.io-client";

const base_url = Config.base_url;
const agora_id = Config.agora_id;
const staging = Config.ENV === "DEV" ? true : false;
const translationApiKey = Config.translationApiKey
const paytm_mid = Config.paytm_mid
const webClientId = Config.webClientId
const CONSULT_FEE = 200
const appName = "Amar e-clinic"
const supportEmailId = "amarhospital@gmail.com"
const playStoreAppLink = "https://play.google.com/store/apps/details?id=com.amarclinic"
const appStoreAppLink = "https://apps.apple.com/us/app/amarclinic/id1544914148"

const intake_url = Config.intake_url;
const room_url = Config.room_url;
const client_id = Config.client_id;
const client_secret = Config.client_secret;

const socket = () => {
    const socketConn = io(base_url)
    socketConn.on("connect", () => {
        console.log(`connect ${socketConn.connected} with id ${socketConn.id}`);
    });
    return socketConn
}

export { paytm_mid, base_url, client_id, client_secret, room_url, intake_url, agora_id, staging, translationApiKey, webClientId, socket, CONSULT_FEE, appName, appStoreAppLink, playStoreAppLink, supportEmailId };