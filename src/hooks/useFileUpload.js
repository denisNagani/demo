import { useState, useEffect } from "react";

import storage from '@react-native-firebase/storage';
import axios from 'axios';
import { Platform } from "react-native";
import { ifValid } from "../utils/ifNotValid";
import { base_url } from "../config";

export const useFileUpload = (file, token) => {
    const [progress, setProgress] = useState(0);
    const [error, setError] = useState(null);
    const [url, setUrl] = useState(null);

    // runs every time the file value changes
    useEffect(() => {
        if (file) {
            if (ifValid(file.uri)) {
                const fileName = `${new Date().getTime()}_${file?.name}`
                const fileData = file?.uri
                const uploadUri = Platform.OS === "ios" ? fileData.replace("file://", "") : fileData;

                // create form data
                const formData = new FormData();
                formData.append("file", {
                    uri: uploadUri,
                    type: file?.type,
                    name: file?.name
                });

                const request = {
                    method: "POST",
                    url: base_url + "api/users/uploadDocument",
                    data: formData,
                    headers: {
                        Authorization: "Bearer " + token,
                        "Content-Type": "multipart/form-data",
                    },
                };

                console.log('making api call', request)
                axios(request)
                    .then((response) => {
                        const statusCode = response?.status
                        if (statusCode == 200) {
                            const downloadUrl = response?.data?.data?.url
                            console.log("upload image successful ", JSON.stringify(downloadUrl))
                            setProgress(100)
                            setUrl(downloadUrl)
                        } else {
                            setError('Something went wrong...')
                        }
                    })
                    .catch((error) => {
                        console.log('api error ', JSON.stringify(error));
                        setError(error)
                    });

            } else {
                console.log("missing uri");
                setError("missing uri")
            }
        }
    }, [file]);

    return { progress, url, error };
};
