import { useState, useEffect } from "react";

import storage from '@react-native-firebase/storage';
import { Platform } from "react-native";
import { ifValid } from "../utils/ifNotValid";

export const useStorage = (file) => {
    const [progress, setProgress] = useState(0);
    const [error, setError] = useState(null);
    const [url, setUrl] = useState(null);

    // runs every time the file value changes
    useEffect(() => {
        if (file) {
            if (ifValid(file.uri)) {
                const fileName = `${new Date().getTime()}_${file.name}`
                const fileData = file.uri
                const uploadUri = Platform.OS === "ios" ? fileData.replace("file://", "") : fileData;

                let metaData = {
                    contentType: file?.type
                }
                // storage refs
                const storageRef = storage().ref("chats/" + fileName);

                storageRef.putFile(uploadUri, metaData).on(
                    "state_changed",
                    (snap) => {
                        // track the upload progress
                        let percentage = Math.round(
                            (snap.bytesTransferred / snap.totalBytes) * 100
                        );
                        setProgress(percentage);
                        console.log("file upload progress " + percentage);
                    },
                    (err) => {
                        setError(err);
                        console.log("uploading error " + error);
                    },
                    async () => {
                        // get the public download img url
                        const downloadUrl = await storageRef.getDownloadURL();

                        // save the url to local state
                        setUrl(downloadUrl);
                    }
                );
            } else {
                alert("missing uri")
            }
        }
    }, [file]);

    return { progress, url, error };
};
