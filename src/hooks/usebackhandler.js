import React, { useEffect } from 'react'
import { BackHandler } from 'react-native'

export function useBackHandler(handler, check) {
    const myHandler = () => {
        if (check) {
            handler()
            return true
        }
        return false
    }
    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', myHandler)

        return () => BackHandler.removeEventListener('hardwareBackPress', myHandler)
    }, [handler])
}