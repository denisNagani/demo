import { createStackNavigator } from "react-navigation-stack";
import DashboardWaitingScreen from "../scenes/doctor/dash/dashboard-waiting";
import DoctorUpcomingBookingsScreen from "../scenes/doctor/upcoming-bookings";
import DoctorPastBookingsScreen from "../scenes/doctor/past-bookings";
import ViewPatientIntakeFormScreen from "../scenes/doctor/patient-intake-form";
import { createDrawerNavigator } from "react-navigation-drawer";
import React from "react";
import DoctorDrawerBar from "../components/doctor-drawer";
import EditDoctorProfile from "../scenes/doctor/show-profile/edit-profile";
import DoctorLoginScreen from "../scenes/authentication/doctor-login";
import UpcomingPastBookingsDoctor from "../scenes/doctor/upcoming-past-bookings";
import AppointedDetails from "../scenes/doctor/appointed-detail";
import PrescriptionForm from "../scenes/doctor/perscription-form";
import AddMedicine from "../scenes/doctor/add-medicine";
import ShowDoctorProfile from "../scenes/doctor/show-profile";
import PDFExample from "../scenes/doctor/PDFExample";
import PrescriptionFormMedicine from "../scenes/doctor/perscription-form-medicine";
import AppointedDetailsAdded from "../scenes/doctor/appointed-detail-added";
import PatientDetails from "../scenes/patient/patient-details";
import DashboardScreen from "../scenes/doctor/dash/dashboard-scheduled";
import JoinCall from "../scenes/doctor/dash/dashboard-waiting/join_call";
import ChangePassword from "../scenes/doctor/show-profile/change-password";
import AboutUs from "../components/AboutUs";
import ContactUs from "../components/ContactUs";
import Preview from "../scenes/doctor/add-medicine/Preview";
import FAQDOCTOR from "../scenes/doctor/FAQ";
import DoctorChat from "../scenes/doctor/Chat";
import DoctorChatScreen from "../components/ChatScreen";
import Appointmentslots from "../scenes/doctor/Appointmentslots";
import AppointmentHistory from "../scenes/doctor/AppointmentHistory";

export const DoctorStack = createStackNavigator({
    DashboardScreen: {
        screen: DashboardScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    PDFExample: {
        screen: PDFExample,
        navigationOptions: () => ({ headerShown: false }),
    },
    FAQ_doctor: {
        screen: FAQDOCTOR,
        navigationOptions: () => ({ headerShown: false }),
    },
    DashboardWaitingScreen: {
        screen: DashboardWaitingScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    DoctorUpcomingBookingsScreen: {
        screen: DoctorUpcomingBookingsScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    DoctorPastBookingsScreen: {
        screen: DoctorPastBookingsScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    PatientDetails: {
        screen: PatientDetails,
        navigationOptions: () => ({ headerShown: false }),
    },
    JoinCall: {
        screen: JoinCall,
        navigationOptions: () => ({ headerShown: false }),
    },

    ViewPatientIntakeFormScreen: {
        screen: ViewPatientIntakeFormScreen,
        navigationOptions: () => ({ headerShown: false }),
    },

    EditDoctorProfile: {
        screen: EditDoctorProfile,
        navigationOptions: () => ({ headerShown: false }),
    },
    DoctorLogin: {
        screen: DoctorLoginScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    UpcomingPastBookingsDoctor: {
        screen: UpcomingPastBookingsDoctor,
        navigationOptions: () => ({ headerShown: false }),
    },
    PrescriptionForm: {
        screen: PrescriptionForm,
        navigationOptions: () => ({ headerShown: false }),
    },
    PrescriptionFormMedicine: {
        screen: PrescriptionFormMedicine,
        navigationOptions: () => ({ headerShown: false }),
    },
    AddMedicine: {
        screen: AddMedicine,
        navigationOptions: () => ({ headerShown: false }),
    },
    Preview: {
        screen: Preview,
        navigationOptions: () => ({ headerShown: false }),
    },
    AppointedDetails: {
        screen: AppointedDetails,
        navigationOptions: () => ({ headerShown: false }),
    },
    AppointedDetailsAdded: {
        screen: AppointedDetailsAdded,
        navigationOptions: () => ({ headerShown: false }),
    },
    ShowDoctorProfile: {
        screen: ShowDoctorProfile,
        navigationOptions: () => ({ headerShown: false }),
    },
    ChangePassword: {
        screen: ChangePassword,
        navigationOptions: () => ({ headerShown: false }),
    },
    about: {
        screen: AboutUs,
        navigationOptions: () => ({ headerShown: false }),
    },
    contact: {
        screen: ContactUs,
        navigationOptions: () => ({ headerShown: false }),
    },
    DoctorChat: {
        screen: DoctorChat,
        navigationOptions: () => ({ headerShown: false }),
    },
    DoctorChatScreen: {
        screen: DoctorChatScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    Appointmentslots: {
        screen: Appointmentslots,
        navigationOptions: () => ({ headerShown: false }),
    },
    AppointmentHistory: {
        screen: AppointmentHistory,
        navigationOptions: () => ({ headerShown: false }),
    },

},
    {
        defaultNavigationOptions: {
            // gesturesEnabled: false,
        },
    }
);

const DoctorNavigator = createDrawerNavigator(
    {
        DoctorStack: {
            screen: DoctorStack,
            navigationOptions: ({ navigation }) => ({
                headerShown: false,
            }),
        },
    },
    {
        initialRouteName: "DoctorStack",
        contentComponent: (props) => (
            <DoctorDrawerBar {...props} tapToClose={true} />
        ),
    }
);

const DoctorDrawer = createStackNavigator({
    drawer: {
        screen: DoctorNavigator,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
        }),
    },
});

export default DoctorDrawer;
