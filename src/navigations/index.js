import { createAppContainer, createSwitchNavigator } from "react-navigation";
import AuthLoading from "../scenes/authentication/landing";

import LandingNavigator from "./land-navigator";
import DoctorDrawer from "./doctor-navigator";
import PatientDrawer from "./patient-navigator";
import AcknowledgmentScreen from "../scenes/patient/acknowledgment";
import IncomingCallScreen from "../components/IncomingCallScreen";

const RootNavigator = createSwitchNavigator(
	{
		AuthLoading: AuthLoading,
		Land: LandingNavigator,
		Patient: PatientDrawer,
		Doctor: DoctorDrawer,
		AcknowledgmentScreen: AcknowledgmentScreen,
		IncomingCallScreen: IncomingCallScreen,
	},
	{
		initialRouteName: "AuthLoading",
	}
);

export default createAppContainer(RootNavigator);
