import { createStackNavigator } from "react-navigation-stack";

import ForgotPasswordScreen from "../scenes/authentication/forgot-password";
import SignupScreen from "../scenes/authentication/signup";
import DoctorLoginScreen from "../scenes/authentication/doctor-login";
import WaitingRoom from "../scenes/authentication/doctor-login/waiting-room";
import JoinCall from "../scenes/authentication/doctor-login/join_call";
import LoginScreen from "../scenes/authentication/signin";
import PatientLogin from "../scenes/patient/user-login";
import LoginScreenMobile from "../scenes/patient/login-whatsapp";
import OTPScreen from "../scenes/patient/otp-screen";
import GetStarted from "../scenes/patient/GetStarted";
import CareForm from "../scenes/patient/CareForm";
import BookAppointmentScreenFlow2 from "../scenes/patient/book-appointment-flow2";

const LandingNavConfig = {
    initialRouteName: "PatientLogin",
    header: null,
    headerMode: "none",
};

const RouteConfigs = {
    LoginScreen: LoginScreen,
    GetStarted: GetStarted,
    ForgotPassword: ForgotPasswordScreen,
    Signup: SignupScreen,
    WaitingRoom: WaitingRoom,
    JoinCall: JoinCall,
    DoctorLogin: DoctorLoginScreen,
    PatientLogin: PatientLogin,
    LoginScreenMobile: LoginScreenMobile,
    OTPScreen: OTPScreen,
    CareForm: CareForm,
    BookAppointmentScreenFlow2: {
        screen: BookAppointmentScreenFlow2,
        navigationOptions: () => ({ headerShown: false }),
    }
};

const LandingNavigator = createStackNavigator(RouteConfigs, LandingNavConfig);

export default LandingNavigator;
