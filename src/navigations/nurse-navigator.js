import {createStackNavigator} from "react-navigation-stack";
import {createDrawerNavigator} from "react-navigation-drawer";
import React from "react";
import NurseDashBoard from "../scenes/nurse/dashboard";
import NurseWaitingScreen from "../scenes/nurse/dashboard-waiting";
import NurseDrawerBar from "../components/nurse-drawer";
import JoinCall from "../scenes/nurse/dashboard-waiting/join_call";
import ViewPatientIntakeFormScreen from "../scenes/doctor/patient-intake-form";


export const NurseStack = createStackNavigator({
        NurseDashBoard: {
            screen: NurseDashBoard,
            navigationOptions: () => ({headerShown: false}),
        },
        NurseWaitingScreen: {
            screen: NurseWaitingScreen,
            navigationOptions: () => ({headerShown: false}),
        },
        JoinCall: {
            screen: JoinCall,
            navigationOptions: () => ({headerShown: false}),
        },
        ViewPatientIntakeFormScreen: {
            screen: ViewPatientIntakeFormScreen,
            navigationOptions: () => ({headerShown: false}),
        },
    },
    {
        defaultNavigationOptions: {
            gesturesEnabled: false,
        },
    }
);

const NurseNavigator = createDrawerNavigator(
    {
        NurseStack: {
            screen: NurseStack,
            navigationOptions: ({navigation}) => ({
                headerShown: false,
            }),
        },
    },
    {
        initialRouteName: "NurseStack",
        contentComponent: (props) => <NurseDrawerBar {...props} tapToClose={true}/>,
    }
);

const NurseDrawer = createStackNavigator({
    drawer: {
        screen: NurseNavigator,
        navigationOptions: ({navigation}) => ({
            headerShown: false,
        }),
    },
});

export default NurseDrawer;

