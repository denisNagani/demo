import { createStackNavigator } from "react-navigation-stack";
import HomeScreen from "../scenes/patient/home";
import PatientPersonalDetails from "../scenes/patient/personal-details";
import BookAppointmentScreen from "../scenes/patient/book-appointment";
import UpcomingPastBookings from "../scenes/patient/upcoming-past-bookings";
import JoinCall from "../scenes/patient/upcoming-past-bookings/join_call";
import PatientProfileScreen from "../scenes/patient/profile";
import EditPatientProfile from "../scenes/patient/edit-profile";
import WaitingRoom from "../scenes/patient/upcoming-past-bookings/waiting-room";
import { createDrawerNavigator } from "react-navigation-drawer";
import React from "react";
import PatientDrawerBar from "../components/patient-drawer";
import DoctorLoginScreen from "../scenes/authentication/doctor-login";
import changePassword from "../scenes/patient/change-password";
import FindAndBook from "../scenes/patient/find-and-book";
import BookingSummaryScreen from "../scenes/patient/booking-summary";
import PaymentScreen from "../scenes/patient/payment-screen";
import UserLoginScreen from "../scenes/patient/user-login";
import LoginScreenWhatsapp from "../scenes/patient/login-whatsapp";
import OTPScreen from "../scenes/patient/otp-screen";
import PatientDetails from "../scenes/patient/patient-details";
import SelectSpeciality from "../scenes/patient/select-speciality";
import SelectAndBook from "../scenes/patient/select-and-book";
import SelectAndBook2 from "../scenes/patient/select-and-book-2";
import BookAppointmentScreenFlow2 from "../scenes/patient/book-appointment-flow2";
import AppointmentDetails from "../scenes/patient/appointment-detail";
import AboutUs from "../components/AboutUs";
import ContactUs from "../components/ContactUs";
import FilterScreen from "../scenes/patient/select-and-book/filter-screen";
import ApplyCoupon from "../scenes/patient/applyCoupon";
import PaymentDashScreen from "../scenes/patient/Payment";
import FAQ from "../scenes/patient/FAQ";
import Blog from "../scenes/patient/Blog";
import BlogDetails from "../scenes/patient/Blog/BlogDetails";
import PatientChat from "../scenes/patient/Chat";
import PatientChatScreen from "../components/ChatScreen";
import CareForm from "../scenes/patient/CareForm";
import OfferScreen from "../scenes/patient/offer";
import Membership from "../scenes/patient/Membership";
import PlanDetails from "../scenes/patient/Membership/PlanDetails";
import Success from "../scenes/patient/Membership/Success";
import MyHealthCard from "../scenes/patient/MyHealthCard";
import Notification from "../scenes/patient/Notification";
import QuickDoc from "../scenes/patient/QuickDoc";
import DoctorsBySpeciality from "../scenes/patient/doctorsBySpeciality";
import PaymentReceiptModal from "../components/PaymentReceiptModal";
import SelectMember from "../scenes/patient/members/index";
import AddEditMember from "../scenes/patient/members/AddEditMember/index";
import LabTest from "../scenes/patient/LabTest/index";
import OrderMedicine from "../scenes/patient/OrderMedicine";
import RequestReceived from "../scenes/patient/RequestReceived/index";
import MedicineRequestReceived from "../scenes/patient/MedicineRequestReceived";

const PatientStack = createStackNavigator({
    Home: {
        screen: HomeScreen,
        navigationOptions: ({ navigation }) => ({ headerShown: false }),
    },
    PaymentScreen: {
        screen: PaymentScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    FindAndBook: {
        screen: FindAndBook,
        navigationOptions: () => ({ headerShown: false }),
    },
    Blog: {
        screen: Blog,
        navigationOptions: () => ({ headerShown: false }),
    },
    BlogDetails: {
        screen: BlogDetails,
        navigationOptions: () => ({ headerShown: false }),
    },
    SelectSpeciality: {
        screen: SelectSpeciality,
        navigationOptions: () => ({ headerShown: false }),
    },
    SelectAndBook: {
        screen: SelectAndBook,
        navigationOptions: () => ({ headerShown: false }),
    },
    SelectAndBook2: {
        screen: SelectAndBook2,
        navigationOptions: () => ({ headerShown: false }),
    },
    BookAppointmentScreenFlow2: {
        screen: BookAppointmentScreenFlow2,
        navigationOptions: () => ({ headerShown: false }),
    },
    PatientPersonalDetails: {
        screen: PatientPersonalDetails,
        navigationOptions: () => ({ headerShown: false }),
    },
    BookAppointmentScreen: {
        screen: BookAppointmentScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    LoginScreenWhatsapp: {
        screen: LoginScreenWhatsapp,
        navigationOptions: () => ({ headerShown: false }),
    },
    OTPScreen: {
        screen: OTPScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    PatientDetails: {
        screen: PatientDetails,
        navigationOptions: () => ({ headerShown: false }),
    },
    UserLoginScreen: {
        screen: UserLoginScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    AppointmentDetails: {
        screen: AppointmentDetails,
        navigationOptions: ({ navigation }) => ({ headerShown: false }),
    },
    UpcomingPastBookings: {
        screen: UpcomingPastBookings,
        navigationOptions: ({ navigation }) => ({ headerShown: false }),
    },
    ApplyCoupon: {
        screen: ApplyCoupon,
        navigationOptions: ({ navigation }) => ({ headerShown: false }),
    },
    PatientChat: {
        screen: PatientChat,
        navigationOptions: ({ navigation }) => ({ headerShown: false }),
    },
    PatientChatScreen: {
        screen: PatientChatScreen,
        navigationOptions: ({ navigation }) => ({ headerShown: false }),
    },

    // PastBookings: {
    //     screen: PastBookings,
    //     navigationOptions: () => ({headerShown: false}),
    // },

    JoinCall: {
        screen: JoinCall,
        navigationOptions: ({ navigation }) => ({ headerShown: false, swipeEnabled: false }),
    },
    PatientProfileScreen: {
        screen: PatientProfileScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    EditPatientProfileScreen: {
        screen: EditPatientProfile,
        navigationOptions: () => ({ headerShown: false }),
    },
    ChangePasswordScreen: {
        screen: changePassword,
        navigationOptions: () => ({ headerShown: false }),
    },
    WaitingRoom: {
        screen: WaitingRoom,
        navigationOptions: ({ navigation }) => ({ headerShown: false }),
    },
    DoctorLogin: {
        screen: DoctorLoginScreen,
        navigationOptions: ({ navigation }) => ({ headerShown: false }),
    },
    BookingSummaryScreen: {
        screen: BookingSummaryScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    about: {
        screen: AboutUs,
        navigationOptions: () => ({ headerShown: false }),
    },
    contact: {
        screen: ContactUs,
        navigationOptions: () => ({ headerShown: false }),
    },
    FilterScreen: {
        screen: FilterScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    FAQ: {
        screen: FAQ,
        navigationOptions: () => ({ headerShown: false }),
    },
    CareForm: {
        screen: CareForm,
        navigationOptions: () => ({ headerShown: false }),
    },
    offerScreen: {
        screen: OfferScreen,
        navigationOptions: () => ({ headerShown: false }),
    },
    Membership: {
        screen: Membership,
        navigationOptions: () => ({ headerShown: false }),
    },
    PlanDetails: {
        screen: PlanDetails,
        navigationOptions: () => ({ headerShown: false }),
    },
    Success: {
        screen: Success,
        navigationOptions: () => ({ headerShown: false }),
    },
    MyHealthCard: {
        screen: MyHealthCard,
        navigationOptions: () => ({ headerShown: false }),
    },
    Notification: {
        screen: Notification,
        navigationOptions: () => ({ headerShown: false }),
    },
    QuickDoc: {
        screen: QuickDoc,
        navigationOptions: () => ({ headerShown: false }),
    },
    DoctorsBySpeciality: {
        screen: DoctorsBySpeciality,
        navigationOptions: () => ({ headerShown: false }),
    },
    PaymentReceiptModal: {
        screen: PaymentReceiptModal,
        navigationOptions: () => ({ headerShown: false }),
    },
    SelectMember: {
        screen: SelectMember,
        navigationOptions: () => ({ headerShown: false }),
    },
    AddEditMember: {
        screen: AddEditMember,
        navigationOptions: () => ({ headerShown: false }),
    },
    LabTest: {
        screen: LabTest,
        navigationOptions: () => ({ headerShown: false }),
    },
    OrderMedicine: {
        screen: OrderMedicine,
        navigationOptions: () => ({ headerShown: false }),
    },
    RequestReceived: {
        screen: RequestReceived,
        navigationOptions: () => ({ headerShown: false }),
    },
    MedicineRequestReceived: {
        screen: MedicineRequestReceived,
        navigationOptions: () => ({ headerShown: false }),
    },
},
    {
        defaultNavigationOptions: {
            // gesturesEnabled: false,
        },
    }
);

const PatientNavigator = createDrawerNavigator(
    {
        PatientStack: {
            screen: PatientStack,
            navigationOptions: ({ navigation }) => ({
                headerShown: false,
            }),
        },
    },
    {
        initialRouteName: "PatientStack",
        contentComponent: (props) => (
            <PatientDrawerBar {...props} tapToClose={true} />
        ),
    }
);

const PatientDrawer = createStackNavigator({
    drawer: {
        screen: PatientNavigator,
        navigationOptions: ({ navigation }) => ({
            headerShown: false,
        }),
    },
});

export default PatientDrawer;
