import { createAppContainer, createSwitchNavigator } from "react-navigation";

import LandingNavigator from "./land-navigator";
import DoctorDrawerFlow2 from "./doctor-navigator-flow2";
// import IncomingCallScreen from "../components/IncomingCallScreen";
import AuthLoading from "../scenes/authentication/landing";
import JoinCall from "../scenes/doctor/dash/dashboard-waiting/join_call";

const RootNavigator = createSwitchNavigator(
    {
        AuthLoading: AuthLoading,
        Doctor: DoctorDrawerFlow2,
        Land: LandingNavigator,
        JoinCall: JoinCall
    },
    {
        initialRouteName: "AuthLoading",
    }
);

export default createAppContainer(RootNavigator);
