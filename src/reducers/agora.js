import {SHOW_PRESCRIPTION_MODEL} from "../utils/constant";

const initialState = {
    presModel: false,
};

const agoraReducer = (state = initialState, action) => {

    switch (action.type) {
        case SHOW_PRESCRIPTION_MODEL:
            return {...state, presModel: action.payload};
        default:
            return state;
    }
};
export default agoraReducer;
