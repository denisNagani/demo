import {ADD_MEDICINE_RESPONSE, GET_APPOINTED_DETAILS, PDF_URL} from "../utils/constant";

const initialState = {
    appointedDetails: {},
    pdfUrl: '',
};

const appointedReducer = (state = initialState, action) => {

    switch (action.type) {
        case GET_APPOINTED_DETAILS:
            return {...state, appointedDetails: action.payload};
        case PDF_URL:
            return {...state, pdfUrl: action.payload};
        default:
            return state;
    }
};
export default appointedReducer;
