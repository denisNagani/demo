import {
    BOOKING_FLOW,
    DIRECT_LOGIN,
    DOCTOR_SLOT,
    POST_DOCTOR_SLOT,
    SELECTED_DOCTOR,
    SELECTED_SLOT_OBJ,
    SLOT_TIMEZONE,
    START_RECORDING,
    STOP_RECORDING,
    SUMMARY_DATE_N_TIME,
    COUPON_APPLIED,
    FOLLOW_UP,
    SUB_PLANS,
    SELECT_PLAN_MODEL,
    SET_MEMBERSHIP_ID,
    SET_JOIN_CALL_DATA,
    SET_APPOINTMENT_TYPE,
    SELECTED_SLOT_OBJ_FOR_QUICK_CALL
} from "../utils/constant";

const initialState = {
    docSlot: [],
    postDocRes: {},
    directLogin: {},
    startRecording: {},
    stopRecording: {},
    slotTimeZone: {},
    selectedSlotObj: {},
    bookingFlow1or2: 0,
    selectedDoctor: {},
    summaryDateNTime: {},
    appliedCoupon: null,
    followUpData: null,
    subPlans: {
        plans: [],
        couponData: null
    },
    selectPlanModal: false,
    membershipId: null,
    joinCallDoctorData: null,
    appointmentType: null,
    selectedSlotObjForQuickBook: {},
};

const appointment = (state = initialState, action) => {

    switch (action.type) {
        case DOCTOR_SLOT:
            return { ...state, docSlot: action.payload };
        case SLOT_TIMEZONE:
            return { ...state, slotTimeZone: action.payload };
        case POST_DOCTOR_SLOT:
            return { ...state, postDocRes: action.payload };
        case DIRECT_LOGIN:
            return { ...state, directLogin: action.payload };
        case START_RECORDING:
            return { ...state, startRecording: { stopped: false, ...action.payload } };
        case STOP_RECORDING:
            return { ...state, stopRecording: action.payload, startRecording: { ...state.startRecording, stopped: true } };
        case SELECTED_SLOT_OBJ:
            return { ...state, selectedSlotObj: action.payload };
        case BOOKING_FLOW:
            console.log("flow == ", action.payload);
            return { ...state, bookingFlow1or2: action.payload };
        case SELECTED_DOCTOR:
            return { ...state, selectedDoctor: action.payload };
        case SUMMARY_DATE_N_TIME:
            return { ...state, summaryDateNTime: action.payload };
        case COUPON_APPLIED:
            return { ...state, appliedCoupon: action.payload };
        case FOLLOW_UP:
            return { ...state, followUpData: action.payload };
        case SUB_PLANS:
            return { ...state, subPlans: action.payload };
        case SELECT_PLAN_MODEL:
            return { ...state, selectPlanModal: action.payload };
        case SET_MEMBERSHIP_ID:
            return { ...state, membershipId: action.payload };
        case SET_JOIN_CALL_DATA:
            return { ...state, joinCallDoctorData: action.payload };
        case SET_APPOINTMENT_TYPE:
            return { ...state, appointmentType: action.payload };
        case SELECTED_SLOT_OBJ_FOR_QUICK_CALL:
            return { ...state, selectedSlotObjForQuickBook: action.payload };
        default:
            return state;
    }
};
export default appointment;
