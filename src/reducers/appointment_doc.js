import { GET_ALL_DOCTOR_LEAVES, GET_SLOTS_BY_DATE } from "../utils/constant";

const initialState = {
    // slotsByDate: [],
    slotsByDate: [],
    allLeavesTaken: [],
};

const appointment = (state = initialState, action) => {

    switch (action.type) {
        case GET_SLOTS_BY_DATE:
            return { ...state, slotsByDate: action.payload };
        case GET_ALL_DOCTOR_LEAVES:
            return { ...state, allLeavesTaken: action.payload };
        default:
            return state;
    }
};
export default appointment;
