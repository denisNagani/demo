import {
	BOOKING_HISTORY,
	UPCOMING_BOOKINGS,
	UPCOMING_BOOKINGS_BY_DATE,
	DOCTOR_NAME,
	DOCTOR_IMAGE,
	DOCTOR_WAITING_LIST,
	DOCTOR_UPDATE_ROOM,
	SELECTED_SLOT,
	SELECTED_APPOINTMENT,
	SELECTED_HEADER,
	PAST_BOOKINGS,
	PATIENT_ON_WAITING_SCREEN
} from "../utils/constant";

const initialState = {
	upcomingBookings: [],
	pastBookings: [],
	upcomingBookingsByDate: [],
	bookingsHistory: [],
	docName: "",
	docImage: "",
	docWaitingList: [],
	updateRoom: {},
	selectedSlot: {},
	selectedAppointment: -1,
	selectedHeader: -1,
	patientOnWaitingScreen: false
};

const bookings = (state = initialState, action) => {
	switch (action.type) {
		case UPCOMING_BOOKINGS:
			return { ...state, upcomingBookings: action.payload };
		case PAST_BOOKINGS:
			return { ...state, pastBookings: action.payload };
		case BOOKING_HISTORY:
			return { ...state, bookingsHistory: action.payload };
		case DOCTOR_NAME:
			return { ...state, docName: action.payload };
		case DOCTOR_IMAGE:
			return { ...state, docImage: action.payload };
		case DOCTOR_WAITING_LIST:
			return { ...state, docWaitingList: action.payload };
		case DOCTOR_UPDATE_ROOM:
			return { ...state, updateRoom: action.payload };
		case UPCOMING_BOOKINGS_BY_DATE:
			return { ...state, upcomingBookingsByDate: action.payload };
		case SELECTED_SLOT:
			return { ...state, selectedSlot: action.payload };
		case SELECTED_APPOINTMENT:
			return { ...state, selectedAppointment: action.payload };
		case SELECTED_HEADER:
			return { ...state, selectedHeader: action.payload };
		case PATIENT_ON_WAITING_SCREEN:
			return { ...state, patientOnWaitingScreen: action.payload };

		default:
			return state;
	}
};
export default bookings;
