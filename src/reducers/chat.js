import { SET_FILE_URL, CLEAR_FILE_URL, GET_CHAT_DATA, SET_DOC_URL, CLEAR_DOC_URL } from "../utils/constant";

let intiState = {
    url: null,
    type: "",
    chatData: [],
    docUrl: null
};

const loader = (state = intiState, action) => {
    switch (action.type) {
        case SET_FILE_URL:
            return { ...state, url: action.payload.url, type: action.payload.type };
        case CLEAR_FILE_URL:
            return { ...state, url: action.payload };
        case SET_DOC_URL:
            return { ...state, docUrl: action.payload };
        case CLEAR_DOC_URL:
            return { ...state, docUrl: action.payload };
        case GET_CHAT_DATA:
            return { ...state, chatData: action.payload };
        default:
            return state;
    }
};

export default loader;
