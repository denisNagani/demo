import { PATIENT_GUEST_RESPONSE, INCOMING_URL, DIRECT_MODEL, GET_ALL_BLOGS, GET_ALL_MEMBERSHIP_PLANS, SUBSCRIPTION_OBJ, SET_PLAN_DETAILS, SET_SEHET_CARD_FLOW, SET_FIREBASE_MEM_DATA } from "../utils/constant";

let intiState = {
    url: "",
    patientGuest: {},
    modelVisible: false,
    blogs: [],
    membershipCards: [],
    sehetCardFlow: false,
    subObj: null,
    planDetails: null,
    firebaseData: null
};
const directLogin = (state = intiState, action) => {
    switch (action.type) {
        case INCOMING_URL:
            return { ...state, url: action.payload };
        case PATIENT_GUEST_RESPONSE:
            return { ...state, patientGuest: action.payload };
        case DIRECT_MODEL:
            return { ...state, modelVisible: action.payload };
        case GET_ALL_BLOGS:
            return { ...state, blogs: action.payload };
        case GET_ALL_MEMBERSHIP_PLANS:
            return { ...state, membershipCards: action.payload };
        case SUBSCRIPTION_OBJ:
            return { ...state, subObj: action.payload };
        case SET_PLAN_DETAILS:
            return { ...state, planDetails: action.payload };
        case SET_SEHET_CARD_FLOW:
            return { ...state, sehetCardFlow: action.payload };
        case SET_FIREBASE_MEM_DATA:
            return { ...state, firebaseData: action.payload };
        default:
            return state;
    }
};

export default directLogin;
