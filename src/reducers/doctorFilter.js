import {
    AVAILABILITY_FILTER,
    FILTER_CLEAR,
    GENDER_FILTER,
    SPECIALITY_FILTER_LIST,
    SPECIALITY_FILTER_SELECTED_LIST,
    FILTER_APPLIED,
    CITY_FILTER_LIST,
    CITY_FILTER_SELECTED_LIST,
    FILTER_BODY
} from "../utils/constant";


const genderFilter = [
    { label: 'Male', value: false },
    { label: 'Female', value: false },
    { label: 'Both', value: false },
    { label: 'Not Applicable', value: false },
];


const initialState = {
    specialityFilter: [
        // {id: 1, name: 'Before Breakfast', value: false},
        // {id: 2, name: 'After Breakfast', value: false},
        // {id: 3, name: 'Before Lunch', value: false},
        // {id: 4, name: 'Before Lunch', value: false},
        // {id: 5, name: 'Before Lunch', value: false},
        // {id: 6, name: 'Before Lunch', value: false},
    ],
    specialityFilterSelected: [
        // {id: 1, name: 'Before Breakfast', value: false},
        // {id: 2, name: 'After Breakfast', value: false},
        // {id: 3, name: 'Before Lunch', value: false},
        // {id: 4, name: 'Before Lunch', value: false},
        // {id: 5, name: 'Before Lunch', value: false},
        // {id: 6, name: 'Before Lunch', value: false},
    ],
    cityFilter: [],
    cityFilterSelected: [],
    genderFilter: genderFilter,
    avalibilty: '',
    filterApplied: false,
    filterBody: null
};

const doctorFilterReducer = (state = initialState, action) => {

    switch (action.type) {
        case SPECIALITY_FILTER_LIST:
            return { ...state, specialityFilter: action.payload };
        case SPECIALITY_FILTER_SELECTED_LIST:
            return { ...state, specialityFilterSelected: action.payload };
        case CITY_FILTER_LIST:
            return { ...state, cityFilter: action.payload };
        case CITY_FILTER_SELECTED_LIST:
            return { ...state, cityFilterSelected: action.payload };
        case GENDER_FILTER:
            return { ...state, genderFilter: action.payload };
        case AVAILABILITY_FILTER:
            return { ...state, avalibilty: action.payload };
        case FILTER_CLEAR:
            return {
                ...state,
                specialityFilterSelected: state.specialityFilter,
                cityFilterSelected: state.cityFilter,
                genderFilter: genderFilter,
                avalibilty: '',
                filterApplied: false
            };
        case FILTER_APPLIED:
            return { ...state, filterApplied: action.payload }
        case FILTER_BODY:
            return { ...state, filterBody: action.payload }

        default:
            return state;
    }
};
export default doctorFilterReducer;
