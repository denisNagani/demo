import profile from "./profile";
import { combineReducers } from "redux";
import appointment from "./appointment";
import bookings from "./bookings";
import loader from "./loader";
import modal from "./modal";
import directLogin from "./direct-login";
import shareRoom from "./share-room";
import specialitiesReducer from "./speciality-reducer";
import userReducer from "./user-reducer";
import upcomingPast from "./upcommingPast";
import prescriptionReducer from "./prescription";
import appointedReducer from "./appointed";
import agoraReducer from "./agora";
import { USER_LOGOUT } from "../utils/constant";
import doctorFilterReducer from "./doctorFilter";
import chat from "./chat";
import appointment_doc from "./appointment_doc";

const appReducer = combineReducers({
	profile,
	appointment,
	bookings,
	loader,
	modal,
	directLogin,
	shareRoom,
	specialitiesReducer,
	userReducer,
	upcomingPast,
	prescriptionReducer,
	appointedReducer,
	agoraReducer,
	doctorFilterReducer,
	chat,
	appointment_doc
});


const rootReducer = (state, action) => {
	if (action.type === USER_LOGOUT) {
		state = undefined
	}
	return appReducer(state, action)
}

export default rootReducer;
