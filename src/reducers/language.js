import { GET_APP_LANGUAGE, SET_APP_LANGUAGE } from "../utils/constant";

let intiState = {
    lang: 'en',
};

const loader = (state = intiState, action) => {
    switch (action.type) {
        case GET_APP_LANGUAGE:
            return { ...state, lang: action.payload };
        case SET_APP_LANGUAGE:
            return { ...state, lang: action.payload };
        default:
            return state;
    }
};

export default loader;
