let intiState = {
	isLoading: false,
};
const loader = (state = intiState, action) => {
	switch (action.type) {
		case "SHOW_LOADER":
			return { ...state, isLoading: action.payload };
		case "HIDE_LOADER":
			return { ...state, isLoading: action.payload };
		default:
			return state;
	}
};

export default loader;
