import { CLOCK_TIME, HIDE_APPOINTMENT_MODAL, HIDE_MODAL, SHOW_APPOINTMENT_MODAL, SHOW_MODAL, SHOW_MEDIA_MODAL, HIDE_MEDIA_MODAL, MIC_MODEL, TC_MODAL, SET_COUNTRY, SET_PHOTO_PICKER_MODAL } from "../utils/constant";

let intiState = {
    modalVisible: false,
    iconName: "",
    text: "",
    subText: "",
    iconColor: "",
    button: false,
    isIcon: true,
    appointmentModal: false,
    clockTime: 3,
    mediaModal: false,
    micModal: false,
    tcModal: {
        visible: false
    },
    country: false,
    photoPickerModal: false
};

const modal = (state = intiState, action) => {
    switch (action.type) {
        case SHOW_MODAL:
            return {
                ...state,
                modalVisible: action.payload.modalVisible,
                iconName: action.payload.iconName,
                text: action.payload.text,
                subText: action.payload.subText,
                iconColor: action.payload.iconColor,
                button: action.payload.button,
                isIcon: action.payload.isIcon,
            };
        case HIDE_MODAL:
            return {
                ...state,
                modalVisible: action.payload,
            };
        case SHOW_APPOINTMENT_MODAL:
            return {
                ...state,
                appointmentModal: action.payload,
            };
        case HIDE_APPOINTMENT_MODAL:
            return { ...state, appointmentModal: action.payload };

        case SHOW_MEDIA_MODAL:
            return {
                ...state,
                mediaModal: action.payload,
            };
        case HIDE_MEDIA_MODAL:
            return { ...state, mediaModal: action.payload };

        case CLOCK_TIME:
            return { ...state, clockTime: action.payload };

        case MIC_MODEL:
            return { ...state, micModal: action.payload };

        case TC_MODAL:
            return { ...state, tcModal: action.payload };

        case SET_COUNTRY:
            return { ...state, country: action.payload };
        case SET_PHOTO_PICKER_MODAL:
            return { ...state, photoPickerModal: action.payload };
        default:
            return state;
    }
};

export default modal;
