import {
    ADD_MEDICINE_ID,
    ADD_MEDICINE_RESPONSE,
    PRESCRIPTION_DATA,
    SELECTED_APPOINTMENT_WAITING,
    SELECTED_PATIENT
} from "../utils/constant";

const initialState = {
    prescription: {},
    selectedPatient: {},
    addMedicineId: {},
    selectedAppointment: {},
};

const prescriptionReducer = (state = initialState, action) => {

    switch (action.type) {
        case SELECTED_APPOINTMENT_WAITING:
            return { ...state, selectedAppointment: action.payload };
        case ADD_MEDICINE_ID:
            return { ...state, addMedicineId: action.payload };
        case ADD_MEDICINE_RESPONSE:
            return { ...state, prescription: action.payload };
        case SELECTED_PATIENT:
            return { ...state, selectedPatient: action.payload };
        case PRESCRIPTION_DATA:
            return {
                ...state,
                selectedPatient: action.payload?.selectedPatient,
                selectedAppointment: action.payload?.selectedAppointment,
                addMedicineId: action.payload?.addMedicineId,
            };
        default:
            return state;
    }
};
export default prescriptionReducer;
