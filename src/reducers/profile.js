import {
    BOOKING_HISTORY,
    GET_SPECIALITIES,
    IMAGE_SUCCESS,
    LOGIN_REFRESH,
    UPCOMING_BOOKINGS,
    USER_PROFILE,
    SELECTED_LANGUAGE,
    SET_FROM_DYNAMIC_LINK,
    SET_MY_HEALTH_CARD,
    SET_LOCATION_DATA,
    SET_MEMBERS,
    SET_MEMBERS_EXTRADATA
} from "../utils/constant";

const initialState = {
    userProfile: {},
    loginRefresh: {},
    specialities: [],
    imageSuccess: {},
    selected_language: 'en',
    fromDynamicLink: {
        value: false,
        data: null
    },
    myHealthCard: null,
    locationData: null,
    members: [],
    membersExtraData: [],
};

const profile = (state = initialState, action) => {

    switch (action.type) {
        case USER_PROFILE:
            return { ...state, userProfile: action.payload };
        case GET_SPECIALITIES:
            return { ...state, specialities: action.payload };
        case LOGIN_REFRESH:
            return { ...state, loginRefresh: action.payload };
        case IMAGE_SUCCESS:
            return { ...state, imageSuccess: action.payload };
        case SELECTED_LANGUAGE:
            return { ...state, selected_language: action.payload };
        case SET_FROM_DYNAMIC_LINK:
            return {
                ...state, fromDynamicLink: {
                    value: action.payload.value,
                    data: action.payload.data
                }
            };
        case SET_MY_HEALTH_CARD:
            return { ...state, myHealthCard: action.payload };
        case SET_LOCATION_DATA:
            return { ...state, locationData: action.payload };
        case SET_MEMBERS:
            return { ...state, members: action.payload };
        case SET_MEMBERS_EXTRADATA:
            return { ...state, membersExtraData: action.payload };
        default:
            return state;
    }
};
export default profile;
