import {GET_GUEST_URL, GET_SLOTS, POST_SHARE_ROOM} from "../utils/constant";

const initialState = {
    shareRoom: {},
    slots: [],
    guestUrl : {}
};

const shareRoom = (state = initialState, action) => {

    switch (action.type) {
        case POST_SHARE_ROOM:
            return {...state, shareRoom: action.payload};
        case GET_SLOTS:
            return {...state, slots: action.payload};
        case GET_GUEST_URL:
            return {...state, guestUrl: action.payload};
        default:
            return state;
    }
};
export default shareRoom;
