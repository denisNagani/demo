import { GET_SPECIALITIES, GET_TOP_SPECIALITIES, SELECTED_SPECIALITY, USER_PROFILE, UPDATE_PRICE, APPLY_COUPOUN_MODAL, SET_DISCOUNT, GET_HOSPITALS } from "../utils/constant";

const initialState = {
    userProfile: {},
    specialities: [],
    specialitiesTop: [],
    selectedSpeciality: {},
    selectedSpeciality: {},
    applyCouponModal: false,
    dis_price: 0,
    hospitals: []
};

const specialitiesReducer = (state = initialState, action) => {

    switch (action.type) {
        case USER_PROFILE:
            return { ...state, userProfile: action.payload };
        case GET_SPECIALITIES:
            return { ...state, specialities: action.payload };
        case GET_TOP_SPECIALITIES:
            return { ...state, specialitiesTop: action.payload };
        case SELECTED_SPECIALITY:
            return { ...state, selectedSpeciality: action.payload };
        case APPLY_COUPOUN_MODAL:
            return { ...state, applyCouponModal: action.payload };
        case UPDATE_PRICE:
            return {
                ...state, selectedSpeciality: {
                    ...state.selectedSpeciality,
                    price: action.payload
                }
            };
        case SET_DISCOUNT:
            return {
                ...state,
                dis_price: action.payload
            };
        case GET_HOSPITALS:
            return {
                ...state,
                hospitals: action.payload
            };

        default:
            return state;
    }
};
export default specialitiesReducer;
