import { PREVIEW_TAB_SCREEN, PREVIEW_TAB_DETAIL_SCREEN, APPOINTMENT_SLOT_ACTIVE } from "../utils/constant";

const initialState = {
    active: 'UPCOMING',
    detailActive: 'UPCOMING',
    active_tab: 'MEDICINES',
    detailActive_tab: 'MEDICINES',
    activeTab: 0,
    appointmentActive: "APPOINTMENT_SLOT"
};

const upcomingPast = (state = initialState, action) => {

    switch (action.type) {
        case 'UPCOMING_PAST_ACTIVE':
            return { ...state, active: action.payload };
        case 'UPCOMING_PAST_DETAIL_ACTIVE':
            return { ...state, detailActive: action.payload };
        case 'SET_ACTIVE_TAB':
            return { ...state, activeTab: action.payload };
        case PREVIEW_TAB_SCREEN:
            return { ...state, active_tab: action.payload };
        case PREVIEW_TAB_DETAIL_SCREEN:
            return { ...state, detailActive_tab: action.payload };
        case APPOINTMENT_SLOT_ACTIVE:
            return { ...state, appointmentActive: action.payload };
        default:
            return state;
    }
};
export default upcomingPast;
