import {
    DOC_PROFILE,
    FEMALE_CHECK,
    GET_DOC_LIST,
    GET_DOC_LIST_TEMP,
    GET_OFFERS,
    GET_BANNERS,
    MALE_CHECk,
    OTP_RESPONSE_GOOGLE,
    OTP_SESSION,
    PATIENT_ADDED, SEARCH_DOCTOR_NAME,
    USER_PROFILE,
    VERIFIED_EMAIL,
    VERIFIED_MOBILE,
    VERIFY_SLOT_EXIST,
    GET_DOC_LIST_TOP,
    OTP_USING,
    GET_FAQS,
    QUICK_DOC,
    KEYWORD_SUGGESTION,
    EMPTY_DOC_LIST,
    GET_NEARBY_DOC,
    GET_DOCTOR_BY_SPECIALITY
} from "../utils/constant";

const initialState = {
    userProfile: {},
    docList: [],
    docListTop: [],
    docListTemp: [],
    docProfile: {},
    patientAdded: false,
    slotStillExist: false,
    otpResponseGoogle: {},
    verifiedMobile: '',
    verifiedEmail: '',
    otpSession: '',
    otpUsing: '',
    offerUrls: [],
    maleCheck: false,
    femaleCheck: false,
    searchDocName: '',
    bannerUrls: [],
    FAQS: [],
    quickDoc: {},
    keywordSuggestions: [],
    nearbyDocs: [],
    doctorBySpeciality: [],
    doctorBySpecialityTemp: []
};

const userReducer = (state = initialState, action) => {

    switch (action.type) {
        case USER_PROFILE:
            return { ...state, userProfile: action.payload };
        case GET_DOC_LIST_TOP:
            return { ...state, docListTop: action.payload };
        case GET_DOC_LIST:
            let docs = [...state.docList, ...action.payload]
            return { ...state, docList: docs };
        case EMPTY_DOC_LIST:
            return { ...state, docList: [] };
        case GET_DOC_LIST_TEMP:
            return { ...state, docListTemp: action.payload };
        case DOC_PROFILE:
            return { ...state, docProfile: action.payload };
        case PATIENT_ADDED:
            return { ...state, patientAdded: action.payload };
        case VERIFY_SLOT_EXIST:
            return { ...state, slotStillExist: action.payload };
        case OTP_RESPONSE_GOOGLE:
            return { ...state, otpResponseGoogle: action.payload };
        case OTP_SESSION:
            return { ...state, otpSession: action.payload };
        case OTP_USING:
            return { ...state, otpUsing: action.payload };
        case VERIFIED_MOBILE:
            return { ...state, verifiedMobile: action.payload };
        case VERIFIED_EMAIL:
            return { ...state, verifiedEmail: action.payload };
        case GET_OFFERS:
            return { ...state, offerUrls: action.payload };
        case GET_BANNERS:
            return { ...state, bannerUrls: action.payload };
        case GET_FAQS:
            return { ...state, FAQS: action.payload };
        case MALE_CHECk:
            return { ...state, maleCheck: action.payload };
        case FEMALE_CHECK:
            return { ...state, femaleCheck: action.payload };
        case SEARCH_DOCTOR_NAME:
            return { ...state, searchDocName: action.payload };
        case QUICK_DOC:
            return { ...state, quickDoc: action.payload };
        case KEYWORD_SUGGESTION:
            return { ...state, keywordSuggestions: action.payload };
        case GET_NEARBY_DOC:
            return { ...state, docList: action.payload };
        case GET_DOCTOR_BY_SPECIALITY:
            let finalData = action?.payload?.search == true ? {
                doctorBySpeciality: action?.payload?.data
            } : {
                doctorBySpeciality: action?.payload?.data,
                doctorBySpecialityTemp: action?.payload?.data,
            }
            return { ...state, ...finalData };
        default:
            return state;
    }
};
export default userReducer;
