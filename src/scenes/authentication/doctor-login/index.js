import React, {Component} from "react";
import {BackHandler, Image, ImageBackground, View} from "react-native";
import {Button, CheckBox, Content, Form, Icon, Input, Item, Label, Text,} from "native-base";
import Images from "../../../assets/images";
import {callNumber} from "../../../utils/helpline-no";
import styles from "./style";
import PoliciesModel from "../../../components/policies-model";
import InputModel from "../../../components/model-input";
import moment from "moment";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {postDirectLogin} from "../../../actions/appointment";
import {setCallHeader} from "../../../actions/bookings";
import {checkUrlGuestPatient, setIncomingUrl, setModelVisible} from "../../../actions/direct-login";
import {methodLog, recordError, successLog} from "../../../utils/fireLog";

const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

class DoctorLoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: "",
            roomUrl: "",
            roomUrlError: false,
            checked: false,
            modal1: false,
            visibleModel: false,
            visibleModelInput: false,
            togglePicker: false,
            dob: "",
            userType: "",
            isDatePickerVisible: false,
        };
        //     this.state.roomUrl = this.props.directLogin;

        if (this.props.directLogin !== undefined && this.props.directLogin !== null) {
            let result = entities.decode(this.props.directLogin)
            this.state.roomUrl = result;
        }
    }


    onChecked = () => {
        this.setState({checked: !this.state.checked});
    };

    onModal1 = () => {
        this.setState({visibleModel: !this.state.visibleModel});
    };

    onUrlChange = (str) => {
        try {
            this.setState({roomUrl: str});
            if (this.checkIsValidUrl(str)) {
                this.setState({roomUrlError: false});
            } else {
                this.setState({roomUrlError: true});
            }
        } catch (e) {
            recordError(e)
        }
    };

    checkIsValidUrl = (str) => {
        try {
            let pattern = /(ftp|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            return pattern.test(str);
        } catch (e) {
            recordError(e)
        }
    };

    setVisibility = () => this.props.setModelVisible(false)

    handleConfirm = (date) => {
        methodLog("handleConfirm")
        try {
            const dob = moment(date).format("YYYY-MM-DD");
            this.setState({
                dob,
                isDatePickerVisible: !this.state.isDatePickerVisible,
            });
        } catch (e) {
            recordError(e)
        }
    };

    toggleDatePicker = () => {
        this.setState({isDatePickerVisible: !this.state.isDatePickerVisible});
    };

    componentDidMount() {
        successLog("DoctorLoginScreen screen loaded")
        try {
            this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
                this.props.setIncomingUrl("")
                this.props.setModelVisible(false)
                return true;
            });
        } catch (e) {
            recordError(e)
        }
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }


    onContinuePress = () => {
        methodLog("onContinuePress")
        /// check room url.
        try {
            let userType = ""
            if (this.state.roomUrl.includes("PATIENT")) userType = "PATIENT"
            else userType = "GUEST"
            this.setState({
                userType: userType
            })
            this.props.setModelVisible(true)
        } catch (e) {
            recordError(e)
        }
    };

    getUrlParams = () => {
        methodLog("getUrlParams")
        try {
            let regex = /[?&]([^=#]+)=([^&#]*)/g,
                _params = {},
                match;
            while (match = regex.exec(this.state.roomUrl))
                _params[match[1]] = match[2];
            return _params

        } catch (e) {
            recordError(e)
        }
    }


    resetState = () => {
        this.setState({
            roomUrl: "",
            checked: false,
        })
    }

    onContinuePressModel = () => {
        methodLog("onContinuePressModel")
        try {
            this.props.setModelVisible(false)
            let urlParams = this.getUrlParams();
            if (urlParams === undefined || urlParams === null)
                return;

            if (urlParams.user === "GUEST") {
                this.onJoinCallRelative()
            } else {
                let appointmentId = urlParams && urlParams.appointmentId;
                let room = urlParams && urlParams.room;
                let token = urlParams && urlParams.token;
                if (appointmentId === undefined || appointmentId === null)
                    return
                this.resetState();
                this.props.checkUrlGuestPatient(this.state.userName, this.state.dob, appointmentId, this.props.navigation, room, token)

            }
        } catch (e) {
            recordError(e)
        }
    };

    onJoinCallRelative = () => {
        methodLog("onJoinCallRelative")
        try {
            let urlParams = this.getUrlParams();
            if (urlParams === undefined || urlParams === null)
                return;
            let appointmentId = urlParams && urlParams.appointmentId;
            if (appointmentId === undefined || appointmentId === null)
                return
            let appointment = appointmentId.replace('_GUEST', '')
            let room = urlParams && urlParams.room;
            this.props.setIncomingUrl("")
            this.props.setCallHeader(this.state.userName)
            this.resetState();
            this.props.navigation.navigate("WaitingRoom", {
                channel: room,
                user: "PATIENT",
                doctor: room,
                bookingId: appointment,
                guestName: this.state.userName
            });
        } catch (e) {
            recordError(e)
        }
    };


    render() {
        return (
            <Content>
                <ImageBackground
                    source={Images.auth_bg}
                    style={styles.image}
                    imageStyle={styles.imageBg}
                >
                    <View style={styles.container}>
                        <Icon
                            style={styles.backIcon}
                            name="arrowleft"
                            type="AntDesign"
                            onPress={() => this.props.navigation.goBack()}
                        />
                        <View style={styles.section1}>
                            <Text style={styles.welcomeText}>Direct Login</Text>
                        </View>
                        <View style={styles.section2}>
                            <Form>
                                <Item floatingLabel>
                                    <Label>Paste the doctor URL here</Label>
                                    <Input
                                        value={this.state.roomUrl}
                                        onChangeText={(value) => this.onUrlChange(value)}
                                        autoCapitalize="none"
                                        autoCorrect={false}
                                    />
                                    <Icon style={styles.attachIcon} active name="md-link"/>
                                </Item>
                                {this.state.roomUrlError === true &&
                                this.state.roomUrl.length > 0 ? (
                                    <Item style={{borderBottomColor: "transparent"}}>
                                        <Text style={{color: "red"}}>
                                            Please enter correct url
                                        </Text>
                                    </Item>
                                ) : null}
                            </Form>

                            <View>
                                <Item style={styles.checkboxItem}>
                                    <CheckBox
                                        checked={this.state.checked}
                                        onPress={this.onChecked}
                                        onAnimationType="bounce"
                                        offAnimationType="bounce"
                                        onTintColor="#1B80F3"
                                    />
                                    <Text style={styles.mainText}>
                                        <Text style={styles.tAndCText}>
                                            I agree to Sehet's online
                                        </Text>
                                        <Text style={styles.tAndCLink} onPress={this.onModal1}>
                                            {" "}
                                            video consultation policies.
                                        </Text>
                                    </Text>
                                </Item>

                                {/*    model */}
                            </View>
                            {this.state.checked === false ||
                            this.state.roomUrlError === true ? (
                                <Button block disabled={true} style={styles.disabled}>
                                    <Text>Continue</Text>
                                </Button>
                            ) : (
                                <Button
                                    block
                                    onPress={() => this.onContinuePress()}
                                    style={styles.danger}
                                >
                                    <Text>Continue</Text>
                                </Button>
                            )}
                        </View>
                        <View style={styles.section3}>
                            <View style={styles.footer}>
                                <Image source={Images.call} style={{width: 18, height: 18}}/>
                                <Text
                                    style={styles.help}
                                    uppercase={false}
                                    onPress={() => callNumber("+1-209-848-8410")}
                                >
                                    Helpline Number
                                </Text>
                            </View>
                        </View>
                    </View>

                    <PoliciesModel
                        onPress={() => {
                            this.setState({visibleModel: !this.state.visibleModel});
                        }}
                        visible={this.state.visibleModel}
                    />

                    <InputModel
                        visible={this.props.modelVisible}
                        desc={""}
                        isDatePickerVisible={this.state.isDatePickerVisible}
                        value={this.state.userName}
                        onTouchOutside={() => this.setVisibility()}
                        onBackdropPress={() => this.setVisibility()}
                        onSwipeOut={() => this.setVisibility()}
                        dob={this.state.dob}
                        user={this.state.userType}
                        handleConfirm={(date) => this.handleConfirm(date)}
                        togglePicker={() => this.toggleDatePicker()}
                        onContinue={(obj) => {
                            this.setVisibility();
                            const interval = setInterval(() => {
                                this.onContinuePressModel();
                                clearInterval(interval);
                            }, 80);
                        }}
                        onChangeText={(o) => {
                            this.setState({userName: o});
                        }}
                        onCancel={() => {
                            this.setVisibility();
                        }}
                    />
                </ImageBackground>
            </Content>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({
        postDirectLogin,
        setCallHeader,
        setIncomingUrl,
        checkUrlGuestPatient,
        setModelVisible
    }, dispatch);

const mapStateToProps = (state) => ({
    userProfile: state.profile.userProfile,
    directLogin: state.directLogin.url,
    modelVisible: state.directLogin.modelVisible,
    patientGuest: state.directLogin.patientGuest,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DoctorLoginScreen);
