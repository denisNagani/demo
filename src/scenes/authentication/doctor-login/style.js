import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default (styles = StyleSheet.create({
	container: {
		padding: "5%",
	},
	imageBg: {
		height: hp("100%"),
	},
	backIcon: {
		color: "#fff",
		marginTop: hp("1%"),
		fontSize: hp("3.5%"),
		marginLeft: 10,
	},
	section1: {
		marginTop: hp("6%"),
		marginLeft: wp("4%"),
	},
	welcomeText: {
		fontSize: 24,
		color: "#fff",
	},
	section2: {
		marginTop: hp("15%"),
	},
	section3: {
		alignItems: "center",
		justifyContent: "space-between",
		marginTop: hp("30%"),
	},
	image: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center",
	},
	m_t_15: {
		marginTop: "10%",
	},
	help: {
		color: "#A0A9BE",
		textDecorationLine: "underline",
		marginLeft: 5,
	},
	footer: {
		flexDirection: "row",
		alignItems: "center",
	},
	danger: {
		backgroundColor: "#c70315",
		borderRadius: 10,
		width: wp("85%"),
		alignSelf: "center",
	},
	disabled: {
		backgroundColor: "#A0A9BE",
		borderRadius: 10,
		width: wp("85%"),
		alignSelf: "center",
	},
	attachIcon: {
		color: "#fff",
	},
	mainText: {
		marginLeft: wp("5%"),
	},
	checkboxItem: {
		borderBottomColor: "transparent",
		marginVertical: hp("5%"),
	},
	tAndCText: {
		fontSize: 13,
	},
	tAndCLink: {
		color: "#1B80F3",
		fontSize: 13,
	},
	modal: {
		justifyContent: "center",
		paddingHorizontal: wp("5%"),
		paddingVertical: hp("2%"),
	},
	modalContainer: {
		flex: 1,
	},
	modalText: {
		textAlign: "center",
	},
	modal1Close: {
		alignItems: "center",
		justifyContent: "center",
	},
	modalheader: {
		backgroundColor: "#1B80F3",
		height: hp("9%"),
		flexDirection: "row",
		alignItems: "center",
	},
	modalheaderBackIcon: {
		flex: 1,
		fontSize: hp("3.5%"),
		color: "#fff",
		marginLeft: wp("5%"),
	},
	modalheading: {
		flex: 8,
		color: "#fff",
		fontSize: hp("2.9%"),
	},
}));
