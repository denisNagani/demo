import React, { Component } from "react";
import { Image, SafeAreaView, View } from "react-native";
import { Container, Icon, Text, } from "native-base";
import { connect } from "react-redux";
import styles from "../../../../styles/common";
import Images from "../../../../assets/images";
import database from "@react-native-firebase/database";
import { bindActionCreators } from "redux";
import { getUpcomingBookings, updateAppointment, } from "../../../../actions/bookings";
import { showModal } from "../../../../actions/modal";
import { addUserLogToFirebase } from "../../../../utils/helper";
import { JOINED } from "../../../../utils/constant";
import { successLog } from "../../../../utils/fireLog";

class WaitingRoom extends Component {
	constructor(props) {
		super(props);
		this.state = {
			connect: false,
			isLoading: false,
		};
	}

	componentDidMount() {
		successLog("WaitingRoom direct login screen loaded")
		this.checkCallStatus();
	}

	componentWillUnmount() {
		database()
			.ref(`/doctor/${this.props.navigation.state.params.bookingId}`)
			.off("value");
	}

	checkCallStatus = () => {
		let bookingId = this.props.navigation.state.params.bookingId;
		console.log(bookingId)
		database()
			.ref(`/doctor/${bookingId}`)
			.on("value", (snapshot) => {
				if (snapshot.val() !== null && snapshot.val().status === "APPROVED") {
					this.addUserInList(bookingId)
					// add log
					addUserLogToFirebase(bookingId, { event: JOINED, name: `${this.props.navigation.state.params.guestName} (Guest)` }).then(r => {
						this.redirect(bookingId);
					})

				} else if (
					snapshot.val() !== null &&
					snapshot.val().status === "REJECTED"
				) {
					let payload = {
						text: "Your appointment has been Rejected",
						subtext: "",
						iconName: "closecircleo",
						modalVisible: true,
					};
					this.props.showModal(payload);
					this.props.navigation.goBack();
				}
			});
	};

	redirect = (bookingId) => {
		this.props.navigation.navigate("JoinCall", {
			channel: this.props.navigation.state.params.channel,
			user: "DOCTOR",
			guestName: this.props.navigation.state.params.guestName,
			bookingId: bookingId,
		});
	};

	addUserInList = async (id) => {
		let guestName = this.props.navigation.state.params.guestName;
		await database().ref(`/users/${id}`)
			.update({ [guestName]: { fullName: `${guestName} (guest)`, image: "" } })
			.then((obj) => {
			});
	};

	addUserInListForRequest = async (id) => {
		let guestName = this.props.navigation.state.params.guestName;
		await database().ref(`/guests/${id}`)
			.update({ [guestName]: { fullName: guestName, image: "", status: "WAITING" } })
			.then((obj) => {
			});
	};

	render() {
		let doctor = this.props.navigation.state.params.doctor;
		return (
			<Container>
				<SafeAreaView style={styles.header}>
					<Icon
						name="arrowleft"
						type="AntDesign"
						style={styles.headerBackIcon}
						onPress={() => this.props.navigation.goBack()}
					/>
					<Text style={styles.heading}>Waiting Room</Text>
				</SafeAreaView>
				<View style={styles.waitingBoxContainer}>
					<View style={styles.waitingBox}>
						<View>
							<Text style={{ color: "#FFFFFF", fontSize: 15 }}>
								{doctor
									? doctor
									: this.props.navigation.state.params.roomName}
							</Text>
							<Text style={{ color: "#A0A9BE", fontSize: 12 }}>
								Waiting for your provider to come online
							</Text>
						</View>
						<Text style={styles.offlineStyle}>
							{" "}
							<Image
								source={Images.phoneOff}
								style={styles.offlineImageStyle}
							/>{" "}
							offline
						</Text>
					</View>

					<View style={{ alignItems: "center" }}>
						<View
							style={{
								borderStyle: "dotted",
								height: 50,
								borderLeftWidth: 1,
								borderLeftColor: "#A0A9BE",
							}}
						/>
						<View>
							<Text style={{ color: "#03FF9E", fontSize: 18 }}>
								Connecting..
							</Text>
						</View>
						<View
							style={{
								borderStyle: "dotted",
								height: 50,
								borderLeftWidth: 1,
								borderLeftColor: "#A0A9BE",
							}}
						/>
					</View>

					<View
						style={{ borderColor: "#A0A9BE", borderWidth: 1, borderRadius: 10 }}
					>
						<Image
							source={{
								uri:
									this.props.userProfile &&
										this.props.userProfile.userId &&
										this.props.userProfile.userId.imageUrl !== null
										? this.props.userProfile.userId.imageUrl
										: "https://genslerzudansdentistry.com/wp-content/uploads/2015/11/anonymous-user.png",
							}}
							style={{
								resizeMode: "contain",
								width: 154,
								height: 152,
								borderRadius: 10,
							}}
						/>
					</View>
				</View>

				<View />
			</Container>
		);
	}
}

const mapDispatchToProps = (dispatch) =>
	bindActionCreators({ getUpcomingBookings, updateAppointment, showModal }, dispatch);

const mapStateToProps = (state) => ({
	upcomingBookings: state.bookings.upcomingBookings,
	userProfile: state.profile.userProfile,
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(WaitingRoom);
