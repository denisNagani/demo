import React, { Component } from "react";
import { View, SafeAreaView } from "react-native";
import { Button, Icon, Input, Item, Text } from "native-base";

import styles from "./style";
import { showModal } from "../../../actions/modal";
import httpServices from "../../../services/http";
import validate from "../../../utils/validation_wrapper";
import ImageContainer from "../../../components/ImageContainer";
import { connect } from "react-redux";
import { methodLog, successLog } from "../../../utils/fireLog";
import { strings } from "../../../utils/translation";

class ForgotPasswordScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            emailError: null,
        };
        successLog("ForgotPasswordScreen screen loaded")
    }

    isValid = () => {
        const emailError = validate("email", this.state.email.toLowerCase());
        this.setState({ emailError });
        return !emailError;
    };

    _onChangeValue(key, value) {
        this.setState({ [key]: value });
    }

    _onBlur(key) {
        this.setState({
            [`${key}Error`]: validate(key, this.state[key]),
        });
    }

    render() {
        const backToLogin = () => {
            if (this.isValid()) {
                this.props.navigation.navigate("UserLogin");
            }
        };

        const onBtnPress = () => {
            methodLog("onBtnPress")
            if (this.isValid()) {
                const { email } = this.state;
                let payload = {
                    email,
                };
                httpServices.post("users/forgotPassword", payload).then((response) => {
                    successLog("forgotPassword", response.status)
                    if (response.status === 200) {
                        const payload = {
                            text: "Reset link has been sent to your email successfully!",
                            iconName: "checkcircleo",
                            modalVisible: true,
                        };
                        this.props.showModal(payload);
                        backToLogin();
                    } else if (response.status === 204) {
                        const payload = {
                            text: "User does not exist!!",
                            iconName: "closecircleo",
                            modalVisible: true,
                            iconColor: "#C70315",
                        };
                        this.props.showModal(payload);
                    }
                });
            }
        };
        return (
            <ImageContainer>
                <SafeAreaView style={styles.container}>
                    <View style={styles.mainContainer}>
                        <View style={[styles.backIcon]}>
                            <Icon
                                name="arrowleft"
                                type="AntDesign"
                                style={styles.backIconText}
                                onPress={() => this.props.navigation.goBack()}
                            />
                        </View>

                        <View style={styles.title}>
                            <Text style={[styles.titleText]}>{strings.forgot_password}</Text>
                        </View>
                        <View style={styles.input}>
                            <Item
                                error={this.state.emailError !== null}
                            // style={styles.email}
                            >
                                <Input
                                    style={{ color: '#FFF' }}
                                    value={this.state.email}
                                    onChangeText={(value) => this._onChangeValue("email", value)}
                                    placeholder="Enter your email"
                                    onBlur={() => this._onBlur("email")}
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                />
                            </Item>
                        </View>
                        <View style={styles.forgetBtn}>
                            <Button
                                block
                                disabled={
                                    this.state.email != "" && this.state.emailError == null
                                        ? false
                                        : true
                                }
                                style={
                                    this.state.email != "" && this.state.emailError == null
                                        ? styles.danger
                                        : styles.disabled
                                }
                                onPress={() => onBtnPress()}
                            >
                                <Text uppercase={false}>{strings.send_email}</Text>
                            </Button>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageContainer>
        );
    }
}

export default connect(
    null,
    { showModal }
)(ForgotPasswordScreen);
