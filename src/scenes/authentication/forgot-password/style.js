import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default (styles = StyleSheet.create({
	container: {
		paddingHorizontal: wp("5%"),
		height: hp("100%"),
	},
	mainContainer: {
		flex: 1,
		paddingHorizontal: 10
	},
	backIcon: {
		marginTop: hp("3%"),
		fontSize: hp("3.5%"),
		flex: 0.1,
	},
	backIconText: {
		color: "#FFF",
		fontSize: 30,
	},
	title: {
		flex: 0.3,
	},
	titleText: {
		color: '#FFF',
		fontSize: 26,
	},
	input: {
		flex: 0.15,
	},
	forgetBtn: {
		flex: 0.1,
	},
	danger: {
		backgroundColor: "#FF5E38",
		borderRadius: 10,
		marginBottom: hp("12%"),
		width: wp("85%"),
		alignSelf: "center",
	},
	disabled: {
		backgroundColor: "#A0A9BE",
		borderRadius: 10,
		marginBottom: hp("12%"),
		width: wp("85%"),
		alignSelf: "center",
	},
}));
