import React, { Component } from "react";
import { Image, ImageBackground, View, Text, Platform } from "react-native";
import { Button } from "native-base";
import styles from "./style";
import Images from "../../../assets/images";
import { getRefToken, getRole, getSkipped, getGetStarted, getAppLangugae, setIsVideoNotification, getVideoNotification, getOfferFlag, setOfferFlag, setOfferId, getIncomingCallScreen, setIncomingCallScreen } from "../../../services/auth";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { hide, show } from "../../../utils/loader/action";
import { callNumber } from "../../../utils/helpline-no";
import branch from "react-native-branch";
import { setIncomingUrl } from "../../../actions/direct-login";
import { methodLog, recordError, successLog } from "../../../utils/fireLog";
import Video from "react-native-video";
import { lang } from "moment";
import { Set_App_Language, setFromDynamicLink, SetSpecialityDetails } from "../../../actions/profile";
import { changeLaguage } from "../../../utils/translation";
import { getTrackingStatus, requestTrackingPermission } from 'react-native-tracking-transparency';
import NavigationService from "../../../components/startScreen/RootNavigation";
import dynamicLinks from '@react-native-firebase/dynamic-links'
import firestore from '@react-native-firebase/firestore'
import { isValid } from "../../../utils/ifNotValid";
import { acceptCallAction } from "../../../actions/start-consultation";
class LandingScreen extends Component {

    constructor(props) {
        super(props);
        this._unsubscribeFromBranch = null
        this.state = {
            link: null,
            doc: null
        }
    }

    componentDidMount() {
        successLog("LandingScreen screen loaded")
        if (Platform.OS === 'ios') {
            this.Apptrackingpermission()
        }
        // this.props.show();
        // this.checkToken()
    }

    callInit() {
        this.props.show();
        dynamicLinks()
            .getInitialLink()
            .then(link => {
                console.log("dynmic link url ", link);
                if (link?.url != null) {
                    firestore().collection("dynamic_links")
                        .get()
                        .then(async querySnapshot => {
                            if (querySnapshot.size > 0) {
                                let arr = []
                                await querySnapshot.forEach(documentSnapshot => {
                                    let document = documentSnapshot.data()
                                    arr.push(document)
                                });

                                console.log(arr);

                                arr?.map(item => {
                                    console.log("item link ", item.link);
                                    if (item?.link == link?.url) {
                                        // return item
                                        this.setState({
                                            link: item?.link,
                                            doc: item?.doc
                                        }, this.checkToken)
                                    }
                                })
                                this.checkToken()
                            }
                        })
                        .catch(err => console.log(err))
                } else {
                    this.checkToken()
                }
            })
            .catch((err) => {
                this.checkToken()
            })
    }

    Apptrackingpermission = async () => {
        const trackingStatus = await getTrackingStatus();
        console.log("Apptrackingpermission status " + trackingStatus);
        if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
            // enable tracking features
        } else {
            const trackingStatus = await requestTrackingPermission();
            console.log("Apptrackingpermission status " + trackingStatus);
            if (trackingStatus === 'authorized' || trackingStatus === 'unavailable') {
                // enable tracking features
            }
        }
    }

    componentWillUnmount() {
        setTimeout(x => {
            this._unsubscribeFromBranch = null
        }, 0)
    }


    checkToken = async () => {
        methodLog("checkToken")
        // const checkIsVideoCalling = await getVideoNotification()
        // if (checkIsVideoCalling == 'true') {
        //     if (Platform.OS === 'android') {
        //         NavigationService.navigate("DashboardWaitingScreen")
        //         setIsVideoNotification("false")
        //     }
        // } else {
        if (this.state.link == null) {
            try {
                // setIsVideoNotification("false")
                let result = await getRefToken();
                let role = await getRole();
                let skipped = JSON.parse(await getSkipped());
                let getStarted = JSON.parse(await getGetStarted());
                let getOffer = JSON.parse(await getOfferFlag());
                let incomingCallScreen = await getIncomingCallScreen()
                console.log("skiied flag ", skipped);
                console.log("role is ", role);
                console.log("getStarted is ", getStarted);
                console.log("getIncomingCallScreen is ", incomingCallScreen);
                const getlang = await getAppLangugae()
                if (getlang != null) {
                    this.props.Set_App_Language(getlang)
                    changeLaguage(getlang)
                }
                if (getOffer == true) {
                    this.props.navigation.navigate("offerScreen");
                    setOfferFlag(false)
                } else {
                    if (skipped) {
                        this.props.navigation.navigate("Home");
                    } else {
                        if (role !== null) {
                            console.log("role is ", role);
                            this.props.hide();
                            if (role === "DOCTOR") {
                                this.checkDoctorRoute(incomingCallScreen)
                            } else if (role === "PATIENT") {
                                this.props.navigation.navigate("Patient");
                            } else if (role === "ADMIN") {
                                this.props.navigation.navigate("Nurse");
                            }
                        } else {
                            if (getStarted) {
                                this.props.navigation.navigate("PatientLogin");
                                this.props.hide();
                            } else {
                                this.props.navigation.navigate("GetStarted");
                                this.props.hide();
                            }
                        }
                    }
                }

            } catch (e) {
                recordError(e)
            }
        } else {
            this.props.setFromDynamicLink(true, null)
            this.props.SetSpecialityDetails(this.state.doc)
            this.props.navigation.navigate("BookAppointmentScreenFlow2", {
                doc: this.state.doc
            })
        }
        // }
    };

    checkDoctorRoute = (val) => {
        if (val == "true") {
            setIncomingCallScreen('false')
            if (isValid(this.props.joinCallDoctorData?.appointmentData)) {
                this.props.acceptCallAction(
                    this.props.joinCallDoctorData?.appointmentData,
                    "APPROVED",
                    this.props.joinCallDoctorData?.userData,
                    () => {
                        this.props.navigation.navigate("JoinCall");
                    })
            } else {
                this.props.navigation.navigate("Doctor");
            }
        } else {
            this.props.navigation.navigate("Doctor");
        }
    }

    render() {
        return (
            <Video
                source={Images.video_splash}
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0
                }}
                onEnd={() => {
                    this.callInit()
                }}
                muted={true}
                resizeMode="cover"
                onError={(err) => {
                    console.log(JSON.stringify(err))
                    this.checkToken()
                }}
            />
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({ show, hide, setIncomingUrl, Set_App_Language, setFromDynamicLink, SetSpecialityDetails, acceptCallAction }, dispatch);

const mapStateToProps = (state) => ({
    modalVisible: state.modal.modalVisible,
    joinCallDoctorData: state.appointment.joinCallDoctorData,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(LandingScreen);
