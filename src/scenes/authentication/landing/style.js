import { StyleSheet, Dimensions } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";

const screenHeight = Math.round(Dimensions.get("window").height);

export default (styles = StyleSheet.create({
	container: {
		padding: "5%",
		flex: 1,
		flexDirection: "column",
	},
	section1: {
		flex: 2,
	},
	section2: {
		flex: 3,
		alignItems: "center",
	},
	section3: {
		alignItems: "center",
	},
	image: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center",
		height: hp("110%"),
	},
	m_b_15: {
		marginBottom: hp("10%"),
	},
	hyperlink: {
		color: "#1B80F3",
	},
	help: {
		color: "#A0A9BE",
		textDecorationLine: "underline",
		marginLeft: 5,
	},
	footer: {
		flexDirection: "row",
		alignItems: "center",
	},
	danger: {
		backgroundColor: "#c70315",
		borderRadius: 10,
	},
	secondary: {
		backgroundColor: "#A0A8BE",
		borderRadius: 10,
		marginBottom: hp("5%"),
	},
}));
