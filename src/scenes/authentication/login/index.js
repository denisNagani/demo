import React, { useState } from 'react';
import { Dimensions, Image, StyleSheet, TextInput, TouchableOpacity, View } from 'react-native';
// import TextInput from '../../../components/TextInput';
import { emailValidator, passwordValidator } from '../../../utils/core';
import Images from "../../../assets/images";
import { Button, Content, Footer, Input, Item, Text } from "native-base";
import {
    FONT_FAMILY_SF_PRO_LIGHT,
    FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_SemiBold, FONT_SIZE_13,
    FONT_SIZE_14, FONT_SIZE_15,
    FONT_SIZE_16
} from "../../../styles/typography";
import common from "../../../styles/common";
import { sendOtpAction } from "../../../actions/sehet/user-action";

const LoginScreen = ({ navigation }) => {
    const [email, setEmail] = useState({ value: '', error: '' });
    const [password, setPassword] = useState({ value: '', error: '' });
    const image = { uri: "https://reactjs.org/logo-og.png" };

    const _onLoginPressed = () => {
        const emailError = emailValidator(email.value);
        const passwordError = passwordValidator(password.value);

        if (emailError || passwordError) {
            setEmail({ ...email, error: emailError });
            setPassword({ ...password, error: passwordError });
            return;
        }

        navigation.navigate('Dashboard');
    };

    return (
        <Content>
            <Image source={Images.bell_icon} style={styles.image} />
            <View style={styles.overlay}>
                <View style={{
                    flex: 1.5,
                    justifyContent: 'space-around',
                    paddingHorizontal: 20
                }}>

                    <View />
                    <View />

                    <Image source={Images.loginLogo} style={{ width: 110, height: 97 }} />


                    <View>

                        <Item style={styles.Item}>
                            <Input
                                label="Email"
                                returnKeyType="next"
                                placeholder={"Enter your Email"}
                                placeholderColor={"#848484"}
                                value={email.value}
                                onChangeText={text => setEmail({ value: text, error: '' })}
                                error={!!email.error}
                                errorText={email.error}
                                autoCapitalize="none"
                                autoCompleteType="email"
                                textContentType="emailAddress"
                                keyboardType="email-address" />
                        </Item>
                        <Item style={styles.Item}>
                            <Input
                                label="Password"
                                returnKeyType="next"
                                placeholder={"Password "}
                                placeholderColor={"#848484"}
                                value={email.value}
                                onChangeText={text => setEmail({ value: text, error: '' })}
                                error={!!email.error}
                                secureTextEntry={true}
                                errorText={email.error}
                                autoCapitalize="none"
                                autoCompleteType="email"
                                textContentType="emailAddress"
                                keyboardType="email-address" />
                        </Item>

                    </View>

                    <Button danger style={[common.footerBtn]}
                        onPress={() => {
                            console.log(smsWhatsapp);
                            if (smsWhatsapp == 'sms') {
                                signInWithPhoneNumber(mobile).then(_ => {
                                })
                            } else {
                                dispatch(sendOtpAction({
                                    mobileNo: mobile,
                                    msgMode: "MOBILE"
                                }, dispatch, props.navigation))
                            }
                        }}>
                        <Text uppercase={false} style={[styles.number91, { color: 'white' }]}>Login</Text>
                    </Button>


                </View>


                <View style={{ flex: 1, paddingBottom: 30, alignItems: 'center', justifyContent: 'space-around' }}>
                    <Text style={{ marginTop: 10, fontFamily: FONT_FAMILY_SF_PRO_SemiBold, color: '#080A72', fontSize: FONT_SIZE_13 }}>Forgot your password?</Text>

                    <Text style={styles.textAlready}>If you're a patient,
                        <Text style={styles.textAlreadyLogin} onPress={() => {
                            navigation.navigate("RegisterScreen")
                        }}> Login </Text>here.</Text>
                </View>
            </View>
        </Content>
    );
};

const styles = StyleSheet.create({
    textAlready: {
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold,
        fontSize: FONT_SIZE_15
    },
    textAlreadyLogin: {
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold,
        fontSize: FONT_SIZE_15,
        color: '#FF5E38'
    },
    linearGradient: {
        flex: 1,
        borderRadius: 8,
        justifyContent: 'center',
        paddingLeft: 15,
        paddingRight: 15,
    },
    container: {
        flex: 1,
        flexDirection: "column"
    },
    image: {
        resizeMode: "stretch",
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    overlay: {
        height: Dimensions.get('window').height
    },
    text: {
        color: "grey",
        fontSize: 30,
        fontWeight: "bold"
    }
});

export default LoginScreen;
