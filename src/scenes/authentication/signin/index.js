import React, { Component } from "react";
import { Image, SafeAreaView, View, TouchableOpacity, Platform } from "react-native";
import { Button, Form, Icon, Input, Item, Label, Text } from "native-base";
import styles from "./style";
import Images from "../../../assets/images";
import validate from "../../../utils/validation_wrapper";
import { connect } from "react-redux";
import { callLoginApi } from "../../../actions/login";
import { methodLog, recordError, successLog } from "../../../utils/fireLog";
import { client_id, client_secret } from "../../../config";
import moment from "moment";
import { getFcmToken } from "../../../services/auth";
import { ScrollView } from "react-native-gesture-handler";
import ImageContainer from "../../../components/ImageContainer";
import { strings } from "../../../utils/translation";
import DeviceInfo from 'react-native-device-info'

class SigninScreen extends Component {
    constructor(props) {
        super(props);
        successLog("Login in Screen loaded")
        this.state = {
            email: "",
            emailError: null,
            password: "",
            passwordError: null,
            secureTextEntry: true
        };
    }

    updateSecureTextEntry = () => {
        this.setState({
            secureTextEntry: !this.state.secureTextEntry,
        });
    };

    isValid = () => {
        methodLog("isValid")
        const emailError = validate("email", this.state.email);
        const passwordError = validate("password", this.state.password);
        this.setState({ emailError, passwordError });
        return !emailError && !passwordError;
    };

    _back() {
        this.props.navigation.goBack();
    }

    _onChangeValue(key, value) {
        try {
            let val = ''
            if (key == "fullName") {
                val = value.replace(/[^A-Za-z_ ]/gi, "");
                if (val === undefined || val === null)
                    return
                this.setState({ [key]: val });
            } else this.setState({ [key]: value.trim() });
        } catch (e) {
            recordError(e)
        }
    }

    _onBlur(key) {
        this.setState({
            [`${key}Error`]: validate(key, this.state[key]),
        });
    }


    onSigninBtnPress = async () => {
        methodLog("onSigninBtnPress")
        try {
            if (this.isValid()) {
                const deviceId = DeviceInfo.getUniqueId()
                const fcmToken = await getFcmToken()
                const parsedFcmToken = JSON.parse(fcmToken)
                let appPlatform = Platform.OS
                console.log("login FCM 1", parsedFcmToken);
                console.log("login FCM 2", fcmToken);
                const { email, password } = this.state;
                const tokenObj = {
                    token: parsedFcmToken,
                    deviceId: deviceId,
                    platform: appPlatform
                }
                let payload = `username=${email}&password=${password}&fcmToken=${JSON.stringify(tokenObj)}&offset=${moment(new Date()).format()}&grant_type=password&client_id=${client_id}&client_secret=${client_secret}`;
                this.props.callLoginApi(payload, this.props.navigation)
            }
        } catch (e) {
            recordError(e)
        }
    };

    render() {
        return (
            <ImageContainer>
                <SafeAreaView style={styles.main_container}>
                    <>
                        <View style={styles.container}>
                            <View style={{ alignItems: 'center', justifyContent: 'center', height: '45%' }}>
                                <Image
                                    resizeMode="contain"
                                    source={Images.home_logo}
                                    style={{ width: '100%', height: '40%' }} />
                            </View>
                            <Form style={{ marginTop: 10 }}>
                                <Item
                                    floatingLabel
                                    error={this.state.emailError !== null}
                                >
                                    <Label style={{ color: '#A0A9BE' }}>Enter your email</Label>
                                    <Input
                                        style={{ height: 60, color: '#FFFFFF' }}
                                        value={this.state.email}
                                        onChangeText={(value) =>
                                            this._onChangeValue("email", value)
                                        }
                                        onBlur={() => this._onBlur("email")}
                                    />
                                </Item>
                                <Item
                                    floatingLabel
                                    error={this.state.passwordError !== null}
                                >
                                    <Label style={{ color: '#A0A9BE' }}>Enter your password</Label>
                                    <Input
                                        style={{ height: 60, color: '#FFFFFF' }}
                                        value={this.state.password}
                                        onChangeText={(value) =>
                                            this._onChangeValue("password", value)
                                        }
                                        onBlur={() => this._onBlur("password")}
                                        secureTextEntry={this.state.secureTextEntry}
                                    />
                                    {this.state.secureTextEntry ? (
                                        <Icon
                                            name="eye-off"
                                            type="Feather"
                                            style={styles._passwordIcon}
                                            onPress={this.updateSecureTextEntry}
                                        />
                                    ) : (
                                        <Icon
                                            name="eye"
                                            type="Feather"
                                            style={styles._passwordIconOn}
                                            onPress={this.updateSecureTextEntry}
                                        />
                                    )}
                                </Item>
                            </Form>
                            <Button
                                block
                                onPress={this.onSigninBtnPress}
                                style={styles.loginBtn}
                            >
                                <Text uppercase={false}>{strings.login}</Text>
                            </Button>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('ForgotPassword')}>
                                <Text style={styles.forgotPassword}>
                                    {strings.forgot_password}
                                </Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ flex: 1, alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
                            <Text style={styles.loginHere}>{strings.ifPatient}</Text>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("PatientLogin")}><Text style={styles.loginLink}> {strings.login} </Text></TouchableOpacity>
                            <Text style={styles.loginHere}>{strings.here}</Text>
                        </View>
                    </>
                </SafeAreaView>
            </ImageContainer>
        );
    }
}

export default connect(
    null,
    { callLoginApi }
)(SigninScreen);
