import { StyleSheet } from "react-native";
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default (styles = StyleSheet.create({
    main_container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        // backgroundColor: '#FFF'
    },
    container: {
        padding: "5%",
    },
    loginBtn: {
        backgroundColor: '#FF5E38',
        marginHorizontal: 15,
        marginBottom: 25,
        marginTop: 35,
        borderRadius: 5,
    },
    loginLink: {
        color: '#FF5E38',
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: -10,
        marginBottom: 10
    },
    loginHere: {
        fontSize: 18,
        textAlign: 'center',
        marginTop: -10,
        fontWeight: 'bold',
        color: '#FFFFFF',
        marginBottom: 10
    },
    forgotPassword: {
        textAlign: 'center',
        fontSize: 14,
        fontWeight: 'bold',
        color: '#038BEF'
    },
    _passwordIcon: {
        color: "#A0A9BE",
        fontSize: 20,
    },
    _passwordIconOn: {
        color: "#1B80F3",
        fontSize: 20,
    },
    hyperlink: {
        color: "#1B80F3",
    },
    modal: {
        justifyContent: "center",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("2%"),
    },
    modalContainer: {
        flex: 1,
    },
    modalTextHeader: {
        fontSize: 24,
        marginTop: 27,
        marginBottom: 6,
        color: "#041A32",
    },
    modalTextSubHeader: {
        fontSize: 14,
        color: "#041A32",
        marginTop: 20,
        marginBottom: 6,
    },
    modalText: {
        fontSize: 13,
        color: "#4E5C76",
    },
    modal1Close: {
        alignItems: "center",
        justifyContent: "center",
    },
    modal2Close: {
        alignItems: "center",
        justifyContent: "center",
    },
    modalheader: {
        // backgroundColor: "#1B80F3",
        height: hp("9%"),
        flexDirection: "row",
        alignItems: "center",
    },
    modalheaderBackIcon: {
        flex: 1,
        fontSize: hp("3.5%"),
        color: "#1B80F3",
        marginLeft: wp("5%"),
    },
    modalheading: {
        flex: 8,
        color: "#041A32",
        fontSize: hp("2.9%"),
    },
    successSignInIcon: {
        marginTop: 30,
        alignSelf: "center",
        fontSize: hp("10%"),
        color: "#1B80F3",
    },
    failSignInIcon: {
        marginTop: 30,
        alignSelf: "center",
        fontSize: hp("10%"),
        color: "#C70315",
    },
    successSignInText: {
        color: "#041A32",
        marginTop: 30,
        alignSelf: "center",
    },
    successSignInSubtext: {
        alignSelf: "center",
    },
}));
