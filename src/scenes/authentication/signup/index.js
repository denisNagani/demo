import React, {Component} from "react";
import {Dimensions, Image, ImageBackground, Modal, SafeAreaView, View,} from "react-native";
import {Body, Button, CheckBox, Content, Form, Icon, Input, Item, Label, Text,} from "native-base";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import {showModal} from "../../../actions/modal";
import styles from "./style";
import Images from "../../../assets/images";
import validate from "../../../utils/validation_wrapper";
import {ScrollView} from "react-native-gesture-handler";
import {callNumber} from "../../../utils/helpline-no";
import {connect} from "react-redux";
import {signUpCall} from "../../../actions/login";
import {methodLog, recordError, successLog} from "../../../utils/fireLog";

class SignupScreen extends Component {
    constructor(props) {
        super(props);
        successLog("Sign up Screen screen loaded")
        this.state = {
            fullName: "",
            fullNameError: null,
            email: "",
            emailError: null,
            password: "",
            passwordError: null,
            dob: "",
            isDatePickerVisible: false,
            checked: false,
            secureTextEntry: true,
            modal1: false,
            modal2: false,
        };
    }

    toggleDatePicker = () => {
        this.setState({isDatePickerVisible: !this.state.isDatePickerVisible});
    };

    updateSecureTextEntry = () => {
        this.setState({
            secureTextEntry: !this.state.secureTextEntry,
        });
    };

    handleConfirm = (date) => {
        methodLog("handleConfirm")
        try {
            const dob = moment(date).format("YYYY-MM-DD");
            this.setState({
                dob: "  ",
                isDatePickerVisible: false,
            }, () => {
                this.setState({
                    dob: dob,
                    isDatePickerVisible: false,
                })
            });
        } catch (e) {
            recordError(e)
        }
    };

    isValid = () => {
        methodLog("isValid")
        const fullNameError = validate("fullName", this.state.fullName);
        const emailError = validate("email", this.state.email);
        const passwordError = validate("password", this.state.password);
        this.setState({emailError, passwordError, fullNameError});
        return !emailError && !passwordError && !fullNameError;
    };

    _back() {
        this.props.navigation.goBack();
    }

    _onChangeValue(key, value) {
        try {
            let val = ''
            if (key == "fullName") {
                val = value.replace(/[^A-Za-z_ ]/gi, "");
                if (val === undefined || val === null)
                    return
                this.setState({[key]: val});
            } else this.setState({[key]: value.trim()});
        } catch (e) {
            recordError(e)
        }
    }

    _onBlur(key) {
        this.setState({
            [`${key}Error`]: validate(key, this.state[key]),
        });
    }

    _resetData() {
        this.setState({
            fullName: null,
            fullNameError: null,
            email: null,
            emailError: null,
            password: null,
            passwordError: null,
            dob: "",
        });
    }

    onSignupBtnPress = () => {
        methodLog("onSignupBtnPress")
        try {
            if (this.isValid()) {
                const {fullName, email, password, dob} = this.state;
                let payload = {
                    fullName,
                    email,
                    password,
                    dob,
                };
                this.props.signUpCall(payload, this.props.navigation, () => this._resetData())
            }
        } catch (e) {
            recordError(e)
        }
    };

    onChecked = () => {
        this.setState({checked: !this.state.checked});
    };

    onModal1 = () => {
        this.setState({modal1: !this.state.modal1});
    };

    onModal2 = () => {
        this.setState({modal2: !this.state.modal2});
    };

    render() {
        let {height} = Dimensions.get("window");
        return (
            <Content>
                <ImageBackground
                    source={height < 700 ? Images.auth_bg_sm : Images.auth_bg}
                    style={styles.image}
                    imageStyle={styles.imageBg}
                >
                    <View style={styles.container}>
                        <Button transparent onPress={() => this._back()}>
                            <Icon style={styles.backIcon} name="arrowleft" type="AntDesign"/>
                        </Button>
                        <View style={height < 700 ? styles.section1_sm : styles.section1}>
                            <Text style={styles.welcomeText}>
                                Welcome to Pathway Healthcare
                            </Text>
                        </View>
                        <View style={height < 700 ? styles.section2_sm : styles.section2}>
                            <Form>
                                <Item floatingLabel error={this.state.fullNameError !== null}>
                                    <Label>Enter your name *</Label>
                                    <Input
                                        value={this.state.fullName}
                                        onChangeText={(value) =>
                                            this._onChangeValue("fullName", value)
                                        }
                                        onBlur={() => this._onBlur("fullName")}
                                    />
                                </Item>
                                <Item
                                    floatingLabel
                                    error={this.state.emailError !== null}
                                    // style={styles.item}
                                >
                                    <Label>Enter your email *</Label>
                                    <Input
                                        value={this.state.email}
                                        onChangeText={(value) =>
                                            this._onChangeValue("email", value)
                                        }
                                        onBlur={() => this._onBlur("email")}
                                    />
                                </Item>
                                <Item
                                    floatingLabel
                                    onPress={() => this.toggleDatePicker()}
                                    // style={styles.item}
                                >
                                    <Label>Date of birth *</Label>
                                    <Input
                                        value={
                                            this.state.dob.toString() == ""
                                                ? ""
                                                : moment(this.state.dob)
                                                    .format("DD MMMM YYYY")
                                                    .toString()
                                        }
                                        disabled
                                    />
                                    <Icon
                                        name="calendar"
                                        type="Feather"
                                        style={styles.calIcon}
                                    />
                                </Item>
                                {
                                    <DateTimePickerModal
                                        isVisible={this.state.isDatePickerVisible}
                                        mode="date"
                                        onConfirm={this.handleConfirm}
                                        onCancel={this.toggleDatePicker}
                                        maximumDate={new Date()}
                                    />
                                }
                                <Item
                                    floatingLabel
                                    error={this.state.passwordError !== null}
                                    // style={styles.item}
                                >
                                    <Label>Enter your password *</Label>
                                    <Input
                                        value={this.state.password}
                                        onChangeText={(value) =>
                                            this._onChangeValue("password", value)
                                        }
                                        onBlur={() => this._onBlur("password")}
                                        secureTextEntry={this.state.secureTextEntry}
                                    />
                                    {this.state.secureTextEntry ? (
                                        <Icon
                                            name="eye-off"
                                            type="Feather"
                                            style={styles._passwordIcon}
                                            onPress={this.updateSecureTextEntry}
                                        />
                                    ) : (
                                        <Icon
                                            name="eye"
                                            type="Feather"
                                            style={styles._passwordIconOn}
                                            onPress={this.updateSecureTextEntry}
                                        />
                                    )}
                                </Item>
                            </Form>
                            <View>
                                <Item style={styles.checkboxItem}>
                                    <CheckBox
                                        checked={this.state.checked}
                                        onPress={this.onChecked}
                                        onAnimationType="bounce"
                                        offAnimationType="bounce"
                                        onTintColor="#1B80F3"
                                    />
                                    <Body>
                                        <Text style={styles.mainText}>
                                            <Text style={styles.tAndCText}>
                                                I agree to Pathway Healthcare’s{" "}
                                            </Text>
                                            <Text style={styles.tAndCLink} onPress={this.onModal1}>
                                                terms and conditions
                                            </Text>
                                            <Text style={styles.tAndCText}>
                                                {" "}
                                                and understand their
                                            </Text>
                                            <Text style={styles.tAndCLink} onPress={this.onModal2}>
                                                {" "}
                                                privacy policy.
                                            </Text>
                                        </Text>
                                    </Body>
                                </Item>
                                <Modal visible={this.state.modal1} animationType="slide">
                                    <SafeAreaView style={styles.modalContainer}>
                                        <View style={styles.modalheader}>
                                            <Icon
                                                name="close"
                                                type="MaterialCommunityIcons"
                                                style={styles.modalheaderBackIcon}
                                                onPress={this.onModal1}
                                            />
                                            <Text style={styles.modalheading}>
                                                Terms and Conditions
                                            </Text>
                                        </View>
                                        <ScrollView contentContainerStyle={styles.modal}>
                                            <Text style={styles.modalText}>
                                                This policy was last updated Date (dd/mm/yyyy)-
                                                Inquiries at (legal@sehet.com)
                                            </Text>
                                            <Text style={styles.modalTextHeader}>1. Header</Text>
                                            <Text style={styles.modalText}>
                                                By accessing the Pathway Healthcare app, you are
                                                agreeing to be bound by these terms of service, all
                                                applicable laws and regulations, and agree that you are
                                                responsible for compliance with any applicable local
                                                laws.
                                            </Text>
                                            <Text style={styles.modalTextSubHeader}>Subheader</Text>
                                            <Text style={styles.modalText}>
                                                If you do not agree with any of these terms, you are
                                                prohibited from using or accessing this site. The
                                                materials contained in this website are protected by
                                                applicable copyright and trademark law. ornare
                                            </Text>
                                            <Text style={styles.modalTextHeader}>2. Header</Text>
                                            <Text style={styles.modalText}>
                                                Permission is granted to temporarily download one copy
                                                of the materials (information or software) website for
                                                personal, non-commercial transitory viewing only. This
                                                is the grant of a license, not a transfer of title, and
                                                under this license you may not:
                                            </Text>
                                            <Text style={styles.modalText}>
                                                • modify or copy the materials;{"\n"}• use the materials
                                                for any commercial purpose, or for any public display
                                                (commercial or non-commercial);{"\n"}• remove any
                                                copyright or other proprietary notations from the
                                                materials; or{"\n"}• transfer the materials to another
                                                person or "mirror" the materials on any other server.
                                            </Text>
                                        </ScrollView>
                                    </SafeAreaView>
                                </Modal>
                                <Modal visible={this.state.modal2} animationType="slide">
                                    <SafeAreaView style={styles.modalContainer}>
                                        <View style={styles.modalheader}>
                                            <Icon
                                                name="close"
                                                type="MaterialCommunityIcons"
                                                style={styles.modalheaderBackIcon}
                                                onPress={this.onModal2}
                                            />
                                            <Text style={styles.modalheading}>Privacy Policy</Text>
                                        </View>
                                        <ScrollView contentContainerStyle={styles.modal}>
                                            <Text style={styles.modalText}>
                                                This policy was last updated Date (dd/mm/yyyy)-
                                                Inquiries at (legal@sehet.com)
                                            </Text>
                                            <Text style={styles.modalTextHeader}>1. Header</Text>
                                            <Text style={styles.modalText}>
                                                By accessing the Pathway Healthcare app, you are
                                                agreeing to be bound by these terms of service, all
                                                applicable laws and regulations, and agree that you are
                                                responsible for compliance with any applicable local
                                                laws.
                                            </Text>
                                            <Text style={styles.modalTextSubHeader}>Subheader</Text>
                                            <Text style={styles.modalText}>
                                                If you do not agree with any of these terms, you are
                                                prohibited from using or accessing this site. The
                                                materials contained in this website are protected by
                                                applicable copyright and trademark law. ornare
                                            </Text>
                                            <Text style={styles.modalTextHeader}>2. Header</Text>
                                            <Text style={styles.modalText}>
                                                Permission is granted to temporarily download one copy
                                                of the materials (information or software) website for
                                                personal, non-commercial transitory viewing only. This
                                                is the grant of a license, not a transfer of title, and
                                                under this license you may not:
                                            </Text>
                                            <Text style={styles.modalText}>
                                                • modify or copy the materials;{"\n"}• use the materials
                                                for any commercial purpose, or for any public display
                                                (commercial or non-commercial);{"\n"}• remove any
                                                copyright or other proprietary notations from the
                                                materials; or{"\n"}• transfer the materials to another
                                                person or "mirror" the materials on any other server.
                                            </Text>
                                        </ScrollView>
                                    </SafeAreaView>
                                </Modal>
                            </View>
                            <Button
                                block
                                onPress={this.onSignupBtnPress}
                                disabled={
                                    this.state.fullName != "" &&
                                    this.state.email != "" &&
                                    this.state.password != "" &&
                                    this.state.checked != false &&
                                    (this.state.emailError == null &&
                                        this.state.passwordError == null &&
                                        this.state.fullNameError == null)
                                        ? false
                                        : true
                                }
                                style={
                                    this.state.fullName != "" &&
                                    this.state.email != "" &&
                                    this.state.password != "" &&
                                    this.state.checked != false &&
                                    (this.state.emailError == null &&
                                        this.state.passwordError == null &&
                                        this.state.fullNameError == null)
                                        ? styles.danger
                                        : styles.disabled
                                }
                            >
                                <Text uppercase={false}>Sign Up</Text>
                            </Button>
                        </View>
                        <View style={height < 700 ? styles.section3_sm : styles.section3}>
                            <Text>
                                Already Registered?
                                <Text
                                    style={styles.hyperlink}
                                    onPress={() => this.props.navigation.navigate("UserLogin")}
                                >
                                    {" "}
                                    Login
                                </Text>
                            </Text>
                            <Button
                                iconLeft
                                transparent
                                onPress={() => callNumber("+1-209-848-8410")}
                            >
                                <Image source={Images.call} style={{width: 18, height: 18}}/>
                                <Text style={styles.footer} uppercase={false}>
                                    Helpline Number
                                </Text>
                            </Button>
                        </View>
                    </View>
                </ImageBackground>
            </Content>
        );
    }
}

export default connect(
    null,
    {showModal, signUpCall}
)(SignupScreen);
