import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default (styles = StyleSheet.create({
	container: {
		padding: "5%",
	},
	image: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center",
	},
	imageBg: {
		height: hp("100%"),
	},
	backIcon: {
		color: "#fff",
		fontSize: hp("3.5%"),
		height: 35,
		marginTop: hp("5%"),
		marginLeft: wp("3%"),
	},
	section1: {
		marginTop: hp("8%"),
		marginLeft: wp("4%"),
	},
	section1_sm: {
		marginTop: hp("5%"),
		marginLeft: wp("4%"),
	},
	section2: {
		marginTop: hp("6%"),
	},
	section2_sm: {
		marginTop: hp("5%"),
	},
	section3: {
		marginTop: hp("6%"),
		alignItems: "center",
	},
	section3_sm: {
		marginTop: hp("5%"),
		alignItems: "center",
	},
	welcomeText: {
		fontSize: 24,
		color: "#fff",
		width: wp("70%"),
	},
	_passwordIcon: {
		color: "#A0A9BE",
		fontSize: 20,
	},
	_passwordIconOn: {
		color: "#1B80F3",
		fontSize: 20,
	},
	item: {
		marginVertical: hp("2%"),
		paddingBottom: hp("1.5%"),
		borderBottomColor: "#A0A9BE",
	},
	hyperlink: {
		color: "#1B80F3",
	},
	footer: {
		color: "gray",
		textDecorationLine: "underline",
	},
	danger: {
		backgroundColor: "#c70315",
		alignSelf: "center",
		width: wp("85%"),
		height: hp("6%"),
		borderRadius: 10,
		marginTop: hp("1.2%"),
	},
	disabled: {
		backgroundColor: "#A0A9BE",
		alignSelf: "center",
		width: wp("85%"),
		height: hp("6%"),
		borderRadius: 10,
		marginTop: hp("1.2%"),
	},
	calIcon: {
		color: "#1B80F3",
	},
	mainText: {
		marginLeft: wp("5%"),
	},
	checkboxItem: {
		borderBottomColor: "transparent",
		marginVertical: hp("1%"),
	},
	tAndCText: {
		fontSize: 13,
	},
	tAndCLink: {
		color: "#1B80F3",
		fontSize: 13,
	},
	modal: {
		justifyContent: "center",
		paddingHorizontal: wp("5%"),
		paddingVertical: hp("2%"),
	},
	modalContainer: {
		flex: 1,
	},
	modalTextHeader: {
		fontSize: 24,
		marginTop: 27,
		marginBottom: 6,
		color: "#041A32",
	},
	modalTextSubHeader: {
		fontSize: 14,
		color: "#041A32",
		marginTop: 20,
		marginBottom: 6,
	},
	modalText: {
		fontSize: 13,
		color: "#4E5C76",
	},
	modal1Close: {
		alignItems: "center",
		justifyContent: "center",
	},
	modal2Close: {
		alignItems: "center",
		justifyContent: "center",
	},
	modalheader: {
		// backgroundColor: "#1B80F3",
		height: hp("9%"),
		flexDirection: "row",
		alignItems: "center",
	},
	modalheaderBackIcon: {
		flex: 1,
		fontSize: hp("3.5%"),
		color: "#1B80F3",
		marginLeft: wp("5%"),
	},
	modalheading: {
		flex: 8,
		color: "#041A32",
		fontSize: hp("2.9%"),
	},
	successSignUpIcon: {
		marginTop: 30,
		alignSelf: "center",
		fontSize: hp("10%"),
		color: "#1B80F3",
	},
	failSignUpIcon: {
		marginTop: 30,
		alignSelf: "center",
		fontSize: hp("10%"),
		color: "#C70315",
	},
	successSignUpText: {
		color: "#041A32",
		marginTop: 30,
		alignSelf: "center",
	},
	successSignUpSubtext: {
		alignSelf: "center",
	},
}));
