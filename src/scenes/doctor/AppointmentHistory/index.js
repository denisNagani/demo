import React, { useEffect, useState } from 'react'
import { FlatList, StyleSheet, Text, View, Image } from 'react-native'
import HeaderSehet from '../../../components/header-common'
import { strings } from '../../../utils/translation'
import http from '../../../services/http'
import { hide, show } from '../../../utils/loader/action'
import { useDispatch, useSelector } from 'react-redux'
import { ifNotValid, ifValid, isValid } from '../../../utils/ifNotValid'
import moment from 'moment'
import { setUpcomingPastDetailView } from '../../patient/appointment-detail/action'
import Images from '../../../assets/images'
import PowerTranslator from '../../../components/PowerTranslator'
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_13, FONT_SIZE_16 } from '../../../styles/typography'
import { Icon, Item } from 'native-base'
import styles from "../upcoming-past-bookings/style";

const Index = (props) => {
    const dispatch = useDispatch()
    const loading = useSelector(state => state.loader.isLoading)
    const data = props.navigation.state.params?.data
    let patient_email = data?.intakeInfo?.email

    const [pastHistory, setPastHistory] = useState([])
    const [pageNo, setPageNo] = useState(0)

    useEffect(() => {
        if (isValid(patient_email)) {
            getPatientHistory(patient_email, pageNo)
        }
    }, [pageNo])

    const getPatientHistory = (email, pageNo) => {
        dispatch(show())
        http
            .get(`api/past/appointmentsByPatient/${email}?page=${pageNo}`, dispatch)
            .then(res => {
                console.log('res bhai', res);
                if (res?.status === 200) {
                    let finaldata = res?.data?.appointments?.length > 0 ? res?.data?.appointments : []
                    setPastHistory([...pastHistory, ...finaldata])
                }
                dispatch(hide())
            })
            .catch(err => {
                dispatch(hide())
            })
    }

    const renderItem = ({ item }) => {

        let patientId = ifNotValid(item.patientId) ? {} : item.patientId;

        let fullName = patientId.fullName
        if (ifNotValid(fullName)) fullName = '-'

        let hindiFullName = patientId.hindiFullName
        if (ifNotValid(hindiFullName)) hindiFullName = fullName

        let name = isValid(item?.intakeInfo?.name) ? item?.intakeInfo?.name : patientId?.fullName

        let imageUrl = patientId.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = Images.user
        else imageUrl = { uri: imageUrl }

        let isInvalid = false;
        let day = new Date()
        if (ifValid(day)) day = moment(item.utcDate).format("dddd")
        else isInvalid = true;

        let date = new Date()
        if (ifValid(date)) date = moment(item.utcDate).format("D MMM")
        else isInvalid = true

        let time = new Date()
        if (ifValid(time)) time = moment(item.utcDate).format("hh:mm A")
        else isInvalid = true

        let callType = item?.callType
        let appointmentType = callType == "Online" ? "Online" : "Clinic Visit"

        return (
            <Item
                style={{ flex: 1, backgroundColor: '#F9F9F9' }}
                onPress={() => {
                    dispatch(setUpcomingPastDetailView('UPCOMING'))
                    props.navigation.navigate("AppointedDetailsAdded", { data: item })
                }}
            >
                <View style={{ flexDirection: 'row', marginHorizontal: 10, marginVertical: 10, padding: 2, }}>
                    <Image source={imageUrl} style={{ width: 57, height: 57, borderRadius: 28.5 }} />
                    <View style={innerStyle.dateTimeContainer}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>

                            <PowerTranslator
                                style={[innerStyle.nameStyle, { flex: 1 }]}
                                hinText={name}
                                enText={name}
                            />

                            <View style={{
                                backgroundColor: '#048BEF',
                                paddingHorizontal: 8,
                                paddingVertical: 5,
                                borderRadius: 5,
                            }}>
                                <Text style={[styles.patientCardTextTime, { color: '#FFF' }]}>
                                    {appointmentType}
                                </Text>
                            </View>

                        </View>


                        {isInvalid === true ?
                            <Text>...</Text> :
                            <Text style={innerStyle.subTitle}>
                                <Icon style={[innerStyle.subIcon]} type="AntDesign"
                                    name="calendar" /> {day}, {date}, <Icon style={innerStyle.subIcon}
                                        type="Feather"
                                        name="clock" /> {time}
                            </Text>}
                    </View>
                </View>

            </Item>
        );
    };

    const NoBookings = () => {
        if (loading == true) {
            return null
        }
        return <View style={styles.noBookingsBlock}>
            <Image source={Images.no_bookings} style={styles.noBookingsImage} />
            <Text style={styles.noBookingsTitle}>{strings.No_appointments}</Text>
        </View>
    }

    const loadMoreData = () => {
        setPageNo(pageNo + 1)
    }

    return (
        <View style={innerStyle.container}>
            <HeaderSehet hideshadow headerText={strings.appointment_history} navigation={props.navigation} />

            <FlatList
                style={{ flex: 1 }}
                scrollEnabled
                keyExtractor={(item, index) => index.toString()}
                data={pastHistory}
                renderItem={renderItem}
                ListEmptyComponent={<NoBookings />}
                onEndReached={loadMoreData}
                onEndReachedThreshold={0.3}
            />
        </View>
    )
}

export default Index

const innerStyle = StyleSheet.create({
    container: {
        flex: 1
    },
    subTitle: {
        fontSize: FONT_SIZE_13,
        color: "#A0A9BE",
    },
    addPres: {
        color: '#038BEF',
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_13,
    },
    presIconStyle: { width: 17, height: 17 },
    subIcon: {
        fontSize: FONT_SIZE_13,
        color: "#A0A9BE",
    },
    nameStyle: {
        fontSize: FONT_SIZE_16,
        paddingBottom: 5,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: "#041A32"
    },
    dateTimeContainer: {
        flex: 1, marginLeft: 10, paddingHorizontal: 5, justifyContent: 'space-around'
    },
});