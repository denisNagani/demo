import { View, Text, Image, StyleSheet, ScrollView } from 'react-native'
import React, { useRef, useState } from 'react'
import { FONT_FAMILY_SF_PRO_MEDIUM } from '../../../styles/typography'
import DateTimePicker from 'react-native-modal-datetime-picker'
import Images from '../../../assets/images/index'
import PickerUI from './PickerUI'
import moment from 'moment'
import DropDownPicker from 'react-native-dropdown-picker';
import { Icon } from 'native-base'
import MultiSelect from 'react-native-multiple-select'
import { isValid } from '../../../utils/ifNotValid'
import { useDispatch, useSelector } from 'react-redux'
import { showModal } from '../../../actions/modal'
import { errorPayload } from '../../../utils/helper'
import { createSlots } from './action'
import SaveCancelFooter from '../../../components/SaveCancelFooter'
import { strings } from '../../../utils/translation'

const AppointmentSlot = () => {
    //refs
    let multiSelect = useRef(null)
    const dispatch = useDispatch()
    const doctorProfile = useSelector(state => state.profile.userProfile?.userId)

    // states
    // select slot dropdown
    const [open, setOpen] = useState(false);
    const [open2, setOpen2] = useState(false);
    const [selectedSlots, setSelectedSlots] = useState(null);
    const [selectedSlotsType, setSelectedSlotsType] = useState(null);
    const [slots, setSlots] = useState([
        { label: '20 min', value: '20' },
        { label: '30 min', value: '30' },
        { label: '40 min', value: '40' },
        { label: '60 min', value: '60' },
    ]);

    const [slotsType, setSlotType] = useState([
        { label: 'ONLINE', value: 'ONLINE' },
        { label: 'OFFLINE', value: 'OFFLINE' },
    ]);

    // select days dropdown 
    const [items, setItems] = useState([
        { id: 'SUNDAY', name: 'SUNDAY' },
        { id: 'MONDAY', name: 'MONDAY' },
        { id: 'TUESDAY', name: 'TUESDAY' },
        { id: 'WEDNESDAY', name: 'WEDNESDAY' },
        { id: 'THURSDAY', name: 'THURSDAY' },
        { id: 'FRIDAY', name: 'FRIDAY' },
        { id: 'SATURDAY', name: 'SATURDAY' },
    ]);
    const [selectedDays, setSelectedDays] = useState([])

    const [appoinmentState, setAppointmentState] = useState({
        startTime: 'Select time',
        endTime: 'Select time',
        days: []
    })
    const [picker, setPicker] = useState({ startTimePicker: false, endTimePicker: false })

    // functions
    const onPickerStateChange = (key, value) => {
        setPicker({
            ...picker,
            [key]: value
        })
    }
    const onAppointmentStateChange = (key, value) => {
        setAppointmentState({
            ...appoinmentState,
            [key]: value
        })
    }

    const handleConfirmPicker1 = (date) => {
        const time = moment(date).format('HH:mm')
        onAppointmentStateChange("startTime", time)
        onPickerStateChange("startTimePicker", false)
    };
    const handleConfirmPicker2 = (date) => {
        const time = moment(date).format('HH:mm')
        onAppointmentStateChange("endTime", time)
        onPickerStateChange("endTimePicker", false)
    };
    const onSelectedItemsChange = (selectedItems) => {
        setSelectedDays(selectedItems)
    }

    const isValidSelectedDate = (appstate) => {
        return appstate != 'Select Date' ? true : false
    }

    const validation = () => {
        if (selectedDays?.length == 0) {
            dispatch(showModal(errorPayload('Please select days')))
            return false
        }
        if (!isValidSelectedDate(appoinmentState.startTime)) {
            dispatch(showModal(errorPayload('Please select start time')))
            return false
        }
        if (!isValidSelectedDate(appoinmentState.endTime)) {
            dispatch(showModal(errorPayload('Please select end time')))
            return false
        }
        if (!isValid(selectedSlots)) {
            dispatch(showModal(errorPayload('Please select slot length')))
            return false
        }
        if (!isValid(selectedSlotsType)) {
            dispatch(showModal(errorPayload('Please select slot type')))
            return false
        }
        return true
    }

    const onPressCreateSlot = () => {
        if (validation()) {
            const body = {
                dayName: selectedDays,
                doctorId: doctorProfile?._id,
                endTime: appoinmentState.endTime,
                interval: selectedSlots,
                startTime: appoinmentState.startTime,
                consultType: selectedSlotsType
            }
            console.log(body);
            dispatch(createSlots(body))
        }
    }

    const onCancelPressed = () => {
        setAppointmentState({
            startTime: 'Select time',
            endTime: 'Select time',
            days: []
        })
        setSelectedDays([])
        setSelectedSlots(null)
    }
    return (
        <View style={{ flex: 1 }}>

            <ScrollView style={{ flex: 1 }}>
                <View style={{ backgroundColor: '#E5F3FE', paddingHorizontal: 20, paddingVertical: 10 }}>
                    <Text style={{ fontSize: 18, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, color: '#041A32', marginVertical: 10 }}>{strings.select_days}</Text>
                    <MultiSelect
                        hideTags
                        items={items}
                        uniqueKey="id"
                        displayKey="name"
                        selectText={`${selectedDays?.length > 0 ? "  " : `   ${strings.select_days}`}`}
                        ref={(component) => {
                            multiSelect = component
                        }}
                        onSelectedItemsChange={onSelectedItemsChange}
                        selectedItems={selectedDays}
                        onChangeInput={(text) => console.log(text)}
                        selectedItemTextColor="#FF5E38"
                        selectedItemIconColor="#FF5E38"
                        // itemFontSize={13}
                        hideDropdown={true}
                        selectedItemIconType="AntDesign"
                        itemTextColor="#000"
                        searchInputStyle={{ color: '#CCC' }}
                        submitButtonColor="#FF5E38"
                        submitButtonText="Submit"
                    />
                </View>

                <View>
                    {multiSelect.current && multiSelect.current.getSelectedItemsExt &&
                        multiSelect.current.getSelectedItemsExt(selectedDays)}
                </View>


                <View style={{ flex: 1, paddingHorizontal: 20 }}>
                    <Text style={{ fontSize: 18, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, color: '#041A32', marginVertical: 10 }}>{strings.manage_appointment_slots}</Text>
                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.timeTitle}>{strings.start_time}</Text>
                        <PickerUI text={appoinmentState.startTime} icon={Images.timeIcon} onPress={() => onPickerStateChange("startTimePicker", !picker.startTimePicker)} textStyle={styles.timeTitle2} />
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.timeTitle}>{strings.end_time}</Text>
                        <PickerUI text={appoinmentState.endTime} icon={Images.timeIcon} onPress={() => onPickerStateChange("endTimePicker", !picker.endTimePicker)} textStyle={styles.timeTitle2} />
                    </View>
                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.timeTitle}>{strings.select_slots_length}</Text>
                        <DropDownPicker
                            style={{ borderWidth: 1, borderRadius: 8, borderColor: '#D9DCE5', marginTop: 8 }}
                            open={open}
                            value={selectedSlots}
                            items={slots}
                            setOpen={setOpen}
                            setValue={setSelectedSlots}
                            setItems={setSlots}
                            dropDownDirection="TOP"
                            placeholderStyle={{
                                color: '#041A32',
                                fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                                paddingHorizontal: 8
                            }}
                            placeholder="Select slot"
                            dropDownContainerStyle={{
                                borderColor: '#D9DCE5',
                            }}
                        />

                    </View>

                    <View style={{ marginTop: 10 }}>
                        <Text style={styles.timeTitle}>{"Select Slot Type"}</Text>
                        <DropDownPicker
                            style={{ borderWidth: 1, borderRadius: 8, borderColor: '#D9DCE5', marginTop: 8 }}
                            open={open2}
                            value={selectedSlotsType}
                            items={slotsType}
                            setOpen={setOpen2}
                            setValue={setSelectedSlotsType}
                            setItems={setSlotType}
                            dropDownDirection="TOP"
                            placeholderStyle={{
                                color: '#041A32',
                                fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                                paddingHorizontal: 8
                            }}
                            placeholder="Select slot type"
                            dropDownContainerStyle={{
                                borderColor: '#D9DCE5',
                            }}
                        />

                    </View>
                </View>

                <DateTimePicker
                    isVisible={picker.startTimePicker}
                    mode="time"
                    onConfirm={handleConfirmPicker1}
                    onCancel={() => onPickerStateChange("startTimePicker", false)}
                />
                <DateTimePicker
                    isVisible={picker.endTimePicker}
                    mode="time"
                    onConfirm={handleConfirmPicker2}
                    onCancel={() => onPickerStateChange("endTimePicker", false)}
                />
            </ScrollView>


            <SaveCancelFooter
                leftText={strings.create_slots}
                rightText={strings.cancel}
                onSaveClick={onPressCreateSlot}
                onCancelClick={onCancelPressed}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    timeTitle: { color: '#7F8A96', fontSize: 12, },
    timeTitle2: { color: '#041A32', fontSize: 14, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, marginHorizontal: 10 },
})

export default AppointmentSlot