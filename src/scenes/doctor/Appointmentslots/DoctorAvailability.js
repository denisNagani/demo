import { View, Text, StyleSheet, TouchableOpacity, FlatList, ScrollView } from 'react-native'
import React, { useEffect, useState } from 'react'
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_REGULAR } from '../../../styles/typography'
import PickerUI from './PickerUI'
import Images from '../../../assets/images'
import DateTimePicker from 'react-native-modal-datetime-picker'
import moment from 'moment'
import { Icon } from 'native-base'
import { useDispatch, useSelector } from 'react-redux'
import { applyLeave, cancelLeave, deleteDoctorSlots, getAllSlotsByDate, getDoctorSlots } from './action'
import { isValid } from '../../../utils/ifNotValid'
import { showModal } from '../../../actions/modal'
import { errorPayload } from '../../../utils/helper'
import { strings } from '../../../utils/translation'

const DoctorAvailability = () => {
    const dispatch = useDispatch()

    const doctorProfile = useSelector(state => state.profile.userProfile?.userId)
    const leavesTaken = useSelector(state => state.appointment_doc.allLeavesTaken)
    const slotsDataByDate = useSelector(state => state.appointment_doc.slotsByDate)

    const [picker, setPicker] = useState({
        leavePicker: false,
        deleteSlotPicker: false,
    })
    const [appState, setAppState] = useState({
        leaveDate: 'Select Date',
        deleteSlotDate: 'Select Date',
    })
    const [selectAllState, setSelectAllState] = useState(false)
    const onPickerStateChange = (key, value) => {
        setPicker({
            ...picker,
            [key]: value
        })
    }

    const onAppStateChange = (key, value) => {
        setAppState({
            ...appState,
            [key]: value
        })
    }
    const handleConfirmPicker1 = (date) => {
        const MY_DATE = moment(date).format('YYYY-MM-DD')
        onPickerStateChange("leavePicker", false)
        onAppStateChange("leaveDate", MY_DATE)
    }
    const handleConfirmPicker2 = (date) => {
        const MY_DATE = moment(date).format('YYYY-MM-DD')
        onPickerStateChange("deleteSlotPicker", false)
        onAppStateChange("deleteSlotDate", MY_DATE)

        const dt = moment(date, "YYYY-MM-DD HH:mm:ss")
        const day = dt.format('dddd')
        getSlotsByDateSelection(day)
    }

    const isValidSelectedDate = (appstate) => {
        return appstate != 'Select Date' ? true : false
    }

    const onPressApplyLeave = () => {
        if (isValidSelectedDate(appState.leaveDate)) {
            const body = {
                date: appState.leaveDate,
                doctorId: doctorProfile?._id
            }
            console.log(body)
            dispatch(applyLeave(body))
        } else {
            dispatch(showModal(errorPayload("Please select date")))
        }
    }

    const onPressDeleteSlots = () => {
        if (isValidSelectedDate(appState.deleteSlotDate)) {
            const selectedSlots = slotsDataByDate?.filter(item => item?.selected == true)
            if (selectedSlots?.length > 0) {
                const selectedSlotsId = selectedSlots?.map(itt => itt._id)
                const dt = moment(appState.deleteSlotDate, "YYYY-MM-DD HH:mm:ss")
                const day = dt.format('dddd')
                dispatch(deleteDoctorSlots(selectedSlotsId, day))
                setSelectAllState(false)
            } else {
                dispatch(showModal(errorPayload("Please select slots")))
            }
        } else {
            dispatch(showModal(errorPayload("Please select date")))
        }
    }
    const getSlotsByDateSelection = (day) => {
        dispatch(getDoctorSlots(day))
    }
    const onSelectAllClick = () => {
        setSelectAllState(!selectAllState)
        const data = slotsDataByDate?.map(item => {
            return {
                ...item,
                selected: !selectAllState
            }
        })
        dispatch(getAllSlotsByDate(data))
    }

    return (
        <ScrollView>
            <View style={{ backgroundColor: '#E5F3FE', paddingHorizontal: 20, paddingTop: 10, paddingBottom: 30 }}>
                <Text style={{ fontSize: 18, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, color: '#041A32', marginVertical: 10 }}>{strings.apply_for_leave}</Text>
                <View style={{ marginVertical: 10 }}>
                    <Text style={styles.timeTitle}>{strings.date_of_leave}</Text>
                    <PickerUI text={appState.leaveDate} icon={Images.calendar_icon} onPress={() => onPickerStateChange("leavePicker", true)} textStyle={styles.timeTitle2} />
                </View>
                <TouchableOpacity style={{ backgroundColor: '#FF5E38', padding: 10, borderRadius: 8 }} onPress={onPressApplyLeave}>
                    <Text style={{ color: '#fff', textAlign: 'center' }}>{strings.apply_for_leave}</Text>
                </TouchableOpacity>
            </View>

            <View style={{ paddingHorizontal: 20, paddingTop: 10, paddingBottom: 30 }}>
                <Text style={{ fontSize: 18, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, color: '#041A32', marginVertical: 10 }}>{strings.delete_slots}</Text>

                <View style={{ marginVertical: 10 }}>
                    <Text style={styles.timeTitle}>{strings.select_date}</Text>
                    <PickerUI text={appState.deleteSlotDate} icon={Images.calendar_icon} onPress={() => onPickerStateChange("deleteSlotPicker", true)} textStyle={styles.timeTitle2} />
                </View>

                {
                    slotsDataByDate?.length > 0 && <TouchableOpacity style={{ flexDirection: 'row', alignItems: 'center', alignSelf: 'flex-end', paddingVertical: 8, paddingHorizontal: 5 }} onPress={onSelectAllClick}>
                        <View style={[{ borderColor: '#D9DCE5', borderRadius: 3, borderWidth: 1, marginHorizontal: 2 }, { backgroundColor: selectAllState ? '#FF5E38' : '#FFF' }]}>
                            <Icon name='check' type='Feather' style={{ color: '#FFF', fontSize: 22 }} />
                        </View>
                        <Text style={{ color: '#041A32', fontSize: 13, marginLeft: 3 }}>{`Select All`}</Text>
                    </TouchableOpacity>
                }

                <FlatList
                    data={slotsDataByDate}
                    keyExtractor={(item, index) => index.toString()}
                    numColumns={2}
                    renderItem={({ item, index }) => {
                        const isSelectedColor = item?.selected ? '#FF5E38' : '#FFF'
                        const slotStartTime = moment(item?.startTime, "HHmmss").format("HH:mm")
                        const slotEndTime = moment(item?.endTime, "HHmmss").format("HH:mm")
                        const onPressItem = (item) => {
                            const newSlots = slotsDataByDate?.map(slot => {
                                if (item?.startTime === slot?.startTime) {
                                    slot.selected = !slot?.selected
                                }
                                return slot
                            })
                            dispatch(getAllSlotsByDate(newSlots))
                        }
                        return (
                            <TouchableOpacity key={index} style={{ flex: 1, flexDirection: 'row', alignItems: 'center', paddingVertical: 8 }} onPress={() => onPressItem(item)}>
                                <View style={[{ borderColor: '#D9DCE5', borderRadius: 3, borderWidth: 1, marginHorizontal: 2 }, { backgroundColor: isSelectedColor }]}>
                                    <Icon name='check' type='Feather' style={{ color: '#FFF' }} />
                                </View>
                                <Text style={{ color: '#041A32', fontSize: 13, marginLeft: 3 }}>{`${slotStartTime} to ${slotEndTime}`}</Text>
                            </TouchableOpacity>
                        )
                    }}
                    ListEmptyComponent={<Text style={styles.noDataStyle}>{strings.no_slots_available}</Text>}
                />

                <TouchableOpacity style={{ backgroundColor: '#FF5E38', padding: 10, borderRadius: 8 }} onPress={onPressDeleteSlots}>
                    <Text style={{ color: '#fff', textAlign: 'center' }}>{strings.delete_slots}</Text>
                </TouchableOpacity>
            </View>

            <View style={{ paddingHorizontal: 20, backgroundColor: '#FFF', paddingBottom: 10 }}>
                <Text style={{ fontSize: 18, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, color: '#041A32', marginVertical: 10 }}>{strings.your_applied_leaves}</Text>

                <FlatList
                    data={leavesTaken}
                    keyExtractor={(item, index) => index.toString()}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item }) => {
                        const dt = moment(item?.date, "YYYY-MM-DD HH:mm:ss")
                        const itemDay = dt.format('dddd')
                        const day = isValid(itemDay) ? itemDay : ''
                        const date = moment(item?.date).format('DD/MM/YYYY')
                        const status = item?.status


                        // Parse the provided date using Moment.js
                        const providedDate = moment(item?.date);
                        // Get the current date
                        const currentDate = moment();
                        // Compare the two dates
                        let datecheck = currentDate.isSameOrAfter(providedDate) ? true : false

                        const disableClick = status == "CANCELLED" || datecheck

                        const onCancelLeave = () => {
                            const body = {
                                _id: item?._id,
                                doctorId: item?.doctorId,
                                status: "CANCELLED",
                                date: item?.date,
                            }
                            dispatch(cancelLeave(body))
                        }
                        return (
                            <View style={[styles.card, { paddingHorizontal: 20, marginVertical: 10, marginHorizontal: 3, paddingVertical: 10, marginRight: 8 }]}>
                                <Text style={{ color: '#041A32', fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, fontSize: 14 }}>{date}</Text>
                                <Text style={{ color: '#7F8A96', fontSize: 11, fontFamily: FONT_FAMILY_SF_PRO_REGULAR, marginVertical: 3 }}>{day}</Text>
                                <Text style={{ color: '#038BEF', fontSize: 11, fontFamily: FONT_FAMILY_SF_PRO_REGULAR }}>{status}</Text>
                                <TouchableOpacity
                                    disabled={disableClick ? true : false}
                                    style={{ backgroundColor: disableClick == true ? 'lightgrey' : '#FBE7E3', marginVertical: 5, borderRadius: 5 }}
                                    onPress={onCancelLeave}
                                >
                                    <Text style={{ color: '#FF5E38', textAlign: 'center', padding: 3 }}>{strings.cancel}</Text>
                                </TouchableOpacity>
                            </View>
                        )
                    }}
                />
            </View>

            <DateTimePicker
                isVisible={picker.leavePicker}
                mode="date"
                minimumDate={new Date()}
                date={isValidSelectedDate(appState.leaveDate) ? new Date(appState.leaveDate) : new Date()}
                onConfirm={handleConfirmPicker1}
                onCancel={() => onPickerStateChange("leavePicker", false)}
            />
            <DateTimePicker
                isVisible={picker.deleteSlotPicker}
                mode="date"
                minimumDate={new Date()}
                date={isValidSelectedDate(appState.deleteSlotDate) ? new Date(appState.deleteSlotDate) : new Date()}
                onConfirm={handleConfirmPicker2}
                onCancel={() => onPickerStateChange("deleteSlotPicker", false)}
            />
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    timeTitle: { color: '#7F8A96', fontSize: 12, },
    timeTitle2: { color: '#041A32', fontSize: 14, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, marginHorizontal: 10 },
    card: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        elevation: 5,
        shadowColor: 'grey',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
    },
    noDataStyle: { textAlign: 'center', fontFamily: FONT_FAMILY_SF_PRO_REGULAR, marginBottom: 10, fontSize: 16 }
})

export default DoctorAvailability