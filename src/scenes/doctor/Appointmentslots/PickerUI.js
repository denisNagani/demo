import { View, Text, Image, TouchableOpacity } from 'react-native'
import React from 'react'
import Images from '../../../assets/images'

const PickerUI = ({ text, icon, onPress, textStyle }) => {
    return (
        <TouchableOpacity style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
            backgroundColor: '#FFFFFF',
            borderColor: '#D9DCE5',
            borderWidth: 1,
            padding: 8,
            marginTop: 5
        }} onPress={onPress}>
            <Text style={textStyle}>{text}</Text>
            <Image source={icon} style={{ height: 20, width: 20 }} resizeMode='contain' />
        </TouchableOpacity>
    )
}

export default PickerUI