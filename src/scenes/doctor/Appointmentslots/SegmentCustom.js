import React from "react";
import { StyleSheet, Text } from "react-native";
import { Button, Segment } from "native-base";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
import { getAllSlotsByDate, setAppointmentSlotActive } from "./action";
import { strings } from "../../../utils/translation";

const SegmentCustom = ({ }) => {
    const dispatch = useDispatch();
    const slotsDataByDate = useSelector(state => state.appointment_doc.slotsByDate)
    let activeButton = useSelector((state) => state.upcomingPast.appointmentActive);
    // ifValid(activeButton) ? activeButton = 'APPOINTMENT_SLOT' : "DOCTOR_AVALIBILITY"
    const onPressButton = () => {
        if (slotsDataByDate?.length > 0) {
            dispatch(getAllSlotsByDate([]))
        }
        activeButton === "APPOINTMENT_SLOT" ?
            dispatch(setAppointmentSlotActive("DOCTOR_AVALIBILITY")) :
            dispatch(setAppointmentSlotActive("APPOINTMENT_SLOT"))
    }


    return (
        <Segment style={styles.segment}>
            <Button
                first
                onPress={() => onPressButton()}
                style={[styles.segmentButton, activeButton === 'APPOINTMENT_SLOT' ? styles.btnActive : {
                    ...styles.btnInactive, borderTopRightRadius: 0, borderBottomRightRadius: 0,
                    borderTopLeftRadius: 5, borderBottomLeftRadius: 5,
                }]}>


                <Text style={activeButton === 'APPOINTMENT_SLOT' ? {
                    ...styles.segmentTextActive,
                    color: '#fff'
                } : {
                    ...styles.segmentTextActive,
                    color: '#4E5C76',
                }}>{strings.Appointmentslots}</Text>
            </Button>


            <Button
                onPress={() => onPressButton()}
                last
                style={[styles.segmentButton, activeButton === 'DOCTOR_AVALIBILITY' ? {
                    ...styles.btnActive,
                    borderTopRightRadius: 5,
                    borderTopLeftRadius: 0,
                    borderBottomLeftRadius: 0,
                    borderBottomRightRadius: 5
                } : styles.btnInactive]}
            >
                <Text style={activeButton === 'DOCTOR_AVALIBILITY' ? {
                    ...styles.segmentTextActive,
                    color: '#fff'
                } : {
                    ...styles.segmentTextActive,
                    color: '#4E5C76',
                }}>{strings.DoctorsAvailability}</Text>
            </Button>
        </Segment>
    );
}
const styles = StyleSheet.create({
    segment: {
        backgroundColor: "#F9F9F9",
        marginBottom: 10,
        marginTop: 5,
    },

    segmentButton: {
        height: hp("6.5%"),
        width: wp("45%"),
        justifyContent: "center",
        marginTop: hp("3%"),
        backgroundColor: '#F9F9F9'
    },

    btnActive: {
        backgroundColor: "#038BEF",
        fontWeight: "bold",
        color: "white",
        borderBottomLeftRadius: 5,
        borderTopLeftRadius: 5,
    },

    btnInactive: {
        backgroundColor: "#E6E8EC",
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
    },

    segmentTextActive: {
        fontWeight: "bold",
        fontSize: hp("2%"),
        color: "#fff",
    },
    segmentTextInActive: {
        fontSize: hp("2%"),
        color: "black",
        fontWeight: "bold",
    },
})
export default SegmentCustom;
