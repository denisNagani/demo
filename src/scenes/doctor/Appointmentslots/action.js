import { showModal } from "../../../actions/modal";
import http from "../../../services/http";
import { APPOINTMENT_SLOT_ACTIVE, GET_ALL_DOCTOR_LEAVES, GET_SLOTS_BY_DATE } from "../../../utils/constant";
import { successPayload } from "../../../utils/helper";
import { hide, show } from "../../../utils/loader/action";

export const setAppointmentSlotActive = (payload) => {
    return {
        type: APPOINTMENT_SLOT_ACTIVE,
        payload: payload,
    };
};

export const getDoctorLeaves = (docId) => {
    return async dispatch => {
        dispatch(show());
        http
            .get(`api/doctors/leaves/${docId}`, dispatch)
            .then(res => {
                console.log(res);
                if (res?.status === 200) {
                    dispatch(setAllLeaves(res?.data))
                    dispatch(hide());
                }
                dispatch(hide());
            })
            .catch(err => {
                console.log(err);
                dispatch(hide());
            })
    }
}

export const applyLeave = (body) => {
    return async dispatch => {
        dispatch(show());
        http
            .post(`api/doctors/leaves`, body, dispatch)
            .then(res => {
                console.log(res);
                if (res.status === 200) {
                    dispatch(showModal(successPayload("Applied for Leave succussfully")))
                    dispatch(getDoctorLeaves(body?.doctorId))
                }
                dispatch(hide());
            })
            .catch(err => {
                console.log(err);
                dispatch(hide());
            })
    }
}

export const cancelLeave = (body) => {
    return async dispatch => {
        dispatch(show());
        http
            .put(`api/doctors/leaves`, body, dispatch)
            .then(res => {
                console.log(res);
                if (res?.status === 200) {
                    dispatch(showModal(successPayload("Leave canceled succussfully")))
                    dispatch(getDoctorLeaves(body?.doctorId))
                }
                dispatch(hide());
            })
            .catch(err => {
                console.log(err);
                dispatch(hide());
            })
    }
}

export const createSlots = (body) => {
    return async dispatch => {
        dispatch(show());
        http
            .post(`api/createDynamicSlots`, body, dispatch)
            .then(res => {
                console.log(res);
                if (res?.status === 200) {
                    dispatch(showModal(successPayload("Slots created")))
                }
                dispatch(hide());
            })
            .catch(err => {
                console.log(err);
                dispatch(hide());
            })
    }
}

export const getDoctorSlots = (day) => {
    return async dispatch => {
        dispatch(show());
        http
            .get(`api/getdoctorSlotByDay/${day?.toUpperCase()}`, dispatch)
            .then(res => {
                console.log(res);
                if (res?.status === 200) {
                    dispatch(getAllSlotsByDate(res?.data))
                    dispatch(hide());
                } else {
                    dispatch(getAllSlotsByDate([]))
                }
                dispatch(hide());
            })
            .catch(err => {
                console.log(err);
                dispatch(hide());
            })
    }
}

export const deleteDoctorSlots = (slots, day) => {
    return async dispatch => {
        dispatch(show());
        http
            .delete(`api/deleteMultpileSlots?slots=${slots}`, dispatch)
            .then(res => {
                console.log(res);
                if (res?.status === 200) {
                    dispatch(getDoctorSlots(day))
                    dispatch(hide());
                }
                dispatch(hide());
            })
            .catch(err => {
                console.log(err);
                dispatch(hide());
            })
    }
}

export const setAllLeaves = (payload) => {
    return {
        type: GET_ALL_DOCTOR_LEAVES,
        payload: payload,
    };
};

export const getAllSlotsByDate = (payload) => {
    return {
        type: GET_SLOTS_BY_DATE,
        payload: payload,
    };
};