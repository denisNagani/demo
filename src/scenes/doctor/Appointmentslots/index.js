import { View, Text, SafeAreaView } from 'react-native'
import React, { useEffect } from 'react'
import HeaderSehet from '../../../components/header-common'
import { strings } from '../../../utils/translation'
import SegmentCustom from './SegmentCustom'
import AppointmentSlot from './AppointmentSlot'
import DoctorAvailability from './DoctorAvailability'
import { useDispatch, useSelector } from 'react-redux'
import SaveCancelFooter from '../../../components/SaveCancelFooter'
import { getAllSlotsByDate, getDoctorLeaves } from './action'

const index = (props) => {
    const dispatch = useDispatch()

    let activeButton = useSelector((state) => state.upcomingPast.appointmentActive);
    const doctorProfile = useSelector(state => state.profile.userProfile?.userId)
    const slotsDataByDate = useSelector(state => state.appointment_doc.slotsByDate)

    useEffect(() => {
        dispatch(getDoctorLeaves(doctorProfile?._id))
        if (slotsDataByDate?.length > 0) {
            dispatch(getAllSlotsByDate([]))
        }
    }, [])

    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
            <HeaderSehet headerText={strings.Appointmentslots} navigation={props.navigation} />
            <View style={{ flex: 1, justifyContent: 'space-between' }}>
                <SegmentCustom />
                <View style={{ flex: 1, paddingTop: 10 }}>
                    {
                        activeButton === "APPOINTMENT_SLOT"
                            ? <AppointmentSlot />
                            : <DoctorAvailability />
                    }
                </View>
            </View>
        </SafeAreaView>
    )
}

export default index