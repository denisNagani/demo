import React, { useEffect } from 'react'
import { View, Text, FlatList, TouchableOpacity } from 'react-native'
import HeaderSehet from '../../../components/header-common'
import { useDispatch, useSelector } from 'react-redux'
import { getUpcomingBookings, getPastBookings } from '../../../actions/bookings'
import ChatItem from '../../../components/ChatItem'
import NoBookings from '../../../components/NoBookings'
import { ScrollView } from 'react-native-gesture-handler'
import { FONT_FAMILY_SF_PRO_MEDIUM } from '../../../styles/typography'

const index = (props) => {

    const dispatch = useDispatch()

    const chats = useSelector(state => state.chat.chatData?.appointment)
    useEffect(() => {
    }, [])

    return (
        <View style={{
            flex: 1,
            backgroundColor: '#F9F9F9'
        }}>
            <HeaderSehet
                headerText={"Chat"}
                navigation={props.navigation}
            />
            <ScrollView>
                <Text style={{
                    marginHorizontal: 20,
                    marginTop: 8,
                    marginBottom: 5,
                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                    fontSize: 14,
                    textAlign: 'center',
                }}>You can chat with patient regarding your appointment only</Text>
                <FlatList
                    keyExtractor={(item, index) => index.toString()}
                    data={chats}
                    renderItem={({ item }) => (
                        <ChatItem item={item} navigation={props.navigation} />
                    )}
                    ListEmptyComponent={<NoBookings />}
                />
            </ScrollView>
            <Text>
            </Text>
        </View>
    )
}

export default index
