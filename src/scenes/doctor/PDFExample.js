import React from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import PDFView from "react-native-view-pdf";
import HeaderSehet from "../../components/header-common";
import {useDispatch, useSelector} from "react-redux";
import {ifNotValid} from "../../utils/ifNotValid";
import {successLog} from "../../utils/fireLog";
import {showModal} from "../../actions/modal";
import {errorPayload} from "../../utils/helper";

const resources = {
    file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
    url: 'https://sehet.s3.ap-south-1.amazonaws.com/pdf/5fbbc5e5b11344000aa07de2.pdf',
    base64: 'JVBERi0xLjMKJcfs...',
};

const PDFExample = (props) => {
    const dispatch = useDispatch();
    let pdfUrl = useSelector(state => state.appointedReducer.pdfUrl);
    successLog('pdfUrl ' + pdfUrl)

    return (
        <View style={{flex: 1}}>
            <HeaderSehet headerText={"PDF file"} navigation={props.navigation}/>
            {ifNotValid(pdfUrl) ? <View style={{flex: 1 ,alignItems:'center',justifyContent:'center'}}>
                    <Text>Invalid PDF File</Text>
                </View>: <PDFView
                    fadeInDuration={250.0}
                    style={{flex: 1}}
                    resource={pdfUrl}
                    resourceType={'url'}
                    onLoad={() => console.log(`PDF rendered from ${'url'}`)}
                    onError={(error) => {
                        dispatch(showModal(errorPayload('Something went wrong', error)))
                    }}/>}
        </View>
    );
}
export default PDFExample;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginTop: 25,
    },
    pdf: {
        flex: 1,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    }
});
