import React, { useEffect, useState } from "react";
import { Dimensions, FlatList, ScrollView, StyleSheet, Text, View, Image } from "react-native";
import { Container, Item, Content } from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { errorLog, successLog } from "../../../../utils/fireLog";
import { ifNotValid, ifValid } from "../../../../utils/ifNotValid";
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_FAMILY_SF_PRO_SemiBold,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_16,
    FONT_FAMILY_SF_PRO_BOLD
} from "../../../../styles/typography";
import HeaderSehet from "../../../../components/header-common";
import { calculate_age, errorPayload } from "../../../../utils/helper";
import common from "../../../../styles/common";
import SaveCancelFooter from "../../../../components/SaveCancelFooter";
import { showModal } from "../../../../actions/modal";
import { addMedicinePost } from "../action";
import SegmentCustom from "../SegmentCustom";
import Images from "../../../../assets/images";
import Line from "../../../../components/line";
import { strings } from "../../../../utils/translation";
let deviceWidth = Dimensions.get('window').width

const Preview = (props) => {
    const dispatch = useDispatch();
    const docProfile = useSelector((state) => state.userReducer.docProfile);
    const medicineOrComment = useSelector((state) => state.upcomingPast.active_tab);
    // const prescription = useSelector((state) => state.prescriptionReducer.prescription);
    const prescription = props.navigation.state.params.data;
    console.log("prop vale == " + JSON.stringify(prescription));
    //by default false..
    const prescriptionAdded = useSelector((state) => state.prescriptionReducer.prescriptionAdded);
    const selectedPatient = useSelector(state => state.prescriptionReducer.selectedPatient);
    const addMedicineId = useSelector((state) => state.prescriptionReducer.addMedicineId);

    useEffect(() => {
        successLog("Prescription form loaded..!")
    }, [])

    const checkValid = () => {
        let valid = true
        let msg = ''
        let _data = prescription.forEach((obj) => {
            if (obj.medicineName === '') {
                if (!msg.includes('Medicine'))
                    msg = msg + "Please fill Medicine name\n"
                valid = false
            }
            if (obj.duration === '') {
                if (!msg.includes('Duration'))
                    msg = msg + "Please fill Duration\n"
                valid = false
            }
            if (obj.typeOfMedicine === '') {
                if (!msg.includes('Type'))
                    msg = msg + "Please select Type of medicine\n"
                valid = false
            }

            if (obj.dose === '') {
                if (!msg.includes('dose'))
                    msg = msg + "Please select dose per day\n"
                valid = false
            }
            if (obj.eventOfDay === '') {
                if (!msg.includes('dosage'))
                    msg = msg + "Please select time of dosage in a day\n"
                valid = false
            }
            if (valid === false) {
                dispatch(showModal(errorPayload(msg)))
            }
        })
        return valid;
    }
    const onSaveClick = () => {

        // if (ifNotValid(addMedicineId)) {
        //     dispatch(showModal(errorPayload('Invalid Id')))
        //     return
        // }
        // if (!checkValid()) {
        //     errorLog('validation failed handled..!')
        //     return;
        // }

        // let _data = prescription.map(x => {
        //     return {
        //         medicineName: x.medicineName,
        //         duration: x.duration,
        //         typeOfMedicine: x.typeOfMedicine,
        //         dose: x.dose,
        //         eventOfDay: x.eventOfDay.join(),
        //     }
        // })

        // successLog(JSON.stringify(_data))
        try {
            let payload = {
                appointmentId: addMedicineId,
                medicine: prescription.medicine,
                comments: prescription.comments
            }
            dispatch(addMedicinePost(payload, props.navigation))
        } catch (e) {
            errorLog(e)
        }
    }

    const TableRow = ({ title, value }) => {

        return <View style={{ flexDirection: 'row', marginHorizontal: 10, }}>
            <Item bordered style={[{ flex: 1 }, common.itemBorderStyle]}>

                <Text style={{
                    padding: 4,
                    color: '#041A32',
                    fontSize: FONT_SIZE_15,
                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
                }}>{title}</Text>
            </Item>
            <Item bordered style={[{ flex: 2 }, common.itemBorderStyle]}>

                <Text style={{
                    padding: 4,
                    color: '#041A32',
                    fontSize: FONT_SIZE_14,
                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
                }}>{value}</Text>
            </Item>
        </View>

    }
    const _onAddPress = () => {
        try {
            props.navigation.navigate('AddMedicine')
        } catch (e) {
            errorLog(e)
        }
    }

    const headerView = () => {
        return <View style={{ flexDirection: 'row', marginHorizontal: 10, }}>
            <Item style={[common.itemBorderStyle, { width: 70, },]}>
                <Text style={styles.textStyle}>{'No.'}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 150 },]}>
                <Text style={styles.textStyle}>{strings.medicine_name} </Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 110 },]}>
                <Text style={styles.textStyle}>{strings.type}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 110 },]}>
                <Text style={styles.textStyle}>{strings.dose_per_day} </Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 200 },]}>
                <Text style={styles.textStyle}>{strings.time_of_dosage}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 90 },]}>
                <Text style={styles.textStyle}>{strings.Duration}</Text>
            </Item>
        </View>
    }


    const getPrescriptionDetails = () => {
        if (ifNotValid(prescription)) return;
        if (ifNotValid(prescription.medicine)) return;
        return <View>
            <Text style={styles.headingTitle}>Medicine Details</Text>

            <ScrollView horizontal={true}>
                <View>
                    {headerView()}
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        scrollEnabled
                        data={prescription.medicine}
                        renderItem={({ item, index }) => getSingleMedicineRows(item, index + 1)}
                    />
                    {ifValid(prescription.comments) ?
                        <View style={{ width: '94%', marginTop: 10, }}>
                            <Text style={[styles.headingTitle, { marginBottom: 0 }]}>Comment:</Text>
                            <Text style={[styles.headingTitle, {
                                marginTop: 0,
                                fontFamily: FONT_FAMILY_SF_PRO_REGULAR
                            }]}>{ifValid(prescription.comments) ? prescription.comments : ''}</Text>
                        </View> : null}
                </View>
            </ScrollView>
        </View>
    }

    const getSingleMedicineRows = (obj, index) => {
        successLog(JSON.stringify(obj))
        let medicineName = ifValid(obj.medicineName) ? obj.medicineName : '-'
        let typeOfMedicine = ifValid(obj.typeOfMedicine) ? obj.typeOfMedicine : '-'
        let dose = ifValid(obj.dose) ? obj.dose : '-'
        let eventOfDay = ifValid(obj.eventOfDay) ? obj.eventOfDay : '-'
        let duration = ifValid(obj.duration) ? `${obj.duration} ${obj.duration === 1 ? +" day" : ' days'}` : '-'
        return <View style={{ flexDirection: 'row', marginHorizontal: 10, }}>
            <Item style={[common.itemBorderStyle, { width: 70 },]}>
                <Text numberOfLines={1} style={styles.textStyle}>{index}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 150 },]}>
                <Text style={styles.textStyle}>{medicineName}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 110 },]}>
                <Text style={styles.textStyle}>{typeOfMedicine}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 110 },]}>
                <Text style={styles.textStyle}>{dose}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 200 },]}>
                <Text style={styles.textStyle}>{eventOfDay}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 90 },]}>
                <Text style={styles.textStyle}>{duration}</Text>
            </Item>
        </View>
    }

    const renderMedicines = (item) => {
        const medicines = prescription?.medicine;
        // alert(JSON.stringify(medicines))
        if (medicines.length === 1 && (medicines[0]?.duration == "" && medicines[0]?.dose == "" && medicines[0]?.typeOfMedicine == "" && medicines[0]?.eventOfDay == "" && medicines[0]?.medicineName == "")) {
            return null
        }
        return <View style={{ backgroundColor: '#FFF', marginHorizontal: 20, marginVertical: 10, borderRadius: 5, padding: 10 }}>
            <View style={{ marginHorizontal: 10 }}>
                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Image
                        source={Images.pill_capsule}
                        style={{ height: 20, width: 20, marginRight: 10 }}
                    />
                    <Text style={{ fontSize: 18, color: '#041A32', fontFamily: FONT_FAMILY_SF_PRO_BOLD }}>{item.medicineName}</Text>
                </View>

                <Line style={{ borderColor: '#EEEEEE', marginVertical: 5 }} />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={styles.medtitle}>{strings.Duration} : </Text>
                        <Text style={styles.medValue}>{item.duration}</Text>
                    </View>
                    <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={styles.medtitle}>{strings.type} : </Text>
                        <Text style={styles.medValue}>{item.typeOfMedicine}</Text>
                    </View>
                </View>
                <View style={{ marginVertical: 10, flexDirection: 'row', alignItems: 'center' }}>
                    <Text style={styles.medtitle}>{strings.dose_per_day} : </Text>
                    <Text style={styles.medValue}>{item.dose}</Text>
                </View>
                <View style={{ marginVertical: 10, flexDirection: 'row' }}>
                    <Text style={styles.medtitle}>{strings.time_of_dosage} : </Text>
                    <Text style={[styles.medValue, { flex: 1 }]}>{item.eventOfDay}</Text>
                </View>
            </View>
        </View>
    }

    const renderComment = (item) => {
        // console.log("comment val" + JSON.stringify(item));
        return <View style={{ backgroundColor: '#FFFFFF', marginHorizontal: 20, marginVertical: 10, borderRadius: 5, padding: 15 }}>
            <Text style={{
                color: '#041A32',
                fontSize: 15,
                fontFamily: FONT_FAMILY_SF_PRO_REGULAR
            }}>
                {item == undefined || item == null || item == ""
                    ? "No Comments Added"
                    : item
                }
            </Text>
        </View>
    }


    let patientId = ifNotValid(selectedPatient) || ifNotValid(selectedPatient.patientId) ? {} : selectedPatient.patientId;
    let name = patientId.fullName;
    if (ifNotValid(name)) name = "..."
    let age = calculate_age(patientId.dob);
    if (ifNotValid(age)) age = "..."
    let email = patientId.email;
    if (ifNotValid(email)) email = "..."
    let overview = patientId.medicalDescription;
    if (ifNotValid(overview)) overview = "..."
    return (
        <Container style={styles.container}>
            <View style={{ width: '100%' }}>
                <HeaderSehet
                    rightIconStyle={styles.rightIconStyle}
                    headerText={strings.prescription_preview} navigation={props.navigation} />
            </View>

            <SegmentCustom />
            <View style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
                <FlatList
                    scrollEnabled
                    keyExtractor={(item, index) => index.toString()}
                    data={medicineOrComment === "MEDICINES" ? prescription.medicine : [prescription.comments]}
                    renderItem={({ item }) => medicineOrComment === "MEDICINES" ? renderMedicines(item) : renderComment(item)}
                    ListEmptyComponent={<Text>No data</Text>}
                />
            </View>

            {/* <ScrollView showsVerticalScrollIndicator={false} style={{ width: '94%', marginTop: 10, }}>
                {getPrescriptionDetails()}
            </ScrollView> */}

            <View style={{ width: '100%' }}>
                <SaveCancelFooter
                    // leftText="Preview"
                    onSaveClick={() => onSaveClick()}
                    // onSaveClick={() => onPreviewClick()}
                    onCancelClick={() => props.navigation.goBack()}
                />
            </View>

        </Container>
    );
};

export default Preview;
const styles = StyleSheet.create({
    rowStyle: {
        textAlign: 'center',
        borderWidth: 0.4, borderColor: 'black', width: '8%', alignItems: 'center', padding: 1,
    },
    textStyle: { textAlign: 'center', paddingLeft: 2, paddingVertical: 10, margin: 1, flex: 1, },
    textHeaderStyle: { textAlign: 'center' },
    container: {
        // alignItems: "center", 
        flex: 1,
        width: '100%',
        backgroundColor: '#F9F9F9'
    },
    headingTitle: {
        marginHorizontal: 10,
        marginVertical: 10,
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold, fontSize: FONT_SIZE_16, color: 'black'
    },
    medtitle: {
        fontSize: 14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: '#656E83'
    },
    medValue: {
        fontSize: 15,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: '#041A32'
    }
})
