import React from "react";
import { StyleSheet, Text } from "react-native";
import { Button, Segment } from "native-base";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
import { setPreviewScreen } from "./action";
import { strings } from "../../../utils/translation";

const SegmentCustom = ({ }) => {
    const dispatch = useDispatch();
    let activeButton = useSelector((state) => state.upcomingPast.active_tab);
    // ifValid(activeButton) ? activeButton = 'UPCOMING' : "PAST"
    const onPressButton = () => {
        activeButton === "MEDICINES" ?
            dispatch(setPreviewScreen("COMMENT")) :
            dispatch(setPreviewScreen("MEDICINES"))
    }


    return (
        <Segment style={styles.segment}>
            <Button
                first
                onPress={() => onPressButton()}
                style={[styles.segmentButton, activeButton === 'MEDICINES' ? styles.btnActive : {
                    ...styles.btnInactive, borderTopRightRadius: 0, borderBottomRightRadius: 0,
                    borderTopLeftRadius: 8, borderBottomLeftRadius: 8,
                }]}>


                <Text style={activeButton === 'MEDICINES' ? {
                    ...styles.segmentTextActive,
                    color: '#fff'
                } : {
                        ...styles.segmentTextActive,
                        color: '#4E5C76',
                    }}>{strings.medicines}</Text>
            </Button>


            <Button
                onPress={() => onPressButton()}
                last
                style={[styles.segmentButton, activeButton === 'COMMENT' ? {
                    ...styles.btnActive,
                    borderTopRightRadius: 8,
                    borderTopLeftRadius: 0,
                    borderBottomLeftRadius: 0,
                    borderBottomRightRadius: 8
                } : styles.btnInactive]}
            >
                <Text style={activeButton === 'COMMENT' ? {
                    ...styles.segmentTextActive,
                    color: '#fff'
                } : {
                        ...styles.segmentTextActive,
                        color: '#4E5C76',
                    }}>{strings.comment}</Text>
            </Button>
        </Segment>
    );
}
const styles = StyleSheet.create({
    segment: {
        backgroundColor: "#F9F9F9",
        marginVertical: hp("3%"),
    },

    segmentButton: {
        height: hp("6%"),
        width: wp("45%"),
        justifyContent: "center",
        marginTop: hp("2%"),
        backgroundColor: '#F9F9F9'
    },

    btnActive: {
        backgroundColor: "#038BEF",
        fontWeight: "bold",
        color: "white",
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
    },

    btnInactive: {
        backgroundColor: "#F5F6F9",
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
    },

    segmentTextActive: {
        fontWeight: "bold",
        fontSize: hp("2%"),
        color: "#fff",
    },
    segmentTextInActive: {
        fontSize: hp("2%"),
        color: "black",
        fontWeight: "bold",
    },
})
export default SegmentCustom;
