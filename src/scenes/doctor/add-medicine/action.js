import { hide, show } from "../../../utils/loader/action";
import http from "../../../services/http";
import { errorLog, successLog } from "../../../utils/fireLog";
import { ADD_MEDICINE_RESPONSE, PREVIEW_TAB_SCREEN } from "../../../utils/constant";
import { ifNotValid } from "../../../utils/ifNotValid";
import { getPastBookings } from "../../../actions/bookings";
import { showModal } from "../../../actions/modal";

export const addMedicinePost = (body, navigation) => {

    return (dispatch) => {
        dispatch(show());
        console.log("action body ==== ", body);
        http.post(`api/doctors/prescription`, body, dispatch)
            .then((res) => {
                successLog("addMedicinePost ", res.status)
                dispatch(hide());
                if (ifNotValid(res.data)) {
                    successLog(JSON.stringify(res.data) + "wrong data ")
                    return
                }
                dispatch({ type: ADD_MEDICINE_RESPONSE, payload: res.data });
                dispatch(getPastBookings());
                const msg = {
                    text: "Prescription added successfully!!",
                    subText: "",
                    iconName: "checkcircleo",
                    modalVisible: true,
                };
                dispatch(showModal(msg))
                navigation.navigate("UpcomingPastBookingsDoctor")
            })
            .catch((o) => {
                errorLog("addMedicinePost", o)
                dispatch(hide());
            });
    };
};

export const setPreviewScreen = (screen_name) => {
    return dispatch => {
        dispatch({
            type: PREVIEW_TAB_SCREEN,
            payload: screen_name
        })
    }
}
