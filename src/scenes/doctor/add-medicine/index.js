import React, { useEffect, useRef, useState } from "react";
import { ScrollView, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import { Container, Icon } from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { errorLog, successLog } from "../../../utils/fireLog";
import HeaderSehet from "../../../components/header-common";
import PickerCommon from "../../../components/PickerCommon";
import InputCommon from "../../../components/InputCommon";
import SaveCancelFooter from "../../../components/SaveCancelFooter";
import { addMedicinePost } from "./action";
import { ifNotValid } from "../../../utils/ifNotValid";
import { showModal } from "../../../actions/modal";
import { errorPayload } from "../../../utils/helper";
import { strings } from "../../../utils/translation";
import MultiSelect from 'react-native-multiple-select';
import common from "../../../styles/common";
import { Card } from "react-native-elements";

// Before Breakfast",
// "After Breakfast",
//     "Before Lunch",
//     "After Lunch",
//     "Before Dinner",
//     "After Dinner"
const items = [
    { id: 'Before Breakfast', name: 'Before Breakfast' },
    { id: 'After Breakfast', name: 'After Breakfast' },
    { id: 'Before Lunch', name: 'Before Lunch' },
    { id: 'After Lunch', name: 'After Lunch' },
    { id: 'Before Dinner', name: 'Before Dinner' },
    { id: 'After Dinner', name: 'After Dinner' },
    { id: 'Before Sleep', name: 'Before Sleep' }
];

const AddMedicine = (props) => {
    const dispatch = useDispatch();
    const docProfile = useSelector((state) => state.userReducer.docProfile);
    const addMedicineId = useSelector((state) => state.prescriptionReducer.addMedicineId);
    const [amountCount, setAmountCount] = useState(2);
    const [comment, setComment] = useState('');
    const [data, setData] = useState([
        { srNo: 1, medicineName: "", duration: '', typeOfMedicine: '', dose: '', eventOfDay: '' },
    ]);
    const [selectedItems, setSelectedItems] = useState([]);
    let multiSelect = useRef(null)

    useEffect(() => {
        successLog("Prescription form loaded..!")
    }, [])

    const checkValid = () => {
        let valid = true
        let msg = ''
        // let _data = data.forEach((obj) => {
        //     if (obj.medicineName === '') {
        //         if (!msg.includes('Medicine'))
        //             msg = msg + "Please fill Medicine name\n"
        //         valid = false
        //     }
        //     if (obj.duration === '') {
        //         if (!msg.includes('Duration'))
        //             msg = msg + "Please fill Duration\n"
        //         valid = false
        //     }
        //     if (obj.typeOfMedicine === '') {
        //         if (!msg.includes('Type'))
        //             msg = msg + "Please select Type of medicine\n"
        //         valid = false
        //     }

        //     if (obj.dose === '') {
        //         if (!msg.includes('dose'))
        //             msg = msg + "Please select dose per day\n"
        //         valid = false
        //     }
        //     if (obj.eventOfDay === '') {
        //         if (!msg.includes('dosage'))
        //             msg = msg + "Please select time of dosage in a day\n"
        //         valid = false
        //     }
        //     if (valid === false) {
        //         dispatch(showModal(errorPayload(msg)))
        //     }
        // })
        if (comment == "") {
            valid = false
        }
        if (valid === false) {
            dispatch(showModal(errorPayload("comment required")))
        }
        return valid;
    }

    const onSaveClick = () => {

        if (ifNotValid(addMedicineId)) {
            dispatch(showModal(errorPayload('Invalid Id')))
            return
        }
        if (!checkValid()) {
            errorLog('validation failed handled..!')
            return;
        }

        let _data = data.map(x => {
            return {
                medicineName: x.medicineName,
                duration: x.duration,
                typeOfMedicine: x.typeOfMedicine,
                dose: x.dose,
                eventOfDay: x.eventOfDay.join(),
            }
        })

        successLog(JSON.stringify(_data))
        try {
            let payload = {
                appointmentId: addMedicineId,
                medicine: _data,
                comments: comment
            }
            dispatch(addMedicinePost(payload, props.navigation))
        } catch (e) {
            errorLog(e)
        }
    }
    const onPreviewClick = () => {

        if (ifNotValid(addMedicineId)) {
            dispatch(showModal(errorPayload('Invalid Id')))
            return
        }
        if (!checkValid()) {
            errorLog('validation failed handled..!')
            return;
        }

        let _data = data.map(x => {
            return {
                medicineName: x.medicineName,
                duration: x.duration,
                typeOfMedicine: x.typeOfMedicine,
                dose: x.dose,
                eventOfDay: x?.eventOfDay != '' ? x.eventOfDay.join() : "",
            }
        })

        successLog(JSON.stringify(_data))
        try {
            let payload = {
                appointmentId: addMedicineId,
                medicine: _data,
                comments: comment
            }
            props.navigation.navigate('Preview', {
                data: payload
            })
            // console.log("preview data === ", payload);
        } catch (e) {
            errorLog(e)
        }
    }

    const onAddMoreClick = () => {
        setData([...data, {
            srNo: data.length + 1,
            medicineName: '',
            duration: '',
            typeOfMedicine: '',
            dose: 0,
            eventOfDay: '',
        }])
    }
    const onRemoveClick = (srNo) => {
        if (ifNotValid(srNo)) {
            errorLog('invalid srno')
            return
        }
        let _data = data.filter((x) => {
            return x.srNo !== srNo;
        })
        if (ifNotValid(_data)) {
            errorLog('invalid data filtered')
            return
        }
        setData(_data)
    }

    const onMedicineNameChange = (srNo, value) => commonFilter(srNo, value, 'medicineName')
    const onDurationChange = (srNo, value) => commonFilter(srNo, value, 'duration')
    const onTypeChange = (srNo, value) => commonFilter(srNo, value, 'typeOfMedicine')
    const onDoseChange = (srNo, value) => commonFilter(srNo, value, 'dose')
    const onSelectedItemsChange = (srNo, value) => commonFilter(srNo, value, 'eventOfDay')
    // const onHowOftenChange = (srNo, value) => commonFilter(srNo, value, 'howOften')
    // const onTimeOfDayChange = (srNo, value) => commonFilter(srNo, value, 'timeOfDay')
    // const onRemarkChange = (srNo, value) => commonFilter(srNo, value, 'remark')
    const commonFilter = (srNo, value, KeyName) => {
        try {
            let _data = data.map((obj) => srNo === obj.srNo ? { ...obj, [KeyName]: value } : obj)
            setData(_data)
        } catch (e) {
            errorLog(e)
        }
    }


    return (
        <Container style={styles.container}>
            <HeaderSehet headerText={strings.add_medicine} navigation={props.navigation} />
            <ScrollView>
                {data.map((obj, index) => {
                    return <View style={[{ flex: 1, margin: 6 }]}>
                        <Card key={index.toString()}>
                            <View style={{ flexDirection: 'row' }}>

                                <View style={{ flex: 8.5 }}>
                                    <InputCommon styleInput={{}}
                                        value={obj.medicineName}
                                        onChangeText={(v) => onMedicineNameChange(obj.srNo, v)}
                                        title={strings.medicine_name} />
                                </View>
                            </View>
                            <InputCommon style={{ marginTop: 15 }}
                                value={obj.duration}
                                placeholder={"For how many days"}
                                keyboardType="numeric"
                                returnKeyType="done"
                                maxLength={3}
                                onChangeText={(v) => onDurationChange(obj.srNo, v)}
                                title={strings.Duration} />

                            <View style={{ flexDirection: 'row', marginTop: 18, marginBottom: 15 }}>
                                <PickerCommon
                                    data={[
                                        // { label: 'Pill', 'value': 'Pill' },
                                        { label: 'Tab', 'value': 'Tab' },
                                        { label: 'Cap', 'value': 'Cap' },
                                        { label: 'Capt', 'value': 'Capt' },
                                        { label: 'Susp', 'value': 'Susp' },
                                        { label: 'Syrup', 'value': 'Syrup' },
                                        { label: 'Inhalers', 'value': 'Inhalers' },
                                        { label: 'Injection', 'value': 'Injection' },
                                        { label: 'Gel', 'value': 'Gel' },
                                        { label: 'Spray', 'value': 'Spray' },
                                        { label: 'Others', 'value': 'Others' },
                                    ]}
                                    value={obj.typeOfMedicine}
                                    onDropValueChange={(v) => onTypeChange(obj.srNo, v)}
                                    title={strings.type}
                                    style={{ marginRight: 9, flex: 1, }} />
                                <View style={{ flex: 1, }}>
                                    {/*<Text style={common.titleStyle_}>Dose per Day</Text>*/}
                                    {/*<View style={styles.boxStyleAmount}>*/}
                                    {/*    <TouchableIcon onPress={() => onAmountIncrement(obj.srNo, 'd')} icon='minus'*/}
                                    {/*                   style={[styles.minusStyle, {backgroundColor: '#f3efef'}]}/>*/}
                                    {/*    <Text>{obj.amount}</Text>*/}
                                    {/*    <TouchableIcon onPress={() => onAmountIncrement(obj.srNo, 'i')} icon='plus'*/}
                                    {/*                   style={styles.minusStyle}/>*/}
                                    {/*</View>*/}
                                    <PickerCommon
                                        data={[
                                            { label: '1 Time', 'value': '1 Time' },
                                            { label: '2 Times', 'value': '2 Times' },
                                            { label: '3 Times', 'value': '3 Times' },
                                        ]}
                                        value={obj.dose}
                                        onDropValueChange={(v) => onDoseChange(obj.srNo, v)}
                                        title={strings.dose_per_day}
                                        style={{ flex: 1, }} />
                                </View>
                            </View>

                            <Text style={[common.titleStyle_, { marginBottom: 5 }]}> {strings.time_of_dosage}</Text>
                            <View style={[common.boxStyleAmount]}>
                                <View style={{ marginTop: 1.5, }}>
                                    <MultiSelect
                                        hideTags
                                        items={items}
                                        uniqueKey="id"
                                        ref={(component) => {
                                            multiSelect = component
                                        }}
                                        onSelectedItemsChange={(v) => onSelectedItemsChange(obj.srNo, v)}
                                        selectedItems={obj.eventOfDay}
                                        onChangeInput={(text) => console.log(text)}
                                        tagRemoveIconColor="#CCC"
                                        tagBorderColor="#CCC"
                                        tagTextColor="black"
                                        selectedItemTextColor="blue"
                                        searchIcon={<Icon name="search" type="MaterialIcons" style={{ fontSize: 22 }} />}
                                        iconSearch={false}
                                        textInputProps={{ autoFocus: false, style: { padding: 0, margin: 0, } }}
                                        hideDropdown={true}
                                        selectedItemIconColor="blue"
                                        selectedItemIconType="AntDesign"
                                        itemTextColor="#000"
                                        displayKey="name"
                                        searchInputStyle={{ color: '#CCC' }}
                                        submitButtonColor="red"
                                        submitButtonText="Submit"
                                        styleDropdownMenuSubsection={[{
                                            borderBottomColor: 'transparent',
                                        }]}
                                    />
                                </View>
                                <View>
                                    {multiSelect.current && multiSelect.current.getSelectedItemsExt &&
                                        multiSelect.current.getSelectedItemsExt(obj.eventOfDay)}
                                </View>
                            </View>


                            {data.length - 1 === index ?
                                <View style={styles.addRemoveContainer}>

                                    {data.length >= 2 ? <TouchableOpacity style={styles.removeButton}
                                        onPress={() => onRemoveClick(obj.srNo)}>
                                        <Text style={styles.removeTextStyle}>{strings.remove}</Text>
                                    </TouchableOpacity> : null}

                                    <TouchableOpacity style={{ backgroundColor: '#FF5E38', borderRadius: 10, padding: 8 }}
                                        onPress={() => onAddMoreClick(data.length - 1)}>
                                        <Text style={styles.addMoreTextStyle}>{strings.add_more}</Text>
                                    </TouchableOpacity>

                                </View>
                                : null}
                        </Card>

                    </View>
                })}
                <InputCommon
                    style={{ margin: 20 }}
                    value={comment}
                    // numberOfLines={3}
                    // multiline={true}
                    textAlignVertical='top'
                    placeholder={"Comment"}
                    onChangeText={(v) => setComment(v)}
                    title={strings.comment}
                    styleInput={{ height: 50 }}
                />
            </ScrollView>



            <SaveCancelFooter
                leftText={strings.preview}
                // onSaveClick={() => onSaveClick()}
                onSaveClick={() => onPreviewClick()}
                onCancelClick={() => props.navigation.goBack()}
            />
        </Container>
    );
};

export default AddMedicine;
const styles = StyleSheet.create({
    container: { flex: 1, backgroundColor: '#F9F9F9' },
    addRemoveContainer: {
        flexDirection: 'row', flex: 1, justifyContent: 'flex-end',
        alignItems: 'flex-end', marginTop: 10, paddingLeft: 10,
    },
    removeButton: { backgroundColor: '#F9F9F9', borderRadius: 10, marginRight: 15, padding: 10 },
    addMoreTextStyle: { paddingHorizontal: 8, paddingVertical: 1, color: 'white' },
    removeTextStyle: { paddingHorizontal: 8, paddingVertical: 1, color: 'black' },
    cardContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 10,
        padding: 15,
    },
    minusStyle: {
        backgroundColor: '#81c5f7',
        borderRadius: 20, fontSize: 20, padding: 2, color: 'black'
    },
    boxStyleAmount: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: 6, flex: 1, borderRadius: 9, borderColor: '#B6BECB', borderWidth: 0.6,
        padding: 8,
        justifyContent: 'space-between'
    },
})
