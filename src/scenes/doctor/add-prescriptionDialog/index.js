import React, { useEffect, useState } from "react";
import { Image, Modal, StyleSheet, Text, TouchableOpacity, View } from "react-native";

import { useDispatch, useSelector } from "react-redux";

import Images from "../../../assets/images";
import ImageContainer from "../../../components/ImageContainer";
import { successLog } from "../../../utils/fireLog";
import { ifNotValid, ifValid } from "../../../utils/ifNotValid";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_18, FONT_SIZE_20, FONT_SIZE_30 } from "../../../styles/typography";
import { setPrescriptionModel } from "../../../actions/presModel";


const AddPrescriptionDialog = ({ props }) => {
    const dispatch = useDispatch();
    const selectedAppointment = useSelector((state) => state.prescriptionReducer.selectedAppointment);
    const presModel = useSelector((state) => state.agoraReducer.presModel);

    const doctorId = ifValid(selectedAppointment) ? ifValid(selectedAppointment.doctorId) ? selectedAppointment.doctorId : {} : {}
    const patientId = ifValid(selectedAppointment) ? ifValid(selectedAppointment.intakeInfo) ? selectedAppointment.intakeInfo : {} : {}

    let patientName = doctorId.fullName;
    if (ifNotValid(patientName)) patientName = "..."

    let doctorName = ifValid(selectedAppointment?.intakeInfo) ? selectedAppointment?.intakeInfo?.name : "...";

    useEffect(() => {
        successLog("Prescription form loaded..!")
    }, [])

    return (
        <View style={styles.centeredView}>
            <Modal
                animationType="slide"
                transparent={true}
                onDismiss={() => {
                    dispatch(setPrescriptionModel(false))
                }}
                visible={presModel}
                onRequestClose={() => {
                    dispatch(setPrescriptionModel(false))
                }}
            >
                <ImageContainer>
                    <View style={styles.centeredView}>
                        <View style={styles.modalView}>
                            <Text style={styles.headerText}>Hello,</Text>
                            <Text style={[styles.headerText, { marginTop: 0 }]}>{patientName}</Text>
                            <Text style={styles.subHeader}>Please add prescription</Text>
                            {/*<Text style={[styles.subHeader, {marginTop: 0}]}>for your patient Carl Pope</Text>*/}
                            <Text style={[styles.subHeader, { marginTop: 0, marginBottom: 30 }]}>for your patient {doctorName}</Text>
                            <TouchableOpacity onPress={() => {
                                dispatch(setPrescriptionModel(false));
                                props.navigation.navigate('PrescriptionForm')
                            }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#FF5E38', paddingHorizontal: 15, paddingVertical: 5, justifyContent: 'center' }}>
                                    <Text style={{
                                        color: 'white',
                                        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                                        fontSize: 16
                                    }}>Add Prescription</Text>
                                    <Image source={Images.plusWhite}
                                        style={{ tintColor: 'white' }} />
                                </View>
                            </TouchableOpacity>
                            <Text onPress={() => dispatch(setPrescriptionModel(false))

                            } style={styles.doLaterStyle}>I will do it later</Text>
                        </View>
                    </View>
                </ImageContainer>
            </Modal>
        </View>
    );
};

export default AddPrescriptionDialog;
const styles = StyleSheet.create({
    headerText: {
        marginTop: 20,
        fontSize: FONT_SIZE_30,
        textAlign: 'center',
        color: 'rgb(72, 79, 95)',
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
    },
    subHeader: {
        marginTop: 20,
        paddingHorizontal: 10,
        fontSize: FONT_SIZE_20,
        textAlign: 'center',
        color: 'rgb(3, 139, 239)',
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
    },
    presStyle: {
        alignItems: 'center',
        justifyContent: 'center',
        marginTop: 20,
        fontSize: FONT_SIZE_20,
        textAlign: 'center',
        color: 'white',
        paddingHorizontal: 20,
        paddingVertical: 6,
        borderRadius: 6,
        backgroundColor: '#FF5E38',
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
    },
    doLaterStyle: {
        marginTop: 20,
        marginBottom: 20,
        fontSize: FONT_SIZE_18,
        textAlign: 'center',
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: "#A3ACC0"
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    plusStyle: { width: 20.75, height: 20.75 },
    modalView: {
        margin: 10,
        padding: 20,
        backgroundColor: "white",
        borderRadius: 10,
        alignItems: "center",
        shadowColor: "#0000000B",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },

})
