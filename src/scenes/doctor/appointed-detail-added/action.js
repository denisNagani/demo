import {hide, show} from "../../../utils/loader/action";
import http from "../../../services/http";
import {errorLog, successLog} from "../../../utils/fireLog";
import {ifNotValid} from "../../../utils/ifNotValid";
import {GET_APPOINTED_DETAILS, PDF_URL} from "../../../utils/constant";


export const setUpcomingPastDetailView = (payload) => {
    return {
        type: 'UPCOMING_PAST_DETAIL_ACTIVE',
        payload: payload,
    };
};


export const getAppointedDetails = (id='5fbbc5e5b11344000aa07de2') => {

    return (dispatch) => {
        dispatch(show());
        http.get(`prescription/${id}`, dispatch)
            .then((res) => {
                successLog("getAppointedDetails ", res.status)
                dispatch(hide());
                if (ifNotValid(res.data)) {
                    successLog(JSON.stringify(res.data) + "wrong data ")
                    return
                }
                dispatch({type: GET_APPOINTED_DETAILS, payload: res.data});
            })
            .catch((o) => {
                errorLog("getAppointedDetails", o)
                dispatch(hide());
            });
    };
};

export const setPdfUrl = (payload) => {
    return {
        type: PDF_URL,
        payload: payload,
    };
};
