import React, { useEffect, useState } from "react";
import { Alert, Image, Platform, ScrollView, StyleSheet, TouchableOpacity, View } from "react-native";
import { Button, Container, Footer, Text } from "native-base";

import Images from "../../../assets/images";
import { useDispatch, useSelector } from "react-redux";
import { ifNotValid, ifValid, isValid } from "../../../utils/ifNotValid";
import HeaderSehet from "../../../components/header-common";
import {
    FONT_FAMILY_SF_PRO_BOLD,
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_FAMILY_SF_PRO_SemiBold,
    FONT_SIZE_12,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_26
} from "../../../styles/typography";
import { getAppointedDetails, setPdfUrl } from "./action";
import moment from "moment";
import styles from "../../patient/appointment-detail/style";
import { setSelectedPatient } from "../perscription-form/components/action";
import { successLog } from "../../../utils/fireLog";
import { strings, TranslationConfig } from "../../../utils/translation";
// import PowerTranslator from "react-native-power-translator";
import PowerTranslator from "../../../components/PowerTranslator";
import MediaPreview from "../../../components/ChatScreen/component/MediaPreview";
import { showMediaModal, showModal } from "../../../actions/modal";
import { PERMISSIONS, RESULTS, checkMultiple, openSettings, requestMultiple } from "react-native-permissions";
import { hide, show } from "../../../utils/loader/action";
import RNFetchBlob from 'rn-fetch-blob'
import { v4 as uuidv4 } from 'uuid';

const AppointedDetailsAdded = (props) => {
    const dispatch = useDispatch();
    const navigation = props.navigation;
    const doctorSlot = useSelector((state) => state.appointment.docSlot);
    let upcomingPast = useSelector((state) => state.upcomingPast.detailActive);
    let appointedDetails = useSelector((state) => state.appointedReducer.appointedDetails);
    const modalVisible = useSelector(state => state.modal.mediaModal)
    const [documentData, setDocumentData] = useState({
        type: "",
        url: ""
    })
    let data = props.navigation.state.params.data;

    let intakeForm = {
        name: isValid(data?.intakeInfo?.name) ? data.intakeInfo.name : data?.patientId?.fullName,
        age: isValid(data?.intakeInfo?.dob) ? moment().diff(data.intakeInfo.dob, 'years') : moment().diff(data?.patientId?.dob, 'years'),
        gender: isValid(data?.intakeInfo?.gender) ? data.intakeInfo.gender : data?.patientId?.gender,
        email: isValid(data?.intakeInfo?.email) ? data.intakeInfo.email : data?.patientId?.email,
    }

    const prescriptionStatus = 'ADDED_PRES';
    const attachmentAdded = false;

    const lang = useSelector(state => state.profile.selected_language)
    TranslationConfig(lang)

    // if (ifNotValid(upcomingPast)) {
    //     upcomingPast = "UPCOMING"
    //     console.log("details view type not provided..!")
    // }

    useEffect(() => {
        dispatch(getAppointedDetails(undefined))
    }, []);


    const keyExtractor = (item, index) => index.toString();

    const HeaderBookAppointment = () => {

        if (ifNotValid(data)) return <View />

        let fullName = ifValid(data) && ifValid(data.patientId) ? data.patientId.fullName : undefined;
        if (ifNotValid(fullName)) fullName = "..."

        const patient = data.patientId;
        let hindiFullName = patient.hindiFullName;
        if (ifNotValid(hindiFullName)) hindiFullName = fullName

        let experience = data.experience
        if (ifNotValid(experience)) experience = "..."

        let _imageUrl = ifValid(data) && ifValid(data.patientId) ? data.patientId.imageUrl : undefined
        if (ifNotValid(_imageUrl)) _imageUrl = Images.user
        else _imageUrl = { uri: _imageUrl }

        let gender = isValid(patient?.gender) ? patient?.gender?.toLowerCase() : "-";
        let age = isValid(patient?.dob) ? moment().diff(patient?.dob, 'years') : "-"

        return <View style={{
            backgroundColor: '#FFFFFF',
            height: '25%',
            shadowColor: '#0000000B',
            shadowOffset: { width: 0, height: 3 },
            shadowOpacity: 1,
            shadowRadius: 5,
            elevation: 5
        }}>
            <HeaderSehet hideshadow headerText={strings.appointment_details} navigation={props.navigation} />
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <>
                    <Image source={_imageUrl} style={{ width: 76, height: 76, borderRadius: 40, marginRight: 15, marginLeft: 20, marginVertical: 10 }} />
                    <View>
                        <PowerTranslator
                            style={stylesInner.nameStyle}
                            enText={intakeForm.name}
                            hinText={intakeForm.name}
                        />
                        <Text style={stylesInner.name2Style}>{`${strings.gender} (${intakeForm.gender})`}</Text>
                        <Text style={stylesInner.name2Style}>{`${strings.age} (${intakeForm.age})`}</Text>
                    </View>
                </>
            </View>
        </View>
    }
    let overview = ifValid(data) && ifValid(data.patientId) ? data.patientId.overview : undefined
    if (ifNotValid(overview)) overview = "..."
    let pdfUploaded = ifValid(data.prescriptionId) ? data.prescriptionId.pdfUploaded : undefined;
    let imageURL = ifValid(data.prescriptionId) ? data.prescriptionId.imageURL : undefined;
    const timeHr = moment(data.utcDate).format("HH:MM").substr(0, 2)

    let appointmentDate = ifValid(data) && ifValid(data) ? data.appointmentDate : undefined;
    appointmentDate = moment(data.appointmentDate).format("DD MMM YYYY")
    if (ifNotValid(appointmentDate)) appointmentDate = "..."

    let startTime = ifValid(data) ? data.startTime : undefined;
    successLog('startTime' + startTime)
    startTime = moment(startTime, "HH:mm:ss").format("hh:mm A")
    if (ifNotValid(startTime)) startTime = "..."

    let endTime = ifValid(data) ? data.endTime : undefined;
    endTime = moment(endTime, "HH:mm:ss").format("hh:mm A")
    if (ifNotValid(endTime)) endTime = "..."

    let med_desc = data?.intakeInfo?.medicalDescription

    console.log('data?.intakeForm?.documentDetails ', data?.intakeInfo);
    let documentDetails =
        data?.intakeInfo?.documentDetails?.imageUrl?.length > 0
            ? data?.intakeInfo?.documentDetails?.imageUrl
            : []
    let all_Docs = documentDetails?.map((item, index) => {
        let obj;
        typeof item == "string"
            ? obj = {
                fileName: "document_" + (index + 1),
                location: item,
                type: item?.includes('.pdf') ? "application/pdf" : "image"
            }
            : obj = item
        return obj
    })

    const RenderDocuments = ({ docs }) => {

        const onPressDocument = (itm) => {
            if (itm?.type == "application/pdf") {
                props.navigation.navigate("PDFExample")
                dispatch(setPdfUrl(itm?.location))
            }
            if (itm?.type == "image/png" || itm?.type == "image/jpg" || itm?.type == "image/jpeg" || itm?.type == "image") {
                setDocumentData({ type: "image", url: itm?.location })
                dispatch(showMediaModal())
            }
        }

        return <View style={{ paddingTop: 10 }}>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image source={Images.watchRed} style={{ fontSize: FONT_SIZE_15 }} />
                <Text style={{ marginLeft: 5, marginBottom: 5 }}>{"Uploaded Documents"}</Text>
            </View>
            {
                docs?.map((itm) => {
                    return <TouchableOpacity onPress={() => onPressDocument(itm)} style={[stylesInner.pdfContainer, { marginTop: 10 }]}>
                        <Image source={Images.pin} style={{ width: 16, height: 16, }} />
                        <View style={{ marginLeft: 10 }}>
                            <Text style={stylesInner.bloodStyle}>{itm?.fileName}</Text>
                        </View>
                    </TouchableOpacity>
                })
            }
        </View>
    }

    const onPressDownloadImageprescriptions = async (images) => {
        console.log(images);

        if (isValid(images)) {
            dispatch(show())
            if (Platform.Version > 32) {
                downloadImages(images)
            } else {
                const typess = [PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE, PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]
                const permiss = await checkMultiple(typess)

                if (permiss["android.permission.READ_EXTERNAL_STORAGE"] === RESULTS.GRANTED && permiss["android.permission.WRITE_EXTERNAL_STORAGE"] === RESULTS.GRANTED) {
                    downloadImages(images)
                } else if (permiss["android.permission.READ_EXTERNAL_STORAGE"] === RESULTS.BLOCKED && permiss["android.permission.WRITE_EXTERNAL_STORAGE"] === RESULTS.BLOCKED) {
                    dispatch(hide())
                    showSettingsAlert()
                } else {
                    requestMultiple(typess)
                        .then(() => downloadImages(images))
                        .catch(err => {
                            console.log(err);
                            dispatch(hide())
                        })
                }
            }
        }
    }

    const showSettingsAlert = () => {
        Alert.alert(
            "Permission required!",
            "Please click open settings and allow storage permission under Permissions",
            [
                {
                    text: "Open Settings",
                    onPress: () => openSettings(),
                    style: "cancel",
                },
            ],
            {
                cancelable: true,
            }
        );
    }

    const downloadImages = (images) => {
        if (images?.length > 0) {
            const prms = [];

            images?.map(downUrl => {
                const promise = new Promise((resolve, reject) => {
                    const dirs = RNFetchBlob.fs.dirs.DownloadDir;
                    const localPath = dirs + `/${uuidv4()}.jpg`
                    RNFetchBlob
                        .config({
                            path: localPath,
                            addAndroidDownloads: {
                                useDownloadManager: true,
                                path: localPath,
                                notification: true,
                                mediaScannable: true,
                            }
                        })
                        .fetch('GET', downUrl)
                        .progress((received, total) => {
                            let percentage = (received / total) * 100
                            let roundPercentage = Math.round(percentage)
                            console.log('progress', roundPercentage)
                        })
                        .then((res) => {
                            resolve(res?.path())
                        })
                        .catch((err) => {
                            console.log('err  ', err)
                            reject(err)
                        })
                })
                prms.push(promise)
            })

            Promise.all(prms)
                .then(results => {
                    dispatch(hide())
                    const payload = {
                        text: `Prescription downloaded on your device`,
                        subtext: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    }
                    dispatch(showModal(payload))
                    console.log('results ', results);
                })
                .catch(err => {
                    console.log(err);
                    dispatch(hide())
                })
        }
    }

    return (
        <Container>
            {HeaderBookAppointment()}
            <ScrollView>
                <View style={{ padding: 20, flex: 1, backgroundColor: '#F9F9F9' }}>
                    {/* <View style={{ marginBottom: 20 }}>
                    <View style={{
                        // paddingHorizontal: 25,
                    }}>
                        <Text style={stylesInner.dateTimeStyle}>{appointmentDate},</Text>
                        <Text style={stylesInner.dateTimeStyle}>{startTime} - {endTime}</Text>
                    </View>
                    <Text style={stylesInner.bloodDateStyle}>Overview</Text>
                    <Text numberOfLines={3} style={[stylesInner.bloodStyle, { marginTop: 0 }]}>{overview}</Text>
                </View> */}

                    <View style={{ marginBottom: 40 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={Images.watchRed} style={{ fontSize: FONT_SIZE_15 }} />
                            <Text style={{ marginLeft: 5 }}>{strings.appointment_time}</Text>
                        </View>
                        <View style={{ marginLeft: 22 }}>
                            <Text style={stylesInner.subText}>
                                {
                                    timeHr < 12
                                        ? "Morning"
                                        : timeHr < 18
                                            ? "Afternoon"
                                            : "Evening"
                                }
                            </Text>
                            <Text
                                style={stylesInner.subText}>{moment(data.utcDate).format("dddd D MMM YYYY")}</Text>
                            <Text
                                style={stylesInner.time}>{startTime}  - {endTime} </Text>
                        </View>
                    </View>

                    {isValid(med_desc) && <View style={{ marginBottom: 40 }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Image source={Images.watchRed} style={{ fontSize: FONT_SIZE_15 }} />
                            <Text style={{ marginLeft: 5 }}>{strings.medical_description}</Text>
                        </View>
                        <View style={{ marginLeft: 22 }}>
                            <Text style={stylesInner.subText}>{med_desc}</Text>
                        </View>
                    </View>}

                    {isValid(pdfUploaded) ?
                        <>
                            <TouchableOpacity onPress={() => {
                                props.navigation.navigate("PDFExample")
                                dispatch(setPdfUrl(pdfUploaded))
                            }} style={stylesInner.pdfContainer}>
                                <Image source={Images.pin} style={{ width: 16, height: 16, }} />
                                <View style={{ marginLeft: 10 }}>
                                    <Text style={stylesInner.bloodStyle}>Pdf file</Text>
                                    <Text
                                        style={stylesInner.bloodDateStyle}>{moment(appointedDetails.createdAt).format("DD MMM YYYY")}</Text>
                                </View>
                            </TouchableOpacity>

                            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 32 }}>
                                <Text style={stylesInner.presStyle}>{strings.Prescription_Added}</Text>
                                <Image source={Images.pinRed} style={{ width: 16, height: 16, marginLeft: 4, }} />
                            </View>
                        </> :
                        isValid(imageURL)
                            ? <View>
                                <TouchableOpacity onPress={() => onPressDownloadImageprescriptions(imageURL)} style={stylesInner.pdfContainer}>
                                    <Image source={Images.pin} style={{ width: 16, height: 16, }} />
                                    <View style={{ marginLeft: 10 }}>
                                        <Text style={stylesInner.bloodStyle}>Download Prescription</Text>
                                        <Text
                                            style={stylesInner.bloodDateStyle}>{moment(appointedDetails.createdAt).format("DD MMM YYYY")}</Text>
                                    </View>
                                </TouchableOpacity>

                                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 32 }}>
                                    <Text style={stylesInner.presStyle}>{strings.Prescription_Added}</Text>
                                    <Image source={Images.pinRed} style={{ width: 16, height: 16, marginLeft: 4, }} />
                                </View>
                            </View>
                            : null}

                    {all_Docs?.length > 0 && <RenderDocuments docs={all_Docs} />}

                </View>
            </ScrollView>

            {ifNotValid(pdfUploaded) ?
                <Footer style={styles.footer}>
                    <Button disabled={ifValid(upcomingPast) && upcomingPast === 'UPCOMING'}
                        style={[styles.footerBtn, { backgroundColor: ifValid(upcomingPast) && upcomingPast === 'UPCOMING' ? '#b5b5b5' : '#FF5E38' }]}
                        onPress={() => {
                            dispatch(setSelectedPatient(data))
                            props.navigation.navigate("PrescriptionForm")
                        }}>
                        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                            <Text style={stylesInner.buttonTextStyle}>{strings.Add_Prescription}</Text>
                            <Image
                                source={Images.plusWhite}
                                style={stylesInner.buttonImage} />
                        </View>
                    </Button>
                </Footer>
                : null}

            <MediaPreview
                url={documentData?.url}
                type={documentData?.type}
                visible={modalVisible}
            />
        </Container>
    );
};
export default AppointedDetailsAdded;
const stylesInner = StyleSheet.create({
    pdfContainer: {
        padding: 10, alignItems: 'center',
        backgroundColor: 'rgb(229, 249, 247)', flexDirection: 'row'
    },
    footer: {
        width: '100%',
    },
    whiteImageBorderBig: {
        width: 90,
        height: 90,
        borderRadius: 45,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonTextStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
        color: '#FFFFFF',
        textAlign: 'center'
    },
    buttonImage: {
        width: 17.75,
        height: 17.75,
    },
    bloodStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
        color: '#041A32'
    },
    bloodDateStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_12,
        color: '#A0A9BE'
    },
    presStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_15,
        color: '#038BEF'
    },
    nameStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: '#041A32',
        fontSize: 20,
    },
    name2Style: {
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        color: '#041A32',
        fontSize: 16,
    },
    whiteImageBorderSmall: {
        marginLeft: '8%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    subText: {
        color: "#041A32",
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        fontSize: FONT_SIZE_15,
        paddingVertical: 8,
    }, time: {
        color: "#038BEF",
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        paddingVertical: 4,
        fontSize: FONT_SIZE_15,
    }, downloadText: {
        color: "#038BEF",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        paddingVertical: 4,
        fontSize: FONT_SIZE_15,
    }, dateTimeStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold,
        fontSize: 25,
        color: '#041A32'
    }
})
