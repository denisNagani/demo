

export const setUpcomingPastDetailView = (payload) => {
    return {
        type: 'UPCOMING_PAST_DETAIL_ACTIVE',
        payload: payload,
    };
};
