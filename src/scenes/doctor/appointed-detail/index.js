import React, { useEffect } from "react";
import { Image, StyleSheet, View } from "react-native";
import { Button, Container, Footer, Text } from "native-base";

import Images from "../../../assets/images";
import { useDispatch, useSelector } from "react-redux";
import { ifNotValid } from "../../../utils/ifNotValid";
import HeaderSehet from "../../../components/header-common";
import {
    FONT_FAMILY_SF_PRO_BOLD,
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_FAMILY_SF_PRO_SemiBold,
    FONT_SIZE_12,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_26
} from "../../../styles/typography";
import styles from "../../patient/appointment-detail/style";
import moment from 'moment'

const AppointedDetails = (props) => {
    const dispatch = useDispatch();
    const navigation = props.navigation;
    const doctorSlot = useSelector((state) => state.appointment.docSlot);
    let upcomingPast = useSelector((state) => state.upcomingPast.detailActive);
    const appointedPateint = props.navigation.state.params.data;
    console.log(appointedPateint.patientId.fullName);
    if (ifNotValid(upcomingPast)) {
        upcomingPast = "UPCOMING"
        console.log("details view type not provided..!")
    }

    useEffect(() => {

    }, []);


    const keyExtractor = (item, index) => index.toString();

    const HeaderBookAppointment = (doc) => {

        // let doc = {};
        if (ifNotValid(doc)) return <View />

        let fullName = doc.doctorId.fullName
        if (ifNotValid(fullName)) fullName = "Dr Jacob"

        let specialization = doc.doctorId.specialization
        if (ifNotValid(specialization)) specialization = "heart surgen"

        let experience = doc.doctorId.experience
        if (ifNotValid(experience)) experience = "..."


        let _imageUrl = doc.patientId.imageUrl;
        console.log("image url ", _imageUrl);
        if (ifNotValid(_imageUrl)) _imageUrl = Images.user
        else _imageUrl = { uri: _imageUrl }

        return <View style={{
            backgroundColor: '#038BEF',
            height: '35%'
        }}>
            <HeaderSehet hideshadow headerText={"Appointment Details"} navigation={props.navigation} />
            <View style={{
                paddingHorizontal: 25,
                shadowColor: '#0000000B',
                shadowOffset: { width: 0, height: 3 },
                shadowOpacity: 1,
                shadowRadius: 5,
                elevation: 5
            }}>
                <Text style={stylesInner.dateTimeStyle}>{moment(doc.utcDate).format("D MMM YYYY")}</Text>
                <Text
                    style={stylesInner.dateTimeStyle}>{doc.startTime} {moment(doc.utcDate).format("A")} - {doc.endTime} {moment(doc.utcDate).format("A")}</Text>

            </View>
            <View style={{
                flexDirection: 'row',
                position: 'absolute',
                left: 25,
                bottom: -45,
            }}>
                <View style={stylesInner.whiteImageBorderBig}>
                    <Image source={_imageUrl} style={{ width: 80, height: 80, borderRadius: 80 / 2 }} />
                </View>
                <View style={stylesInner.whiteImageBorderSmall}>
                    <Image source={Images.phone} style={{ width: 50, height: 50, borderRadius: 25 }} />
                </View>
            </View>
        </View >
    }


    return (
        <Container>
            {HeaderBookAppointment(appointedPateint)}
            <View style={{ marginHorizontal: 20, marginVertical: 40, justifyContent: 'space-around', flex: 1, }}>
                <Text style={stylesInner.nameStyle}>{appointedPateint.patientId.fullName}</Text>
                <Text style={stylesInner.bloodDateStyle}>Complete blood count</Text>
                <Text style={stylesInner.bloodStyle}>Complete blood count</Text>

                <View style={{
                    padding: 10, alignItems: 'center',
                    backgroundColor: 'rgb(229, 249, 247)', flexDirection: 'row'
                }}>
                    <Image source={Images.pin} style={{ width: 16, height: 16, }} />
                    <View style={{ marginLeft: 10 }}>
                        <Text style={stylesInner.bloodStyle}>Complete blood count</Text>
                        <Text style={stylesInner.bloodDateStyle}>6 march 2019</Text>
                    </View>
                </View>

                <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 32 }}>
                    <Text style={stylesInner.presStyle}>Prescription Added </Text>
                    <Image source={Images.pinRed} style={{ width: 16, height: 16, marginLeft: 4, }} />
                </View>
            </View>


            <Footer style={styles.footer}>
                <Button danger style={styles.footerBtn} onPress={() => {
                }}>
                    <Text style={stylesInner.buttonTextStyle}>Start Call</Text>
                </Button>

                <Button danger style={[styles.footerBtn, { flex: 1.1, marginLeft: 0, padding: 0 }]} onPress={() => {
                }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                        <Text style={stylesInner.buttonTextStyle}>Add Prescription </Text>
                        <Image source={Images.plusWhite}
                            style={stylesInner.buttonImage} />
                    </View>
                </Button>

            </Footer>


        </Container>
    );
};
export default AppointedDetails;
const stylesInner = StyleSheet.create({
    pdfContainer: {
        padding: 10, alignItems: 'center',
        backgroundColor: 'rgb(229, 249, 247)', flexDirection: 'row'
    },
    buttonImage: {
        width: 16,
        height: 16,
    },
    whiteImageBorderBig: {
        width: 90,
        height: 90,
        borderRadius: 45,
        backgroundColor: 'white',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonTextStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
        color: '#FFFFFF',
    },
    bloodStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
        color: '#041A32'
    },
    bloodDateStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_12,
        color: '#A0A9BE'
    },
    presStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_15,
        color: '#038BEF'
    },
    nameStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_BOLD,
        color: 'black',
        fontSize: FONT_SIZE_26,
    },
    whiteImageBorderSmall: {
        marginLeft: '8%',
        alignItems: 'center',
        justifyContent: 'center'
    },
    subText: {
        color: "#041A32",
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        fontSize: FONT_SIZE_15,
        paddingVertical: 6,
    }, time: {
        color: "#038BEF",
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        paddingVertical: 4,
        fontSize: FONT_SIZE_15,
    }, downloadText: {
        color: "#038BEF",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        paddingVertical: 4,
        fontSize: FONT_SIZE_15,
    }, dateTimeStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold, fontSize: 25, color: '#FFFFFF'
    }
})
