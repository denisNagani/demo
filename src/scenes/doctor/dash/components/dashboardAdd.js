import React, { useEffect } from "react";
import { TouchableOpacity, View, Dimensions, ScrollView, Image } from "react-native";
import styles from "./style";
import Images from "../../../../assets/images";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { useSelector, useDispatch } from "react-redux";
import { getBanners } from "../../../../actions/sehet/user-action";
import Carousel from "react-native-banner-carousel";


const DashboardAdd = ({ props }) => {
    const BannerWidth = Dimensions.get('window').width;
    const BannerHeight = 200;
    const bannerUrls = useSelector((state) => state.userReducer.bannerUrls);
    const images = bannerUrls.filter(function (el) {
        return el != null;
    })

    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getBanners())
    }, [])

    const renderBanners = (image, index) => {
        return (
            <View style={{ height: BannerHeight, width: BannerWidth, marginVertical: 10 }} key={index}>
                <View style={{ marginHorizontal: 25 }}>
                    <Image
                        resizeMode="stretch"
                        style={{ height: '100%', width: '100%' }}
                        source={{ uri: image }}
                    // defaultSource={Images.logo_sehet} 
                    />
                </View>
            </View>
        );
    }

    return <View style={styles.cardContainer}>
        <>
            <Carousel
                autoplay
                autoplayTimeout={3000}
                loop
                index={0}
                pageSize={BannerWidth}
                // pageIndicatorStyle={{ marginBottom: 15 }}
                showsPageIndicator={false}
            >

                {images.map((item, index) => renderBanners(item.imageUrl, index))}
            </Carousel>


            {/* <Image resizeMode="stretch" source={Images.doc_banner}
                style={[styles.card, { width: wp("88%"), borderRadius: 5 }]} /> */}

            {/* <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
            >
                <View style={{ flexDirection: 'row', marginTop: 25 }}>
                    {
                        images.map((item, index) => (
                            <Image
                                resizeMode="stretch"
                                source={{ uri: item.imageUrl }}
                                style={{
                                    borderRadius: 10,
                                    height: 200,
                                    width: BannerWidth - 100,
                                    marginLeft: 25
                                }}
                            />
                        ))
                    }

                </View>
            </ScrollView> */}
        </>
    </View>


}
export default DashboardAdd;
