import { SafeAreaView, Text, View, Image, TouchableOpacity, Platform, Share } from "react-native";
import styles from "./style";
import { Icon } from "native-base";
import common from "../../../../styles/common";
import React, { useState } from "react";
import Images from "../../../../assets/images";
import { appStoreAppLink, playStoreAppLink } from "../../../../config";

const getFirstName = (fullName) => {
    let full = fullName.split(" ");
    return full[0] + "! ";
};

const DashboardHeader = ({ props, userProfile }) => {

    const [appLinks, setAppLinks] = useState({
        iosLink: appStoreAppLink,
        androidLink: playStoreAppLink
    })

    const shareApp = async () => {
        let url = Platform.OS === "ios" ? appLinks.iosLink : appLinks.androidLink;
        // Linking.openURL(url)
        try {
            const result = await Share.share({
                title: 'App link',
                message: `Sehet App ${url}`,
                url: url
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                    console.log("activityType " + activityType);
                } else {
                    // shared
                    console.log("shared");
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
                console.log("dismissed");
            }
        } catch (error) {
            console.log(error.message);
        }
    }

    return <View style={styles.headerContainer}>
        <SafeAreaView style={styles.header}>
            <Icon
                name="menu"
                type="Feather"
                style={styles.sideDrawerIcon}
                onPress={() => props.navigation.openDrawer()}
            />
            <View style={common.headerBellAndText}>
                <View style={{ marginLeft: -10, flexDirection: 'row' }}>
                    <Image
                        style={{ height: 40 }}
                        source={Images.home_logo2}
                        resizeMode="contain"
                    />
                </View>
            </View>
            <View>
                <Icon
                    name="chat"
                    type="Entypo"
                    onPress={() => props.navigation.navigate('DoctorChat')}
                />
            </View>
            {/* <TouchableOpacity activeOpacity={1} onPress={shareApp}>
                <Image style={{
                    height: 25,
                    width: 25
                }}
                    source={Images.share_icon}

                />
            </TouchableOpacity> */}
        </SafeAreaView>
    </View>

}
export default DashboardHeader;
