import { StyleSheet, Dimensions } from "react-native";

import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_18 } from "../../../../styles/typography";
const windowWidth = Dimensions.get("window").width;

export default (styles = StyleSheet.create({

	cardContainer: {
		marginVertical: hp("1%"),
		justifyContent: "space-around",
		width: "100%",
		flexDirection: "row",
	},
	header: {
		flexDirection: "row",
		marginHorizontal: wp("4%"),
		alignItems: 'center'
	},
	sideDrawerIcon: {
		fontSize: hp("3.5%"),
		marginTop: hp("0.2%"),
		color: "#041A32",
	},
	headerContainer: {
		height: hp("12%"),
		backgroundColor: "#FFFFFF",
		justifyContent: "center",
		paddingHorizontal: wp("2%"),
		shadowColor: '#0000000B',
		shadowOffset: { width: 0, height: 3 },
		shadowOpacity: 1,
		shadowRadius: 5,
		elevation: 5
	},
	headerText: {
		color: "#041A32",
		fontSize: FONT_SIZE_18,
		fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
		alignSelf: "center",
		marginLeft: wp("5%"),
	},


	card: {
		height: hp("23%"),
		width: hp("25%"),
		justifyContent: "flex-end",
		alignItems: "center",
		marginVertical: hp("2%"),
		paddingBottom: hp("2%"),
	},
	cardInner1: {
		width: "90%",
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: wp("3%"),
		alignItems: "center",
		// borderColor: "red",
		// borderWidth: 1,
		// borderStyle: "solid",
	},
	cardInner2: {
		width: "90%",
		flexDirection: "row",
		justifyContent: "space-between",
		paddingHorizontal: wp("3%"),
		alignItems: "center",
		// borderColor: "red",
		// borderWidth: 1,
		// borderStyle: "solid",
	},
	cardText: {
		fontWeight: "bold",
		fontSize: wp("3.7%"),
		color: "#1E2081",
		fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,

	},
	cardIconColor: {
		color: "#1B80F3",
		fontSize: hp("3%"),
	},
	segment: {
		backgroundColor: "white",
	},

	segmentButton: {
		height: hp("6.5%"),
		width: windowWidth * 0.45,
		justifyContent: "center",
		marginTop: 20,
	},

	btnActive: {
		backgroundColor: "#1B80F3",
		fontWeight: "bold",
		color: "white",
		borderBottomLeftRadius: 10,
		borderTopLeftRadius: 10,
	},

	btnInactive: {
		backgroundColor: "#A0A9BE1A",
		borderBottomRightRadius: 10,
		borderTopRightRadius: 10,
	},

	segmentTextActive: {
		fontWeight: "bold",
		fontSize: hp("2%"),
		color: "#fff",
	},
	segmentTextInActive: {
		fontSize: hp("2%"),
		color: "black",
		fontWeight: "bold",
	},
	date: {
		marginTop: 40,
		marginLeft: windowWidth * 0.05,
		color: "#041A32",
		fontSize: 18,
		fontWeight: "bold",
	},
	patientView: {
		marginVertical: hp("1%"),
	},
	patientCard: {
		width: wp("90%"),
		borderRadius: 10,
	},
	list: {
		justifyContent: "center",
		alignSelf: "center",
		marginTop: 20,
		flex: 1,
		marginBottom: 5
	},
	patientCardText: {
		fontSize: 16,
		width: windowWidth * 0.9,
		marginLeft: wp("5%"),
	},
	patientCardSubText: {
		color: "#A0A9BE",
		marginTop: 8,
		width: windowWidth * 0.9,
		fontSize: 12,
		marginLeft: wp("3%"),
	},
	patientCardTextIcon: {
		fontSize: 13,
		color: "#A0A9BE",
	},
	intakeBtn: {
		backgroundColor: "#C70315",
		borderRadius: 7,
		padding: 5,
	},
	intakeBtnText: {
		color: "#fff",
		fontSize: 12,
	},
	patientCardTextTime: {
		marginTop: 6,
		color: "#A0A9BE",
		fontSize: 12,
	},
	myProfile: {
		marginRight: 30,
		marginTop: 25,
	},
	dateTab: {
		marginTop: "5%",
		flexDirection: "row",
		width: "90%",
		marginHorizontal: "5%",
	},
	prev: {
		flex: 2,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-start",
	},
	prevText: {
		color: "#1B80F3",
		fontSize: hp("2.5%"),
	},
	prevIcon: {
		color: "#1B80F3",
		marginRight: 5,
	},
	current: {
		flex: 6,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
	},
	currentText: {
		fontSize: hp("2.5%"),
		color: "#041A32",
	},
	next: {
		fontSize: 18,
		flex: 2,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-end",
	},
	nextIcon: {
		color: "#1B80F3",
		marginLeft: 5,
	},
	nextText: {
		fontSize: hp("2.5%"),
		color: "#1B80F3",
	},
}));
