import React, { Component } from "react";
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View, } from "react-native";
import { Button, Card, CardItem, Container, Icon, Left, Right, Segment, Thumbnail, } from "native-base";
import moment from "moment";

import styles from "./style";
import Images from "../../../../assets/images";
import { bindActionCreators } from "redux";
import { getUpcomingBookingsByDate, setCallHeader } from "../../../../actions/bookings";
import { connect } from "react-redux";
import { getRefToken } from "../../../../services/auth";
import { getRefreshTokenDash, saveLatLongOfDoctors } from "../../../../actions/profile";
import { hideAppointmentModal, showAppointmentModal, showModal } from "../../../../actions/modal";
import { emptySlot, getShareRoomSlots, postShareRoom } from "../../../../actions/share-room";
import { getSlotTimeZone } from "../../../../actions/appointment";
import { methodLog, recordError, successLog } from "../../../../utils/fireLog";
import DashboardHeader from "../components/dashboardHeader";
import DashboardAdd from "../components/dashboardAdd";
import AppStateComponent from "../../../../components/AppStateComponent";
import { ifNotValid, ifValid, isValid } from "../../../../utils/ifNotValid";
import stylesUpcoming from "../../upcoming-past-bookings/style";
import { strings, TranslationConfig } from "../../../../utils/translation";
import { getFAQ } from "../../../../actions/sehet/user-action";
import { getChatData } from "../../../../scenes/patient/Chat/action";
// import PowerTranslator from "react-native-power-translator";
import PowerTranslator from "../../../../components/PowerTranslator";
import { askForMyCurrentLocation } from "../../../../services/permission";
import { setUpcomingPastDetailView } from "../../../patient/appointment-detail/action";
import { FONT_FAMILY_SF_PRO_MEDIUM } from "../../../../styles/typography";

class DashboardScreen extends Component {
    constructor(props) {
        super(props);
        this.getToken();
        this.state = {
            currentDate: moment(new Date()).format("DD MMM YYYY"),
            name: "",
            email: "",
            mobile: "",
            dob: "",
            appointmentDate: "",
            gender: 'Select',
            slots: [],
            selectedSlot: '',
            isPickerVisibleDob: false,
            isPickerVisibleAppointment: false,
            visible: false,
            isFetching: false,
        };
    }

    componentDidMount() {
        successLog("DashboardScreenDoctor screen loaded")
        this.props.getSlotTimeZone()
        this.props.getFAQ()
        this.props.getChatData()
        TranslationConfig(this.props.lang)

        askForMyCurrentLocation((cords) => {
            this.props.saveLatLongOfDoctors(cords)
        })
    }

    componentWillUnmount() {
        this.resetState()
    }

    resetState = () => {
        this.setState({
            name: "",
            email: "",
            mobile: "",
            dob: "",
            appointmentDate: "",
            gender: 'Select',
            slots: [],
            selectedSlot: '',
            isPickerVisibleDob: false,
            isPickerVisibleAppointment: false,
            isFetching: false,

        })
    }

    getToken = async () => {
        let token = await getRefToken();
        console.log(token);
        this.props.getRefreshTokenDash(token);
    };

    nextDate = () => {
        methodLog("nextDate")
        try {
            const apiDate = moment(this.state.currentDate)
                .add(1, "day")
                .format("YYYY-MM-DD");
            console.log(apiDate);
            const date = moment(this.state.currentDate)
                .add(1, "day")
                .format("DD MMM YYYY");
            this.setState({
                currentDate: date,
            });
            this.props.getUpcomingBookingsByDate(apiDate);
        } catch (e) {
            recordError(e)
        }
    };

    prevDate = () => {
        methodLog("prevDate")
        try {
            const apiDate = moment(this.state.currentDate)
                .subtract(1, "day")
                .format("YYYY-MM-DD");
            console.log(apiDate);
            const date = moment(this.state.currentDate)
                .subtract(1, "day")
                .format("DD MMM YYYY");
            this.setState({
                currentDate: date,
            });
            this.props.getUpcomingBookingsByDate(apiDate);
        } catch (e) {
            recordError(e)
        }
    };

    onShareClick = async () => {
        this.props.showAppointmentModal(!this.props.appointmentModal)
    };

    handleConfirmDob = (date) => {
        methodLog("handleConfirmDob")
        try {
            const dob = moment(date).format("YYYY-MM-DD");
            this.setState({
                dob: "  ",
                isPickerVisibleDob: false,
            }, () => {
                this.setState({
                    dob,
                    isPickerVisibleDob: false,
                })
            });
        } catch (e) {
            recordError(e)
        }
    };

    setVisibility = () => {
        this.props.hideAppointmentModal()
    };


    onRefresh() {
        this.setState({ isFetching: true });
        this.props.getUpcomingBookingsByDate(moment(this.state.currentDate).format("YYYY-MM-DD"), () => {
            this.setState({ isFetching: false })
        })
    }


    renderItem = ({ item }) => {
        let patientId = ifNotValid(item.patientId) ? {} : item.patientId;
        let imageUrl = patientId.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = Images.user
        else imageUrl = { uri: imageUrl }

        let fullName = ifValid(item.patientId) ? ifValid(item.patientId.fullName) ? item.patientId.fullName : '-' : '-'

        let hindiFullName = patientId.hindiFullName
        if (ifNotValid(hindiFullName)) hindiFullName = fullName

        let name = isValid(item?.intakeInfo?.name) ? item?.intakeInfo?.name : patientId?.fullName
        let callType = item?.callType
        let appointmentType = callType == "Online" ? "Online" : "Clinic Visit"

        return (
            <TouchableOpacity style={styles.patientView} onPress={() => {
                this.props.setUpcomingPastDetailView('UPCOMING')
                this.props.navigation.navigate('AppointedDetailsAdded', { data: item })
            }}>
                <Card style={styles.patientCard}>
                    <CardItem style={styles.patientCard}>
                        <Left>
                            <Thumbnail source={imageUrl} />
                            <View style={{ flexDirection: "column" }}>
                                <View>
                                    {/* <Text style={styles.patientCardText}>
                                        {ifValid(fullName) && fullName.length > 19 ?
                                            ((fullName.substring(0, 19 - 3)) + '...') : fullName}
                                    </Text> */}
                                    <PowerTranslator
                                        style={styles.patientCardText}
                                        enText={name}
                                        hinText={name}
                                    />
                                </View>
                                <View>
                                    <Text style={styles.patientCardSubText}>
                                        <Icon type="AntDesign" name="calendar" style={styles.patientCardTextIcon} />{" "}
                                        {item.utcDate &&
                                            moment(item.utcDate).format("DD:MM:YYYY")}
                                    </Text>
                                </View>
                            </View>
                        </Left>
                        <Right>
                            {/*<TouchableOpacity*/}
                            {/*    style={common.intakeBtn}*/}
                            {/*    onPress={() =>*/}
                            {/*        Linking.openURL(`${intake_url}pdf/${item.patientId._id}.pdf`)*/}
                            {/*    }>*/}
                            {/*    <Text style={common.intakeBtnText}>Intake Form</Text>*/}
                            {/*</TouchableOpacity>*/}
                            <View style={{
                                backgroundColor: '#048BEF',
                                paddingHorizontal: 8,
                                paddingBottom: 5,
                                borderRadius: 5,
                                marginRight: -5,
                                marginBottom: 5
                            }}>
                                <Text style={[styles.patientCardTextTime, { color: '#FFF' }]}>
                                    {appointmentType}
                                </Text>
                            </View>
                            <Text style={styles.patientCardTextTime}>
                                <Icon name="clock" type={"Feather"} style={styles.patientCardTextIcon} />{" "}
                                {item.utcDate && moment(item.utcDate).format("hh:mm A")}
                            </Text>
                            <TouchableOpacity
                                style={innerStyle.hostoryBtnCont}
                                onPress={() => {
                                    this.props.navigation.navigate('AppointmentHistory', {
                                        data: item
                                    })
                                }}>
                                <Text style={innerStyle.hostoryBtnText}>Show History</Text>
                            </TouchableOpacity>
                        </Right>
                    </CardItem>
                </Card>
            </TouchableOpacity>
        );
    };


    noBookings = () => {
        return (
            <View style={stylesUpcoming.noBookingsBlock}>
                <Image source={Images.no_bookings} style={[stylesUpcoming.noBookingsImage, { width: 100, height: 100 }]} />
                <Text style={stylesUpcoming.noBookingsTitle}>{strings.No_appointments}</Text>
                <Text style={stylesUpcoming.noBookingsSubTitle}>
                    {strings.No_appointments2}
                </Text>
            </View>
        );
    };


    render() {

        let userProfile = this.props.userProfile
        return (
            <Container style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
                <AppStateComponent />
                <DashboardHeader
                    props={this.props}
                    userProfile={userProfile}
                />

                <DashboardAdd
                    props={this.props} />

                <View style={{ backgroundColor: '#F9F9F9' }}>
                    <Segment style={styles.segment}>
                        <Button first active style={[styles.segmentButton, styles.btnActive]}>
                            <Text style={styles.segmentTextActive}>{strings.scheduled}</Text>
                        </Button>
                        <Button onPress={() => this.props.navigation.navigate("DashboardWaitingScreen")}
                            last
                            style={[styles.segmentButton, styles.btnInactive]}>
                            <Text style={styles.segmentTextInActive}>{strings.waiting}</Text>
                        </Button>
                    </Segment>
                </View>
                <View style={styles.dateTab}>
                    <TouchableOpacity style={styles.prev} onPress={this.prevDate}>
                        <Icon style={styles.prevIcon} name="arrowleft" type="AntDesign" />
                        <Text style={styles.prevText}>Prev</Text>
                    </TouchableOpacity>
                    <View style={styles.current}>
                        <Text style={styles.currentText}>
                            {this.state.currentDate.toString()}
                        </Text>
                    </View>
                    <TouchableOpacity style={styles.next} onPress={this.nextDate}>
                        <Text style={styles.nextText}>Next</Text>
                        <Icon style={styles.nextIcon} name="arrowright" type="AntDesign" />
                    </TouchableOpacity>
                </View>

                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.props.upcomingBookingsByDate}
                    refreshing={this.state.isFetching}
                    style={{ flex: 1, }}
                    ListEmptyComponent={() => this.noBookings()}
                    onRefresh={() => this.onRefresh()}
                    renderItem={this.renderItem}
                    keyExtractor={(item) => item.id}
                />

            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            setCallHeader,
            getUpcomingBookingsByDate,
            getSlotTimeZone,
            getRefreshTokenDash,
            showModal,
            postShareRoom,
            getShareRoomSlots,
            emptySlot,
            hideAppointmentModal,
            showAppointmentModal,
            getFAQ,
            getChatData,
            askForMyCurrentLocation,
            saveLatLongOfDoctors,
            setUpcomingPastDetailView
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    upcomingBookingsByDate: state.bookings.upcomingBookingsByDate,
    userProfile: state.profile.userProfile,
    slots: state.shareRoom.slots,
    slotTimeZone: state.appointment.slotTimeZone,
    appointmentModal: state.modal.appointmentModal,
    lang: state.profile.selected_language
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardScreen);

export const innerStyle = StyleSheet.create({
    hostoryBtnCont: {
        marginTop: 3,
        borderRadius: 8,
    },
    hostoryBtnText: {
        paddingVertical: 3,
        color: '#038BEF',
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: 14,
    },
})