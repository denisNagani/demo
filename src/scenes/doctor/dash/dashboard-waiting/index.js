import React, { Component } from "react";
import { Alert, BackHandler, FlatList, Image, TouchableOpacity, View, } from "react-native";
import { Button, Card, CardItem, Container, Left, Right, Segment, Text, Thumbnail, } from "native-base";

import styles from "./style";
import stylesUpcoming from "../../upcoming-past-bookings/style";
import Images from "../../../../assets/images";
import { bindActionCreators } from "redux";
import {
    checkRoomUrlExist,
    getUpcomingBookingsByDate,
    getWaitingListDoctor,
    setCallHeader,
    setSelectedAppointment,
    updateAppointment,
    updateAppointmentDirect,
} from "../../../../actions/bookings";
import { connect } from "react-redux";
import database from "@react-native-firebase/database";
import { getRefreshTokenDash } from "../../../../actions/profile";
import { addUserLogToFirebase, checkUserProfile } from "../../../../utils/helper";
import moment from "moment";
import { hideAppointmentModal, showAppointmentModal, showModal } from "../../../../actions/modal";
import common from "../../../../styles/common";
import { getSlotTimeZone, setJoinCallData } from "../../../../actions/appointment";
import { emptySlot, getShareRoomSlots, postShareRoom } from "../../../../actions/share-room";
import { methodLog, recordError, successLog } from "../../../../utils/fireLog";
import DashboardHeader from "../components/dashboardHeader";
import DashboardAdd from "../components/dashboardAdd";
import { ifNotValid, ifValid, isValid } from "../../../../utils/ifNotValid";
import AddPrescriptionDialog from "../../add-prescriptionDialog";
import { setSelectedPatient } from "../../perscription-form/components/action";
import { setAppointmentIdForAddMedicine, setAppointmentObject } from "../../../patient/appointment-detail/action";
import { sendNotification } from "../../../../actions/notification";
import AsyncStorage from "@react-native-community/async-storage";
import { IO_EVENTS, USER_LOGOUT } from "../../../../utils/constant";
import { strings, TranslationConfig } from "../../../../utils/translation";
// import PowerTranslator from "react-native-power-translator";
import PowerTranslator from "../../../../components/PowerTranslator";
import { getIncomingCallScreen, setIncomingCallScreen } from "../../../../services/auth";
import { getUserDetails } from "../../../../actions/profile";
import { hide, show } from "../../../../utils/loader/action";
import NavigationService from '../../../../components/startScreen/RootNavigation'
import { socket } from "../../../../config";

class DashboardWaitingScreen extends Component {
    state = {
        modelVisible: false,
        roomName: "",
        data: [],
        currentDate: moment(new Date()).format("DD MMM YYYY"),
        name: "",
        email: "",
        mobile: "",
        dob: "",
        appointmentDate: "",
        gender: 'Select',
        slots: [],
        selectedSlot: '',
        isPickerVisibleDob: false,
        isPickerVisibleAppointment: false,
        visible: false,
        isFetching: false,
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        successLog("DashboardWaitingScreen screen loaded")
        this.checkAndGetData()
        this.props.getWaitingListDoctor();
        TranslationConfig(this.props.lang)

        this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
            if (this.props.joinCallDoctorData?.setIncomingCallScreen === true) {
                BackHandler.exitApp()
            } else {
                this.props.navigation.goBack();
            }
            return true;
        });

        socket().on(IO_EVENTS.STATUS_UPDATED, (data) => {
            if (data == this.props.userProfile?.userId?._id) {
                this.props.getWaitingListDoctor();
            }
        })
    }

    async checkAndGetData() {
        const flag = await getIncomingCallScreen()
        if (flag == "true") {
            this.props.show()
            setIncomingCallScreen("false")
            this.props.getUserDetails()
            // if (isValid(this.props.joinCallDoctorData)) {
            //     this.approveRejectCall(this.props.joinCallDoctorData, "APPROVED")
            // }
        }
    }

    componentWillUnmount() {
        this.resetState()
        this.backHandler.remove();
        // this.backHandler.remove();

        socket().off(IO_EVENTS.STATUS_UPDATED)
    }

    onShareClick = async () => {
        this.props.showAppointmentModal(!this.props.appointmentModal)
    };

    resetState = () => {
        methodLog("resetState")
        this.setState({
            name: "",
            email: "",
            mobile: "",
            dob: "",
            appointmentDate: "",
            gender: 'Select',
            slots: [],
            selectedSlot: '',
            isPickerVisibleDob: false,
            isPickerVisibleAppointment: false,
            isFetching: false,
            visible: false
        })
    }


    handleConfirmDob = (date) => {
        methodLog("handleConfirmDob")
        try {
            const dob = moment(date).format("YYYY-MM-DD");
            this.setState({
                dob: "  ",
                isPickerVisibleDob: false,
            }, () => {
                this.setState({
                    dob,
                    isPickerVisibleDob: false,
                })
            });
        } catch (e) {
            recordError(e)
        }
    };

    setVisibility = () => {
        this.props.hideAppointmentModal()
    };

    approveRejectCall = (item, status) => {
        methodLog("approveRejectCall")
        console.log(JSON.stringify(item));
        this.props.setSelectedPatient(item)
        let pfullName = ifValid(item.intakeInfo) ? item.intakeInfo.name : "-"

        try {
            this.props.setCallHeader(pfullName);
            this.props.setSelectedAppointment(item._id);
            let obj = { [item._id]: { status: status } };
            const newReference = database()
                .ref("/doctor")
                .update(obj)
                .then(async (obj) => {
                    if (status !== "REJECTED") {
                        //adding user
                        let fullName = isValid(item?.doctorId?.fullName) ? item?.doctorId?.fullName : "--"
                        let image = isValid(item?.doctorId?.imageUrl) ? item?.doctorId?.imageUrl : "--"

                        await addUserLogToFirebase(item._id, { event: status, name: `${fullName} (Doctor)` }).then(r => {
                        })

                        /// adding first user in list.
                        const users = database()
                            .ref(`/users/${[item._id]}`)
                            .update({ doctor: { fullName: `${fullName}`, image: image } })
                            .then((obj) => {
                                // this.props.navigation.navigate("JoinCall", {
                                //     channel: item.doctorId.roomName,
                                //     user: "DOCTOR",
                                //     id: item._id,
                                //     directLogin: false
                                // });

                                this.props.setJoinCallData({
                                    userData: {
                                        fullName: this.props.userProfile?.userId?.fullName,
                                        image: this.props.userProfile?.userId?.image
                                    },
                                    joinCallData: {
                                        channel: item?.doctorId?.roomName,
                                        user: "DOCTOR",
                                        id: item?._id,
                                        directLogin: false
                                    },
                                    appointmentData: item,
                                })
                                this.props.navigation.navigate("JoinCall");
                            });
                    }
                });

            if (status === "REJECTED") {
                if (item.directLogin && item.directLogin == "true")
                    this.props.updateAppointmentDirect(
                        item._id,
                        this.props.navigation,
                        "REJECTED"
                    );
                else
                    this.props.updateAppointment(
                        item._id,
                        this.props.navigation,
                        "REJECTED"
                    );
            }
        } catch (e) {
            recordError(e)
        }
    };

    renderItem = ({ item }) => {
        let patientId = ifNotValid(item.patientId) ? {} : item.patientId;
        let imageUrl = patientId.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = Images.user
        else imageUrl = { uri: imageUrl }

        let fullName = patientId.fullName
        if (ifNotValid(fullName)) fullName = '-'

        let hindiFullName = patientId.hindiFullName
        if (ifNotValid(hindiFullName)) hindiFullName = fullName

        let name = isValid(item?.intakeInfo?.name) ? item?.intakeInfo?.name : patientId?.fullName

        return (
            <Card style={styles.patientCard}>
                <CardItem style={styles.patientCard}>
                    <Left>
                        <Thumbnail source={imageUrl} />
                        <View>
                            <View>
                                {/* <Text style={styles.patientCardText}>
                                    {ifValid(item.patientId) ? ifValid(item.patientId.fullName) ? item.patientId.fullName : "-" : '-'}
                                </Text> */}
                                <PowerTranslator
                                    style={styles.patientCardText}
                                    hinText={name}
                                    enText={name}

                                />
                            </View>
                            <View sytle={{ flexDirection: "row" }}>
                                <Text style={styles.patientCardText} note>
                                    {item.directLogin && item.directLogin == "true"
                                        ? moment(item.createdAt).format("hh:mm A")
                                        : item.utcDate &&
                                        moment(item.utcDate).format("hh:mm A")}
                                </Text>
                            </View>
                        </View>
                    </Left>
                    <Right>
                        {item.directLogin && item.directLogin == "true" ? (
                            <TouchableOpacity
                                style={[
                                    styles.intakeBtn,
                                    { backgroundColor: "purple" },
                                ]}
                            >
                                <Text style={styles.intakeBtnText}>Direct login user</Text>
                            </TouchableOpacity>
                        ) : (
                            // <TouchableOpacity
                            //     style={[styles.intakeBtn, {}]}
                            //     onPress={() =>
                            //         Linking.openURL(
                            //             `${intake_url}pdf/${item.patientId._id}.pdf`
                            //         )
                            //     }
                            // >
                            //     <Text style={styles.intakeBtnText}>Intake Form</Text>
                            // </TouchableOpacity>
                            <Text />
                        )}
                    </Right>
                </CardItem>
                <View style={{ flexDirection: "row", marginHorizontal: 20, marginBottom: 10 }}>

                    <TouchableOpacity
                        style={[common.intakeBtn, { flex: 1, backgroundColor: "#F9F9F9", borderColor: '#A0A9BE', borderWidth: 1 }]}
                        onPress={() => {
                            Alert.alert("Reject", "Do you really want to reject the call ?", [
                                { text: "Cancel", onPress: () => { } },
                                {
                                    text: "OK", onPress: () => {
                                        const body = {
                                            doctorId: patientId,
                                            type: 'REJECT',
                                            appointmentId: item?._id
                                        }
                                        this.props.sendNotification(body)
                                        this.approveRejectCall(item, "REJECTED");
                                    }
                                },
                            ]);

                        }}
                    >
                        <Text style={[common.intakeBtnText, { color: '#A0A9BE' }]}>{strings.Reject}</Text>
                    </TouchableOpacity>


                    <TouchableOpacity
                        style={[common.intakeBtn, { backgroundColor: "#FF5E38", flex: 1, borderColor: '#C70315', borderWidth: 1 }]}
                        onPress={() => {
                            const body = {
                                doctorId: patientId,
                                type: 'ACCEPT',
                                appointmentId: item?._id
                            }
                            this.props.sendNotification(body)
                            this.props.setAppointmentIdForAddMedicine(item._id);
                            this.props.setAppointmentObject(item);

                            if (
                                item.doctorId.roomName === undefined ||
                                item.doctorId.roomName == null
                            ) {
                                this.setState({
                                    modelVisible: !this.state.modelVisible,
                                });
                            } else {
                                this.approveRejectCall(item, "APPROVED");
                            }
                        }}
                    >
                        <Text style={[common.intakeBtnText, { color: '#FFFFFF', textAlign: 'center' }]}>{strings.Approve}</Text>
                    </TouchableOpacity>

                </View>

            </Card>
        );
    };


    onRefresh() {
        this.setState({ isFetching: true });
        this.props.getWaitingListDoctor(() => {
            this.setState({ isFetching: false })
        });
    }


    noBookings = () => {
        return (
            <View style={stylesUpcoming.noBookingsBlock}>
                <Image source={Images.no_bookings} style={[stylesUpcoming.noBookingsImage, { width: 100, height: 100 }]} />
                <Text style={stylesUpcoming.noBookingsTitle}>{strings.No_appointments}</Text>
                <Text style={stylesUpcoming.noBookingsSubTitle}>
                    {strings.No_appointments2}
                </Text>
            </View>
        );
    };

    render() {
        let userProfile = this.props.userProfile;
        return (
            <Container style={{ backgroundColor: '#F9F9F9' }}>
                <DashboardHeader
                    props={this.props}
                    userProfile={userProfile}
                />

                <DashboardAdd props={this.props} />

                <View>
                    <Segment style={styles.segment}>
                        <Button
                            onPress={() => {
                                this.props.getUpcomingBookingsByDate(moment(new Date()).format("YYYY-MM-DD"))
                                this.props.navigation.navigate("DashboardScreen")
                            }}
                            first
                            active
                            style={[styles.segmentButton, styles.btnInactive]}
                        >
                            <Text style={styles.segmentTextActive} uppercase={false}>
                                {strings.scheduled}
                            </Text>
                        </Button>
                        <Button last style={[styles.segmentButton, styles.btnActive]}>
                            <Text style={styles.segmentTextInActive} uppercase={false}>
                                {strings.waiting}
                            </Text>
                        </Button>
                    </Segment>
                </View>

                <View style={styles.dateTab} />


                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={this.props.docWaitingList}
                    ListEmptyComponent={() => this.noBookings()}
                    style={{ flex: 1, marginVertical: 20, }}
                    refreshing={this.state.isFetching}
                    onRefresh={() => this.onRefresh()}
                    renderItem={this.renderItem}
                    keyExtractor={(item) => item.id}
                />


                <View>
                    <AddPrescriptionDialog props={this.props} />
                </View>

            </Container>
        )
            ;
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getWaitingListDoctor,
            updateAppointment,
            checkRoomUrlExist,
            setSelectedAppointment,
            getRefreshTokenDash,
            updateAppointmentDirect,
            setCallHeader,
            showModal,
            getUpcomingBookingsByDate, setAppointmentObject,
            getSlotTimeZone, postShareRoom, getShareRoomSlots, emptySlot,
            hideAppointmentModal, setSelectedPatient,
            showAppointmentModal, setAppointmentIdForAddMedicine, sendNotification, getUserDetails,
            show, hide, setJoinCallData
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    docWaitingList: state.bookings.docWaitingList,
    userProfile: state.profile.userProfile,
    slots: state.shareRoom.slots,
    slotTimeZone: state.appointment.slotTimeZone,
    joinCallDoctorData: state.appointment.joinCallDoctorData,
    appointmentModal: state.modal.appointmentModal,
    lang: state.profile.selected_language
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DashboardWaitingScreen);
