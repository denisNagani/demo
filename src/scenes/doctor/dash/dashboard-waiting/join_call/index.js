/* eslint-disable prettier/prettier */
import React, { Component } from "react";
import { Alert, BackHandler, NativeModules, View, } from "react-native";
import { RtcEngine } from "react-native-agora";
import database from "@react-native-firebase/database";
import { bindActionCreators } from "redux";
import { completeAppointment, setPatientIsOnWaiting, updateAppointmentDirect, } from "../../../../../actions/bookings";
import { connect } from "react-redux";
import { requestCameraAndAudioPermission } from "../../../../../services/permission";
import RNBeep from "react-native-a-beep";
import { addUserLogToFirebase, checkUserProfile, handleBackPress } from "../../../../../utils/helper";
import VideoCommon from "../../../../../components/video-common";
import { getRoomURLForGuest } from "../../../../../actions/share-room";
import { agora_id } from "../../../../../config";
import { DISCONNECTED } from "../../../../../utils/constant";
import { postAttendeesLogs } from "../../../../../actions/firebaseLogs";
import { successLog } from "../../../../../utils/fireLog";
import { setPrescriptionModel } from "../../../../../actions/presModel";
import { setJoinCallData, startRecording, stopRecording } from "../../../../../actions/appointment";
import { ifValid, isValid } from "../../../../../utils/ifNotValid";
import NavigationService from '../../../../../components/startScreen/RootNavigation'

const { Agora } = NativeModules; //Define Agora object as a native module

const { FPS30, AudioProfileDefault, AudioScenarioDefault, Adaptative } = Agora; //Set defaults for Stream

const config = {
    //Setting config of the app
    appid: agora_id, //Enter the App ID generated from the Agora Website
    channelProfile: 0, //Set channel profile as 0 for RTC
    videoEncoderConfig: {
        //Set Video feed encoder settings
        width: 720,
        height: 1080,
        bitrate: 1,
        frameRate: FPS30,
        orientationMode: Adaptative,
    },
    audioProfile: AudioProfileDefault,
    audioScenario: AudioScenarioDefault,
};

class JoinCall extends Component {
    constructor(props) {
        super(props);
        this.state = {
            peerIds: [], //Array for storing connected peers
            uid: Math.floor(Math.random() * 100), //Generate a UID for local user
            appid: config.appid,
            channelName: "", //Channel Name for the current session
            joinSucceed: false, //State variable for storing success
            audMute: false,
            vidMute: false,
            showList: false,
            userList: [],
            minutes: 0,
            seconds: 0,
            usersListFirebase: [],
            fullScreen: false,
            peerIdSelected: null,
            logSent: false
        };
        this.state.channelName = this.props.joinCallDoctorData?.joinCallData?.channel;
        this.state.id = this.props.joinCallDoctorData?.joinCallData?.id;
        this.state.user = this.props.joinCallDoctorData?.joinCallData?.user;
        this.state.directLogin = this.props.joinCallDoctorData?.joinCallData?.directLogin;

        requestCameraAndAudioPermission().then((_) => {
            console.log("requested!");
            this.startCall();
        });
    }

    componentWillUnmount() {
        successLog("JoinCall screen loaded")
        this.props.setPatientIsOnWaiting(false)
        this.props.setJoinCallData(null)
        let id = this.state.id;
        // let { fullName, image } = checkUserProfile(this.props.userProfile);
        let { fullName, image } = this.props.joinCallDoctorData?.userData;
        addUserLogToFirebase(id, { event: DISCONNECTED, name: `${fullName} (Doctor)` }).then(r => {
            this.makeFinalLogEntry().then(r => {
                this.backHandler.remove();
                clearInterval(this.interval);
                this.endCall();
                database().ref(`/doctor/${this.state.id}`)
                    .off("value");

                database().ref(`/logs/${this.state.id}`)
                    .off("value");

            })

        })

    }

    tick = () => {
        this.setState({
            counter: this.state.counter + 1,
        });
    };

    checkCallStatus = () => {
        database()
            .ref(`/doctor/${this.state.id}`)
            .on("value", (snapshot) => {
                if (snapshot.val() !== null && snapshot.val().status === "FINISHED") {
                    //_failureToastWithoutOk("Appointment is ended by the doctor!!");
                    this.endCall();
                }
                console.log("User data: ", snapshot.val());
            });
    };

    getUsersList = () => {
        let users = [];
        const result = database()
            .ref(`/users/${this.props.selectedAppointment}/`)
            .on("value", (snapshot) => {
                if (snapshot.val() !== null) {
                    let data = snapshot.val();
                    users = Object.values(data);
                    this.setState({
                        usersListFirebase: users,
                    });
                }
            });
    };

    componentDidMount() {
        this.props.setPatientIsOnWaiting(true)
        this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
            Alert.alert(
                "",
                "Are you sure you want to disconnect this call",
                [
                    {
                        text: "No",
                        onPress: () => {
                            console.log("Cancel Pressed");
                        },
                        style: "cancel"
                    },
                    {
                        text: "Yes", onPress: () => {
                            if (this.props.joinCallDoctorData?.setIncomingCallScreen === true) {
                                NavigationService.navigate('DashboardWaitingScreen')
                            } else {
                                this.props.navigation.goBack();
                            }
                        }
                    }
                ],
                { cancelable: false }
            );
            return true;
        });

        this.checkCallStatus();
        this.getUsersList();
        this.interval = setInterval(() => {
            const { seconds, minutes } = this.state;
            if (seconds < 60) {
                this.setState(({ seconds }) => ({
                    seconds: seconds + 1,
                }));
            } else {
                this.setState(({ minutes }) => ({
                    minutes: minutes + 1,
                    seconds: 0,
                }));
            }
        }, 1000);

        RtcEngine.on("userJoined", (data) => {
            const { peerIds } = this.state; //Get currrent peer IDs
            if (peerIds.indexOf(data.uid) === -1) {
                RNBeep.beep(false);
                //If new user has joined
                this.setState({
                    peerIds: [...peerIds, data.uid], //add peer ID to state array
                });
            }
        });
        RtcEngine.on("userOffline", (data) => {
            //If user leaves
            RNBeep.beep(false);
            this.setState({
                peerIds: this.state.peerIds.filter((uid) => uid !== data.uid), //remove peer ID from state array
            });
        });
        RtcEngine.on("joinChannelSuccess", (data) => {
            //If Local user joins RTC channel
            RtcEngine.startPreview(); //Start RTC preview
            RNBeep.beep(false);
            this.setState({
                joinSucceed: true, //Set state variable to true
            });
        });
        RtcEngine.init(config); //Initialize the RTC engine
    }

    startCall = () => {
        RNBeep.beep(false);
        this.props.startRecording(this.state.channelName, this.state.uid, this.state.id);
        console.log("INSTARRT " + this.state.uid + "   " + this.state.channelName);
        // const channelUID = isValid(this.props.userProfile?.index) ? this.props.userProfile?.index : this.state.uid;
        const channelUID = this.state.uid;
        RtcEngine.joinChannel(this.state.channelName, channelUID); //Join Channel
        RtcEngine.enableAudio(); //Enable the audio
    };

    makeFinalLogEntry = async () => {
        let logs = [];
        return await database().ref(`/logs/${this.props.selectedAppointment}/`)
            .once("value").then(snapshot => {
                if (snapshot.val() !== null) {
                    let data = snapshot.val();
                    logs = Object.values(data);
                    this.props.postAttendeesLogs(this.props.selectedAppointment, logs)
                    this.state.logSent = true;
                }
            });
    }


    endCall = () => {
        try {

            if (ifValid(this.props.startRecordingResponse) && this.props.startRecordingResponse.stopped === false) {
                let body = {
                    cname: this.state.channelName,
                    uid: this.state.uid.toString(),
                    resourceId: this.props.startRecordingResponse.resourceId,
                    sid: this.props.startRecordingResponse.sid,
                    appointmentId: this.state.id,
                    clientRequest: {},
                };
                this.props.stopRecording(body);
            } else {
                console.log("startRecording response empty..!")
            }
            clearInterval(this.interval);
            this.removeFromList();
            RtcEngine.leaveChannel();
            this.setState({
                peerIds: [],
                joinSucceed: false,
            });

            let user = this.state.user;
            let id = this.state.id;
            let directLogin = this.state.directLogin;
            if (user && user === "PATIENT") this.props.navigation.pop(1);
            else {
                let obj = { [id]: { status: "FINISHED" } };
                const newReference = database()
                    .ref("/doctor")
                    .update(obj)
                    .then((obj) => {
                        if (this.props.joinCallDoctorData?.setIncomingCallScreen === true) {
                            NavigationService.navigate('DashboardWaitingScreen')
                        } else {
                            this.props.setPrescriptionModel(true)
                            this.props.navigation.goBack();
                        }
                    });

                if (directLogin === true) {
                    this.props.updateAppointmentDirect(
                        id,
                        this.props.navigation,
                        "COMPLETED"
                    );
                } else {
                    // this.props.completeAppointment(id, this.props.navigation, "COMPLETED");
                }
            }
        } catch (e) {
            console.log(JSON.stringify(e))
        }
    };

    removeFromList = async () => {

        await database()
            .ref(`/users/${this.props.selectedAppointment}/doctor`)
            .remove();
    };

    toggleVideo = () => {
        let mute = this.state.vidMute;
        RtcEngine.muteLocalVideoStream(!mute);
        console.log("Video toggle", mute);
        this.setState({
            vidMute: !mute,
        });
    };

    toggleAudio = () => {
        let mute = this.state.audMute;
        console.log("Audio toggle", mute);
        RtcEngine.muteLocalAudioStream(!mute);
        this.setState({
            audMute: !mute,
        });
    };


    makeFullScreen = (peerId) => {
        let fullScreen = this.state.fullScreen;
        this.setState({
            fullScreen: !fullScreen, peerIdSelected: peerId
        });
    };

    switchCamera = () => {
        RtcEngine.switchCamera()
            .then(() => {
                console.log(" switchCamera called");
            }).catch((err) => console.log(" switchCamera called err " + err))
    }

    videoView() {
        return (
            <View style={{ flex: 1 }}>
                <VideoCommon
                    selectedHeader={this.props.selectedHeader}
                    showList={this.state.showList}
                    peerIds={this.state.peerIds}
                    audMute={this.state.audMute}
                    vidMute={this.state.vidMute}
                    joinSucceed={this.state.joinSucceed}
                    minutes={this.state.minutes}
                    seconds={this.state.seconds}
                    usersListFirebase={this.state.usersListFirebase}
                    onPressShowList={() => {
                        this.setState({
                            showList: !this.state.showList
                        })
                    }}
                    endCall={() => this.endCall()}
                    toggleAudio={() => this.toggleAudio()}
                    toggleVideo={() => this.toggleVideo()}
                    switchCamera={() => this.switchCamera()}
                    fullScreen={this.state.fullScreen}
                    peerIdSelected={this.state.peerIdSelected}
                    makeFullScreen={(id) => this.makeFullScreen(id)}
                // copyUrl={()=> {
                // 	this.props.getRoomURLForGuest(this.state.id)
                // }}
                />
            </View>
        );
    }

    render() {
        return this.videoView();
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            postAttendeesLogs,
            completeAppointment, startRecording,
            stopRecording,
            updateAppointmentDirect,
            getRoomURLForGuest, setPrescriptionModel, setPatientIsOnWaiting, setJoinCallData
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    // docWaitingList: state.bookings.docWaitingList,
    userProfile: state.profile.userProfile,
    selectedAppointment: state.bookings.selectedAppointment,
    selectedHeader: state.bookings.selectedHeader,
    startRecordingResponse: state.appointment.startRecording,
    joinCallDoctorData: state.appointment.joinCallDoctorData,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(JoinCall);
