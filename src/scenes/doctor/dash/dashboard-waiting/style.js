import { Dimensions, StyleSheet } from "react-native";

import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_18 } from "../../../../styles/typography";

const windowWidth = Dimensions.get("window").width;

export default (styles = StyleSheet.create({

    card: {
        height: hp("23%"),
        width: hp("25%"),
        justifyContent: "flex-end",
        alignItems: "center",
        marginVertical: hp("2%"),
        paddingBottom: hp("2%"),
    },
    cardInner1: {
        width: "90%",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: wp("3%"),
        alignItems: "center",
        // borderColor: "red",
        // borderWidth: 1,
        // borderStyle: "solid",
    },
    cardInner2: {
        width: "90%",
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: wp("3%"),
        alignItems: "center",
        // borderColor: "red",
        // borderWidth: 1,
        // borderStyle: "solid",
    },
    cardText: {
        fontWeight: "bold",
        fontSize: wp("3.7%"),
        color: "#1E2081",
    },
    cardIconColor: {
        color: "#1B80F3",
        fontSize: hp("3%"),
    },
    segment: {
        backgroundColor: "#F9F9F9",
    },

    segmentButton: {
        height: hp("6.5%"),
        width: windowWidth * 0.45,
        justifyContent: "center",
        marginTop: 20,
    },

    btnActive: {
        backgroundColor: "#038BEF",
        fontWeight: "bold",
        color: "white",
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
    },

    btnInactive: {
        backgroundColor: "#A0A9BE1A",
        color: "black",
        borderBottomLeftRadius: 10,
        borderTopLeftRadius: 10,
    },

    segmentTextActive: {
        fontWeight: "bold",
        fontSize: hp("2%"),
        color: "black",
    },
    segmentTextInActive: {
        fontSize: hp("2%"),
        color: "white",
        fontWeight: "bold",
    },
    date: {
        marginTop: 40,
        marginLeft: windowWidth * 0.05,
        color: "#041A32",
        fontSize: 18,
        fontWeight: "bold",
    },
    patientCard: {
        borderRadius: 0,
        backgroundColor: '#F9F9F9'
    },
    list: {
        justifyContent: "center",
        alignSelf: "center",
        marginTop: 20,
        flex: 1,
        marginBottom: 10
    },
    patientCardText: {
        marginRight: 40,
        width: 150,
        marginLeft: wp("5%"),
    },
    patientCardTextIcon: {
        fontSize: 12,
        color: "grey",
    },
    intakeBtn: {
        backgroundColor: "#C70315",
        borderRadius: 7,
        margin: 2,
        padding: 4,
    },
    intakeBtnText: {
        textAlign: "center",
        color: "#fff",
        fontSize: 12,
    },
    patientCardTextTime: {
        marginTop: 6,
        color: "#A0A9BE",
        fontSize: 12,
    },
}));
