import React, { useEffect } from "react";
import { FlatList, Image, SafeAreaView, StyleSheet, TouchableOpacity, View, } from "react-native";
import { Container, Icon, Item, Text } from "native-base";

import styles from "./style";
import Images from "../../../assets/images";
import { useDispatch, useSelector } from "react-redux";
import { getRefreshTokenHome } from "../../../actions/profile";
import { getRefToken } from "../../../services/auth";
import { getUpcomingBookings } from "../../../actions/start-consultation";
import { methodLog, successLog } from "../../../utils/fireLog";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import common from "../../../styles/common";
import { getTopSpeciality } from "../../../actions/sehet/speciality-action";
import { ifNotValid, ifValid } from "../../../utils/ifNotValid";
import moment from "moment";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_13, FONT_SIZE_16 } from "../../../styles/typography";
import Line from "../../../components/line";
import database from "@react-native-firebase/database";
import { addUserLogToFirebase, checkUserProfile } from "../../../utils/helper";
import { setCallHeader, setSelectedAppointment, getUpcomingBookingsByDate } from "../../../actions/bookings";

const HomeScreenDoctor = (props) => {
    const dispatch = useDispatch();
    const userProfile = useSelector((state) => state.profile.userProfile);
    const todaysBookings = useSelector((state) => state.bookings.upcomingBookingsByDate);
    const specialities = useSelector((state) => state.specialitiesReducer.specialitiesTop);

    useEffect(() => {
        // dispatch(getTopSpeciality())
        dispatch(getUpcomingBookingsByDate(moment(new Date()).format("YYYY-MM-DD")))
        console.log(JSON.stringify(todaysBookings))
        successLog("doctor home screen loaded")
        return () => {
        };
    }, []);

    const getToken = async () => {
        let token = await getRefToken();
        // dispatch(getRefreshTokenHome(token, props.navigation));
    };


    const getFirstName = (fullName) => {
        let full = fullName.split(" ");
        return full[0] + "! ";
    };

    const getUpcomingList = (fullName) => {
    };


    const approveRejectCall = (item_, status) => {
        let item = {
            _id: 1,
            fullName: '',
            doctorId: {
                roomName: "ARJUN"
            }
        }
        methodLog("approveRejectCall")
        try {
            dispatch(setCallHeader(ifValid(item.patientId) ? `${item.patientId.fullName}` : " "))
            dispatch(setSelectedAppointment(item._id))
            let obj = { 11: { status: status } };
            successLog('inside firebasdde' + JSON.stringify(obj))
            const newReference = database().ref("/doctor")
                .update(obj)
                .then((__) => {
                    if (status !== "REJECTED") {
                        //adding user
                        let fullName = 'ddd'
                        //let {fullName, image} = checkUserProfile(this.props.userProfile);
                        //  fullName ="ASS"
                        // addUserLogToFirebase(item._id, {event: status, name: `${fullName} (Doctor)`}).then(r => {
                        // })
                        /// adding first user in list.
                        successLog('addUserLogToFirebase')
                        const users = database()
                            .ref(`/users/${[item._id]}`)
                            .update({ doctor: { fullName: `${fullName}`, image: 'image' } })
                            .then((obj) => {
                                props.navigation.navigate("JoinCall", {
                                    channel: item.doctorId.roomName,
                                    user: "DOCTOR",
                                    id: item._id,
                                    directLogin:
                                        item.directLogin && item.directLogin == "true" ? true : false,
                                });
                            });
                    }
                });

            if (status === "REJECTED") {
                if (item.directLogin && item.directLogin == "true")
                    this.props.updateAppointmentDirect(
                        item._id,
                        this.props.navigation,
                        "REJECTED"
                    );
                else
                    props.updateAppointment(
                        item._id,
                        this.props.navigation,
                        "REJECTED"
                    );
            }
        } catch (e) {
            recordError(e)
        }
    };

    const renderItem = ({ item }) => {
        // if (ifNotValid(item))
        //     return <View/>
        let doctorId = item.doctorId;

        // if (ifNotValid(doctorId))
        //     return <View/>

        let fullName = item.patientId.fullName
        ifNotValid(fullName) ? fullName = "ddd" : "Ddd"

        let imageUrl = item.patientId.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = Images.user
        else imageUrl = { uri: imageUrl }

        let isInvalid = false;
        let day = item.utcDate
        if (ifValid(day)) day = moment(item.utcDate).format("dddd")
        else isInvalid = true;


        let date = item.utcDate
        if (ifValid(date)) date = moment(item.utcDate).format("D MMM")
        else isInvalid = true

        let time = item.utcDate
        if (ifValid(time)) time = moment(item.utcDate).format("hh:mm A")
        else isInvalid = true


        return (
            <Item style={{ flex: 1, margin: 2, }}>
                <View style={{ flexDirection: 'row', marginHorizontal: 20, marginVertical: 10, padding: 2, }}>

                    <Image source={imageUrl} style={{ width: 57, height: 57 }} />

                    <View style={{ flex: 1, paddingHorizontal: 5 }}>

                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                            <Text style={{
                                fontSize: FONT_SIZE_16,
                                paddingBottom: 5,
                                fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                                color: "#041A32"
                            }}>
                                {fullName}
                            </Text>

                            <TouchableOpacity
                                onPress={() => {
                                    //dispatch(setUpcomingPastActive("PAST"))
                                    //props.navigation.navigate("UpcomingPastBookingsDoctor")
                                    approveRejectCall(item, "APPROVED");
                                }}
                                style={[common.intakeBtn, { backgroundColor: "#C70315" }]}>
                                <Text style={[common.intakeBtnText, { color: '#fff', paddingHorizontal: 4 }]}>Start
                                    Call</Text>
                            </TouchableOpacity>
                        </View>


                        {isInvalid === true ?
                            <Text>...</Text> :
                            <Text style={innerStyle.subTitle}>
                                <Icon style={[innerStyle.subIcon]} type="AntDesign"
                                    name="calendar" /> {day}, {date}, <Icon style={innerStyle.subIcon}
                                        type="Feather"
                                        name="clock" /> {time}
                            </Text>}
                    </View>
                </View>
                <View style={{ margin: 1 }}>
                    <Line style={{ borderBottomWidth: 0.3 }} />
                </View>
            </Item>


        );
    };


    return (
        <Container>
            <View style={styles.headerContainer}>
                <SafeAreaView style={styles.header}>
                    <Icon
                        name="menu"
                        type="Feather"
                        style={styles.sideDrawerIcon}
                        onPress={() => props.navigation.openDrawer()}
                    />
                    <View style={common.headerBellAndText}>
                        <Text style={styles.headerText}>
                            Consult doctor
                            here…{" "} {userProfile?.userId !== undefined ? getFirstName(userProfile?.userId.fullName) : ""}
                        </Text>
                        {/*<TouchableOpacity>*/}
                        {/*    <Image source={Images.bell_icon} style={common.bellStyle}/>*/}
                        {/*</TouchableOpacity>*/}
                    </View>
                </SafeAreaView>
            </View>
            <Container style={{ flex: 1, backgroundColor: '#FFFFFF' }}>
                <View style={styles.cardContainer}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => props.navigation.navigate("PatientDetails")}>
                        <Image resizeMode="stretch" source={Images.coupen_code}
                            style={[styles.card, { width: wp("88%") }]} />
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1 }}>
                    <Text style={{ marginHorizontal: 25, marginVertical: 10, color: 'grey' }}>Today's appointments</Text>
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        scrollEnabled
                        // data={[{}, {}, {}, {}, {}, {}, {}]}
                        data={todaysBookings}
                        renderItem={(item) => renderItem(item)}
                    // renderItem={this.renderItem}
                    />
                </View>


            </Container>
        </Container>
    );
};
export default HomeScreenDoctor;

const innerStyle = StyleSheet.create({
    subTitle: {
        fontSize: FONT_SIZE_13,
        color: "#A0A9BE",
    },
    subIcon: {
        fontSize: FONT_SIZE_13,
        color: "#A0A9BE",
    },
});
