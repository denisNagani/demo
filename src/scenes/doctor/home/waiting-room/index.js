import React, { Component } from "react";
import { Image, SafeAreaView, View } from "react-native";
import { Container, Icon, Text, } from "native-base";
import { connect } from "react-redux";
import styles from "../../../../styles/common";
import Images from "../../../../assets/images";
import database from "@react-native-firebase/database";
import { bindActionCreators } from "redux";
import { getUpcomingBookings, updateAppointment, } from "../../../../actions/bookings";
import { showModal } from "../../../../actions/modal";
import { addUserLogToFirebase, checkUserProfile } from "../../../../utils/helper";
import { JOINED } from "../../../../utils/constant";
import { methodLog, recordError, successLog } from "../../../../utils/fireLog";

class WaitingRoom extends Component {
    constructor(props) {
        super(props);
        this.state = {
            connect: false,
            isLoading: false,
        };
    }

    componentDidMount() {
        successLog("WaitingRoom screen loaded")
        this.checkCallStatus();
    }

    componentWillUnmount() {
        database()
            .ref(`/doctor/${this.props.navigation.state.params.bookingId}`)
            .off("value");
    }

    checkCallStatus = () => {
        methodLog("checkCallStatus")
        let id = this.props.navigation.state.params.bookingId;
        try {
            database()
                .ref(`/doctor/${this.props.navigation.state.params.bookingId}`)
                .on("value", (snapshot) => {
                    if (snapshot.val() !== null && snapshot.val().status === "APPROVED") {
                        let { fullName, image } = checkUserProfile(this.props.userProfile);
                        /// add log in firebase.
                        addUserLogToFirebase(id, { event: JOINED, name: `${fullName} (Patient)` }).then(r => {
                            this.redirect();
                        })

                    } else if (
                        snapshot.val() !== null &&
                        snapshot.val().status === "REJECTED"
                    ) {
                        let payload = {
                            text: "Your appointment has been Rejected",
                            subtext: "",
                            iconName: "closecircleo",
                            modalVisible: true,
                        };
                        this.props.showModal(payload);
                        this.props.getUpcomingBookings();
                        this.props.navigation.goBack();
                    }
                });
        } catch (e) {
            recordError(e)
        }
    };

    redirect = () => {
        methodLog("redirect")
        try {
            this.props.navigation.navigate("JoinCall", {
                channel: this.props.navigation.state.params.roomName,
                user: "PATIENT",
                bookingId: this.props.navigation.state.params.bookingId,
            });
        } catch (e) {
            recordError(e)
        }
    };

    render() {
        let doctor = this.props.navigation.state.params.doctor;
        return (
            <Container>
                <SafeAreaView style={styles.header}>
                    <Icon
                        name="arrowleft"
                        type="AntDesign"
                        style={styles.headerBackIcon}
                        onPress={() => this.props.navigation.goBack()}
                    />
                    <Text style={styles.heading}>Waiting Room</Text>
                </SafeAreaView>
                <View style={styles.waitingBoxContainer}>
                    <View style={styles.waitingBox}>
                        <View>
                            <Text style={{ color: "#FFFFFF", fontSize: 15 }}>
                                {doctor
                                    ? doctor.fullName
                                    : this.props.navigation.state.params.roomName}
                            </Text>
                            <Text style={{ color: "#A0A9BE", fontSize: 12 }}>
                                waiting for your doctor to come online
                            </Text>
                        </View>
                        <Text style={styles.offlineStyle}>
                            {" "}
                            <Image
                                source={Images.phoneOff}
                                style={styles.offlineImageStyle}
                            />{" "}
                            offline
                        </Text>
                    </View>

                    <View style={{ alignItems: "center" }}>
                        <View
                            style={{
                                borderStyle: "dotted",
                                height: 50,
                                borderLeftWidth: 1,
                                borderLeftColor: "#A0A9BE",
                            }}
                        />
                        <View>
                            <Text style={{ color: "#03FF9E", fontSize: 18 }}>
                                Connecting..
                            </Text>
                        </View>
                        <View
                            style={{
                                borderStyle: "dotted",
                                height: 50,
                                borderLeftWidth: 1,
                                borderLeftColor: "#A0A9BE",
                            }}
                        />
                    </View>

                    <View
                        style={{ borderColor: "#A0A9BE", borderWidth: 1, borderRadius: 10 }}
                    >
                        <Image
                            source={{
                                uri:
                                    this.props.userProfile &&
                                        this.props.userProfile.userId &&
                                        this.props.userProfile.userId.imageUrl !== null
                                        ? this.props.userProfile.userId.imageUrl
                                        : "https://genslerzudansdentistry.com/wp-content/uploads/2015/11/anonymous-user.png",
                            }}
                            style={{
                                resizeMode: "contain",
                                width: 154,
                                height: 152,
                                borderRadius: 10,
                            }}
                        />
                    </View>
                </View>

                <View />
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        { getUpcomingBookings, updateAppointment, showModal },
        dispatch
    );

const mapStateToProps = (state) => ({
    upcomingBookings: state.bookings.upcomingBookings,
    userProfile: state.profile.userProfile,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(WaitingRoom);
