import React, {Component} from "react";
import {FlatList, Image, Linking, SafeAreaView, Text, TouchableOpacity, View,} from "react-native";
import {ListItem} from "react-native-elements";
import moment from "moment";
import {Button, Container, Icon, Segment,} from "native-base";
import styles from "./style";
import Images from "../../../assets/images/index";
import http from "../../../services/http";
import {intake_url} from "../../../config";
import {bindActionCreators} from "redux";
import {showModal} from "../../../actions/modal";
import {connect} from "react-redux";
import common from "../../../styles/common";
import {recordError, successLog} from "../../../utils/fireLog";

class DoctorPastBookingsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bookings: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        successLog("DoctorPastBookingsScreen screen loaded")
        try {
            this.setState({
                isLoading: true,
            });
            http.get("api/past/appointments").then((response) => {
                console.log(response);
                this.setState({
                    isLoading: false,
                });
                if (response.status === 400) {
                    const payload = {
                        text: response.message,
                        subText: "",
                        iconName: "closecircleo",
                        modalVisible: true,
                        iconColor: "#C70315",
                    };
                    this.props.showModal(payload);

                } else if (response.status === 200) {
                    // set bookings here
                    this.setState({
                        bookings: response.data,
                    });
                }
            });
        } catch (e) {
            recordError(e)
        }

    }

    keyExtractor = (item) => item._id.toString();

    renderItem = ({item}) => {
        if (item.patientId === undefined || item.patientId == null) {
            return null;
        }
        let status = item.status
        if (status === undefined || status == null) {
            status = ""
        }
        return (
            <ListItem
                title={item.patientId.fullName}
                subtitle={
                    <View>
                        <Text style={styles.subTitle}>
                            <Icon style={[styles.subIcon]} type="AntDesign" name="calendar"/>{" "}
                            {item.utcDate && moment(item.utcDate).format("dddd DD MMM")}
                            {"   "}
                            <Icon style={[styles.subIcon]} type="Feather" name="clock"/>{" "}
                            {item.utcDate && moment(item.utcDate).format("hh:mm A")}
                        </Text>
                    </View>
                }
                leftAvatar={
                    item.patientId.imageUrl !== "" && item.patientId.imageUrl !== null
                        ? {source: {uri: item.patientId.imageUrl}}
                        : {source: Images.user}
                }
                bottomDivider
                rightElement={
                    <View>
                        <TouchableOpacity
                            style={[common.intakeBtn, {}]}
                            onPress={() =>
                                Linking.openURL(
                                    `${intake_url}pdf/${item.patientId._id}.pdf`
                                )
                            }
                        >
                            <Text style={common.intakeBtnText}>Intake Form</Text>

                        </TouchableOpacity>


                        <TouchableOpacity
                            style={[common.intakeBtn, {backgroundColor: "#1B80F3"}]}>
                            <Text style={[common.intakeBtnText, {color: '#fff'}]}>{status}</Text>
                        </TouchableOpacity>

                    </View>
                }
            />
        );
    }

    render() {
        let noBookings;
        if (!this.state.isLoading && this.state.bookings.length <= 0) {
            noBookings = (
                <View style={styles.noBookingsBlock}>
                    <Image source={Images.no_bookings} style={styles.noBookingsImage}/>
                    <Text style={styles.noBookingsTitle}>No Past Appointments!</Text>
                    <Text style={styles.noBookingsSubTitle}>
                        Currently there are no past appointments
                    </Text>
                    <Button
                        block
                        danger
                        style={styles.noBookingsBtn}
                        onPress={() => {
                            this.props.navigation.navigate("DashboardScreen");
                        }}
                    >
                        <Text style={styles.noBookingsBtnText}>GO TO DASHBOARD</Text>
                    </Button>
                </View>
            );
        }
        let loader;
        if (this.state.isLoading) {
            loader = <Text style={{textAlign: "center"}}>Loading...</Text>;
        }
        return (
            <Container>
                <SafeAreaView style={styles.header}>
                    <Icon
                        name="arrowleft"
                        type="AntDesign"
                        style={styles.headerBackIcon}
                        onPress={() => this.props.navigation.goBack()}
                    />
                    <Text style={styles.heading}>My Appointments</Text>
                </SafeAreaView>
                <View>
                    <Segment style={styles.segment}>
                        <Button
                            onPress={() =>
                                this.props.navigation.navigate("DoctorUpcomingBookingsScreen")
                            }
                            first
                            active
                            style={[styles.segmentButton, styles.btnInactive]}
                        >
                            <Text style={styles.segmentTextActive}>Upcoming</Text>
                        </Button>
                        <Button last style={[styles.segmentButton, styles.btnActive]}>
                            <Text style={styles.segmentTextInActive}>Past</Text>
                        </Button>
                    </Segment>
                </View>

                <View style={styles.listView}>
                    {noBookings}
                    {loader}
                    <FlatList
                        scrollEnabled
                        keyExtractor={this.keyExtractor}
                        data={this.state.bookings}
                        renderItem={this.renderItem}
                    />
                </View>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({showModal}, dispatch);
const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DoctorPastBookingsScreen);
