import React, {Component} from "react";
import {SafeAreaView, Text} from "react-native";
import {Container, Content, DatePicker, Form, Icon, Input, Item, Label, Picker, View,} from "native-base";
import styles from "./style";
import {Collapse, CollapseBody, CollapseHeader,} from "accordion-collapse-react-native";

export default class ViewPatientIntakeFormScreen extends Component {
	render() {
		return (
			<Container>
				<SafeAreaView style={styles.header}>
					<Icon
						name="ios-arrow-back"
						style={styles.headerBackIcon}
						onPress={() => {
							this.props.navigation.goBack();
						}}
					/>
					<Text style={styles.heading}>Rohnold Jay Intake Form</Text>
				</SafeAreaView>

				<Content>
					<Collapse isCollapsed="true">
						<CollapseHeader style={styles.collapseHeader}>
							<View style={styles.headerView}>
								<View style={{ flex: 0.8, fontSize: 17 }}>
									<Text style={styles.headerTitle}>Personal Information</Text>
									<Text style={styles.textSuccess}>Completed</Text>
								</View>
								<Icon
									style={{ flex: 0.1, color: "lightgray" }}
									type="Ionicons"
									name="chevron-forward-circle"
								/>
							</View>
						</CollapseHeader>
						<CollapseBody style={styles.collapseBody}>
							<Form>
								<View style={[styles.formGroup]}>
									<Item floatingLabel style={[styles.firstName, styles.input]}>
										<Label style={styles.label}>First Name*</Label>
										<Input value="John" disabled />
									</Item>
									<Item floatingLabel style={[styles.lastName, styles.input]}>
										<Label style={styles.label}>Last Name*</Label>
										<Input value="Doe" disabled placeholder="Last Name" />
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item floatingLabel style={[styles.email, styles.input]}>
										<Label style={styles.label}>Email*</Label>
										<Input value="johndoe123@gmail.com" />
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item floatingLabel style={[styles.email, styles.input]}>
										<Label style={styles.label}>Mobile Number*</Label>
										<Input value="+1555-123212" />
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item floatingLabel style={[styles.email, styles.input]}>
										<Label style={styles.label}>Sexual Orientation</Label>
										<Input />
									</Item>
								</View>

								<View style={[styles.formGroup]}>
									<Item picker style={[styles.genderPicker, styles.input]}>
										<Picker
											mode="dropdown"
											style={styles.pickerItem}
											placeholder="Gender"
										>
											<Picker.Item label="Gender*" value="" />
											<Picker.Item label="Male" value="male" />
											<Picker.Item label="female" value="female" />
										</Picker>
									</Item>
									<Item style={[styles.datePicker, styles.input]}>
										<DatePicker
											locale={"en"}
											timeZoneOffsetInMinutes={undefined}
											modalTransparent={false}
											animationType={"fade"}
											androidMode={"default"}
											placeHolderText="Date of birth*"
											textStyle={{ color: "gray" }}
											placeHolderTextStyle={{ color: "gray" }}
											disabled={false}
										/>
									</Item>
								</View>

								<View style={[styles.formGroup]}>
									<Item floatingLabel style={[styles.firstName, styles.input]}>
										<Label style={styles.label}>Mother's Name</Label>
										<Input />
									</Item>
									<Item floatingLabel style={[styles.lastName, styles.input]}>
										<Label style={styles.label}>Father's Name</Label>
										<Input />
									</Item>
								</View>

								<View style={[styles.formGroup]}>
									<Item style={[styles.racePicker, styles.input]}>
										<Picker mode="dropdown" style={styles.pickerItem}>
											<Picker.Item label="Race" value="" />
											<Picker.Item label="One" value="one" />
											<Picker.Item label="Two" value="two" />
										</Picker>
									</Item>
									<Item style={[styles.ethentiCityPicker, styles.input]}>
										<Picker mode="dropdown" style={styles.pickerItem}>
											<Picker.Item label="Ethnicity" value="" />
											<Picker.Item label="One" value="one" />
											<Picker.Item label="Two" value="two" />
										</Picker>
									</Item>
								</View>

								<View style={[styles.formGroup]}>
									<Item style={[styles.languagePicker, styles.input]}>
										<Picker mode="dropdown" style={styles.pickerItem}>
											<Picker.Item label="Language" value="" />
											<Picker.Item label="One" value="one" />
											<Picker.Item label="Two" value="two" />
										</Picker>
									</Item>
									<Item
										floatingLabel
										style={[styles.socialSecurity, styles.input]}
									>
										<Label style={styles.label}>Social security #</Label>
										<Input />
									</Item>
								</View>
							</Form>
						</CollapseBody>
					</Collapse>

					<Collapse>
						<CollapseHeader style={styles.collapseHeader}>
							<View style={styles.headerView}>
								<View style={{ flex: 0.8, fontSize: 17 }}>
									<Text style={styles.headerTitle}>Insurance Details</Text>
									<Text style={styles.textWarning}>50% Completed</Text>
								</View>
								<Icon
									style={{ flex: 0.1, color: "lightgray" }}
									type="Ionicons"
									name="chevron-forward-circle"
								/>
							</View>
						</CollapseHeader>
						<CollapseBody style={styles.collapseBody}>
							<Form>
								<View style={styles.formGroup}>
									<Item style={[styles.pInsurancePicker, styles.input]}>
										<Picker
											mode="dropdown"
											placeholder="Gender"
											style={styles.pickerItem}
										>
											<Picker.Item label="Insurance1" value="1" />
											<Picker.Item label="Insurance1" value="2" />
										</Picker>
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item
										floatingLabel
										style={[styles.secondaryInsurace, styles.input]}
									>
										<Label style={styles.label}>Secondary Insurace</Label>
										<Input disabled />
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item floatingLabel style={[styles.partyName, styles.input]}>
										<Label style={styles.label}>Responsible Party Name*</Label>
										<Input disabled />
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item
										floatingLabel
										style={[styles.partyRelation, styles.input]}
									>
										<Label style={styles.label}>
											Responsible Party Relationship*
										</Label>
										<Input value="4561132" disabled />
									</Item>
								</View>
							</Form>
						</CollapseBody>
					</Collapse>

					<Collapse>
						<CollapseHeader style={styles.collapseHeader}>
							<View style={styles.headerView}>
								<View style={{ flex: 0.8, fontSize: 17 }}>
									<Text style={styles.headerTitle}>Address Information</Text>
									<Text style={styles.textSecondary}>Incomplete</Text>
								</View>
								<Icon
									style={{ flex: 0.1, color: "lightgray" }}
									type="Ionicons"
									name="chevron-forward-circle"
								/>
							</View>
						</CollapseHeader>
						<CollapseBody style={styles.collapseBody}>
							<Form>
								<View style={styles.formGroup}>
									<Item floatingLabel style={[styles.address1, styles.input]}>
										<Label style={styles.label}>Primary address line 1</Label>
										<Input />
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item floatingLabel style={[styles.address2, styles.input]}>
										<Label style={styles.label}>Primary address line 2</Label>
										<Input />
									</Item>
								</View>

								<View style={[styles.formGroup]}>
									<Item picker style={[styles.cityPicker, styles.input]}>
										<Picker
											mode="dropdown"
											style={styles.pickerItem}
											placeholder="City"
										>
											<Picker.Item label="City*" value="" />
											<Picker.Item label="City 1" value="c2" />
											<Picker.Item label="City 2" value="c2" />
										</Picker>
									</Item>
									<Item style={[styles.statePicker, styles.input]}>
										<Picker mode="dropdown" style={styles.pickerItem}>
											<Picker.Item label="State*" value="" />
											<Picker.Item label="State 1" value="s1" />
											<Picker.Item label="STate 2" value="s2" />
										</Picker>
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item floatingLabel style={[styles.zipcode, styles.input]}>
										<Label style={styles.label}>Zipcode*</Label>
										<Input />
									</Item>
								</View>
							</Form>
						</CollapseBody>
					</Collapse>

					<Collapse>
						<CollapseHeader style={styles.collapseHeader}>
							<View style={styles.headerView}>
								<View style={{ flex: 0.8, fontSize: 17 }}>
									<Text style={styles.headerTitle}>Medical History</Text>
									<Text style={styles.textSecondary}>Incomplete</Text>
								</View>
								<Icon
									style={{ flex: 0.1, color: "lightgray" }}
									type="Ionicons"
									name="chevron-forward-circle"
								/>
							</View>
						</CollapseHeader>
						<CollapseBody style={styles.collapseBody}>
							<Form>
								<View style={styles.formGroup}>
									<Item style={[styles.pInsurancePicker, styles.input]}>
										<Picker mode="dropdown" style={styles.pickerItem}>
											<Picker.Item label="Chiledhood Diseases*" value="" />
											<Picker.Item label="Measles" value="1" />
											<Picker.Item label="Mumps" value="2" />
											<Picker.Item label="MumChicken Pox" value="2" />
											<Picker.Item label="Rheumatic fever" value="2" />
											<Picker.Item label="Other" value="2" />
										</Picker>
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item
										floatingLabel
										style={[styles.secondaryInsurace, styles.input]}
									>
										<Label style={styles.label}>
											Medications and Allergies
										</Label>
										<Input />
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item floatingLabel style={[styles.partyName, styles.input]}>
										<Label style={styles.label}>
											Current and Past Medical Conditions
										</Label>
										<Input />
									</Item>
								</View>

								<View style={styles.formGroup}>
									<Item
										floatingLabel
										style={[styles.partyRelation, styles.input]}
									>
										<Label style={styles.label}>
											Responsible Party Relationship*
										</Label>
										<Input />
									</Item>
								</View>
							</Form>
						</CollapseBody>
					</Collapse>
				</Content>

				{/*<Footer style={styles.footer}>*/}
				{/*	<Button*/}
				{/*		danger*/}
				{/*		style={styles.footerBtn}*/}
				{/*		onPress={() =>*/}
				{/*			this.props.navigation.navigate("DoctorUpcomingBookingsScreen")*/}
				{/*		}*/}
				{/*	>*/}
				{/*		<Text style={styles.footerBtnText}>Join Call</Text>*/}
				{/*	</Button>*/}
				{/*</Footer>*/}
			</Container>
		);
	}
}
