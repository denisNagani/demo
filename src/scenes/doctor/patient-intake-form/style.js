import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default (styles = StyleSheet.create({
	header: {
		backgroundColor: "#1E2081",
		height: hp("13%"),
		flexDirection: "row",
		alignItems: "center",
	},
	headerBackIcon: {
		flex: 1,
		fontSize: hp("5%"),
		color: "#fff",
		marginLeft: wp("5%"),
	},
	heading: {
		flex: 8,
		color: "#fff",
		fontSize: hp("2.9%"),
	},
	headerView: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
	collapseHeader: {
		padding: 20,
		borderBottomWidth: 1,
		backgroundColor: "#fafafa",
		borderColor: "#f1f1f1",
	},
	collapseBody: {
		padding: 20,
	},
	formGroup: {
		flex: 1,
		flexDirection: "row",
		padding: "2%",
	},

	input: {
		padding: "2%",
	},
	label: {
		color: "gray",
	},
	pickerItem: {
		color: "gray",
	},
	firstName: {
		flex: 0.5,
	},

	lastName: {
		flex: 0.5,
	},

	email: {
		flex: 1,
	},
	genderPicker: {
		flex: 0.5,
	},
	continueBtn: {
		width: "100%",
		marginBottom: 30,
	},
	btnText: {
		color: "#fff",
		fontSize: 20,
	},
	datePicker: {
		flex: 0.5,
	},
	racePicker: {
		flex: 0.5,
	},
	ethentiCityPicker: {
		flex: 0.5,
	},
	languagePicker: {
		flex: 0.5,
	},
	socialSecurity: {
		flex: 0.5,
	},

	// insurance form
	pInsurancePicker: {
		flex: 1,
	},
	secondaryInsurace: {
		flex: 1,
	},
	partyName: {
		flex: 1,
	},
	partyRelation: {
		flex: 1,
	},
	// address information
	address1: {
		flex: 1,
	},

	address2: {
		flex: 1,
	},
	cityPicker: {
		flex: 0.5,
	},
	statePicker: {
		flex: 0.5,
	},
	zipcode: {
		flex: 1,
	},

	textSecondary: {
		color: "#A0A9BE",
	},
	textSuccess: {
		color: "#56B732",
	},
	textWarning: {
		color: "#FF863B",
	},

	footerBtn: {
		flex: 1,
		margin: 10,
		justifyContent: "center",
		fontSize: 20,
	},
	cancelBtn: {
		backgroundColor: "#A0A9BE",
	},
	footer: {
		height: 70,
		backgroundColor: "#fff",
	},
	footerBtnText: {
		color: "#fff",
		fontSize: 15,
		fontWeight: "bold",
	},
}));
