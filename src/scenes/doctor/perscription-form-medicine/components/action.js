import {GET_PRESCRIPTION_DETAILS} from "../../../../utils/constant";
import {hide, show} from "../../../../utils/loader/action";
import http from "../../../../services/http";
import {errorLog, successLog} from "../../../../utils/fireLog";
import {ifNotValid} from "../../../../utils/ifNotValid";


export const getPrescriptionDetails = (id) => {
    return (dispatch) => {
        dispatch(show());
        http.get(`prescription/${id}`, dispatch)
            .then((res) => {
                successLog("getPrescriptionDetails ", res.status)
                dispatch(hide());
                if (ifNotValid(res.data)) {
                    successLog(JSON.stringify(res.data) + "wrong data ")
                    return
                }
                dispatch({type: GET_PRESCRIPTION_DETAILS, payload: res.data});
            })
            .catch((o) => {
                errorLog("getPrescriptionDetails", o)
                dispatch(hide());
            });
    };
};
