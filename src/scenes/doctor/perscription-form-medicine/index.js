import React, { useEffect } from "react";
import { Dimensions, FlatList, ScrollView, StyleSheet, Text, View, BackHandler } from "react-native";
import { Container, Item, Content } from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { errorLog, successLog } from "../../../utils/fireLog";
import { ifNotValid, ifValid } from "../../../utils/ifNotValid";
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_FAMILY_SF_PRO_SemiBold,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_16
} from "../../../styles/typography";
import HeaderSehet from "../../../components/header-common";
import { calculate_age } from "../../../utils/helper";
import { strings } from "../../../utils/translation";
import common from "../../../styles/common";
let deviceWidth = Dimensions.get('window').width

const PrescriptionFormMedicine = (props) => {
    const dispatch = useDispatch();
    const docProfile = useSelector((state) => state.userReducer.docProfile);
    const prescription = useSelector((state) => state.prescriptionReducer.prescription);
    //by default false..
    const prescriptionAdded = useSelector((state) => state.prescriptionReducer.prescriptionAdded);
    const selectedPatient = useSelector(state => state.prescriptionReducer.selectedPatient);

    useEffect(() => {
        successLog("Prescription form loaded..!")
        BackHandler.addEventListener("hardwareBackPress", () => {
            props.navigation.navigate('UpcomingPastBookingsDoctor')
        })
    }, [])


    const TableRow = ({ title, value }) => {

        return <View style={{ flexDirection: 'row', marginHorizontal: 10, }}>
            <Item bordered style={[{ flex: 1 }, common.itemBorderStyle]}>

                <Text style={{
                    padding: 4,
                    color: '#041A32',
                    fontSize: FONT_SIZE_15,
                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
                }}>{title}</Text>
            </Item>
            <Item bordered style={[{ flex: 2 }, common.itemBorderStyle]}>

                <Text style={{
                    padding: 4,
                    color: '#041A32',
                    fontSize: FONT_SIZE_14,
                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
                }}>{value}</Text>
            </Item>
        </View>

    }
    const _onAddPress = () => {
        try {
            props.navigation.navigate('AddMedicine')
        } catch (e) {
            errorLog(e)
        }
    }

    const headerView = () => {
        return <View style={{ flexDirection: 'row', marginHorizontal: 10, }}>
            <Item style={[common.itemBorderStyle, { width: 70, },]}>
                <Text style={styles.textStyle}>{'No.'}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 150 },]}>
                <Text style={styles.textStyle}>Medicine Name </Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 110 },]}>
                <Text style={styles.textStyle}>Type</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 110 },]}>
                <Text style={styles.textStyle}>Dose per Day </Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 200 },]}>
                <Text style={styles.textStyle}>Time Of Day</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 90 },]}>
                <Text style={styles.textStyle}>Duration</Text>
            </Item>
        </View>
    }


    const getPrescriptionDetails = () => {
        if (ifNotValid(prescription)) return;
        if (ifNotValid(prescription.medicine)) return;
        return <View>
            <Text style={styles.headingTitle}>Medicine Details</Text>

            <ScrollView horizontal={true}>
                <View>
                    {headerView()}
                    <FlatList
                        keyExtractor={(item, index) => index.toString()}
                        scrollEnabled
                        data={prescription.medicine}
                        renderItem={({ item, index }) => getSingleMedicineRows(item, index + 1)}
                    />
                    {ifValid(prescription.comments) ?
                        <View style={{ width: '94%', marginTop: 10, }}>
                            <Text style={[styles.headingTitle, { marginBottom: 0 }]}>Comment:</Text>
                            <Text style={[styles.headingTitle, {
                                marginTop: 0,
                                fontFamily: FONT_FAMILY_SF_PRO_REGULAR
                            }]}>{ifValid(prescription.comments) ? prescription.comments : ''}</Text>
                        </View> : null}
                </View>
            </ScrollView>
        </View>
    }

    const getSingleMedicineRows = (obj, index) => {
        successLog(JSON.stringify(obj))
        let medicineName = ifValid(obj.medicineName) ? obj.medicineName : '-'
        let typeOfMedicine = ifValid(obj.typeOfMedicine) ? obj.typeOfMedicine : '-'
        let dose = ifValid(obj.dose) ? obj.dose : '-'
        let eventOfDay = ifValid(obj.eventOfDay) ? obj.eventOfDay : '-'
        let duration = ifValid(obj.duration) ? `${obj.duration} ${obj.duration === 1 ? +" day" : ' days'}` : '-'
        return <View style={{ flexDirection: 'row', marginHorizontal: 10, }}>
            <Item style={[common.itemBorderStyle, { width: 70 },]}>
                <Text numberOfLines={1} style={styles.textStyle}>{index}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 150 },]}>
                <Text style={styles.textStyle}>{medicineName}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 110 },]}>
                <Text style={styles.textStyle}>{typeOfMedicine}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 110 },]}>
                <Text style={styles.textStyle}>{dose}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 200 },]}>
                <Text style={styles.textStyle}>{eventOfDay}</Text>
            </Item>
            <Item style={[common.itemBorderStyle, { width: 90 },]}>
                <Text style={styles.textStyle}>{duration}</Text>
            </Item>
        </View>
    }


    let patientId = ifNotValid(selectedPatient) || ifNotValid(selectedPatient.patientId) ? {} : selectedPatient.patientId;
    let name = patientId.fullName;
    if (ifNotValid(name)) name = "..."
    let age = calculate_age(patientId.dob);
    if (ifNotValid(age)) age = "..."
    let email = patientId.email;
    if (ifNotValid(email)) email = "..."
    let overview = patientId.medicalDescription;
    if (ifNotValid(overview)) overview = "..."
    return (
        <Container style={styles.container}>
            <View style={{ width: '100%' }}>
                <HeaderSehet
                    rightIconStyle={styles.rightIconStyle}
                    headerText={strings.prescription_form}
                    // navigateTo="UpcomingPastBookingsDoctor"
                    navigation={props.navigation}
                />
            </View>

            <View style={{ width: '94%', marginTop: 10, }}>

                <TableRow
                    title={"Patient Name"}
                    value={name} />

                <TableRow
                    title={"Patient Age"}
                    value={calculate_age(patientId.dob)} />

                <TableRow
                    title={"Patient Email"}
                    value={email} />

                {/*<TableRow*/}
                {/*    title={"Description"}*/}
                {/*    value={overview}/>*/}
            </View>

            <ScrollView showsVerticalScrollIndicator={false} style={{ width: '94%', marginTop: 10, }}>
                {getPrescriptionDetails()}
            </ScrollView>




        </Container>
    );
};

export default PrescriptionFormMedicine;
const styles = StyleSheet.create({
    rowStyle: {
        textAlign: 'center',
        borderWidth: 0.4, borderColor: 'black', width: '8%', alignItems: 'center', padding: 1,
    },
    textStyle: { textAlign: 'center', paddingLeft: 2, paddingVertical: 10, margin: 1, flex: 1, },
    textHeaderStyle: { textAlign: 'center' },
    container: {
        alignItems: "center", flex: 1,
        width: '100%'
    },
    headingTitle: {
        marginHorizontal: 10,
        marginVertical: 10,
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold, fontSize: FONT_SIZE_16, color: 'black'
    }
})
