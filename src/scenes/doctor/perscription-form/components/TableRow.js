import {Text, View} from "react-native";
import {FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_14, FONT_SIZE_15} from "../../../../styles/typography";
import React from "react";

export const TableRow = ({title, value}) => {
    return <View style={{flexDirection: 'row', marginHorizontal: 10}}>
        <View style={{
            borderWidth: 0.4,
            borderColor: 'black', flex: 1,
        }}>
            <Text style={{
                padding: 4,
                color: '#041A32',
                fontSize: FONT_SIZE_15,
                fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
            }}>{title}</Text>
        </View>
        <View style={{borderWidth: 0.4, borderColor: 'black', flex: 2,}}>
            <Text style={{
                padding: 6,
                color: '#041A32',
                fontSize: FONT_SIZE_14,
                fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
            }}>{value}</Text>
        </View>
    </View>
}
