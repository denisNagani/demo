import {SELECTED_PATIENT} from "../../../../utils/constant";


export const setIsPrescriptionAdded = (payload) => {
    return {
        type: '',
        payload: payload,
    };
};

export const setSelectedPatient = (payload) => {
    return {
        type: SELECTED_PATIENT,
        payload: payload,
    };
};
