import React, { useEffect } from "react";
import { StyleSheet, Text, View, Image } from "react-native";
import { Button, Container, Footer, Item } from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { errorLog, successLog } from "../../../utils/fireLog";
import { ifNotValid, isValid } from "../../../utils/ifNotValid";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_14, FONT_SIZE_15, FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_SF_PRO_BOLD } from "../../../styles/typography";
import HeaderSehet from "../../../components/header-common";
import { calculate_age } from "../../../utils/helper";
import { strings, TranslationConfig } from "../../../utils/translation";
import common from "../../../styles/common";
import Images from "../../../assets/images";
import Line from "../../../components/line";
// import PowerTranslator from "react-native-power-translator";
import PowerTranslator from "../../../components/PowerTranslator";
import moment from 'moment';
import PhotoPickerModal from "../../../components/PhotoPickerModal";
import { requestPermissionCamera, requestPermissionStorage } from "../../../services/requestPermissions";
import { setPhotoPickerModal, showModal } from "../../../actions/modal";
import { launchCamera, launchImageLibrary } from "react-native-image-picker";
import { uploadPrescriptionImage } from "../../../actions/sehet/patient-action";
import { getPastBookings } from "../../../actions/bookings";
import { hide } from "../../../utils/loader/action";

const PrescriptionForm = (props) => {
    const dispatch = useDispatch();
    const docProfile = useSelector((state) => state.userReducer.docProfile);
    const prescription = useSelector((state) => state.prescriptionReducer.prescription);
    //by default false..
    const prescriptionAdded = useSelector((state) => state.prescriptionReducer.prescriptionAdded);
    const selectedPatient = useSelector(state => state.prescriptionReducer.selectedPatient);
    const lang = useSelector(state => state.profile.selected_language)
    TranslationConfig(lang)

    const photoPickerModal = useSelector(state => state.modal.photoPickerModal)
    const photoPickerTitles = [
        {
            id: 1,
            title: "Pick From Gallary",
            text: "Gallary",
        },
        {
            id: 2,
            title: "Capture Image",
            text: "Camera",
        },
    ]

    useEffect(() => {
        successLog("Prescription form loaded..!")
    }, [])


    const TableRow = ({ title, value }) => {

        return <View style={{ flexDirection: 'row', marginRight: 5, }}>
            <Item bordered style={[{ flex: 1 }, common.itemBorderStyle]}>
                <Text style={{
                    padding: 6,
                    color: '#041A32',
                    fontSize: FONT_SIZE_15,
                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
                }}>{title}</Text>
            </Item>
            <Item
                bordered style={[{ flex: 2 }, common.itemBorderStyle]}>
                <Text style={{
                    color: '#041A32',
                    padding: 6,
                    fontSize: FONT_SIZE_14,
                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
                }}>{value}</Text>
            </Item>
        </View>

    }
    const _onAddPress = () => {
        try {
            // props.navigation.navigate('AddMedicine')
            dispatch(setPhotoPickerModal(true))
        } catch (e) {
            errorLog(e)
        }
    }

    const onPressModal = (selectedItem) => {
        if (selectedItem) {
            let value = selectedItem?.text
            if (value == 'Camera') {
                requestPermissionCamera()
                    .then((status) => {
                        if (status == true) {
                            _handleSelectCamera()
                        }
                    })
            } else {
                requestPermissionStorage()
                    .then((status) => {
                        if (status == true) {
                            _handleSelectDoc()
                        }
                    })
            }
        }
    }

    const _handleSelectDoc = async () => {

        try {
            launchImageLibrary({
                selectionLimit: 2
            }, (results) => {

                if (results.didCancel) return

                let imageArray = []
                const res = results.assets
                if (res.length > 0) {
                    res.map((im) => {
                        imageArray.push({
                            uri: im.uri,
                            type: im.type,
                            name: im.fileName,
                            size: im.fileSize
                        })
                    })
                    uploadImagePrescrition(imageArray)
                }
            });
        } catch (error) {
        }

    }

    const _handleSelectCamera = async () => {

        try {
            launchCamera({
                mediaType: 'photo',
                cameraType: 'back',
            }, (results) => {

                if (results.didCancel) return

                let imageArray = []
                const res = results.assets
                if (res.length > 0) {
                    res.map((im) => {
                        imageArray.push({
                            uri: im.uri,
                            type: im.type,
                            name: im.fileName,
                            size: im.fileSize
                        })
                    })
                    uploadImagePrescrition(imageArray)
                }
            });
        } catch (error) {
        }

    }

    const createFormData = (docs) => {
        let imgName = "";
        const data = new FormData();
        docs.map((photo, index) => {
            if (photo.fileName === undefined) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else if (photo.fileName === null) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else {
                imgName = photo.fileName;
            }
            let newFile = {
                name: Platform.OS === 'android' ? photo.name : imgName,
                type: photo.type,
                uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
            }
            data.append("multi-files", newFile);
        })
        return data;
    };

    const uploadImagePrescrition = (imageData) => {
        console.log(JSON.stringify(imageData));
        if (imageData?.length > 0) {
            let appointment_id = selectedPatient?._id
            dispatch(uploadPrescriptionImage(
                createFormData(imageData),
                appointment_id,
                (data) => {
                    console.log('updatedBody ', JSON.stringify(data));
                    dispatch(hide())
                    dispatch(getPastBookings());
                    const msg = {
                        text: "Prescription added successfully!!",
                        subText: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(showModal(msg))
                    props?.navigation?.navigate("UpcomingPastBookingsDoctor")
                }
            ))
        }
    }


    // let patientId = ifNotValid(selectedPatient) || ifNotValid(selectedPatient.intakeIno) ? {} : selectedPatient.patientId;
    let patientId = selectedPatient?.intakeInfo;
    let name = patientId?.name;
    if (ifNotValid(name)) name = "..."

    // let age = patientId.dob;
    let age = moment().diff(patientId?.dob, 'years');
    if (ifNotValid(age)) age = "..."
    let email = patientId?.email;
    if (ifNotValid(email)) email = "..."
    let overview = isValid(patientId?.medicalDescription) ? patientId?.medicalDescription : "...";
    let gender = isValid(patientId?.gender) ? patientId?.gender?.toLowerCase() : "-";

    return (
        <Container style={styles.container}>
            <View style={{ width: '100%' }}>
                <HeaderSehet
                    headerText={strings.prescription_form} navigation={props.navigation} />
            </View>

            <View style={{ backgroundColor: '#FFFFFF', marginHorizontal: 30, marginVertical: 20, padding: 10 }}>
                <View style={{ flexDirection: 'row', marginVertical: 10, alignItems: 'center', borderRadius: 5 }}>
                    <Image
                        source={Images.userImage}
                        style={{ height: 50, width: 40, borderRadius: 40 }}
                    />
                    {/* <Text style={{ fontSize: 18, color: '#041A32', fontFamily: FONT_FAMILY_SF_PRO_BOLD, paddingLeft: 10 }}>{name}</Text> */}
                    {/* <PowerTranslator style={{ fontSize: 18, color: '#041A32', fontFamily: FONT_FAMILY_SF_PRO_BOLD, paddingLeft: 10 }} text={name} /> */}
                    <PowerTranslator
                        style={{ fontSize: 18, color: '#041A32', fontFamily: FONT_FAMILY_SF_PRO_BOLD, paddingLeft: 10 }}
                        hinText={name}
                        enText={name}
                    />
                </View>
                <Line style={{ borderColor: '#EEEEEE' }} />
                <Text style={[styles.title, { marginTop: 10 }]}>{strings.patient} {strings.email}</Text>
                <Text style={styles.text}>{email}</Text>
                <Text style={styles.title}>{strings.patient} {strings.age}</Text>
                <Text style={styles.text}>{age}</Text>
                <Text style={styles.title}>{strings.patient} {strings.gender}</Text>
                <Text style={styles.text}>{gender}</Text>
                <Text style={styles.title}>{strings.medical_description}</Text>
                <Text style={styles.text}>{overview}</Text>
            </View>

            <View style={{ width: '94%', marginTop: 10, flex: 1, }}>
            </View>

            <Footer style={common.footer}>
                <Button danger style={common.footerBtn} onPress={() => _onAddPress()}>
                    <Text style={{ color: 'white' }}>{strings.Add_Prescription}</Text>
                </Button>
            </Footer>

            <PhotoPickerModal visible={photoPickerModal} filter={photoPickerTitles} onPress={onPressModal} />

        </Container>
    );
};

export default PrescriptionForm;
const styles = StyleSheet.create({
    rowStyle: {
        textAlign: 'center',
        borderWidth: 0.4, borderColor: 'black', width: '8%', alignItems: 'center', padding: 1,
    },
    textStyle: { textAlign: 'center' },
    textHeaderStyle: { textAlign: 'center' },
    rightIconStyle: {
        alignItems: 'center',
        backgroundColor: '#ff2f2f', marginRight: 9,
        borderRadius: 20, fontSize: 19, padding: 2, color: 'white'
    },
    container: {
        flex: 1,
        width: '100%',
        backgroundColor: '#F9F9F9'
    },
    title: { color: '#656E83', fontSize: 14, fontFamily: FONT_FAMILY_SF_PRO_REGULAR, marginVertical: 3 },
    text: { fontSize: 15, color: '#041A32', fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, marginBottom: 20 }
})
