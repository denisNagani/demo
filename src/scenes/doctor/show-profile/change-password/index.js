import React, { Component } from "react";
import { SafeAreaView, TextInput, TouchableOpacity } from "react-native";
import { Container, Form, Icon, Input, Item, Label, Content, } from "native-base";
import styles from "../style";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { methodLog, recordError, successLog } from "../../../../utils/fireLog";
import validate from "../../../../utils/validation_wrapper";
import { getUserDetails, image_UploadDoctor } from "../../../../actions/profile";
import { showModal } from "../../../../actions/modal";
import TopBar from "../componetns/TopBar";
import http from '../../../../services/http';
import { validateUserProfile } from "../componetns/functions/validateUser";
import SaveCancelFooter from "../../../../components/SaveCancelFooter";
import { errorPayload } from "../../../../utils/helper";

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: '',
            passwordError: null,
            confirmPassword: '',
            confirmPasswordError: null,
            isFocused: false,
            secureTextEntry: true,
            disableSave: true,
        };
    }

    componentDidMount() {
        successLog("EditDoctorProfile screen loaded")
    }


    isValid() {
        let { password, confirmPassword } = this.state;
        if (password === confirmPassword && confirmPassword.length >= 5) {
            this.setState({ disableSave: false })
        } else {
            this.setState({ disableSave: true })
        }
    }

    _onBlur(key) {
        this.setState({
            [`${key}Error`]: validate(key, this.state[key]),
        });
    }

    _onSave = () => {
        methodLog("_onSave")
        try {
            http.put("api/user/changePassword", { password: this.state.password })
                .then((res) => {
                    console.log(res);
                    if (res.status === 200) {
                        const payload = {
                            text: "Password changed successfully",
                            subText: "",
                            iconName: "checkcircleo",
                            modalVisible: true,
                        };
                        this.props.showModal(payload);
                        this.props.navigation.goBack();
                    }
                })
                .catch((err) => {
                    this.props.showModal(errorPayload('Something went wrong', err));
                });
        } catch (e) {
            recordError(e)
        }
    };

    _onChangeValue(key, value) {
        this.setState({ [key]: value }, () => {
            this.isValid()
        });
    }

    updateSecureTextEntry = () => {
        this.setState({
            secureTextEntry: !this.state.secureTextEntry,
        });
    };

    render() {
        const { imageUrl, experience, email, fullName, gender, qualification, dob } = validateUserProfile(this.props.userProfile)
        return (
            <Container>
                <SafeAreaView style={styles.header}>
                    <TouchableOpacity
                        transparent
                        onPress={() => this.props.navigation.goBack()}>
                        <Icon
                            name="arrowleft"
                            type="AntDesign"
                            style={styles.headerBackIcon} />
                    </TouchableOpacity>

                </SafeAreaView>

                <Content>
                    <TopBar
                        cameraButton={false}
                        imgUrl={imageUrl}
                        fullName={fullName}
                        email={email} />

                    <Container style={{ backgroundColor: '#F9F9F9' }}>
                        <Form style={styles.formItem}>


                            <Item floatingLabel error={this.state.passwordError !== null}>
                                <Label>Password</Label>
                                <Input
                                    value={this.state.confirmPassword}
                                    onChangeText={(value) =>
                                        this._onChangeValue("confirmPassword", value)
                                    }
                                    style={{ margin: 4 }}
                                    onBlur={() => this._onBlur("password")}
                                    secureTextEntry={this.state.secureTextEntry ? true : false}
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                />
                                {this.state.secureTextEntry ? (
                                    <Icon
                                        name="eye-off"
                                        type="Feather"
                                        style={{ fontSize: 20, color: "#A0A9BE", }}
                                        onPress={this.updateSecureTextEntry}
                                    />) : (
                                        <Icon
                                            name="eye"
                                            type="Feather"
                                            style={{ color: "#1B80F3", fontSize: 20 }}
                                            onPress={this.updateSecureTextEntry}
                                        />
                                    )}
                            </Item>


                            <Item floatingLabel error={this.state.passwordError !== null}>
                                <Label>Confirm Password</Label>
                                <Input
                                    value={this.state.password}
                                    onChangeText={(value) =>
                                        this._onChangeValue("password", value)
                                    }
                                    onBlur={() => this._onBlur("password")}
                                    style={{ margin: 4 }}
                                    secureTextEntry={this.state.secureTextEntry ? true : false}
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                />
                                {this.state.secureTextEntry ? (<Icon
                                    name="eye-off"
                                    type="Feather"
                                    style={{ fontSize: 20, color: "#A0A9BE", }}
                                    onPress={this.updateSecureTextEntry} />) : (
                                        <Icon
                                            name="eye"
                                            type="Feather"
                                            style={{ color: "#1B80F3", fontSize: 20 }}
                                            onPress={this.updateSecureTextEntry}
                                        />
                                    )}
                            </Item>

                        </Form>
                    </Container>

                </Content>

                <SaveCancelFooter
                    disableSave={this.state.disableSave}
                    onSaveClick={() => this._onSave()}
                    onCancelClick={() => this.props.navigation.goBack()}
                />
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({ getUserDetails, image_UploadDoctor, showModal }, dispatch);

const mapStateToProps = (state) => ({
    userProfile: state.profile.userProfile,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangePassword);
