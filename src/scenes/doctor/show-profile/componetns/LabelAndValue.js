import {StyleSheet, Text, View} from "react-native";
import React from "react";
import {FONT_FAMILY_SF_PRO_REGULAR, FONT_SIZE_17} from "../../../../styles/typography";

const LabelAndValue = ({label, value}) => {
    return <View style={{
        margin:20,
    }}>
        <Text style={styles.labelStyle}>{label}</Text>
        <Text style={styles.valueStyle}>{value}</Text>
    </View>
}
export default LabelAndValue

const styles = StyleSheet.create({
    labelStyle: {
        color :'#A0A9BE',
        fontSize :FONT_SIZE_17,
        fontFamily:FONT_FAMILY_SF_PRO_REGULAR
    },
    valueStyle: {
        color :'#041A32',
        fontSize :FONT_SIZE_17,
        fontFamily:FONT_FAMILY_SF_PRO_REGULAR
    }
})
