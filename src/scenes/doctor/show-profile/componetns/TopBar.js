import { View } from "native-base";
import { Image, StyleSheet, Text, TouchableOpacity } from "react-native";
import Images from "../../../../assets/images";
import React from "react";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_16, FONT_SIZE_18 } from "../../../../styles/typography";

const TopBar = ({ imgUrl, fullName = "...", email = '..', onPress, handleChoosePhoto, cameraButton = true }) => {
    return <View style={styles.inHeader}>
        <View style={styles.section1}>
            <Image
                source={imgUrl}
                style={styles.userImage}
            />

            {cameraButton ? <View
                style={styles.cameraBtn}
                onTouchStart={() => handleChoosePhoto()}>
                <Image source={Images.camera} />
            </View> : null}

        </View>

        <View style={styles.section2}>
            <Text style={styles.title}>{fullName}</Text>
            <Text style={styles.subTitle} note>{email}</Text>
        </View>

        {onPress ?
            <TouchableOpacity style={{ marginRight: 28, }} onPress={onPress}>
                <Image style={{ width: 21, height: 21, color: '#041A32' }} source={Images.pencil_edit} />
            </TouchableOpacity> : null}

    </View>

}
export default TopBar;
const styles = StyleSheet.create({
    inHeader: {
        flexDirection: "row",
        backgroundColor: "#FFF",
        height: hp("15%"),
        paddingLeft: wp("5%"),
        alignItems: "center",
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 5
    },
    userImage: { width: 67, height: 67, borderRadius: 33, },
    editIcon: { color: "#041A32", fontSize: 30, },
    section2: {
        flex: 1,
        marginLeft: 20,
    },
    cameraBtn: {
        position: "absolute",
        bottom: -5,
        right: 5,
    },
    title: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_18,
        color: '#041A32'
    },
    subTitle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_16,
        color: '#041A32',
        opacity: 0.5,
    },


})
