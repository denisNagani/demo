import {ifNotValid, ifValid} from "../../../../../utils/ifNotValid";
import Images from "../../../../../assets/images";

export const validateUserProfile = (_userProfile) => {
    let user = ifValid(_userProfile) && ifValid(_userProfile.userId) ? _userProfile.userId : {};
    let userProfile = ifValid(_userProfile) ? _userProfile : {};

    let imageUrl = user.imageUrl;
    if (ifNotValid(imageUrl)) imageUrl = Images.user
    else imageUrl = {uri: imageUrl}

    let experience = userProfile.experience
    ifNotValid(experience) ? experience = "..." : experience

    let email = user.email
    ifNotValid(email) ? email = "" : email

    let fullName = user.fullName
    ifNotValid(fullName) ? fullName = "" : fullName

    let gender = user.gender
    ifNotValid(gender) ? gender = "" : gender

    let qualification = userProfile.education
    ifNotValid(qualification) ? qualification = "" : qualification

    let dob = user.dob
    ifNotValid(dob) ? dob = "" : dob;

    return {imageUrl, experience, email, fullName, gender, qualification, dob}

}
