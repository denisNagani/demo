import React, { Component } from "react";
import { Image, SafeAreaView, TextInput, TouchableOpacity, View } from "react-native";
import { Container, Content, Form, Icon, Label, } from "native-base";
import styles from "../style";
import { launchImageLibrary } from "react-native-image-picker";
import { accessToken } from "../../../../services/auth";
import { showModal } from "../../../../actions/modal";
import { bindActionCreators } from "redux";
import { getUserDetails, image_UploadDoctor, updateDoctorProfile } from "../../../../actions/profile";
import { connect } from "react-redux";
import { methodLog, recordError, successLog } from "../../../../utils/fireLog";
import SaveCancelFooter from "../../../../components/SaveCancelFooter";
import TopBar from "../componetns/TopBar";
import InputCommon from "../../../../components/InputCommon";
import moment from "moment";
import DatePickerModal from "../../../../components/datePickerModal";
import Images from "../../../../assets/images";
import validate from "../../../../utils/validation_wrapper";
import { validateUserProfile } from "../componetns/functions/validateUser";
import { ifValid } from "../../../../utils/ifNotValid";
import { strings } from "../../../../utils/translation";
import { requestPermissionStorage } from "../../../../services/requestPermissions";

class EditDoctorProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photo: null,
            fullName: '',
            email: '',
            imageUrl: '',
            dob: '',
            gender: '',
            experience: '',
            qualification: '',
            isPickerVisibleDob: false,
            fullNameError: null,
            mobileNoError: null,
            isDatePickerVisible: false,
            response: ''
        };
    }

    componentDidMount() {
        successLog("EditDoctorProfile screen loaded");
        this.getUserData();
    }

    getUserData() {
        const { imageUrl, experience, email, fullName, gender, qualification, dob } = validateUserProfile(this.props.userProfile)
        this.setState({ fullName, email, gender, qualification, dob: dob, experience, imageUrl })
    }


    createFormData = (photo) => {
        methodLog("createFormData")
        let imgName = "";
        try {
            if (photo.fileName === undefined) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else if (photo.fileName === null) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else {
                imgName = photo.fileName;
            }
            const data = new FormData();
            data.append("file", {
                name: Platform.OS === 'android' ? photo.fileName : imgName,
                type: photo.type,
                uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
            });
            return data;
        } catch (e) {
            recordError(e)
        }
    };


    handleUploadPhoto = async (photo, payload) => {
        methodLog("handleUploadPhoto")
        let token = await accessToken().then((token) => {
            return token;
        });
        this.props.image_UploadDoctor(this.createFormData(photo), token, payload, this.props.navigation);
    };

    handleChoosePhoto = () => {
        methodLog("handleChoosePhoto")
        try {
            requestPermissionStorage()
                .then((status) => {
                    if (status == true) {
                        const options = {
                            noData: true,
                        };
                        launchImageLibrary(options, (results) => {
                            if (results?.assets?.length > 0) {
                                let response = {
                                    uri: results?.assets[0]?.uri,
                                    fileName: results?.assets[0]?.fileName,
                                    fileSize: results?.assets[0]?.fileSize,
                                    type: results?.assets[0]?.type,
                                }
                                if (response?.uri) {
                                    this.setState({ photo: response });
                                    this.setState({
                                        imageUrl: { uri: response.uri },
                                        response: response,
                                    });
                                    this.state.response = response
                                }
                            }

                        });
                    }
                })
        } catch (e) {
            recordError(e)
        }
    };

    isValid = () => {
        const fullNameError = validate("fullName", this.state.fullName);
        // const mobileNoError = validate("mobileNo", this.state.mobileNo);
        this.setState(fullNameError);
        return !fullNameError;
    };

    _onSave = () => {
        methodLog("_onSave")
        try {
            if (this.isValid()) {
                const { fullName, dob, email, mobileNo, experience, qualification, gender } = this.state;
                let payload = {
                    fullName: fullName,
                    experience: experience.toString(),
                    education: qualification,
                    dob: moment(dob).format("YYYY-MM-DD"),
                };
                if (this.state.response === '') {
                    this.props.updateDoctorProfile(payload, this.props.navigation)
                } else {
                    this.handleUploadPhoto(this.state.response, payload)
                }
            }
        } catch (e) {
            recordError(e)
        }
    };

    _onChangeValue(key, value) {
        this.setState({ [key]: value });
    }

    updateSecureTextEntry = () => {
        this.setState({
            secureTextEntry: !this.state.secureTextEntry,
        });
    };

    setSelectedDate = () => {
        this.setState({ isPickerVisibleDob: true });
    };

    handleAppointmentConfirm = (date) => {
        successLog(date)
        try {
            this.setState({
                birthDate: date,
                dob: date,
                isPickerVisibleDob: !this.state.isPickerVisibleDob
            }, () => {
            });
        } catch (e) {
            recordError(e)
        }
    };

    render() {
        const { photo, imageUrl, email, dob, isPickerVisibleDob, fullName, experience, qualification } = this.state;
        return (
            <Container style={{ backgroundColor: '#F9F9F9' }}>
                <SafeAreaView style={styles.header}>
                    <TouchableOpacity
                        transparent
                        onPress={() => this.props.navigation.goBack()}>
                        <Icon
                            name="arrowleft"
                            type="AntDesign"
                            style={styles.headerBackIcon}
                        />
                    </TouchableOpacity>
                </SafeAreaView>

                <Content style={{ backgroundColor: '#F9F9F9' }}>
                    <TopBar
                        imgUrl={imageUrl}
                        fullName={fullName}
                        email={email}
                        handleChoosePhoto={() => this.handleChoosePhoto()}
                    />
                    <Form style={{ marginHorizontal: 20, }}>
                        <InputCommon
                            title={strings.full_name}
                            onChangeText={(value) => this._onChangeValue("fullName", value)}
                            value={fullName}
                            titleStyle={styles.titleStyle}
                            style={{ marginTop: 18 }}
                        />

                        <InputCommon
                            title={strings.experience}
                            onChangeText={(value) => this._onChangeValue("experience", value)}
                            value={ifValid(experience) ? experience.toString() : ''}
                            keyboardType="numeric"
                            returnKeyType="done"
                            titleStyle={styles.titleStyle}
                            style={{ marginTop: 18 }}
                        />

                        <Label style={[styles.titleStyle, { marginTop: 18 }]}>{strings.dob}</Label>
                        <View style={styles.boxStyleAmount}>
                            <TextInput
                                style={{ padding: 5 }}
                                value={dob === "" ? "" : moment(dob).format("DD MMMM YYYY")}
                                editable={false}
                            />
                            <TouchableOpacity disabled={true} onPress={() => {
                                // this.setSelectedDate()
                            }}>
                                <Image source={Images.calendarGray} style={{ width: 16, height: 18, marginRight: 5, }} />
                            </TouchableOpacity>
                        </View>

                        <InputCommon
                            style={{ marginTop: 18 }}
                            onChangeText={(value) => this._onChangeValue("qualification", value)}
                            value={qualification}
                            title={strings.qualification}
                            titleStyle={styles.titleStyle}
                        />


                        <DatePickerModal
                            isVisible={isPickerVisibleDob}
                            onConfirm={(date) => this.handleAppointmentConfirm(date)}
                            onCancel={() => {
                            }}
                            maximumDate={new Date()}
                        />
                    </Form>
                </Content>
                <SaveCancelFooter
                    onSaveClick={() => this._onSave()}
                    onCancelClick={() => this.props.navigation.goBack()}
                />
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({ getUserDetails, image_UploadDoctor, showModal, updateDoctorProfile, }, dispatch);

const mapStateToProps = (state) => ({
    userProfile: state.profile.userProfile,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditDoctorProfile);
