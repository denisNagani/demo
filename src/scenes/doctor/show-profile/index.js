import React, { Component } from "react";
import { SafeAreaView, Text, TouchableOpacity, View } from "react-native";
import { Container, Content, Form, Icon } from "native-base";
import styles from "./style";
import { showModal } from "../../../actions/modal";
import { bindActionCreators } from "redux";
import { getUserDetails, image_UploadDoctor } from "../../../actions/profile";
import { connect } from "react-redux";
import { successLog } from "../../../utils/fireLog";
import TopBar from "./componetns/TopBar";
import LabelAndValue from "./componetns/LabelAndValue";
import moment from "moment";
import { validateUserProfile } from "./componetns/functions/validateUser";
import { FONT_FAMILY_SF_PRO_REGULAR, FONT_SIZE_17 } from "../../../styles/typography";
import { strings } from "../../../utils/translation";

class ShowDoctorProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            photo: null,
            fullName: "",
            email: "",
            imageUrl: '',
            roomName: "",
            birthDate: "",
            isPickerVisibleDob: false,
        };
    }

    componentDidMount() {
        this.props.getUserDetails()
        successLog("EditDoctorProfile screen loaded")
    }


    render() {
        const { imageUrl, experience, email, fullName, gender, qualification, dob } = validateUserProfile(this.props.userProfile)
        return (
            <Container style={{ backgroundColor: '#F9F9F9' }}>
                <SafeAreaView style={styles.header}>
                    <TouchableOpacity
                        transparent
                        onPress={() => this.props.navigation.goBack()}
                    >
                        <Icon
                            name="arrowleft"
                            type="AntDesign"
                            style={styles.headerBackIcon}
                        />
                    </TouchableOpacity>
                </SafeAreaView>

                <Content>
                    <TopBar
                        imgUrl={imageUrl}
                        cameraButton={false}
                        fullName={fullName}
                        email={email}
                        onPress={() => {
                            successLog("hi")
                            this.props.navigation.navigate('EditDoctorProfile')
                        }} />
                    {/*<AddPrescriptionDialog/>*/}

                    <Form>
                        <LabelAndValue
                            label={strings.full_name}
                            value={fullName} />

                        <LabelAndValue
                            label={strings.dob}
                            value={moment(dob).format("DD MMM YYYY")} />

                        <LabelAndValue
                            label={strings.experience}
                            value={experience} />

                        <LabelAndValue
                            label={strings.qualification}
                            value={qualification} />

                        <View style={{ marginHorizontal: 20, marginVertical: 10 }}>
                            <TouchableOpacity onPress={() => this.props.navigation.navigate("ChangePassword")}>
                                <Text style={{
                                    color: 'blue',
                                    fontSize: FONT_SIZE_17,
                                    fontFamily: FONT_FAMILY_SF_PRO_REGULAR
                                }}>Change Password</Text>
                            </TouchableOpacity>
                        </View>

                    </Form>
                </Content>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({ getUserDetails, image_UploadDoctor, showModal }, dispatch);

const mapStateToProps = (state) => ({
    userProfile: state.profile.userProfile,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ShowDoctorProfile);
