import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_15 } from "../../../styles/typography";

export default (styles = StyleSheet.create({
    header: {
        backgroundColor: "#FFFFFF",
        flex: 0.1,
    },
    inHeader: {
        flexDirection: "row",
        backgroundColor: "#038BEF",
        height: hp("13%"),
        paddingLeft: wp("5%"),
        alignItems: "center",
    },
    headerBackIcon: {
        fontSize: hp("3.5%"),
        color: "#041A32",
        marginTop: wp("5%"),
        marginLeft: wp("7%"),
    },
    headerTitle: {
        fontSize: 17,
        fontWeight: "bold",
    },
    formItem: {
        marginHorizontal: 15
    },
    boxStyleAmount: {
        flexDirection: 'row',
        marginTop: 6,
        paddingRight: 8,
        alignItems: 'center', borderRadius: 9, borderColor: '#B6BECB', borderWidth: 0.6,
        justifyContent: 'space-between'
    },
    titleStyle: { color: '#A3ACC0', fontSize: FONT_SIZE_15, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }
}));
