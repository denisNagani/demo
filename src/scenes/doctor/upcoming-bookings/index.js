import React, {Component} from "react";
import {FlatList, Image, Linking, SafeAreaView, Text, View,} from "react-native";
import {ListItem} from "react-native-elements";
import httpServices from "../../../services/http";
import {Button, Container, Content, Icon, Segment,} from "native-base";
import styles from "./style";
import {intake_url} from "../../../config";
import Images from "../../../assets/images/index";
import Moment from "moment";
import moment from "moment";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {showModal} from "../../../actions/modal";
import {recordError, successLog} from "../../../utils/fireLog";

class DoctorUpcomingBookingsScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bookings: [],
            isLoading: false,
        };
    }

    componentDidMount() {
        successLog("DoctorUpcomingBookingsScreen screen loaded")
        try {
            let date = moment().format("YYYY-MM-DD")
            this.setState({
                isLoading: true,
            });
            httpServices
                .get(`api/upcoming/appointments?date=${date}`)
                .then((response) => {
                    this.setState({
                        isLoading: false,
                    });
                    if (response.status === 400) {
                        const payload = {
                            text: response.message,
                            subText: "",
                            iconName: "closecircleo",
                            modalVisible: true,
                            iconColor: "#C70315",
                        };
                        this.props.showModal(payload);
                    } else if (response.status === 200) {
                        // set bookings here
                        this.setState({
                            bookings: response.data,
                        });
                    }
                })
                .catch((err) => {
                    const payload = {
                        text: "Something went wrong",
                        subText: "",
                        iconName: "closecircleo",
                        modalVisible: true,
                        iconColor: "#C70315",
                    };
                    this.props.showModal(payload);
                    this.setState({
                        isLoading: false,
                    });
                });
        } catch (e) {
            recordError(e)
        }
    }

    keyExtractor = (item) => item._id.toString();

    render() {
        let noBookings;
        if (!this.state.isLoading && this.state.bookings.length <= 0) {
            noBookings = (
                <View style={styles.noBookingsBlock}>
                    <Image source={Images.no_bookings} style={styles.noBookingsImage}/>
                    <Text style={styles.noBookingsTitle}>No Upcoming Appointments!</Text>
                    <Text style={styles.noBookingsSubTitle}>
                        Currently there are no upcoming appointments
                    </Text>
                    <Button
                        block
                        danger
                        style={styles.noBookingsBtn}
                        onPress={() => {
                            this.props.navigation.navigate("DashboardScreen");
                        }}
                    >
                        <Text style={styles.noBookingsBtnText}>GO TO DASHBOARD</Text>
                    </Button>
                </View>
            );
        }
        let loader;
        if (this.state.isLoading) {
            loader = <Text style={{textAlign: "center"}}>Loading...</Text>;
        }
        const renderItem = ({item}) => (
            <ListItem
                title={item.patientId.fullName}
                subtitle={
                    <View>
                        <Text style={styles.subTitle}>
                            <Icon style={[styles.subIcon]} type="AntDesign" name="calendar"/>{" "}
                            {Moment(item.utcDate).format("dddd DD MMM")}
                            {"   "}
                            <Icon style={[styles.subIcon]} type="Feather" name="clock"/>{" "}
                            {item.utcDate && moment(item.utcDate).format("hh:mm A")}
                        </Text>
                    </View>
                }
                leftAvatar={
                    item.patientId.imageUrl !== "" && item.patientId.imageUrl !== null
                        ? {source: {uri: item.patientId.imageUrl}}
                        : {source: Images.user}
                }
                bottomDivider
                rightElement={
                    <View>
                        <Button
                            danger
                            small
                            style={styles.intakeFormBtn}
                            onPress={() =>
                                Linking.openURL(`${intake_url}pdf/${item.patientId._id}.pdf`)
                            }
                        >
                            <Text style={styles.intakeFormBtnText} numberOfLines={1}>
                                Intake Form
                            </Text>
                        </Button>
                    </View>
                }
            />
        );
        return (
            <Container>
                <SafeAreaView style={styles.header}>
                    <Icon
                        name="arrowleft"
                        type="AntDesign"
                        style={styles.headerBackIcon}
                        onPress={() => this.props.navigation.navigate("DashboardScreen")}
                    />
                    <Text style={styles.heading}>My Appointments</Text>
                </SafeAreaView>
                <View>
                    <Segment style={styles.segment}>
                        <Button
                            first
                            active
                            style={[styles.segmentButton, styles.btnActive]}
                        >
                            <Text style={styles.segmentTextActive}>Upcoming</Text>
                        </Button>
                        <Button
                            onPress={() =>
                                this.props.navigation.navigate("DoctorPastBookingsScreen")
                            }
                            last
                            style={[styles.segmentButton, styles.btnInactive]}
                        >
                            <Text style={styles.segmentTextInActive}>Past</Text>
                        </Button>
                    </Segment>
                </View>

                <Content>
                    <View style={styles.listView}>
                        {noBookings}
                        {loader}
                        <FlatList
                            scrollEnabled
                            keyExtractor={this.keyExtractor}
                            data={this.state.bookings}
                            renderItem={renderItem}
                        />
                    </View>
                </Content>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({showModal}, dispatch);
const mapStateToProps = (state) => ({});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DoctorUpcomingBookingsScreen);
