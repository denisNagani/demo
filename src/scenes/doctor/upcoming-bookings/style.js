import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {FONT_SIZE_12, FONT_SIZE_13, FONT_SIZE_15} from "../../../styles/typography";

export default (styles = StyleSheet.create({
	header: {
		backgroundColor: "#1E2081",
		height: hp("13%"),
		flexDirection: "row",
		alignItems: "center",
	},
	headerBackIcon: {
		flex: 1,
		fontSize: hp("3.5%"),
		color: "#fff",
		marginLeft: wp("5%"),
	},
	heading: {
		flex: 8,
		color: "#fff",
		fontSize: hp("2.9%"),
	},
	segment: {
		backgroundColor: "white",
		marginVertical: hp("3%"),
	},

	segmentButton: {
		height: hp("6.5%"),
		width: wp("45%"),
		justifyContent: "center",
		marginTop: 20,
	},

	btnActive: {
		backgroundColor: "#1B80F3",
		fontWeight: "bold",
		color: "white",
		borderBottomLeftRadius: 10,
		borderTopLeftRadius: 10,
	},

	btnInactive: {
		backgroundColor: "#A0A9BE1A",
		borderBottomRightRadius: 10,
		borderTopRightRadius: 10,
	},

	segmentTextActive: {
		fontWeight: "bold",
		fontSize: hp("2%"),
		color: "#fff",
	},
	segmentTextInActive: {
		fontSize: hp("2%"),
		color: "black",
		fontWeight: "bold",
	},
	listView: { marginTop: hp("3%"), flex: 1 },
	intakeFormBtn: {
		width: wp("21%"),
		justifyContent: "center",
		borderRadius: 5,
	},
	intakeFormBtnText: {
		fontSize: FONT_SIZE_12,
		color: "#fff",
	},
	subIcon: {
		fontSize: hp("1.6%"),
		color: "#A0A9BE",
	},
	subTitle: {
		fontSize : FONT_SIZE_12,
		color: "#A0A9BE",
	},
	noBookingsBlock: {
		justifyContent: "center",
		alignItems: "center",
	},
	noBookingsImage: {
		margin: 30,
		height: 200,
		width: 200,
	},
	noBookingsTitle: {
		fontSize: 20,
		fontWeight: "bold",
	},
	noBookingsSubTitle: {
		fontSize: FONT_SIZE_15,
		fontWeight: "normal",
		color: "gray",
		textAlign:'center'
	},
	noBookingsBtnText: {
		color: "#fff",
	},
	noBookingsBtn: {
		margin: 20,
	},
}));
