import {DOCTOR_SLOT} from "../../../utils/constant";


export const setUpcomingPastActive = (payload) => {
    return {
        type: 'UPCOMING_PAST_ACTIVE',
        payload: payload,
    };
};
