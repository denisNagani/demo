import React, { Component } from "react";
import { FlatList, Image, StyleSheet, Text, TouchableOpacity, View, } from "react-native";
import { Container, Icon, Item } from "native-base";
import styles from "./style";
import moment from "moment";
import Images from "../../../assets/images";
import { bindActionCreators } from "redux";
import {
    getPastBookings,
    getUpcomingBookings,
    setCallHeader,
    setSelectedAppointment,
    updateAppointmentPatient,
} from "../../../actions/bookings";
import { connect } from "react-redux";
import database from "@react-native-firebase/database";
import { hide, show } from "../../../utils/loader/action";
import { checkUserProfile } from "../../../utils/helper";
import { showModal } from "../../../actions/modal";
import { methodLog, successLog } from "../../../utils/fireLog";
import HeaderSehet from "../../../components/header-common";
import SegmentCustom from "./components/SegmentCustom";
import { ifNotValid, ifValid, isValid } from "../../../utils/ifNotValid";
import { setUpcomingPastActive } from "./action";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_13, FONT_SIZE_16 } from "../../../styles/typography";
import { setAppointmentIdForAddMedicine, setUpcomingPastDetailView } from "../../patient/appointment-detail/action";
import { setSelectedPatient } from "../perscription-form/components/action";
import { strings } from "../../../utils/translation";
// import PowerTranslator from "react-native-power-translator";
import PowerTranslator from "../../../components/PowerTranslator";
import { rowStyle } from "../../../styles/commonStyle";

class UpcomingPastBookingsDoctor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            connect: false,
            isLoading: false,
            modal: false,
            id: null,
            roomName: null,
            doctor: null,
            appointmentDate: null,
            activeButton: 'PAST'
        };
    }

    componentDidMount() {
        successLog("UpcomingBookings screen loaded")
        this.props.getUpcomingBookings();
        this.props.getPastBookings();
    }


    addUserInList = async (id) => {
        methodLog("addUserInList")
        let { fullName, image } = checkUserProfile(this.props.userProfile);
        console.log(fullName + image);
        await database()
            .ref(`/users/${id}`)
            .update({ patient: { fullName: fullName, image: image } })
            .then((obj) => {
            });
    };

    onModal = () => {
        methodLog("onModal")
        this.setState({ modal: !this.state.modal });
    };

    redirect = (roomName, id) => {
        methodLog("redirect")
        this.props.navigation.navigate("JoinCall", {
            channel: roomName,
            user: "PATIENT_DIRECT",
            bookingId: id,
        });
    };


    renderItem = ({ item }) => {
        // console.log(JSON.stringify(item));
        // if (ifNotValid(item))
        //     return <View/>
        let patientId = ifNotValid(item.patientId) ? {} : item.patientId;

        // if (ifNotValid(doctorId))
        //     return <View/>

        let fullName = patientId.fullName
        if (ifNotValid(fullName)) fullName = '-'

        let hindiFullName = patientId.hindiFullName
        if (ifNotValid(hindiFullName)) hindiFullName = fullName

        let name = isValid(item?.intakeInfo?.name) ? item?.intakeInfo?.name : patientId?.fullName

        let imageUrl = patientId.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = Images.user
        else imageUrl = { uri: imageUrl }

        let isInvalid = false;
        let day = new Date()
        if (ifValid(day)) day = moment(item.utcDate).format("dddd")
        else isInvalid = true;

        let date = new Date()
        if (ifValid(date)) date = moment(item.utcDate).format("D MMM")
        else isInvalid = true

        let time = new Date()
        if (ifValid(time)) time = moment(item.utcDate).format("hh:mm A")
        else isInvalid = true

        let callType = item?.callType
        let appointmentType = callType == "Online" ? "Online" : "Clinic Visit"

        return (
            <Item
                style={{ flex: 1, backgroundColor: '#F9F9F9' }}
                onPress={() => {
                    this.props.setUpcomingPastDetailView('UPCOMING')
                    this.props.navigation.navigate("AppointedDetailsAdded", { data: item })
                }}
            >
                <View style={{ flex: 1 }}>
                    <View style={{ flexDirection: 'row', marginHorizontal: 10, marginVertical: 10, padding: 2, }}>
                        <Image source={imageUrl} style={{ width: 57, height: 57, borderRadius: 28.5 }} />

                        <View style={innerStyle.dateTimeContainer}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                {/* <Text style={innerStyle.nameStyle}>{fullName}</Text> */}

                                <PowerTranslator
                                    style={[innerStyle.nameStyle, { flex: 1 }]}
                                    hinText={name}
                                    enText={name}
                                />

                                <View style={{
                                    backgroundColor: '#048BEF',
                                    paddingHorizontal: 8,
                                    paddingVertical: 5,
                                    borderRadius: 5,
                                }}>
                                    <Text style={[styles.patientCardTextTime, { color: '#FFF' }]}>
                                        {appointmentType}
                                    </Text>
                                </View>

                                {/*<TouchableOpacity*/}
                                {/*    onPress={() => Linking.openURL(`${intake_url}pdf/${item.patientId._id}.pdf`)}*/}
                                {/*    style={[common.intakeBtn, {backgroundColor: "#C70315"}]}>*/}
                                {/*    <Text style={[common.intakeBtnText, {color: '#fff', paddingHorizontal: 4}]}>Intake Form</Text>*/}
                                {/*</TouchableOpacity>*/}

                            </View>

                            <View style={{ marginVertical: 3 }}>
                                {isInvalid === true ?
                                    <Text>...</Text> :
                                    <Text style={innerStyle.subTitle}>
                                        <Icon style={[innerStyle.subIcon]} type="AntDesign"
                                            name="calendar" /> {day}, {date}, <Icon style={innerStyle.subIcon}
                                                type="Feather"
                                                name="clock" /> {time}
                                    </Text>}

                                <TouchableOpacity
                                    style={innerStyle.hostoryBtnCont}
                                    onPress={() => {
                                        this.props.navigation.navigate('AppointmentHistory', {
                                            data: item
                                        })
                                    }}>
                                    <Text style={innerStyle.hostoryBtnText}>Show History</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>


                </View>

            </Item>
        );
    };

    renderItemPast = ({ item }) => {
        // if (ifNotValid(item))
        //     return <View/>
        let patientId = ifNotValid(item.patientId) ? {} : item.patientId;

        // if (ifNotValid(doctorId))
        //     return <View/>

        let fullName = patientId.fullName
        if (ifNotValid(fullName)) fullName = '-'

        let hindiFullName = patientId.hindiFullName
        if (ifNotValid(hindiFullName)) hindiFullName = fullName

        let name = isValid(item?.intakeInfo?.name) ? item?.intakeInfo?.name : patientId?.fullName

        let imageUrl = patientId.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = Images.user
        else imageUrl = { uri: imageUrl }

        let day = item.utcDate
        ifValid(day) ? day = moment(item.utcDate).format("dddd") : '...'

        let date = item.utcDate
        ifValid(date) ? date = moment(item.utcDate).format("D MMM") : '...'

        let time = item.utcDate
        ifValid(time) ? time = moment(item.utcDate).format("hh:mm A") : '...'

        let prescriptionType = "plus"
        if (ifValid(prescriptionType)) {
            if (prescriptionType === 'file')
                prescriptionType = 'file';
            else if (prescriptionType === 'plus')
                prescriptionType = 'plus';
            else prescriptionType = 'NA'
        } else {
            prescriptionType = 'NA'
        }

        let pdfUploaded = isValid(item.prescriptionId)
            ? item.prescriptionId?.imageURL?.length > 0 || isValid(item.prescriptionId?.pdfUploaded)
            : false

        let callType = item?.callType
        let appointmentType = callType == "Online" ? "Online" : "Clinic Visit"
        return (
            <Item
                style={{ flex: 1, }}
                onPress={() => {
                    this.props.setUpcomingPastDetailView('PAST');
                    this.props.setAppointmentIdForAddMedicine(item._id);
                    this.props.navigation.navigate("AppointedDetailsAdded", { data: item })
                }}
            >
                <View style={{ flexDirection: 'row', marginHorizontal: 10, marginVertical: 10, padding: 2, }}>
                    <Image source={imageUrl} style={{ width: 57, height: 57, borderRadius: 28.5 }} />
                    <View style={innerStyle.dateTimeContainer}>
                        {/* <Text style={innerStyle.nameStyle}>{fullName}</Text> */}
                        <View style={rowStyle}>
                            <PowerTranslator
                                style={[innerStyle.nameStyle, { flex: 1 }]}
                                hinText={name}
                                enText={name}
                            />
                            <View style={{
                                backgroundColor: '#048BEF',
                                paddingHorizontal: 8,
                                paddingVertical: 5,
                                borderRadius: 5,
                                marginBottom: 5
                            }}>
                                <Text style={[styles.patientCardTextTime, { color: '#FFF' }]}>
                                    {appointmentType}
                                </Text>
                            </View>
                        </View>
                        <Text style={innerStyle.subTitle}>
                            <Icon style={[innerStyle.subIcon]} type="AntDesign"
                                name="calendar" /> {day}, {date}, <Icon style={innerStyle.subIcon}
                                    type="Feather"
                                    name="clock" /> {time}
                        </Text>
                        <TouchableOpacity disabled={pdfUploaded} style={{ marginTop: 5, }}
                            onPress={() => {
                                this.props.setAppointmentIdForAddMedicine(item._id);
                                this.props.setSelectedPatient(item)
                                this.props.navigation.navigate("PrescriptionForm")
                            }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={innerStyle.addPres}>{
                                    pdfUploaded != true ?
                                        `${strings.Add_Prescription}` : `${strings.Prescription_Added}`}

                                </Text>
                                <Image source={pdfUploaded != true ? Images.plusRed : Images.pinRed}
                                    style={innerStyle.presIconStyle} />
                            </View>
                        </TouchableOpacity>


                    </View>
                </View>
            </Item>
        );
    };

    noBookings = () => {
        return (
            <View style={styles.noBookingsBlock}>
                <Image source={Images.no_bookings} style={styles.noBookingsImage} />
                <Text style={styles.noBookingsTitle}>{strings.No_appointments}</Text>
                <Text style={styles.noBookingsSubTitle}>
                    {strings.No_appointments2}
                </Text>
            </View>
        );
    };

    render() {
        let bookings = false;
        let upcomingBookings = this.props.upcomingBookings;
        ifValid(upcomingBookings) && upcomingBookings.length > 0 ? bookings = true : bookings = true
        let { activeButton } = this.state;
        return (
            <Container style={{ backgroundColor: '#F9F9F9' }}>
                <HeaderSehet headerText={strings.my_appointments} navigation={this.props.navigation} />

                <SegmentCustom />
                {bookings ? (
                    <View style={{ flex: 1 }}>
                        <FlatList
                            scrollEnabled
                            keyExtractor={(item, index) => index.toString()}
                            data={this.props.activeUpcomingOrPast === "UPCOMING" ? this.props.upcomingBookings : this.props.pastBookings}
                            renderItem={this.props.activeUpcomingOrPast === "UPCOMING" ? this.renderItem : this.renderItemPast}
                            ListEmptyComponent={this.noBookings}
                        // renderItem={this.renderItem}
                        />
                    </View>
                ) : (
                    this.noBookings()
                )}

            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getUpcomingBookings,
            getPastBookings, updateAppointmentPatient, setSelectedAppointment,
            show, hide, setUpcomingPastDetailView, setCallHeader, setAppointmentIdForAddMedicine,
            setUpcomingPastActive, showModal, setSelectedPatient,
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    upcomingBookings: state.bookings.upcomingBookings,
    pastBookings: state.bookings.pastBookings,
    userProfile: state.profile.userProfile,
    activeUpcomingOrPast: state.upcomingPast.active,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UpcomingPastBookingsDoctor);

const innerStyle = StyleSheet.create({
    subTitle: {
        fontSize: FONT_SIZE_13,
        color: "#A0A9BE",
    },
    addPres: {
        color: '#038BEF',
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_13,
    },
    presIconStyle: { width: 17, height: 17 },
    subIcon: {
        fontSize: FONT_SIZE_13,
        color: "#A0A9BE",
    },
    nameStyle: {
        fontSize: FONT_SIZE_16,
        paddingBottom: 5,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: "#041A32"
    },
    dateTimeContainer: {
        flex: 1, marginLeft: 10, paddingHorizontal: 5, justifyContent: 'space-around'
    },
    hostoryBtnCont: {
        // backgroundColor: 'red',
        marginTop: 3,
        borderRadius: 8,
        alignSelf: 'flex-start',
    },
    hostoryBtnText: {
        paddingVertical: 3,
        color: '#038BEF',
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: 14,
    },
});
