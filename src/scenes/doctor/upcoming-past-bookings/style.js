import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {FONT_SIZE_12, FONT_SIZE_13, FONT_SIZE_16} from "../../../styles/typography";

export default (styles = StyleSheet.create({
	header: {
		backgroundColor: "#1E2081",
		height: hp("13%"),
		flexDirection: "row",
		alignItems: "center",
	},
	headerBackIcon: {
		flex: 1,
		fontSize: hp("3.5%"),
		color: "#fff",
		marginLeft: wp("5%"),
	},
	heading: {
		flex: 8,
		color: "#fff",
		fontSize: hp("2.9%"),
	},
	segment: {
		backgroundColor: "white",
		marginVertical: hp("3%"),
	},

	segmentButton: {
		height: hp("6.5%"),
		width: wp("45%"),
		justifyContent: "center",
		marginTop: hp("3%"),
	},

	btnActive: {
		backgroundColor: "#FF5E38",
		fontWeight: "bold",
		color: "white",
		borderBottomLeftRadius: 10,
		borderTopLeftRadius: 10,
	},

	btnInactive: {
		backgroundColor: "#F5F6F9",
		borderBottomRightRadius: 10,
		borderTopRightRadius: 10,
	},

	segmentTextActive: {
		fontWeight: "bold",
		fontSize: hp("2%"),
		color: "#fff",
	},
	segmentTextInActive: {
		fontSize: hp("2%"),
		color: "black",
		fontWeight: "bold",
	},
	listView: { marginTop: hp("3%"), flex: 1 },
	joinCallBtn: {
		justifyContent: "center",
		width: wp("17%"),
		borderRadius: 5,
	},
	joinCallBtnText: {
		fontSize: FONT_SIZE_12,
		color: "#fff",
	},
	subIcon: {
		fontSize: FONT_SIZE_13,
		color: "#A0A9BE",
	},
	subTitle: {
		fontSize : FONT_SIZE_13,
		color: "#A0A9BE",
	},
	modal: {
		justifyContent: "center",
		paddingHorizontal: wp("5%"),
		paddingVertical: hp("2%"),
	},
	modalContainer: {
		flex: 1,
	},
	modalTextHeader: {
		fontSize: 24,
		marginTop: 0,
		marginBottom: 0,
		color: "#041A32",
	},
	modalTextSubHeader: {
		fontSize: 14,
		color: "#041A32",
		marginTop: 20,
		marginBottom: 6,
	},
	modalText: {
		fontSize: 13,
		marginTop:8,
		color: "#4E5C76",
	},
	modalTextSub: {
		fontSize: 16,
		marginTop:5,
		color: "#4E5C76",
	},
	modal1Close: {
		alignItems: "center",
		justifyContent: "center",
	},
	modalheader: {
		// backgroundColor: "#1B80F3",
		height: hp("9%"),
		flexDirection: "row",
		alignItems: "center",
	},
	modalheaderBackIcon: {
		flex: 1,
		fontSize: hp("3.5%"),
		color: "#1B80F3",
		marginLeft: wp("5%"),
	},
	modalheading: {
		flex: 8,
		color: "#041A32",
		fontSize: hp("2.2%"),
	},

	noBookingsBlock: {
		flex: 1,
		justifyContent: "center",
		alignItems: "center",
	},
	noBookingsImage: {
		margin: 30,
		height: 200,
		width: 200,
	},
	noBookingsTitle: {
		fontSize: 20,
		fontWeight: "bold",
	},
	noBookingsSubTitle: {
		fontSize: 15,
		fontWeight: "normal",
		color: "gray",
	},
	noBookingsBtnText: {
		color: "#fff",
	},
	noBookingsBtn: {
		margin: 20,
	},
}));
