import React, {Component} from "react";
import {FlatList, Linking, SafeAreaView, Text, TouchableOpacity, View,} from "react-native";
import {Button, Card, CardItem, Container, Content, Icon, Left, Right, Segment, Thumbnail,} from "native-base";

import styles from "./style";
import {bindActionCreators} from "redux";
import {
    checkRoomUrlExist,
    getUpcomingBookingsAdmin,
    getWaitingListAdmin,
    updateAppointment,
    updateAppointmentDirectNurse,
} from "../../../actions/bookingsNurse";
import {connect} from "react-redux";
import {intake_url} from "../../../config";
import ModelInput from "./modelInput";
import database from "@react-native-firebase/database";
import {getRefreshTokenDash} from "../../../actions/profile";
import {addUserLogToFirebase, checkUserProfile} from "../../../utils/helper";
import {setCallHeader, setSelectedAppointment, updateAppointmentDirect,} from "../../../actions/bookings";
import Images from "../../../assets/images";
import moment from "moment";
import common from "../../../styles/common";
import {methodLog, recordError, successLog} from "../../../utils/fireLog";

class NurseWaitingScreen extends Component {
    state = {
        modelVisible: false,
        roomName: "",
        data: [],
    };

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        successLog("NurseWaitingScreen screen loaded")
        this.props.getWaitingListAdmin();
    }

    setVisibility = () => {
        this.setState({
            modelVisible: false,
        });
    };

    approveRejectCall = (item, status) => {
        // dispatch(setCallHeader(res.data.fullName))
        methodLog("approveRejectCall")
        try {
            this.props.setSelectedAppointment(item._id);
            this.props.setCallHeader(
                item.patientId !== undefined
                    ? `${item.patientId.fullName}`
                    : item.fullName
            );

            let obj = {[item._id]: {status: status}};
            const newReference = database()
                .ref("/doctor")
                .update(obj)
                .then((obj) => {
                    let {fullName, image} = checkUserProfile(this.props.userProfile);
                    /// put user log in array
                    addUserLogToFirebase(item._id, {event: status, name: `${fullName} (Nurse)`}).then(r => {
                    })

                    if (status !== "REJECTED") {
                        let user = this.props.userProfile;
                        let {fullName, image} = checkUserProfile({
                            userId: this.props.userProfile,
                        });
                        /// adding user in list.
                        const users = database()
                            .ref(`/users/${[item._id]}`)
                            .update({nurse: {fullName: fullName, image: image}})
                            .then((obj) => {
                                //send user to join call
                                this.props.navigation.navigate("JoinCall", {
                                    channel: item.doctorId.roomName,
                                    user: "DOCTOR",
                                    id: item._id,
                                });
                            });
                    }
                });
            if (status === "REJECTED") {
                if (item.directLogin && item.directLogin == "true")
                    this.props.updateAppointmentDirectNurse(
                        item._id,
                        this.props.navigation,
                        "REJECTED"
                    );
                else
                    this.props.updateAppointment(
                        item._id,
                        this.props.navigation,
                        "REJECTED"
                    );
            }
        } catch (e) {
            recordError(e)
        }
    };

    render() {
        const renderItem = ({item}) => {
            return (
                <View>
                    <Card style={styles.patientCard}>
                        <CardItem style={styles.patientCard}>
                            <Left>
                                <Thumbnail
                                    source={
                                        item.patientId &&
                                        item.patientId.imageUrl !== "" &&
                                        item.patientId.imageUrl !== null
                                            ? {uri: item.patientId.imageUrl}
                                            : item.patientId && item.patientId.gender === "MALE"
                                            ? Images.profile_male
                                            : Images.userProfile
                                    }
                                />
                                <View>
                                    <View>
                                        <Text style={styles.patientCardText}>
                                            {item.patientId === undefined
                                                ? item.fullName
                                                : item.patientId.fullName}
                                        </Text>
                                    </View>
                                    <View sytle={{flexDirection: "row"}}>
                                        <Text style={styles.patientCardText} note>
                                            {
                                                item.directLogin && item.directLogin == "true"
                                                    ? moment(item.createdAt).format("hh:mm A")
                                                    : item.utcDate && moment(item.utcDate).format("hh:mm A")}
                                        </Text>
                                    </View>
                                </View>
                            </Left>
                            <Right>
                                {item.directLogin && item.directLogin == "true" ? (
                                    <TouchableOpacity
                                        style={[common.intakeBtn, {backgroundColor: "purple"},]}
                                    >
                                        <Text style={common.intakeBtnText}>Direct login user</Text>
                                    </TouchableOpacity>
                                ) : (

                                    <TouchableOpacity
                                        style={[common.intakeBtn, {}]}
                                        onPress={() =>
                                            Linking.openURL(
                                                `${intake_url}pdf/${item.patientId._id}.pdf`
                                            )
                                        }
                                    >
                                        <Text style={common.intakeBtnText}>Intake Form</Text>
                                    </TouchableOpacity>
                                )}


                                <View style={{flexDirection: "row"}}>
                                    <TouchableOpacity
                                        style={[common.intakeBtn, {backgroundColor: "green"}]}
                                        onPress={() => {
                                            if (
                                                item.doctorId.roomName === undefined ||
                                                item.doctorId.roomName == null
                                            ) {
                                                this.setState({
                                                    modelVisible: !this.state.modelVisible,
                                                });
                                            } else {
                                                this.approveRejectCall(item, "APPROVED");
                                            }
                                        }}
                                    >
                                        <Text style={styles.intakeBtnText}>Approve</Text>
                                    </TouchableOpacity>

                                    <TouchableOpacity
                                        style={[common.intakeBtn, {backgroundColor: "red"}]}
                                        onPress={() => {
                                            this.approveRejectCall(item, "REJECTED");
                                        }}
                                    >
                                        <Text style={common.intakeBtnText}>Reject</Text>
                                    </TouchableOpacity>
                                </View>
                            </Right>
                        </CardItem>
                    </Card>
                </View>
            );
        };
        return (
            <Container>
                <SafeAreaView style={styles.header}>
                    <Icon
                        name="menu"
                        type="Feather"
                        style={styles.sideDrawerIcon}
                        onPress={() => this.props.navigation.openDrawer()}
                    />
                    <Text style={styles.headerText}>Dashboard</Text>
                </SafeAreaView>

                <View>
                    <Segment style={styles.segment}>
                        <Button
                            onPress={() => {
                                this.props.getUpcomingBookingsAdmin();
                                this.props.navigation.navigate("NurseDashBoard");
                            }}
                            first
                            active
                            style={[styles.segmentButton, styles.btnInactive]}
                        >
                            <Text style={styles.segmentTextActive} uppercase={false}>
                                Scheduled
                            </Text>
                        </Button>
                        <Button last style={[styles.segmentButton, styles.btnActive]}>
                            <Text style={styles.segmentTextInActive} uppercase={false}>
                                Waiting
                            </Text>
                        </Button>
                    </Segment>
                </View>
                <Content>
                    <SafeAreaView style={styles.list}>
                        <FlatList
                            data={this.props.docWaitingList}
                            showsVerticalScrollIndicator={false}
                            renderItem={renderItem}
                            keyExtractor={(item) => item.id}
                        />
                    </SafeAreaView>
                </Content>

                <ModelInput
                    visible={this.state.modelVisible}
                    text={"Enter room name"}
                    desc={""}
                    value={this.state.roomName}
                    onTouchOutside={() => this.setVisibility()}
                    onBackdropPress={() => this.setVisibility()}
                    onSwipeOut={() => this.setVisibility()}
                    onJoin={(obj) => {
                        this.setVisibility();
                        this.props.checkRoomUrlExist(this.state.roomName);
                    }}
                    onChangeText={(o) => {
                        this.setState({roomName: o});
                    }}
                    onCancel={() => {
                        this.setVisibility();
                    }}
                />
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getWaitingListAdmin,
            updateAppointment,
            checkRoomUrlExist,
            getUpcomingBookingsAdmin,
            setCallHeader,
            getRefreshTokenDash,
            updateAppointmentDirectNurse,
            setSelectedAppointment,
            updateAppointmentDirect,
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    docWaitingList: state.bookings.docWaitingList,
    userProfile: state.profile.userProfile,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NurseWaitingScreen);
