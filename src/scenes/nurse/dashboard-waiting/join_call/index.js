/* eslint-disable prettier/prettier */
import React, {Component} from "react";
import {BackHandler, FlatList, Image, NativeModules, Text, TouchableOpacity, View, SafeAreaView} from "react-native";
import {AgoraView, RtcEngine} from "react-native-agora";
import styles from "../../../../styles/common";
import Images from "../../../../assets/images";
import {Icon} from "native-base";
import {AttendeesItem} from "../../../../components/AttendeesItem";
import {FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_REGULAR,} from "../../../../styles/typography";
import database from "@react-native-firebase/database";
import {bindActionCreators} from "redux";
import {completeAppointment} from "../../../../actions/bookings";
import {connect} from "react-redux";
import {_failureToastWithoutOk} from "../../../../components/Toast";
import {getWaitingListAdmin} from "../../../../actions/bookingsNurse";
import HeaderCommon from "../../../../components/header";
import {requestCameraAndAudioPermission} from "../../../../services/permission";
import RNBeep from 'react-native-a-beep';
import {addUserLogToFirebase, checkUserProfile, handleBackPress} from "../../../../utils/helper";
import VideoCommon from "../../../../components/video-common";
import {showModal} from "../../../../actions/modal";
import {getRoomURLForGuest} from "../../../../actions/share-room";
import {agora_id} from "../../../../config";
import {DISCONNECTED} from "../../../../utils/constant";
import {successLog} from "../../../../utils/fireLog";

const {Agora} = NativeModules; //Define Agora object as a native module

const {FPS30, AudioProfileDefault, AudioScenarioDefault, Adaptative} = Agora; //Set defaults for Stream

const config = {
    //Setting config of the app
    appid: agora_id, //Enter the App ID generated from the Agora Website
    channelProfile: 0, //Set channel profile as 0 for RTC
    videoEncoderConfig: {
        //Set Video feed encoder settings
        width: 720,
        height: 1080,
        bitrate: 1,
        frameRate: FPS30,
        orientationMode: Adaptative,
    },
    audioProfile: AudioProfileDefault,
    audioScenario: AudioScenarioDefault,
};

class JoinCall extends Component {
    constructor(props) {
        super(props);
        this.state = {
            peerIds: [], //Array for storing connected peers
            uid: Math.floor(Math.random() * 100), //Generate a UID for local user
            appid: config.appid,
            channelName: "", //Channel Name for the current session
            joinSucceed: false, //State variable for storing success
            audMute: false,
            vidMute: false,
            showList: false,
            userList: [],
            minutes: 0,
            seconds: 0,
            usersListFirebase: [],
            fullScreen:false,
            peerIdSelected: null,
        };
        this.state.channelName = this.props.navigation.state.params.channel;
        console.log(this.state.channelName);
        requestCameraAndAudioPermission().then((_) => {
            console.log("requested!");
            this.startCall();
        });
    }

    componentWillUnmount() {
        successLog("JoinCall screen loaded")
        let {fullName, image} = checkUserProfile(this.props.userProfile);
        let id = this.props.navigation.state.params.id;
        /// put user log in array
        addUserLogToFirebase(id, {event: DISCONNECTED, name: `${fullName} (Nurse)`}).then(r => {})

        this.backHandler.remove();
        clearInterval(this.interval);
        this.endCall();
        database()
            .ref(`/doctor/${this.props.navigation.state.params.id}`)
            .off("value");
    }

    tick = () => {
        this.setState({
            counter: this.state.counter + 1,
        });
    };

    checkCallStatus = () => {
        database().ref(`/doctor/${this.props.navigation.state.params.id}`)
            .on('value', snapshot => {
                if (snapshot.val() !== null && snapshot.val().status === "FINISHED") {
                    const payload = {
                        text:"Appointment is ended by the doctor!!",
                        iconName: "closecircleo",
                        modalVisible: true,
                        subText: "",
                    };
                    this.props.showModal(payload)
                    this.endCall();
                }
            });
    }

    getUsersList = () => {
        let users = []
        const result = database().ref(`/users/${this.props.selectedAppointment}/`)
            .on('value', snapshot => {
                if(snapshot.val() !== null){
                    let data = snapshot.val();
                    users = Object.values(data);
                    this.setState({
                        usersListFirebase: users
                    })
                }
            });
    };

    componentDidMount() {
        this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
            handleBackPress(this.props)
            return true;
        });

        this.getUsersList()
        this.checkCallStatus();
        this.interval = setInterval(() => {
            const {seconds, minutes} = this.state;
            if (seconds < 60) {
                this.setState(({seconds}) => ({
                    seconds: seconds + 1,
                }));
            } else {
                this.setState(({minutes}) => ({
                    minutes: minutes + 1,
                    seconds: 0,
                }));
            }
        }, 1000);

        RtcEngine.on("userJoined", (data) => {
            const {peerIds} = this.state; //Get currrent peer IDs
            if (peerIds.indexOf(data.uid) === -1) {
                //If new user has joined
                RNBeep.beep(false)
                this.setState({
                    peerIds: [...peerIds, data.uid], //add peer ID to state array
                });
            }
        });
        RtcEngine.on("userOffline", (data) => {
            //If user leaves
            RNBeep.beep(false)
            this.setState({
                peerIds: this.state.peerIds.filter((uid) => uid !== data.uid), //remove peer ID from state array
            });
        });
        RtcEngine.on("joinChannelSuccess", (data) => {
            //If Local user joins RTC channel
            RNBeep.beep(false)
            RtcEngine.startPreview(); //Start RTC preview
            this.setState({
                joinSucceed: true, //Set state variable to true
            });
        });
        RtcEngine.init(config); //Initialize the RTC engine
    }

    /**
     * @name startCall
     * @description Function to start the call
     */
    startCall = () => {
        RNBeep.beep(false)
        console.log("INSTARRT" + this.state.uid + "   " + this.state.channelName);
        RtcEngine.joinChannel(this.state.channelName, this.state.uid); //Join Channel
        RtcEngine.enableAudio(); //Enable the audio
    };
    /**
     * @name endCall
     * @description Function to end the call
     */
    endCall = () => {
        clearInterval(this.interval);
        this.props.getWaitingListAdmin();
        this.removeFromList();
        RtcEngine.leaveChannel();
        this.setState({
            peerIds: [],
            joinSucceed: false,
        });
        this.props.navigation.goBack();
    };

    removeFromList = async () => {
        await database()
            .ref(`/users/${this.props.selectedAppointment}/nurse`)
            .remove();
    }

    toggleVideo = () => {
        let mute = this.state.vidMute;
        RtcEngine.muteLocalVideoStream(!mute);
        console.log("Video toggle", mute);
        this.setState({
            vidMute: !mute,
        });
    };

    toggleAudio = () => {
        let mute = this.state.audMute;
        console.log("Audio toggle", mute);
        RtcEngine.muteLocalAudioStream(!mute);
        this.setState({
            audMute: !mute,
        });
    };

    makeFullScreen = (peerId) => {
        let fullScreen = this.state.fullScreen;
        this.setState({
            fullScreen: !fullScreen, peerIdSelected : peerId
        });
    };

    videoView() {
        return (
            <View style={{flex:1}}>
                <VideoCommon
                    selectedHeader={this.props.selectedHeader}
                    showList={this.state.showList}
                    peerIds={this.state.peerIds}
                    audMute={this.state.audMute}
                    vidMute={this.state.vidMute}
                    joinSucceed={this.state.joinSucceed}
                    minutes={this.state.minutes}
                    seconds={this.state.seconds}
                    usersListFirebase={this.state.usersListFirebase}
                    onPressShowList={()=> {
                        this.setState({
                            showList : !this.state.showList
                        })
                    }}
                    endCall={()=>this.endCall()}
                    toggleAudio={()=>this.toggleAudio()}
                    toggleVideo={()=>this.toggleVideo()}
                    fullScreen={this.state.fullScreen}
                    peerIdSelected={this.state.peerIdSelected}
                    makeFullScreen={(id)=>this.makeFullScreen(id)}
                    copyUrl={()=> {
                        this.props.getRoomURLForGuest(this.props.navigation.state.params.id, this.state.channelName)
                    }}
                />
            </View>
        );
    }

    render() {
        return this.videoView();
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            completeAppointment,
            getWaitingListAdmin, showModal, getRoomURLForGuest
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    userProfile: state.profile.userProfile,
    selectedAppointment: state.bookings.selectedAppointment,
    selectedHeader : state.bookings.selectedHeader

});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(JoinCall);
