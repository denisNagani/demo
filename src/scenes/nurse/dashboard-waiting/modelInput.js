import Modal, {ModalContent} from "react-native-modals";
import {Button, Input, Text} from "native-base";
import styles from "../../../styles/common";
import {View, TextInput} from "react-native";
import React, {useState} from 'react';
import Line from "../../../components/line";

const ModelInput = ({type, visible, onTouchOutside, onSwipeOut, value, onBackdropPress, text, desc, onJoin, onCancel, onChangeText}) => {
    const [deleteText, setDeleteText] = useState('')

    return <Modal
        width={0.9}
        visible={visible}
        onTouchOutside={onTouchOutside}
        rounded
        actionsBordered
        onBackdropPress={() => onBackdropPress}
        onSwipeOut={onSwipeOut}>
        <ModalContent style={{backgroundColor: '#fff'}}>
            <Text style={styles.successModalMainTxt}>{text}</Text>
            <Text style={styles.successModalSubTxt}>{desc}</Text>
            <View style={{width: '100%', marginTop: 5}}>
                <TextInput
                    style={{marginBottom:1,color:'black',fontSize:17,paddingVertical:0}}
                    value={value}
                    onChangeText={onChangeText}
                />
                <Line />
                <Button success block style={{backgroundColor: 'red',marginTop:20}} onPress={onJoin}>
                    <Text createAccountText>CREATE ROOM</Text>
                </Button>

            </View>
        </ModalContent>
    </Modal>
}
export default ModelInput;
