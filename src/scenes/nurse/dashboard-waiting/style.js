import { StyleSheet, Dimensions } from "react-native";

import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";
const windowWidth = Dimensions.get("window").width;

export default (styles = StyleSheet.create({
	header: {
		backgroundColor: "#1E2081",
		height: hp("13%"),
		flexDirection: "row",
		alignItems: "center",
	},
	sideDrawerIcon: {
		fontSize: hp("3.5%"),
		marginTop: hp("0.2%"),
		color: "#fff",
		marginLeft: wp("5%"),
	},
	headerText: {
		color: "#fff",
		fontSize: hp("2.9%"),
		alignSelf: "center",
		marginLeft: wp("5%"),
	},
	cardContainer: {
		justifyContent: "space-around",
		width: "100%",
		flexDirection: "row",
	},
	card: {
		height: hp("25%"),
		width: wp("44%"),
		justifyContent: "flex-end",
		marginVertical: hp("2%"),
	},
	cardInner: {
		flexDirection: "row",
		alignItems: "flex-end",
		justifyContent: "space-between",
		paddingVertical: hp("2.2%"),
		paddingHorizontal: wp("4.3%"),
	},
	cardText: {
		width: wp("30%"),
		fontWeight: "bold",
		fontSize: wp("3.7%"),
		color: "#1E2081",
	},
	cardIconColor: {
		color: "#1B80F3",
		fontSize: hp("3%"),
	},
	segment: {
		backgroundColor: "white",
	},

	segmentButton: {
		height: hp("6.5%"),
		width: windowWidth * 0.45,
		justifyContent: "center",
		marginTop: 20,
	},

	btnActive: {
		backgroundColor: "#1B80F3",
		fontWeight: "bold",
		color: "white",
		borderBottomRightRadius: 10,
		borderTopRightRadius: 10,
	},

	btnInactive: {
		backgroundColor: "#A0A9BE1A",
		color: "black",
		borderBottomLeftRadius: 10,
		borderTopLeftRadius: 10,
	},

	segmentTextActive: {
		fontWeight: "bold",
		fontSize: hp("2%"),
		color: "black",
	},
	segmentTextInActive: {
		fontSize: hp("2%"),
		color: "white",
		fontWeight: "bold",
	},
	date: {
		marginTop: 40,
		marginLeft: windowWidth * 0.05,
		color: "#041A32",
		fontSize: 18,
		fontWeight: "bold",
	},
	patientCard: {
		width: wp("90%"),
		borderRadius: 10,
	},
	list: {
		justifyContent: "center",
		alignSelf: "center",
		marginTop: 20,
		flex: 1,
	},
	patientCardText: {
		marginRight: 40,
		width: 150,
		marginLeft: wp("5%"),
	},
	patientCardTextIcon: {
		fontSize: 12,
		color: "grey",
	},
	intakeFormBtn: {
		width: wp("21%"),
		justifyContent: "center",
		borderRadius: 5,
	},
	intakeFormBtnText: {
		fontSize: 12,
		color: "#fff",
	},
	approvalView: {
		flexDirection: "row",
		paddingTop: 10,
	},
	approveBtn: {
		width: wp("12%"),
		justifyContent: "center",
		borderRadius: 5,
	},
	approveBtnText: {
		fontSize: 12,
		color: "#fff",
	},

	intakeBtn: {
		backgroundColor: "#C70315",
		borderRadius: 7,
		margin: 2,
		padding: 4,
	},
	intakeBtnText: {
		textAlign: "center",
		color: "#fff",
		fontSize: 12,
	},

	rejectBtn: {
		marginLeft: 10,
		width: wp("12%"),
		justifyContent: "center",
		borderRadius: 5,
	},
	rejectBtnText: {
		fontSize: 12,
		color: "#fff",
	},
	patientCardTextTime: {
		marginTop: 6,
		color: "#A0A9BE",
		fontSize: 12,
	},
}));
