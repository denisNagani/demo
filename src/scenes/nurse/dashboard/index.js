import React, {Component} from "react";
import {FlatList, Linking, SafeAreaView, Text, TouchableOpacity, View,} from "react-native";
import {Button, Card, CardItem, Container, Content, Icon, Left, Right, Segment, Thumbnail,} from "native-base";
import AntDesign from "react-native-vector-icons/AntDesign";
import moment from "moment";

import styles from "./style";
import common from "../../../styles/common";
import Images from "../../../assets/images";
import {bindActionCreators} from "redux";
import {getUpcomingBookingsAdmin, getUpcomingBookingsByDate,} from "../../../actions/bookingsNurse";
import ModalComponent from "../../../components/modal";
import {connect} from "react-redux";
import {intake_url} from "../../../config";
import {getRefToken} from "../../../services/auth";
import {getRefreshTokenDash} from "../../../actions/profileNurse";
import {successLog} from "../../../utils/fireLog";

class NurseDashBoard extends Component {
    state = {
        currentDate: moment(new Date()).format("DD MMM YYYY"),
    };

    constructor(props) {
        super(props);
        this.getToken();
    }

    componentDidMount() {
        successLog("NurseDashBoard screen loaded")
        this.props.getUpcomingBookingsAdmin();
    }

    getToken = async () => {
        let token = await getRefToken();
        this.props.getRefreshTokenDash(token);
    };

    render() {
        const renderItem = ({item}) => {
            const status = item.status;
            return (
                <View style={styles.patientView}>
                    <Card style={styles.patientCard}>
                        <CardItem style={styles.patientCard}>
                            <Left>
                                <Thumbnail
                                    source={
                                        item.patientId &&
                                        item.patientId.imageUrl !== "" &&
                                        item.patientId.imageUrl !== null
                                            ? {uri: item.patientId.imageUrl}
                                            : item.patientId && item.patientId.gender === "MALE"
                                            ? Images.profile_male
                                            : Images.userProfile
                                    }
                                />
                                <View style={{flexDirection: "column"}}>
                                    <View>
                                        <Text style={styles.patientCardText}>
                                            {
                                                item.patientId && ((item.patientId.fullName).length > 19) ?
                                                    (((item.patientId.fullName).substring(0, 19 - 3)) + '...') :
                                                    item.patientId.fullName
                                            }
                                        </Text>
                                    </View>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={styles.patientCardSubText}>
                                            <AntDesign
                                                name="calendar"
                                                style={styles.patientCardTextIcon}
                                            />{" "}
                                            {item.utcDate && moment(item.utcDate).format("DD:MM:YYYY")}
                                        </Text>
                                        <Text style={[styles.patientCardSubText, {marginLeft: 4}]}>
                                            <Icon style={styles.patientCardTextIcon} type="Feather" name="clock"/>{" "}
                                            {item.utcDate && moment(item.utcDate).format("hh:mm A")}
                                        </Text>
                                    </View>
                                </View>
                            </Left>
                            <Right>
                                <TouchableOpacity
                                    style={[common.intakeBtn, {}]}
                                    onPress={() =>
                                        Linking.openURL(
                                            `${intake_url}pdf/${item.patientId._id}.pdf`
                                        )
                                    }
                                >
                                    <Text style={common.intakeBtnText}>Intake Form</Text>

                                </TouchableOpacity>


                                <TouchableOpacity
                                    style={[common.intakeBtn, {backgroundColor: "#1B80F3"}]}>
                                    <Text style={[common.intakeBtnText, {color: '#fff'}]}>{status.toLowerCase()}</Text>
                                </TouchableOpacity>


                            </Right>
                        </CardItem>
                    </Card>
                </View>
            );
        };
        return (
            <Container>
                <SafeAreaView style={styles.header}>
                    <Icon
                        name="menu"
                        type="Feather"
                        style={styles.sideDrawerIcon}
                        onPress={() => this.props.navigation.openDrawer()}
                    />
                    <Text style={styles.headerText}>Dashboard</Text>
                    <ModalComponent/>
                </SafeAreaView>

                <View style={{marginBottom: 20}}>
                    <Segment style={styles.segment}>
                        <Button
                            first
                            active
                            style={[styles.segmentButton, styles.btnActive]}
                        >
                            <Text style={styles.segmentTextActive}>Scheduled</Text>
                        </Button>
                        <Button
                            onPress={() => {
                                this.props.navigation.navigate("NurseWaitingScreen");
                                this.props.getUpcomingBookingsAdmin();
                            }}
                            last
                            style={[styles.segmentButton, styles.btnInactive]}
                        >
                            <Text style={styles.segmentTextInActive}>Waiting</Text>
                        </Button>
                    </Segment>
                </View>
                <View style={styles.dateTab}>

                </View>
                <Content contentContainerStyle={styles.list}>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={this.props.upcomingBookingsByDate}
                        renderItem={renderItem}
                        keyExtractor={(item) => item.id}
                    />
                </Content>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getUpcomingBookingsByDate,
            getRefreshTokenDash,
            getUpcomingBookingsAdmin,
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    upcomingBookingsByDate: state.bookings.upcomingBookingsByDate,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(NurseDashBoard);
