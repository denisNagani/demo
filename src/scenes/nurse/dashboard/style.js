import { StyleSheet, Dimensions } from "react-native";

import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {FONT_SIZE_11, FONT_SIZE_12} from "../../../styles/typography";
const windowWidth = Dimensions.get("window").width;

export default (styles = StyleSheet.create({
	header: {
		backgroundColor: "#1E2081",
		height: hp("13%"),
		flexDirection: "row",
		alignItems: "center",
	},
	sideDrawerIcon: {
		fontSize: hp("3.5%"),
		marginTop: hp("0.2%"),
		color: "#fff",
		marginLeft: wp("5%"),
	},
	headerText: {
		color: "#fff",
		fontSize: hp("2.9%"),
		width: wp("80%"),
		alignSelf: "center",
		marginLeft: wp("5%"),
	},
	cardContainer: {
		justifyContent: "space-around",
		width: "100%",
		flexDirection: "row",
	},
	card: {
		height: hp("25%"),
		width: wp("44%"),
		justifyContent: "flex-end",
		marginVertical: hp("2%"),
	},
	cardInner: {
		flexDirection: "row",
		alignItems: "flex-end",
		justifyContent: "space-between",
		paddingVertical: hp("2.2%"),
		paddingHorizontal: wp("4.3%"),
	},
	cardText: {
		width: wp("30%"),
		fontSize: wp("3.7%"),
		color: "#1E2081",
	},
	cardIconColor: {
		color: "#1B80F3",
		fontSize: hp("3%"),
	},
	segment: {
		backgroundColor: "white",
	},

	segmentButton: {
		height: hp("6.5%"),
		width: windowWidth * 0.45,
		justifyContent: "center",
		marginTop: 20,
	},

	btnActive: {
		backgroundColor: "#1B80F3",
		fontWeight: "bold",
		color: "white",
		borderBottomLeftRadius: 10,
		borderTopLeftRadius: 10,
	},

	btnInactive: {
		backgroundColor: "#A0A9BE1A",
		borderBottomRightRadius: 10,
		borderTopRightRadius: 10,
	},

	segmentTextActive: {
		fontWeight: "bold",
		fontSize: hp("2%"),
		color: "#fff",
	},
	segmentTextInActive: {
		fontSize: hp("2%"),
		color: "black",
		fontWeight: "bold",
	},
	date: {
		marginTop: 40,
		marginLeft: windowWidth * 0.05,
		color: "#041A32",
		fontSize: 18,
		fontWeight: "bold",
	},
	patientView: {
		marginVertical: hp("1%"),
	},
	patientCard: {
		width: wp("90%"),
		borderRadius: 10,
	},
	list: {
		justifyContent: "center",
		alignSelf: "center",
		marginBottom: hp("2%"),
	},
	patientCardText: {
		fontSize: 16,
		width: windowWidth * 0.9,
		marginLeft: wp("3%"),
	},
	patientCardSubText: {
		color: "#A0A9BE",
		marginTop: 8,
		marginLeft: wp("3%"),
		fontSize: 12,
	},
	patientCardTextIcon: {
		fontSize: 11,
		color: "#A0A9BE",
	},
	intakeFormBtn: {
		width: wp("21%"),
		justifyContent: "center",
		borderRadius: 5,
	},
	intakeFormBtnText: {
		fontSize: FONT_SIZE_12,
		color: "#fff",
	},
	patientCardTextTime: {
		marginTop: 6,
		color: "#A0A9BE",
		fontSize: 12,
	},
	myProfile: {
		marginRight: 30,
		marginTop: 25,
	},
	dateTab: {
		marginTop: "1%",
		flexDirection: "row",
		width: "90%",
		marginHorizontal: "5%",
	},
	prev: {
		flex: 2,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-start",
	},
	prevText: {
		color: "#1B80F3",
		fontSize: hp("2.5%"),
	},
	prevIcon: {
		color: "#1B80F3",
		marginRight: 5,
	},
	current: {
		flex: 6,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
	},
	currentText: {
		fontSize: hp("2.5%"),
		color: "#041A32",
	},
	next: {
		fontSize: 18,
		flex: 2,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "flex-end",
	},
	nextIcon: {
		color: "#1B80F3",
		marginLeft: 5,
	},
	nextText: {
		fontSize: hp("2.5%"),
		color: "#1B80F3",
	},
}));
