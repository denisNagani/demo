import React from 'react'
import { View, Text, Image, ScrollView, Dimensions } from 'react-native'
import HeaderSehet from '../../../../components/header-common'
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_REGULAR } from '../../../../styles/typography';
import Images from '../../../../assets/images';
import moment from 'moment'
import Line from '../../../../components/line';
import { ifNotValid } from '../../../../utils/ifNotValid';
import AutoHeightWebView from '../../../../components/AutoWebViewSize/index';


const index = (props) => {
    const item = props.navigation.state.params.item;

    let title = item.title;
    if (ifNotValid(title)) title = "..."

    let description = item.description;
    if (ifNotValid(description)) description = "..."

    let publishDate = item.publishDate;
    if (ifNotValid(publishDate)) publishDate = new Date()

    let imageUrl = item.imageUrl;
    if (ifNotValid(imageUrl)) imageUrl = Images.sehet_logo_login
    else imageUrl = { uri: imageUrl }



    return (
        <View style={{
            flex: 1,
            backgroundColor: '#F9F9F9'
        }}>
            <HeaderSehet headerText={"Details"} navigation={props.navigation} changeTheme={true} />
            <ScrollView
                style={{
                    flex: 1,
                    backgroundColor: '#F9F9F9'
                }}
            >

                <Image
                    source={imageUrl}
                    style={{
                        height: 250,
                        width: '100%'
                    }}
                />

                <View style={{
                    flex: 1,
                    margin: 10
                }}>

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center'
                    }}>
                        <Image
                            source={Images.profile_male}
                            style={{ margin: 10, height: 50, width: 50, borderRadius: 50 / 2 }}
                        />
                        <View style={{ marginLeft: 5 }}>
                            <Text style={{ fontSize: 16, fontWeight: 'bold', marginBottom: 5 }}>{item.authorName}</Text>
                            <Text>{moment(publishDate).fromNow()}</Text>
                        </View>
                    </View>

                    <Line lineColor="lightgrey" />

                    <Text style={{
                        fontSize: 20,
                        fontFamily: FONT_FAMILY_SF_PRO_BOLD,
                        marginVertical: 10,
                        marginHorizontal: 10
                    }}>
                        {title}
                    </Text>

                    <AutoHeightWebView
                        style={{
                            width: Dimensions.get('window').width - 40,
                            marginLeft: 10,
                            marginBottom: 10
                        }}
                        onSizeUpdated={size => console.log(size.height)}
                        source={{ html: description }}
                    // viewportContent={'width=device-width, user-scalable=no'}
                    />
                </View>
            </ScrollView>
        </View >
    )
}

export default index
