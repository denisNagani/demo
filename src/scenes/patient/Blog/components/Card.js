import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import Images from '../../../../assets/images'
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_REGULAR } from '../../../../styles/typography'
import { Button } from 'native-base'
import moment from 'moment'
import Line from '../../../../components/line'
import { ifNotValid } from '../../../../utils/ifNotValid'
import WebView from 'react-native-webview'

const Card = ({ item, navigation }) => {

    let title = item.title;
    if (ifNotValid(title)) title = "..."

    let description = item.description;
    if (ifNotValid(description)) description = "..."

    let imageUrl = item.imageUrl;
    if (ifNotValid(imageUrl)) imageUrl = Images.sehet_logo_login
    else imageUrl = { uri: imageUrl }


    return (
        <View style={styles.card}>
            <Image
                // source={Images.loginHalf}
                source={imageUrl}
                resizeMode="cover"
                style={styles.imageStyle}
            />
            <View style={{
                paddingVertical: 10,
                paddingHorizontal: 10,
            }}>
                <Text numberOfLines={2} style={styles.title}>{title}</Text>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                    {/* <Text numberOfLines={4} style={styles.description}>
                        {description}
                    </Text> */}
                    <WebView
                        style={{
                            height: 100
                        }}
                        scalesPageToFit={true}
                        source={{
                            html: `<html>
                                    <meta 
                                    content="width=device-width, 
                                    initial-scale=1.0, 
                                    maximum-scale=1.0, 
                                    user-scalable=0" 
                                    name="viewport"
                                    />
                                    <body style="margin-left:20px;margin-right:20px;padding:0px;">
                                            ${description}
                                    </body>
                                  </html>
                        `,
                        }}
                    />

                    <Line lineColor="lightgrey" style={{ marginTop: 10 }} />

                    <View style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        justifyContent: 'space-between'
                    }}>
                        <Button style={{
                            // alignSelf: 'flex-end',
                            backgroundColor: '#FFF',
                            elevation: 0
                        }}
                            onPress={() => {
                                navigation.navigate('BlogDetails', {
                                    item: item
                                })
                            }}
                        >
                            <Text style={{
                                paddingHorizontal: 10,
                                color: '#00A2FF',
                                textAlign: 'center',
                                fontSize: 15
                            }}>View More</Text>
                        </Button>

                        <Text
                            style={{
                                fontSize: 15,
                                marginRight: 5
                            }}>
                            {moment(item.publishDate).fromNow()}
                        </Text>
                    </View>
                </View>

            </View>
        </View>
    )
}

export default Card

const styles = StyleSheet.create({
    card: {
        margin: 15,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 5
    },
    title: {
        fontSize: 20,
        fontFamily: FONT_FAMILY_SF_PRO_BOLD,
        marginVertical: 10,
        marginHorizontal: 10
    },
    description: {
        marginHorizontal: 10,
        fontSize: 16,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        letterSpacing: 0.5
    },
    imageStyle: {
        height: 200,
        width: '100%',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10
    }
})