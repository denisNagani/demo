import React, { useEffect } from 'react'
import { View, FlatList, Text, Image } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { getAllBlogs } from '../../../actions/blog'
import HeaderSehet from '../../../components/header-common'
import Card from './components/Card'
import stylesUpcoming from "../../doctor/upcoming-past-bookings/style";
import Images from '../../../assets/images'
import { strings } from '../../../utils/translation'

const index = (props) => {

    const blogs = useSelector(state => state.directLogin.blogs)
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(getAllBlogs())
    }, [])

    const NoDataFound = () => (
        <View style={stylesUpcoming.noBookingsBlock}>
            <Image source={Images.no_bookings} style={[stylesUpcoming.noBookingsImage, { width: 200, height: 200 }]} />
            <Text style={stylesUpcoming.noBookingsTitle}>{"No Blogs Found"}</Text>
        </View>
    )

    return (
        <View style={{
            flex: 1,
            backgroundColor: '#F9F9F9'
        }}>
            <HeaderSehet headerText={strings.blogs} navigation={props.navigation} changeTheme={true} />
            <FlatList
                keyExtractor={(item, index) => index.toString()}
                data={blogs}
                renderItem={({ item }) => (
                    <Card item={item} navigation={props.navigation} />
                )}
                ListEmptyComponent={() => NoDataFound()}
            />
        </View>
    )
}

export default index
