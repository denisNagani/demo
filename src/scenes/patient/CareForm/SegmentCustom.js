import React from "react";
import { StyleSheet, Text } from "react-native";
import { Button, Segment } from "native-base";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { useDispatch, useSelector } from "react-redux";
// import { ifValid } from "../../../../utils/ifNotValid";
import { setActiveTab } from "./action";
import { strings } from "../../../utils/translation";

const SegmentCustom = ({ activeButton }) => {
    const dispatch = useDispatch();
    // ifValid(activeButton) ? activeButton = 'UPCOMING' : "PAST"
    const onPressButton = () => {
        activeButton === 0 ?
            dispatch(setActiveTab(1)) :
            dispatch(setActiveTab(0))
    }


    return (
        <Segment style={styles.segment}>
            <Button
                first
                onPress={() => onPressButton()}
                style={[styles.segmentButton, activeButton === 0 ? styles.btnActive : {
                    ...styles.btnInactive, borderTopRightRadius: 0, borderBottomRightRadius: 0,
                    borderTopLeftRadius: 5, borderBottomLeftRadius: 5,
                }]}>


                <Text style={activeButton === 0 ? {
                    ...styles.segmentTextActive,
                    color: '#fff'
                } : {
                        ...styles.segmentTextActive,
                        color: '#4E5C76',
                    }}>{"I can help!"}</Text>
            </Button>


            <Button
                onPress={() => onPressButton()}
                last
                style={[styles.segmentButton, activeButton === 1 ? {
                    ...styles.btnActive,
                    borderTopRightRadius: 5,
                    borderTopLeftRadius: 0,
                    borderBottomLeftRadius: 0,
                    borderBottomRightRadius: 5
                } : styles.btnInactive]}
            >
                <Text style={activeButton === 1 ? {
                    ...styles.segmentTextActive,
                    color: '#fff'
                } : {
                        ...styles.segmentTextActive,
                        color: '#4E5C76',
                    }}>{"I need help!"}</Text>
            </Button>
        </Segment>
    );
}
const styles = StyleSheet.create({
    segment: {
        backgroundColor: "#F9F9F9",
        marginVertical: hp("3%"),
    },

    segmentButton: {
        height: hp("6.5%"),
        width: wp("45%"),
        justifyContent: "center",
        marginTop: hp("3%"),
        backgroundColor: '#F9F9F9'
    },

    btnActive: {
        backgroundColor: "#038BEF",
        fontWeight: "bold",
        color: "white",
        borderBottomLeftRadius: 5,
        borderTopLeftRadius: 5,
    },

    btnInactive: {
        backgroundColor: "#E6E8EC",
        borderBottomRightRadius: 5,
        borderTopRightRadius: 5,
    },

    segmentTextActive: {
        fontWeight: "bold",
        fontSize: hp("2%"),
        color: "#fff",
    },
    segmentTextInActive: {
        fontSize: hp("2%"),
        color: "black",
        fontWeight: "bold",
    },
})
export default SegmentCustom;
