import { successLog, errorLog } from "../../../utils/fireLog";
import http from "../../../services/http";
import { showModal, hideAppointmentModal, hideModal, setClockTime } from "../../../actions/modal";
import { SHOW_MODAL } from "../../../utils/constant";

export const SubmitSurvey = (payload, navigation) => {
    successLog('calling api.')
    return (dispatch) => {
        http.post("covid-survey", payload, dispatch)
            .then((res) => {
                console.log(res);
                if (res.status === 200) {
                    // successLog("update updateDoctorProfile profile ", res.status)
                    const payload = {
                        text: "Your Response has been shared with Sehet team. They will contact you soon. For more details Contact info@sehet.in or 08047166558/9983497088",
                        subText: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    navigation.goBack();
                    // dispatch(showModal(payload))
                    dispatch(hideAppointmentModal());
                    dispatch(hideModal());
                    dispatch(setClockTime(5));
                    dispatch({ type: SHOW_MODAL, payload: payload })
                }
            })
            .catch((err) => {
                errorLog("updateDoctorProfile", err)
            });
    };
};

export const setActiveTab = (payload) => {
    return {
        type: 'SET_ACTIVE_TAB',
        payload: payload,
    };
};