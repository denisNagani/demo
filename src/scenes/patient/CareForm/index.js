import React, { useState, useRef } from 'react'
import { View, Text, StyleSheet, TextInput, ScrollView, TouchableOpacity } from 'react-native'
import HeaderSehet from '../../../components/header-common'
import { Label, Icon, Picker, Form, Content } from 'native-base'
import InputCommon from '../../../components/InputCommon'
import { strings } from '../../../utils/translation'
import { FONT_FAMILY_SF_PRO_MEDIUM } from '../../../styles/typography'
import MultiSelect from 'react-native-multiple-select'
import { useDispatch, useSelector } from 'react-redux'
import { SubmitSurvey } from './action'
import { ifValid, ifNotValid } from '../../../utils/ifNotValid'
import { showModal } from '../../../actions/modal'
import SegmentCustom from './SegmentCustom'
import { SafeAreaView } from 'react-native'

const index = (props) => {
    // const formtypeuser = props.navigation.state.params.data;
    let activeButton = useSelector((state) => state.upcomingPast.activeTab);

    const dispatch = useDispatch()

    let multiSelect = useRef(null)
    const [selectedItems, setSelectedItems] = useState([])

    const [formData, setFormData] = useState({
        fullName: "",
        bloodGroup: "",
        typeOfUser: "",
        optFor: selectedItems,
        mobileNo: "",
        remarks: "",
        city: "",
        state: ""
    })

    const setOnChange = (field, value) => {
        setFormData({ ...formData, [field]: value })
    }

    const onSelectedItemsChange = selectedItems => {
        setSelectedItems(selectedItems);
    };

    const optFotItems = [
        { id: "Plasma", name: 'Plasma' },
        { id: "Oxygen", name: 'Oxygen' },
        { id: "Remdesivir", name: 'Remdesivir' },
        { id: "Beds", name: 'Beds' },
        { id: "Ambulance", name: 'Ambulance' },
        { id: "Medicines", name: 'Medicines' },
        { id: "Food", name: 'Food' },
        { id: "Others", name: 'Others' }
    ];

    const submitForm = () => {
        if (formData.fullName == "") {
            const payload = {
                text: "Full Name required!",
                subText: "",
                iconName: "closecircleo",
                modalVisible: true,
            };
            dispatch(showModal(payload))
            return
        }
        // else if (formData.bloodGroup == "") {
        //     const payload = {
        //         text: "BloodGroup required!",
        //         subText: "",
        //         iconName: "closecircleo",
        //         modalVisible: true,
        //     };
        //     dispatch(showModal(payload))
        //     return
        // } 
        else if (selectedItems.length == 0) {
            const payload = {
                text: "Opt For required!",
                subText: "",
                iconName: "closecircleo",
                modalVisible: true,
            };
            dispatch(showModal(payload))
            return
        }
        else if (formData.mobileNo == "" || formData.mobileNo.length != 10) {
            const payload = {
                text: "Enter Valid Mobile Number!",
                subText: "",
                iconName: "closecircleo",
                modalVisible: true,
            };
            dispatch(showModal(payload))
            return
        }
        const obj = {
            fullName: formData.fullName,
            bloodGroup: formData.bloodGroup,
            typeOfUser: activeButton === 0 ? "Donar" : "Acceptor",
            optFor: selectedItems.toString(),
            mobileNo: formData.mobileNo,
            remarks: formData.remarks,
            city: formData.city,
            state: formData.state
        }
        dispatch(SubmitSurvey(obj, props.navigation))
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: '#F9F9F9'
        }}>
            <SafeAreaView style={{
                // flex : 1,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginHorizontal: 10,
                marginTop: 10
            }}>
                <View>
                    <Text style={{
                        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                        fontSize: 20
                    }}>Covid Care Medical Assistance</Text>
                    <Text style={{
                        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                        fontSize: 16,
                        color: '#4E5C76'
                    }}>Add details of yours to continue</Text>
                </View>
                <Icon
                    name="close"
                    type="AntDesign"
                    style={{ color: '#038BEF' }}
                    onPress={() => props.navigation.goBack()}
                />
            </SafeAreaView>

            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{
                    marginHorizontal: 10,
                }}>

                <SegmentCustom activeButton={activeButton} />
                <Form>
                    <Content>
                        <InputCommon
                            style={{ margin: 20 }}
                            value={formData.fullName}
                            textAlignVertical='top'
                            // placeholder={"Full Name"}
                            onChangeText={(v) => setOnChange("fullName", v)}
                            title={"Enter Name*"}
                            styleInput={{ height: 40, paddingVertical: 10 }}
                        />


                        <Text style={{
                            marginHorizontal: 20,
                            marginVertical: 10,
                            fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                            color: 'black',
                            fontSize: 14
                        }}>{"Blood Group"}</Text>

                        <View style={{ borderRadius: 9, borderColor: '#B6BECB', borderWidth: 0.6, marginHorizontal: 15, marginBottom: 8 }}>
                            <Picker
                                mode="dropdown"
                                iosIcon={<Icon name="keyboard-arrow-down" type="MaterialIcons" />}
                                style={{ width: '100%' }}
                                selectedValue={formData.bloodGroup}
                                onValueChange={(value) => {
                                    if (value != "")
                                        setOnChange("bloodGroup", value)
                                }}>
                                <Picker.Item label="Select" value="" />
                                <Picker.Item label="A+" value="A+" />
                                <Picker.Item label="A-" value="A-" />
                                <Picker.Item label="B+" value="B+" />
                                <Picker.Item label="B-" value="B-" />
                                <Picker.Item label="O+" value="O+" />
                                <Picker.Item label="O-" value="O-" />
                                <Picker.Item label="AB+" value="AB+" />
                                <Picker.Item label="AB-" value="AB-" />
                            </Picker>

                        </View>

                        <Text style={{
                            marginHorizontal: 20,
                            fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                            color: 'black',
                            fontSize: 14,
                            marginVertical: 15
                        }}>{"Opt For*"}</Text>

                        <View style={{
                            marginHorizontal: 15
                        }}>
                            <MultiSelect
                                hideTags
                                items={optFotItems}
                                uniqueKey="id"
                                // ref={(component) => {
                                //     multiSelect = component
                                // }}
                                onSelectedItemsChange={onSelectedItemsChange}
                                // selectedItems={obj.eventOfDay}
                                onChangeInput={(text) => console.log(text)}
                                selectedItems={selectedItems}
                                tagRemoveIconColor="#CCC"
                                tagBorderColor="#CCC"
                                tagTextColor="black"
                                selectedItemTextColor="blue"
                                searchIcon={<Icon name="search" type="MaterialIcons" style={{ fontSize: 22 }} />}
                                iconSearch={false}
                                textInputProps={{ autoFocus: false, style: { padding: 0, margin: 0, } }}
                                hideDropdown={true}
                                selectedItemIconColor="blue"
                                selectedItemIconType="AntDesign"
                                itemTextColor="#000"
                                displayKey="name"
                                selectText={"  Select"}
                                fontSize={16}
                                textColor={"#000"}
                                searchInputStyle={{ color: '#CCC' }}
                                submitButtonColor="#FF5E38"
                                submitButtonText="Submit"
                                styleDropdownMenuSubsection={[{
                                    borderColor: '#B6BECB',
                                    borderWidth: .6,
                                    height: 50,
                                    borderRadius: 9,
                                    backgroundColor: 'transparent'
                                }]}
                            />
                        </View>

                        <InputCommon
                            style={{ margin: 20 }}
                            value={formData.mobileNo}
                            // numberOfLines={3}
                            // multiline={true}
                            textAlignVertical='top'
                            // placeholder={"Mobile Number"}
                            keyboardType='numeric'
                            returnKeyType="done"
                            maxLength={10}
                            onChangeText={(v) => setOnChange("mobileNo", v)}
                            title={"Enter Mobile Number*"}
                            styleInput={{ height: 40, paddingVertical: 10 }}
                        />
                        <InputCommon
                            style={{ margin: 20 }}
                            value={formData.remarks}
                            // numberOfLines={3}
                            // multiline={true}
                            textAlignVertical='top'
                            // placeholder={"Remark"}
                            onChangeText={(v) => setOnChange("remarks", v)}
                            title={"Enter Remark"}
                            styleInput={{ height: 40, paddingVertical: 10 }}
                        />
                        <InputCommon
                            style={{ margin: 20 }}
                            value={formData.city}
                            // numberOfLines={3}
                            // multiline={true}
                            textAlignVertical='top'
                            // placeholder={"City"}
                            onChangeText={(v) => setOnChange("city", v)}
                            title={"Enter City"}
                            styleInput={{ height: 40, paddingVertical: 10 }}
                        />
                        <InputCommon
                            style={{ margin: 20 }}
                            value={formData.state}
                            // numberOfLines={3}
                            // multiline={true}
                            textAlignVertical='top'
                            // placeholder={"State"}
                            onChangeText={(v) => setOnChange("state", v)}
                            title={"Enter State"}
                            styleInput={{ height: 40, paddingVertical: 10 }}
                        />

                        <TouchableOpacity style={{
                            marginHorizontal: 10,
                            backgroundColor: '#FF5E38',
                            padding: 15,
                            borderRadius: 10,
                            marginVertical: 10
                        }}
                            onPress={submitForm}
                        >
                            <Text style={{ textAlign: 'center', color: '#FFF', fontWeight: 'bold', fontSize: 16 }}>Submit</Text>
                        </TouchableOpacity>
                    </Content>
                </Form>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    Label: {

    }
})

export default index
