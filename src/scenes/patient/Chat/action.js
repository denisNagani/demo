import http from "../../../services/http";
import { successLog, errorLog } from "../../../utils/fireLog";
import { GET_CHAT_DATA } from "../../../utils/constant";
import { show, hide } from "../../../utils/loader/action";

export const getChatData = () => {
    return (dispatch) => {
        // dispatch(show())
        http.get(`/api/fetch-appointments`, dispatch)
            .then((res) => {
                successLog("get upcoming bookings", res.status)
                dispatch({ type: GET_CHAT_DATA, payload: res.data });
                // dispatch(hide())
            })
            .catch((o) => {
                errorLog("getUpcomingBookings", o)
                // dispatch(hide())
            });
    };
};