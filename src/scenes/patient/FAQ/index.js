import React, { useEffect } from 'react'
import { View, Text, SafeAreaView, ScrollView, Platform } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { getFAQ } from '../../../actions/sehet/user-action';
import HeaderSehet from '../../../components/header-common';
import { FONT_FAMILY_SF_PRO_MEDIUM } from '../../../styles/typography';
import { Content, Accordion, Container, Icon } from 'native-base';

const index = (props) => {
    const FAQS_DATA = useSelector((state) => state.userReducer.FAQS);
    const FAQS = []
    FAQS_DATA.map(item => {
        if (item.type == "patient") {
            const new_item = {
                title: item.question,
                content: item?.srno == 2
                    ? Platform.OS === 'ios'
                        ? item?.answerIOS
                        : item?.answerAndroid
                    : item.answer
            }
            FAQS.push(new_item)
        }
    })
    console.log(FAQS);

    const _renderHeader = (item, expanded) => {
        return (
            <View style={{
                flexDirection: "row",
                paddingVertical: 15,
                paddingHorizontal: 15,
                justifyContent: "space-between",
                alignItems: "center",
                backgroundColor: "#f7f5f2",
            }}>
                <Text style={{ fontWeight: "600", width: '93%', fontSize: 16, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }}>
                    {item.title}
                </Text>
                {expanded
                    ? <Icon style={{ fontSize: 18 }} name="remove-circle-outline" type='MaterialIcons' />
                    : <Icon style={{ fontSize: 18 }} name="add-circle" type='Ionicons' />}
            </View>
        );
    }

    const _renderContent = (item) => {
        return (
            <Text
                style={{
                    backgroundColor: "#f3f2f2",
                    paddingHorizontal: 15,
                    paddingVertical: 10,
                    fontSize: 15
                    // fontStyle: "italic",
                }}
            >
                {item.content}
            </Text>
        );
    }

    return (
        <View style={{
            flex: 1,
            backgroundColor: '#F9F9F9'
        }}>
            <HeaderSehet headerText={"FAQ's"} navigation={props.navigation} />

            <ScrollView
                showsVerticalScrollIndicator={false}
            >
                <Content style={{
                    padding: 15
                }}>
                    <Accordion
                        dataArray={FAQS}
                        renderHeader={_renderHeader}
                        renderContent={_renderContent}
                    />
                </Content>
            </ScrollView>
        </View>
    )
}

export default index
