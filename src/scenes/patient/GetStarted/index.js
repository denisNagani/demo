import React from 'react'
import { View, Text, Dimensions, Image, SafeAreaView, ImageBackground } from 'react-native'
import Carousel from "react-native-banner-carousel";
import Images from '../../../assets/images';
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_REGULAR } from '../../../styles/typography';
import { Button } from 'native-base';
import common from "../../../styles/common";
import { setGetStarted } from '../../../services/auth';

const index = (props) => {
    const BannerWidth = Dimensions.get('window').width;
    const images = [
        { imageUrl: Images.g1 },
        { imageUrl: Images.g2 },
        { imageUrl: Images.g3 },
    ]
    const steps = [
        { text: "Choose Speciality/ Doctor", imageUrl: Images.stethoscope },
        { text: "Book Date & \n Time", imageUrl: Images.calendar_2 },
        { text: "Consult Via Video Call", imageUrl: Images.video_calling_app },
    ]

    const renderBanner = (imageUrl, index) => {
        return (
            <View style={{
                height: '80%',
                width: BannerWidth,
                marginTop: 40,
                // 
            }} key={index}>
                <View style={{ flex: 1, marginHorizontal: 25, justifyContent: 'center', alignItems: 'center' }}>
                    <Image
                        resizeMode="contain"
                        style={{
                            height: index == 2 ? '75%' : '100%',
                            width: '100%'
                        }}
                        source={imageUrl}
                        defaultSource={Images.coupen_code}
                    />
                    <Text style={{
                        fontSize: index == 1 ? 26 : 22,
                        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
                        color: index == 1 ? '#FFAD17' : '#041A32',
                        fontWeight: index == 1 ? 'bold' : 'normal',
                        textAlign: 'center'
                    }}>
                        {index == 0 && "Best doctors of"}
                        {index == 1 && "Consult Online"}
                        {index == 2 && "Book Online Appointment"}
                    </Text>
                    <Text style={{
                        fontSize: index == 1 ? 18 : 26,
                        color: index == 1 ? '#041A32' : '#00C4B4',
                        fontWeight: index == 1 ? 'normal' : 'bold',
                        textAlign: 'center',
                    }}>
                        {index == 0 && "India"}
                        {index == 1 && "Traveling a major concern to consult a Doctor?"}
                        {index == 2 && "In 3 Simple Steps"}
                    </Text>
                    {
                        index == 2 && (
                            <View style={{
                                flexDirection: 'row',
                                marginTop: 20
                            }}>
                                {steps.map((item, index) => (
                                    <View
                                        style={{
                                            width: '30%',
                                            // padding: 10,
                                            backgroundColor: '#D7EDFE',
                                            marginHorizontal: 5,
                                            marginVertical: 10,
                                            borderRadius: 5,
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}>
                                        <Image
                                            source={item.imageUrl}
                                            style={{
                                                height: 40,
                                                width: 40,
                                                marginVertical: 8
                                            }}
                                            resizeMode="contain"
                                        />
                                        <Text style={{
                                            // marginLeft: 10,
                                            fontSize: 12,
                                            color: '#041A32',
                                            textAlign: 'center',
                                            marginVertical: 5
                                        }}>
                                            {item.text}
                                        </Text>
                                    </View>
                                ))}

                            </View>
                        )
                    }
                </View>
            </View>
        )
    }
    return (
        <SafeAreaView>
            <ImageBackground
                source={Images.back_screen}
                style={{ height: '100%', width: '100%' }}
                resizeMode="contain"
            >
                <Carousel
                    autoplay
                    autoplayTimeout={3000}
                    index={0}
                    autoplay={false}
                    pageIndicatorOffset={18}
                    pageIndicatorStyle={{ height: 8, width: 8, borderRadius: 8 }}
                    activePageIndicatorStyle={{ backgroundColor: '#FF5E38' }}
                >

                    {images.map((item, index) => renderBanner(item.imageUrl, index))}
                </Carousel>

                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: '10%' }}>
                    <Button danger style={[common.footerBtn, {
                        marginHorizontal: 20,
                        alignSelf: 'center',
                        backgroundColor: '#FF5E38'
                    }]}
                        onPress={async () => {
                            await setGetStarted(true)
                            props.navigation.navigate("PatientLogin")
                        }}>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Text uppercase={false}
                                style={[styles.number91, { color: 'white', flex: 1, paddingHorizontal: 15, fontSize: 15, textAlign: 'center' }]}>
                                Get Started
                        </Text>
                        </View>
                    </Button>
                </View>


            </ImageBackground>
        </SafeAreaView>
    )
}

export default index
