import { Image, SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useCallback, useEffect, useState } from 'react'
import HeaderSehet from '../../../components/header-common'
import { strings } from '../../../utils/translation'
import { Content, Form, Icon, Input, Item, Label, Picker } from 'native-base'
import AppButton from '../../../components/AppButton'
import Line from '../../../components/line'
import { setPhotoPickerModal, showModal } from '../../../actions/modal'
import { errorPayload } from '../../../utils/helper'
import { useDispatch, useSelector } from 'react-redux'
import DropDownPicker from 'react-native-dropdown-picker'
import Images from '../../../assets/images'
import { FONT_FAMILY_SF_PRO_MEDIUM } from '../../../styles/typography'
import PhotoPickerModal from '../../../components/PhotoPickerModal'
import { launchCamera, launchImageLibrary } from 'react-native-image-picker'
import { requestPermissionCamera, requestPermissionStorage } from '../../../services/requestPermissions'
import http from '../../../services/http'
import { hide, show } from '../../../utils/loader/action'
import { uploadAttachment } from '../../../actions/sehet/patient-action'

const index = (props) => {

    const action = props.navigation.state.params?.action
    const userProfile = useSelector(state => state.profile.userProfile)
    const photoPickerModal = useSelector(state => state.modal.photoPickerModal)

    const photoPickerTitles = [
        {
            id: 1,
            title: "Pick From Gallary",
            text: "Gallary",
        },
        {
            id: 2,
            title: "Capture Image",
            text: "Camera",
        },
    ]

    // picker states
    const [open, setOpen] = useState(false);
    const [selectLab, setSelectlab] = useState(null);
    const [open2, setOpen2] = useState(false);
    const [selectTest, setSelectTest] = useState(null);
    const [labs, setLabs] = useState([])
    const [labTest, setLabTests] = useState([])

    const [selectedImages, setSelectedImages] = useState([])

    const dispatch = useDispatch()
    const [formState, setFormState] = useState({
        fullName: "",
        mobileNo: "",
        email: "",
        address: "",
        locality: "",
        landmark: "",
        city: "",
        state: "",
        pincode: "",
    })

    useEffect(() => {
        getLabsData()
    }, [])

    useEffect(() => {
        if (selectLab?.id) {
            getTestsByLabId(selectLab)
        }
    }, [selectLab])

    const getLabsData = () => {
        dispatch(show())
        http
            .get('api/ActiveLabForAdmin')
            .then((res) => {
                const data = res?.data
                console.log('lab data ', data);
                if (data?.length > 0) {
                    let newA = []
                    data.map((itm) => {
                        newA.push({
                            label: itm.name,
                            value: itm.name,
                            id: itm._id
                        })
                    })
                    setLabs(newA)
                }
                dispatch(hide())
            })
    }

    const getTestsByLabId = (doc) => {
        dispatch(show())
        http
            .get(`api/labTest/${doc?.id}`)
            .then((res) => {
                const data = res?.data
                if (data?.length > 0) {
                    let newA = []
                    data.map((itm) => {
                        newA.push({
                            label: itm?.testName,
                            value: itm?.testName,
                            id: itm?._id
                        })
                    })
                    setLabTests(newA)
                }
                dispatch(hide())
            })
    }

    const changeFormState = (key, value) => {
        setFormState(state => ({
            ...state,
            [key]: value
        }))
    }

    const validateEmail = (email) => {
        let reg = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/);
        return reg.test(email)
    }

    const UploadBtn = ({ title, onPress }) => {
        return <TouchableOpacity style={styles.btnStyle} onPress={onPress}>
            <Text style={styles.btnTextStyle}>{title}</Text>
        </TouchableOpacity>
    }

    const onPressModal = (selectedItem) => {
        if (selectedItem) {
            let value = selectedItem?.text
            if (value == 'Camera') {
                requestPermissionCamera()
                    .then((status) => {
                        if (status == true) {
                            _handleSelectCamera()
                        }
                    })
            } else {
                requestPermissionStorage()
                    .then((status) => {
                        if (status == true) {
                            _handleSelectDoc()
                        }
                    })
            }
        }
    }

    const _handleSelectDoc = async () => {

        try {
            launchImageLibrary({
                selectionLimit: 1
            }, (results) => {

                if (results.didCancel) return

                const res = results.assets[0]
                let selImageObj = {
                    uri: res.uri,
                    type: res.type,
                    name: res.fileName,
                    size: res.fileSize
                }
                setSelectedImages([...selectedImages, selImageObj])
            });
        } catch (error) {
        }

    }

    const _handleSelectCamera = async () => {

        try {
            launchCamera({
                mediaType: 'photo',
                cameraType: 'back',
            }, (results) => {

                if (results.didCancel) return

                const res = results.assets[0]
                let selImageObj = {
                    uri: res.uri,
                    type: res.type,
                    name: res.fileName,
                    size: res.fileSize
                }
                setSelectedImages([...selectedImages, selImageObj])
            });
        } catch (error) {
        }

    }

    const _removeDocFunc = (indexToRemove) => {
        const newArr = selectedImages;
        if (newArr.length > 0) {
            const newArray = [
                ...newArr.slice(0, indexToRemove),
                ...newArr.slice(indexToRemove + 1),
            ];
            setSelectedImages(newArray)
        }
    }

    const onItemSelected = useCallback((ii) => {
        setSelectlab(ii)
    })
    const onItemSelected2 = useCallback((ii) => {
        setSelectTest(ii)
    })

    const createFormData = (docs) => {
        let imgName = "";
        const data = new FormData();
        docs.map((photo, index) => {
            if (photo.fileName === undefined) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else if (photo.fileName === null) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else {
                imgName = photo.fileName;
            }
            let newFile = {
                name: Platform.OS === 'android' ? photo.name : imgName,
                type: photo.type,
                uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
            }
            data.append("multi-files", newFile);
        })
        return data;
    };

    const onPressContinue = () => {
        if (formState.fullName == "" || formState.email == "" || formState.mobileNo == "" || formState.address == "" || formState.locality == "" || formState.city == "" || formState.state == "" || formState.pincode == "") {
            dispatch(showModal(errorPayload("All Fields are required!")))
            return
        }

        if (selectLab == null) {
            dispatch(showModal(errorPayload("Please Select Lab")))
            return
        }
        if (selectTest == null) {
            dispatch(showModal(errorPayload("Please Select Test")))
            return
        }

        if (formState.mobileNo.length != 10) {
            dispatch(showModal(errorPayload("Mobile number should be 10 digits")))
            return
        }

        let userID = userProfile?.userId?._id
        let body = {
            "name": formState.fullName,
            "userId": userID,
            "contactNumber": formState.mobileNo,
            "email": formState.email,
            "address": formState.address,
            "locality": formState.locality,
            "landmark": formState.landmark,
            "city": formState.city,
            "state": formState.state,
            "pincode": formState.pincode,
            "selectTest": selectTest?.value,
        }

        console.log('body ', body);
        // return
        dispatch(show())
        if (selectedImages.length > 0) {
            dispatch(uploadAttachment(
                createFormData(selectedImages),
                userID,
                (data) => {
                    let updatedBody = {
                        ...body,
                        prescription: data?.imageUrl
                    }
                    console.log('updatedBody ', JSON.stringify(updatedBody));
                    subMitrequestApiCall(updatedBody)
                }
            ))
        } else {
            subMitrequestApiCall(body)
        }

    }

    const subMitrequestApiCall = (body) => {
        dispatch(show())
        http
            .post('api/labTestForms', body, dispatch)
            .then((res) => {
                if (res?.status == 200) {
                    props?.navigation?.navigate("RequestReceived")
                    dispatch(hide())
                } else {
                    dispatch(hide())
                }
            })
    }

    return (
        <View style={styles.contianer}>
            <HeaderSehet
                headerText={"Lab Test Form"}
                navigation={props?.navigation}
                changeTheme={true}
            />

            <SafeAreaView style={styles.mainContainer}>

                <Content>
                    <Form style={{ marginHorizontal: 10, marginBottom: 10 }}>
                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.full_name} *`} </Label>
                            <Input
                                value={formState.fullName}
                                returnKeyType="done"
                                style={styles.marginItem}
                                numberOfLines={2}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("fullName", value)} />
                        </Item>

                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.contact_number} *`} </Label>
                            <Input
                                value={formState.mobileNo}
                                returnKeyType="done"
                                style={styles.marginItem}
                                keyboardType='number-pad'
                                maxLength={10}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("mobileNo", value)} />
                        </Item>

                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.email} *`} </Label>
                            <Input
                                value={formState.email}
                                returnKeyType="done"
                                style={styles.marginItem}
                                keyboardType='email-address'
                                numberOfLines={2}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("email", value)} />
                        </Item>

                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.address} *`} </Label>
                            <Input
                                value={formState.address}
                                returnKeyType="done"
                                style={styles.marginItem}
                                numberOfLines={2}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("address", value)} />
                        </Item>

                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.locality} *`} </Label>
                            <Input
                                value={formState.locality}
                                returnKeyType="done"
                                style={styles.marginItem}
                                numberOfLines={2}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("locality", value)} />
                        </Item>

                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.landmark} *`} </Label>
                            <Input
                                value={formState.landmark}
                                returnKeyType="done"
                                style={styles.marginItem}
                                numberOfLines={2}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("landmark", value)} />
                        </Item>

                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <Item
                                style={{ flex: 1 }}
                                floatingLabel>
                                <Label style={styles.labelStyle}>{`${strings.city_only} *`} </Label>
                                <Input
                                    value={formState.city}
                                    returnKeyType="done"
                                    style={styles.marginItem}
                                    numberOfLines={2}
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    onChangeText={(value) => changeFormState("city", value)} />
                            </Item>

                            <Item
                                style={{ flex: 1 }}
                                floatingLabel>
                                <Label style={styles.labelStyle}>{`${strings.state} *`} </Label>
                                <Input
                                    value={formState.state}
                                    returnKeyType="done"
                                    style={styles.marginItem}
                                    numberOfLines={2}
                                    autoCorrect={false}
                                    autoCapitalize="none"
                                    onChangeText={(value) => changeFormState("state", value)} />
                            </Item>
                        </View>

                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`Pincode *`} </Label>
                            <Input
                                value={formState.pincode}
                                returnKeyType="done"
                                style={styles.marginItem}
                                keyboardType='number-pad'
                                maxLength={6}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("pincode", value)} />
                        </Item>


                        <DropDownPicker
                            style={{
                                borderWidth: 0,
                                marginTop: 15,
                                backgroundColor: '#FFF',
                                paddingLeft: 15,
                            }}
                            open={open}
                            value={selectLab?.value}
                            items={labs}
                            setOpen={setOpen}
                            onSelectItem={onItemSelected}
                            dropDownDirection="TOP"
                            placeholderStyle={{
                                color: '#041A32',
                                fontSize: 16,
                            }}
                            placeholder="Select Lab"
                            dropDownContainerStyle={{
                                borderColor: '#D9DCE5',
                                paddingLeft: 5,
                                backgroundColor: '#F5F5F5'
                            }}
                            textStyle={{
                                fontSize: 16
                            }}
                        />
                        <Line lineColor={"#A0A9BE"} style={{ marginHorizontal: 10 }} />

                        <DropDownPicker
                            style={{
                                borderWidth: 0,
                                marginTop: 15,
                                backgroundColor: '#FFF',
                                paddingLeft: 15,
                            }}
                            open={open2}
                            value={selectTest?.value}
                            items={labTest}
                            setOpen={setOpen2}
                            onSelectItem={onItemSelected2}
                            dropDownDirection="AUTO"
                            placeholderStyle={{
                                color: '#041A32',
                                fontSize: 16,
                            }}
                            placeholder="Select Test"
                            dropDownContainerStyle={{
                                borderColor: '#D9DCE5',
                                paddingLeft: 5,
                                backgroundColor: '#F5F5F5'
                            }}
                            textStyle={{
                                fontSize: 16
                            }}
                        />

                        <Line lineColor={"#A0A9BE"} style={{ marginHorizontal: 10 }} />

                    </Form>

                    <View style={styles.uploadCont}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', marginBottom: 10, }}>

                            {
                                selectedImages.length > 0
                                    ? selectedImages?.map((doc, index) => (
                                        <View>
                                            {
                                                <Image
                                                    source={
                                                        doc.uri != null && doc.type === "image/jpeg"
                                                            ? { uri: doc.uri }
                                                            : Images.upload_doc_icon
                                                    }
                                                    style={{ height: 90, width: 90, borderRadius: 15, marginHorizontal: 10 }}
                                                />
                                            }
                                            <TouchableOpacity
                                                onPress={() => _removeDocFunc(index)}
                                                style={{ position: 'absolute', right: 0 }}
                                            >
                                                <Icon
                                                    name="circle-with-cross"
                                                    type="Entypo"
                                                    style={{ color: 'red', }}
                                                />
                                            </TouchableOpacity>
                                        </View>
                                    ))
                                    : <Image
                                        style={styles.imageStyle}
                                        source={Images.upload_doc_icon}
                                    />
                            }
                        </View>
                        <UploadBtn title={strings.upload_prescription} onPress={() => dispatch(setPhotoPickerModal(true))} />
                    </View>

                </Content>

                <View style={{ marginHorizontal: 20 }}>
                    <AppButton
                        title={strings.place_order}
                        style={{
                            borderRadius: 50,
                            backgroundColor: '#D72820',
                            marginBottom: 10,
                        }}
                        onPress={onPressContinue}
                    />
                </View>


            </SafeAreaView>

            <PhotoPickerModal visible={photoPickerModal} filter={photoPickerTitles} onPress={onPressModal} />

        </View>
    )
}

export default index

const styles = StyleSheet.create({
    contianer: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    marginItem: {
        marginTop: 3,
        fontSize: 15,
    },
    formtitle: {
        fontSize: 16,
        color: '#041A32',
        fontWeight: 'bold',
        marginHorizontal: 15,
        marginTop: 10,
    },
    labelStyle: { fontSize: 16 },
    checkBoxStyle: {
        backgroundColor: 'red',
        marginRight: 5,
        borderRadius: 5,
        alignSelf: 'flex-start'
    },
    modalTextStyle: {
        color: '#080A72'
    },
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between'
    },
    genderPicker: {
        marginTop: 20,
        alignItems: 'center',
        // justifyContent: 'center',
        // flexDirection: 'row'
    },
    imageStyle: {
        height: 90,
        width: 90,
        resizeMode: 'contain',
        borderRadius: 10
    },
    uploadCont: {
        margin: 20,
    },
    btnStyle: {
        flex: 1,
        // backgroundColor: '#FFF',
        borderWidth: 1,
        borderColor: '#D72820',
        padding: 10,
        borderRadius: 50,
        marginHorizontal: 10,
    },
    btnTextStyle: {
        color: '#D72820',
        textAlign: 'center',
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: 16,
    },
})