import { SafeAreaView, StyleSheet, Text, View, Image, TouchableOpacity, Platform } from 'react-native'
import React from 'react'
import Images from '../../../assets/images'
import AppButton from '../../../components/AppButton'
import { useBackHandler } from '../../../hooks/usebackhandler'
import { strings } from '../../../utils/translation'

const index = (props) => {

    const onPressBack = () => {
        props.navigation.navigate('Home')
    }

    if (Platform.OS == 'android') {
        useBackHandler(onPressBack, true)
    }
    return (
        <View style={styles.contstyle}>
            <SafeAreaView style={{ flex: 1 }}>
                <View style={{ flex: 1, justifyContent: 'center', }}>
                    <View style={{ alignItems: 'center' }}>
                        <Image
                            source={Images.order_request}
                            style={styles.imageStyle}
                        />
                        <Text style={styles.title1}>{strings.medicine_order_success_title1}</Text>
                        <Text style={styles.title2}>{strings.medicine_order_success_title2}</Text>
                    </View>
                    <AppButton
                        style={{
                            backgroundColor: '#D72820',
                            marginHorizontal: 20,
                        }}
                        title="Back to Home Page"
                        onPress={onPressBack}
                    />
                </View>
            </SafeAreaView>
        </View>
    )
}

export default index

const styles = StyleSheet.create({
    contstyle: {
        flex: 1,
        backgroundColor: '#484F5F'
    },
    imageStyle: {
        height: 150,
        width: '100%',
        resizeMode: 'contain',
    },
    title1: {
        fontSize: 22,
        textAlign: 'center',
        color: '#FFF',
        marginVertical: 10,
    },
    title2: {
        fontSize: 14,
        textAlign: 'center',
        color: '#8993A7',
        marginVertical: 15,
    },
})