import React, { useState } from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList, TextInput } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { createPaytmTxnTokenApi } from '../../../../actions/membership'
import { showModal } from '../../../../actions/modal'
import { getDocProfile } from '../../../../actions/sehet/user-action'
import Images from '../../../../assets/images'
import HeaderSehet from '../../../../components/header-common'
import ViewProfileModal from '../../../../components/view-profile-modal'
import http from '../../../../services/http'
import { FONT_FAMILY_SF_PRO_REGULAR } from '../../../../styles/typography'
import { DOC_PROFILE } from '../../../../utils/constant'
import { errorPayload } from '../../../../utils/helper'
import { ifValid } from '../../../../utils/ifNotValid'
import { hide, show } from '../../../../utils/loader/action'
import { strings } from '../../../../utils/translation'

const Index = (props) => {
    const plan_data = useSelector(state => state.directLogin.planDetails)

    const benefitsOfPlan = plan_data?.benifits
    const doctors = plan_data?.doctorId


    const [refCode, setRefCode] = useState('')
    const [showDoctorProfile, setShowDoctorProfile] = useState(false)
    const dispatch = useDispatch()

    const onPressBuyNow = () => {
        dispatch(show())
        http
            .get(`api/checkPlanSubscription/${plan_data?._id}`, dispatch)
            .then(res => {
                console.log({ res });
                if (res?.status === 200) {
                    // add plan to card
                    const data = {
                        ...plan_data,
                        referenceCode: refCode,
                        newCard: false,
                        subId: res?.data
                    }
                    dispatch(createPaytmTxnTokenApi({ price: plan_data?.price }, data))
                }
                else if (res?.status === 201) {
                    // create new card
                    const data = {
                        ...plan_data,
                        referenceCode: refCode,
                        newCard: true
                    }
                    dispatch(createPaytmTxnTokenApi({ price: plan_data?.price }, data))
                }
                else if (res?.status === 204) {
                    dispatch(showModal(errorPayload(res?.message)))
                    dispatch(hide())
                }
                else if (res?.status === 206) {
                    dispatch(showModal(errorPayload(strings.pleaseUpdateprofile)))
                    setTimeout(() => {
                        props.navigation.navigate("EditPatientProfileScreen", { fromCard: true });
                    }, 2000);
                    dispatch(hide())
                }
                else {
                    dispatch(hide())
                }
            })
            .catch(err => {
                dispatch(hide())
            })
    }

    return (
        <View style={styles.container}>
            <HeaderSehet headerText={plan_data?.name} navigation={props.navigation} changeTheme={true} />
            <View style={{ flex: 1, justifyContent: 'space-between' }}>
                <ScrollView>
                    <View style={styles.sectionContainer}>
                        {doctors?.length > 0 && <Text style={styles.sectionTitleStyle}>{strings.Doctors}</Text>}
                        <FlatList
                            data={doctors}
                            style={{ marginVertical: 8 }}
                            keyExtractor={(item, index) => index?.toString()}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            renderItem={({ item }) => {
                                const imageUrl = ifValid(item?.imageUrl) ? { uri: item?.imageUrl } : Images.amar_logo

                                const onPressDoctor = () => {
                                    dispatch(getDocProfile(item?._id))
                                    setShowDoctorProfile(!showDoctorProfile)
                                }
                                return (
                                    <TouchableOpacity
                                        onPress={onPressDoctor}
                                        activeOpacity={1}
                                        style={{
                                            marginHorizontal: 8,
                                            padding: 8,
                                            borderWidth: 2,
                                            borderColor: item?.selected ? '#FF5E38' : '#F9F9F9',
                                        }}>
                                        <Image
                                            source={imageUrl}
                                            style={{
                                                height: 75,
                                                width: 75,
                                                borderRadius: 75,
                                            }}
                                        />
                                        <Text style={{
                                            width: 80,
                                            textAlign: 'center',
                                            fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
                                            marginVertical: 3,
                                            fontWeight: 'bold'
                                        }}>{item?.fullName}</Text>
                                    </TouchableOpacity>
                                )
                            }}
                        />
                    </View>

                    <View style={[styles.sectionContainer, { marginBottom: 5 }]}>
                        <Text style={[styles.sectionTitleStyle, { marginVertical: 8 }]}>{strings.Benefits_of_card}</Text>
                        {
                            benefitsOfPlan?.map(ii => (
                                <View style={{ marginVertical: 5, flexDirection: 'row', marginHorizontal: 3, }}>
                                    <Text style={{ fontSize: 8, color: '#FF5E38', paddingTop: 3 }}>{`\u2B24`}</Text>
                                    <Text style={{ fontSize: 14, marginHorizontal: 5 }}>{`${ii}`}</Text>
                                </View>
                            ))
                        }
                    </View>

                    <View style={styles.sectionContainer}>
                        <Text style={styles.sectionTitleStyle}>{strings.plan_price}</Text>
                        <Text style={{ fontSize: 15, marginVertical: 3 }}>{`₹ ${plan_data?.price}`}</Text>
                    </View>

                    <View style={styles.sectionContainer}>
                        <Text style={[styles.sectionTitleStyle, { paddingHorizontal: 0, paddingVertical: 5 }]}>{strings.reference_code}</Text>
                        <TextInput
                            value={refCode}
                            placeholder={strings.enter_here}
                            placeholderTextColor="#484F5F"
                            style={styles.textInputStyle}
                            autoCorrect={false}
                            autoCapitalize="characters"
                            onChangeText={text => setRefCode(text)}
                        />
                    </View>

                </ScrollView>

                <TouchableOpacity
                    style={styles.btnStyle}
                    onPress={onPressBuyNow}
                >
                    <Text style={styles.btnTextStyle}>{strings.buy_now}</Text>
                </TouchableOpacity>
            </View>
            <ViewProfileModal
                onTouchOutside={() => {
                    setShowDoctorProfile(!showDoctorProfile)
                }}
                onHardwareBackPress={() => {
                    dispatch({ type: DOC_PROFILE, payload: {} });
                    setShowDoctorProfile(!showDoctorProfile)
                }}
                specialization={null}
                name={undefined}
                experience={undefined}
                imageUrl={""}
                visible={showDoctorProfile} />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },
    imageStyle: {
        height: 200,
        width: '100%',
        resizeMode: 'contain',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10,
        marginVertical: 10
    },
    detailContainer: {
        margin: 10,
        padding: 10,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 5
    },
    sectionCOntainer: {
        flexDirection: 'row',
        marginHorizontal: 20,
        paddingVertical: 15,
        borderBottomWidth: 1,
        borderColor: '#9c9c9c'
    },
    titlestyle: {
        flexGrow: 1,
        fontSize: 16,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
    },
    titleValstyle: {
        fontSize: 16,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR
    },
    btnStyle: {
        backgroundColor: '#D72820',
        padding: 10,
        borderRadius: 8,
        margin: 10,
    },
    btnTextStyle: {
        textAlign: 'center',
        fontSize: 16,
        color: '#fff'
    },
    planTitle: {
        fontSize: 20,
        marginHorizontal: 20,
        paddingVertical: 10,
        fontWeight: 'bold',
        textAlign: 'center',
        borderBottomWidth: 1,
        borderColor: '#9c9c9c'
    },
    refCodeContainer: {
        marginHorizontal: 10,
    },
    sectionTitleStyle: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: 'bold',
    },
    sectionContainer: {
        paddingTop: 5,
        paddingBottom: 15,
        marginHorizontal: 15,
        borderBottomWidth: 1,
        borderColor: '#707070'
    },
    textInputStyle: {
        marginVertical: 5,
        borderRadius: 8,
        height: 50,
        borderColor: '#9c9c9c',
        borderWidth: 1,
        paddingLeft: 10
    }
})

export default Index