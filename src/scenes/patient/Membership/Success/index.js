import React, { useEffect, useState } from "react";
import { Image, View, BackHandler, TouchableOpacity, Platform, Alert } from "react-native";
import { Button, Text } from "native-base";
import styles from "./style";
import Images from "../../../../assets/images";
import PowerTranslator from "react-native-power-translator";
import ImageContainer from "../../../../components/ImageContainer";
import { strings } from "../../../../utils/translation";
import { FONT_FAMILY_SF_PRO_MEDIUM } from "../../../../styles/typography";
import { useDispatch, useSelector } from "react-redux";
import { showModal } from "../../../../actions/modal";
import { ifValid, isValid } from "../../../../utils/ifNotValid";
import RNFetchBlob from 'rn-fetch-blob'
import { v4 as uuidv4 } from 'uuid';
import { checkMultiple, PERMISSIONS, RESULTS, openSettings, requestMultiple } from 'react-native-permissions'
import { hide, show } from "../../../../utils/loader/action";
import { isArray } from "validate.js";

const Success = (props) => {

    const plan_data = props.navigation?.state?.params?.planData
    const subObj = useSelector(state => state.directLogin.subObj)
    const imgSrc = isValid(plan_data?.doctorId) && plan_data?.doctorId?.length > 0
        ? { uri: plan_data?.doctorId[0]?.imageUrl }
        : Images?.amar_logo

    const dispatch = useDispatch()

    useEffect(() => {
        const handleValidateClose = () => {
            props.navigation.navigate('Home')
            return true;
        };
        const handler = BackHandler.addEventListener('hardwareBackPress', handleValidateClose);
        return () => handler.remove();
    }, [])

    const showSettingsAlert = () => {
        Alert.alert(
            "Permission required!",
            "Please click open settings and allow storage permission under Permissions",
            [
                {
                    text: "Open Settings",
                    onPress: () => openSettings(),
                    style: "cancel",
                },
            ],
            {
                cancelable: true,
            }
        );
    }

    const downloadImages = () => {
        const prms = [];
        let downloadImgs = subObj?.imagePath
        if (downloadImgs.length > 0) {
            downloadImgs?.map(downUrl => {
                const promise = new Promise((resolve, reject) => {
                    const dirs = RNFetchBlob.fs.dirs.DownloadDir;
                    let fileName = `${Date.now() + Math.random()}`
                    const localPath = dirs + `/${fileName}.jpg`
                    RNFetchBlob
                        .config({
                            path: localPath,
                            addAndroidDownloads: {
                                useDownloadManager: true,
                                path: localPath,
                                notification: true,
                                mediaScannable: true,
                                // title: 'sehet card downloaded'
                            }
                        })
                        .fetch('GET', downUrl)
                        .progress((received, total) => {
                            let percentage = (received / total) * 100
                            let roundPercentage = Math.round(percentage)
                            console.log('progress', roundPercentage)
                        })
                        .then((res) => {
                            resolve(res?.path())
                        })
                        .catch((err) => {
                            console.log('err  ', err)
                            reject(err)
                        })
                })
                prms.push(promise)
            })

            Promise.all(prms)
                .then(results => {
                    dispatch(hide())
                    const payload = {
                        text: `Card downloaded on your device`,
                        subtext: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    }
                    dispatch(showModal(payload))
                    console.log('results ', results);
                })
                .catch(err => {
                    console.log(err);
                    dispatch(hide())
                })
        }
    }

    const onPressDownloadCard = async () => {
        if (ifValid(subObj?.imagePath)) {
            dispatch(show())
            if (Platform.Version > 32) {
                downloadImages()
            } else {
                const typess = [PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE, PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]
                const permiss = await checkMultiple(typess)

                if (permiss["android.permission.READ_EXTERNAL_STORAGE"] === RESULTS.GRANTED && permiss["android.permission.WRITE_EXTERNAL_STORAGE"] === RESULTS.GRANTED) {
                    downloadImages()
                } else if (permiss["android.permission.READ_EXTERNAL_STORAGE"] === RESULTS.BLOCKED && permiss["android.permission.WRITE_EXTERNAL_STORAGE"] === RESULTS.BLOCKED) {
                    dispatch(hide())
                    showSettingsAlert()
                } else {
                    requestMultiple(typess)
                        .then(() => downloadImages())
                        .catch(err => {
                            console.log(err);
                            dispatch(hide())
                        })
                }
            }
        }
    }

    const onPressViewReceipt = () => {
        props.navigation?.navigate("PaymentReceiptModal", {
            url: subObj?.receiptUrl,
        })
    }


    return (
        <ImageContainer>
            <View style={styles.container}>
                <View style={[styles.Image, { marginTop: '20%' }]}>
                    <Image
                        source={imgSrc}
                        style={styles.docImg}
                    />
                    <View
                        style={styles.cameraBtn}>
                        <Image source={Images.checkCircle} style={{ width: 30, height: 30 }} />
                    </View>
                </View>

                <View style={styles.content}>
                    <Text style={[styles.contentText, { fontSize: 20 }]}>
                        {strings.mem_success_title}
                    </Text>
                    <View style={{ backgroundColor: '#FFF', paddingHorizontal: 5, marginVertical: 15, paddingVertical: 10, borderRadius: 10 }}>
                        <Text style={{ textAlign: 'center', color: '#041A32', fontSize: 22, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }}>{strings.plan_details}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Text style={{ color: '#484F5F', fontSize: 17 }}>
                                <Image
                                    source={Images.icon_clock}
                                    style={{ height: 20, width: 20 }}
                                />
                                <Text>  {plan_data?.name}</Text>
                                <Text> | </Text>
                                <Text>₹ {plan_data?.price}</Text>
                            </Text>

                        </View>
                    </View>
                    <Text style={{ color: 'white', fontSize: 13, textAlign: 'center' }}>{strings.subContextText}</Text>
                </View>

                <TouchableOpacity
                    style={{
                        backgroundColor: '#FF5E38',
                        width: '90%',
                        padding: 12,
                        borderRadius: 8,
                        marginVertical: 25
                    }}
                    onPress={onPressDownloadCard}
                >
                    <Text uppercase={false} style={[styles.number91, { color: 'white', textAlign: 'center' }]}>{strings.download_card}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={{
                        backgroundColor: '#FF5E38',
                        width: '90%',
                        padding: 12,
                        borderRadius: 8,
                    }}
                    onPress={onPressViewReceipt}
                >
                    <Text uppercase={false} style={[styles.number91, { color: 'white', textAlign: 'center' }]}>{strings.view_receipt}</Text>
                </TouchableOpacity>
            </View>
        </ImageContainer>
    );
};

export default Success