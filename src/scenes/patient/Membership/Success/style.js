import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_SIZE_14,
    FONT_SIZE_20
} from "../../../../styles/typography";

export default (styles = StyleSheet.create({
    container: {
        alignItems: "center",
    },
    docImg: {
        height: 120,
        width: 120,
        borderRadius: 120 / 2,
    },
    Image: {
        backgroundColor: "#A0A9BE",
        borderRadius: 100,
        marginTop: hp("20%"),
    },
    content: {
        marginTop: hp("6%"),
        marginHorizontal: 20,
    },
    contentText: {
        color: "#FFFFFF",
        fontSize: 22,
        textAlign: "center",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
    },
    subContentText: {
        marginTop: 15,
        textAlign: "center",
        fontSize: FONT_SIZE_14,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
    },
    Btn: {
        marginTop: 50,
        backgroundColor: '#FF5E38',
        justifyContent: "center",
        borderRadius: 10,
    },
    cameraBtn: {
        position: "absolute",
        bottom: 0,
        right: 10,
    },
}));
