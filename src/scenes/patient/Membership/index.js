import React, { useEffect } from 'react'
import { View, Text, FlatList, StyleSheet, Image, TouchableOpacity, Dimensions, ImageBackground } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { setFirebaseMemData, setPlanDetails, setSehetCardFlow } from '../../../actions/membership.js'
import { showModal } from '../../../actions/modal.js'
import Images from '../../../assets/images/index.js'
import HeaderSehet from '../../../components/header-common'
import { getSkipped } from '../../../services/auth.js'
import { FONT_FAMILY_BOLD, FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_SemiBold } from '../../../styles/typography.js'
import { getCardsWithbgColor } from '../../../utils/helper.js'
import { ifValid, isValid } from '../../../utils/ifNotValid.js'
import { strings } from '../../../utils/translation/index.js'
import firestore from '@react-native-firebase/firestore'
import moment from 'moment'

const Index = (props) => {
    const dispatch = useDispatch()
    const allPlans = useSelector(state => state.directLogin.membershipCards)

    const onPressView = async (item) => {
        let skip = await getSkipped();
        if (skip === "false") {
            props.navigation.navigate('PlanDetails')
            dispatch(setPlanDetails(item))
        } else {
            dispatch(setPlanDetails(item))
            dispatch(setSehetCardFlow(true))
            const payload = {
                text: `${strings.please_login_to_continue}`,
                subtext: "",
                iconName: "closecircleo",
                modalVisible: true,
            }
            dispatch(showModal(payload))
            props.navigation.navigate('PatientLogin')
        }
    }

    const CardItem = ({ item, index }) => {
        const cardBgImage = isValid(item?.backgroundUrl) ? { uri: item?.backgroundUrl } : Images.sehet_card_bg;
        const planImage = isValid(item?.imageUrl) ? { uri: item?.imageUrl } : Images.amar_logo;
        const planName = isValid(item?.name) ? item?.name : '-';
        const planKinds = isValid(item?.kinds) ? item?.kinds : '-';
        const planColor = isValid(item?.colorCode) ? item?.colorCode : '#000000';

        return <TouchableOpacity
            activeOpacity={1}
            style={styles.planContainer}
            onPress={() => onPressView(item)}
        >

            <View style={{ flex: 1, }}>
                <View style={[styles.card, { margin: 15 }]}>

                    <ImageBackground
                        style={{
                            height: 200,
                            width: '100%',
                        }}
                        imageStyle={{ borderRadius: 10 }}
                        resizeMode="cover"
                        source={cardBgImage}
                    >
                        {/* <Text style={[styles.kindText, { color: planColor }]}>{planKinds}</Text> */}

                        <View style={styles.con2}>
                            <View style={styles.con1}>
                                <Text style={styles.planName}>{planName}</Text>
                                {/* <Image
                                    source={planImage}
                                    style={styles.planImage}
                                /> */}
                            </View>
                        </View>

                    </ImageBackground>

                </View>

            </View>

        </TouchableOpacity>
    }

    return (
        <View style={styles.container}>
            <HeaderSehet headerText={strings.membership_plans} navigation={props.navigation} changeTheme={true} />

            <FlatList
                data={allPlans}
                keyExtractor={(item, index) => index?.toString()}
                renderItem={({ item, index }) => (
                    <CardItem item={item} index={index} />
                )}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },
    card: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        margin: 15,
        paddingVertical: 10,
        paddingHorizontal: 15,
        backgroundColor: 'white',
        borderRadius: 10,
        shadowColor: 'grey',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 5
    },
    imageStyle: {
        height: 180,
        width: '100%',
        // resizeMode: 'contain',
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10
    },
    planContainer: {
        flex: 1,
        justifyContent: 'space-between'
    },
    card: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        elevation: 3,
        shadowColor: 'grey',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
    },
    kindText: {
        backgroundColor: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        width: '100%',
        textTransform: 'uppercase',
        textAlign: 'center',
        position: 'absolute',
        bottom: 20,
        paddingVertical: 3
    },
    con2: {
        position: 'absolute',
        width: '100%',
        bottom: 65,
    },
    con1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 25
    },
    planName: {
        fontSize: 18,
        fontFamily: FONT_FAMILY_SF_PRO_BOLD,
        color: '#000',
        marginTop: 10
    },
    planImage: {
        height: 50,
        width: 50,
        borderRadius: 50,
    },
})

export default Index