import moment from 'moment'
import React, { useEffect } from 'react'
import { StyleSheet, Text, View, Image, FlatList, Dimensions, ScrollView, TouchableOpacity, ImageBackground, Clipboard, ToastAndroid } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { isArray } from 'validate.js'
import { getAllMebershipPlans, getMyCardDetails } from '../../../actions/membership'
import Images from '../../../assets/images'
import CircularProgress from '../../../components/CircularProgress'
import HeaderSehet from '../../../components/header-common'
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_SF_PRO_SemiBold } from '../../../styles/typography'
import { isValid } from '../../../utils/ifNotValid'
import { strings } from '../../../utils/translation'
import IconCopy from 'react-native-vector-icons/Feather'

const index = (props) => {
    const dispatch = useDispatch()
    const width = Dimensions.get('screen').width
    const myHealthCard = useSelector(state => state.profile.myHealthCard)
    const isLoading = useSelector(state => state.loader.isLoading)

    const Cards = isArray(myHealthCard?.imagePath) ? myHealthCard?.imagePath?.reverse() : []

    useEffect(() => {
        dispatch(getMyCardDetails())
    }, [])

    const CardImage = ({ item }) => {
        return <View
            style={[styles.card, { marginRight: 10 }]}
        >
            <Image
                style={{
                    height: 200,
                    width: width - 50,
                    borderRadius: 10,
                }}
                resizeMode="contain"
                source={{ uri: item }}
            />
        </View>
    }

    const CurrentPlan = ({ item }) => {

        const dateFormat = 'DD/MM/YYYY'

        const percent = isValid(item?.countsTaken) ? item?.countsTaken * 10 : 0;
        const cardBgImage = isValid(item?.plan?.backgroundUrl) ? { uri: item?.plan?.backgroundUrl } : Images.sehet_card_bg;
        const planImage = isValid(item?.plan?.imageUrl) ? { uri: item?.plan?.imageUrl } : Images.amar_logo;
        const planName = isValid(item?.plan?.name) ? item?.plan?.name : '-';
        const planKinds = isValid(item?.plan?.kinds) ? item?.plan?.kinds : '-';
        const planColor = isValid(item?.plan?.colorCode) ? item?.plan?.colorCode : '#000000';
        const startDate = moment(item?.buyingDate).format(dateFormat);
        const endDate = moment(item?.buyingDate).add(1, 'years').format(dateFormat);
        const totalCount = isValid(item?.plan?.count) ? item?.plan?.count : 10;

        const compareDate = moment(new Date(), dateFormat);
        const sDate = moment(startDate, dateFormat);
        const eDate = moment(endDate, dateFormat);
        const isBetween = compareDate.isBetween(sDate, eDate)

        const cardStatus = isBetween ? 'Active' : 'Expried'

        return <View style={styles.planContainer}>

            <View style={{ flex: 1, }}>
                <View style={[styles.card, { marginRight: 10 }]}>

                    <ImageBackground
                        style={{
                            height: 200,
                            width: width - 50,
                        }}
                        imageStyle={{ borderRadius: 10 }}
                        resizeMode="cover"
                        source={cardBgImage}
                    >
                        <Text style={[styles.kindText, { color: planColor }]}>{planKinds}</Text>

                        <View style={styles.con2}>
                            <View style={styles.con1}>
                                <Text style={styles.planName}>{planName}</Text>
                                <Image
                                    source={planImage}
                                    style={styles.planImage}
                                />
                            </View>
                        </View>

                    </ImageBackground>

                    <View
                        style={{
                            position: 'absolute',
                            top: 0,
                            right: 0,
                            backgroundColor: cardStatus === 'Active' ? '#FFFFFF' : '#382F2D',
                            paddingHorizontal: 15,
                            paddingVertical: 5,
                            borderTopRightRadius: 10,
                            borderBottomLeftRadius: 10
                        }}
                    >
                        <Text style={{
                            color: cardStatus === 'Active' ? planColor : '#FFFFFF',
                            fontSize: 15
                        }}>{cardStatus}</Text>
                    </View>

                </View>

                <Text style={styles.sectionSubTitle}>Plan Details</Text>

                <View style={[styles.card, { width: width - 50, padding: 10 }]}>
                    <View style={styles.containerText}>
                        <Text>Total Consultation</Text>
                        <Text style={styles.textStyle}>{totalCount}</Text>
                    </View>
                    <View style={styles.containerText}>
                        <Text>Start Date</Text>
                        <Text style={styles.textStyle}>{startDate}</Text>
                    </View>
                    <View style={styles.containerText}>
                        <Text>End Date</Text>
                        <Text style={styles.textStyle}>{endDate}</Text>
                    </View>

                    <View style={styles.ciruclaProg} >
                        <CircularProgress
                            size={150}
                            strokeWidth={10}
                            text={`${percent / 10}/${totalCount}`}
                            progressPercent={percent}
                            totalPercent={totalCount * 10}
                            bgColor={"#FBDAC7"}
                            pgColor={"#FF5E38"}
                        />
                    </View>
                </View>

                <Text style={styles.sectionTitle}>Benefits of plan</Text>
                {item?.plan?.benifits?.map(benefit => (
                    <View style={styles.benefit}>
                        <Text style={styles.redDot}>{`\u2B24`}</Text>
                        <Text style={{ fontSize: 15, width: width - 50 }}>{benefit}</Text>
                    </View>
                ))}
            </View>

            <TouchableOpacity
                style={styles.btnStyle}
                onPress={onPressCheckPlans}
            >
                <Text style={styles.btnTextStyle}>Check other plans</Text>
            </TouchableOpacity>

        </View>
    }

    const onPressCheckPlans = () => {
        props.navigation.navigate("Membership")
        dispatch(getAllMebershipPlans())
    }

    const onPressCopy = (val) => {
        Clipboard.setString(val)
        ToastAndroid.show('Membership Code Copied...', ToastAndroid.SHORT)
    }

    return (
        <View style={styles.bg}>
            <HeaderSehet headerText={strings.my_health_card} navigation={props?.navigation} changeTheme={true} />
            {
                myHealthCard?.planDetails?.length > 0 ? <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={styles.mainContainer}>
                        <FlatList
                            keyExtractor={(item, index) => index?.toString()}
                            data={Cards}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            renderItem={({ item }) => {
                                return <CardImage item={item} />
                            }}
                        />

                        <TouchableOpacity
                            onPress={() => onPressCopy(myHealthCard?.uniqueId)}
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                marginTop: 10,
                                alignSelf: 'flex-start'
                            }}
                        >
                            <IconCopy
                                name='copy'
                                size={28}
                            />
                            <Text style={[styles.sectionTitle, { fontSize: 16 }]}>Copy Membership Code</Text>
                        </TouchableOpacity>

                        <Text style={styles.sectionTitle}>Current Plans</Text>

                        <FlatList
                            keyExtractor={(item, index) => index?.toString()}
                            data={myHealthCard?.planDetails}
                            horizontal
                            showsHorizontalScrollIndicator={false}
                            renderItem={({ item }) => {
                                return <CurrentPlan item={item} />
                            }}
                        />
                    </View>

                </ScrollView>
                    : !isLoading && <View style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                        marginHorizontal: 15
                    }}>
                        <Text style={{ fontWeight: 'bold', fontFamily: FONT_FAMILY_SF_PRO_REGULAR, fontSize: 20 }}>No Card Available</Text>
                        <TouchableOpacity
                            style={[styles.btnStyle, { width: '100%', marginVertical: 15 }]}
                            onPress={onPressCheckPlans}
                        >
                            <Text style={styles.btnTextStyle}>Check plans</Text>
                        </TouchableOpacity>
                    </View>
            }
        </View>
    )
}

export default index

const styles = StyleSheet.create({
    card: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        elevation: 3,
        shadowColor: 'grey',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
    },
    containerText: {
        padding: 10
    },
    textStyle: {
        fontWeight: 'bold',
        fontSize: 15
    },
    sectionTitle: {
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold,
        fontSize: 20,
        marginVertical: 15,
        marginLeft: 5
    },
    sectionSubTitle: {
        marginVertical: 15,
        fontSize: 15,
        marginLeft: 5,
        color: '#656E83'
    },
    btnStyle: {
        backgroundColor: '#D72820',
        padding: 10,
        borderRadius: 8,
        margin: 10,
    },
    btnTextStyle: {
        textAlign: 'center',
        fontSize: 16,
        color: '#fff',
        textTransform: 'uppercase'
    },
    planContainer: {
        flex: 1,
        justifyContent: 'space-between'
    },
    mainContainer: {
        paddingVertical: 25,
        paddingLeft: 15
    },
    bg: {
        flex: 1,
        backgroundColor: '#F9F9F9'
    },

    kindText: {
        backgroundColor: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        width: '100%',
        textTransform: 'uppercase',
        textAlign: 'center',
        position: 'absolute',
        bottom: 20,
        paddingVertical: 3
    },
    con2: {
        position: 'absolute',
        width: '100%',
        bottom: 65,
    },
    con1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 25
    },
    planName: {
        fontSize: 18,
        fontFamily: FONT_FAMILY_SF_PRO_BOLD,
        color: '#FFF',
        marginTop: 10
    },
    planImage: {
        height: 50,
        width: 50,
        borderRadius: 50,
    },
    ciruclaProg: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
        position: 'absolute',
        right: 10,
        top: 0,
        left: 0,
        bottom: 0,
    },
    benefit: {
        marginLeft: 10,
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10,
        marginTop: -3
    },
    redDot: { fontSize: 8, color: '#FF5E38', paddingTop: 3, paddingRight: 5 }
})