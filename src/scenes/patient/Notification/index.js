import React, { useEffect, useState } from 'react'
import { FlatList, Image, SectionList, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import HeaderSehet from '../../../components/header-common'
import Http from '../../../services/http'
import { isValid } from '../../../utils/ifNotValid'
import Images from '../../../assets/images/index'
import { FONT_FAMILY_REGULAR, FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_SemiBold } from '../../../styles/typography'
import moment from 'moment'
import { convertDateToText } from '../../../utils/helper'
import http from '../../../services/http'

const index = (props) => {
    const [notifs, setNotifs] = useState([])

    useEffect(() => {
        getNotifications()
    }, [])

    const getNotifications = () => {
        http
            .get('pushNotification')
            .then((res) => {
                if (res?.status === 200) {
                    if (isValid(res?.data)) {
                        const sortedData = res?.data?.map(item => {
                            return {
                                ...item,
                                data: item?.data?.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))
                            }
                        })
                        const data = sortedData?.sort((a, b) => new Date(b._id) - new Date(a._id))
                        setNotifs(data)
                    }
                }
            })
            .catch(err => {
                console.log(err);
            })
    }

    const Card = ({ item, navigation }) => {
        const imgUrl = isValid(item?.imageUrl) ? { uri: item?.imageUrl } : Images.appIcon
        const title = isValid(item?.title) ? item?.title : 'Reminder for Appointment'
        const body = isValid(item?.body) ? item?.body : 'Lorem ipsum dolor sit amet, consetetur vfvfkvnfkvnfv'
        const time = moment(item?.createdAt).format('hh:mm A');

        const onPressCard = () => {
            navigation.navigate('offerScreen', {
                data: item
            })
        }

        return (
            <TouchableOpacity
                style={styles.cardStyle}
                onPress={onPressCard}
            >
                <Image
                    source={imgUrl}
                    style={styles.imageStyle}
                />
                <View style={{ flex: 1, marginHorizontal: 10 }}>
                    <Text style={styles.titleStyle} numberOfLines={1}>{title}</Text>
                    <Text style={styles.subtitleStyle} numberOfLines={1}>{body}</Text>
                </View>
                <Text style={styles.timeStyle}>{time}</Text>
            </TouchableOpacity>
        )
    }

    const HeaderSection = ({ section }) => {
        const notification_date = section?._id
        const date = convertDateToText(notification_date)
        return (
            <Text style={styles.headerSectionTitle}>{date}</Text>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
            <HeaderSehet headerText={"Notifications"} navigation={props?.navigation} changeTheme={true} />

            <SectionList
                keyExtractor={(item, index) => index?.toString()}
                sections={notifs}
                style={styles.flatlistStyle}
                renderSectionHeader={({ section }) => {
                    return <HeaderSection section={section} />
                }}
                renderItem={({ item }) => {
                    return <Card item={item} navigation={props?.navigation} />
                }}
                showsVerticalScrollIndicator={false}
            />
        </View>
    )
}

export default index

const styles = StyleSheet.create({
    flatlistStyle: { backgroundColor: '#F9F9F9', paddingVertical: 20, paddingHorizontal: 15 },
    cardStyle: {
        flexDirection: 'row',
        marginVertical: 5,
        backgroundColor: '#FFF',
        padding: 15,
        borderRadius: 8,
        elevation: 3,
    },
    imageStyle: {
        height: 50,
        width: 50,
    },
    titleStyle: {
        color: '#041A32',
        fontFamily: FONT_FAMILY_SF_PRO_BOLD,
        fontSize: 16
    },
    subtitleStyle: {
        color: '#041A32',
        fontFamily: FONT_FAMILY_REGULAR,
        fontSize: 12
    },
    timeStyle: {
        color: '#7C8793',
        fontFamily: FONT_FAMILY_REGULAR,
        fontSize: 10
    },
    headerSectionTitle: {
        color: '#041A32',
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold,
        fontSize: 17,
        marginVertical: 5,
    }
})