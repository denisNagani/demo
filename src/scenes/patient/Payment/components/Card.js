import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { Icon } from 'native-base'

const Card = ({ payName }) => {
    return (
        <View style={styles.card}>
            <Text style={{ fontSize: 18 }}>{payName}</Text>
            <Icon type="Feather" name="chevron-right" style={{ fontSize: 20 }} />
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginHorizontal: 10,
        marginVertical: 3,
        backgroundColor: 'white',
        padding: 10
    }
})

export default Card
