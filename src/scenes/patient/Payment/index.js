// import React, { useEffect, useState } from 'react'
// import { View, Text, SafeAreaView, StyleSheet } from 'react-native'
// import { Icon } from 'native-base'
// import { ScrollView, TextInput } from 'react-native-gesture-handler'
// import Card from './components/Card'
// import PaytmCustomuisdk from 'paytm-customuisdk-react-native'


// const index = (props) => {
//     const data = props.navigation.state.params.data;
//     const [UPI_ID, setUPI_ID] = useState("7777777777@paytm")
//     useEffect(() => {
//     }, [])

//     const initPaytmSdk = () => {
//         PaytmCustomuisdk.initPaytmSDK(
//             data.mid,
//             data.orderId,
//             data.txnToken,
//             data.amount,
//             data.isStaging,
//             ''
//         );
//     }
//     return (
//         <SafeAreaView style={{ flex: 1 }}>
//             <View style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: 'white', height: 60 }}>
//                 <Icon name="arrow-back" type="Ionicons" style={{ fontSize: 26, paddingHorizontal: 10 }} />
//                 <Text style={{ fontSize: 16 }}>Select a payment Mode</Text>
//             </View>

//             <View style={[styles.card, { marginTop: 20 }]}>
//                 <Text style={{ fontSize: 15, fontWeight: 'bold' }}>Total payable amount </Text>
//                 <Text style={{ fontSize: 15, fontWeight: 'bold' }}>75 rs</Text>
//             </View>

//             <ScrollView>
//                 <Text style={{ marginHorizontal: 10, paddingHorizontal: 10, marginTop: 20, marginBottom: 10, fontSize: 16, fontWeight: 'bold' }}>Payment Options</Text>
//                 <Card payName="Credit Card" />
//                 <Card payName="Debit Card" />
//                 <Card payName="Net Banking" />
//                 <Card payName="UPI" />

//                 <View style={{ margin: 10 }}>
//                     <Text style={{
//                         marginVertical: 5,
//                         fontSize: 15
//                     }}>Enter your UPI ID</Text>
//                     <TextInput
//                         onChangeText={(e => setUPI_ID(e))}
//                         style={{
//                             backgroundColor: 'white',
//                             borderRadius: 1,
//                             borderWidth: 1,
//                             borderColor: '#3c3c3c'
//                         }}
//                         value={UPI_ID}
//                     />
//                 </View>
//             </ScrollView>
//         </SafeAreaView>
//     )
// }

// const styles = StyleSheet.create({
//     card: {
//         flexDirection: 'row',
//         alignItems: 'center',
//         justifyContent: 'space-between',
//         marginHorizontal: 10,
//         marginVertical: 3,
//         backgroundColor: 'white',
//         padding: 10
//     }
// })

// export default index
