import moment from 'moment'
import { Icon } from 'native-base'
import React, { useState } from 'react'
import { View, Text, Platform, SafeAreaView, TouchableOpacity, Image, ScrollView } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { getDocProfile } from '../../../actions/sehet/user-action'
import Images from '../../../assets/images'
import HeaderSehet from '../../../components/header-common'
import PowerTranslator from '../../../components/PowerTranslator'
import ViewProfileModal from '../../../components/view-profile-modal'
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_REGULAR, FONT_SIZE_15, FONT_SIZE_18 } from '../../../styles/typography'
import { DOC_PROFILE } from '../../../utils/constant'
import { ifNotValid, ifValid, isValid } from '../../../utils/ifNotValid'
import { strings } from '../../../utils/translation'
import styles from '../book-appointment-flow2/style'
import styles2 from '../../patient/booking-summary/style'
import { selectedSlotObjForQuickCall } from '../book-appointment/action'
import Line from '../../../components/line'
import styles3 from '../upcoming-past-bookings/style'

const index = (props) => {
    const dispatch = useDispatch()
    const [modelProfile, setProfileModel] = useState(false);
    let quickCallDoc = useSelector((state) => state.userReducer.quickDoc);
    let tZone = useSelector((state) => state.appointment.slotTimeZone);
    const userProfile = useSelector(state => state.profile.userProfile)
    const params = props.navigation?.state?.params
    console.log(params);

    const doc = quickCallDoc?.userId;
    const exp = quickCallDoc?.experience;
    const payableAmount = 200
    const todaydate = params?.date
    const date = params?.date
    const time = params?.time
    const isAvailable = params?.available

    const onPressContinue = () => {
        const payload = {
            doctorId: doc?._id,
            appointmentDate: todaydate,
            status: "BOOKED",
            timeZone: tZone?.timeZone,
            appointmentType: "VIDEO",
            paidAmount: payableAmount,
            type: "BOOKING",
            callType: "Quick",
            startTime: time
        };
        dispatch(selectedSlotObjForQuickCall(payload))
        let user = userProfile?.userId
        let isUpdatedProfile = isValid(user?.fullName) && isValid(user?.email) && isValid(user?.gender)
        if (isUpdatedProfile) {
            props.navigation.navigate("SelectMember")
        } else {
            props.navigation.navigate('PatientDetails')
        }
    }

    const HeaderBookAppointment = () => {
        const fullName = isValid(doc?.fullName) ? doc?.fullName : '-'
        const _hindiFullName = isValid(doc?.hindiFullName) ? doc?.hindiFullName : '-'
        const _experience = isValid(exp) ? exp : '-'
        const _education = isValid(doc?.education) ? doc?.education : '-'

        let gender = doc?.gender;
        let _imageUrl = doc?.imageUrl;
        if (ifNotValid(_imageUrl)) _imageUrl = gender == "FEMALE" ? Images.female_doctor : Images.male_doctor
        else _imageUrl = { uri: _imageUrl }

        return <SafeAreaView style={{
            backgroundColor: "#FFFFFF",
            paddingVertical: 10,
            shadowColor: '#0000000B',
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.8,
            shadowRadius: 2,
            elevation: 5,
        }}>
            <Text style={{ marginLeft: 15, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, fontSize: 16, marginBottom: 15 }}>Available Doctor</Text>
            <View style={styles.docDetails}>
                <Image source={_imageUrl} style={styles.headerImg} />
                <View style={styles.docDesc}>
                    {/* <Text numberOfLines={1} style={styles.docName}>{doc.fullName}</Text> */}
                    <PowerTranslator
                        style={styles.docName}
                        enText={fullName}
                        hinText={_hindiFullName}
                        numberOfLines={1}
                    />
                    {/* <Text style={styles.specializationStyle}>{_speciality}</Text> */}
                    <PowerTranslator
                        style={styles.specializationStyle}
                        hinText={_education}
                        enText={_education}
                    />
                    {/* <Text style={styles.specializationStyle}>{_experience}</Text> */}
                    <PowerTranslator
                        style={styles.specializationStyle}
                        hinText={`${_experience} years`}
                        enText={`${_experience} years`}
                    />
                    <Text onPress={() => {
                        dispatch({ type: DOC_PROFILE, payload: {} });
                        dispatch(getDocProfile(doc._id))
                        setProfileModel(!modelProfile)
                    }} style={styles.viewProfileStyle}>{strings.View_Profile}</Text>
                </View>
            </View>
        </SafeAreaView>
    }
    const noBookings = () => {
        return (
            <View style={[styles3.noBookingsBlock, { justifyContent: 'flex-start' }]}>
                <Image source={Images.no_bookings} style={styles3.noBookingsImage} />
                <Text style={styles3.noBookingsTitle}>{strings.no_doc_found}</Text>
                <Text style={[styles3.noBookingsSubTitle, { textAlign: 'center', marginHorizontal: 20 }]}>
                    {strings.try_after_somtime}
                </Text>
            </View>
        );
    };
    return (
        <View style={{ flex: 1 }}>
            <View style={{ backgroundColor: '#041A32' }}>
                <SafeAreaView style={{ flexDirection: 'row', alignItems: 'center' }}>
                    <Icon
                        name="arrowleft"
                        type="AntDesign"
                        style={{ color: "#fff", marginHorizontal: 15, marginVertical: 15 }}
                        onPress={() => {
                            props.navigation.goBack()
                        }}
                    />
                    <Text style={{ color: '#FFF', fontSize: 18 }}>Quick Doc</Text>
                </SafeAreaView>
            </View>
            {
                !isAvailable
                    ? noBookings()
                    : <>
                        <ScrollView style={{ flexGrow: 1 }}>
                            {HeaderBookAppointment()}
                            <View style={{ marginHorizontal: 20 }}>

                                <View style={{ marginTop: 10 }}>
                                    <Text style={{ color: '#656E83' }}>{strings.booking_details}</Text>
                                    <View style={[styles2.row, , { marginVertical: 10 }]}>
                                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                                            <Image source={Images.icon_clock} style={styles2.bookingIcon} />
                                            <Text
                                                style={styles2.timeStyle}>{time}</Text>

                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                                            <Image source={Images.icon_calender}
                                                style={styles2.bookingIcon} />
                                            <Text
                                                style={styles2.timeStyle}>{date}</Text>
                                        </View>
                                    </View>
                                </View>

                                <Line lineColor={"#B6BECB"} />

                                <View style={{ marginVertical: 0 }}>
                                    <Text style={{ marginTop: 10, color: '#656E83' }}>{strings.payment_details}</Text>
                                    <View>
                                        <View style={styles2.rowSpaceBetween}>
                                            <Text style={{ fontSize: FONT_SIZE_15 }}>{strings.consultation_fee}</Text>
                                            <Text>₹ {payableAmount}</Text>
                                        </View>
                                        <View style={styles2.rowSpaceBetween}>
                                            <Text style={{ fontSize: FONT_SIZE_15 }}>{strings.taxes}</Text>
                                            <Text>₹ 0</Text>
                                        </View>
                                        <View style={styles2.rowSpaceBetween}>
                                            <Text style={{ fontSize: FONT_SIZE_15 }}>{strings.convinience_fee}</Text>
                                            <Text>₹ 0</Text>
                                        </View>
                                        <View style={styles2.rowSpaceBetween}>
                                            <Text style={{ fontSize: FONT_SIZE_15 }}>{strings.discount}</Text>
                                            <Text>₹ {payableAmount}</Text>
                                        </View>
                                        <View style={styles2.rowSpaceBetween}>
                                            <Text style={{ fontSize: FONT_SIZE_15, fontWeight: 'bold' }}>{strings.total_pay}</Text>
                                            <Text style={{ fontWeight: 'bold', color: '#038BEF', fontSize: FONT_SIZE_18 }}>{payableAmount == "Free" ? "" : "₹"}{payableAmount}</Text>
                                        </View>
                                    </View>
                                </View>

                            </View>
                        </ScrollView>

                        <SafeAreaView style={{ paddingVertical: 10 }}>
                            <TouchableOpacity
                                style={{
                                    backgroundColor: '#FF5E38',
                                    padding: 10,
                                    marginHorizontal: 10,
                                    borderRadius: 8
                                }}
                                onPress={onPressContinue}
                            >
                                <Text style={{
                                    fontSize: 18,
                                    color: 'white',
                                    textAlign: 'center'
                                }}>{strings.continue}</Text>
                            </TouchableOpacity>
                        </SafeAreaView>
                    </>
            }

            <ViewProfileModal
                onTouchOutside={() => {
                    // dispatch({ type: DOC_PROFILE, payload: {} });
                    setProfileModel(!modelProfile)
                }}
                onHardwareBackPress={() => {
                    dispatch({ type: DOC_PROFILE, payload: {} });
                    setProfileModel(!modelProfile)
                }}
                specialization={null}
                name={undefined}
                experience={undefined}
                imageUrl={""}
                visible={modelProfile} />
        </View>
    )
}

export default index