import React, { useEffect } from "react";
import { Image, View } from "react-native";
import { Button, Container, Text } from "native-base";
import { useDispatch, useSelector } from "react-redux";
import moment from 'moment'

import styles from "./style";
import Images from "../../../assets/images";
import ImageContainer from "../../../components/ImageContainer";
import { successLog } from "../../../utils/fireLog";
import { ifNotValid, ifValid } from "../../../utils/ifNotValid";
import common from "../../../styles/common";
import { setUpcomingPastActive } from "../upcoming-past-bookings/components/action";
import { ApplyCoupon, ApplyMembershipCoupon } from "../../../actions/coupn";
import { FONT_FAMILY_SF_PRO_MEDIUM } from "../../../styles/typography";
import { TranslationConfig, strings } from "../../../utils/translation";
import PowerTranslator from "../../../components/PowerTranslator";
import { getSpecialitiesNames } from "../../../utils/helper";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { socket } from "../../../config";
import { IO_EVENTS } from "../../../utils/constant";

const AcknowledgmentScreen = (props) => {
    const dispatch = useDispatch();
    const docProfile = useSelector((state) => state.userReducer.docProfile);
    const appliedCoupon = useSelector((state) => state.appointment.appliedCoupon);
    const appointment = useSelector((state) => state.appointment.postDocRes);
    const dateNtime = useSelector((state) => state.appointment.summaryDateNTime);

    const lang = useSelector(state => state.profile.selected_language)
    TranslationConfig(lang)

    let name = ifValid(docProfile) && ifValid(docProfile.userId) ? docProfile.userId.fullName : "";
    if (ifNotValid(name)) name = "..."

    let srr = docProfile.userId
    let hindiname = "";
    if (srr != undefined || srr != null) {
        hindiname = srr.hindiFullName
    } else {
        hindiname = name
    }

    let speciality = ifValid(docProfile) && ifValid(docProfile.specialityId) ? getSpecialitiesNames(docProfile?.specialityId, 'name') : "";
    if (ifNotValid(speciality)) speciality = "..."

    let hindiSpeciality = ifValid(docProfile) && ifValid(docProfile.specialityId) ? getSpecialitiesNames(docProfile?.specialityId, 'hindiName') : "";

    let date = ifValid(dateNtime) && ifValid(dateNtime.appointmentDate) ? dateNtime.appointmentDate : "";
    if (ifNotValid(date)) date = "..."

    let time = ifValid(dateNtime) && ifValid(dateNtime.appointmentTime) ? dateNtime.appointmentTime : "";
    if (ifNotValid(time)) time = "..."

    let imageUrl = ifValid(docProfile) && ifValid(docProfile.userId) ? docProfile.userId.imageUrl : "";
    if (ifNotValid(imageUrl) || imageUrl === "") imageUrl = Images.user
    else imageUrl = { uri: imageUrl }

    useEffect(() => {
        // to trigger event after booking
        sendSoctIoEvent()

        //if coupon applied
        if (appliedCoupon != null) {
            // const body = {
            //     appointmentId: appointment._id,
            //     couponId: appliedCoupon.couponId,
            //     deviceId: appliedCoupon.deviceId,
            //     isApplied: appliedCoupon.isApplied,
            //     userId: appliedCoupon.userId
            // }
            // dispatch(ApplyCoupon(body))

            // const body = {
            //     couponId: appliedCoupon?.couponName,
            //     isApplied: true,
            //     doctorId: appliedCoupon?.doctorId
            // }

            const body = {
                subscriptionId: appliedCoupon?.couponName,
                appointmentId: appointment?._id,
                planId: appliedCoupon?.planId
            }
            dispatch(ApplyMembershipCoupon(body))
        } else {
            console.log("No coupon code is applied");
        }
    }, [])

    const sendSoctIoEvent = () => {
        socket().emit(IO_EVENTS.BOOK_APPOINTMENT, appointment)
    }

    return (
        <ImageContainer>
            <View style={styles.container}>
                <View style={[styles.Image, { marginTop: hp("10%") }]}>
                    <Image
                        source={imageUrl}
                        style={styles.docImg}
                    />
                    <View
                        style={styles.cameraBtn}>
                        <Image source={Images.checkCircle} style={{ width: 30, height: 30 }} />
                    </View>
                </View>
                <View style={styles.content}>
                    {/* <Text style={styles.contentText}>
                        Your booking with <Text style={[styles.contentText, { color: '#038BEF' }]}>Dr. {name}</Text> {"\n"}({speciality}) has been confirmed.
                    </Text> */}
                    <Text style={styles.contentText}>
                        {strings.ackno_line}
                        <PowerTranslator
                            style={styles.contentText}
                            hinText={hindiname}
                            enText={name}
                        />
                        {" " + strings.ackno_line2}
                    </Text>
                    <View style={{ backgroundColor: '#FFF', paddingHorizontal: 5, marginVertical: 15, paddingVertical: 10, borderRadius: 10 }}>
                        <Text style={{ textAlign: 'center', color: '#041A32', fontSize: 22, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }}>{strings.slot_details}</Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
                            <Text style={{ color: '#484F5F', fontSize: 17 }}>
                                <Image
                                    source={Images.calendar}
                                    style={{ height: 20, width: 20 }}
                                />
                                <Text>  {time === '' ? "" : moment(time, 'HH:mm:ss').format("hh:mm A")}</Text>
                                <Text> | </Text>
                                <Text> {date === '' ? "" : moment(date).format("DD MMM YYYY")}  </Text>
                            </Text>

                        </View>
                    </View>
                    <Text note style={styles.subContentText}>{strings.subContextText}</Text>
                </View>

                <Button danger style={[common.footerBtn]}
                    onPress={() => {
                        props.navigation.navigate("UpcomingPastBookings")
                        dispatch(setUpcomingPastActive("UPCOMING"))
                    }}>
                    <Text uppercase={false} style={[styles.number91, { color: 'white' }]}>{strings.backToBookings}</Text>
                </Button>

                <Button danger style={[common.footerBtn]}
                    onPress={() => {
                        props.navigation?.navigate("PaymentReceiptModal", {
                            url: appointment?.receiptUrl,
                        })
                    }}>
                    <Text uppercase={false} style={[styles.number91, { color: 'white' }]}>{strings.view_receipt}</Text>
                </Button>
            </View>
        </ImageContainer>
    );
};

export default AcknowledgmentScreen;
