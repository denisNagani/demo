import React, { useState } from 'react'
import { TextInput, Text, ScrollView, Image, KeyboardAvoidingView } from 'react-native'
import HeaderSehet from '../../../components/header-common'
import { Button, View, Form, Item, Label, Input, Content, Container } from 'native-base'
import { useDispatch, useSelector } from 'react-redux'
import { CheckCoupon, CoupnApplied, UpdatePrice } from '../../../actions/coupn'
import common from '../../../styles/common'
import { showModal } from '../../../actions/modal'
import { errorPayload } from '../../../utils/helper'
import DeviceInfo from "react-native-device-info";
import Images from '../../../assets/images'


const index = (props) => {
    const dispatch = useDispatch()
    const user = useSelector(state => state.userReducer.userProfile.userId)
    const selected_slot = useSelector(state => state.appointment.selectedSlotObj)
    const get_price = useSelector(state => state.specialitiesReducer.selectedSpeciality)
    const [couponCode, setCouponCode] = useState('')

    const handleCoupon = () => {
        if (couponCode == null || couponCode == '' || couponCode == undefined) {
            dispatch(showModal(errorPayload('Enter coupon Code')))
        } else {
            const slot_id = selected_slot.slotId
            const user_id = user._id
            const og_price = get_price.m_price
            const deviceId = DeviceInfo.getUniqueId()
            dispatch(CheckCoupon(couponCode, user_id, slot_id, og_price, deviceId))
        }
    }

    return (
        <Container>
            <HeaderSehet headerText={"Apply Coupon"} navigation={props.navigation} />
            <Content>
                <View style={{ flex: 1, flexDirection: 'column' }}>

                    <Form>
                        <Image
                            resizeMode="stretch"
                            style={{ height: 200, width: '100%' }}
                            source={Images.coupen_code}
                            defaultSource={Images.coupen_code}
                        />
                        <Item
                            floatingLabel>
                            <Label style={{ margin: 10, }}>Enter Coupon Code *</Label>
                            <Input
                                value={couponCode}
                                style={{
                                    margin: 10,
                                    borderRadius: 15,
                                }}
                                onChangeText={text => setCouponCode(text)}
                            />
                        </Item>
                        {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: '10%' }}> */}
                        <Button danger style={[common.footerBtn, {
                            marginHorizontal: 10,
                            alignSelf: 'center'
                        }]}
                            onPress={handleCoupon}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Text uppercase={false}
                                    style={[{ fontSize: 16, fontWeight: 'bold', textAlign: 'center', color: 'white', flex: 1, paddingHorizontal: 15 }]}>Apply Code </Text>

                            </View>
                        </Button>
                        {/* </View> */}

                        {/* <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', height: '10%' }}> */}
                        <Button style={[common.footerBtn, {
                            marginHorizontal: 5,
                            alignSelf: 'center',
                            backgroundColor: 'grey'
                        }]}
                            onPress={() => {
                                dispatch(CoupnApplied(null))
                                dispatch(UpdatePrice(get_price.m_price))
                                props.navigation.navigate('BookingSummaryScreen', {
                                    dis_value: 0
                                })
                            }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <Text uppercase={false}
                                    style={[{ fontSize: 16, fontWeight: 'bold', textAlign: 'center', color: 'white', flex: 1, paddingHorizontal: 15 }]}>Skip</Text>

                            </View>
                        </Button>
                        {/* </View> */}
                    </Form>
                </View>
            </Content>
        </Container>
    )
}

export default index
