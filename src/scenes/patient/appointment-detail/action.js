import {ADD_MEDICINE_ID, SELECTED_APPOINTMENT_WAITING} from "../../../utils/constant";


export const setUpcomingPastDetailView = (payload) => {
    return {
        type: 'UPCOMING_PAST_DETAIL_ACTIVE',
        payload: payload,
    };
};

export const setAppointmentIdForAddMedicine = (payload) => {
    return {
        type: ADD_MEDICINE_ID,
        payload: payload,
    };
};

export const setAppointmentObject = (payload) => {
    return {
        type: SELECTED_APPOINTMENT_WAITING,
        payload: payload,
    };
};
