import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { FONT_FAMILY_SF_PRO_REGULAR, FONT_SIZE_11, FONT_SIZE_13 } from "../../../../styles/typography";

const Rating = ({ img, text = "", experiance }) => {
    const dispatch = useDispatch();
    let activeButton = useSelector((state) => state.upcomingPast.active);

    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', marginRight: 10, }}>
            <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                <Image source={img} style={{ width: 7, height: 11, marginRight: 3, alignItems: 'center', justifyContent: 'center' }} />
                <Text style={{ color: '#FFFFFFCC', textAlign: 'center', fontSize: FONT_SIZE_13 }}>{experiance} years</Text>
            </View>
            <Text style={{
                color: '#FFFFFFCC',
                fontSize: FONT_SIZE_11,
                fontFamily: FONT_FAMILY_SF_PRO_REGULAR
            }}>{text}</Text>
        </View>
    );
}
const styles = StyleSheet.create({})
export default Rating;
