import React, { useEffect, useState } from "react";
import { Alert, Image, Platform, SafeAreaView, StyleSheet, TouchableOpacity, View } from "react-native";
import { Button, Container, Content, Footer, Icon, Text } from "native-base";
import Images from "../../../assets/images";
import styles from "./style";
import { useDispatch, useSelector } from "react-redux";
import { ifNotValid, ifValid, isValid } from "../../../utils/ifNotValid";
import HeaderSehet from "../../../components/header-common";
import Rating from "./components/Rating";
import RNFetchBlob from 'rn-fetch-blob'
const { config, fs, base64 } = RNFetchBlob;
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_SIZE_12,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_19
} from "../../../styles/typography";
import moment from 'moment'
import { requestReadWritePermissionAndDownload } from "../upcoming-past-bookings/components/checkPermission";
import { strings } from "../../../utils/translation";
import { getSpecialitiesNames } from "../../../utils/helper";
import { PERMISSIONS, RESULTS, checkMultiple, openSettings, requestMultiple } from "react-native-permissions";
import { v4 as uuidv4 } from 'uuid';
import { hide, show } from "../../../utils/loader/action";
import { showModal } from "../../../actions/modal";

const AppointmentDetails = (props) => {
    const dispatch = useDispatch();
    const navigation = props.navigation;
    const appointedItem = props.navigation.state.params.data;
    const doctorSlot = useSelector((state) => state.appointment.docSlot);
    let upcomingPast = useSelector((state) => state.upcomingPast.detailActive);

    console.log('appointedItem?.intakeInfo ', appointedItem?.intakeInfo);

    let intakeForm = {
        name: isValid(appointedItem?.intakeInfo?.name) ? appointedItem.intakeInfo.name : appointedItem?.patientId?.fullName,
        age: isValid(appointedItem?.intakeInfo?.dob) ? moment().diff(appointedItem.intakeInfo.dob, 'years') : moment().diff(appointedItem?.patientId?.dob, 'years'),
        phone: isValid(appointedItem?.intakeInfo?.mobileNo) ? appointedItem.intakeInfo.mobileNo : appointedItem?.patientId?.mobileNo,
        email: isValid(appointedItem?.intakeInfo?.email) ? appointedItem.intakeInfo.email : appointedItem?.patientId?.email,
    }

    if (ifNotValid(upcomingPast)) {
        upcomingPast = "UPCOMING"
        console.log("details view type not provided..!")
    }

    useEffect(() => {
        console.log(appointedItem);
    }, []);


    const keyExtractor = (item, index) => index.toString();

    const HeaderBookAppointment = (doc) => {
        console.log("doc object ", doc);

        if (ifNotValid(doc)) return <View />

        let fullName = doc?.doctorId?.fullName
        if (ifNotValid(fullName)) fullName = "..."

        let specialization = ifValid(doc?.doctorId?.specialityId) ? getSpecialitiesNames(doc.doctorId.specialityId, "name") : doc.specialization
        if (ifNotValid(specialization)) specialization = "-"

        let experience = doc?.doctorId?.experience
        if (ifNotValid(experience)) experience = "0"


        let _imageUrl = doc?.doctorId?.imageUrl;
        if (ifNotValid(_imageUrl)) _imageUrl = Images.user
        else _imageUrl = { uri: _imageUrl }

        let txtStyleExtra = { color: '#FFF' }

        return <SafeAreaView style={[styles.header, { backgroundColor: '#141E46', height: '28%' }]}>
            <HeaderSehet headerText={upcomingPast === 'PAST' ? `${strings.past_appointment}` : `${strings.upcoming_appointment}`}
                navigation={props.navigation} hideshadow changeTheme={true} />
            <View style={[styles.docDetails, { marginLeft: '10%' }]}>
                <Image source={_imageUrl} style={styles.headerImg} />
                <View style={styles.docDesc}>
                    <Text numberOfLines={2} style={[styles.docName, txtStyleExtra]}>{fullName}</Text>
                    <Text numberOfLines={1} style={[styles.specializationStyle, txtStyleExtra]}>{specialization}</Text>

                    <View style={{ flexDirection: 'row', marginTop: 15 }}>
                        {/*<Rating img={Images.starWhite} text="Rating"/>*/}
                        {/*<Rating img={Images.usersWhite} text="People"/>*/}
                        {/* <Rating img={Images.rewardWhite} text="Experience" experiance={experience} /> */}
                    </View>

                </View>
            </View>
        </SafeAreaView>
    }

    // let pdfUploaded = ifValid(appointedItem.prescriptionId) ? appointedItem.prescriptionId.pdfUploaded : undefined;
    const timeHr = moment(appointedItem.utcDate).format("HH:MM").substr(0, 2)

    let pdfUploaded = isValid(appointedItem.prescriptionId)
        ? appointedItem.prescriptionId?.imageURL?.length > 0 || isValid(appointedItem.prescriptionId?.pdfUploaded)
        : false
    let pdfUploadedData = appointedItem?.prescriptionId?.pdfUploaded;
    let imageURLData = appointedItem?.prescriptionId?.imageURL;


    const onPressDownloadImageprescriptions = async (images) => {
        console.log(images);

        if (isValid(images)) {
            dispatch(show())
            if (Platform.Version > 32) {
                downloadImages(images)
            } else {
                const typess = [PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE, PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]
                const permiss = await checkMultiple(typess)

                if (permiss["android.permission.READ_EXTERNAL_STORAGE"] === RESULTS.GRANTED && permiss["android.permission.WRITE_EXTERNAL_STORAGE"] === RESULTS.GRANTED) {
                    downloadImages(images)
                } else if (permiss["android.permission.READ_EXTERNAL_STORAGE"] === RESULTS.BLOCKED && permiss["android.permission.WRITE_EXTERNAL_STORAGE"] === RESULTS.BLOCKED) {
                    dispatch(hide())
                    showSettingsAlert()
                } else {
                    requestMultiple(typess)
                        .then(() => downloadImages(images))
                        .catch(err => {
                            console.log(err);
                            dispatch(hide())
                        })
                }
            }
        }
    }

    const showSettingsAlert = () => {
        Alert.alert(
            "Permission required!",
            "Please click open settings and allow storage permission under Permissions",
            [
                {
                    text: "Open Settings",
                    onPress: () => openSettings(),
                    style: "cancel",
                },
            ],
            {
                cancelable: true,
            }
        );
    }

    const downloadImages = (images) => {
        if (images?.length > 0) {
            const prms = [];

            images?.map(downUrl => {
                const promise = new Promise((resolve, reject) => {
                    const dirs = RNFetchBlob.fs.dirs.DownloadDir;
                    const localPath = dirs + `/${uuidv4()}.jpg`
                    RNFetchBlob
                        .config({
                            path: localPath,
                            addAndroidDownloads: {
                                useDownloadManager: true,
                                path: localPath,
                                notification: true,
                                mediaScannable: true,
                            }
                        })
                        .fetch('GET', downUrl)
                        .progress((received, total) => {
                            let percentage = (received / total) * 100
                            let roundPercentage = Math.round(percentage)
                            console.log('progress', roundPercentage)
                        })
                        .then((res) => {
                            resolve(res?.path())
                        })
                        .catch((err) => {
                            console.log('err  ', err)
                            reject(err)
                        })
                })
                prms.push(promise)
            })

            Promise.all(prms)
                .then(results => {
                    dispatch(hide())
                    const payload = {
                        text: `Prescription downloaded on your device`,
                        subtext: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    }
                    dispatch(showModal(payload))
                    console.log('results ', results);
                })
                .catch(err => {
                    console.log(err);
                    dispatch(hide())
                })
        }
    }

    return (
        <Container>
            {HeaderBookAppointment(appointedItem)}
            <Content style={{ backgroundColor: '#F9F9F9' }}>
                <View style={{ margin: 20 }}>
                    <View>
                        <Text style={stylesInner.cardHeaderText}>
                            <Image source={Images.watchRed} style={{ fontSize: FONT_SIZE_15 }} /> {strings.appointment_time}</Text>
                        <View style={{ marginLeft: 22 }}>
                            <Text style={stylesInner.subText}>
                                {
                                    timeHr < 12
                                        ? "Morning"
                                        : timeHr < 18
                                            ? "Afternoon"
                                            : "Evening"
                                }
                            </Text>
                            <Text
                                style={stylesInner.subText}>{moment(appointedItem.utcDate).format("dddd D MMM YYYY")}</Text>
                            <Text
                                style={stylesInner.time}>{appointedItem.startTime} {moment(appointedItem.utcDate).format("A")} - {appointedItem.endTime} {moment(appointedItem.utcDate).format("A")} </Text>
                        </View>
                    </View>

                    <View style={{ marginTop: 15 }}>
                        <Text style={stylesInner.cardHeaderText}>
                            <Image source={Images.userRed} style={{ fontSize: FONT_SIZE_15 }} /> {strings.patient_info}</Text>
                        <View style={{ marginLeft: 22 }}>
                            <Text numberOfLines={1} style={stylesInner.subText}>Name
                                : {intakeForm.name}</Text>
                            <Text style={stylesInner.subText}>Age
                                : {intakeForm.age}</Text>
                            <Text style={stylesInner.subText}>Phone : {intakeForm.phone}</Text>
                            <Text style={stylesInner.subText}>E-mail : {intakeForm.email}</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 15 }}>
                        <Text style={stylesInner.cardHeaderText}>
                            <Image source={Images.dollarRed} style={{ fontSize: FONT_SIZE_15 }} /> {strings.fees_info}</Text>
                        <View style={{ marginLeft: 22 }}>
                            {/* {
                                appointedItem.paymentStatus === "UNPAID"
                                    ? <Text style={stylesInner.time}>UnPaid</Text>
                                    : <>
                                        <Text style={stylesInner.time}>Paid</Text>
                                        <Text style={stylesInner.subText}>$10</Text>
                                    </>
                            } */}
                            <Text style={{ marginVertical: 5, fontSize: 14, color: '#038BEF' }}>Paid</Text>
                            {ifValid(appointedItem.paidAmount) && <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Icon type="FontAwesome" name="rupee" style={{ fontSize: 14, marginRight: 3 }} />
                                <Text style={{ marginVertical: 10, fontSize: 14 }}>{appointedItem.paidAmount}</Text>
                            </View>}

                            {
                                isValid(appointedItem?.receiptUrl)
                                &&
                                <TouchableOpacity onPress={() => {
                                    console.log('urlllll ', appointedItem?.receiptUrl);
                                    props.navigation?.navigate("PaymentReceiptModal", {
                                        url: appointedItem?.receiptUrl,
                                    })
                                }} style={stylesInner.pdfContainer}>
                                    <Image source={Images.pin} style={{ width: 16, height: 16, }} />
                                    <View style={{ marginLeft: 10 }}>
                                        <Text style={stylesInner.bloodStyle}>Payment receipt</Text>
                                        <Text
                                            style={stylesInner.bloodDateStyle}>{moment(appointedItem?.utcDate).format("DD MMM YYYY")}</Text>
                                    </View>
                                </TouchableOpacity>
                            }
                        </View>
                    </View>

                    {upcomingPast === 'PAST' && pdfUploaded == true ?
                        <TouchableOpacity onPress={() => {
                            if (imageURLData?.length > 0) {
                                onPressDownloadImageprescriptions(imageURLData)
                            }
                            if (isValid(pdfUploadedData)) {
                                dispatch(requestReadWritePermissionAndDownload(pdfUploadedData))
                            }
                        }}
                            style={{ flexDirection: 'row', alignItems: 'center', }}>
                            <Text style={stylesInner.downloadText}>Download Prescription</Text>
                            <Icon name={'download'} style={{ fontSize: FONT_SIZE_19, marginLeft: 10, color: '#FF5E38' }}
                                type={"Feather"} />
                        </TouchableOpacity> : null}

                </View>
            </Content>

        </Container>
    );
};
export default AppointmentDetails;
const stylesInner = StyleSheet.create({
    pdfContainer: {
        padding: 10, alignItems: 'center',
        backgroundColor: 'rgb(229, 249, 247)', flexDirection: 'row'
    },
    bloodStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
        color: '#041A32'
    },
    bloodDateStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_12,
        color: '#A0A9BE'
    },
    footer: {
        height: 70,
        backgroundColor: "#FFFFFF",
        justifyContent: "center",
        alignItems: "center",
    },
    footerBtn: {
        justifyContent: "center",
        backgroundColor: '#FF5E38',
        alignItems: "center",
        flex: 1, margin: 10,
        borderRadius: 9,
    },
    cardHeaderText: {
        color: '#656E83',
        fontSize: FONT_SIZE_14,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR
    },
    subText: {
        color: "#041A32",
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        fontSize: FONT_SIZE_15,
        paddingVertical: 6,
    }, time: {
        color: "#038BEF",
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        paddingVertical: 4,
        fontSize: FONT_SIZE_15,
    }, downloadText: {
        color: "#038BEF",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        paddingVertical: 4,
        fontSize: FONT_SIZE_15,
    }
})
