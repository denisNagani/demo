import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_SIZE_12,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_16,
    FONT_SIZE_19
} from "../../../styles/typography";

export default (styles = StyleSheet.create({
    f_white: {
        color: "#FFFFFF",
    },
    container: {
        backgroundColor: "#ffffff",
    },
    header: {
        height: hp("25%"),
        backgroundColor: "#FFFFFF",
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    heading: {
        flexDirection: "row",
        marginTop: hp("5%"),
        alignItems: "center",
    },
    headerText: {
        color: "#fff",
        fontSize: hp("2.9%"),
        alignSelf: "center",
        marginLeft: wp("5%"),
    },
    headerBackIcon: {
        fontSize: hp("3.5%"),
        color: "#fff",
    },
    headerImg: {
        width: 90,
        height: 90,
        borderRadius: 90 / 2,
    },

    specializationStyle: {
        fontSize: FONT_SIZE_15, color: '#041A32', fontFamily: FONT_FAMILY_SF_PRO_REGULAR
    },
    viewProfileStyle: {
        fontSize: FONT_SIZE_14, color: '#FFFFFF', fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, marginTop: 8
    },


    //    book appointment
    docDetails: {
        flexDirection: "row",
        marginLeft: wp("15%"),
    },
    docDesc: {
        flexDirection: "column",
        justifyContent: "space-evenly",
        marginLeft: wp("5%"),
        marginRight: wp("25%"),
    },
    docName: {
        color: "#041A32",
        fontSize: FONT_SIZE_19,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    datePicker: {
        marginTop: 20,
    },
    text: {
        marginLeft: 20,
        fontSize: FONT_SIZE_16,
        color: "#041A32",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    timezone: {
        marginLeft: 20,
        fontSize: FONT_SIZE_12,
        color: "gray",
    },
    calendar: {
        marginTop: 10,
    },
    calendarIconStyle: {
        width: wp("15%"),
        height: wp("15%"),
    },
    timeZone: {
        marginTop: 10,
    },
    timePicker: {
        marginTop: 20,
    },
    timeBtns: {
        marginTop: 10,
    },
    item: {
        flex: 1,
        borderRadius: 6,
        height: 30,
        margin: 10,
        backgroundColor: "#A0A9BE1A",
        justifyContent: "center",
        alignItems: "center",
    },
    itemContainer: {
        width: '24%'
    },
    list: {
        flex: 1,
    },
    footer: {
        height: 70,
        backgroundColor: "#FFFFFF",
        justifyContent: "center",
        alignItems: "center",
    },
    footerBtn: {
        justifyContent: "center",
        backgroundColor: '#FF5E38',
        alignItems: "center",
        flex: 1, margin: 10,
        borderRadius: 9,
    },
    tAndCText: {
        color: '#A0A9BE', fontSize: FONT_SIZE_12
    },
    tAndCLink: {
        color: "#080A72",
        fontSize: FONT_SIZE_12,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
    },
    modal: {
        justifyContent: "center",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("2%"),
    },
    modalContainer: {
        flex: 1,
    },
    modalTextHeader: {
        fontSize: 24,
        marginTop: 27,
        marginBottom: 6,
        color: "#041A32",
    },

}));
