import React, { useEffect, useState } from "react";
import { FlatList, Image, SafeAreaView, TouchableOpacity, View } from "react-native";
import { Button, Container, Content, Footer, Icon, Text } from "native-base";
import { Calendar } from "react-native-calendars";

import Images from "../../../assets/images";
import styles from "./style";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import {
    emptySlot,
    getDoctorsSlot,
    getSlotTimeZone,
    postDoctorSlot,
    setSummaryDateNTime,
} from "../../../actions/appointment";
import { setDoctorImage, setDoctorName } from "../../../actions/bookings";
import { showModal } from "../../../actions/modal";
import { methodLog, recordError, successLog } from "../../../utils/fireLog";
import ViewProfileModal from "../../../components/view-profile-modal";
import { ifNotValid, ifValid, isValid } from "../../../utils/ifNotValid";
import { getDocProfile } from "../../../actions/sehet/user-action";
import { DOC_PROFILE } from "../../../utils/constant";
import HeaderSehet from "../../../components/header-common";
import { errorPayload, getSpecialitiesNames } from "../../../utils/helper";
import { setIsBookingFlow1or2 } from "../book-appointment/action";
import { strings, TranslationConfig } from "../../../utils/translation";
import PowerTranslator from "../../../components/PowerTranslator";
import { BookFlow2Action, SetSpecialityDetails, setFromDynamicLink } from "../../../actions/profile";
import { setFromCheckout, getSkipped } from "../../../services/auth";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_SF_PRO_SemiBold } from "../../../styles/typography";
import { rowStyle } from "../../../styles/commonStyle";

const BookAppointmentScreenFlow2 = (props) => {
    const dispatch = useDispatch();
    const navigation = props.navigation;
    const docProps = props.navigation.state.params?.doc;
    const selectedDoctor = useSelector((state) => state.appointment.selectedDoctor);
    const doc = ifValid(docProps) ? docProps : selectedDoctor;
    const [markedDates, setMarkedDates] = useState({});
    const [selectedDate, setSelectedDate] = useState(null);
    const [data, setData] = useState([]);
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);
    const [selectedObject, setSelectedObject] = useState(null);
    const doctorSlot = useSelector((state) => state.appointment.docSlot);
    const slotTimeZone = useSelector((state) => state.appointment.slotTimeZone);
    const get_price = useSelector(state => state.specialitiesReducer.selectedSpeciality)
    let selectSlotLimit = 1;
    const [modelProfile, setProfileModel] = useState(false);
    let docProfile = useSelector((state) => state.userReducer.docProfile);
    let checkFromDynamicLink = useSelector((state) => state.profile.fromDynamicLink);
    let isConsulationModeOffline = docProfile?.allowOffline;
    let fee = docProfile?.fee;
    let offlineFee = docProfile?.offlineFee;
    const [selectedCosultMode, setSelectedconsultMode] = useState("ONLINE");

    useEffect(() => {
        // const listener = navigation.addListener("didFocus", () => {
        dispatch(getDocProfile(doc._id))
        getSelectedDayEvents(new Date());
        dispatch(getSlotTimeZone())
        successLog("BookAppointmentScreenFlow2 screen loaded")
        // });
    }, []);

    useEffect(() => {
        successLog("BookAppointmentScreenFlow2 screen loaded")
        if (ifValid(doctorSlot)) {
            addSelectedProperty(doctorSlot);
        }
    }, [doctorSlot]);

    const addSelectedProperty = (dataOld) => {
        methodLog("addSelectedProperty")
        try {
            let data = dataOld.map((obj) => {
                return { ...obj, selected: false };
            });
            setData(data);
        } catch (e) {
            recordError(e)
        }
    };

    const updateTimeData = (_id) => {
        methodLog("updateTimeData")
        try {
            let result = data.map((o) => {
                if (o._id === _id) {
                    setSelectedObject(o);
                    return { ...o, selected: !o.selected };
                } else {
                    if (selectSlotLimit === 1) return { ...o, selected: false };
                    else return o;
                }
            });
            setData(result);
        } catch (e) {
            recordError(e)
        }
    };

    const getSelectedDayEvents = (date) => {
        methodLog("getSelectedDayEvents")
        try {
            let obj = {
                [moment(date).format("YYYY-MM-DD")]: {
                    selected: true,
                    endingDay: true,
                    selectedColor: "#1B80F3",
                    color: "green",
                    textColor: "gray",
                },
            };
            let serviceDate = moment(date).format("YYYY-MM-DD");
            let time = moment().format("HH:mm:ss");
            let dateTime = moment(serviceDate + " " + time).format();
            setMarkedDates(obj);
            setSelectedDate(dateTime);
        } catch (e) {
            recordError(e)
        }
    };

    useEffect(() => {
        if (selectedDate !== null) dispatch(getDoctorsSlot(doc._id, selectedDate, selectedCosultMode));
    }, [selectedDate, selectedCosultMode]);

    const postSlot = async () => {
        methodLog("postSlot")
        try {
            if (selectedObject !== null) {
                let consultFee = {
                    ...get_price
                }
                if (get_price?.bookNow != false) {
                    consultFee = selectedCosultMode == 'ONLINE'
                        ? {
                            ...get_price,
                            m_price: fee
                        }
                        : {
                            ...get_price,
                            m_price: offlineFee
                        }
                    dispatch(SetSpecialityDetails({
                        ...doc,
                        fee: selectedCosultMode == 'ONLINE' ? fee : offlineFee
                    }))
                }
                let datee = moment(selectedDate).format("YYYY-MM-DD");

                if (selectedDate == null) {
                    dispatch(showModal(errorPayload('Please select date')))
                    return;
                }

                if (checkFromDynamicLink?.value) {
                    const skipped = await getSkipped()
                    const isSkipped = JSON.parse(skipped)
                    if (isSkipped == true || isSkipped == null) {
                        const payload = {
                            text: `${strings.please_login_to_continue}`,
                            subtext: "",
                            iconName: "closecircleo",
                            modalVisible: true,
                        }
                        setFromCheckout(true)
                        const linkData = {
                            doc: doc,
                            datee: datee,
                            selectedObject: selectedObject,
                            slotTimeZone: slotTimeZone,
                            props: props,
                            get_price: consultFee
                        }
                        dispatch(setFromDynamicLink(true, linkData))
                        props.navigation.navigate('PatientLogin')
                        dispatch(showModal(payload))
                    } else {
                        dispatch(BookFlow2Action(doc, datee, selectedObject, slotTimeZone, props, consultFee, selectedCosultMode))

                        setSelectedDate(null);
                        setMarkedDates({});
                    }
                } else {
                    dispatch(BookFlow2Action(doc, datee, selectedObject, slotTimeZone, props, consultFee, selectedCosultMode))

                    setSelectedDate(null);
                    setMarkedDates({});
                }

            } else {
                let payload = {
                    text: "Please select date and time!!",
                    subtext: "",
                    iconName: "closecircleo",
                    modalVisible: true,
                };

                if (data === undefined || !data.length > 0) {
                    payload = { ...payload, text: 'No slots available' };
                }
                dispatch(showModal(payload))
            }
        } catch (e) {
            recordError(e)
        }
    };

    const _renderItem = (item, i) => {
        return item.status === "INACTIVE" || item.isTimeout === true ? (
            <TouchableOpacity style={styles.itemContainer} disabled onPress={() => updateTimeData(item._id)}>
                <View style={styles.item} key={i}>
                    <Text
                        style={{
                            color: "#4e5c76",
                            opacity: 0.5,
                            fontSize: 15
                        }}
                    >
                        {moment(item.startTime, "HHmmss").format("hh:mm A")}
                    </Text>
                </View>
            </TouchableOpacity>
        ) : (
            <TouchableOpacity style={styles.itemContainer} onPress={() => updateTimeData(item._id)}>
                <View style={[
                    styles.item,
                    { backgroundColor: item.selected ? "#D72820" : "#A0A9BE1A" },
                ]}
                    key={i}>
                    <Text style={{
                        color: item.selected ? "white" : "black",
                        fontSize: 15
                    }}>
                        {moment(item.startTime, "HHmmss").format("hh:mm A")}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const keyExtractor = (item, index) => index.toString();

    const HeaderBookAppointment = () => {
        let _experience = docProfile.experience;
        let _speciality = ifValid(docProfile.specialityId) ? getSpecialitiesNames(docProfile?.specialityId, "name") : "-";

        let _hinspeciality = ifValid(docProfile.specialityId)
            ? getSpecialitiesNames(docProfile.specialityId, "hindiName")
            : _speciality

        if (ifNotValid(_experience)) _experience = "-"
        else _experience = _experience + strings.years_experiance

        let _hindiFullName = ifValid(doc.hindiFullName) ? doc.hindiFullName : doc.fullName
        if (ifNotValid(_hindiFullName)) _hindiFullName = doc.fullName

        if (ifNotValid(_speciality)) _speciality = "-"
        else _speciality = _speciality

        let gender = doc.gender;
        let _imageUrl = doc.imageUrl;

        if (ifNotValid(_imageUrl)) _imageUrl = gender == "FEMALE" ? Images.female_doctor : Images.male_doctor
        else _imageUrl = { uri: _imageUrl }

        let txtStyleExtra = { color: '#FFF' }

        return <SafeAreaView style={[styles.header, { backgroundColor: '#141E46' }]}>
            <HeaderSehet hideshadow headerText={strings.Book_appointment} navigation={props.navigation} changeTheme={true} />
            <View style={styles.docDetails}>
                <Image source={_imageUrl} style={styles.headerImg} />
                <View style={styles.docDesc}>
                    {/* <Text numberOfLines={1} style={styles.docName}>{doc.fullName}</Text> */}
                    <PowerTranslator
                        style={[styles.docName, txtStyleExtra]}
                        enText={doc.fullName}
                        hinText={_hindiFullName}
                        numberOfLines={1}
                    />
                    {/* <Text style={styles.specializationStyle}>{_speciality}</Text> */}
                    <PowerTranslator
                        style={[styles.specializationStyle, txtStyleExtra]}
                        hinText={_hinspeciality}
                        enText={_speciality}
                    />
                    {/* <Text style={styles.specializationStyle}>{_experience}</Text> */}
                    <PowerTranslator
                        style={[styles.specializationStyle, txtStyleExtra]}
                        hinText={_experience}
                        enText={_experience}
                    />
                    <Text onPress={() => {
                        dispatch({ type: DOC_PROFILE, payload: {} });
                        dispatch(getDocProfile(doc._id))
                        setProfileModel(!modelProfile)
                    }} style={[styles.viewProfileStyle, txtStyleExtra]}>{strings.View_Profile}</Text>
                </View>
            </View>
        </SafeAreaView>
    }

    const handleConfirm = (date) => {
        setIsDatePickerVisible(false)
        getSelectedDayEvents(date)
    }

    const handleCancel = () => {
        setIsDatePickerVisible(false)
    }

    const RenderFee = ({ }) => {
        const isOnline = selectedCosultMode == 'ONLINE'
        return <View style={styles.datePicker}>
            <View style={{
                backgroundColor: '#CFE1D9',
                paddingVertical: 10,
                paddingHorizontal: 8,
                borderRadius: 5,
                alignItems: 'center'
            }}>
                <View style={{ flex: 1 }}>
                    <Text style={{
                        fontSize: 16,
                        fontFamily: FONT_FAMILY_SF_PRO_REGULAR
                    }}
                    >{`${isOnline ? "Online" : "Offline"} Appointment Charges`}</Text>
                    <Text style={{
                        fontSize: 16,
                        fontWeight: 'bold',
                        textAlign: 'center',
                        marginVertical: 3
                    }}>{`₹${isOnline ? fee : offlineFee}`}</Text>
                </View>
            </View>
        </View>
    }


    return (
        <Container>
            {HeaderBookAppointment()}
            <Content>
                <View style={{ backgroundColor: '#FFF' }}>

                    <View style={styles.datePicker}>
                        {/* <Text style={[styles.text, {}]}>{strings.consult_modes}</Text> */}
                        <View style={[styles.calendar, { flexDirection: 'row', alignItems: 'center', marginHorizontal: 15 }]}>

                            <TouchableOpacity
                                onPress={() => setSelectedconsultMode("ONLINE")}
                                style={{
                                    flex: 1,
                                    marginRight: 3,
                                    paddingHorizontal: 20,
                                    paddingVertical: 5,
                                    borderRadius: 5,
                                    backgroundColor: selectedCosultMode == "ONLINE" ? "#11693A" : '#FFF',
                                    borderColor: '#11693A',
                                    borderWidth: 1,
                                    flexDirection: 'row',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}
                            >
                                <Text style={{
                                    color: selectedCosultMode == "ONLINE" ? "#FFF" : '#11693A',
                                    fontSize: 16,
                                }}>{"Online\nAppointment"}</Text>
                            </TouchableOpacity>

                            {
                                isConsulationModeOffline == true
                                    ? <TouchableOpacity
                                        onPress={() => setSelectedconsultMode("OFFLINE")}
                                        style={{
                                            flex: 1,
                                            marginLeft: 3,
                                            paddingHorizontal: 20,
                                            paddingVertical: 15,
                                            backgroundColor: selectedCosultMode == "OFFLINE" ? '#11693A' : "#F0F1F4",
                                            borderColor: '#11693A',
                                            borderWidth: 1,
                                            borderRadius: 5,
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                    >
                                        <Text style={{
                                            color: selectedCosultMode == "OFFLINE" ? "#FFF" : '#11693A',
                                            fontSize: 16,
                                        }}>{"Clinic Visit"}</Text>
                                    </TouchableOpacity>
                                    : <View
                                        style={{
                                            flex: 1,
                                            marginLeft: 3,
                                            paddingHorizontal: 20,
                                            paddingVertical: 10,
                                            backgroundColor: '#F9F9F9',
                                            borderRadius: 5,
                                            flexDirection: 'row',
                                            alignItems: 'center',
                                            justifyContent: 'center'
                                        }}
                                    >
                                    </View>
                            }
                        </View>
                    </View>

                    <View style={{ marginHorizontal: 15, marginTop: -5 }}>
                        <RenderFee />
                    </View>

                    <View style={styles.datePicker}>
                        <Text style={styles.text}>{strings.select_date}</Text>
                        <View style={styles.calendar}>
                            <TouchableOpacity
                                style={{ marginHorizontal: 10, paddingHorizontal: 20, paddingVertical: 5, backgroundColor: '#FFF', borderRadius: 8, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderWidth: 1, borderColor: '#A0A9BE' }}
                                onPress={() => setIsDatePickerVisible(!isDatePickerVisible)}
                            >
                                <Text style={{ color: 'black', fontSize: 16 }}>{isValid(selectedDate) ? moment(selectedDate).format('Do MMMM YYYY, dddd') : "Select Date"}</Text>
                                <Icon name={`${isDatePickerVisible ? 'keyboard-arrow-up' : 'keyboard-arrow-down'}`} type="MaterialIcons" />
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={styles.timePicker}>
                        {data !== undefined && data.length > 0 ? (
                            <Text style={styles.text}>{strings.select_time}</Text>
                        ) : (
                            <Text style={{ flex: 1, textAlign: "center" }}>
                                {strings.noSlots}
                            </Text>
                        )}
                        <View style={styles.timeBtns}>
                            {data !== null && data.length > 0 ? (
                                <FlatList
                                    key={data}
                                    keyExtractor={keyExtractor}
                                    style={{ flex: 1 }}
                                    data={data}
                                    numColumns={4}
                                    renderItem={({ item }, index) => {
                                        return _renderItem(item, index);
                                    }}
                                />
                            ) : null}
                        </View>
                    </View>
                </View>
                <ViewProfileModal
                    onTouchOutside={() => {
                        // dispatch({ type: DOC_PROFILE, payload: {} });
                        setProfileModel(!modelProfile)
                    }}
                    onHardwareBackPress={() => {
                        dispatch({ type: DOC_PROFILE, payload: {} });
                        setProfileModel(!modelProfile)
                    }}
                    specialization={null}
                    name={undefined}
                    experience={undefined}
                    imageUrl={""}
                    visible={modelProfile} />
            </Content>
            <Footer style={styles.footer}>
                <Button danger style={[styles.footerBtn, { backgroundColor: '#D72820' }]} onPress={() => postSlot()}>
                    <Text style={{}} uppercase={false}>{strings.continue}</Text>
                </Button>
            </Footer>

            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="date"
                date={new Date(selectedDate)}
                minimumDate={new Date()}
                onConfirm={handleConfirm}
                onCancel={handleCancel}
            />

        </Container>
    );
};
export default BookAppointmentScreenFlow2;
