import { BOOKING_FLOW, SELECTED_SLOT_OBJ, SELECTED_SLOT_OBJ_FOR_QUICK_CALL } from "../../../utils/constant";

export const selectedSlotObj = (payload) => {
    return {
        type: SELECTED_SLOT_OBJ,
        payload: payload,
    };
};

export const setIsBookingFlow1or2 = (payload) => {
    return {
        type: BOOKING_FLOW,
        payload: payload,
    };
};

export const selectedSlotObjForQuickCall = (payload) => {
    return {
        type: SELECTED_SLOT_OBJ_FOR_QUICK_CALL,
        payload: payload,
    };
};