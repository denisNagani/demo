import React, { useEffect, useState } from "react";
import { FlatList, Image, TouchableOpacity, View } from "react-native";
import { Button, Container, Content, Footer, Text, Icon } from "native-base";
import { Calendar } from "react-native-calendars";
import styles from "./style";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { emptySlot, getSlotTimeZone, postDoctorSlot, setSummaryDateNTime, } from "../../../actions/appointment";
import { setDoctorImage, setDoctorName } from "../../../actions/bookings";
import { showModal } from "../../../actions/modal";
import { methodLog, recordError, successLog } from "../../../utils/fireLog";
import HeaderSehet from "../../../components/header-common";
import { getDoctorsSlotFlow1 } from "../../../actions/sehet/booking-flow1-action";
import { ifValid, isValid } from "../../../utils/ifNotValid";
import Images from "../../../assets/images";
import { errorPayload } from "../../../utils/helper";
import { setIsBookingFlow1or2 } from "./action";
import { strings } from "../../../utils/translation";
import http from "../../../services/http";
import { hide, show } from "../../../utils/loader/action";
import DateTimePickerModal from "react-native-modal-datetime-picker";

const BookAppointmentScreen = (props) => {
    const dispatch = useDispatch();
    const navigation = props.navigation;
    const [markedDates, setMarkedDates] = useState({});
    const [selectedDate, setSelectedDate] = useState(null);
    const [data, setData] = useState([]);
    const [selectedObject, setSelectedObject] = useState(null);
    const doctorSlot = useSelector((state) => state.appointment.docSlot);
    const slotTimeZone = useSelector((state) => state.appointment.slotTimeZone);
    const get_price = useSelector(state => state.specialitiesReducer.selectedSpeciality)
    let selectSlotLimit = 1;
    // const doc = props.navigation.state.params.doc;
    const selectedSpeciality = useSelector((state) => state.specialitiesReducer.selectedSpeciality);
    const doc = selectedSpeciality ? selectedSpeciality : props.navigation.state.params.doc;
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);

    useEffect(() => {
        // const listener = navigation.addListener("didFocus", () => {
        getSelectedDayEvents(new Date());
        dispatch(getSlotTimeZone())
        // });
    }, []);

    const addSelectedProperty = (dataOld) => {
        methodLog("addSelectedProperty")
        try {
            let data = dataOld.map((obj) => {
                return { ...obj, selected: false };
            });
            setData(data);
        } catch (e) {
            recordError(e)
        }
    };

    const updateTimeData = (_id) => {
        methodLog("updateTimeData")
        try {
            let result = data.map((o) => {
                if (o._id === _id) {
                    setSelectedObject(o);
                    return { ...o, selected: !o.selected };
                } else {
                    if (selectSlotLimit === 1) return { ...o, selected: false };
                    else return o;
                }
            });
            setData(result);
        } catch (e) {
            recordError(e)
        }
    };

    const getSelectedDayEvents = (date) => {
        methodLog("getSelectedDayEvents")
        try {
            let obj = {
                [moment(date).format("YYYY-MM-DD")]: {
                    selected: true,
                    endingDay: true,
                    selectedColor: "#1B80F3",
                    color: "green",
                    textColor: "gray",
                },
            };
            let serviceDate = moment(date).format("YYYY-MM-DD");
            let time = moment().format("HH:mm:ss");
            let dateTime = moment(serviceDate + " " + time).format();
            setMarkedDates(obj);
            setSelectedDate(dateTime);
        } catch (e) {
            recordError(e)
        }
    };

    useEffect(() => {
        if (selectedDate !== null) dispatch(getDoctorsSlotFlow1(doc._id, selectedDate));
    }, [selectedDate]);

    useEffect(() => {
        successLog("BookAppointmentScreen screen loaded")
        if (ifValid(doctorSlot)) {
            addSelectedProperty(doctorSlot);
        }
    }, [doctorSlot]);


    const postSlot = () => {
        methodLog("postSlot");

        try {
            if (selectedObject !== null) {
                let datee = moment(selectedDate).format("YYYY-MM-DD");

                if (selectedDate == null) {
                    dispatch(showModal(errorPayload('Please select date')))
                    return;
                }

                dispatch(show())
                http
                    .get(`api/slots/${doc?._id}/${datee}/${selectedObject?.startTime}`, dispatch)
                    .then((res) => {
                        console.log(res);
                        if (res?.data) {
                            const doctor = res?.data?.doctorId
                            let data = {
                                doctorId: doctor?._id,
                                appointmentDate: datee,
                                appointmentType: "VIDEO",
                                slotId: selectedObject._id,
                                status: "BOOKED",
                                timeZone: slotTimeZone.timeZone,
                                type: "BOOKING"
                            };
                            console.log(JSON.stringify(data))

                            setSelectedDate(null);
                            setMarkedDates({});

                            dispatch(setSummaryDateNTime({
                                appointmentDate: datee,
                                appointmentTime: ifValid(selectedObject.startTime) ? selectedObject.startTime : ''
                            }));
                            dispatch(postDoctorSlot(data, props.navigation, get_price));
                            dispatch(setDoctorName(doc.fullName));
                            dispatch(setDoctorImage(doc.imageUrl));
                            dispatch(setIsBookingFlow1or2(1));
                            dispatch(hide())
                        }
                    })
                    .catch(err => {
                        console.log(err);
                        dispatch(hide())
                    })
            } else {
                let payload = {
                    text: "Please select date and time!!",
                    subtext: "",
                    iconName: "closecircleo",
                    modalVisible: true,
                };

                if (data === undefined || !data.length > 0) {
                    payload = { ...payload, text: 'No slots available' };
                }
                if (selectedDate == null) {
                    payload = { ...payload, text: 'Please select date' };
                }
                dispatch(showModal(payload))
            }
        } catch (e) {
            recordError(e)
        }
    };


    const _renderItem = (item, i) => {
        return item.status === "INACTIVE" || item.isTimeout === true ? (
            <TouchableOpacity style={styles.itemContainer} disabled onPress={() => updateTimeData(item._id)}>
                <View style={styles.item} key={i}>
                    <Text
                        style={{
                            color: "#4e5c76",
                            opacity: 0.5,
                        }}
                    >
                        {moment(item.startTime, "HHmmss").format("hh:mm")}
                    </Text>
                </View>
            </TouchableOpacity>
        ) : (
            <TouchableOpacity style={styles.itemContainer} onPress={() => updateTimeData(item._id)}>
                <View
                    style={[
                        styles.item,
                        { backgroundColor: item.selected ? "#038BEF" : "#A0A9BE1A" },
                    ]}
                    key={i}
                >
                    <Text style={{ color: item.selected ? "white" : "black" }}>
                        {moment(item.startTime, "HHmmss").format("hh:mm")}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    };
    const keyExtractor = (item, index) => index.toString();

    const handleConfirm = (date) => {
        setIsDatePickerVisible(false)
        getSelectedDayEvents(date)
    }

    const handleCancel = () => {
        setIsDatePickerVisible(false)
    }


    return (
        <Container>
            <HeaderSehet headerText={strings.Book_appointment} navigation={props.navigation} />
            <Content>
                <View>
                    <View style={styles.datePicker}>
                        <Text style={[styles.text, { marginBottom: 10, fontSize: 18, textAlign: 'center' }]}>{doc?.name}</Text>
                        <Text style={styles.text}>{strings.select_date}</Text>
                        {/* <Text style={styles.timezone}>All slots are according
                            to {slotTimeZone !== undefined && slotTimeZone !== null ? slotTimeZone.name : "..."}</Text> */}
                        <View style={styles.calendar}>
                            <TouchableOpacity
                                style={{ marginHorizontal: 10, paddingHorizontal: 20, paddingVertical: 10, backgroundColor: '#FFF', borderRadius: 8, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}
                                onPress={() => setIsDatePickerVisible(!isDatePickerVisible)}
                            >
                                <Text style={{ color: 'black', fontSize: 16 }}>{isValid(selectedDate) ? moment(selectedDate).format('Do MMMM YYYY, dddd') : "Select Date"}</Text>
                                <Icon name={`${isDatePickerVisible ? 'keyboard-arrow-up' : 'keyboard-arrow-down'}`} type="MaterialIcons" />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={styles.timePicker}>
                        {data !== undefined && data.length > 0 ? (
                            <Text style={styles.text}>{strings.select_time}</Text>
                        ) : selectedDate == null ? null :
                            (
                                <Text style={{ flex: 1, textAlign: "center" }}>
                                    {strings.noSlots}
                                </Text>
                            )}
                        <View style={styles.timeBtns}>
                            {data !== null && data.length > 0 ? (
                                <FlatList
                                    key={data}
                                    keyExtractor={keyExtractor}
                                    style={styles.list}
                                    data={data}
                                    numColumns={4}
                                    renderItem={({ item }, index) => {
                                        return _renderItem(item, index);
                                    }}
                                />
                            ) : null}
                        </View>
                    </View>
                </View>
            </Content>
            <Footer style={styles.footer}>
                <Button danger style={styles.footerBtn} onPress={() => postSlot()}>
                    <Text>{strings.continue}</Text>
                </Button>
            </Footer>


            <DateTimePickerModal
                isVisible={isDatePickerVisible}
                mode="date"
                date={new Date(selectedDate)}
                minimumDate={new Date()}
                onConfirm={handleConfirm}
                onCancel={handleCancel}
            />

        </Container>
    );
};
export default BookAppointmentScreen;
