import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_12, FONT_SIZE_16 } from "../../../styles/typography";

export default (styles = StyleSheet.create({
	f_white: {
		color: "#FFFFFF",
	},
	container: {
		backgroundColor: "#ffffff",
	},
	header: {
		height: hp("13%"),
		backgroundColor: "#1E2081",
		paddingHorizontal: wp("5%"),
	},
	heading: {
		flexDirection: "row",
		marginTop: hp("6%"),
		alignItems: "center",
	},
	headerText: {
		color: "#fff",
		fontSize: hp("2.9%"),
		alignSelf: "center",
		marginLeft: wp("5%"),
	},
	headerBackIcon: {
		fontSize: hp("3.5%"),
		color: "#fff",
	},
	headerImg: {
		width: 110,
		height: 110,
		borderRadius: 110 / 2,
	},
	docDetails: {
		flexDirection: "row",
		marginTop: hp("5%"),
		marginLeft: wp("15%"),
	},
	docDesc: {
		flexDirection: "column",
		justifyContent: "space-evenly",
		marginLeft: wp("8%"),
		marginRight: wp("25%"),
	},
	docName: {
		color: "#FFFFFF",
		fontSize: hp("2.9%"),
	},
	datePicker: {
		marginTop: 20,
	},
	text: {
		marginLeft: 20,
		fontSize: FONT_SIZE_16,
		color: "#041A32",
		fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
	},
	timezone: {
		marginLeft: 20,
		fontSize: FONT_SIZE_12,
		color: "gray",
	},
	calendar: {
		marginTop: 10,
	},
	calendarIconStyle: {
		width: wp("15%"),
		height: wp("15%"),
	},
	timeZone: {
		marginTop: 10,
	},
	timePicker: {
		marginTop: 20,
	},
	timeBtns: {
		marginTop: 10,
	},
	item: {
		flex: 1,
		borderRadius: 6,
		height: 30,
		margin: 10,
		backgroundColor: "#A0A9BE1A",
		justifyContent: "center",
		alignItems: "center",
	},
	itemContainer: {
		width: '24%'
	},

	list: {
		flex: 1,
	},
	footer: {
		height: 80,
		backgroundColor: "#ffffff",
		justifyContent: "center",
		alignItems: "center",
	},
	footerBtn: {
		alignSelf: "center",
		justifyContent: "center",
		backgroundColor: '#FF5E38',
		alignItems: "center",
		width: wp("90%"),
		borderRadius: 9,
	},
	tAndCText: {
		color: '#A0A9BE',
		fontSize: FONT_SIZE_12,
		textAlign: 'center'
	},
	tAndCLink: {
		color: "#038BEF",
		fontSize: FONT_SIZE_12,
		fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
	},

}));
