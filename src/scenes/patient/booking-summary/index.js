import React, { useEffect, useState } from "react";
import { Alert, Image, Text, View, BackHandler, TouchableOpacity } from "react-native";
import { Button, Container, } from "native-base";
import styles from "./style";
import Images from "../../../assets/images/index";
import Line from "../../../components/line";
import common from "../../../styles/common";
import { FONT_SIZE_15, FONT_SIZE_18, FONT_SIZE_16, FONT_FAMILY_SF_PRO_MEDIUM } from "../../../styles/typography";
import HeaderSehet from "../../../components/header-common";
import { useDispatch, useSelector } from "react-redux";
import { ifNotValid, ifValid, isValid } from "../../../utils/ifNotValid";
import moment from "moment";
import { getDocProfile } from "../../../actions/sehet/user-action";
import { DOCTOR_SLOT } from "../../../utils/constant";
import Footer from "../../../components/Footer";
import ApplyCouponModal from "../../../components/ApplyCouponModal";
import { ScrollView } from "react-native-gesture-handler";
import { setApplyCouponModal } from "../../../actions/sehet/speciality-action";
import { TranslationConfig, strings } from "../../../utils/translation";
// import PowerTranslator from "react-native-power-translator";
import PowerTranslator from "../../../components/PowerTranslator";
import PlansModal from "../../../components/PlansModal";
import { setAppointmentType } from "../../../actions/appointment";

const BookingSummaryScreen = (props) => {
    const dispatch = useDispatch();

    const applyCouponModal = useSelector((state) => state.specialitiesReducer.applyCouponModal);
    const planSelectModal = useSelector((state) => state.appointment.selectPlanModal);
    const selectedSpeciality = useSelector((state) => state.specialitiesReducer.selectedSpeciality);
    const selectedSlotObj = useSelector((state) => state.appointment.selectedSlotObj);
    const isCopoupApplied = useSelector((state) => state.appointment.appliedCoupon);
    const getFlow = useSelector((state) => state.appointment.bookingFlow1or2);
    let docProfile = useSelector((state) => state.userReducer.docProfile);
    const [isSlotAvailable, setIsSlotAvailable] = useState(true);
    const summaryDateNTime = useSelector((state) => state.appointment.summaryDateNTime);
    // const dis_value = ifValid(props.navigation.state.params.dis_value) ? props.navigation.state.params.dis_value : 0;
    const dis_value = useSelector((state) => state.specialitiesReducer.dis_price);
    const isApplied = ifValid(isCopoupApplied) ? isCopoupApplied.isApplied : false;
    const lang = useSelector(state => state.profile.selected_language)
    TranslationConfig(lang)
    const [count, setCount] = useState(0)
    const userProfile = useSelector(state => state.profile.userProfile)


    const handleBackButtonClick = () => {
        Alert.alert(strings.payment_pending,
            strings.payment_pending_sure, [
            {
                text: "Cancel", onPress: () => {
                }
            },
            {
                text: "OK", onPress: () => {
                    dispatch({ type: DOCTOR_SLOT, payload: [] });
                    if (getFlow == 1) {
                        props.navigation.navigate('BookAppointmentScreen');
                    } else {
                        props.navigation.navigate('BookAppointmentScreenFlow2');
                    }
                }
            },
        ]);
        return true;
    }

    const onBackPress = () => {
        if (applyCouponModal) {
            return true
        } else {
            return false
        }
    }

    // useEffect(() => {
    //     BackHandler.addEventListener("hardwareBackPress", onBackPress);
    // }, [BackHandler])

    useEffect(() => {
        if (ifValid(selectedSlotObj) && ifValid(selectedSlotObj.doctorId)) {
            dispatch(getDocProfile(selectedSlotObj.doctorId))
        }
    }, [])

    docProfile = ifValid(docProfile.userId) ? docProfile.userId : {};
    let name = docProfile.fullName;
    if (ifNotValid(name)) name = "..."

    let hindiname = docProfile.hindiFullName;
    if (ifNotValid(hindiname)) hindiname = name

    let price = isApplied ? selectedSpeciality.price : selectedSpeciality.m_price;
    if (ifNotValid(price)) price = "..."
    if (price == 0) price = "Free"

    let m_price = selectedSpeciality.m_price;
    if (ifNotValid(m_price)) m_price = "..."

    let imageUrl = docProfile.imageUrl;
    if (ifNotValid(imageUrl) || imageUrl === "") imageUrl = Images.user
    else imageUrl = { uri: imageUrl }

    let time = ifValid(summaryDateNTime) ? ifValid(summaryDateNTime.appointmentTime) ? summaryDateNTime.appointmentTime : '' : ''
    let date = ifValid(summaryDateNTime) ? ifValid(summaryDateNTime.appointmentDate) ? summaryDateNTime.appointmentDate : '' : ''

    const checkSlotAvailability = () => {
        const body = {
            ...selectedSlotObj,
            paidAmount: selectedSpeciality.price
        }
        dispatch(checkSlotAvailable(body, props.navigation))
    }

    return (
        <Container style={{
            flex: 1, backgroundColor: '#F9F9F9'
        }}>

            <HeaderSehet
                onPressCustom={() => handleBackButtonClick()}
                navigation={props.navigation}
                headerText={strings.booking_summary}
                changeTheme={true} />

            <ScrollView>

                <Container style={{ margin: 20, backgroundColor: '#F9F9F9' }}>
                    <View style={{ marginVertical: 15 }}>
                        <Text style={{ color: '#656E83' }}>{strings.selected_speciality}</Text>
                        <View style={[styles.row, { marginVertical: 20 }]}>
                            <Image source={imageUrl}
                                style={styles.specialityImg} />
                            <View style={{ marginLeft: 15 }}>
                                {/* <Text numberOfLines={2} style={styles.specialityName}>Dr. {name}</Text> */}
                                <PowerTranslator
                                    style={styles.specialityName}
                                    enText={`${name}`}
                                    hinText={`${hindiname}`}
                                />
                                <Text style={styles.specialityPrice}>₹{m_price}</Text>
                            </View>
                        </View>
                    </View>

                    <Line lineColor={"#B6BECB"} />

                    <View style={{ marginVertical: 15 }}>
                        <Text style={{ color: '#656E83' }}>{strings.booking_details}</Text>
                        <View style={[styles.row, , { marginVertical: 20 }]}>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                                <Image source={Images.icon_clock} style={styles.bookingIcon} />
                                <Text
                                    style={styles.timeStyle}>{time === '' ? "" : moment(time, 'HH:mm:ss').format("hh:mm A")}</Text>

                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                                <Image source={Images.icon_calender}
                                    style={styles.bookingIcon} />
                                <Text
                                    style={styles.timeStyle}>{date === '' ? "" : moment(date).format("DD MMM YYYY")}</Text>
                            </View>
                        </View>
                    </View>

                    <Line lineColor={"#B6BECB"} />

                    <View style={{ marginVertical: 5, justifyContent: 'center' }}>
                        <TouchableOpacity onPress={() => {
                            dispatch(setApplyCouponModal(true))
                        }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 25 }}>
                                <Image source={Images.icon_coupen}
                                    style={{ width: 25, height: 23, alignSelf: 'center', marginRight: 10 }} />
                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                    <Text style={{
                                        fontSize: FONT_SIZE_16,
                                        alignSelf: 'center',
                                        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
                                    }}>{strings.apply_Coupon}</Text>
                                    <Image source={Images.icon_arrow_right}
                                        style={{ width: 18, height: 18, alignSelf: 'center' }} />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <Line lineColor={"#B6BECB"} />
                    <View style={{ marginVertical: 15 }}>
                        <Text style={{ marginTop: 20, color: '#656E83' }}>{strings.payment_details}</Text>
                        <View>
                            <View style={styles.rowSpaceBetween}>
                                <Text style={{ fontSize: FONT_SIZE_15 }}>{strings.consultation_fee}</Text>
                                <Text>₹ {m_price}</Text>
                            </View>
                            <View style={styles.rowSpaceBetween}>
                                <Text style={{ fontSize: FONT_SIZE_15 }}>{strings.taxes}</Text>
                                <Text>₹ 0</Text>
                            </View>
                            <View style={styles.rowSpaceBetween}>
                                <Text style={{ fontSize: FONT_SIZE_15 }}>{strings.convinience_fee}</Text>
                                <Text>₹ 0</Text>
                            </View>
                            <View style={styles.rowSpaceBetween}>
                                <Text style={{ fontSize: FONT_SIZE_15 }}>{strings.discount}</Text>
                                <Text>₹ {ifValid(dis_value) ? dis_value : m_price}</Text>
                            </View>
                            <View style={styles.rowSpaceBetween}>
                                <Text style={{ fontSize: FONT_SIZE_15, fontWeight: 'bold' }}>{strings.total_pay}</Text>
                                <Text style={{ fontWeight: 'bold', color: '#038BEF', fontSize: FONT_SIZE_18 }}>{price == "Free" ? "" : "₹"}{price}</Text>
                            </View>
                        </View>
                    </View>

                </Container>
                <ApplyCouponModal visible={applyCouponModal} />
                <PlansModal visible={planSelectModal} />
            </ScrollView>
            <Footer>
                <Button danger disabled={!isSlotAvailable} style={[common.footerBtn, {
                    margin: 0,
                    alignSelf: 'center',
                    backgroundColor: isSlotAvailable ? '#D72820' : '#b5b5b5'
                }]}
                    onPress={() => {
                        let user = userProfile?.userId
                        let isUpdatedProfile = isValid(user?.fullName) && isValid(user?.email) && isValid(user?.gender)
                        if (isUpdatedProfile) {
                            props.navigation.navigate("SelectMember")
                        } else {
                            props.navigation.navigate("PatientDetails")
                        }
                        dispatch(setAppointmentType('regular'))
                    }}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Text uppercase={false}
                            style={[styles.number91, { color: 'white', flex: 1, paddingHorizontal: 15, fontSize: 15 }]}>
                            {strings.continue_payment}
                        </Text>
                        <Text uppercase={false}
                            style={[styles.number91, { color: 'white', paddingHorizontal: 10, fontSize: 15 }]}>{price == "Free" ? "" : "₹"}{price}</Text>
                    </View>
                </Button>
            </Footer>

        </Container>
    );
}

export default BookingSummaryScreen
