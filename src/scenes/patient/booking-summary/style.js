import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_14, FONT_SIZE_17 } from "../../../styles/typography";

export default (styles = StyleSheet.create({
    header: {
        backgroundColor: "#038BEF",
        height: hp("10%"),
        flexDirection: "row",
        alignItems: "center",
    },
    headerBackIcon: {
        flex: 1,
        fontSize: hp("3.5%"),
        color: "#fff",
        marginLeft: wp("5%"),
    },
    heading: {
        flex: 8,
        color: "#fff",
        fontSize: hp("2.5%"),
    },
    row: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    rowSpaceBetween: {
        flexDirection: 'row', justifyContent: 'space-between', marginTop: 20,
    },

    specialityImg: {
        height: 50,
        width: 50,
        borderRadius: 25
    },
    bookingIcon: {
        height: 20,
        width: 20,
        margin: 10,
    },
    alignCenter: {
        alignSelf: 'center',
    },
    specialityName: {
        fontSize: FONT_SIZE_17,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: 'black'
    },
    specialityPrice: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
        color: '#484F5F'
    },
    timeStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_17,
        color: '#041A32'
    }

}));
