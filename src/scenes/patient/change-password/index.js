import React, {Component} from "react";
import {Text} from "react-native";
import {
    Body,
    Button,
    Container,
    Content,
    Footer,
    Form,
    Header,
    Icon,
    Input,
    Item,
    Label,
    Left,
    Title,
    View,
} from "native-base";
import styles from "./style";
import http from "../../../services/http";
import validate from "../../../utils/validation_wrapper";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {getUserDetails} from "../../../actions/profile";
import {showModal} from "../../../actions/modal";
import {errorLog, methodLog, recordError, successLog} from "../../../utils/fireLog";
import HeaderSehet from "../../../components/header-common";

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
            password: null,
            passwordError: null,
        };
    }

    isValid = () => {
        const passwordError = validate("password", this.state.password);
        this.setState({passwordError});
        return !passwordError;
    };

    _onBlur(key) {
        this.setState({
            [`${key}Error`]: validate(key, this.state[key]),
        });
    }

    componentDidMount() {
        successLog("ChangePassword screen loaded")
    }

    _onChangeValue(key, value) {
        this.setState({[key]: value});
        this.isValid();
    }

    changePassword() {
        methodLog("changePassword")
        try {
            if (this.isValid()) {
                http
                    .put("api/user/changePassword", {
                        password: this.state.password,
                    })
                    .then((res) => {
                        successLog("changePassword", res.status)
                        if (res.status === 200) {
                            const payload = {
                                text: "Password changed Successfully",
                                subText: "",
                                iconName: "checkcircleo",
                                modalVisible: true,
                            };
                            this.props.showModal(payload);
                            this.props.navigation.goBack();
                        } else {
                            // alert(res.message)
                            const payload = {
                                text: res.message,
                                subText: "",
                                iconName: "closecircleo",
                                modalVisible: true,
                                iconColor: "#C70315",
                            };
                            this.props.showModal(payload);
                        }
                    })
                    .catch((err) => {
                        errorLog("changePassword", err)
                        console.log(err);
                    });
            }
        } catch (e) {
            recordError(e)
        }
    }

    render() {
        const {photo} = this.state;
        return (
            <Container>

                <HeaderSehet navigation={this.props.navigation} headerText="Change Password"/>
                <Content>
                    <Form>
                        <View style={styles.formGroup}>
                            <Item
                                error={this.state.passwordError !== null}
                                floatingLabel
                                style={[styles.password, styles.input]}
                            >
                                <Label style={styles.label}>New Password</Label>
                                <Input
                                    value={this.state.password}
                                    onChangeText={(value) =>
                                        this._onChangeValue("password", value)
                                    }
                                />
                            </Item>
                        </View>
                    </Form>
                </Content>

                <Footer style={styles.footer}>
                    <Button
                        style={[styles.footerBtn, styles.cancelBtn]}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                    >
                        <Text style={styles.footerBtnText}>Cancel</Text>
                    </Button>
                    <Button
                        onPress={() => this.changePassword()}
                        danger
                        style={styles.footerBtn}
                    >
                        <Text style={styles.footerBtnText}>Change Password</Text>
                    </Button>
                </Footer>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({getUserDetails, showModal}, dispatch);

const mapStateToProps = (state) => ({
    // upcomingBookings: state.bookings.upcomingBookings,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ChangePassword);
