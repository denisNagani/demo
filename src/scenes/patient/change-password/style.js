import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export default (styles = StyleSheet.create({
	header: {
		backgroundColor: "#1E2081",
		height: 100,
	},
	heading: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: 20,
		marginTop: 20,
	},
	headerBackIcon: {
		fontSize: hp("3.5%"),
		color: "#fff",
	},

	formGroup: {
		flex: 1,
		flexDirection: "row",
		padding: "2%",
	},

	input: {
		padding: "2%",
	},
	label: {
		color: "gray",
	},
	password: {
		flex: 1,
	},
	btnText: {
		color: "#fff",
		fontSize: 20,
	},

	footerBtn: {
		flex: 0.5,
		margin: 10,
		justifyContent: "center",
		fontSize: 20,
	},
	cancelBtn: {
		backgroundColor: "#A0A9BE",
	},
	footer: {
		height: 70,
		backgroundColor: "#fff",
	},
	footerBtnText: {
		color: "#fff",
		fontSize: 15,
		fontWeight: "bold",
	},
}));
