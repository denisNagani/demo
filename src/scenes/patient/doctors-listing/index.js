import React, { useEffect, useState } from "react";
import { Container, Icon, Input, Text } from "native-base";
import { FlatList, Image, TouchableOpacity, View } from "react-native";
import { ListItem } from "react-native-elements";
import styles from "./style";
import { findDoctors } from "../../../actions/profile";
import { useDispatch, useSelector } from "react-redux";
import { showModal } from "../../../actions/modal";
import Images from "../../../assets/images";
import { methodLog, recordError, successLog } from "../../../utils/fireLog";

const DoctorListing = (props) => {
    const doctors = useSelector((state) => state.profile.docList);
    const [_doctors, setDoctors] = useState([]);
    const [docName, setDocName] = useState("");
    const dispatch = useDispatch();

    const showCallClinic = () => {
        methodLog("showCallClinic")
        const payload = {
            text:
                "Cannot book appointment for this doctor from app, please call clinic to book an appointment",
            iconName: "checkcircleo",
            isIcon: false,
            modalVisible: true,
            button: true,
        };
        dispatch(showModal(payload));
    };

    useEffect(() => {
        successLog("Doctor listing screen loaded")
        dispatch(findDoctors());
    }, []);

    useEffect(() => {
        try {
            if (doctors !== undefined && doctors !== null && doctors.length > 0) {

                if (docName.length > 0) {

                    let searchName = docName;

                    if (searchName.includes("Dr")) {
                        searchName = docName.replace("Dr", "").trim()
                    }
                    if (searchName.includes(".")) {
                        searchName = docName.replace(".", "").trim()
                        searchName = searchName.replace("Dr", "").trim()
                    }


                    console.log(searchName)
                    let result = doctors.filter((item) => {
                        let name = item.fullName.toLowerCase();
                        let specialization =
                            item.specialization !== undefined && item.specialization !== null
                                ? item.specialization.toLowerCase()
                                : "";
                        return (
                            name.includes(searchName.toLowerCase()) ||
                            specialization.includes(searchName.toLowerCase())
                        );
                    });
                    setDoctors(result);
                } else setDoctors(doctors);
            }
        } catch (e) {
            recordError(e)
        }
    }, [doctors, docName]);

    const keyExtractor = (item, index) => index.toString();

    const renderItem = ({ item }) => {
        return (
            <ListItem
                title={`${item.fullName}`}
                subtitle={item.specialization}
                leftAvatar={
                    item.imageUrl !== "" && item.imageUrl !== null ? (
                        {
                            source: { uri: item.imageUrl },
                            avatarStyle: { width: 44, height: 44, borderRadius: 22 },
                        }
                    ) : (
                            <View
                                style={{
                                    height: 44,
                                    width: 44,
                                    backgroundColor: "pink",
                                    borderRadius: 22,
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                avtar
                            >
                                <Text avtarTxt>
                                    {item.fullName
                                        .split(" ")
                                        .map((n) => n[0])
                                        .join("")}
                                </Text>
                            </View>
                        )
                }
                bottomDivider
                rightElement={() => (
                    <TouchableOpacity
                        style={styles.listBtn}
                        activeOpacity={1}
                        onPress={() =>
                            item.isAvailableToBook
                                ? props.navigation.navigate("BookAppointmentScreen", {
                                    doc: item,
                                })
                                : showCallClinic()
                        }
                    >
                        <Text style={styles.listBtnText}>Book</Text>
                    </TouchableOpacity>
                )}
            />
        );
    };

    const onChange = (value) => {
        try {
            let val = value.replace(/[^A-Za-z_ .]/gi, "");
            if (val === undefined || val === null)
                return
            setDocName(val);
        } catch (e) {
            recordError(e)
        }
    };


    return (
        <Container>
            <View style={styles.header}>
                <View style={styles.heading}>
                    <TouchableOpacity onPress={() => props.navigation.goBack()}>
                        <Icon
                            name="arrowleft"
                            type="AntDesign"
                            style={styles.headerBackIcon}
                        />
                    </TouchableOpacity>
                    <Text style={styles.headerText}>Find and Book</Text>
                </View>
                <View style={styles.headerSearch}>
                    <Input
                        value={docName}
                        placeholder="Find your doctor"
                        placeholderTextColor="#fff"
                        style={styles.searchBarText}
                        pattern={"[A-Za-z]"}
                        onChangeText={(name) => onChange(name)}
                    />
                    <Icon name="search" type="Feather" style={styles.headerSearchIcon} />
                </View>
            </View>
            {_doctors !== undefined && _doctors !== null && _doctors.length > 0 ? (
                <FlatList
                    scrollEnabled
                    keyExtractor={keyExtractor}
                    data={_doctors}
                    renderItem={(item) => renderItem(item)}
                />
            ) :
                docName !== undefined && docName !== null && docName.length > 0 && _doctors !== undefined && _doctors !== null && _doctors.length === 0 ?
                    (
                        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Image source={Images.notFound} style={{ width: 80, height: 85 }} />
                            <Text style={styles.notFound}>Search Failed!</Text>
                            <Text style={styles.notFoundDetails}>Try finding with better names to get the results</Text>
                        </View>
                    ) :
                    (
                        <View
                            style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Text>No results found</Text>
                        </View>
                    )
            }
        </Container>
    );
};
export default DoctorListing;
