import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import {FONT_FAMILY_SF_PRO_MEDIUM} from "../../../styles/typography";

export default (styles = StyleSheet.create({
	header: {
		height: hp("25%"),
		backgroundColor: "#1E2081",
		justifyContent: "center",
		paddingHorizontal: 8,
	},
	heading: {
		flexDirection: "row",
		marginLeft: 18,
		alignItems: "center",
		justifyContent: "flex-start",
	},
	headerText: {
		color: "#fff",
		fontSize: hp("2.9%"),
		marginLeft: wp("4%"),
	},
	headerBackIcon: {
		fontSize: hp("3.5%"),
		color: "#fff",
	},
	headerSearch: {
		height: hp("6%"),
		flexDirection: "row",
		alignItems: "center",
		paddingHorizontal: wp("4%"),
		marginTop: 20,
		marginHorizontal: "4%",
		backgroundColor: "rgba(255,255,255,0.2)",
		borderRadius: hp("1%"),
	},
	searchBarText: {
		color: "#ffffff",
		fontSize: hp("2%"),
	},
	headerSearchIcon: {
		fontSize: 24,
		marginRight: 10,
		color: "white",
	},
	resultsView: {
		marginTop: 19,
		marginLeft: 26,
	},
	resultsText: {
		color: "#041A32",
		fontSize: 17,
	},
	notFound: {
		color: "#041A32",
		fontSize: 18,
		marginBottom:13,
		marginTop:20,
		fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
	},
	notFoundDetails: {
		color: "#A0A9BE",
		fontSize: 14,
		fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
	},
	listView: { marginTop: 20 },
	listBtn: {
		borderRadius: 10,
		borderWidth: 2,
		backgroundColor: "#fff",
		width: 60,
		borderColor: "#C70315",
		justifyContent: "center",
		alignItems: "center",
		height: 30,
	},
	listBtnText: { color: "#041A32" },
}));
