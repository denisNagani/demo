import React, { useEffect, useState } from 'react'
import { FlatList, Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import { Text } from 'native-base'
import Images from '../../../assets/images'
import HeaderSearchBar from '../../../components/header-searchbar'
import PowerTranslator from '../../../components/PowerTranslator'
import { getSpecialitiesNames } from '../../../utils/helper'
import { ifNotValid, ifValid, isValid } from '../../../utils/ifNotValid'
import { strings } from '../../../utils/translation'
import Line from '../../../components/line'
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_SF_PRO_SemiBold, FONT_SIZE_13, FONT_SIZE_14, FONT_SIZE_15 } from '../../../styles/typography'
import { useDispatch, useSelector } from 'react-redux'
import { getSpecialityDoctors, setDoctorsBySpeciality } from '../../../actions/sehet/user-action'
import { setFollowUp } from '../upcoming-past-bookings/components/action'
import { selectedSpecialityObj } from '../../../actions/sehet/speciality-action'
import { getSkipped, setFromCheckout } from '../../../services/auth'
import { setIsBookingFlow1or2 } from '../book-appointment/action'
import { showModal } from '../../../actions/modal'
import { SetSpecialityDetails } from '../../../actions/profile'
import { CONSULT_FEE } from '../../../config'
import ViewProfileModal from '../../../components/view-profile-modal'
import { DOC_PROFILE } from '../../../utils/constant'
import { getDocList, getDocListByKeyword, getDocProfile } from "../../../actions/sehet/user-action";
import PriceFilterModal from '../../../components/PriceFilterModal'
import { filterListByName } from '../select-and-book/action'

const index = (props) => {
    const specialityData = props?.navigation?.state?.params?.specialityData
    const [doctorName, setDoctNameValue] = useState("");
    const [pageNo, setPageNo] = useState(1);
    const [pageNoForFilter, setPageNoForFilter] = useState(1);

    const [priceFilterData, setPriceFilterData] = useState([
        {
            id: 1,
            title: "Price : Less than 300",
            text: "300",
        },
        {
            id: 2,
            title: "Price : Between 300-500",
            text: "300-500",
        },
        {
            id: 3,
            title: "Price: More than 500",
            text: "500",
        },
    ]);

    const [priceModal, setPriceModal] = useState(false);
    const [modelProfile, setProfileModel] = useState(false);
    const [selectedPrice, setSelectedPrice] = useState('');

    const doctorBySpeciality = useSelector((state) => state.userReducer.doctorBySpeciality);
    const doctorBySpecialityTemp = useSelector((state) => state.userReducer.doctorBySpecialityTemp);
    const isLoading = useSelector(state => state.loader.isLoading)
    const dispatch = useDispatch()

    useEffect(() => {
        const { id } = specialityData
        dispatch(getSpecialityDoctors(id, 10, pageNo))
    }, [pageNo])

    useEffect(() => {
        if (isValid(selectedPrice)) {
            const { id } = specialityData
            dispatch(getSpecialityDoctors(id, 10, pageNoForFilter, selectedPrice))
        }
    }, [selectedPrice])

    useEffect(() => {

        if (isValid(doctorName)) {
            const searchedDocs = doctorBySpecialityTemp?.filter(itm => {
                let txt = doctorName?.trim()
                if (itm?.userId?.fullName?.includes(txt)) {
                    return itm
                }
            })
            dispatch(setDoctorsBySpeciality(searchedDocs, true))
        }
        if (doctorName?.length == 0) {
            dispatch(setDoctorsBySpeciality(doctorBySpecialityTemp, true))
        }
    }, [doctorName])

    const onChange = (value) => {
        try {
            let val = value.replace(/[^A-Za-z_ .]/gi, "");
            if (val === undefined || val === null)
                return
            setDoctNameValue(val);
        } catch (e) {
            console.log(e)
        }
    };

    const renderItem = ({ item }) => {

        let name = item?.userId?.fullName;
        if (ifNotValid(name)) name = "..."

        let hinFullName = item?.userId?.hindiFullName;
        if (ifNotValid(hinFullName)) hinFullName = name

        let speciality = ifValid(item.specialityId) ? getSpecialitiesNames(item.specialityId, 'name') : item.specialization;
        if (ifNotValid(speciality)) speciality = ""

        let hinSpeciality = ifValid(item.specialityId) ? getSpecialitiesNames(item.specialityId, 'hindiName') : item.specialization;
        if (ifNotValid(hinSpeciality)) hinSpeciality = speciality


        let gender = item?.userId?.gender;
        let imageUrl = item?.userId?.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = gender == "FEMALE" ? Images.female_doctor : Images.male_doctor
        else imageUrl = { uri: imageUrl }

        let experience = item.experience;
        if (ifNotValid(experience)) experience = "-"

        let docFee = isValid(item?.fee) ? item?.fee : CONSULT_FEE;

        let onlineFee = isValid(item?.fee) ? item?.fee : CONSULT_FEE;
        let offlineFee = isValid(item?.offlineFee) && item?.offlineFee != 0 ? item?.offlineFee : null;

        const handleBookNow = async (obj) => {
            let doc = {
                ...obj?.userId,
                fee: isValid(obj?.fee) ? obj?.fee : CONSULT_FEE
            }

            dispatch(SetSpecialityDetails(doc))

            const skipped = await getSkipped()
            const isSkipped = JSON.parse(skipped)
            if (isSkipped) {
                const payload = {
                    text: `${strings.please_login_to_continue}`,
                    subtext: "",
                    iconName: "closecircleo",
                    modalVisible: true,
                }
                setFromCheckout(true)
                props.navigation.navigate('PatientLogin')
                dispatch(showModal(payload))
            } else {
                props.navigation.navigate("BookAppointmentScreenFlow2", {
                    doc
                })
            }
        }

        const _viewProfile = (_id) => {
            dispatch(getDocProfile(_id))
            setProfileModel(!modelProfile);
        }

        return (
            <View style={{ backgroundColor: '#F9F9F9', paddingHorizontal: 14, }}>
                <View style={styles.cardContainer}>
                    <View style={{ flex: 2 }}>
                        <Image source={imageUrl} style={styles.cardImageStyle} />
                    </View>
                    <View style={{ flex: 8, marginLeft: 5 }}>
                        <PowerTranslator
                            hinText={hinFullName}
                            enText={name}
                            style={styles.cardTextStyleName}
                        />
                        <PowerTranslator
                            hinText={hinSpeciality}
                            enText={speciality}
                            style={styles.cardTextStyleSpeciality}
                        />
                        {/* <PowerTranslator
                            hinText={`₹ ${docFee}`}
                            enText={`₹ ${docFee}`}
                            style={styles.cardTextStyleSpeciality}
                        /> */}

                        <View style={{
                            backgroundColor: '#E8F5FF',
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginHorizontal: 3,
                            marginVertical: 5,
                            padding: 10,
                            alignSelf: 'flex-start'
                        }}>
                            <View style={{ flex: 1, }}>
                                <Text style={{
                                    color: '#586278',
                                    fontSize: 13
                                }}>Online : Rs {onlineFee}</Text>
                            </View>
                            {offlineFee != null && <View style={{}}>
                                <Text style={{
                                    color: '#586278',
                                    fontSize: 13
                                }}>Offline : Rs {offlineFee}</Text>
                            </View>}
                        </View>

                    </View>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.buttonStyle} onPress={() => _viewProfile(item?.userId?._id)}>
                        <Text uppercase={false} style={styles.buttonTxtStyle}>{strings.View_Profile}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => handleBookNow(item)}
                        style={[styles.buttonStyle, {
                            backgroundColor: '#FF5E38',
                            borderColor: '#FF5E38'
                        }]}>
                        <Text uppercase={false} style={[styles.buttonTxtStyle, { color: 'white' }]}>{strings.Book_Now}</Text>
                    </TouchableOpacity>
                </View>
                <Line style={{ marginTop: 8, borderBottomWidth: 0.2 }} lineColor="#A0A9BE" />
            </View >
        );
    };

    const loadMoreData = () => {
        if (doctorBySpeciality?.length > 4) {
            if (isValid(selectedPrice)) {
                setPageNoForFilter(pageNoForFilter + 1)
            } else {
                setPageNo(pageNo + 1)
            }
        }
    }


    return (
        <View style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
            <HeaderSearchBar
                navigation={props.navigation}
                headerText={specialityData?.name}
                inputTextValue={doctorName}
                inputTextPlaceholder={strings.searchByDoctorName}
                onChange={(value) => onChange(value)}
                showFilter
                showFilterCb={() => {
                    setPriceModal(true)
                }}
            />

            {doctorBySpeciality?.length > 0
                ? <FlatList
                    scrollEnabled
                    showsVerticalScrollIndicator={false}
                    keyExtractor={(item, index) => index.toString()}
                    data={doctorBySpeciality}
                    renderItem={(item) => renderItem(item)}
                    onEndReached={loadMoreData}
                    onEndReachedThreshold={0.7}
                />
                : isLoading == false && <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontSize: 18, fontWeight: '400', textAlign: 'center', marginBottom: 50 }}>No Doctors found</Text>
                </View>
            }

            <ViewProfileModal
                onTouchOutside={() => {
                    dispatch({ type: DOC_PROFILE, payload: {} });
                    setProfileModel(!modelProfile)
                }}
                onHardwareBackPress={() => {
                    dispatch({ type: DOC_PROFILE, payload: {} });
                    setProfileModel(!modelProfile)
                }}
                specialization={null}
                name={undefined}
                experience={undefined}
                imageUrl={""}
                visible={modelProfile} />

            <PriceFilterModal
                visible={priceModal}
                data={priceFilterData}
                closeModal={() => setPriceModal(false)}
                onItemPressCb={(item) => {
                    console.log('cb called ', item);
                    const updateFilters = priceFilterData?.map(filterI => {
                        return {
                            ...filterI,
                            selected: filterI.id == item?.id ? true : false
                        }
                    })
                    setPriceFilterData(updateFilters)
                    setSelectedPrice(item?.text)
                    setPriceModal(false)
                }}
            />
        </View>
    )
}

export default index

const styles = StyleSheet.create({

    notFound: {
        color: "#041A32",
        fontSize: 18,
        marginBottom: 13,
        marginTop: 20,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    notFoundDetails: {
        color: "#A0A9BE",
        fontSize: 14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    cardTextStyleName: {
        marginHorizontal: 10,
        width: '100%',
        fontSize: FONT_SIZE_15,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: '#484F5F'
    },
    cardTextStyleSpeciality: {
        marginHorizontal: 10,
        width: '100%',
        fontSize: FONT_SIZE_13,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        color: '#A0A9BE'
    },
    cardImageStyle: {
        width: 60,
        height: 60,
        borderRadius: 60 / 2,
    },
    cardContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 10,
        padding: 15,
    },
    buttonContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    buttonStyle: {
        borderRadius: 8,
        borderColor: '#00A2FF',
        padding: 7,
        borderWidth: 1,
        flex: 1,
        marginTop: 5,
        marginBottom: 8,
        marginHorizontal: 5
    },
    buttonTxtStyle: {
        color: '#00A2FF',
        padding: 0,
        margin: 0,
        textAlign: 'center',
        fontSize: FONT_SIZE_15
    },
    radioTextStyle: {
        marginLeft: 19,
        color: '#041A32',
        fontSize: FONT_SIZE_13
    },
    bottomContainer: {
        height: 50,
        borderRadius: 8,
        marginVertical: 10,
        marginHorizontal: 23,
        backgroundColor: '#E5F3FE',
        flexDirection: 'row',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    filter: {
        color: '#244262',
        fontSize: FONT_SIZE_15,
        textAlign: 'center',
        // fontFamily: FONT_FAMILY_BALOO_SEMI_BOLD
    },
    sortStyle: {
        color: '#244262',
        fontSize: FONT_SIZE_15,
        textAlign: 'center',
        // fontFamily: FONT_FAMILY_BALOO_SEMI_BOLD
    },
    lineStyle: {
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderLeftWidth: 0.5,
        borderLeftColor: '#244262',
    },
    buttonView: {
        // flex: 1,
        height: 40,
        marginLeft: 0,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginRight: 0.1,
        backgroundColor: '#E5F3FE',
        borderRadius: 5
    },
    searchTextStyle: {
        paddingHorizontal: 20,
        paddingTop: 10,
        backgroundColor: '#F9F9F9',
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold,
    }

});