import React, { Component } from "react";
import { Image, SafeAreaView, TouchableOpacity } from "react-native";
import { Button, Container, Content, Footer, Form, Icon, Input, Item, Label, Picker, Text, View, } from "native-base";
import styles from "./style";
import Images from "../../../assets/images/index";
import { launchImageLibrary } from "react-native-image-picker";
import { accessToken } from "../../../services/auth";
import moment from "moment";
import validate from "../../../utils/validation_wrapper";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getUserDetails, image_Upload, updateProfilePatient } from "../../../actions/profile";
import { showModal } from "../../../actions/modal";
import { methodLog, recordError, successLog } from "../../../utils/fireLog";
import { ifValid, isValid as isValidString } from "../../../utils/ifNotValid";
import { strings, translateToHindi } from "../../../utils/translation";
import common from "../../../styles/common";
import DatePickerModal from "../../../components/datePickerModal";
import { requestPermissionStorage } from "../../../services/requestPermissions";

class EditPatientProfile extends Component {
    constructor(props) {
        super(props);

        this.fromcard = ifValid(props.navigation?.state?.params?.fromCard) ? props.navigation.state.params.fromCard : false

        const user = ifValid(this.props.userProfile) ? ifValid(this.props.userProfile.userId)
            ? this.props.userProfile.userId : {} : {};
        this.state = {
            photo: null,
            fullName: user.fullName,
            email: user.email,
            imageUrl: user.imageUrl,
            dob: isValidString(user.dob) ? user.dob : '',
            gender: user.gender,
            city: user?.city,
            mobileNo: ifValid(user.mobileNo) ? user.mobileNo : "-",
            fullNameError: null,
            mobileNoError: null,
            cityError: null,
            isDatePickerVisible: false,
            response: ''
        };
    }

    componentDidMount() {
        successLog("EditPatientProfile screen loaded")
    }

    toggleDatePicker = () => {
        this.setState({ isDatePickerVisible: !this.state.isDatePickerVisible })
    };

    handleConfirm = (date) => {
        methodLog("handleConfirm")
        const dob = moment(date).format("YYYY-MM-DD");
        this.setState({
            dob,
            isDatePickerVisible: !this.state.isDatePickerVisible,
        }, () => {
            this.setState({ dob: dob })
        });
    };

    isValid = () => {
        const fullNameError = validate("fullName", this.state.fullName);
        // const mobileNoError = validate("mobileNo", this.state.mobileNo);
        this.setState(fullNameError);
        return !fullNameError;
    };


    createFormData = (photo) => {
        methodLog("createFormData")
        let imgName = "";
        try {
            if (photo.fileName === undefined) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else if (photo.fileName === null) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else {
                imgName = photo.fileName;
            }
            const data = new FormData();
            data.append("file", {
                name: Platform.OS === 'android' ? photo.fileName : imgName,
                type: photo.type,
                uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
            });
            return data;
        } catch (e) {
            recordError(e)
        }
    };

    handleUploadPhoto = async (photo, payload) => {
        methodLog("handleUploadPhoto")
        let token = await accessToken().then((token) => {
            return token;
        });
        this.props.image_Upload(this.createFormData(photo), token, payload, this.props.navigation);
    };

    handleChoosePhoto = () => {
        methodLog("handleChoosePhoto")
        try {
            requestPermissionStorage()
                .then((status) => {
                    if (status == true) {
                        const options = {
                            noData: true,
                        };
                        launchImageLibrary(options, (results) => {
                            if (results?.assets?.length > 0) {
                                let response = {
                                    uri: results?.assets[0]?.uri,
                                    fileName: results?.assets[0]?.fileName,
                                    fileSize: results?.assets[0]?.fileSize,
                                    type: results?.assets[0]?.type,
                                }
                                if (response.uri) {
                                    this.setState({ photo: response });
                                    this.setState({
                                        imageUrl: response.uri,
                                    });
                                    this.state.response = response
                                }
                            }
                        });
                    }
                })

        } catch (e) {
            recordError(e)
        }
    };

    _onChangeValue(key, value) {
        this.setState({ [key]: value });
    }


    async updateProfile() {
        methodLog("updateProfile")
        try {
            if (this.state.gender === undefined && this.state.mobileNo === "-") {
                this.props.navigation.goBack();
            } else {
                if (this.isValid()) {
                    const { fullName, dob, email, mobileNo, gender, city } = this.state;
                    const hindiFullName = await translateToHindi(fullName)
                    let payload = {
                        fullName: fullName,
                        hindiFullName: hindiFullName,
                        dob: isValidString(dob) ? moment(dob).format("YYYY-MM-DD") : "",
                        email: email,
                        mobileNo: mobileNo,
                        gender: gender,
                        city: city,
                    };
                    if (this.state.response === '') {
                        this.props.updateProfilePatient(payload, this.props.navigation, this.fromcard)
                    } else {
                        this.handleUploadPhoto(this.state.response, payload)
                    }
                }
            }
        } catch (e) {
            recordError(e)
        }
    }

    render() {
        const { photo } = this.state;
        return (
            <Container>
                <SafeAreaView style={[styles.header, { backgroundColor: '#141E46' }]}>
                    <TouchableOpacity
                        transparent
                        onPress={() => this.props.navigation.goBack()}
                    >
                        <Icon
                            name="arrowleft"
                            type="AntDesign"
                            style={[styles.headerBackIcon, { color: '#FFF' }]}
                        />
                    </TouchableOpacity>
                </SafeAreaView>

                <Content>
                    <View style={[styles.inHeader, { backgroundColor: '#141E46' }]}>
                        <View style={styles.section1}>
                            <View>
                                <Image
                                    source={
                                        this.state.imageUrl !== "" && this.state.imageUrl !== null
                                            ? { uri: this.state.imageUrl }
                                            : Images.user
                                    }
                                    style={styles.userImage}
                                />
                                <View
                                    style={styles.cameraBtn}
                                    onTouchStart={() => this.handleChoosePhoto()}>
                                    <Image source={Images.camera} />
                                </View>
                            </View>
                        </View>
                        <View style={styles.section2}>
                            <Text style={[styles.title, { color: '#FFF' }]}>{this.state.fullName}</Text>
                            <Text
                                style={[common.subTitleProfile, { color: '#FFF' }]}>{ifValid(this.state.email) ? this.state.email : ''}</Text>

                        </View>
                    </View>

                    <Form style={styles.formItem}>
                        <Item
                            error={this.state.fullNameError !== null}
                            floatingLabel
                            style={[styles.fullName, { marginBottom: 0 }]}>
                            <Label>{strings.full_name}</Label>
                            <Input
                                style={styles.marginItem}
                                value={this.state.fullName}
                                onChangeText={(value) => this._onChangeValue("fullName", value)}
                            />
                        </Item>

                        <Item
                            error={this.state.fullNameError !== null}
                            floatingLabel
                            style={styles.fullName}>
                            <Label>{strings.email}</Label>
                            <Input
                                style={styles.marginItem}
                                value={this.state.email}
                                onChangeText={(value) => this._onChangeValue("email", value)}
                            />
                        </Item>

                        <Item picker style={styles.genderPicker}>
                            <Label style={{ flex: 0.8 }}>{strings.gender}</Label>
                            <Picker
                                mode="dropdown"
                                placeholder="select"
                                style={{ flex: 2, }}
                                selectedValue={this.state.gender}
                                onValueChange={(value) => {
                                    if (value != "0")
                                        this._onChangeValue("gender", value)
                                }}
                            >
                                <Picker.Item label="Select" value="0" />
                                <Picker.Item label="Male" value="MALE" />
                                <Picker.Item label="Female" value="FEMALE" />
                                <Picker.Item label="Prefer Not to Say" value="Prefer Not to Say" />
                                <Picker.Item label="Not Applicable" value="NOT APPLICABLE" />
                            </Picker>
                        </Item>


                        <Item
                            style={[styles.datePicker, { marginBottom: 0 }]}
                            floatingLabel
                            onPress={this.toggleDatePicker}>
                            <Label>{strings.dob}</Label>
                            <Input
                                disabled
                                style={styles.marginItem}
                                value={
                                    ifValid(this.state.dob) && this.state.dob != ""
                                        ? moment(new Date(this.state.dob)).format("DD MMMM YYYY")
                                        : ""
                                }
                            />
                            <Icon
                                name="calendar"
                                type="Feather"
                                style={styles.calIcon} />
                        </Item>

                        <DatePickerModal
                            isVisible={this.state.isDatePickerVisible}
                            onConfirm={(date) => this.handleConfirm(date)}
                            onCancel={() => this.toggleDatePicker()}
                            maximumDate={new Date()}
                        />

                        <Item
                            error={this.state.mobileNoError !== null}
                            floatingLabel>
                            <Label>{strings.Mobile_no}</Label>
                            <Input
                                style={styles.marginItem}
                                maxLength={10}
                                value={this.state.mobileNo}
                                onChangeText={(value) => this._onChangeValue("mobileNo", value)}
                                keyboardType="numeric"
                            />
                        </Item>

                        <Item
                            error={this.state.cityError !== null}
                            floatingLabel
                            style={styles.fullName}>
                            <Label>{strings.city}</Label>
                            <Input
                                style={styles.marginItem}
                                value={this.state.city}
                                onChangeText={(value) => this._onChangeValue("city", value)}
                            />
                        </Item>


                    </Form>
                </Content>

                <Footer style={styles.footer}>
                    <Button
                        style={[styles.footerBtn, styles.cancelBtn]}
                        onPress={() => {
                            this.props.navigation.goBack();
                        }}
                    >
                        <Text style={styles.footerBtnText} uppercase={false}>
                            {strings.cancel}
                        </Text>
                    </Button>
                    <Button
                        onPress={() => this.updateProfile()}
                        danger
                        style={[styles.footerBtn, { backgroundColor: '#FF5E38' }]}
                    >
                        <Text style={styles.footerBtnText} uppercase={false}>
                            {strings.update}
                        </Text>
                    </Button>
                </Footer>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({ getUserDetails, image_Upload, showModal, updateProfilePatient }, dispatch);

const mapStateToProps = (state) => ({
    userProfile: state.profile.userProfile,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(EditPatientProfile);
