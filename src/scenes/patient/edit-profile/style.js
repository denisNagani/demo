import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import React from "react";

export default (styles = StyleSheet.create({
    header: {
        backgroundColor: "#FFFFFF",
        paddingBottom: 10
        // flex: 0.09,
    },
    inHeader: {
        flexDirection: "row",
        backgroundColor: "#FFFFFF",
        height: hp("13%"),
        paddingLeft: wp("5%"),
        alignItems: "center",
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 5
    },
    headerBackIcon: {
        fontSize: hp("3.5%"),
        color: "#041A32",
        marginTop: wp("5%"),
        marginLeft: wp("7%"),
    },
    headerTitle: {
        fontSize: 17,
        fontWeight: "bold",
    },
    headerView: {
        flexDirection: "row",
        justifyContent: "space-between",
    },
    section1: {
        flex: 0.2,
    },
    section2: {
        flex: 0.65,
    },
    section3: {
        flex: 0.2,
    },

    formGroup: {
        flex: 1,
        flexDirection: "row",
        padding: "2%",
    },

    label: {
        color: "gray",
    },
    fullName: {
        marginBottom: "8%",
    },
    email: {
        marginBottom: "8%",
    },
    datePicker: {
        marginLeft: "3%",
        marginBottom: "8%",
    },
    marginItem: {
        marginVertical: 4
    },
    boxStyleAmount: {
        flexDirection: 'row',
        marginTop: 6,
        paddingRight: 8,
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    genderPicker: {
        // flex: 1,
        marginLeft: "3%",
        marginBottom: "5%",
    },
    mobile: {
        flex: 1,
    },
    password: {
        flex: 1,
    },
    btnText: {
        color: "#fff",
        fontSize: 20,
    },
    formItem: {
        marginTop: hp("5%"),
        paddingHorizontal: wp("2%"),
    },

    footerBtn: {
        flex: 0.5,
        margin: 10,
        justifyContent: "center",
        fontSize: 20,
    },
    cancelBtn: {
        backgroundColor: "#A0A9BE",
    },
    footer: {
        height: 70,
        backgroundColor: "#fff",
    },
    footerBtnText: {
        color: "#fff",
        fontSize: 15,
        fontWeight: "bold",
    },
    title: {
        fontSize: 18,
        color: "#041A32",
    },
    subTitle: {
        fontSize: 12,
        color: "#041A32",
    },

    userImage: {
        width: 60,
        height: 60,
        borderRadius: 50,
    },
    editIcon: {
        color: "#fff",
        fontSize: 30,
    },
    cameraBtn: {
        position: "absolute",
        bottom: 0,
        right: 10,
    },
}));
