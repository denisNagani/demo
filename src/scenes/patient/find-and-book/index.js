import React, { useEffect, useState } from "react";
import { Container, Text } from "native-base";
import { FlatList, Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { showModal } from "../../../actions/modal";
import Images from "../../../assets/images";
import { methodLog, recordError, successLog } from "../../../utils/fireLog";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_13, FONT_SIZE_14, FONT_SIZE_15 } from "../../../styles/typography";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { ifNotValid } from "../../../utils/ifNotValid";
import { selectedSpecialityObj } from "../../../actions/sehet/speciality-action";
import HeaderSearchBar from "../../../components/header-searchbar";
import { getSkipped, setFromCheckout } from "../../../services/auth";
import { setIsBookingFlow1or2 } from "../book-appointment/action";
import { strings, translateToHindi, TranslationConfig } from "../../../utils/translation";
import PowerTranslator from "../../../components/PowerTranslator";
import { setFollowUp } from "../upcoming-past-bookings/components/action";
import { setDoctorsBySpeciality } from "../../../actions/sehet/user-action";

const FindAndBook = (props) => {
    const specialities = useSelector((state) => state.specialitiesReducer.hospitals)
    const lang = useSelector(state => state.profile.selected_language)

    const [_doctors, setSpeciality] = useState([]);
    const [specialitiesName, setSpecialityName] = useState("");
    const dispatch = useDispatch();
    // TranslationConfig(lang)


    // if (lang == "hin") {
    //     specialities.map(item => {
    //         return translateToHindi(item.name)
    //             .then(res => {
    //                 item.name = res
    //             })
    //     })
    // }

    const showCallClinic = () => {
        methodLog("showCallClinic")
        const payload = {
            text: "Cannot book appointment for this doctor from app, please call clinic to book an appointment",
            iconName: "checkcircleo",
            isIcon: false,
            modalVisible: true,
            button: true,
        };
        dispatch(showModal(payload));
    };

    useEffect(() => {
        successLog("Doctor listing screen loaded")
        setSpeciality(specialities)
    }, [specialities]);

    useEffect(() => {
        try {
            if (specialities !== undefined && specialities !== null && specialities.length > 0) {

                if (specialitiesName.length > 0) {

                    let searchName = specialitiesName;

                    let result = specialities.filter((item) => {
                        let name = item.name.toLowerCase();
                        let specialization = item.specialization !== undefined && item.specialization !== null ? item.specialization.toLowerCase() : "";
                        return (
                            name.includes(searchName.toLowerCase())
                            // specialization.includes(searchName.toLowerCase())
                        );
                    });
                    setSpeciality(result);
                } else setSpeciality(specialities);
            }
        } catch (e) {
            recordError(e)
        }
    }, [specialitiesName]);

    const keyExtractor = (item, index) => index.toString();

    const onSpecialityClick = async (obj) => {
        dispatch(setDoctorsBySpeciality([]))
        props.navigation.navigate('DoctorsBySpeciality', {
            specialityData: {
                name: obj?.name,
                id: obj?._id
            }
        })
        // return
        // dispatch(setFollowUp(null))
        // dispatch(selectedSpecialityObj({ ...obj, m_price: obj.price, dateTime: new Date() }))
        // const skipped = await getSkipped()
        // const isSkipped = JSON.parse(skipped)
        // if (isSkipped) {
        //     setFromCheckout(true)
        //     dispatch(setIsBookingFlow1or2(1))
        //     const payload = {
        //         text: `${strings.please_login_to_continue}`,
        //         subtext: "",
        //         iconName: "closecircleo",
        //         modalVisible: true,
        //     }
        //     dispatch(showModal(payload))
        //     props.navigation.navigate('PatientLogin')
        // } else {
        //     props.navigation.navigate("BookAppointmentScreen", {
        //         doc: {
        //             fullName: obj.name,
        //             _id: obj._id,
        //         }
        //     })
        // }
    }

    const renderItem = ({ item }) => {

        let price = item.price;
        if (price === undefined || price === null) {
            price = "..."
        }
        let healthProblem = item.name;
        if (healthProblem === undefined || healthProblem === null) {
            healthProblem = "..."
        }

        let hinhealthProblem = item.hindiName;
        if (hinhealthProblem === undefined || hinhealthProblem === null) {
            hinhealthProblem = healthProblem
        }

        let imageUrl = item.imageUrl;
        if (ifNotValid(imageUrl) || imageUrl === "") imageUrl = Images.amar_logo
        else imageUrl = { uri: imageUrl }

        return (
            <TouchableOpacity style={styles.cardContainer} onPress={() => onSpecialityClick(item)}>
                <Image source={imageUrl} style={styles.healthProblemImageStyle} />
                <View style={{ flex: 1, paddingLeft: 10, marginVertical: 10 }}>
                    {/* <Text numberOfLines={2} style={styles.healthProblemStyle}>{healthProblem}</Text> */}
                    {/* <PowerTranslator style={styles.healthProblemStyle} text={healthProblem} /> */}
                    <PowerTranslator
                        style={styles.healthProblemStyle}
                        hinText={hinhealthProblem}
                        enText={healthProblem}
                    />
                    {/* <Text style={styles.healthProblemPriceStyle}>{`₹${price}`}</Text> */}
                </View>
            </TouchableOpacity>
        );
    };

    const onChange = (value) => {
        try {
            // let val = value.replace(/[^A-Za-z_ .]/gi, "");
            // if (val === undefined || val === null)
            //     return
            setSpecialityName(value);
        } catch (e) {
            recordError(e)
        }
    };


    return (
        <Container style={{ backgroundColor: '#F9F9F9' }}>
            <HeaderSearchBar
                navigation={props.navigation}
                headerText={strings.find_and_book}
                inputTextValue={specialitiesName}
                inputTextPlaceholder={strings.searchByHospitalName}
                onChange={(value) => onChange(value)} />

            {_doctors !== undefined && _doctors !== null && _doctors.length > 0 ? (
                <View style={{
                    justifyContent: 'center',
                    marginTop: 10,
                    flex: 1,
                    marginHorizontal: 10,

                }}>
                    <FlatList
                        scrollEnabled
                        showsVerticalScrollIndicator={false}
                        keyExtractor={keyExtractor}
                        numColumns={2}
                        data={_doctors}
                        renderItem={(item) => renderItem(item)}
                    />
                </View>
            ) :
                specialitiesName !== undefined && specialitiesName !== null && specialitiesName.length > 0 && _doctors !== undefined && _doctors !== null && _doctors.length === 0 ?
                    (
                        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Image source={Images.notFound} style={{ width: 80, height: 85 }} />
                            <Text style={styles.notFound}>Search Failed!</Text>
                            <Text style={styles.notFoundDetails}>Try different words to get better results</Text>
                        </View>
                    ) :
                    (
                        <View
                            style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Text>No results found</Text>
                        </View>
                    )
            }
        </Container>
    );
};
export default FindAndBook;

const styles = StyleSheet.create({
    header: {
        height: hp("25%"),
        backgroundColor: "#1E2081",
        justifyContent: "center",
        paddingHorizontal: 8,
    },
    heading: {
        flexDirection: "row",
        marginLeft: 18,
        alignItems: "center",
        justifyContent: "flex-start",
    },
    headerText: {
        color: "#fff",
        fontSize: hp("2.9%"),
        marginLeft: wp("4%"),
    },
    headerBackIcon: {
        fontSize: hp("3.5%"),
        color: "#fff",
    },
    headerSearch: {
        height: hp("6.5%"),
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: wp("4%"),
        marginTop: 20,
        opacity: 0.80,
        marginHorizontal: "4%",
        backgroundColor: "rgba(255,255,255,0.2)",
        borderRadius: hp("0.8%"),
    },
    searchBarText: {
        color: "#ffffff",
        fontSize: FONT_SIZE_14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    headerSearchIcon: {
        fontSize: 24,
        marginRight: 10,
        color: "white",
    },
    resultsView: {
        marginTop: 19,
        marginLeft: 26,
    },
    resultsText: {
        color: "#041A32",
        fontSize: 17,
    },
    notFound: {
        color: "#041A32",
        fontSize: 18,
        marginBottom: 13,
        marginTop: 20,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    notFoundDetails: {
        color: "#A0A9BE",
        fontSize: 14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    listView: { marginTop: 20 },
    listBtn: {
        borderRadius: 10,
        borderWidth: 2,
        backgroundColor: "#fff",
        width: 60,
        borderColor: "#C70315",
        justifyContent: "center",
        alignItems: "center",
        height: 30,
    },
    listBtnText: { color: "#041A32" },
    healthProblemStyle: { fontSize: FONT_SIZE_15, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, color: 'black' },
    healthProblemPriceStyle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_13,
        color: '#484F5F'
    },
    healthProblemImageStyle: { resizeMode: 'contain', width: 60, height: 60, },
    cardContainer: {
        width: '45%',
        // flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#A0A9BE1A',
        marginHorizontal: 10,
        marginTop: 10,
        padding: 10,
        marginBottom: 10,
        borderRadius: 10,
    },
});

