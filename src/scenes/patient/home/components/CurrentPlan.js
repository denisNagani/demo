import React, { useState } from 'react'
import { View, Text, FlatList, StyleSheet, ImageBackground, Image, Dimensions, ScrollView, TouchableOpacity } from 'react-native'
import moment from 'moment'
import { isValid } from '../../../../utils/ifNotValid'
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_SF_PRO_SemiBold, FONT_SIZE_13, FONT_SIZE_15 } from '../../../../styles/typography'
import { useDispatch, useSelector } from 'react-redux'
import Images from '../../../../assets/images'
import DoctorCard from './DoctorCard'
import { useNavigation } from '@react-navigation/native'
import PowerTranslator from '../../../../components/PowerTranslator'
import { ScrollView as GestureHandlerScrollView } from 'react-native-gesture-handler'
import Line from '../../../../components/line'
import { strings } from '../../../../utils/translation'
import { handleBookNow, setMembershipId } from '../../../../actions/appointment'
import { DOC_PROFILE, EMPTY_DOC_LIST } from '../../../../utils/constant'
import ViewProfileModal from '../../../../components/view-profile-modal'
import { getDocList, getDocProfile } from '../../../../actions/sehet/user-action'
import { Button } from 'native-base'
import { setSearchDocName } from '../../select-and-book/action'
import { getSkipped } from '../../../../services/auth'
import { getSpecialitiesNames } from '../../../../utils/helper'

const CurrentPlan = ({ item, index, navigation, planLength }) => {
    const dispatch = useDispatch()
    const [modelProfile, setProfileModel] = useState(false)
    const width = Dimensions.get('screen').width

    const dateFormat = 'DD/MM/YYYY'

    const cardBgImage = isValid(item?.plan?.backgroundUrl) ? { uri: item?.plan?.backgroundUrl } : Images.sehet_card_bg;
    const planImage = isValid(item?.plan?.imageUrl) ? { uri: item?.plan?.imageUrl } : Images.amar_logo;
    const planName = isValid(item?.plan?.name) ? item?.plan?.name : '-';
    const planKinds = isValid(item?.plan?.kinds) ? item?.plan?.kinds : '-';
    const planColor = isValid(item?.plan?.colorCode) ? item?.plan?.colorCode : '#000000';
    const subColorCode = isValid(item?.plan?.subColorCode) ? item?.plan?.subColorCode : '#E5F3FE';
    const startDate = moment(item?.buyingDate).format(dateFormat);
    const endDate = moment(item?.buyingDate).add(1, 'years').format(dateFormat);

    const compareDate = moment(new Date(), dateFormat);
    const sDate = moment(startDate, dateFormat);
    const eDate = moment(endDate, dateFormat);
    const isBetween = compareDate.isBetween(sDate, eDate)

    const cardStatus = isBetween ? 'Active' : 'Expried'
    const doctors = item?.plan?.doctorId

    const myHealthCard = useSelector(state => state.profile.myHealthCard)
    const uniqueId = myHealthCard?.uniqueId;

    const getDoctors = async () => {
        let skip = await getSkipped();
        if (skip == "true") {
            dispatch(getDocList("doctors"))
        } else {
            dispatch(getDocList("api/doctors"))
        }
    }

    const setUniqueId = () => {
        dispatch(setMembershipId(uniqueId))
    }


    return <View style={[styles.planContainer, { paddingHorizontal: 25, paddingBottom: 10, backgroundColor: subColorCode }]}>
        {/* header section */}

        <View style={styles.headerTextStyle}>
            <Text style={styles.sectionTitle}>{strings.Active_Plan_Doctors}</Text>
            <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 15 }}>
                <Text style={{ color: planColor }}>{`${strings.Plans} ${index + 1}/`}</Text>
                <Text style={{}}>{`${planLength}`}</Text>
            </View>
        </View>

        {/* card section */}

        <View style={{ flex: 1, marginVertical: 10 }}>
            <View style={[styles.card, { marginRight: 10 }]}>

                <ImageBackground
                    style={{ height: 200, width: width - 50, }}
                    imageStyle={{ borderRadius: 10 }}
                    resizeMode="cover"
                    source={cardBgImage}
                >
                    <Text style={[styles.kindText, { color: planColor }]}>{planKinds}</Text>

                    <View style={styles.con2}>
                        <View style={styles.con1}>
                            <Text style={styles.planName}>{planName}</Text>
                            <Image
                                source={planImage}
                                style={styles.planImage}
                            />
                        </View>
                    </View>

                </ImageBackground>

                <View style={[styles.activeStatusContainer, { backgroundColor: cardStatus === 'Active' ? '#FFFFFF' : '#382F2D' }]}>
                    <Text style={{ color: cardStatus === 'Active' ? planColor : '#FFFFFF', fontSize: 15 }}>{cardStatus}</Text>
                </View>

            </View>

        </View>

        {/* doctors section */}

        <View style={{ marginVertical: 20 }}>
            <GestureHandlerScrollView
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                nestedScrollEnabled={true}
                style={{ width: width - 50 }}
            >
                {
                    doctors?.length === 1
                        ? <View style={{ width: width - 50, backgroundColor: '#F9F9F9', paddingHorizontal: 14, }}>
                            <View style={styles.cardContainer}>
                                <View style={{ flex: 2 }}>
                                    <Image source={isValid(doctors[0]?.imageUrl) ? { uri: doctors[0]?.imageUrl } : Images.male_doctor} style={styles.cardImageStyle} />
                                </View>
                                <View style={{ flex: 8, marginLeft: 5 }}>
                                    <PowerTranslator
                                        hinText={isValid(doctors[0]?.hindiFullName) ? doctors[0]?.hindiFullName : '-'}
                                        enText={doctors[0]?.fullName}
                                        style={styles.cardTextStyleName}
                                    />
                                    <PowerTranslator
                                        hinText={getSpecialitiesNames(doctors[0]?.specialityId, "hindiName")}
                                        enText={getSpecialitiesNames(doctors[0]?.specialityId, "name")}
                                        style={styles.cardTextStyleSpeciality}
                                    />
                                </View>
                            </View>
                            <View style={styles.buttonContainer}>
                                <TouchableOpacity style={styles.buttonStyle} onPress={() => {

                                    dispatch(getDocProfile(doctors[0]?._id))
                                    setProfileModel(!modelProfile);
                                }}>
                                    <Text uppercase={false} style={styles.buttonTxtStyle}>{strings.View_Profile}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity onPress={() => {
                                    dispatch(handleBookNow(doctors[0]))
                                    setUniqueId()
                                }}
                                    style={[styles.buttonStyle, {
                                        backgroundColor: '#FF5E38',
                                        borderColor: '#FF5E38'
                                    }]}>
                                    <Text uppercase={false} style={[styles.buttonTxtStyle, { color: 'white' }]}>{strings.Book_Now}</Text>
                                </TouchableOpacity>
                            </View>
                            <Line style={{ marginTop: 8, borderBottomWidth: 0.2 }} lineColor="#A0A9BE" />
                        </View>
                        : doctors?.length === 0
                            ? <TouchableOpacity
                                style={{
                                    width: width - 50,
                                    backgroundColor: '#FBE7E3',
                                    borderRadius: 5,
                                    padding: 10,
                                }}
                                onPress={() => {
                                    navigation.navigate("SelectAndBook")
                                    dispatch(setSearchDocName(""))
                                    dispatch({ type: EMPTY_DOC_LIST })
                                    getDoctors()
                                }}>
                                <Text style={{
                                    color: '#FF5E38',
                                    textAlign: 'center'
                                }}>{strings.view_all}</Text>
                            </TouchableOpacity>
                            : <>
                                {doctors?.map((item, index) => {
                                    const imgUrl = isValid(item?.imageUrl) ? { uri: item?.imageUrl } : Images.male_doctor
                                    const hindiFullName = isValid(item?.hindiFullName) ? item?.hindiFullName : '-'
                                    const name = isValid(item?.fullName) ? item?.fullName : '-'

                                    const specialityName = isValid(item?.specialityId) ? getSpecialitiesNames(item?.specialityId, "name") : '-'
                                    const specialityNameHindi = isValid(item?.specialityId) ? getSpecialitiesNames(item?.specialityId, "hindiName") : '-'

                                    const onPressDoctor = (item) => {
                                        dispatch(handleBookNow(item))
                                        setUniqueId()
                                    }

                                    return (
                                        <TouchableOpacity
                                            key={index}
                                            style={styles.docCardContainer}
                                            onPress={() => onPressDoctor(item)}
                                        >

                                            <Image
                                                source={imgUrl}
                                                style={styles.docImageStyle}
                                            />
                                            <View>
                                                <PowerTranslator
                                                    style={{ fontSize: 14, textAlign: 'center', color: '#041A32', fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }}
                                                    numberOfLines={2}
                                                    hinText={hindiFullName}
                                                    enText={name}
                                                />
                                                <PowerTranslator
                                                    style={{ fontSize: 13, textAlign: 'center', color: '#7F8A96', marginVertical: 3, fontFamily: FONT_FAMILY_SF_PRO_REGULAR }}
                                                    numberOfLines={2}
                                                    hinText={specialityNameHindi}
                                                    enText={specialityName}
                                                />
                                            </View>

                                        </TouchableOpacity>
                                    )
                                })}
                            </>
                }
            </GestureHandlerScrollView>
        </View>

        <ViewProfileModal
            onTouchOutside={() => {
                dispatch({ type: DOC_PROFILE, payload: {} });
                setProfileModel(!modelProfile)
            }}
            onHardwareBackPress={() => {
                dispatch({ type: DOC_PROFILE, payload: {} });
                setProfileModel(!modelProfile)
            }}
            specialization={null}
            name={undefined}
            experience={undefined}
            imageUrl={""}
            visible={modelProfile} />

    </View >
}

export default CurrentPlan
const styles = StyleSheet.create({
    card: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        elevation: 3,
        shadowColor: 'grey',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
    },
    sectionTitle: {
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold,
        fontSize: 20,
        marginVertical: 15,
        marginHorizontal: 5,
    },
    planContainer: {
        flex: 1,
        justifyContent: 'space-between'
    },
    kindText: {
        backgroundColor: 'white',
        fontSize: 16,
        fontWeight: 'bold',
        width: '100%',
        textTransform: 'uppercase',
        textAlign: 'center',
        position: 'absolute',
        bottom: 20,
        paddingVertical: 3
    },
    con2: {
        position: 'absolute',
        width: '100%',
        bottom: 65,
    },
    con1: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginHorizontal: 25
    },
    planName: {
        fontSize: 18,
        fontFamily: FONT_FAMILY_SF_PRO_BOLD,
        color: '#FFF',
        marginTop: 10
    },
    planImage: {
        height: 50,
        width: 50,
    },
    docCardContainer: {
        width: 130,
        height: 170,
        backgroundColor: '#FFFFFF',
        padding: 10,
        marginVertical: 5,
        marginRight: 12,
        borderRadius: 8,
        elevation: 3,
        alignItems: 'center'
    },
    docImageStyle: {
        height: 80,
        width: 80,
        borderRadius: 80 / 2,
        marginVertical: 5
    },
    headerTextStyle: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    activeStatusContainer: {
        position: 'absolute',
        top: 0,
        right: 0,
        paddingHorizontal: 15,
        paddingVertical: 5,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10
    },




    notFound: {
        color: "#041A32",
        fontSize: 18,
        marginBottom: 13,
        marginTop: 20,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    notFoundDetails: {
        color: "#A0A9BE",
        fontSize: 14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    cardTextStyleName: {
        marginHorizontal: 10,
        width: '100%',
        fontSize: FONT_SIZE_15,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: '#484F5F'
    },
    cardTextStyleSpeciality: {
        marginHorizontal: 10,
        width: '100%',
        fontSize: FONT_SIZE_13,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        color: '#A0A9BE'
    },
    cardImageStyle: {
        width: 60,
        height: 60,
        borderRadius: 60 / 2,
    },
    cardContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 10,
        padding: 15,
    },
    buttonContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    buttonStyle: {
        borderRadius: 8,
        borderColor: '#00A2FF',
        padding: 7,
        borderWidth: 1,
        flex: 1,
        marginTop: 5,
        marginBottom: 8,
        marginHorizontal: 5
    },
    buttonTxtStyle: {
        color: '#00A2FF',
        padding: 0,
        margin: 0,
        textAlign: 'center',
        fontSize: FONT_SIZE_15
    },
    radioTextStyle: {
        marginLeft: 19,
        color: '#041A32',
        fontSize: FONT_SIZE_13
    },
    bottomContainer: {
        height: 50,
        borderRadius: 8,
        marginVertical: 10,
        marginHorizontal: 23,
        backgroundColor: '#E5F3FE',
        flexDirection: 'row',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    filter: {
        color: '#244262',
        fontSize: FONT_SIZE_15,
        textAlign: 'center',
        // fontFamily: FONT_FAMILY_BALOO_SEMI_BOLD
    },
    sortStyle: {
        color: '#244262',
        fontSize: FONT_SIZE_15,
        textAlign: 'center',
        // fontFamily: FONT_FAMILY_BALOO_SEMI_BOLD
    },
    lineStyle: {
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderLeftWidth: 0.5,
        borderLeftColor: '#244262',
    },
    buttonView: {
        // flex: 1,
        height: 40,
        marginLeft: 0,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginRight: 0.1,
        backgroundColor: '#E5F3FE',
        borderRadius: 5
    }

})