import { Text, TouchableOpacity, View, Image } from "react-native";
import React, { useEffect, useState } from "react";
import Images from "../../../../assets/images";
import { ifNotValid, ifValid, isValid } from "../../../../utils/ifNotValid";
import { getSpeciality, selectedSpecialityObj } from "../../../../actions/sehet/speciality-action";
import { useDispatch, useSelector } from "react-redux";
import { getDoctorsSlotByID } from "../../../../scenes/patient/select-and-book/action";
import { setSelectedDoctor } from "../../../../actions/appointment";
import { getSkipped, setFromCheckout } from "../../../../services/auth";
import { showModal } from "../../../../actions/modal";
import { FONT_FAMILY_SF_PRO_MEDIUM } from "../../../../styles/typography";
import { translateToHindi, TranslationConfig, strings } from "../../../../utils/translation";
// import PowerTranslator, { TranslatorConfiguration, ProviderTypes } from "react-native-power-translator";
import { CONSULT_FEE, translationApiKey } from "../../../../config";
import PowerTranslator from "../../../../components/PowerTranslator";
import { calculateDrConsultationFee } from "../../../../utils/helper";

const DoctorCard = ({ navigation, imageUrl, speciality, object, index, callBackFunc }) => {
    const lang = useSelector(state => state.profile.selected_language)
    // TranslationConfig(lang)
    // TranslatorConfiguration.setConfig(ProviderTypes.Google, translationApiKey, lang);

    // useEffect(() => {
    //     if (lang == "hin") {
    //         translateToHindi(object.fullName)
    //             .then(res => setFullName(res))
    //             .catch(err => {
    //                 console.log("google translate failed to convert " + JSON.stringify(err));
    //                 setFullName(object.fullName)
    //             })
    //     }
    // }, [])

    const [fullName, setFullName] = useState(object.fullName)
    const dispatch = useDispatch();
    let imgUrl = imageUrl;

    let gender = ifValid(object.gender) ? object.gender : ""
    if (ifNotValid(imgUrl) || imgUrl === "") imgUrl = gender == "FEMALE" ? Images.female_doctor : Images.male_doctor
    else imgUrl = { uri: imgUrl }

    let _hindiFullName = ifValid(object) ? object.hindiFullName : ''
    if (ifNotValid(_hindiFullName)) _hindiFullName = fullName

    let _speciality = speciality
    if (_speciality === undefined || _speciality === null)
        _speciality = ""

    const _onSlotSelected = (item) => {
        if (ifNotValid(item) || ifNotValid(item.price))
            return;
        let m_price = parseInt(item.price)
        let _price = parseInt(item.price)
        let m_specialPrice = parseInt(item?.specialPrice)
        let percentage = (_price / 100) * m_specialPrice;
        _price = _price + percentage;
        dispatch(getDoctorsSlotByID(item._id))
        dispatch(selectedSpecialityObj({ ...item, price: _price, m_price: m_price, dateTime: new Date() }))
        navigation.navigate("SelectAndBook", { item: item })
    }

    const handleBookNow = async (doc) => {
        callBackFunc()
        console.log("passed doc ", doc);
        dispatch(setSelectedDoctor(doc))

        // const specialityArray = doc.specialityId;
        // const item = calculateDrConsultationFee(specialityArray)
        // if (ifNotValid(item) || ifNotValid(item.price))
        //     return;
        // let _price = parseInt(item.price)
        // let m_price = parseInt(item.price)
        // let m_specialPrice = parseInt(item?.specialPrice)
        // let percentage = (m_price / 100) * m_specialPrice;
        // m_price = m_price + percentage;
        // dispatch(selectedSpecialityObj({ ...item, price: m_price, m_price: m_price, dateTime: new Date() }))

        let m_price = isValid(doc?.doctorId?.fee) ? doc?.doctorId?.fee : CONSULT_FEE
        dispatch(selectedSpecialityObj({ ...doc, price: m_price, m_price: m_price, dateTime: new Date() }))

        const skipped = await getSkipped()
        const isSkipped = JSON.parse(skipped)
        if (isSkipped) {
            const payload = {
                text: `${strings.please_login_to_continue}`,
                subtext: "",
                iconName: "closecircleo",
                modalVisible: true,
            }
            setFromCheckout(true)
            navigation.navigate('PatientLogin')
            dispatch(showModal(payload))
        } else {
            navigation.navigate("BookAppointmentScreenFlow2", {
                doc
            })
        }

    }

    return <TouchableOpacity
        key={index}
        style={{
            flex: 1,
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: '#FFFFFF',
            padding: 10,
            marginVertical: 5,
            marginRight: index % 2 == 0 ? 10 : 0,
            borderRadius: 8,
            elevation: 3
        }}
        onPress={() => handleBookNow(object)}
    >
        <Image
            source={imgUrl}
            style={{
                height: 80,
                width: 80,
                borderRadius: 80 / 2,
                marginVertical: 5
            }}
        />
        <PowerTranslator
            style={{ fontSize: 16, width: '80%', textAlign: 'center', color: '#041A32', marginVertical: 10, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }}
            hinText={_hindiFullName}
            enText={fullName}
        />
        {/* <PowerTranslator
            text={object.fullName}
            style={{ fontSize: 16, width: '80%', textAlign: 'center', color: '#041A32', marginVertical: 10, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }} /> */}
    </TouchableOpacity>
}
export default DoctorCard;
