import React from 'react'
import { FlatList } from 'react-native'
import { useSelector } from 'react-redux'
import CurrentPlan from './CurrentPlan'

const PlanCard = ({ navigation, data }) => {

    return (
        <>
            <FlatList
                keyExtractor={(item, index) => index?.toString()}
                data={data}
                horizontal
                nestedScrollEnabled={true}
                style={{ paddingBottom: 15 }}
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, index }) => {
                    return <CurrentPlan item={item} index={index} navigation={navigation} planLength={data?.length} />
                }}
            />
        </>
    )
}

export default PlanCard