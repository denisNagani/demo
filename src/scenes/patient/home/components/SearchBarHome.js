import { Icon } from 'native-base'
import React from 'react'
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native'
import { useDispatch } from 'react-redux'
import { setMicModal } from '../../../../actions/sehet/speciality-action'
import { strings } from '../../../../utils/translation'

const SearchBarHome = ({ value, onChangeText, onSubmit, onFocus, onBlur }) => {
    const dispatch = useDispatch()
    return (
        <View style={styles.searchBArContainer}>
            <TextInput
                placeholder={strings.search_symptoms_like}
                style={styles.textInputStyle}
                value={value}
                onChangeText={onChangeText}
                onSubmitEditing={onSubmit}
                onFocus={onFocus}
                onBlur={onBlur}
            />
            <TouchableOpacity
                onPress={() => dispatch(setMicModal(true))}
            >
                <Icon
                    name="mic"
                    type='Feather'
                    style={styles.iconStyle}
                />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    searchBArContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 45,
        marginHorizontal: 20,
        borderRadius: 8,
        marginBottom: -20,
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 5
    },
    textInputStyle: {
        flexGrow: 1,
        borderRadius: 8,
        paddingLeft: 15
    },
    iconStyle: { marginHorizontal: 10 }
})

export default SearchBarHome