import React, { useEffect, useState } from "react";
import { Dimensions, Image, SafeAreaView, ScrollView, TouchableOpacity, View, Platform, Linking, Share, TextInput } from "react-native";
import { Container, Icon, Text, Button } from "native-base";

import styles from "./style";
import Images from "../../../assets/images";
import { useDispatch, useSelector } from "react-redux";
import { getNearbyDoctors, getQuickDoctor, getRefreshTokenHome, getUserDetails } from "../../../actions/profile";
import { getRefToken, getSkipped, setGetStarted, setAppLangugae, setFromCheckout, getFcmToken } from "../../../services/auth";
import { getUpcomingBookings } from "../../../actions/start-consultation";
import { successLog } from "../../../utils/fireLog";
import SpecialityCard from "../../../components/specity-card";
import common from "../../../styles/common";
import { getSpeciality, getTopSpeciality, selectedSpecialityObj, getCities, getHospitals } from "../../../actions/sehet/speciality-action";
import { ifValid, isValid } from "../../../utils/ifNotValid";
import { getOffers, getDocList, getFAQ, setKeywordSuggestions, getTopDocList } from "../../../actions/sehet/user-action";
import Carousel from "react-native-banner-carousel";
import AsyncStorage from "@react-native-community/async-storage";
import { FlatList } from "react-native-gesture-handler";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_SemiBold } from "../../../styles/typography";
import DoctorCard from "./components/DoctorCard";
import ApplyCouponModal from "../../../components/ApplyCouponModal";
import { setDoctorName } from "../../../actions/bookings";
import { setSearchDocName } from "../select-and-book/action";
import { strings, translateToHindi, getTranslatedText } from "../../../utils/translation";
import { setIsBookingFlow1or2 } from "../book-appointment/action";
import { showModal } from "../../../actions/modal";
import { setActiveTab } from "../CareForm/action";
import { getChatData } from "../Chat/action";
import ShareCommon from "../../../components/ShareCommon";
import RNCallKeepComponent from "../../../components/RNCallKeepComponent";
import AppStateComponent from "../../../components/AppStateComponent";
import { setFollowUp } from "../upcoming-past-bookings/components/action";
import { getAllMebershipPlans, getMyCardDetails } from "../../../actions/membership";
import SearchBarHome from "./components/SearchBarHome";
import PlanCard from "./components/PlanCard";
import { setMembershipId } from "../../../actions/appointment";
import MicModal from "../../../components/MicModal";
import moment from "moment";
import { symptomsKeywords } from "../../../services/data";
import { EMPTY_DOC_LIST, GET_DOC_LIST } from "../../../utils/constant";
import { askForMyCurrentLocation } from "../../../services/permission";
import firestore from '@react-native-firebase/firestore'

const HomeScreen = (props) => {
    const BannerWidth = Dimensions.get('window').width;
    const BannerHeight = 200;
    const dispatch = useDispatch();
    const [skipFlag, setSkipFlag] = useState(null)
    const [searchText, setSearchText] = useState('')
    const [keyboardFocus, setKeyboardFocus] = useState(false)
    const userProfile = useSelector((state) => state.profile.userProfile);
    const specialities = useSelector((state) => state.specialitiesReducer.specialities);
    const topDoctors = useSelector((state) => state.userReducer.docListTop);
    const offerslinks = useSelector((state) => state.userReducer.offerUrls);
    const lang = useSelector(state => state.profile.selected_language)
    const myHealthCard = useSelector(state => state.profile.myHealthCard)
    const membershipId = useSelector(state => state.appointment.membershipId)
    const all_plans = myHealthCard?.planDetails;
    const isChat = useSelector(state => state.chat.chatData?.chat)
    const micModal = useSelector(state => state.modal.micModal)
    const keywordSuggestions = useSelector(state => state.userReducer.keywordSuggestions)

    const offers = offerslinks.filter(function (el) {
        return el != null;
    })
    const keywordData = keyboardFocus && searchText?.length > 0 ? keywordSuggestions : []

    useEffect(() => {
        dispatch(getOffers())
        checkProfile();
        getTopDoctors()
        dispatch(getSpeciality())
        dispatch(getFAQ())
        dispatch(getCities())

        return () => {
        };
    }, []);

    useEffect(() => {
        // ask for permission
        askForMyCurrentLocation((cords) => {
            console.log('cords ', cords);
        })
    }, [])


    const checkProfile = async () => {
        let skip = await getSkipped();
        setSkipFlag(skip)
        if (skip === "false") {
            await dispatch(getUserDetails())
            await dispatch(getMyCardDetails())
            await dispatch(getChatData())
        }
    }

    const getDoctors = async () => {
        let skip = await getSkipped();
        if (skip == "true") {
            dispatch(getDocList(`doctors?page=${1}&itemsPerPage=${10}`))
        } else {
            dispatch(getDocList(`api/paginatedDoctors?page=${1}&itemsPerPage=${10}`))
            dispatch(getChatData())
        }
        console.log("tokkkn " + await getFcmToken());
    }

    const getTopDoctors = () => {
        dispatch(getTopDocList(`/topDoctors`))
    }


    const getUpcomingList = (fullName) => {
        dispatch(getUpcomingBookings(props.navigation, userProfile))
    };

    const onSpecialityClickFlow = async (obj) => {
        dispatch(setFollowUp(null))
        dispatch(selectedSpecialityObj({ ...obj, m_price: obj.price, dateTime: new Date() }))
        const skipped = await getSkipped()
        const isSkipped = JSON.parse(skipped)
        if (isSkipped) {
            setFromCheckout(true)
            dispatch(setIsBookingFlow1or2(1))
            const payload = {
                text: `${strings.please_login_to_continue}`,
                subtext: "",
                iconName: "closecircleo",
                modalVisible: true,
            }
            dispatch(showModal(payload))
            props.navigation.navigate('PatientLogin')
        } else {
            props.navigation.navigate("BookAppointmentScreen", {
                doc: {
                    fullName: obj.name,
                    _id: obj._id,
                }
            })
        }
    }

    const onPressImage = (type) => {
        try {
            if (type == "free") {
                const obj = specialities[0];
                if (ifValid(obj)) {
                    onSpecialityClickFlow(obj)
                }
            } else if (type === "mental_health") {
                const obj = specialities[1];
                if (ifValid(obj)) {
                    onSpecialityClickFlow(obj)
                }
            } else {

            }
        } catch (error) {
            console.log("error " + error);
        }
    }

    const renderOffers = (image, index, type) => {
        return (
            <TouchableOpacity
                style={{ height: BannerHeight, width: BannerWidth, marginVertical: 20 }}
                key={index}
                activeOpacity={1}
                onPress={() => onPressImage(type)}
            >
                <View style={{ marginHorizontal: 25 }}>
                    <Image
                        style={{ height: '100%', width: '100%', borderRadius: 8 }}
                        source={{ uri: image }}
                    // defaultSource={Images.logo_sehet}
                    />
                </View>
            </TouchableOpacity>
        );
    }

    const onSearchTextChange = (txt) => {
        setSearchText(txt)
        const suggestedKeywords = symptomsKeywords?.filter(word => {
            const listWord = word?.toLowerCase()
            const txtToMatch = txt?.toLowerCase()
            return listWord?.includes(txtToMatch)
        })
        dispatch(setKeywordSuggestions(suggestedKeywords))
    }

    const onSearchSubmit = (text) => {
        props.navigation.navigate("SelectAndBook", {
            searchText: text
        })
        dispatch(setSearchDocName(""))
        setSearchText('')
        dispatch({ type: EMPTY_DOC_LIST })
    }

    const onPressBellIcon = () => {
        props.navigation.navigate("Notification")
    }

    const setUniqueId = () => {
        if (isValid(membershipId)) {
            dispatch(setMembershipId(null))
        }
    }

    const onPressQuickCall = () => {
        let Dtime = moment().format('HH:mm')
        const body = {
            date: moment().format('YYYY-MM-DD'),
            time: `${Dtime}:00`,
        }
        dispatch(getQuickDoctor(body, props.navigation))
    }
    const onPressCallAmbulance = () => {
        firestore()
            .collection("support")
            .doc("call_ambulance")
            .get()
            .then((doc) => {
                let docData = doc.data()
                if (docData) {
                    let phoneNumber = docData?.phoneNumber
                    Linking.openURL(`tel:${phoneNumber}`)
                }
            })
            .catch(() => {
                console.log('error getting ambulance');
            })
    }

    const handleSkipFlow = async (callfunction) => {
        const skipped = await getSkipped()
        const isSkipped = JSON.parse(skipped)
        if (isSkipped) {
            const payload = {
                text: `${strings.please_login_to_continue}`,
                subtext: "",
                iconName: "closecircleo",
                modalVisible: true,
            }
            props.navigation.navigate('PatientLogin')
            dispatch(showModal(payload))
        } else {
            callfunction()
        }
    }

    return (
        <Container style={{
            backgroundColor: '#F9F9F9'
        }}>
            <View style={[styles.headerContainer, { height: Platform?.OS === 'android' ? 100 : 150, justifyContent: 'space-around', backgroundColor: '#041A32', paddingTop: 5 }]}>
                {skipFlag == "false" && <AppStateComponent />}
                <SafeAreaView style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                }}>
                    {
                        skipFlag == "false"
                            ? <TouchableOpacity
                                style={{ paddingHorizontal: 15 }}
                                onPress={() => props.navigation.openDrawer()}>
                                <Icon
                                    name="menu"
                                    type="Feather"
                                    style={styles.sideDrawerIcon}
                                />
                            </TouchableOpacity>
                            : <Icon
                                name="arrowleft"
                                type="AntDesign"
                                style={styles.sideDrawerIcon}
                                onPress={() => {
                                    AsyncStorage.clear()
                                    setGetStarted(true)
                                    setAppLangugae(lang)
                                    props.navigation.navigate('Land')
                                }}
                            />
                    }
                    <View style={common.headerBellAndText}>
                        <View style={{ marginLeft: -10, flexDirection: 'row' }}>
                            <Image
                                style={{ height: 40 }}
                                source={Images.home_logo}
                                resizeMode="contain"
                            />
                            {/* <Text style={[styles.headerText, { fontSize: 22 }]}>
                                Sehet
                            </Text> */}
                        </View>
                    </View>
                    {
                        skipFlag == "false"
                            ? <TouchableOpacity onPress={onPressQuickCall}>
                                <Icon
                                    name="phone-call"
                                    type="Feather"
                                    style={{
                                        height: 35,
                                        width: 35,
                                        resizeMode: 'contain',
                                        marginRight: 10,
                                        color: 'white'
                                    }}
                                />
                            </TouchableOpacity>
                            : null
                    }

                    {/* <ShareCommon /> */}

                </SafeAreaView>

                <SearchBarHome value={searchText} onChangeText={(txt) => onSearchTextChange(txt)} onSubmit={() => onSearchSubmit(searchText)} onFocus={() => setKeyboardFocus(true)} onBlur={() => setKeyboardFocus(false)} />
            </View>

            <View
                style={{ zIndex: 1, marginHorizontal: 30 }}
            >
                {keywordData?.map((item, index) => {
                    return <TouchableOpacity
                        key={index}
                        style={{ paddingHorizontal: 10, paddingVertical: 15, borderBottomWidth: 1, borderBottomColor: '#B6BECB' }}
                        onPress={() => onSearchSubmit(item)}
                    >
                        <Text style={{ fontSize: 16, color: '#000', }}>{item}</Text>
                    </TouchableOpacity>
                })}
            </View>

            <ScrollView style={{ flex: 1, backgroundColor: '#F9F9F9', marginTop: 10 }}>

                <Carousel
                    autoplay
                    autoplayTimeout={5000}
                    loop
                    index={0}
                    pageSize={BannerWidth}
                    // pageIndicatorStyle={{ marginBottom: 15 }}
                    showsPageIndicator={false}
                >

                    {offers.map((item, index) => renderOffers(item.imageUrl, index, item.type))}
                </Carousel>

                <>
                    {all_plans?.length > 0 && <PlanCard navigation={props.navigation} data={all_plans} />}
                </>

                <View style={{ flex: 1, marginHorizontal: 25, marginBottom: 15 }}>
                    <Text style={{ fontFamily: FONT_FAMILY_SF_PRO_SemiBold, fontSize: 20, marginHorizontal: 5, marginBottom: 10 }}>{strings.membership_plans}</Text>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => {
                            props.navigation.navigate("Membership")
                            dispatch(getAllMebershipPlans())
                        }}>
                        <Image
                            resizeMode="contain"
                            source={Images.amar_health_card}
                            style={{
                                height: BannerHeight,
                                width: '100%',
                                borderRadius: 8
                            }}
                        />
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, marginHorizontal: 25 }}>
                    <TouchableOpacity
                        activeOpacity={1}
                        onPress={() => {
                            props.navigation.navigate("FindAndBook")
                            dispatch(getHospitals())
                            setUniqueId()
                        }}>
                        <Image
                            resizeMode="contain"
                            source={Images.search_by_hospitals}
                            style={{
                                height: BannerHeight,
                                width: '100%',
                                borderRadius: 8,
                            }}
                        />
                    </TouchableOpacity>
                </View>

                <View style={{ flexDirection: 'column', marginHorizontal: 25 }}>
                    <Text style={styles.headingText}>{strings.find_doctors}</Text>

                    <FlatList
                        style={{ marginVertical: 10 }}
                        keyExtractor={(item, index) => index.toString()}
                        data={topDoctors}
                        horizontal={false}
                        numColumns={2}
                        renderItem={({ item, index }) => (
                            <DoctorCard
                                navigation={props.navigation}
                                object={item}
                                speciality={item.fullName}
                                imageUrl={item.imageUrl}
                                index={index}
                                callBackFunc={setUniqueId}
                            />
                        )}
                        ListEmptyComponent={<Text adjustsFontSizeToFit style={styles.noUpcomingApp}>{strings.doctors_not_found}</Text>}
                    />
                    {
                        topDoctors.length > 0 && <Button block style={{
                            backgroundColor: '#000000',
                            borderRadius: 30,
                            marginVertical: 10,
                            elevation: 0,
                            padding: 0,
                        }}
                            onPress={() => {
                                props.navigation.navigate("SelectAndBook", {
                                    show_nearby: false
                                })
                                dispatch({ type: EMPTY_DOC_LIST })
                                dispatch(setSearchDocName(""))
                                setUniqueId()
                            }}>
                            <Text style={{
                                color: '#FFF',
                            }}>{strings.view_all}</Text>
                        </Button>
                    }

                    {/* <Button block style={{
                        backgroundColor: '#FBE7E3',
                        borderRadius: 5,
                        marginVertical: 10,
                        elevation: 0
                    }}
                        onPress={() => {
                            props.navigation.navigate("SelectAndBook", {
                                show_nearby: true
                            })
                            dispatch({ type: EMPTY_DOC_LIST })
                            dispatch(setSearchDocName(""))
                            setUniqueId()
                        }}>
                        <Text style={{
                            color: '#FF5E38'
                        }}>{strings.nearby_doctors}</Text>
                    </Button> */}
                </View>

                <TouchableOpacity
                    style={{ flex: 1, marginHorizontal: 25, marginBottom: 25, marginTop: 10, }}
                    onPress={() => {
                        handleSkipFlow(() => props.navigation.navigate('LabTest'))
                    }}>
                    <Image
                        resizeMode="contain"
                        source={Images.lab_test}
                        style={{
                            height: 130,
                            width: '100%',
                            borderRadius: 8,
                        }}
                    />
                </TouchableOpacity>

                <TouchableOpacity
                    style={{ flex: 1, marginHorizontal: 25, marginBottom: 20 }}
                    onPress={() => {
                        handleSkipFlow(() => props.navigation.navigate('OrderMedicine'))
                    }}>
                    <Image
                        resizeMode="contain"
                        source={Images.order_medicine}
                        style={{
                            height: 250,
                            width: '100%',
                            borderRadius: 8,
                        }}
                    />
                </TouchableOpacity>

                <TouchableOpacity
                    style={{ flex: 1, marginHorizontal: 25, marginBottom: 20 }}
                    onPress={() => {
                        props.navigation.navigate("SelectAndBook", {
                            show_nearby: true
                        })
                        dispatch({ type: EMPTY_DOC_LIST })
                        dispatch(setSearchDocName(""))
                        setUniqueId()
                    }}
                >
                    <Image
                        resizeMode="cover"
                        source={Images.nearby_doc_banner}
                        style={{
                            height: 120,
                            width: '100%',
                            borderRadius: 8,
                            resizeMode: 'contain'
                        }}
                    />
                </TouchableOpacity>

                <View style={{ alignItems: 'center', marginVertical: 10, }}>
                    <Image
                        source={Images.powered_by}
                        style={{
                            height: 100,
                            width: 100,
                        }}
                    />
                </View>


            </ScrollView>

            {
                <View style={{ position: 'absolute', bottom: 10, right: 15, alignItems: 'center' }}>
                    {
                        (skipFlag == "false" && isChat == true) &&
                        <TouchableOpacity
                            onPress={() => props.navigation.navigate('PatientChat')}
                        >
                            <Image
                                source={Images.chat_new}
                                style={{ height: 55, width: 55, resizeMode: 'contain', marginBottom: 15 }}
                            />
                        </TouchableOpacity>
                    }
                    <TouchableOpacity
                        onPress={onPressCallAmbulance}
                    >
                        <Image
                            source={Images.Call_Ambulance}
                            style={{ height: 55, width: 55, resizeMode: 'contain' }}
                        />
                    </TouchableOpacity>
                </View>
            }


            <MicModal visible={micModal} />
        </Container>
    );
};
export default HomeScreen;
