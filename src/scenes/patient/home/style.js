import { StyleSheet, Platform } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_SIZE_12,
    FONT_SIZE_13,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_18,
    FONT_SIZE_16
} from "../../../styles/typography";

export default (styles = StyleSheet.create({
    f_14: {
        fontSize: FONT_SIZE_14,
    },
    f_12: {
        fontSize: FONT_SIZE_12,
    },

    container: {
        backgroundColor: "#fff",
        flex: 1,
    },

    headerContainer: {
        height: Platform.OS == 'ios' ? hp("12%") : hp("10%"),
        backgroundColor: "#FFFFFF",
        justifyContent: "center",
        alignItems: 'center',
        paddingHorizontal: wp("2%"),
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 3 },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 5
    },
    headerText: {
        color: "#fff",
        fontSize: FONT_SIZE_18,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        alignSelf: "center",
        marginLeft: wp("5%"),
    },
    header: {
        flexDirection: "row",
        marginHorizontal: wp("4%"),
        alignItems: 'center',
    },
    sideDrawerIcon: {
        fontSize: hp("3.5%"),
        marginTop: hp("0.2%"),
        color: "#fff",
    },

    searchBar: {
        height: hp("6%"),
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: wp("4%"),
        justifyContent: "space-between",
        marginTop: 20,
        marginHorizontal: "4%",
        backgroundColor: "rgba(255,255,255,0.2)",
        borderRadius: hp("1%"),
    },
    searchBarText: {
        color: "#ffffff",
        fontSize: hp("2%"),
    },
    searchBarIcon: {
        fontSize: 24,
        color: "#fff",
    },

    // Styles for index.js
    cardContainer: {
        marginVertical: hp("1%"),
        justifyContent: "space-around",
        width: "100%",
        flexDirection: "row",
    },
    card: {
        marginTop: hp("1%"),
        height: hp("28%"),
        width: wp("93%"),
    },
    cardInner: {
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
        paddingVertical: hp("2.2%"),
        paddingHorizontal: wp("4.3%"),
    },
    cardText: {
        marginBottom: hp("0.5%"),
        fontWeight: "bold",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
        color: "#1E2081",
    },
    cardIconColor: {
        marginBottom: hp("0.5%"),
        color: "#1B80F3",
        fontSize: hp("3%"),
    },
    headingView: {
        marginTop: hp("2%"),
        marginLeft: wp("5%"),
        flexDirection: "row",
        justifyContent: "space-between",
        // alignItems: "center",
        flexDirection: 'column',
        marginHorizontal: 25,
    },
    headingText: {
        color: "#041A32",
        fontSize: FONT_SIZE_16,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontWeight: 'bold',
        marginTop: 15,
    },
    seeAllBtn: {
        marginRight: wp("5%"),
        // flex: 2,
        // alignItems: 'flex-start'
    },
    seeAllBtnText: {
        fontSize: FONT_SIZE_13,
        color: "#FF5E38",
        marginHorizontal: 15
    },
    findDoctors: {
        height: hp("15%"),
        alignItems: "center",
        backgroundColor: "#f2f2f2",
        justifyContent: 'space-around',
        width: wp("28%"),
        margin: 5,
        alignSelf: "center",
        borderRadius: 5,
    },
    findListContainer: {
        flexDirection: 'row', justifyContent: 'space-around', margin: 15
    },

    upcomingTabAvatar: {
        backgroundColor: "grey",
        width: wp("13%"),
        height: wp("13%"),
        borderRadius: wp("13%"),
    },
    upcomingTabName: {
        color: "#fff",
        fontSize: FONT_SIZE_18,
    },
    upcomingTabView: {
        width: "60%",
    },
    noUpcomingApp: {
        textAlign: 'center',
        fontSize: FONT_SIZE_13,
    },
    upcomingTabDay: {
        color: "#FFFFFFBD",
        marginTop: 9,
        fontSize: FONT_SIZE_13,
    },
    upcomingTabTime: {
        width: '17%',
        color: "#FFFFFFBD",
        fontSize: FONT_SIZE_13,
        marginTop: hp("4.5%"),
    },
    bookingTab: {
        flexDirection: "row",
        justifyContent: "space-around",
    },
    bookingTabCard: {
        width: wp("40%"),
        borderRadius: wp("2%"),
    },
    bookingTabAvatar: {
        width: hp("5%"),
        height: hp("5%"),
    },
}));
