import { show, hide } from "../../../utils/loader/action"
import { errorLog } from "../../../utils/fireLog"
import auth from '@react-native-firebase/auth'
import { setOtpGoogleResponse, setOptUsing, sendOtpAction } from "../../../actions/sehet/user-action"
import { sendOTPAnalytics } from "../../../services"
import { ifEmpty, ifValid } from "../../../utils/ifNotValid"
import { showModal } from "../../../actions/modal"

// Handle the button press
export const signInWithPhoneNumber = async (phoneNumber, dispatch, navigation, countryCode) => {
    dispatch(show())
    try {
        let output = ''
        let preMobile = countryCode
        if (phoneNumber.length == 10) {
            output = phoneNumber?.trim();
        } else {
            return;
        }

        auth()
            .verifyPhoneNumber(`${preMobile} ${output}`)
            .then(confirmation => {
                dispatch(setOtpGoogleResponse(confirmation))
                dispatch(setOptUsing('firebase'))
                navigation.navigate("OTPScreen", {
                    mobileAndMedia: {
                        mobileNo: phoneNumber,
                        msgMode: "MOBILE",
                    }
                })
                dispatch(hide())
                sendOTPAnalytics("OPT_SUCCESS", phoneNumber, "sent", "OPT sent successfully")
            })
            .catch(error => {
                dispatch(hide())
                const msg = ifEmpty(error.code) ? JSON.stringify(error.code) : "firebase error";
                dispatch(sendOtpAction({
                    mobileNo: output,
                    msgMode: "MOBILE"
                }, dispatch, navigation, true))
                dispatch(setOptUsing('api'))
                sendOTPAnalytics("OPT_FAILED", phoneNumber, "failed", msg)
            });


    } catch (e) {
        dispatch(hide())
        const payload = {
            text: 'Something went wrong',
            subText: ifValid(e) ? e?.toString() : '',
            iconName: "closecircleo",
            modalVisible: true,
            iconColor: "#C70315",
        };
        dispatch(showModal(payload));
    }
}
