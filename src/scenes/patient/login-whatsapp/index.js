import React, { useEffect, useState } from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";
import { Button, Container, Content, Footer, Icon, Input, Item, Text } from "native-base";
import { useDispatch, useSelector } from "react-redux";
import { errorLog, successLog } from "../../../utils/fireLog";
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_16,
    FONT_SIZE_26
} from "../../../styles/typography";
import common from "../../../styles/common";
import { heightPercentageToDP as hp, } from "react-native-responsive-screen";
import { sendOtpAction, setOtpGoogleResponse, setOptUsing, checkIfUserDoctor } from "../../../actions/sehet/user-action";
import { setCountryModal, showModal } from "../../../actions/modal";
import { ifValid, ifEmpty } from "../../../utils/ifNotValid";
import { setSkipped } from "../../../services/auth";
import ImageContainer from "../../../components/ImageContainer";
import auth from '@react-native-firebase/auth'
import { show, hide } from "../../../utils/loader/action";
import { sendOTPAnalytics } from "../../../services/index";
import { strings } from "../../../utils/translation";
import { errorPayload } from "../../../utils/helper";
import CountryPickerModal from "../../../components/CountryPickerModal";
import { getCountries } from "../../../actions/login";


const LoginScreenWhatsapp = (props) => {
    const dispatch = useDispatch();
    const docName = useSelector((state) => state.bookings.docName);
    const docImg = useSelector((state) => state.bookings.docImage);
    const [mobile, setMobile] = useState("");
    const [smsWhatsapp, setSmsWhatsapp] = useState("sms");
    //phone login
    const [confirm, setConfirm] = useState(null);
    const [countries, setCountries] = useState([]);
    const [selectedCountry, setSelectedCountry] = useState("+91");
    const navigation = props.navigation;

    useEffect(() => {
        // return
        getCountries()
            .then((data) => {
                console.log(JSON.stringify(data))
                setCountries(data)
            })
            .catch((error) => {
                console.log('err ', error);
            })
    }, [])

    // Handle the button press
    async function otpSignIn(phoneNumber) {
        dispatch(show())
        dispatch(checkIfUserDoctor("PATIENT", phoneNumber, props.navigation, selectedCountry))
    }


    return (

        <ImageContainer>
            <Content style={styles.container}>
                <View style={[styles.header, { width: '100%', justifyContent: 'center' }]}>
                    <View style={styles.heading}>
                        <Icon
                            name="arrowleft"
                            type="AntDesign"
                            style={{
                                color: 'white'
                            }}
                            onPress={() => props.navigation.navigate("PatientLogin")}
                        />
                    </View>
                </View>
                <View style={{ height: '70%', justifyContent: 'space-between', width: '100%' }}>
                    <Text style={styles.phoneNumber}>
                        {/* Enter Phone Number for {'\n'}Login */}
                        {strings.mobile_login}
                    </Text>

                    <Text style={styles.phoneNumberDetails}>
                        {strings.enterNumberTag2}
                    </Text>

                    <Item style={styles.Item}>
                        <TouchableOpacity
                            style={{
                                flexDirection: 'row',
                                alignItems: 'center'
                            }}
                            onPress={() => {
                                dispatch(setCountryModal(true))
                            }}
                        >
                            <Text style={styles.number91}>{selectedCountry}</Text>
                            <Icon
                                name="chevron-down"
                                type="Entypo"
                                style={{
                                    color: 'white',
                                    // fontSize: 22
                                }}
                            />
                        </TouchableOpacity>
                        <Input
                            style={{ color: '#FFFFFF' }}
                            value={mobile}
                            returnKeyType="done"
                            maxLength={10}
                            keyboardType={"numeric"}
                            placeholderTextColor={"#A0A9BE"}
                            placeholder={strings.enter_mobile_no}
                            onChangeText={(value) => setMobile(value)} />
                    </Item>
                </View>
            </Content>

            <Footer style={styles.footer}>
                <Button disabled={mobile.length !== 10}
                    danger style={[common.footerBtn, {
                        marginVertical: 10
                        // backgroundColor: mobile.length === 10 ? '#FF5E38' : '#b5b5b5'
                    }]}
                    onPress={() => {
                        otpSignIn(mobile)
                    }}>
                    <Text uppercase={false} style={[styles.number91, { color: 'white' }]}>
                        {strings.Next}
                    </Text>
                </Button>

            </Footer>

            <CountryPickerModal
                data={countries}
                callback={(selectedCountry) => {
                    setSelectedCountry(selectedCountry?.code)
                }}
            />
        </ImageContainer>
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
        // backgroundColor: '#FFFFFF'
    },
    phoneNumber: {
        marginTop: 40,
        color: "#FFFFFF",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_26,
    },
    footer: {
        height: 70,
        backgroundColor: "transparent",
    },
    Item: {
        marginTop: hp("2%"),
    },
    phoneNumberDetails: {
        marginTop: 15,
        color: "#A0A9BE",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
    },
    phoneNumberOtp: {
        color: "#302528",
        marginTop: 15,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        fontSize: FONT_SIZE_15,
    },
    number91: {
        color: "#FFFFFF",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        marginVertical: 10,
        marginLeft: 10,
        fontSize: FONT_SIZE_15,
    },

    radioMainContainer: {
        width: '100%', alignItems: 'flex-start', marginTop: 15, marginBottom: 40,
    },
    radioContainer: {
        marginTop: hp("2%"),
        width: '100%',
        flexDirection: 'row'
    },
    header: {
        height: 50,
        justifyContent: 'space-between'
    },
    heading: {
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: "center",
    },
    headerText: {
        flexDirection: "row",
        alignItems: "center",
        color: '#FF5E38',
        fontSize: FONT_SIZE_16,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
    },
    radioButtonStyle: {
        marginRight: 10, borderColor: 'black'
    }
});


export default LoginScreenWhatsapp;
