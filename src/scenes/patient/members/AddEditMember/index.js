import { SafeAreaView, ScrollView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import HeaderSehet from '../../../../components/header-common'
import { strings } from '../../../../utils/translation'
import { Content, Form, Icon, Input, Item, Label, Picker } from 'native-base'
import AppButton from '../../../../components/AppButton'
import { rowStyle } from '../../../../styles/commonStyle'
import Line from '../../../../components/line'
import moment from 'moment'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { showModal } from '../../../../actions/modal'
import { errorPayload } from '../../../../utils/helper'
import { useDispatch, useSelector } from 'react-redux'
import { apiAddNewMember, apiiUpdateMember } from '../api'

const index = (props) => {

    const action = props.navigation.state.params?.action

    const dispatch = useDispatch()
    const [formState, setFormState] = useState({
        fullName: action?.fullName,
        email: action?.email,
        mobileNo: action?.mobileNo,
        dob: action?.dob,
        gender: action?.gender,
        city: action?.city
    })
    const [tcAccept, setTcAccept] = useState(false)
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false)
    const userProfile = useSelector(state => state.profile.userProfile)

    const changeFormState = (key, value) => {
        setFormState(state => ({
            ...state,
            [key]: value
        }))
    }

    const validateEmail = (email) => {
        let reg = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w\w+)+$/);
        return reg.test(email)
    }

    const onPressContinue = () => {
        if (formState.fullName == "" || formState.email == "" || formState.mobileNo == "" || formState.dob == "" || formState.gender == "" || formState.city == "") {
            dispatch(showModal(errorPayload("All Fields are required!")))
            return
        }

        if (formState.mobileNo.length != 10) {
            dispatch(showModal(errorPayload("Mobile number should be 10 digits")))
            return
        }

        if (!validateEmail(formState.email)) {
            dispatch(showModal(errorPayload("Enter valid email ID")))
            return
        }

        if (tcAccept == false) {
            dispatch(showModal(errorPayload("Please accept terms and conditions")))
            return
        }

        if (action?.type == "UPDATE") {
            let body = {
                ...formState,
                userId: userProfile?.userId?._id,
                _id: action?._id,
            }
            dispatch(apiiUpdateMember(body, props.navigation))

        } else {
            let body = {
                ...formState,
                userId: userProfile?.userId?._id,
                dob: moment(formState.dob).format('YYYY-MM-DD'),
            }
            dispatch(apiAddNewMember(body, props.navigation))
        }
    }

    return (
        <View style={styles.contianer}>
            <HeaderSehet
                headerText={action?.type === "UPDATE" ? `${strings.update}` : `${strings.add_new}`}
                navigation={props?.navigation}
                changeTheme={true}
            />

            <SafeAreaView style={styles.mainContainer}>

                <Content>
                    <Form style={{ marginHorizontal: 10, marginVertical: 10 }}>
                        <Text style={styles.formtitle}>{strings.add_new_title}</Text>
                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.full_name} *`} </Label>
                            <Input
                                value={formState.fullName}
                                returnKeyType="done"
                                style={styles.marginItem}
                                numberOfLines={2}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("fullName", value)} />
                        </Item>

                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.email} *`} </Label>
                            <Input
                                value={formState.email}
                                returnKeyType="done"
                                style={styles.marginItem}
                                keyboardType='email-address'
                                numberOfLines={2}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("email", value)} />
                        </Item>

                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.Mobile_no} *`} </Label>
                            <Input
                                value={formState.mobileNo}
                                returnKeyType="done"
                                style={styles.marginItem}
                                keyboardType='number-pad'
                                maxLength={10}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("mobileNo", value)} />
                        </Item>

                        <Item
                            floatingLabel>
                            <Label style={styles.labelStyle}>{`${strings.city_only} *`} </Label>
                            <Input
                                value={formState.city}
                                returnKeyType="done"
                                style={styles.marginItem}
                                numberOfLines={2}
                                autoCorrect={false}
                                autoCapitalize="none"
                                onChangeText={(value) => changeFormState("city", value)} />
                        </Item>

                        <View style={{}}>
                            <View style={styles.genderPicker}>
                                <Label style={{ fontSize: 16, color: '#615e5e', marginHorizontal: 15 }}>{strings.gender}
                                    *</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="keyboard-arrow-down" type="MaterialIcons" />}
                                    style={{ width: '68%', padding: 0, margin: 0 }}
                                    selectedValue={formState.gender}
                                    onValueChange={(value) => {
                                        console.log('valuesss ', value);
                                        if (value != "0")
                                            changeFormState("gender", value)
                                    }}>
                                    <Picker.Item label="Select" value="0" />
                                    <Picker.Item label="Male" value="MALE" />
                                    <Picker.Item label="Female" value="FEMALE" />
                                    <Picker.Item label="Prefer Not to Say" value="Prefer Not to Say" />
                                    <Picker.Item label="Not Applicable" value="NOT APPLICABLE" />
                                </Picker>
                            </View>
                            <Line lineColor={"#A0A9BE"} style={{ marginHorizontal: 10 }} />
                        </View>

                        <Item
                            floatingLabel
                            onPress={() => {
                                setIsDatePickerVisible(!isDatePickerVisible)
                            }}
                        >
                            <Label>{strings.dob}</Label>
                            <Input
                                style={styles.marginItem}
                                value={
                                    formState.dob?.toString() == ""
                                        ? ""
                                        : moment(formState.dob)
                                            .format("DD MMMM YYYY")
                                            .toString()
                                }
                                disabled />
                            <Icon
                                name="calendar"
                                type="Feather"
                                style={styles.calIcon} />
                        </Item>


                        {<DateTimePickerModal
                            isVisible={isDatePickerVisible}
                            mode="date"
                            onConfirm={(date) => {
                                const dob = moment(date).format("YYYY-MM-DD");
                                changeFormState("dob", dob)
                                setIsDatePickerVisible(!isDatePickerVisible)
                            }}
                            onCancel={() => {
                                setIsDatePickerVisible(!isDatePickerVisible)
                            }}
                            maximumDate={new Date()}
                        />}

                    </Form>
                </Content>

                <View style={{ marginHorizontal: 20 }}>
                    <TouchableOpacity style={[rowStyle, { marginBottom: 10 }]} onPress={() => setTcAccept(!tcAccept)}>
                        <View style={[styles.checkBoxStyle, { backgroundColor: tcAccept == false ? '#FFF' : '#49D6B8', padding: 3, borderWidth: 1, borderColor: '#C1C1C1' }]}>
                            <Icon name='check' type='AntDesign' style={{ color: '#FFF', fontSize: 22 }} />
                        </View>
                        <Text style={{ flex: 1, color: '#C1C1C1' }}>By continuing, you agree that you have read and accept our
                            <Text style={styles.modalTextStyle}> T&C</Text>, <Text style={styles.modalTextStyle} onPress={() => { }}>Privacy Policy</Text>, <Text style={styles.modalTextStyle}>Patient Consent</Text> and <Text style={styles.modalTextStyle}>Refund Policy</Text></Text>
                    </TouchableOpacity>
                    <AppButton
                        style={{ backgroundColor: '#D72820' }}
                        title={strings.continue}
                        onPress={onPressContinue}
                    />
                </View>

            </SafeAreaView>

        </View>
    )
}

export default index

const styles = StyleSheet.create({
    contianer: {
        flex: 1,
        backgroundColor: '#F5F5F5'
    },
    marginItem: {
        marginTop: 3,
        fontSize: 15,
    },
    formtitle: {
        fontSize: 16,
        color: '#041A32',
        fontWeight: 'bold',
        marginHorizontal: 15,
        marginTop: 10,
    },
    labelStyle: { fontSize: 16 },
    checkBoxStyle: {
        backgroundColor: 'red',
        marginRight: 5,
        borderRadius: 5,
        alignSelf: 'flex-start'
    },
    modalTextStyle: {
        color: '#080A72'
    },
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between'
    },
    genderPicker: {
        marginTop: 20,
        alignItems: 'center',
        // justifyContent: 'center',
        flexDirection: 'row'
    },
})