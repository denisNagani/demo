import { showModal } from "../../../actions/modal"
import { setMembersData, setMembersExtraData } from "../../../actions/profile"
import http from "../../../services/http"
import { hide, show } from "../../../utils/loader/action"

export const apiGetAllMembers = () => {
    return dispatch => {
        dispatch(show())
        http
            .get('api/allMembers')
            .then((res) => {
                const data = res?.data
                if (data?.length > 0) {
                    const dataWithProfile = []

                    data?.map((itm) => {
                        dataWithProfile.push({
                            ...itm,
                            med_desc: '',
                            lastVisited: '',
                            referenceCode: '',
                            selectedDocs: []
                        })
                    })
                    dispatch(setMembersData(dataWithProfile))
                }
                dispatch(hide())
            })
    }
}

export const apiAddNewMember = (payload, navigation) => {
    return dispatch => {
        dispatch(show())
        http
            .post('api/member', payload, dispatch)
            .then((res) => {
                if (res?.status == 200) {
                    const payload = {
                        text: `New member added!`,
                        subText: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(showModal(payload))
                    navigation?.goBack()
                    dispatch(apiGetAllMembers())
                    return
                }
                dispatch(hide())
            })
    }
}

export const apiiUpdateMember = (payload, navigation) => {
    return dispatch => {
        dispatch(show())
        http
            .put('api/member', payload, dispatch)
            .then((res) => {
                if (res?.status == 200) {
                    const payload = {
                        text: `Member details updated!`,
                        subText: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(showModal(payload))
                    navigation?.goBack()
                    dispatch(apiGetAllMembers())
                }
                dispatch(hide())
            })
    }
}

export const apiDeleteMember = (memberId, navigation) => {
    return dispatch => {
        dispatch(show())
        http
            .delete(`api/member/${memberId}`)
            .then((res) => {
                if (res?.status == 200) {
                    const payload = {
                        text: `Member record deleted!`,
                        subText: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    };
                    dispatch(showModal(payload))
                    dispatch(apiGetAllMembers())
                }
                dispatch(hide())
            })
    }
}