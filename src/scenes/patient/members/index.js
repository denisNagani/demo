import { Alert, FlatList, Image, SafeAreaView, StyleSheet, Text, TextInput, TouchableOpacity, View, ScrollView, Dimensions, Platform } from 'react-native'
import React, { useEffect, useState } from 'react'
import HeaderSehet from '../../../components/header-common'
import { strings } from '../../../utils/translation'
import { rowStyle } from '../../../styles/commonStyle'
import Images from '../../../assets/images/index'
import AppButton from '../../../components/AppButton'
import { CheckBox, Form, Icon, Input, Item, Label } from 'native-base'
import { useDispatch, useSelector } from 'react-redux'
import { apiDeleteMember, apiGetAllMembers } from './api'
import { showModal } from '../../../actions/modal'
import { errorPayload } from '../../../utils/helper'
import { ifValid, isValid } from '../../../utils/ifNotValid'
import moment from 'moment'
import { base_url, paytm_mid, staging } from '../../../config'
import Axios from 'axios'
import { hide, show } from '../../../utils/loader/action'
import { followUpPost, reschedulePost } from '../../../actions/appointment'
import { postDoctorQuickBook, postDoctorSlotFlow1 } from '../../../actions/sehet/patient-action'
import AllInOneSDKManager from 'paytm_allinone_react-native';
import { uploadAttachment } from '../../../actions/sehet/patient-action'
import { CheckReadExternalPermission } from '../../../utils/permissions'
import DocumentPicker from 'react-native-document-picker'
import { requestPermissionStorage } from '../../../services/requestPermissions'

const index = (props) => {

    const dispatch = useDispatch()
    const userProfile = useSelector(state => state.profile.userProfile)
    const members = useSelector(state => state.profile.members)

    const selectedSpeciality = useSelector(state => state.specialitiesReducer.selectedSpeciality)
    const selectedSlotObj = useSelector(state => state.appointment.selectedSlotObj)
    const selectedSlotObjForQuickBook = useSelector(state => state.appointment.selectedSlotObjForQuickBook)
    const followUpData = useSelector(state => state.appointment.followUpData)
    const appointmentType = useSelector(state => state.appointment.appointmentType)

    const [membersData, setMembersData] = useState([])
    const [tcAccept, setTcAccept] = useState(false)
    const [myorderId, setorderID] = useState('')
    const [intakeInfo, setintakeInfo] = useState('')

    useEffect(() => {
        dispatch(apiGetAllMembers())
    }, [userProfile])

    useEffect(() => {
        setMembersData(members)
    }, [members])

    const createFormData = (docs) => {
        let imgName = "";
        const data = new FormData();
        docs.map((photo, index) => {
            if (photo.fileName === undefined) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else if (photo.fileName === null) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else {
                imgName = photo.fileName;
            }
            let newFile = {
                name: Platform.OS === 'android' ? photo.name : imgName,
                type: photo.type,
                uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
            }
            data.append("multi-files", newFile);
        })

        // alert("form data " + JSON.stringify(data))
        return data;
    };

    const onPressContinue = () => {
        if (tcAccept) {
            const selectedMember = membersData?.filter(itm => itm?.selected == true)[0]
            if (selectedMember) {

                dispatch(show())
                const uploadedImageData = selectedMember?.selectedDocs
                const userID = userProfile?.userId?._id
                if (uploadedImageData?.length > 0) {
                    //upload images first
                    dispatch(uploadAttachment(
                        createFormData(uploadedImageData),
                        userID,
                        (data) => {
                            updateProfileApiCall(selectedMember, data)
                        }
                    ))
                } else {
                    updateProfileApiCall(selectedMember, null)
                }
            } else {
                dispatch(showModal(errorPayload("Please select a member to continue")))
            }
        } else {
            dispatch(showModal(errorPayload("Please accept terms and conditions")))
        }
    }

    const updateProfileApiCall = async (data, imageData) => {
        let formBd = {
            name: data?.fullName,
            gender: data?.gender,
            dob: data?.dob,
            email: data?.email,
            mobileNo: data?.mobileNo,
            medicalDescription: data?.med_desc,
            lastVisitedDoctor: data?.lastVisited,
            refCode: data?.referenceCode,
            city: data?.city,
        }

        if (imageData != null) {
            formBd.documentDetails = imageData
        }
        showPaymentScreen(formBd)
    }

    const showPaymentScreen = (formBd) => {
        let checkPrice =
            appointmentType == "quickCall"
                ? selectedSlotObjForQuickBook?.paidAmount
                : selectedSpeciality.price

        if (checkPrice != 0) {
            const price = ifValid(checkPrice) ? checkPrice : 0;

            let random = Math.random()
            let orderId = JSON.stringify(random)

            Axios.post(base_url + "createPaytmTxnToken", {
                custId: "123SSS",
                custMobile: "446645465",
                orderId: orderId,
                txnAmount: price,
                currency: "INR"
            })
                .then((res) => {

                    // if (Platform.OS === "android") {
                    AllInOneSDKManager.startTransaction(
                        orderId,
                        paytm_mid,
                        res.data.data.body.txnToken,
                        price.toString(),
                        "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId,
                        staging,
                        true
                    ).then(result => {
                        displayResult(result, formBd, orderId)
                    }).catch(err => {
                        dispatch(showModal(errorPayload("Transaction cancelled")))
                        console.log("AllInOneSDKManager.startTransaction catch = ", JSON.stringify(err));
                        dispatch(hide())
                    })
                })
                .catch((err) => {
                    dispatch(hide())
                    console.log(JSON.stringify(err));
                    dispatch(showModal(errorPayload('err ' + err)))
                });
        }
        else {
            if (followUpData != null) {
                if (followUpData?.from_reschedule) {
                    //reschedule booking api call
                    const body = {
                        doctorId: followUpData?.doctorId,
                        appointmentDate: followUpData?.appointmentDate,
                        slotId: selectedSlotObj?.slotId,
                        appointmentType: "RESCHEDULED",
                        timeZone: selectedSlotObj?.timeZone,
                        newAppointmentDate: selectedSlotObj?.appointmentDate,
                    }
                    console.log('reschedule body ', body);
                    dispatch(reschedulePost(body, props.navigation))
                } else {
                    //followUp api call
                    const body = {
                        ...selectedSlotObj,
                        appointmentId: followUpData?.appointment_id
                    }
                    dispatch(followUpPost(body, props.navigation))
                }
            } else {
                dispatch(postDoctorSlotFlow1({
                    ...selectedSlotObj,
                    paidAmount: checkPrice,
                    intakeInfo: formBd,
                }, props.navigation))
            }
        }
    }

    const displayResult = async (result, intakeInfo, myorderId) => {
        console.log(JSON.stringify(result))
        dispatch(show())
        await Axios.post(base_url + "paymentStatus", {
            orderId: myorderId,
        })
            .then((res) => {
                console.log(" payment success res " + JSON.stringify(res.data))
                dispatch(hide())
                let _resMsg = res?.data?.data?.body?.resultInfo?.resultCode;
                if (ifValid(_resMsg)) {
                    let paidAmount = res?.data?.data?.body?.txnAmount;
                    if (_resMsg == "01") {
                        if (appointmentType == "quickCall") {
                            dispatch(postDoctorQuickBook(selectedSlotObjForQuickBook, props.navigation))
                        } else {
                            dispatch(postDoctorSlotFlow1({
                                ...selectedSlotObj,
                                paidAmount: paidAmount,
                                intakeInfo: intakeInfo,
                            }, props.navigation))
                        }
                    } else {
                        dispatch(showModal(errorPayload(strings.transaction_cancelled)))
                    }
                } else {
                    console.log(JSON.stringify(res))
                }
            })
            .catch((err) => {
                console.log("paymentStatus err catch " + err);
                dispatch(hide())
            });
    }

    return (
        <View style={styles.contianer}>
            <HeaderSehet
                changeTheme={true}
                headerText={strings.select_member}
                navigation={props?.navigation}
                rightIcon={'plus'}
                rightIconText={'Add New'}
                rightIconStyle={{ color: '#FF5E38', fontSize: 22, marginRight: 3 }}
                onRightIconPress={() => props.navigation.navigate('AddEditMember', {
                    action: {
                        type: "ADD",
                        fullName: "",
                        email: "",
                        mobileNo: "",
                        dob: "",
                        gender: "",
                        city: ""
                    }
                })}
            />

            <SafeAreaView style={{ flex: 1 }}>
                <FlatList
                    keyboardShouldPersistTaps='always'
                    style={{ flex: 1, marginHorizontal: 5 }}
                    keyExtractor={(item, index) => index.toString()}
                    data={membersData}
                    renderItem={({ item, index }) => {
                        let memberItem = item
                        let memberIndex = index


                        const isSelected = memberItem?.selected
                        const memberImageUrl = Images.male_doctor
                        const memberName = memberItem?.fullName
                        const memberEmail = memberItem?.email
                        const memberGender = memberItem?.gender
                        const memberMobile = memberItem?.mobileNo
                        const memberCity = memberItem?.city
                        const memberDOB = memberItem?.dob

                        const isSelectedStyle = isSelected ? { backgroundColor: '#038BEF', } : {}

                        const Section = ({ title, value, width }) => {
                            let giveWidth
                                = isValid(width)
                                    ? { width: width }
                                    : {}
                            return <View style={{ marginHorizontal: 5, }}>
                                <Text style={styles.sectionTitle}>{title}</Text>
                                <Text
                                    style={[styles.sectionValue, giveWidth]}
                                    numberOfLines={2}
                                >{value}</Text>
                            </View>
                        }

                        const onPressCard = () => {
                            const newUpdatedData = membersData.map((itm) => {
                                return {
                                    ...itm,
                                    selected: itm?._id == memberItem?._id ? true : false
                                }
                            })
                            setMembersData(newUpdatedData)
                        }

                        const changeFormState = (key, value) => {
                            const newData = membersData?.map((memI, memInd) => {
                                if (memInd == memberIndex) {
                                    return { ...memI, [key]: value?.toString() }
                                } return memI
                            })
                            setMembersData(newData)
                        }

                        const onPressDelete = () => {
                            Alert.alert("Delete member", "Are you sure want to delete this member?", [
                                {
                                    text: "Cancel",
                                },
                                {
                                    text: "OK",
                                    onPress: async () => {
                                        dispatch(apiDeleteMember(memberItem?._id, props.navigation))
                                    }
                                },
                            ]);
                        }

                        const _handleSelectDoc = async () => {
                            // Pick multiple files
                            try {
                                let isPermitted;
                                if (Platform.Version > 32) {
                                    isPermitted = await requestPermissionStorage()
                                } else {
                                    isPermitted = await CheckReadExternalPermission()
                                }
                                if (isPermitted) {
                                    const results = await DocumentPicker.pickMultiple({
                                        type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
                                    });


                                    console.log('results ,', results);

                                    // format new images 
                                    const newImages = []
                                    results.forEach(res => {
                                        newImages.push({
                                            uri: res.uri,
                                            type: res.type,
                                            name: res.name,
                                            size: res.size
                                        })
                                    })

                                    console.log('newImages ,', newImages);

                                    // add images to members data
                                    const newData = membersData?.map((memI, memInd) => {
                                        if (memInd == memberIndex) {
                                            return {
                                                ...memI,
                                                "selectedDocs": [...memberItem?.selectedDocs, ...newImages]
                                            }
                                        }
                                        return memI
                                    })
                                    setMembersData(newData)

                                    console.log('dadada ', membersData);
                                } else {
                                    console.log("not permitted");
                                }
                            } catch (err) {
                                if (DocumentPicker.isCancel(err)) {
                                    // User cancelled the picker, exit any dialogs or menus and move on
                                    // this.setState({
                                    //     selectedImage: []
                                    // })
                                } else {
                                    console.log(err);
                                }
                            }
                        }

                        const _removeDocFunc = (indexOfDoc) => {
                            const newArr = memberItem?.selectedDocs;
                            if (newArr.length > 0) {
                                newArr.splice(indexOfDoc, 1);
                                console.log(newArr + indexOfDoc);

                                const newData = membersData?.map((memI, memInd) => {
                                    if (memInd == memberIndex) {
                                        return {
                                            ...memI,
                                            "selectedDocs": newArr
                                        }
                                    } return memI
                                })
                                setMembersData(newData)
                            }
                        }

                        let ScreenWidth = Dimensions.get('window').width

                        return <View style={[styles.cardCont, { borderWidth: 1, borderColor: isSelected ? '#038BEF' : '#FFF' }]}>
                            <TouchableOpacity onPress={onPressCard}>
                                <View style={rowStyle}>
                                    <Image
                                        source={memberImageUrl}
                                        style={styles.imageIconStyle}
                                    />
                                    <View style={styles.cont2}>
                                        <Text style={styles.memberNameStyle}>{memberName}</Text>
                                        <Text style={styles.memberEmailStyle}>{memberEmail}</Text>
                                    </View>
                                    <View style={[styles.memberSelectStyle, isSelectedStyle]}>
                                    </View>
                                </View>

                                <View style={[{ flexDirection: 'row', marginVertical: 10, justifyContent: 'space-between' }]}>
                                    <Section title={strings.gender} value={memberGender} width={ScreenWidth / 6} />
                                    <Section title={strings.Mobile_no} value={memberMobile} />
                                    <Section title={strings.city_only} value={memberCity} width={ScreenWidth / 5} />
                                    <Section title={strings.dob} value={memberDOB} />
                                </View>

                                <View style={[rowStyle, { flex: 1, alignSelf: 'flex-end', marginVertical: 5 }]}>
                                    <TouchableOpacity style={[styles.btnStyle, { marginRight: 8 }]} onPress={() => {
                                        if (memberItem?.allowDelete) {
                                            props.navigation.navigate('AddEditMember', { action: { type: "UPDATE", ...memberItem } })
                                        } else {
                                            props.navigation.navigate('EditPatientProfileScreen')
                                        }
                                    }}>
                                        <Text style={styles.btnTextStyle}>{memberItem?.allowDelete ? "Update" : "Update Profile"}</Text>
                                    </TouchableOpacity>
                                    {memberItem?.allowDelete && <TouchableOpacity style={styles.btnStyle} onPress={onPressDelete}>
                                        <Text style={styles.btnTextStyle}>Delete</Text>
                                    </TouchableOpacity>}
                                </View>

                                {
                                    isSelected && (
                                        <Form>
                                            <Text style={styles.formtitle}>{strings.addMOreDetails}</Text>
                                            <Item
                                                floatingLabel>
                                                <Label style={{ fontSize: 15 }}>{strings.medical_description} </Label>
                                                <Input
                                                    value={memberItem?.med_desc}
                                                    returnKeyType="done"
                                                    style={styles.marginItem}
                                                    numberOfLines={2}
                                                    autoCorrect={false}
                                                    autoCapitalize="none"
                                                    onChangeText={(value) => changeFormState("med_desc", value)} />

                                            </Item>

                                            <Item
                                                floatingLabel>
                                                <Label style={{ fontSize: 15 }}>{strings.name_of_last_doc}</Label>
                                                <Input
                                                    style={styles.marginItem}
                                                    value={memberItem?.lastVisited}
                                                    autoCorrect={false}
                                                    autoCapitalize="none"
                                                    onChangeText={(value) => changeFormState("lastVisited", value)} />
                                            </Item>

                                            <Item
                                                floatingLabel>
                                                <Label style={{ fontSize: 15 }}>{strings.reference_code}</Label>
                                                <Input
                                                    style={styles.marginItem}
                                                    value={memberItem?.referenceCode}
                                                    autoCapitalize="none"
                                                    autoCorrect={false}
                                                    onChangeText={(value) => changeFormState("referenceCode", value)} />
                                            </Item>

                                            <Item>
                                                <View style={{ marginVertical: 10 }}>
                                                    <Label style={{ marginVertical: 15, }}>{"Upload reports if required\n(only pdf, jpg)"}</Label>
                                                    <View style={{ flexDirection: 'row' }}>
                                                        <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                                            <TouchableOpacity onPress={_handleSelectDoc}>
                                                                <Image
                                                                    source={Images.uploadImage}
                                                                    style={{ height: 100, width: 100, borderRadius: 15 }}
                                                                />
                                                            </TouchableOpacity>
                                                            {
                                                                memberItem?.selectedDocs?.map((doc, index) => (
                                                                    <View>
                                                                        {
                                                                            doc.type === "image/jpeg" || doc.type === "image/png"
                                                                                ? <Image
                                                                                    source={
                                                                                        doc.uri != null && doc.type === "image/jpeg"
                                                                                            ? { uri: doc.uri }
                                                                                            : Images.uploadImage
                                                                                    }
                                                                                    style={{ height: 100, width: 100, borderRadius: 15, marginHorizontal: 10 }}
                                                                                />
                                                                                : <View style={{ backgroundColor: '#e9eef1', justifyContent: 'center', height: 100, width: 100, borderRadius: 15, marginHorizontal: 10 }}>
                                                                                    <Text style={{ textAlign: 'center', fontSize: 14, fontWeight: 'bold' }}>{doc.type + ' file'}</Text>
                                                                                </View>
                                                                        }
                                                                        <TouchableOpacity
                                                                            onPress={() => _removeDocFunc(index)}
                                                                            style={{ position: 'absolute', right: 0 }}
                                                                        >
                                                                            <Icon
                                                                                name="circle-with-cross"
                                                                                type="Entypo"
                                                                                style={{
                                                                                    color: 'red',
                                                                                    // backgroundColor: 'white'
                                                                                }}
                                                                            />
                                                                        </TouchableOpacity>
                                                                    </View>
                                                                ))
                                                            }
                                                        </ScrollView>
                                                    </View>
                                                </View>
                                            </Item>


                                        </Form>
                                    )
                                }
                            </TouchableOpacity>
                        </View>
                    }}
                />

                <View style={{ marginHorizontal: 15 }}>
                    <TouchableOpacity style={[rowStyle, { marginBottom: 10, marginTop: 5 }]} onPress={() => setTcAccept(!tcAccept)}>
                        <View style={[styles.checkBoxStyle, { backgroundColor: tcAccept == false ? '#FFF' : '#49D6B8', padding: 3, borderWidth: 1, borderColor: '#C1C1C1' }]}>
                            <Icon name='check' type='AntDesign' style={{ color: '#FFF', fontSize: 22 }} />
                        </View>
                        <Text style={{ flex: 1, color: '#C1C1C1' }}>By continuing, you agree that you have read and accept our
                            <Text style={styles.modalTextStyle}> T&C</Text>, <Text style={styles.modalTextStyle} onPress={() => { }}>Privacy Policy</Text>, <Text style={styles.modalTextStyle}>Patient Consent</Text> and <Text style={styles.modalTextStyle}>Refund Policy</Text></Text>
                    </TouchableOpacity>
                    <AppButton
                        style={{ backgroundColor: '#D72820', marginBottom: 10, }}
                        title={strings.continue}
                        onPress={onPressContinue}
                    />
                </View>

            </SafeAreaView>
        </View>
    )
}

export default index

const styles = StyleSheet.create({
    contianer: {
        flex: 1,
        backgroundColor: '#F5F5F5'
    },
    cardCont: {
        margin: 10,
        padding: 10,
        backgroundColor: '#FFF',
        borderRadius: 5,
    },
    imageIconStyle: {
        height: 50,
        width: 50,
        borderRadius: 50,
        marginRight: 10,
        borderWidth: 1,
    },
    cont2: {
        flex: 1
    },
    memberNameStyle: {
        color: '#586278',
        fontSize: 16
    },
    memberEmailStyle: {
        color: '#9297A2',
        fontSize: 13
    },
    memberSelectStyle: {
        borderWidth: 1,
        padding: 10,
        borderRadius: 50,
        borderColor: '#038BEF',
    },
    sectionTitle: {
        color: '#C1C1C1',
        fontSize: 13,
    },
    sectionValue: {
        color: '#586278',
        fontSize: 13
    },
    marginItem: {
        marginTop: 3,
        fontSize: 15,
    },
    formtitle: {
        fontSize: 15,
        color: '#041A32',
        fontWeight: 'bold',
        marginHorizontal: 15,
        marginTop: 10,
    },
    checkBoxStyle: {
        backgroundColor: 'red',
        marginRight: 5,
        borderRadius: 5,
        alignSelf: 'flex-start'
    },
    modalTextStyle: {
        color: '#080A72'
    },
    btnStyle: {
        borderWidth: 1,
        borderColor: '#038BEF',
        padding: 8,
        borderRadius: 5
    },
    btnTextStyle: {
        fontSize: 15,
        color: '#038BEF'
    }
})