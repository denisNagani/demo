import React, { useEffect, useState } from 'react'
import { View, Dimensions, ScrollView, Image } from 'react-native'
import HeaderSehet from '../../../components/header-common'
import AutoHeightWebView from '../../../components/AutoWebViewSize';
import { getOfferId, setOfferId } from '../../../services/auth';
import { isValid } from '../../../utils/ifNotValid';
import http from '../../../services/http';
import { useDispatch } from 'react-redux';
import { appName } from '../../../config';

const index = (props) => {
    const dispatch = useDispatch()
    const BannerWidth = Dimensions.get('window').width;
    const BannerHeight = 200;

    const [data, setData] = useState(null)
    const navigationData = props.navigation.state?.params?.data

    useEffect(() => {
        getDetailsFromApi()
    }, [])

    const getDetailsFromApi = async () => {
        let offerId = JSON.parse(await getOfferId());
        if (isValid(offerId)) {
            //api call
            http
                .get(`pushNotification/${offerId}`, dispatch)
                .then(res => {
                    setOfferId('')
                    if (res.status === 200) {
                        setData(res?.data)
                    }
                })
                .catch(err => {
                    setOfferId('')
                    console.log('/pushNotification err ', err);
                })
        } else {
            if (isValid(navigationData)) {
                setData(navigationData)
            }
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
            <HeaderSehet headerText={appName} navigation={props.navigation} changeTheme={true} />
            <View style={{ flex: 1, marginHorizontal: 10, marginVertical: 10, }}>
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Image
                        style={{ height: 250, width: '100%' }}
                        resizeMode='contain'
                        source={{ uri: data?.imageUrl }}
                    />
                    <AutoHeightWebView
                        style={{
                            width: BannerWidth - 40,
                            marginLeft: 10,
                            marginBottom: 10,
                            marginTop: 15,
                        }}
                        onSizeUpdated={size => console.log(size?.height)}
                        source={{ html: data?.data }}
                    />

                </ScrollView>
            </View>
        </View>
    )
}

export default index
