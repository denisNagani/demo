import React, { useEffect, useRef, useState } from "react";
import { Alert, SafeAreaView, StyleSheet, Text, View } from "react-native";
import { Container, Icon } from "native-base";
import { useDispatch, useSelector } from "react-redux";
import CodeInput from 'react-native-confirmation-code-input';
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_SIZE_14,
    FONT_SIZE_26
} from "../../../styles/typography";
import { sendOtpAction, verifyOtp, verifyOtpFirebase, setOptUsing } from "../../../actions/sehet/user-action";
import { ifNotValid, ifValid } from "../../../utils/ifNotValid";
import { errorLog, methodLog, successLog } from "../../../utils/fireLog";
import { showModal } from "../../../actions/modal";
import { errorPayload } from "../../../utils/helper";
import { getFcmToken } from "../../../services/auth";
import { hide } from "../../../utils/loader/action";
import auth from '@react-native-firebase/auth'
import ImageContainer from "../../../components/ImageContainer";
import { strings } from "../../../utils/translation/index";

const OTPScreen = (props) => {
    const dispatch = useDispatch();
    const docName = useSelector((state) => state.bookings.docName);
    const docImg = useSelector((state) => state.bookings.docImage);
    const otpResponseGoogle = useSelector((state) => state.userReducer.otpResponseGoogle);
    const bookingFlow1or2 = useSelector((state) => state.appointment.bookingFlow1or2);
    const inputRef = useRef('codeInputRef2');
    const [random, SetRandom] = useState(Math.random());
    const data = ifValid(props.navigation.state.params) ? props.navigation.state.params.mobileAndMedia : undefined;
    const [timeLeft, setTimeLeft] = useState(30);
    const navigation = props.navigation;
    const otpSession = useSelector((state) => state.userReducer.otpSession);
    const otpUsing = useSelector((state) => state.userReducer.otpUsing);
    const fromDynamicLink = useSelector((state) => state.profile.fromDynamicLink);
    /// countdown timer.
    useEffect(() => {
        if (timeLeft == 0) {
        }
        if (!timeLeft) return;
        const intervalId = setInterval(() => {
            setTimeLeft(timeLeft - 1);
        }, 1000);
        return () => clearInterval(intervalId);
    }, [timeLeft]);

    ///handleBackButtonClick
    const handleBackButtonClick = () => {
        Alert.alert("OTP is not verified yet", "Are you sure you want to cancel verification ?", [
            {
                text: "Cancel", onPress: () => {
                }
            },
            { text: "OK", onPress: () => props.navigation.navigate('LoginScreenMobile') },
        ]);
        return true;
    }

    ///hardwareBackPress register.
    // useEffect(() => {
    //     // BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    //     // return () => {
    //     //     BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    //     // };
    //     auth().onAuthStateChanged(user => {
    //         console.log("auth changed callled " + JSON.stringify(user));
    //         if (user) {
    //             auth()
    //                 .signOut()
    //                 .then(() => console.log("OPT verify screen firebase opt auto verified and no worries you cleared user"))
    //                 .catch(err => console.log("OPT verify screen  ops there is no user to clear..."))
    //         }
    //         else {
    //             console.log("null user in OPT verify screen ");
    //         }
    //     }, err => {
    //         alert(JSON.stringify(err))
    //     })
    // }, []);


    const ResendOtp = () => {
        methodLog("ResendOtp" + timeLeft)
        if (timeLeft != 0) {
            return;
        }
        setTimeLeft(30)
        dispatch(setOptUsing('api'))
        dispatch(sendOtpAction(data, dispatch, navigation, false))
    }

    //for api opt
    const handleVerify = async (otp) => {
        errorLog("handleVerify")
        if (ifNotValid(data)) {
            errorLog("data is invalid")
            return;
        }
        /// handle sms verification.
        if (ifValid(otpSession) && otpSession !== '') {
            successLog(otpSession)
            //TODO : temporary removing sessionId.
            const fcmToken = await getFcmToken()
            const parsefcmToken = JSON.parse(fcmToken)
            console.log("verify opt passed token ", fcmToken);
            let body = { ...data, otp: otp, sessionId: otpSession, fcmToken: parsefcmToken, msgMode: 'MOBILE' }
            // let body = {...data, otp: otp}
            console.log("verify opt obj" + {
                body, navigation, bookingFlow1or2
            });
            dispatch(verifyOtp(body, navigation, bookingFlow1or2, fromDynamicLink))
        } else {
            errorLog("otp session is invalid ")
            dispatch(showModal(errorPayload("OTP session is invalid")));
        }
    };

    //for firebase opt
    const handleVerifyFirebase = async (otp) => {
        methodLog('confirmFirePhoneAuth')
        try {
            const token = await getFcmToken()
            const fcm_token = JSON.parse(token)
            const body = {
                mobileNo: data.mobileNo,
                otp: otp,
                fcmToken: fcm_token,
                msgMode: 'MOBILE'
            }
            console.log("verifyOtpFirebase body ", body);
            console.log("verify opt obj" + JSON.stringify({
                body, navigation, bookingFlow1or2
            }));
            dispatch(verifyOtpFirebase(otpResponseGoogle, body, navigation, bookingFlow1or2, fromDynamicLink))
        } catch (error) {
            let _error = ifValid(error) ? error.toString() : '';
            console.log('///_error')
            console.log(_error)
            dispatch(hide())
            dispatch(showModal(errorPayload("OTP Verification Failed ", _error)));
        }
    };

    async function confirmFirePhoneAuth(otp) {
        //call verifyOpt Api (if 200 the call login api)
        methodLog('confirmFirePhoneAuth')
        try {
            if (ifValid(otpResponseGoogle)) {
                await otpResponseGoogle.confirm(otp);
                console.log("confirmed")
                if (ifValid(bookingFlow1or2) && bookingFlow1or2 == '1') {
                    props.navigation.navigate("BookingSummaryScreen")
                } else {
                    props.navigation.navigate("BookingSummaryScreen")
                }
            } else {
                errorLog("data.confirm not valid ")
            }
        } catch (error) {
            let _error = ifValid(error) ? error.toString() : '';
            console.log('///_error')
            console.log(_error)
            dispatch(showModal(errorPayload("OTP Verification Failed ", _error)));

        }
    }

    // const veriFire = (code) => {
    //     const creds = auth.PhoneAuthProvider.credential(
    //         otpResponseGoogle.verificationId,
    //         code
    //     )
    //     auth().signInWithCredential(creds)
    //         .then(res => {
    //             console.log("res $$ " + JSON.stringify(res));
    //         })
    //         .catch(err => {
    //             console.log("err $$ " + JSON.stringify(err));

    //         })

    // }


    return (
        <ImageContainer>
            <View style={styles.container}>

                <SafeAreaView style={[styles.header, { width: '100%', justifyContent: 'center' }]}>
                    <View style={styles.heading}>
                        <Icon
                            name="arrowleft"
                            type="AntDesign"
                            style={{ color: '#FFFFFF' }}
                            onPress={() => handleBackButtonClick()}
                        />
                    </View>
                </SafeAreaView>
                <Text style={styles.phoneNumber}>{strings.Verification_code}</Text>
                <Text style={styles.phoneNumberDetails}>
                    {strings.Verification_code_sent}
                </Text>
                <View style={{ height: 85 }}>

                    <CodeInput
                        ref={inputRef}
                        className={'border-b'}
                        activeColor='#FFFFFF'
                        inactiveColor='rgba(0, 0, 0, 1)'
                        space={10}
                        keyboardType="numeric"
                        codeInputStyle={{ flex: 1 }}
                        autoFocus={true}
                        containerStyle={{}}
                        codeLength={6}
                        size={50}
                        inputPosition='center'
                        onFulfill={(code) => otpUsing == "api" ? handleVerify(code) : handleVerifyFirebase(code)}
                    // onFulfill={(code) => veriFire(code)}
                    />
                </View>
                <Text style={[styles.resendStyle, { color: timeLeft === 0 ? '#FFFFFF' : '#A0A9BE' }]} onPress={() => ResendOtp()}>
                    {strings.resend}
                    <Text
                        style={styles.resendStyleTimer}> 0:{timeLeft}</Text></Text>

            </View>
        </ImageContainer>
    );
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor: '#fff',
        padding: 20,
    },
    phoneNumber: {
        marginTop: 40,
        color: "#FFFFFF",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_26,
    },
    phoneNumberDetails: {
        marginTop: 15,
        color: "#A0A9BE",
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
    },
    resendStyle: {
        color: '#041A32',
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
    },
    resendStyleTimer: {
        color: '#FF5E38',
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
    },
    heading: {
        flexDirection: "row",
        justifyContent: 'space-between',
        alignItems: "center",
    },
});


export default OTPScreen;
