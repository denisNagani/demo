import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";

export default (styles = StyleSheet.create({
	container: {
		backgroundColor: "#1E2081",
		alignItems: "center",
	},
	docImg: {
		height: 120,
		width: 120,
		borderRadius: 120 / 2,
	},
	Image: {
		backgroundColor: "#A0A9BE",
		borderRadius: 100,
		marginTop: hp("20%"),
	},
	content: {
		marginTop: hp("6%"),
		marginLeft: 20,
	},
	contentText: {
		color: "#FFFFFF",
		fontSize: 20,
		textAlign: "center",
	},
	subContentText: {
		marginTop: 15,
		textAlign: "center",
	},
	Btn: {
		marginTop: 50,
		width: 330,
		justifyContent: "center",
		borderRadius: 10,
	},
}));
