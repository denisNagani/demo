import React, { Component } from "react";
import { Dimensions, NativeEventEmitter, NativeModules, Platform, View, Image, TouchableOpacity } from "react-native";
import { Body, Button, CheckBox, Container, Content, Form, Icon, Input, Item, Label, Picker, Text } from "native-base";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import { showModal } from "../../../actions/modal";
import styles from "./style";
import validate from "../../../utils/validation_wrapper";
import HeaderSehet from "../../../components/header-common";
import { connect } from "react-redux";
import { signUpCall } from "../../../actions/login";
import { methodLog, recordError, successLog } from "../../../utils/fireLog";
import axios from "axios";
import { base_url, paytm_mid, staging } from "../../../config";
import { ifEmpty, ifNotEmpty, ifNotValid, ifValid, isValid } from "../../../utils/ifNotValid";
import { postDoctorQuickBook, postDoctorSlotFlow1, postPatientDetails, uploadAttachment } from "../../../actions/sehet/patient-action";
import { hide, show } from "../../../utils/loader/action";
import Line from "../../../components/line";
import TAndCCommon from "../../../components/t_and_c_common";
import PrivacyPolicyCommon from "../../../components/privacy_policy_common";
import PatientConsent from "../../../components/PatientConsent";
import Refund from "../../../components/Refund";
import Images from "../../../assets/images";
import DocumentPicker from "react-native-document-picker";
import { ScrollView } from "react-native-gesture-handler";
import { errorPayload } from "../../../utils/helper";
import { strings, translateToHindi } from "../../../utils/translation";
import AllInOneSDKManager from 'paytm_allinone_react-native';
import { CheckReadExternalPermission } from "../../../utils/permissions";
import { followUpPost, reschedulePost } from "../../../actions/appointment";

const { height } = Dimensions.get("window");

// const AllInOneSDKManager = NativeModules.AllInOneSDKManager;
// // instantiate the event emitter
// const CounterEvents = new NativeEventEmitter(
//     NativeModules.AllInOneSDKSwiftWrapper,
// );
// const AllInOneSDKPlugin = NativeModules.AllInOneSDKSwiftWrapper;

class PatientDetails extends Component {

    constructor(props) {
        super(props);
        successLog("Sign up Screen screen loaded");
        let user = ifValid(this.props.userProfile) ? ifValid(this.props.userProfile.userId) ?
            this.props.userProfile.userId : {} : {};

        this.state = {
            userId: ifValid(user._id) ? user._id : '',
            msgMode: ifValid(user.msgMode) ? user.msgMode : '',
            fullName: ifValid(user.fullName) ? user.fullName : '',
            fullNameError: null,
            email: ifValid(user.email) ? user.email : '',
            emailError: null,
            mobile: ifValid(user.mobileNo) ? user.mobileNo.toString() : '',
            mobileError: null,
            gender: ifValid(user.gender) ? user.gender : '',
            city: ifValid(user.city) ? user.city : '',
            dob: ifValid(user.dob) && user.dob !== '' ? moment(user.dob).format("YYYY-MM-DD").toString() : '',
            isDatePickerVisible: false,
            medical: "",
            referenceCode: "",
            lastVisited: "",
            checked: false,
            secureTextEntry: true,
            modal1: false,
            modal2: false,
            modal3: false,
            modal4: false,
            orderId: '',
            selectedImage: null,
            selectedDocs: [],
            intakeInfo: null
        };
    }

    toggleDatePicker = () => {
        this.setState({ isDatePickerVisible: !this.state.isDatePickerVisible });
    };

    updateSecureTextEntry = () => {
        this.setState({
            secureTextEntry: !this.state.secureTextEntry,
        });

    };

    handleConfirm = (date) => {
        methodLog("handleConfirm")
        try {
            const dob = moment(date).format("YYYY-MM-DD");
            this.setState({
                dob: "  ",
                isDatePickerVisible: false,
            }, () => {
                this.setState({
                    dob: dob,
                    isDatePickerVisible: false,
                })
            });
        } catch (e) {
            recordError(e)
        }
    };

    isValid = () => {
        const fullNameError = validate("fullName", this.state.fullName);
        const emailError = validate("email", this.state.email);
        const passwordError = validate("password", this.state.password);
        this.setState({ emailError, passwordError, fullNameError });
        return !emailError && !passwordError && !fullNameError;
    };

    _back() {
        this.props.navigation.goBack();
    }

    _onChangeValue(key, value) {
        try {
            let val = ''
            if (key == "fullName") {
                val = value.replace(/[^A-Za-z_ ]/gi, "");
                if (val === undefined || val === null)
                    return
                this.setState({ [key]: val });
            } else {
                this.setState({ [key]: value }, () => {
                    this._onBlur("email")
                    this._checkMobileValid();
                });
            }
        } catch (e) {
            recordError(e)
        }
    }

    componentDidMount() {
        // subscribe to event
        // CounterEvents.addListener(
        //     'responseIfNotInstalled',
        //     // cf(res)
        //     (res) => this.displayResult(res),
        //     //  console.log("response recieved from paytm event", res)
        // );
    }

    _onBlur(key) {
        this.setState({
            [`${key}Error`]: validate(key, this.state[key]),
        });
    }

    _checkMobileValid = () => {
        let error = false;
        if (this.state.mobile.length === 10) error = null
        else error = true
        this.setState({
            mobileError: error
        })
    }

    showPaymentScreen = () => {
        // const checkPrice = this.props.selectedSpeciality.price
        let checkPrice =
            this.props.appointmentType == "quickCall"
                ? this.props.selectedSlotObjForQuickBook?.paidAmount
                : this.props.selectedSpeciality.price
        if (checkPrice != 0) {
            // const price = ifValid(this.props.selectedSpeciality) ? this.props.selectedSpeciality.price : 0;
            const price = ifValid(checkPrice) ? checkPrice : 0;

            let random = Math.random()
            let orderId = JSON.stringify(random)
            this.setState({
                orderId: orderId
            })
            axios.post(base_url + "createPaytmTxnToken", {
                custId: "123SSS",
                custMobile: "446645465",
                orderId: orderId,
                txnAmount: price,
                currency: "INR"
            })
                .then((res) => {

                    // if (Platform.OS === "android") {
                    AllInOneSDKManager.startTransaction(
                        orderId,
                        paytm_mid,
                        res.data.data.body.txnToken,
                        price.toString(),
                        "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId,
                        staging,
                        true
                    ).then(result => {
                        this.displayResult(result)
                    }).catch(err => {
                        this.props.showModal(errorPayload("Transaction cancelled"))
                        console.log("AllInOneSDKManager.startTransaction catch = ", JSON.stringify(err));
                    })

                    // } else {
                    //     console.log("-------------------------open Native--------", NativeModules);
                    //     let result = AllInOneSDKPlugin.openPaytm(
                    //         paytm_mid,
                    //         orderId,
                    //         res.data.data.body.txnToken,
                    //         price.toString(),
                    //         "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId,
                    //         staging
                    //     );
                    // }

                })
                .catch((err) => {
                    this.props.hide()
                    console.log(JSON.stringify(err));
                    this.props.showModal(errorPayload('err ' + err))
                });
        }
        else {
            if (this.props.followUpData != null) {
                if (this.props.followUpData?.from_reschedule) {
                    //reschedule booking api call
                    const body = {
                        doctorId: this.props.followUpData?.doctorId,
                        appointmentDate: this.props.followUpData?.appointmentDate,
                        slotId: this.props.selectedSlotObj?.slotId,
                        appointmentType: "RESCHEDULED",
                        timeZone: this.props.selectedSlotObj?.timeZone,
                        newAppointmentDate: this.props.selectedSlotObj?.appointmentDate,
                    }
                    console.log('reschedule body ', body);
                    this.props.reschedulePost(body, this.props.navigation)
                } else {
                    //followUp api call
                    const body = {
                        ...this.props.selectedSlotObj,
                        appointmentId: this.props.followUpData?.appointment_id
                    }
                    this.props.followUpPost(body, this.props.navigation)
                }
            } else {
                this.props.postDoctorSlotFlow1({
                    ...this.props.selectedSlotObj,
                    paidAmount: checkPrice,
                    intakeInfo: this.state.intakeInfo,
                    refCode: this.state.referenceCode
                }, this.props.navigation)
            }
        }
    }

    displayResult = async (result) => {
        console.log(JSON.stringify(result))
        this.props.show()
        await axios.post(base_url + "paymentStatus", {
            orderId: this.state.orderId,
        })
            .then((res) => {
                console.log(" payment success res " + JSON.stringify(res.data))
                this.props.hide()
                let _resMsg = res.data.data.body.resultInfo.resultCode;
                let paidAmount = res.data.data.body.txnAmount;
                if (ifValid(_resMsg)) {
                    if (_resMsg == "01") {
                        if (this.props.appointmentType == "quickCall") {
                            this.props.postDoctorQuickBook(this.props.selectedSlotObjForQuickBook, this.props.navigation)
                        } else {
                            this.props.postDoctorSlotFlow1({
                                ...this.props.selectedSlotObj,
                                paidAmount: paidAmount,
                                intakeInfo: this.state.intakeInfo,
                                refCode: this.state.referenceCode,
                            }, this.props.navigation)
                        }
                    } else {
                        // const payload = {
                        //     text: "Transaction cancelled",
                        //     subText: '',
                        //     iconName: "closecircleo",
                        //     modalVisible: true,
                        // };
                        this.props.showModal(errorPayload(strings.transaction_cancelled))
                    }
                } else {
                    console.log(JSON.stringify(res))
                }
            })
            .catch((err) => {
                console.log("paymentStatus err catch " + err);
                this.props.hide()
            });
    }

    updateProfileApiCall = async (data) => {
        const { email, fullName, mobile, gender, city, dob, medical, lastVisited, referenceCode } = this.state;
        let formBd = {
            name: fullName,
            gender: gender,
            dob: isValid(dob) ? moment(dob).format("YYYY-MM-DD").toString() : "",
            email: email,
            mobileNo: mobile,
            medicalDescription: medical,
            lastVisitedDoctor: lastVisited,
            refCode: referenceCode,
            city: city,
        }

        if (isValid(data)) {
            formBd.documentDetails = data
        }

        this.setState({ intakeInfo: formBd }, () => {
            this.showPaymentScreen()
        })
    }

    onChecked = () => {
        this._checkMobileValid()
        this.setState({ checked: !this.state.checked });
    };

    onModal1 = () => {
        this.setState({ modal1: !this.state.modal1 });
    };

    onModal2 = () => {
        this.setState({ modal2: !this.state.modal2 });
    };

    onModal3 = () => {
        this.setState({ modal3: !this.state.modal3 });
    };

    onModal4 = () => {
        this.setState({ modal4: !this.state.modal4 });
    };

    _handleSelectDoc = async () => {
        // Pick multiple files
        try {
            const isPermitted = await CheckReadExternalPermission()
            if (isPermitted) {
                const results = await DocumentPicker.pickMultiple({
                    type: [DocumentPicker.types.pdf, DocumentPicker.types.images],
                });
                results.forEach(res => {
                    this.setState({
                        selectedDocs: [...this.state.selectedDocs, {
                            uri: res.uri,
                            type: res.type,
                            name: res.name,
                            size: res.size
                        }]
                    })
                })
            } else {
                console.log("not permitted");
            }
        } catch (err) {
            if (DocumentPicker.isCancel(err)) {
                // User cancelled the picker, exit any dialogs or menus and move on
                // this.setState({
                //     selectedImage: []
                // })
            } else {
                console.log(err);
            }
        }
    }

    _onContinuePress = () => {
        methodLog("_onContinuePress");
        if (this.state.selectedDocs.length > 0) {
            console.log("user._id ==== ", this.state.userId)
            // const db = this.createFormData(this.state.selectedDocs);
            // console.log("data === ", db);
            this.props.uploadAttachment(
                this.createFormData(this.state.selectedDocs),
                this.state.userId,
                (data) => {
                    this.updateProfileApiCall(data)
                }
            )
        } else {
            this.updateProfileApiCall()
        }
    };

    showAlert = () => {
        alert("data")
    }

    createFormData = (docs) => {
        methodLog("createFormData")
        let imgName = "";
        const data = new FormData();
        docs.map((photo, index) => {
            if (photo.fileName === undefined) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else if (photo.fileName === null) {
                let getFilename = photo.uri.split("/");
                imgName = getFilename[getFilename.length - 1];
            } else {
                imgName = photo.fileName;
            }
            let newFile = {
                name: Platform.OS === 'android' ? photo.name : imgName,
                type: photo.type,
                uri: Platform.OS === 'android' ? photo.uri : photo.uri.replace('file://', ''),
            }
            data.append("multi-files", newFile);
        })

        // alert("form data " + JSON.stringify(data))
        return data;
    };

    _removeDocFunc = (indexOfDoc) => {
        const newArr = this.state.selectedDocs;
        if (newArr.length > 0) {
            newArr.splice(indexOfDoc, 1);
            console.log(newArr + indexOfDoc);
            this.setState({
                selectedDocs: newArr
            })
        }
    }

    render() {
        const { fullName, email, emailError, mobileError, mobile, gender, city, dob, isDatePickerVisible, medical, checked, lastVisited, referenceCode } = this.state;
        return (
            <Container>
                <HeaderSehet headerText={strings.patient_details} navigation={this.props.navigation} />
                <Content style={styles.container}>
                    <Form>
                        <Item floatingLabel>
                            <Label>{strings.full_name} *</Label>
                            <Input
                                style={styles.marginItem}
                                value={fullName}
                                onChangeText={(value) =>
                                    this._onChangeValue("fullName", value)
                                }
                            />
                        </Item>
                        <Item
                            floatingLabel
                            error={emailError !== null}>
                            <Label>{strings.email} *</Label>
                            <Input
                                disabled={this.state.msgMode == "EMAIL" ? true : false}
                                value={email}
                                style={styles.marginItem}
                                onChangeText={(value) =>
                                    this._onChangeValue("email", value)
                                }
                                onBlur={() => this._onBlur("email")}
                            />
                        </Item>

                        <Item floatingLabel error={mobileError !== null} style={{ marginTop: 10 }}>
                            <Label>{strings.Mobile_no} *</Label>
                            <Input
                                disabled={this.state.msgMode == "MOBILE" ? true : false}
                                keyboardType="numeric"
                                value={mobile}
                                returnKeyType="done"
                                maxLength={10}
                                style={styles.marginItem}
                                onChangeText={(value) => {
                                    this._onChangeValue("mobile", value)
                                    this._checkMobileValid()
                                }}
                                onBlur={() => this._checkMobileValid()}
                            />
                        </Item>

                        <Item
                            floatingLabel
                        // error={emailError !== null}
                        >
                            <Label>{strings.city} *</Label>
                            <Input
                                value={city}
                                style={styles.marginItem}
                                onChangeText={(value) =>
                                    this._onChangeValue("city", value)
                                }
                                onBlur={() => this._onBlur("city")}
                            />
                        </Item>

                        <View style={{ marginLeft: "4.2%" }}>
                            <View style={styles.genderPicker}>
                                <Label style={{ flex: 0.8, fontSize: 17, color: '#615e5e', fontFamily: "Roboto" }}>{strings.gender}
                                    *</Label>
                                <Picker
                                    mode="dropdown"
                                    iosIcon={<Icon name="keyboard-arrow-down" type="MaterialIcons" />}
                                    style={{ flex: 2, padding: 0, margin: 0, }}
                                    selectedValue={gender}
                                    onValueChange={(value) => {
                                        if (value != "0")
                                            this._onChangeValue("gender", value)
                                    }}>
                                    <Picker.Item label="Select" value="0" />
                                    <Picker.Item label="Male" value="MALE" />
                                    <Picker.Item label="Female" value="FEMALE" />
                                    <Picker.Item label="Prefer Not to Say" value="Prefer Not to Say" />
                                    <Picker.Item label="Not Applicable" value="NOT APPLICABLE" />
                                </Picker>
                            </View>
                            <Line lineColor={"#A0A9BE"} />
                        </View>


                        <Item
                            floatingLabel
                            onPress={() => this.toggleDatePicker()}>
                            <Label>{strings.dob}</Label>
                            <Input
                                style={styles.marginItem}
                                value={
                                    dob.toString() == ""
                                        ? ""
                                        : moment(dob)
                                            .format("DD MMMM YYYY")
                                            .toString()
                                }
                                disabled />
                            <Icon
                                name="calendar"
                                type="Feather"
                                style={styles.calIcon} />
                        </Item>
                        {<DateTimePickerModal
                            isVisible={isDatePickerVisible}
                            mode="date"
                            onConfirm={this.handleConfirm}
                            onCancel={this.toggleDatePicker}
                            maximumDate={new Date()}
                        />}

                        <Item
                            floatingLabel>
                            <Label>{strings.medical_description} </Label>
                            <Input
                                value={medical}
                                returnKeyType="done"
                                style={styles.marginItem}
                                numberOfLines={2}
                                // multiline={true}
                                onChangeText={(value) =>
                                    this._onChangeValue("medical", value)
                                } />
                        </Item>


                        <Item
                            floatingLabel>
                            <Label>{strings.name_of_last_doc}</Label>
                            <Input
                                style={styles.marginItem}
                                value={lastVisited}
                                onChangeText={(value) =>
                                    this._onChangeValue("lastVisited", value)
                                } />
                        </Item>

                        <Item
                            floatingLabel>
                            <Label>{strings.reference_code}</Label>
                            <Input
                                style={styles.marginItem}
                                value={referenceCode}
                                autoCapitalize="characters"
                                onChangeText={(value) =>
                                    this._onChangeValue("referenceCode", value)
                                } />
                        </Item>

                        <Item>
                            <View style={{ marginVertical: 10 }}>
                                <Label style={{ marginVertical: 15, }}>{"Upload reports if required\n(only pdf, jpg)"}</Label>
                                <View style={{ flexDirection: 'row' }}>
                                    <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                                        <TouchableOpacity onPress={this._handleSelectDoc}>
                                            <Image
                                                source={
                                                    this.state.selectedImage != null
                                                        ? { uri: this.state.selectedImage.uri }
                                                        : Images.uploadImage
                                                }
                                                style={{ height: 100, width: 100, borderRadius: 15 }}
                                            />
                                        </TouchableOpacity>
                                        {
                                            this.state.selectedDocs.map((doc, index) => (
                                                <View>
                                                    {
                                                        doc.type === "image/jpeg" || doc.type === "image/png"
                                                            ? <Image
                                                                source={
                                                                    doc.uri != null && doc.type === "image/jpeg"
                                                                        ? { uri: doc.uri }
                                                                        : Images.uploadImage
                                                                }
                                                                style={{ height: 100, width: 100, borderRadius: 15, marginHorizontal: 10 }}
                                                            />
                                                            : <View style={{ backgroundColor: '#e9eef1', justifyContent: 'center', height: 100, width: 100, borderRadius: 15, marginHorizontal: 10 }}>
                                                                <Text style={{ textAlign: 'center', fontSize: 14, fontWeight: 'bold' }}>{doc.type + ' file'}</Text>
                                                            </View>
                                                    }
                                                    <TouchableOpacity
                                                        onPress={() => this._removeDocFunc(index)}
                                                        style={{ position: 'absolute', right: 0 }}
                                                    >
                                                        <Icon
                                                            name="circle-with-cross"
                                                            type="Entypo"
                                                            style={{
                                                                color: 'red',
                                                                // backgroundColor: 'white'
                                                            }}
                                                        />
                                                    </TouchableOpacity>
                                                </View>
                                            ))
                                        }
                                    </ScrollView>
                                </View>
                            </View>
                        </Item>

                    </Form>
                    <View style={styles.checkboxItem}>
                        <View
                            style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <CheckBox
                                checked={checked}
                                style={{}}
                                onPress={this.onChecked}
                                onAnimationType="bounce"
                                offAnimationType="bounce"
                                onTintColor="#1B80F3"
                            />
                            <Text style={styles.mainText} onPress={() => this.onChecked()}>
                                <Text style={styles.tAndCText}>By continuing, you agree
                                    that you have read and
                                    accept our </Text>
                                <Text style={styles.tAndCLink} onPress={this.onModal1}> T&C , </Text>
                                <Text style={styles.tAndCLink} onPress={this.onModal2}> Privacy Policy.</Text>
                                <Text style={styles.tAndCLink} onPress={this.onModal3}> Patient Consent.</Text>
                                <Text style={styles.tAndCText}> and</Text>
                                <Text style={styles.tAndCLink} onPress={this.onModal4}> Refund Policy.</Text>
                            </Text>
                        </View>
                    </View>

                    <TAndCCommon visible={this.state.modal1} onPress={() => this.onModal1()} />
                    <PrivacyPolicyCommon visible={this.state.modal2} onPress={() => this.onModal2()} />
                    <PatientConsent visible={this.state.modal3} onPress={() => this.onModal3()} />
                    <Refund visible={this.state.modal4} onPress={() => this.onModal4()} />

                    <Button
                        onPress={() => this._onContinuePress()}
                        block
                        disabled={
                            ifEmpty(fullName) ||
                                ifEmpty(email) ||
                                ifEmpty(mobile) ||
                                ifEmpty(gender) ||
                                // ifEmpty(dob) ||
                                emailError != null ||
                                checked == false
                                ? true : false
                        }
                        style={
                            ifNotEmpty(fullName) &&
                                ifNotEmpty(email) &&
                                ifNotEmpty(mobile) &&
                                ifNotEmpty(gender) &&
                                // ifNotEmpty(dob) &&
                                checked != false &&
                                emailError == null &&
                                mobileError == null
                                ? styles.danger
                                : styles.disabled
                        }
                    >
                        <Text uppercase={false}>{strings.continue}</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}


const mapStateToProps = (state) => ({
    upcomingBookings: state.bookings.upcomingBookings,
    userProfile: state.profile.userProfile,
    selectedSpeciality: state.specialitiesReducer.selectedSpeciality,
    emailVerified: state.userReducer.verifiedEmail,
    mobileVerified: state.userReducer.verifiedMobile,
    selectedSlotObj: state.appointment.selectedSlotObj,
    selectedSlotObjForQuickBook: state.appointment.selectedSlotObjForQuickBook,
    followUpData: state.appointment.followUpData,
    appointmentType: state.appointment.appointmentType,
});

export default connect(
    mapStateToProps,
    { showModal, signUpCall, postPatientDetails, show, hide, postDoctorSlotFlow1, postDoctorQuickBook, uploadAttachment, followUpPost, reschedulePost }
)(PatientDetails);
