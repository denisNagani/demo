// import {create} from 'apisauce'
//
// let baseUrl = "";
// let token = ""
//
// const api = create({
//     baseUrl: baseUrl,
//     headers: {
//         Accept: "application/json",
//         Authorization: "Bearer " + token
//     },
// });
//
// export let order_req = (body) =>
//     api.post('/prepareOrder', body)
//         .then(response => response);
//
// export let checksum_req = (body) =>
//     api.post('/getChecksum', body)
//         .then(response => response);
//
//
// import React, {Component} from "react";
// import {ActivityIndicator, Alert, Button, Platform, Text, View} from "react-native";
// import {Container,} from "native-base";
// import moment from "moment";
//
// import styles from "./style";
// import {bindActionCreators} from "redux";
// import {getUpcomingBookingsByDate} from "../../../actions/bookings";
// import {getRefToken} from "../../../services/auth";
// import {getRefreshTokenDash} from "../../../actions/profile";
// import {methodLog, recordError} from "../../../utils/fireLog";
// import Paytm from '@philly25/react-native-paytm';
// import {connect} from "react-redux";
// import {checksum_req, order_req} from "./apirequest";
// import {getUpcomingBookingsAdmin} from "../../../actions/bookingsNurse";
//
// // Data received from PayTM
// const paytmConfig = {
//     MID: 'IbNhtN60791212616340',
//     WEBSITE: 'WEBSTAGING',
//     CHANNEL_ID: 'WAP',
//     INDUSTRY_TYPE_ID: 'Retail',
//     CALLBACK_URL: 'https://securegw.paytm.in/theia/paytmCallback?ORDER_ID='
// };
//
// class DashboardScreen extends Component {
//
//
//     constructor(props) {
//         super(props);
//         this.state = {
//             currentDate: moment(new Date()).format("DD MMM YYYY"),
//             name: "",
//             email: "",
//             mobile: "",
//             dob: "",
//             appointmentDate: "",
//             gender: 'Select',
//             slots: [],
//             selectedSlot: '',
//             isPickerVisibleDob: false,
//             isPickerVisibleAppointment: false,
//             visible: false,
//             processing: false,
//             payment_text: "Requesting please wait..."
//         };
//     }
//
//     componentWillMount() {
//         Paytm.addListener(Paytm.Events.PAYTM_RESPONSE, this.onPayTmResponse);
//     }
//
//     componentWillUnmount() {
//         Paytm.removeListener(Paytm.Events.PAYTM_RESPONSE, this.onPayTmResponse);
//     }
//
//     onPayTmResponse = (resp) => {
//         const {STATUS, status, RESPMSG, response} = resp;
//
//         if (Platform.OS === 'ios') {
//             if (status === 'Success') {
//                 const jsonResponse = JSON.parse(response);
//                 const {STATUS} = jsonResponse;
//
//                 if (STATUS && STATUS === 'TXN_SUCCESS') {
//                     // Payment succeed!
//                     // Payment succeed!
//                     Alert.alert('Transaction successful', 'we have added money to your ac')
//                     this.setState({payment_text: RESPMSG})
//                 } else {
//                     Alert.alert('Transaction Failed', '..')
//                 }
//             }
//         } else {
//             if (STATUS && STATUS === 'TXN_SUCCESS') {
//                 // Payment succeed!
//                 // Payment succeed!
//                 Alert.alert('Transaction successful', 'we have added money to your ac')
//                 this.setState({payment_text: RESPMSG})
//             } else {
//                 Alert.alert('Transaction Failed', '..')
//             }
//         }
//     };
//
//     runTransaction(amount, customerId, orderId, mobile, email, checkSum, mercUnqRef) {
//         const callbackUrl = `${paytmConfig.CALLBACK_URL}${orderId}`;
//         const details = {
//             mode: 'Staging', // 'Staging' or 'Production'
//             MID: paytmConfig.MID,
//             INDUSTRY_TYPE_ID: paytmConfig.INDUSTRY_TYPE_ID,
//             WEBSITE: paytmConfig.WEBSITE,
//             CHANNEL_ID: paytmConfig.CHANNEL_ID,
//             TXN_AMOUNT: `${80}`, // String
//             ORDER_ID: orderId, // String
//             EMAIL: email, // String
//             MOBILE_NO: mobile, // String
//             CUST_ID: customerId, // String
//             CHECKSUMHASH: checkSum, //From your server using PayTM Checksum Utility
//             CALLBACK_URL: callbackUrl,
//             MERC_UNQ_REF: mercUnqRef, // optional
//         };
//         Paytm.startPayment(details);
//     }
//
//     resetState = () => {
//         this.setState({
//             name: "",
//             email: "",
//             mobile: "",
//             dob: "",
//             appointmentDate: "",
//             gender: 'Select',
//             slots: [],
//             selectedSlot: '',
//             isPickerVisibleDob: false,
//             isPickerVisibleAppointment: false,
//         })
//     }
//
//     getToken = async () => { 
//         let token = await getRefToken();
//         console.log(token);
//         this.props.getRefreshTokenDash(token);
//     };
//
//     nextDate = () => {
//         methodLog("nextDate")
//         try {
//             const apiDate = moment(this.state.currentDate)
//                 .add(1, "day")
//                 .format("YYYY-MM-DD");
//             console.log(apiDate);
//             const date = moment(this.state.currentDate)
//                 .add(1, "day")
//                 .format("DD MMM YYYY");
//             this.setState({
//                 currentDate: date,
//             });
//             this.props.getUpcomingBookingsByDate(apiDate);
//         } catch (e) {
//             recordError(e)
//         }
//     };
//
//     prevDate = () => {
//         methodLog("prevDate")
//         try {
//             const apiDate = moment(this.state.currentDate)
//                 .subtract(1, "day")
//                 .format("YYYY-MM-DD");
//             console.log(apiDate);
//             const date = moment(this.state.currentDate)
//                 .subtract(1, "day")
//                 .format("DD MMM YYYY");
//             this.setState({
//                 currentDate: date,
//             });
//             this.props.getUpcomingBookingsByDate(apiDate);
//         } catch (e) {
//             recordError(e)
//         }
//     };
//
//     onShareClick = async () => {
//         this.props.showAppointmentModal(!this.props.appointmentModal)
//     };
//
//     handleConfirmDob = (date) => {
//         methodLog("handleConfirmDob")
//         try {
//             const dob = moment(date).format("YYYY-MM-DD");
//             this.setState({
//                 dob: "  ",
//                 isPickerVisibleDob: false,
//             }, () => {
//                 this.setState({
//                     dob,
//                     isPickerVisibleDob: false,
//                 })
//             });
//         } catch (e) {
//             recordError(e)
//         }
//     };
//
//     handleConfirmAppointment = (date) => {
//         methodLog("handleConfirmAppointment")
//         try {
//             let appointmentDate = moment(date).format("YYYY-MM-DD");
//             let time = moment().format("HH:mm:ss");
//             let dateTime = moment(appointmentDate + " " + time).format();
//
//             this.setState({
//                 appointmentDate: appointmentDate,
//                 isPickerVisibleAppointment: !this.state.isPickerVisibleAppointment,
//             }, () => {
//                 this.props.getShareRoomSlots(this.props.userProfile.userId._id, dateTime)
//             });
//         } catch (e) {
//             recordError(e)
//         }
//
//     };
//
//     setVisibility = () => {
//         this.props.hideAppointmentModal()
//     };
//
//     hitchecksum = (orderId) => {
//         checksum_req(
//             {
//                 CUST_ID: 'Customer1',
//                 CHANNEL_ID: 'WAP',
//                 ORDER_ID: orderId,
//                 TXN_AMOUNT: "850",
//                 MID: 'IbNhtN60791212616340',
//                 WEBSITE: 'WEBSTAGING',
//                 INDUSTRY_TYPE_ID: 'Retail',
//                 CALLBACK_URL: 'https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=' + orderId
//             }
//         ).then(responseJson => {
//             console.log("checksum_req", responseJson)
//             if (responseJson.ok) {
//                 this.setState({processing: false}, () => {
//                     const details = {
//                         mode: 'Staging',
//                         MID: 'IbNhtN60791212616340',
//                         WEBSITE: 'WEBSTAGING',
//                         CHANNEL_ID: 'WAP',
//                         ORDER_ID: orderId,
//                         TXN_AMOUNT: "850",
//                         INDUSTRY_TYPE_ID: 'Retail',
//
//                         CUST_ID: 'Customer1',
//                         CHECKSUMHASH: responseJson.data.checksum,
//                         CALLBACK_URL: 'https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=' + orderId
//                     };
//                     Paytm.startPayment(details)
//                 })
//             }
//
//
//
//
//         })
//     };
//
//     onPress = () => {
//         this.setState({processing: true})
//         order_req({"orderItems": '20'})
//             .then(responseJson => {
//                 console.log("order_req", responseJson)
//                 if (responseJson.ok) {
//                     this.setState({processing: false}, () => {
//                         this.hitchecksum(responseJson.data.order_gatway_id)
//                     })
//                 }
//             })
//     };
//
//     joinMyRoom = () => {
//         methodLog("joinMyRoom")
//         try {
//             let userProfile = this.props.userProfile;
//             if (userProfile === undefined ||
//                 userProfile.userId === undefined ||
//                 userProfile.userId.roomName === null) {
//                 const payload = {
//                     text: "Room url not found",
//                     subText: "",
//                     iconName: "closecircleo",
//                     modalVisible: true,
//                     iconColor: "#C70315",
//                 };
//                 this.props.showModal(payload);
//                 return;
//             }
//             this.props.setCallHeader("My Room");
//             this.props.navigation.navigate("JoinRoom", {
//                 channel: userProfile.userId.roomName,
//             })
//         } catch (e) {
//             recordError(e)
//         }
//     }
//
//     render() {
//         return (
//             <Container>
//                 <Button title={"Pay securly"} onPress={() => this.onPress()}/>
//                 <View style={styles.patientView}>
//                     <Text>{this.state.processing}</Text>
//                     {this.state.processing ?
//                         <ActivityIndicator/> : null}
//                     <Text>{this.state.payment_text}</Text>
//                 </View>
//             </Container>
//         );
//     }
// }
//
// const mapDispatchToProps = (dispatch) =>
//     bindActionCreators(
//         {
//             getUpcomingBookingsByDate,
//             getRefreshTokenDash,
//             getUpcomingBookingsAdmin,
//         },
//         dispatch
//     );
//
// const mapStateToProps = (state) => ({
//     upcomingBookingsByDate: state.bookings.upcomingBookingsByDate,
// });
//
// export default connect(
//     mapStateToProps,
//     mapDispatchToProps
// )(DashboardScreen);