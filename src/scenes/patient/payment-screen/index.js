import React, { useEffect } from "react";
import { NativeModules, TouchableOpacity, Platform } from "react-native";
import { Container, Text, } from "native-base";
import { useDispatch } from "react-redux";
import { successLog } from "../../../utils/fireLog";
import { showModal } from "../../../actions/modal";
import axios from "axios";
import {base_url} from "../../../config";

const AllInOneSDKManager = NativeModules.AllInOneSDKManager;
var AllInOneSDKPlugin = NativeModules.AllInOneSDKSwiftWrapper;


const PaymentScreen = (props) => {
    const dispatch = useDispatch();


    useEffect(() => {
        successLog("patient home screen loaded")
        return () => {
        };
    }, []);

    // const getToken = async () => {
    //     let token = await getRefToken();
    //     dispatch(getRefreshTokenHome(token, props.navigation));
    // };

    const PayNow = async () => {
        let random = Math.random()
        let orderId = JSON.stringify(random)

        axios.post(base_url + "createPaytmTxnToken", {
            custId: "123",
            custMobile: "8605385040",
            orderId: orderId,
            txnAmount: "5.00",
            currency: "INR"
        }, dispatch)
            .then((res) => {
                const payload = {
                    text: JSON.stringify(res.data.data.body),
                    subText: "",
                    iconName: "checkcircleo",
                    modalVisible: true,
                };
                // dispatch(showModal(payload))
                if (Platform.OS === "android") {
                    AllInOneSDKManager.startTransaction(orderId, "oirnGE67266315619909", res.data.data.body.txnToken, "5.00",
                     " https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId, true, displayResult);

                } else {
                    console.log("-------------------------open Native--------", NativeModules);
                    let result = AllInOneSDKPlugin.openPaytm(
                        "oirnGE67266315619909",
                        orderId,
                        res.data.data.body.txnToken,
                        "5.00",
                        " https://securegw-stage.paytm.in/theia/paytmCallback?ORDER_ID=" + orderId, true
                    );
                }

            })
            .catch((err) => {
            });
    };
    const displayResult = (result) => {
        const payload = {
            text: result,
            subText: "",
            iconName: "checkcircleo",
            modalVisible: true,
        };
        dispatch(showModal(payload))
    }


    return (
        <Container style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>

            <TouchableOpacity
                style={{
                    alignItems: "center",
                    justifyContent: "center",
                }}>
                <Text onPress={() => PayNow()} adjustsFontSizeToFit>Pay now</Text>
            </TouchableOpacity>

        </Container>
    );
};
export default PaymentScreen;
