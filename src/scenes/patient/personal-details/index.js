import React, {Component} from "react";
import {Text, TouchableOpacity} from "react-native";
import {
	Button,
	Container,
	Content,
	DatePicker,
	Footer,
	Form,
	Icon,
	Input,
	Item,
	Label,
	Picker,
	View,
} from "native-base";
import styles from "./style";

export default class PatientPersonalDetails extends Component {
    render() {
        return (
            <Container>
                <View style={styles.header}>
                    <View style={styles.heading}>
                        <TouchableOpacity onPress={() => this.props.navigation.goBack()}>
                            <Icon name="ios-arrow-back" style={styles.headerBackIcon}/>
                        </TouchableOpacity>
                        <Text style={styles.headerText}>Patient Personal Details</Text>
                    </View>
                    <TouchableOpacity
                        onPress={() =>
                            this.props.navigation.navigate("PatientInsuranceDetails")
                        }
                    >
                        <Text style={styles.skip}>Skip</Text>
                    </TouchableOpacity>
                </View>
                <Content style={[styles.content]}>
                    <Form>
                        <View style={[styles.formGroup]}>
                            <Item floatingLabel style={[styles.firstName, styles.input]}>
                                <Label style={styles.label}>First Name*</Label>
                                <Input/>
                            </Item>
                            <Item floatingLabel style={[styles.lastName, styles.input]}>
                                <Label style={styles.label}>Last Name*</Label>
                                <Input placeholder="Last Name"/>
                            </Item>
                        </View>

                        <View style={styles.formGroup}>
                            <Item floatingLabel style={[styles.email, styles.input]}>
                                <Label style={styles.label}>Email*</Label>
                                <Input/>
                            </Item>
                        </View>

                        <View style={styles.formGroup}>
                            <Item floatingLabel style={[styles.email, styles.input]}>
                                <Label style={styles.label}>Mobile Number*</Label>
                                <Input/>
                            </Item>
                        </View>

                        <View style={styles.formGroup}>
                            <Item floatingLabel style={[styles.email, styles.input]}>
                                <Label style={styles.label}>Sexual Orientation</Label>
                                <Input/>
                            </Item>
                        </View>

                        <View style={[styles.formGroup]}>
                            <Item picker style={[styles.genderPicker, styles.input]}>
                                <Picker
                                    mode="dropdown"
                                    style={styles.pickerItem}
                                    placeholder="Gender"
                                >
                                    <Picker.Item label="Gender*" value=""/>
                                    <Picker.Item label="Male" value="male"/>
                                    <Picker.Item label="female" value="female"/>
                                </Picker>
                            </Item>
                            <Item style={[styles.datePicker, styles.input]}>
                                <DatePicker
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    placeHolderText="Date of birth*"
                                    textStyle={{color: "gray"}}
                                    placeHolderTextStyle={{color: "gray"}}
                                    disabled={false}
                                />
                            </Item>
                        </View>

                        <View style={[styles.formGroup]}>
                            <Item floatingLabel style={[styles.firstName, styles.input]}>
                                <Label style={styles.label}>Mother's Name</Label>
                                <Input/>
                            </Item>
                            <Item floatingLabel style={[styles.lastName, styles.input]}>
                                <Label style={styles.label}>Father's Name</Label>
                                <Input/>
                            </Item>
                        </View>

                        <View style={[styles.formGroup]}>
                            <Item style={[styles.racePicker, styles.input]}>
                                <Picker mode="dropdown" style={styles.pickerItem}>
                                    <Picker.Item label="Race" value=""/>
                                    <Picker.Item label="One" value="one"/>
                                    <Picker.Item label="Two" value="two"/>
                                </Picker>
                            </Item>
                            <Item style={[styles.ethentiCityPicker, styles.input]}>
                                <Picker mode="dropdown" style={styles.pickerItem}>
                                    <Picker.Item label="Ethenticity" value=""/>
                                    <Picker.Item label="One" value="one"/>
                                    <Picker.Item label="Two" value="two"/>
                                </Picker>
                            </Item>
                        </View>

                        <View style={[styles.formGroup]}>
                            <Item style={[styles.languagePicker, styles.input]}>
                                <Picker mode="dropdown" style={styles.pickerItem}>
                                    <Picker.Item label="Language" value=""/>
                                    <Picker.Item label="One" value="one"/>
                                    <Picker.Item label="Two" value="two"/>
                                </Picker>
                            </Item>
                            <Item floatingLabel style={[styles.socialSecurity, styles.input]}>
                                <Label style={styles.label}>Social security #</Label>
                                <Input/>
                            </Item>
                        </View>
                    </Form>
                </Content>
                <Footer style={styles.footer}>
                    <Button
                        style={[styles.footerBtn]}
                        block
                        danger
                        onPress={() =>
                            this.props.navigation.navigate("PatientInsuranceDetails")
                        }
                    >
                        <Text style={styles.btnText}>Continue</Text>
                    </Button>
                </Footer>
            </Container>
        );
    }
}
