import { StyleSheet } from "react-native";

export default (styles = StyleSheet.create({
	header: {
		height: 100,
		backgroundColor: "#1E2081",
		flexDirection: "row",
		justifyContent: "space-between",
		alignItems: "center",
	},
	heading: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		marginLeft: 20,
		marginTop: 20,
	},
	headerText: {
		color: "#fff",
		fontSize: 18,
		marginLeft: 20,
	},
	headerBackIcon: {
		fontSize: 35,
		color: "#fff",
	},
	skip: {
		marginRight: 20,
		color: "#1B80F3",
		marginTop: 20,
	},
	content: {
		padding: "5%",
	},
	formGroup: {
		flex: 1,
		flexDirection: "row",
		padding: "2%",
	},

	input: {
		padding: "2%",
	},
	label: {
		color: "gray",
	},
	pickerItem: {
		color: "gray",
	},
	firstName: {
		flex: 0.5,
	},

	lastName: {
		flex: 0.5,
		marginLeft: 20,
	},

	email: {
		flex: 1,
	},
	genderPicker: {
		flex: 0.5,
	},
	continueBtn: {
		width: "100%",
		marginBottom: 30,
	},
	btnText: {
		color: "#fff",
		fontSize: 20,
	},
	datePicker: {
		flex: 0.5,
		marginLeft: 20,
	},
	racePicker: {
		flex: 0.5,
	},
	ethentiCityPicker: {
		flex: 0.5,
		marginLeft: 20,
	},
	languagePicker: {
		flex: 0.5,
	},
	socialSecurity: {
		flex: 0.5,
		marginLeft: 20,
	},

	footerBtn: {
		flex: 1,
		margin: 15,
		justifyContent: "center",
		fontSize: 20,
	},
	footer: {
		height: 70,
		backgroundColor: "#fff",
	},
}));
