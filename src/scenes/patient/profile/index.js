import React, { Component } from "react";
import { Image, SafeAreaView, Text } from "react-native";
import { Container, Content, Form, Icon, Input, Item, Label, View, } from "native-base";
import styles from "./style";
import Images from "../../../assets/images/index";
import { TouchableOpacity } from "react-native-gesture-handler";
import moment from "moment";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { getUserDetails } from "../../../actions/profile";
import { successLog } from "../../../utils/fireLog";
import { ifNotValid, ifValid, isValid } from "../../../utils/ifNotValid";
import common from "../../../styles/common";
import { Capitalize } from "../../../utils/helper";
import { strings } from "../../../utils/translation";

class PatientProfileScreen extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        successLog("PatientProfileScreen screen loaded")
        this.props.getUserDetails()
    }


    render() {

        const user = ifValid(this.props.userProfile) ? ifValid(this.props.userProfile.userId) ? this.props.userProfile.userId : {} : {};
        const mobileNo = ifNotValid(user.mobileNo) ? "-" : user.mobileNo;
        const dob = isValid(user.dob) ? moment(user.dob).format("DD MMMM YYYY") : '-';
        const gender = ifNotValid(user.gender) ? '-' : Capitalize(user.gender.toLowerCase());
        const city = ifNotValid(user?.city) ? '-' : user?.city;


        return (
            <Container>
                <SafeAreaView style={[styles.header, { backgroundColor: '#141E46' }]}>
                    <TouchableOpacity
                        transparent
                        onPress={() => this.props.navigation.goBack()}
                    >
                        <Icon
                            name="arrowleft"
                            type="AntDesign"
                            style={[styles.headerBackIcon, { color: '#FFF' }]}
                        />
                    </TouchableOpacity>
                </SafeAreaView>
                <Content style={{ backgroundColor: '#F9F9F9' }}>
                    <View style={[styles.inHeader, { backgroundColor: '#141E46' }]}>
                        <View style={styles.section1}>
                            <Image
                                source={
                                    user.imageUrl != null ? { uri: user.imageUrl } : Images.user
                                }
                                style={styles.userImage}
                            />
                        </View>
                        <View style={styles.section2}>
                            <Text style={[styles.headerTitle, { color: '#FFF' }]}>{user.fullName}</Text>
                            <Text style={[common.subTitleProfile, { color: '#FFF' }]}>{ifValid(user.email) ? user.email : ''}</Text>
                        </View>
                        <View style={styles.section3}>
                            <Icon
                                style={[styles.editIcon, { color: '#FFF' }]}
                                name="edit-2"
                                type="Feather"
                                onPress={() => {
                                    this.props.navigation.navigate("EditPatientProfileScreen");
                                }}
                            />
                        </View>
                    </View>
                    <Form style={styles.formItem}>
                        <Item stackedLabel>
                            <Label>{strings.full_name}</Label>
                            <Input value={user.fullName} disabled={true} />
                        </Item>

                        <Item stackedLabel>
                            <Label>{strings.email}</Label>
                            <Input value={user.email} disabled={true} />
                        </Item>

                        <Item stackedLabel>
                            <Label>{strings.dob}</Label>
                            <Input value={dob} disabled={true} />
                        </Item>

                        <Item stackedLabel>
                            <Label>{strings.Mobile_no}</Label>
                            <Input value={mobileNo} disabled={true} />
                        </Item>

                        <Item stackedLabel>
                            <Label>{strings.gender}</Label>
                            <Input value={gender} disabled={true} />
                        </Item>

                        <Item stackedLabel>
                            <Label>{strings.city}</Label>
                            <Input value={city} disabled={true} />
                        </Item>

                        {/*<Text*/}
                        {/*    style={styles.changePass}*/}
                        {/*    onPress={() => {*/}
                        {/*        this.props.navigation.navigate("ChangePasswordScreen");*/}
                        {/*    }}*/}
                        {/*>*/}
                        {/*    Change Password*/}
                        {/*</Text>*/}
                    </Form>
                </Content>
            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators({ getUserDetails }, dispatch);

const mapStateToProps = (state) => ({
    userProfile: state.profile.userProfile,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PatientProfileScreen);
