import { StyleSheet } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";
export default (styles = StyleSheet.create({
	header: {
		backgroundColor: "#FFFFFF",
		flex: 0.09,
	},
	inHeader: {
		flexDirection: "row",
		backgroundColor: '#FFFFFF',
		height: hp("13%"),
		paddingLeft: wp("5%"),
		alignItems: "center",
		shadowColor: '#0000000B',
		shadowOffset: { width: 0, height: 3 },
		shadowOpacity: 1,
		shadowRadius: 5,
		elevation: 5
	},
	headerBackIcon: {
		fontSize: hp("3.5%"),
		marginTop: wp("5%"),
		marginLeft: wp("7%"),
		color: "#041A32"
	},
	headerTitle: {
		fontSize: 17,
		color: "#041A32",
		fontWeight: "bold",
	},
	headerView: {
		flexDirection: "row",
		justifyContent: "space-between",
	},
	editIcon: {
		color: "#041A32",
	},
	section1: {
		flex: 0.2,
	},
	section2: {
		flex: 0.65,
	},
	section3: {
		flex: 0.2,
	},
	pickerItem: {
		color: "gray",
	},
	continueBtn: {
		width: "100%",
		marginBottom: 30,
	},
	btnText: {
		color: "#fff",
		fontSize: 20,
	},
	formItem: {
		marginTop: hp("5%"),
		paddingHorizontal: wp("2%"),
	},
	changePass: {
		color: "#1B80F3",
		marginLeft: wp("4%"),
		marginBottom: 20,
	},
	textSecondary: {
		color: "#A0A9BE",
	},
	textSuccess: {
		color: "#56B732",
	},
	textWarning: {
		color: "#FF863B",
	},

	footerBtn: {
		flex: 0.5,
		margin: 10,
		justifyContent: "center",
		fontSize: 20,
	},
	cancelBtn: {
		backgroundColor: "#A0A9BE",
	},
	footer: {
		height: 70,
		backgroundColor: "#041A32",
	},
	footerBtnText: {
		color: "#041A32",
		fontSize: 15,
		fontWeight: "bold",
	},
	userImage: {
		width: 60,
		height: 60,
		borderRadius: 50,
	},
}));
