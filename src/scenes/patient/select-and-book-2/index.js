import React, { useEffect, useState } from "react";
import { Container, Text } from "native-base";
import { FlatList, Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { ListItem } from "react-native-elements";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../assets/images";
import { recordError, successLog } from "../../../utils/fireLog";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { FONT_FAMILY_SF_PRO_MEDIUM } from "../../../styles/typography";
import HeaderSearchBar from "../../../components/header-searchbar";
import { strings } from "../../../utils/translation";

const SelectAndBook2 = (props) => {
    const doctors = useSelector((state) => state.profile.docList);
    const [_doctors, setDoctors] = useState([{}, {}, {}]);
    const [docName, setDocName] = useState("");
    const dispatch = useDispatch();

    useEffect(() => {
        successLog("Select and book 2")
    }, []);

    useEffect(() => {
        try {
            if (doctors !== undefined && doctors !== null && doctors.length > 0) {

                if (docName.length > 0) {

                    let searchName = docName;

                    if (searchName.includes("Dr")) {
                        searchName = docName.replace("Dr", "").trim()
                    }
                    if (searchName.includes(".")) {
                        searchName = docName.replace(".", "").trim()
                        searchName = searchName.replace("Dr", "").trim()
                    }

                    let result = doctors.filter((item) => {
                        let name = item.fullName.toLowerCase();
                        let specialization =
                            item.specialization !== undefined && item.specialization !== null
                                ? item.specialization.toLowerCase()
                                : "";
                        return (
                            name.includes(searchName.toLowerCase()) ||
                            specialization.includes(searchName.toLowerCase())
                        );
                    });
                    setDoctors(result);
                } else setDoctors(doctors);
            }
        } catch (e) {
            recordError(e)
        }
    }, [doctors, docName]);

    const keyExtractor = (item, index) => index.toString();

    const renderItem = ({ item }) => {
        let name = item.name;
        item.fullName = "ARJUN MORE"
        if (name === undefined || name === null) {
            name = "Dyaneshwar "
        }
        let specialization = item.speciality;
        if (specialization === undefined || specialization === null) {
            specialization = "Pediatrician"
        }
        let imageUrl = item.imageUrl;
        // if (imageUrl === undefined || imageUrl === null) {
        imageUrl = "https://vignette.wikia.nocookie.net/cartoonhouse/images/5/54/Link.jpg/revision/latest?cb=20151016035216"
        // }

        return (
            <ListItem
                title={`${name}`}
                subtitle={specialization}
                leftAvatar={
                    item.imageUrl !== "" && item.imageUrl !== null ? (
                        {
                            source: { uri: imageUrl },
                            avatarStyle: { width: 44, height: 44, borderRadius: 22 },
                        }
                    ) : (
                            <View
                                style={{
                                    height: 44,
                                    width: 44,
                                    backgroundColor: "pink",
                                    borderRadius: 22,
                                    alignItems: "center",
                                    justifyContent: "center",
                                }}
                                avtar
                            >
                                <Text avtarTxt>
                                    {item.fullName
                                        .split(" ")
                                        .map((n) => n[0])
                                        .join("")}
                                </Text>
                            </View>
                        )
                }
                bottomDivider
                rightElement={() => (
                    <TouchableOpacity
                        style={styles.listBtn}
                        activeOpacity={1}
                        onPress={() =>
                            item.isAvailableToBook
                                ? props.navigation.navigate("BookAppointmentScreen", {
                                    doc: item,
                                })
                                : showCallClinic()
                        }
                    >
                        <Text style={styles.listBtnText}>Book</Text>
                    </TouchableOpacity>
                )}
            />
        );
    };

    const onChange = (value) => {
        try {
            let val = value.replace(/[^A-Za-z_ .]/gi, "");
            if (val === undefined || val === null)
                return
            setDocName(val);
        } catch (e) {
            recordError(e)
        }
    };


    return (
        <Container>
            <HeaderSearchBar
                navigation={props.navigation}
                headerText={strings.select_and_book}
                inputTextValue={docName}
                inputTextPlaceholder={strings.searchByDoctorName}
                onChange={(value) => onChange(value)} />

            {_doctors !== undefined && _doctors !== null && _doctors.length > 0 ? (
                <FlatList
                    scrollEnabled
                    keyExtractor={keyExtractor}
                    data={_doctors}
                    renderItem={(item) => renderItem(item)}
                />
            ) :
                docName !== undefined && docName !== null && docName.length > 0 && _doctors !== undefined && _doctors !== null && _doctors.length === 0 ?
                    (
                        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Image source={Images.notFound} style={{ width: 80, height: 85 }} />
                            <Text style={styles.notFound}>Search Failed!</Text>
                            <Text style={styles.notFoundDetails}>Try finding with better names to get the results</Text>
                        </View>
                    ) :
                    (
                        <View
                            style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Text>No results found</Text>
                        </View>
                    )
            }
        </Container>
    );
};
export default SelectAndBook2;

const styles = StyleSheet.create({
    header: {
        height: hp("25%"),
        backgroundColor: "#1E2081",
        justifyContent: "center",
        paddingHorizontal: 8,
    },
    heading: {
        flexDirection: "row",
        marginLeft: 18,
        alignItems: "center",
        justifyContent: "flex-start",
    },
    headerText: {
        color: "#fff",
        fontSize: hp("2.9%"),
        marginLeft: wp("4%"),
    },
    headerBackIcon: {
        fontSize: hp("3.5%"),
        color: "#fff",
    },
    headerSearch: {
        height: hp("6%"),
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: wp("4%"),
        marginTop: 20,
        marginHorizontal: "4%",
        backgroundColor: "rgba(255,255,255,0.2)",
        borderRadius: hp("1%"),
    },
    searchBarText: {
        color: "#ffffff",
        fontSize: hp("2%"),
    },
    headerSearchIcon: {
        fontSize: 24,
        marginRight: 10,
        color: "white",
    },
    resultsView: {
        marginTop: 19,
        marginLeft: 26,
    },
    resultsText: {
        color: "#041A32",
        fontSize: 17,
    },
    notFound: {
        color: "#041A32",
        fontSize: 18,
        marginBottom: 13,
        marginTop: 20,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    notFoundDetails: {
        color: "#A0A9BE",
        fontSize: 14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    listView: { marginTop: 20 },
    listBtn: {
        borderRadius: 10,
        borderWidth: 2,
        backgroundColor: "#fff",
        width: 60,
        borderColor: "#FF5E38",
        justifyContent: "center",
        alignItems: "center",
        height: 30,
    },
    listBtnText: { color: "#FF5E38" },
});
