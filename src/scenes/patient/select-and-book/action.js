import {
    AVAILABILITY_FILTER,
    FEMALE_CHECK, FILTER_CLEAR, GENDER_FILTER,
    GET_DOC_LIST,
    GET_DOC_LIST_TEMP,
    MALE_CHECk,
    SEARCH_DOCTOR_NAME,
    SPECIALITY_FILTER_SELECTED_LIST,
    SPECIALITY_FILTER_LIST,
    CITY_FILTER_SELECTED_LIST,
    FILTER_APPLIED,
    CITY_FILTER_LIST,
    EMPTY_DOC_LIST,
    FILTER_BODY
} from "../../../utils/constant";
import { errorLog, successLog } from "../../../utils/fireLog";
import { hide, show } from "../../../utils/loader/action";
import { ifValid, isValid } from "../../../utils/ifNotValid";
import http from "../../../services/http";

export const getDoctorsSlotByID = (slotId) => {
    return (dispatch) => {
        dispatch({ type: GET_DOC_LIST, payload: [] });
        dispatch(show());
        http.get(`users/doctors/${slotId}`, dispatch)
            .then((res) => {
                successLog("get doctor slots", res.status)
                if (ifValid(res.data)) {
                    let _output = res.data.map((obj) => {
                        let name = obj.fullName;
                        if (ifValid(obj) && ifValid(name))
                            return { ...obj, fullName: `${name}` }
                        else return obj;
                    })
                    dispatch({ type: GET_DOC_LIST, payload: _output });
                    dispatch({ type: GET_DOC_LIST_TEMP, payload: _output });
                }
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("get doctor slot", o)
                dispatch(hide());
            });
    };
};

export const getDoctorsSlotByTodayAvailability = (id) => {
    return (dispatch) => {
        dispatch({ type: GET_DOC_LIST, payload: [] });
        dispatch(show());
        http.get(`users/doctorsAvailableToday/${id}`, dispatch)
            .then((res) => {
                successLog("getDoctorsSlotByTodayAvailability ", res.status)
                if (ifValid(res.data)) {
                    let _output = res.data.map((obj) => {
                        let name = obj.fullName;
                        if (ifValid(obj) && ifValid(name))
                            return { ...obj, fullName: `${name}` }
                        else return obj;
                    })
                    dispatch({ type: GET_DOC_LIST, payload: _output });
                }
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("getDoctorsSlotByTodayAvailability", o)
                dispatch(hide());
            });
    };
};

export const filterListByName = (searchName, originalList) => {
    return (dispatch) => {
        if (searchName.length > 0) {
            dispatch(setFEMALE_GENDER(false))
            dispatch(setMALE_GENDER(false))

            if (searchName.includes("Dr")) {
                searchName = searchName.replace("Dr", "").trim()
            }
            if (searchName.includes(".")) {
                searchName = searchName.replace(".", "").trim()
                searchName = searchName.replace("Dr", "").trim()
            }

            let result = originalList.filter((item) => {
                let name = ifValid(item.fullName) ? item.fullName.toLowerCase() : ""
                let specialization = ifValid(item.specialization) ? item.specialization.toLowerCase() : "";
                return (
                    name.includes(searchName.toLowerCase()) ||
                    specialization.includes(searchName.toLowerCase())
                );
            });
            dispatch({ type: GET_DOC_LIST, payload: result });

        } else {
            dispatch({ type: GET_DOC_LIST, payload: originalList });
        }

    };
};

export const filterListByCity = (searchName, originalList) => {
    return (dispatch) => {
        if (searchName.length > 0) {
            let result = originalList.filter((item) => {
                let name = ifValid(item.name) ? item.name.toLowerCase() : ""
                return (
                    name.includes(searchName.toLowerCase())
                );
            });
            dispatch({ type: CITY_FILTER_SELECTED_LIST, payload: result });

        } else {
            dispatch({ type: CITY_FILTER_SELECTED_LIST, payload: originalList });
        }

    };
};

export const filterByGender = (gender, value, originalList) => {
    successLog(gender)
    return (dispatch) => {
        dispatch(setSearchDocName(''))
        dispatch({ type: GET_DOC_LIST, payload: [] });
        successLog('clearing list')
        let res = []
        if (ifValid(originalList) && originalList.length > 0 && value == true) {
            res = originalList.filter(obj => {
                successLog(obj.gender + "  " + gender)
                if (ifValid(obj.gender)) {
                    if (obj.gender == gender) return obj;
                }
            })
        } else {
            res = originalList
        }
        successLog('clearing list' + JSON.stringify(res))

        if (gender == 'MALE') {
            dispatch(setMALE_GENDER(value))
            dispatch(setFEMALE_GENDER(false))
        } else {
            dispatch(setFEMALE_GENDER(value))
            dispatch(setMALE_GENDER(false))
        }
        dispatch({ type: GET_DOC_LIST, payload: res });

    };
};


export const setMALE_GENDER = (payload) => {
    return {
        type: MALE_CHECk,
        payload: payload,
    };
};

export const setFEMALE_GENDER = (payload) => {
    return {
        type: FEMALE_CHECK,
        payload: payload,
    };
};

export const setSearchDocName = (payload) => {
    return {
        type: SEARCH_DOCTOR_NAME,
        payload: payload,
    };
};
///////// FILTER SCREEN
export const setSpecialityFilter = (type, payload) => {
    return {
        type: type,
        payload: payload
    }
};

export const updateSepcialitySelected = (payload) => {
    return {
        type: SPECIALITY_FILTER_SELECTED_LIST,
        payload: payload,
    };
};

export const updateCitySelected = (payload) => {
    return {
        type: CITY_FILTER_SELECTED_LIST,
        payload: payload,
    };
};


export const updateGenderFilter = (payload) => {
    return {
        type: GENDER_FILTER,
        payload: payload,
    };
};

export const updateAvailabilityFilter = (payload) => {
    return {
        type: AVAILABILITY_FILTER,
        payload: payload,
    };
};

export const clearFilter = (payload) => {
    return {
        type: FILTER_CLEAR,
        payload: payload,
    };
};

export const specialityFilter = (url, s_ids, gender, date, c_ids, fee, page, itemsPerPage) => {
    // console.log(" filter body ", { url, s_ids, gender, date, c_ids });
    let body = {
        date: date,
        ...(gender != "" && { gender: gender == "BOTH" ? "" : gender }),
        ...(s_ids?.length > 0 && { specialityId: s_ids }),
        ...(c_ids.length > 0 && { cities: c_ids }),
        ...(isValid(fee) > 0 && { fee: fee }),
        page: page,
        itemsPerPage: itemsPerPage,
    }
    return (dispatch) => {
        // dispatch({ type: EMPTY_DOC_LIST });
        dispatch(show());
        http.post(url, body)
            .then((res) => {
                successLog("get doctor slots", res.status)
                if (ifValid(res.data)) {
                    let _output = res.data.map((obj) => {
                        let name = obj.fullName;
                        if (ifValid(obj) && ifValid(name))
                            return { ...obj, fullName: `${name}` }
                        else return obj;
                    })
                    dispatch({ type: GET_DOC_LIST, payload: _output });
                    // dispatch({ type: GET_DOC_LIST_TEMP, payload: _output });
                }
                dispatch(hide());
            })
            .catch((o) => {
                errorLog("get doctor slot", o)
                dispatch(hide());
            });
    };
};

export const setFilterApplied = (payload) => {
    return {
        type: FILTER_APPLIED,
        payload: payload,
    };
}

export const setFilterBody = (payload) => {
    return {
        type: FILTER_BODY,
        payload: payload,
    };
}

export const getDoctorsBySearch = (url) => {
    return (dispatch) => {
        dispatch(show());
        http.get(url, dispatch)
            .then((res) => {
                if (ifValid(res?.data)) {
                    dispatch({ type: GET_DOC_LIST, payload: res?.data });
                    // dispatch({ type: GET_DOC_LIST_TEMP, payload: _output });
                }
                dispatch(hide());
            })
            .catch((o) => {
                dispatch(hide());
            });
    };
};