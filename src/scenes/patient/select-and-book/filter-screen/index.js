import React, { useEffect, useRef, useState } from "react";
import { Button, Content, Footer, Icon, Input, Item, Label, } from "native-base";
import { FlatList, SafeAreaView, StyleSheet, Text, TouchableOpacity, View, TextInput, ScrollView } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { DEVICE_HEIGHT, FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_15, FONT_SIZE_18 } from "../../../../styles/typography";
import common from "../../../../styles/common";
import { clearFilter, updateAvailabilityFilter, updateGenderFilter, updateSepcialitySelected, specialityFilter, setSearchDocName, updateCitySelected, filterListByCity, setSpecialityFilter, setFilterApplied, setFilterBody } from "../action";
import moment from "moment";
import { recordError } from "../../../../utils/fireLog";
import { strings } from "../../../../utils/translation";
import DatePickerModal from "../../../../components/datePickerModal";
import { getDocList } from "../../../../actions/sehet/user-action";
import { getSkipped } from "../../../../services/auth";
import { getCities } from "../../../../actions/sehet/speciality-action";
import { EMPTY_DOC_LIST } from "../../../../utils/constant";
import CityModal from "../../../../components/CityModal";
import PriceFilterModal from "../../../../components/PriceFilterModal";
import { isValid } from "../../../../utils/ifNotValid";


const FilterScreen = (props) => {
    const dispatch = useDispatch();
    const specialityListSelected = useSelector((state) => state.doctorFilterReducer.specialityFilterSelected);
    const cityFilterSelected = useSelector((state) => state.doctorFilterReducer.cityFilterSelected);
    const cities = useSelector((state) => state.doctorFilterReducer.cityFilter);
    // const specialityListSelected = useSelector((state) => state.specialitiesReducer.specialities);
    const genderFilter = useSelector((state) => state.doctorFilterReducer.genderFilter);
    const avalibilty = useSelector((state) => state.doctorFilterReducer.avalibilty);
    const filterApplied = useSelector((state) => state.doctorFilterReducer.filterApplied);
    const [filterUrl, setFilterUrl] = useState("");
    const [isDatePickerVisible, setIsDatePickerVisible] = useState(false);
    const [isOpenDateFilter, setIsOpenDateFilter] = useState(false);
    const [isOpenSpecialityFilter, setIsOpenSpecialityFilter] = useState(false);
    const [isOpenGenderFilter, setIsOpenGenderFilter] = useState(false);
    const [isOpenCityFilter, setIsOpenCityFilter] = useState(false);
    const [isOpenPriceFilter, setIsOpenPriceFilter] = useState(false);
    const [searchCity, setSearchCity] = useState('');
    const [showCityModal, setShowCityModal] = useState(false);
    const [selectedPriceFilter, setSelectedPriceFilter] = useState('');
    const [priceModal, setPriceModal] = useState(false);
    const [priceFilterData, setPriceFilterData] = useState([
        {
            id: 1,
            title: "Price : Less than 300",
            text: "300",
        },
        {
            id: 2,
            title: "Price : Between 300-500",
            text: "300-500",
        },
        {
            id: 3,
            title: "Price: More than 500",
            text: "500",
        },
    ]);

    const filteredCitites = cityFilterSelected?.filter(itm => itm?.value == true)
    const selectedCities = filteredCitites?.map(itm => itm?.name)

    useEffect(() => {
        getUserLoggedIn()
    }, []);

    useEffect(() => {
        dispatch(filterListByCity(searchCity, cities))
    }, [searchCity])

    const getUserLoggedIn = async () => {
        let skip = await getSkipped();
        if (skip == "false") {
            setFilterUrl("api/doctorListBySpeciality")
        } else {
            setFilterUrl("doctorListBySpeciality")
        }
    }

    const header = () => {
        return <SafeAreaView style={styles.header}>
            <View style={styles.heading}>
                <Icon name="arrowleft" type="AntDesign" style={styles.headerBackIcon}
                    onPress={() => props.navigation.goBack()} />
                <Text style={styles.headerText}>{strings.filter}</Text>
                <TouchableOpacity onPress={async () => {
                    dispatch(clearFilter())
                    dispatch(setSearchDocName(""))
                    dispatch({ type: EMPTY_DOC_LIST });
                    let skip = await getSkipped();
                    if (skip == "true") {
                        dispatch(getDocList("doctors?page=1&itemsPerPage=10"))
                    } else {
                        dispatch(getDocList("api/paginatedDoctors?page=1&itemsPerPage=10"))
                    }
                    props.navigation.goBack()
                }}>
                    <Text style={styles.clearText}>{strings.clear_all}</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    }

    const onSelectedSpecialityChange = (item) => {
        let result = specialityListSelected.map(x => {
            return x.id === item.id ? { ...x, value: !x.value } : x;
        })
        dispatch(updateSepcialitySelected(result))
    }

    const onSelectedCityChange = (item) => {
        let result = cityFilterSelected.map(x => {
            return x._id === item._id ? { ...x, value: !x.value } : x;
        })
        dispatch(updateCitySelected(result))
    }

    const onGenderChange = (item) => {
        let result = genderFilter.map(x => {
            return x.label === item.label ? { ...x, value: !x.value } : { ...x, value: false };
        })
        dispatch(updateGenderFilter(result))
    }

    const toggleDatePicker = () => {
        setIsDatePickerVisible(true)
    }

    const handleConfirm = (date) => {
        try {
            const dob = moment(date).format("YYYY-MM-DD");
            setIsDatePickerVisible(false)
            dispatch(updateAvailabilityFilter(dob))
            // dispatch(updateAvailabilityFilter(!avalibilty))
        } catch (e) {
            recordError(e)
        }
    };

    const onApplyButtonPress = () => {
        const checkSpecValue = specialityListSelected.some(el => el.value === true);
        const checkCityValue = cityFilterSelected.some(el => el.value === true);
        const checkGenderValue = genderFilter.some(el => el.value === true);
        const checkPriceValue = isValid(selectedPriceFilter) ? true : false;
        if (checkSpecValue || checkGenderValue || avalibilty || checkCityValue || checkPriceValue) {
            const s_ids = []
            const c_ids = []
            const selectedfee = selectedPriceFilter?.text
            let gender;

            //get selected specialities ids
            specialityListSelected.map(item => {
                const isTrue = item.value
                if (isTrue === true) {
                    s_ids.push(item.specialityId)
                }
            })

            //get selected cities ids
            cityFilterSelected.map(item => {
                const isTrue = item.value
                if (isTrue === true) {
                    c_ids.push(item.name)
                }
            })

            //get selected gender
            const genderSelected = []
            genderFilter.map(item => {
                if (item.value === true) {
                    genderSelected.push(item)
                }
            })
            if (genderSelected.length === 1) {
                gender = genderSelected[0].label.toUpperCase()
            } else {
                gender = ''
            }
            const s_date = avalibilty == null || avalibilty == undefined || avalibilty == ''
                ? ''
                : moment(avalibilty).format('MM-DD-YYYY')

            // dispatch(specialityFilter(filterUrl, s_ids, gender, s_date, c_ids, 1, 10))
            dispatch({ type: EMPTY_DOC_LIST });
            dispatch(clearFilter())
            dispatch(setSearchDocName(""))
            dispatch(setFilterApplied(true))
            dispatch(setFilterBody({ s_ids, gender, date: s_date, c_ids, fee: selectedfee }))
            props.navigation.goBack();
        } else {
            // dispatch(getDocList())
            props.navigation.goBack();
        }
    }

    const onTextSearch = (text) => {
        setSearchCity(text)
    }

    const filterView = () => {
        return <View style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
            <ScrollView>
                <View style={{ flex: 1, marginHorizontal: 20, marginTop: 20 }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Text style={styles.lableStyle}>{strings.by_availibility}</Text>
                        <TouchableOpacity
                            style={{ height: 25, width: 25, backgroundColor: '#FF5E38', borderRadius: 50 }}
                            onPress={() => setIsOpenDateFilter(!isOpenDateFilter)}
                        >
                            <Icon
                                type="Entypo"
                                style={{ color: '#FFF', fontSize: 25 }}
                                name={isOpenDateFilter ? "chevron-up" : "chevron-down"}
                            />
                        </TouchableOpacity>

                    </View>
                    {
                        isOpenDateFilter ? <Item onPress={() => toggleDatePicker()}>
                            <Input style={styles.marginItem}
                                placeholder={"Select Date"}
                                value={avalibilty === "" ? ""
                                    : moment(avalibilty).format("DD MMMM YYYY").toString()}
                                disabled />
                            <Icon name="calendar" type="Feather" style={styles.calIcon} />
                        </Item> : null
                    }
                    {/* <Item>
                    <View style={{ flexDirection: 'row', marginVertical: 10, alignItems: 'center' }}>
                        <CheckBox
                            checked={avalibilty}
                            onPress={handleConfirm}
                            onAnimationType="bounce"
                            offAnimationType="bounce"
                            onTintColor="#00A2FF"
                        />
                        <Text style={{ marginLeft: 25, fontSize: 16 }}>Available for today</Text>
                    </View>
                </Item> */}
                </View>

                <View style={{ flex: 1, marginHorizontal: 20, marginTop: 20 }}>

                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Text style={styles.lableStyle}>{strings.by_speciality}</Text>
                        <TouchableOpacity
                            style={{ height: 25, width: 25, backgroundColor: '#FF5E38', borderRadius: 50 }}
                            onPress={() => setIsOpenSpecialityFilter(!isOpenSpecialityFilter)}
                        >
                            <Icon
                                type="Entypo"
                                style={{ color: '#FFF', fontSize: 25 }}
                                name={isOpenSpecialityFilter ? "chevron-up" : "chevron-down"}
                            />
                        </TouchableOpacity>

                    </View>
                    {
                        isOpenSpecialityFilter ? <Item>
                            <FlatList
                                scrollEnabled
                                showsVerticalScrollIndicator={true}
                                style={{ height: DEVICE_HEIGHT / 4, marginLeft: 20, }}
                                keyExtractor={(item, index) => index.toString()}
                                data={specialityListSelected}
                                ListEmptyComponent={() => <View><Text>No specialities found</Text></View>}
                                renderItem={({ item }) =>
                                    <TouchableOpacity onPress={() => onSelectedSpecialityChange(item)}
                                        style={{
                                            flex: 1, flexDirection: 'row', paddingVertical: 5,
                                            paddingRight: 20
                                        }}>
                                        <Text style={{ flex: 8, color: item.value ? '#038BEF' : 'black' }}>{item.name}</Text>
                                        <Icon type={"AntDesign"} name={"check"} style={{ color: item.value ? '#038BEF' : 'white' }} />
                                    </TouchableOpacity>}
                            />
                        </Item> : null
                    }
                </View>

                <View style={{ margin: 20 }}>


                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Text style={styles.lableStyle}>{strings.by_gender}</Text>
                        <TouchableOpacity
                            style={{ height: 25, width: 25, backgroundColor: '#FF5E38', borderRadius: 50 }}
                            onPress={() => setIsOpenGenderFilter(!isOpenGenderFilter)}
                        >
                            <Icon
                                type="Entypo"
                                style={{ color: '#FFF', fontSize: 25 }}
                                name={isOpenGenderFilter ? "chevron-up" : "chevron-down"}
                            />
                        </TouchableOpacity>

                    </View>
                    {
                        isOpenGenderFilter ? <Item style={{ flex: 1 }}>
                            <FlatList
                                keyExtractor={(item, index) => index.toString()}
                                data={genderFilter}
                                renderItem={({ item }) =>
                                    <TouchableOpacity onPress={() => onGenderChange(item)}
                                        style={{ flex: 1, flexDirection: 'row', paddingVertical: 5 }}>
                                        <Text style={{ flex: 8, color: item.value ? '#038BEF' : 'black' }}>{item.label}</Text>
                                        <Icon type={"AntDesign"} name={"check"} style={{ color: item.value ? '#038BEF' : 'white' }} />
                                    </TouchableOpacity>}
                            />
                        </Item> : null

                    }

                </View>


                <View style={{ marginHorizontal: 20 }}>


                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Text style={styles.lableStyle}>{strings.by_city}</Text>
                        <TouchableOpacity
                            style={{ height: 25, width: 25, backgroundColor: '#FF5E38', borderRadius: 50 }}
                            // onPress={() => setIsOpenCityFilter(!isOpenCityFilter)}
                            onPress={() => {
                                setIsOpenCityFilter(true)
                                setShowCityModal(true)
                            }}
                        >
                            <Icon
                                type="Entypo"
                                style={{ color: '#FFF', fontSize: 25 }}
                                name={isOpenCityFilter ? "chevron-up" : "chevron-down"}
                            />
                        </TouchableOpacity>

                    </View>
                    {
                        isOpenCityFilter ?
                            <View
                                style={{
                                    flex: 1,
                                    marginVertical: 10,
                                    paddingHorizontal: 5
                                }}
                            >
                                <Text>{selectedCities?.toString()}</Text>
                            </View> : null

                    }

                </View>

                <View style={{ margin: 20 }}>


                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                        <Text style={styles.lableStyle}>{strings.by_price}</Text>
                        <TouchableOpacity
                            style={{ height: 25, width: 25, backgroundColor: '#FF5E38', borderRadius: 50 }}
                            // onPress={() => setIsOpenCityFilter(!isOpenCityFilter)}
                            onPress={() => {
                                setIsOpenPriceFilter(true)
                                setPriceModal(true)
                            }}
                        >
                            <Icon
                                type="Entypo"
                                style={{ color: '#FFF', fontSize: 25 }}
                                name={isOpenCityFilter ? "chevron-up" : "chevron-down"}
                            />
                        </TouchableOpacity>

                    </View>
                    {
                        isOpenPriceFilter ?
                            <View
                                style={{
                                    flex: 1,
                                    marginVertical: 10,
                                    paddingHorizontal: 5
                                }}
                            >
                                <Text>{selectedPriceFilter?.title}</Text>
                            </View> : null

                    }

                </View>

            </ScrollView>
        </View>
    }

    return (
        <>
            {header()}
            <Content style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
                {filterView()}
            </Content>


            <DatePickerModal
                isVisible={isDatePickerVisible}
                onConfirm={(date) => handleConfirm(date)}
                onCancel={() => toggleDatePicker()}
                minimumDate={new Date()}
            />

            <Footer style={common.footer}>
                <Button danger
                    style={[common.footerBtn, { backgroundColor: '#D72820' }]} onPress={() => onApplyButtonPress()}>
                    <Text style={[styles.clearText, { color: '#FFF' }]}>{strings.Apply_Filter}</Text>
                </Button>
            </Footer>

            <CityModal visible={showCityModal} onCloseModal={() => setShowCityModal(false)} />

            <PriceFilterModal
                visible={priceModal}
                data={priceFilterData}
                closeModal={() => setPriceModal(false)}
                onItemPressCb={(item) => {
                    console.log('cb called ', item);
                    setSelectedPriceFilter(item)
                    setPriceModal(false)
                }}
            />
        </>
    );
};
export default FilterScreen;


const styles = StyleSheet.create({
    header: {
        height: hp("10%"),
        backgroundColor: "#141E46",
        justifyContent: "center",
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    heading: {
        width: '92%',
        flexDirection: "row",
        marginLeft: 18,
        alignItems: "center",
        justifyContent: "flex-start",
    },
    headerText: {
        color: "#FFF",
        flex: 1,
        fontSize: FONT_SIZE_18,
        marginLeft: wp("4%"),
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    clearText: {
        color: "#FF5E38",
        fontSize: FONT_SIZE_15,
        marginRight: wp("1%"),
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    headerBackIcon: {
        fontSize: hp("3.5%"),
        color: "#FFF",
    },
    lableStyle: {
        fontSize: 16, color: '#041A32', fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    }

})
