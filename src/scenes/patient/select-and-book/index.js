import React, { useEffect, useState } from "react";
import { Button, CheckBox, Container, Icon, Text } from "native-base";
import { FlatList, Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../assets/images";
import { recordError, successLog } from "../../../utils/fireLog";
import {
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_FAMILY_SF_PRO_REGULAR,
    FONT_FAMILY_SF_PRO_SemiBold,
    FONT_SIZE_13,
    FONT_SIZE_15
} from "../../../styles/typography";
import HeaderSearchBar from "../../../components/header-searchbar";
import Line from "../../../components/line";
import { getDocList, getDocListByKeyword, getDocProfile } from "../../../actions/sehet/user-action";
import { ifNotValid, ifValid, isValid } from "../../../utils/ifNotValid";
import ViewProfileModal from "../../../components/view-profile-modal";
import { DOC_PROFILE, SPECIALITY_FILTER_SELECTED_LIST, SPECIALITY_FILTER_LIST, GET_DOC_LIST, CITY_FILTER_SELECTED_LIST, CITY_FILTER_LIST, EMPTY_DOC_LIST } from "../../../utils/constant";
import { setSelectedDoctor } from "../../../actions/appointment";
import { getSkipped, setFromCheckout } from "../../../services/auth";
import { showModal } from "../../../actions/modal";
import { filterByGender, filterListByName, setSearchDocName, setSpecialityFilter, setFilterApplied, getDoctorsBySearch, specialityFilter, setFilterBody } from "./action";
import { selectedSpecialityObj } from "../../../actions/sehet/speciality-action";
import Footer from "../../../components/Footer";
import { strings, translateToHindi } from "../../../utils/translation";
// import PowerTranslator, { TranslatorConfiguration, ProviderTypes } from "react-native-power-translator";
import PowerTranslator from "../../../components/PowerTranslator";
import { CONSULT_FEE, translationApiKey } from "../../../config";
import { setFollowUp } from "../upcoming-past-bookings/components/action";
import { getNearbyDoctors, SetSpecialityDetails } from "../../../actions/profile";
import { getSpecialitiesNames } from "../../../utils/helper";
import { ActivityIndicator } from "react-native";
// import firestore from '@react-native-firebase/firestore'

const SelectAndBook = (props) => {
    const doctorList = useSelector((state) => state.userReducer.docList);
    const docListTemp = useSelector((state) => state.userReducer.docListTemp);
    const femaleCheck = useSelector((state) => state.userReducer.femaleCheck);
    const maleCheck = useSelector((state) => state.userReducer.maleCheck);
    // const searchDocName = useSelector((state) => state.userReducer.searchDocName);
    const specialities = useSelector((state) => state.specialitiesReducer.specialities);
    const cities = useSelector((state) => state.doctorFilterReducer.cityFilter);
    const filterApplied = useSelector((state) => state.doctorFilterReducer.filterApplied);
    const lang = useSelector(state => state.profile.selected_language)
    const isLoading = useSelector(state => state.loader.isLoading)
    const cords = useSelector(state => state.profile.locationData)

    const filterBody = useSelector((state) => state.doctorFilterReducer.filterBody);

    const [modelProfile, setProfileModel] = useState(false);
    const [availableToday, setAvailableToday] = useState(false);
    const [pageNo, setPageNo] = useState(1);
    const [pageNoForSymptom, setPageNoForSymptom] = useState(1);
    const [itemsPerPage, setItemsPerPage] = useState(10);
    const [pageNoForDocSearch, setPageNoForDocSearch] = useState(1);
    const [searchDocName, setSearchDocName] = useState('');
    const [pageNoForFilter, setPageNoForFilter] = useState(1);
    const [view_all, setView_all] = useState(false);
    const item = ifValid(props.navigation.state) && ifValid(props.navigation.state.params) && props.navigation.state.params.item ? props.navigation.state.params.item : undefined;
    const searchText = ifValid(props.navigation?.state?.params?.searchText) ? props.navigation?.state?.params?.searchText : '';
    let nearby = ifValid(props.navigation?.state?.params?.show_nearby) ? props.navigation?.state?.params?.show_nearby : '';
    const [show_nearby, setShowNearby] = useState(nearby)
    const dispatch = useDispatch();


    useEffect(() => {
        if (show_nearby == true) {
            dispatch(getNearbyDoctors(cords))
        }
    }, [show_nearby])

    useEffect(() => {
        if (view_all == true) {
            paginatedCall()
        }
    }, [view_all])

    useEffect(() => {
        if (filterApplied === false) {
            if (specialities.length > 0) {
                const specialityArray = []
                specialities.map((item, index) => {
                    let sp = {
                        specialityId: item._id,
                        id: (index + 1),
                        name: item.name,
                        value: false
                    }
                    specialityArray.push(sp)
                })

                // console.log(" filtered specs " + JSON.stringify(specialityArray))
                dispatch(setSpecialityFilter(SPECIALITY_FILTER_LIST, specialityArray))
                dispatch(setSpecialityFilter(SPECIALITY_FILTER_SELECTED_LIST, specialityArray))

                // dispatch(setFilterApplied(true))
            }
        }
    }, [])

    useEffect(() => {
        if (!isValid(searchText) && show_nearby != true) {
            paginatedCall()
        }
    }, [pageNo])

    useEffect(() => {
        if (isValid(searchText)) {
            callKeyWordSearchApi(searchText)
        }
    }, [pageNoForSymptom, searchText])

    useEffect(() => {
        if (filterApplied == true && isValid(filterBody)) {
            filterPaginatedApi()
        }
    }, [pageNoForFilter, filterBody])

    const paginatedCall = async () => {
        let skip = await getSkipped();
        if (skip == "true") {
            dispatch(getDocList(`doctors?page=${pageNo}&itemsPerPage=${itemsPerPage}`))
        } else {
            dispatch(getDocList(`api/paginatedDoctors?page=${pageNo}&itemsPerPage=${itemsPerPage}`))
        }
    }

    const callKeyWordSearchApi = async (searchText) => {
        let skip = await getSkipped();
        if (skip == "true") {
            dispatch(getDocListByKeyword(`search-by-symptoms/${searchText}?page=${pageNoForSymptom}&itemsPerPage=${itemsPerPage}`))
        } else {
            dispatch(getDocListByKeyword(`api/search-by-symptoms/${searchText}?page=${pageNoForSymptom}&itemsPerPage=${itemsPerPage}`))
        }
    }

    const filterPaginatedApi = async () => {
        let skip = await getSkipped();
        let url;
        if (skip == "false") {
            url = "api/doctorListBySpeciality"
        } else {
            url = "doctorListBySpeciality"
        }
        dispatch(specialityFilter(url, filterBody?.s_ids, filterBody?.gender, filterBody?.date, filterBody?.c_ids, filterBody?.fee, pageNoForFilter, itemsPerPage))
    }


    useEffect(() => {
        if (isValid(searchDocName)) {
            dispatch({ type: EMPTY_DOC_LIST })
            dispatch(getDoctorsBySearch(`searchDoctors?keyword=${searchDocName}&page=${1}&itemsPerPage=${itemsPerPage}`))
        }
    }, [searchDocName]);

    useEffect(() => {
        if (pageNoForDocSearch != 1) {
            dispatch(getDoctorsBySearch(`searchDoctors?keyword=${searchDocName}&page=${pageNoForDocSearch}&itemsPerPage=${itemsPerPage}`))
        }
    }, [pageNoForDocSearch]);

    useEffect(() => {
        return () => {
            if (filterApplied == true) {
                dispatch(setFilterApplied(false))
                dispatch(setFilterBody(null))
            }
        }
    }, [])

    const keyExtractor = (item, index) => index.toString();

    const _viewProfile = (_id) => {
        dispatch(getDocProfile(_id))
        setProfileModel(!modelProfile);
    }

    const handleBookNow = async (doc) => {
        console.log("passed doc ", doc);
        //adding doc data fro dynamic link
        // firestore().collection('dynamic_links').add({
        //     doc: doc,
        //     link: "https://sehet.in/pukhraj_sukhlecha"
        // }).then(() => {
        //     alert('doc added')
        // }).catch((err) => {
        //     console.log(err);
        //     alert('err')
        // })
        // return

        // dispatch(setFollowUp(null))
        // dispatch(setSelectedDoctor(doc))

        // const item = doc.specialityId;
        // if (ifNotValid(item) || ifNotValid(item.price))
        //     return;
        // let _price = parseInt(item.price)
        // let m_price = parseInt(item.price)
        // let m_specialPrice = parseInt(item?.specialPrice)
        // let percentage = (m_price / 100) * m_specialPrice;
        // m_price = m_price + percentage;
        // dispatch(selectedSpecialityObj({ ...item, price: m_price, m_price: m_price, dateTime: new Date() }))

        dispatch(SetSpecialityDetails(doc))

        const skipped = await getSkipped()
        const isSkipped = JSON.parse(skipped)
        if (isSkipped) {
            const payload = {
                text: `${strings.please_login_to_continue}`,
                subtext: "",
                iconName: "closecircleo",
                modalVisible: true,
            }
            setFromCheckout(true)
            props.navigation.navigate('PatientLogin')
            dispatch(showModal(payload))
        } else {
            props.navigation.navigate("BookAppointmentScreenFlow2", {
                doc
            })
        }

    }

    const renderItem = ({ item }) => {

        let name = item.fullName;
        if (ifNotValid(name)) name = "..."

        let hinFullName = item.hindiFullName;
        if (ifNotValid(hinFullName)) hinFullName = name

        let speciality = ifValid(item.specialityId) ? getSpecialitiesNames(item.specialityId, 'name') : item.specialization;
        if (ifNotValid(speciality)) speciality = ""

        let hinSpeciality = ifValid(item.specialityId) ? getSpecialitiesNames(item.specialityId, 'hindiName') : item.specialization;
        if (ifNotValid(hinSpeciality)) hinSpeciality = speciality


        let gender = item.gender;
        let imageUrl = item.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = gender == "FEMALE" ? Images.female_doctor : Images.male_doctor
        else imageUrl = { uri: imageUrl }

        let experience = item.experience;
        if (ifNotValid(experience)) experience = "-"

        let docFee = isValid(item?.fee) ? item?.fee : CONSULT_FEE;

        let onlineFee = isValid(item?.fee) ? item?.fee : CONSULT_FEE;
        let offlineFee = isValid(item?.offlineFee) && item?.offlineFee != 0 ? item?.offlineFee : null;

        let doc = {
            specialization: speciality,
            gender: item.gender,
            imageUrl: item.imageUrl,
            experience: experience,
            fullName: name,
            _id: item._id,
            specialityId: item.specialityId,
            fee: isValid(item?.fee) ? item?.fee : CONSULT_FEE
        }

        return (
            <View style={{ backgroundColor: '#F9F9F9', paddingHorizontal: 14, }}>
                <View style={styles.cardContainer}>
                    <View style={{ flex: 2 }}>
                        <Image source={imageUrl} style={styles.cardImageStyle} />
                    </View>
                    <View style={{ flex: 8, marginLeft: 5 }}>
                        {/* <Text style={styles.cardTextStyleName}>{name}</Text> */}
                        {/* <PowerTranslator text={name} style={styles.cardTextStyleName} /> */}
                        <PowerTranslator
                            hinText={hinFullName}
                            enText={name}
                            style={styles.cardTextStyleName}
                        />
                        {/* <Text style={styles.cardTextStyleSpeciality}>{speciality}</Text> */}
                        {/* <PowerTranslator text={speciality} style={styles.cardTextStyleSpeciality} /> */}
                        <PowerTranslator
                            hinText={hinSpeciality}
                            enText={speciality}
                            style={styles.cardTextStyleSpeciality}
                        />
                        {/* <Text style={styles.cardTextStyleSpeciality}>{experience}</Text> */}
                        <View style={{
                            backgroundColor: '#E8F5FF',
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginHorizontal: 3,
                            marginVertical: 5,
                            padding: 10,
                            alignSelf: 'flex-start'
                        }}>
                            <View style={{ flex: 1, }}>
                                <Text style={{
                                    color: '#586278',
                                    fontSize: 13
                                }}>Online : Rs {onlineFee}</Text>
                            </View>
                            {offlineFee != null && <View style={{}}>
                                <Text style={{
                                    color: '#586278',
                                    fontSize: 13
                                }}>Offline : Rs {offlineFee}</Text>
                            </View>}
                        </View>
                    </View>
                </View>
                <View style={styles.buttonContainer}>
                    <TouchableOpacity style={styles.buttonStyle} onPress={() => _viewProfile(item._id)}>
                        <Text uppercase={false} style={styles.buttonTxtStyle}>{strings.View_Profile}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => handleBookNow(doc)}
                        style={[styles.buttonStyle, {
                            backgroundColor: '#D72820',
                            borderColor: '#D72820'
                        }]}>
                        <Text uppercase={false} style={[styles.buttonTxtStyle, { color: 'white' }]}>{strings.Book_Now}</Text>
                    </TouchableOpacity>
                </View>
                <Line style={{ marginTop: 8, borderBottomWidth: 0.2 }} lineColor="#A0A9BE" />
            </View>
        );
    };

    const onChange = (value) => {
        try {
            let val = value.replace(/[^A-Za-z_ .]/gi, "");
            if (val === undefined || val === null)
                return
            setSpecialityName(val);
        } catch (e) {
            recordError(e)
        }
    };

    const DocListFooter = () => {
        return <ActivityIndicator size={"large"} color={"blue"} />
    }

    const loadMoreData = () => {
        if (filterApplied === false) {
            if (!isValid(searchDocName)) {
                if (isValid(searchText)) {
                    setPageNoForSymptom(pageNoForSymptom + 1)
                } else {
                    setPageNo(pageNo + 1)
                }
            } else {
                if (isValid(searchDocName)) {
                    setPageNoForDocSearch(pageNoForDocSearch + 1)
                }
            }
        } else {
            if (isValid(filterBody)) {
                setPageNoForFilter(pageNoForFilter + 1)
            }
        }
    }

    const FloatingBotton = () => {
        return <TouchableOpacity style={styles.filterIconCont} onPress={() => props.navigation.navigate("FilterScreen")}>
            <Icon name="filter" type="Feather" style={styles.filterIconStyle} />
        </TouchableOpacity>
    }


    return (
        <Container>
            <HeaderSearchBar
                navigation={props.navigation}
                headerText={strings.select_and_book}
                inputTextValue={searchDocName}
                inputTextPlaceholder={strings.searchByDoctorName}
                // onChange={(value) => dispatch(setSearchDocName(value))} />
                onChange={(value) => setSearchDocName(value)} />

            {isValid(searchText) && <Text style={styles.searchTextStyle}>Search "{searchText}"</Text>}

            {doctorList !== undefined && doctorList !== null && doctorList.length > 0 ? (
                <View style={{ flex: 1, }}>
                    <FlatList
                        scrollEnabled
                        showsVerticalScrollIndicator={false}
                        keyExtractor={keyExtractor}
                        data={doctorList}
                        renderItem={(item) => renderItem(item)}
                        onEndReached={loadMoreData}
                        onEndReachedThreshold={0.3}
                    // ListFooterComponent={<DocListFooter />}
                    />
                </View>
            ) :
                isValid(searchDocName) && isValid(doctorList) && isLoading == false ?
                    (
                        <View style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: '#F9F9F9' }}>
                            <Image source={Images.notFound} style={{ width: 80, height: 85 }} />
                            <Text style={styles.notFound}>Search Failed!</Text>
                            <Text style={styles.notFoundDetails}>Try different words to get better results</Text>
                        </View>
                    ) :
                    isLoading == false ? (
                        <View
                            style={{ flex: 1, alignItems: "center", justifyContent: "center", backgroundColor: '#F9F9F9' }}>
                            {show_nearby == true ? (
                                <>
                                    <Text style={{ fontSize: 18, marginVertical: 15 }}>No Nearby Doctors Found</Text>
                                    <Button block style={{
                                        backgroundColor: '#FBE7E3',
                                        borderRadius: 5,
                                        marginVertical: 10,
                                        marginHorizontal: 40,
                                        elevation: 0
                                    }}
                                        onPress={() => {
                                            dispatch({ type: EMPTY_DOC_LIST })
                                            setShowNearby(false)
                                            setView_all(true)
                                        }}>
                                        <Text style={{
                                            color: '#FF5E38'
                                        }}>{strings.view_all}</Text>
                                    </Button>
                                </>
                            ) : <Text style={{ fontSize: 18, fontWeight: 'bold' }}>No results found</Text>}
                        </View>
                    ) : <View style={{ flex: 1 }}></View>
            }

            {/* <View style={styles.bottomContainer}>
                <TouchableOpacity style={styles.buttonView} onPress={() => props.navigation.navigate("FilterScreen")}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={Images.filter} style={{ width: 12, height: 10, marginHorizontal: 10 }} />
                        <Text style={styles.filter}>FILTER</Text>
                    </View>
                </TouchableOpacity>
            </View> */}


            {/* <Footer>
                <TouchableOpacity style={styles.buttonView} onPress={() => props.navigation.navigate("FilterScreen")}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={Images.filter} resizeMode="contain" style={{ width: 18, height: 18, marginHorizontal: 10 }} />
                        <Text style={styles.filter}>{strings.filter}</Text>
                    </View>
                </TouchableOpacity>
            </Footer> */}

            <FloatingBotton />

            <ViewProfileModal
                onTouchOutside={() => {
                    dispatch({ type: DOC_PROFILE, payload: {} });
                    setProfileModel(!modelProfile)
                }}
                onHardwareBackPress={() => {
                    dispatch({ type: DOC_PROFILE, payload: {} });
                    setProfileModel(!modelProfile)
                }}
                specialization={null}
                name={undefined}
                experience={undefined}
                imageUrl={""}
                visible={modelProfile} />
        </Container>
    );
};
export default SelectAndBook;

const styles = StyleSheet.create({
    notFound: {
        color: "#041A32",
        fontSize: 18,
        marginBottom: 13,
        marginTop: 20,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    notFoundDetails: {
        color: "#A0A9BE",
        fontSize: 14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    cardTextStyleName: {
        marginHorizontal: 10,
        width: '100%',
        fontSize: FONT_SIZE_15,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: '#484F5F'
    },
    cardTextStyleSpeciality: {
        marginHorizontal: 10,
        width: '100%',
        fontSize: FONT_SIZE_13,
        fontFamily: FONT_FAMILY_SF_PRO_REGULAR,
        color: '#A0A9BE'
    },
    cardImageStyle: {
        width: 60,
        height: 60,
        borderRadius: 60 / 2,
    },
    cardContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
        marginTop: 10,
        padding: 15,
    },
    buttonContainer: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'center',
    },
    buttonStyle: {
        borderRadius: 8,
        borderColor: '#00A2FF',
        padding: 7,
        borderWidth: 1,
        flex: 1,
        marginTop: 5,
        marginBottom: 8,
        marginHorizontal: 5
    },
    buttonTxtStyle: {
        color: '#00A2FF',
        padding: 0,
        margin: 0,
        textAlign: 'center',
        fontSize: FONT_SIZE_15
    },
    radioTextStyle: {
        marginLeft: 19,
        color: '#041A32',
        fontSize: FONT_SIZE_13
    },
    bottomContainer: {
        height: 50,
        borderRadius: 8,
        marginVertical: 10,
        marginHorizontal: 23,
        backgroundColor: '#E5F3FE',
        flexDirection: 'row',
        shadowColor: '#0000000B',
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.8,
        shadowRadius: 2,
        elevation: 5
    },
    filter: {
        color: '#244262',
        fontSize: FONT_SIZE_15,
        textAlign: 'center',
        // fontFamily: FONT_FAMILY_BALOO_SEMI_BOLD
    },
    sortStyle: {
        color: '#244262',
        fontSize: FONT_SIZE_15,
        textAlign: 'center',
        // fontFamily: FONT_FAMILY_BALOO_SEMI_BOLD
    },
    lineStyle: {
        height: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderLeftWidth: 0.5,
        borderLeftColor: '#244262',
    },
    buttonView: {
        // flex: 1,
        height: 40,
        marginLeft: 0,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
        marginRight: 0.1,
        backgroundColor: '#E5F3FE',
        borderRadius: 5
    },
    searchTextStyle: {
        paddingHorizontal: 20,
        paddingTop: 10,
        backgroundColor: '#F9F9F9',
        fontFamily: FONT_FAMILY_SF_PRO_SemiBold,
    },
    filterIconCont: {
        position: 'absolute',
        bottom: 15,
        right: 15,
        backgroundColor: '#373737',
        borderRadius: 150,
    },
    filterIconStyle: {
        color: '#FFFFFF',
        padding: 20,
        fontSize: 26,
    },
});

