import React, { useEffect, useState } from "react";
import { Container, Text } from "native-base";
import { FlatList, Image, StyleSheet, TouchableOpacity, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import Images from "../../../assets/images";
import { recordError, successLog } from "../../../utils/fireLog";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_14, FONT_SIZE_15 } from "../../../styles/typography";
import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { getSpeciality, selectedSpecialityObj } from "../../../actions/sehet/speciality-action";
import HeaderSearchBar from "../../../components/header-searchbar";
import { ifNotValid } from "../../../utils/ifNotValid";
import { getDoctorsSlotByID } from "../select-and-book/action";

const SelectSpeciality = (props) => {
    const dispatch = useDispatch();
    const specialities = useSelector((state) => state.specialitiesReducer.specialities);
    const [_doctors, setSpeciality] = useState([]);
    const [specialitiesName, setSpecialityName] = useState("");

    useEffect(() => {
        dispatch(getSpeciality())
        successLog("Doctor listing screen loaded")
    }, []);


    const _onSlotSelected = (item) => {
        if (ifNotValid(item) || ifNotValid(item.price))
            return;
        let _price = parseInt(item.price)
        let m_price = parseInt(item.price)
        // let percentage = (_price / 100) * 20;
        // _price = _price + percentage;
        dispatch(getDoctorsSlotByID(item._id))
        dispatch(selectedSpecialityObj({ ...item, price: _price, m_price: m_price, dateTime: new Date() }))
        props.navigation.navigate("SelectAndBook", { item: item })
    }



    useEffect(() => {
        try {
            if (specialities !== undefined && specialities !== null && specialities.length > 0) {

                if (specialitiesName.length > 0) {

                    let searchName = specialitiesName;

                    let result = specialities.filter((item) => {
                        let name = item.name.toLowerCase();
                        let specialization = item.specialization !== undefined && item.specialization !== null ? item.specialization.toLowerCase() : "";
                        return (
                            name.includes(searchName.toLowerCase())
                        );
                    });
                    setSpeciality(result);
                } else setSpeciality(specialities);
            }
        } catch (e) {
            recordError(e)
        }
    }, [specialities, specialitiesName]);

    const keyExtractor = (item, index) => index.toString();

    const renderItem = ({ item }) => {

        let price = item.price;
        if (price === undefined || price === null) {
            price = "0"
        }
        let healthProblem = item.name;
        if (healthProblem === undefined || healthProblem === null) {
            healthProblem = ""
        }
        let imageUrl = item.imageUrl;
        if (imageUrl == undefined || imageUrl == null) imageUrl = Images.ayurveda
        else imageUrl = { uri: imageUrl }

        return (
            <TouchableOpacity style={styles.cardContainer}
                onPress={() => _onSlotSelected(item)}>
                <Image source={imageUrl} style={styles.cardImageStyle} />
                <Text numberOfLines={2} style={styles.cardTextStyle}>{healthProblem}</Text>
            </TouchableOpacity>
        );
    };

    const onChange = (value) => {
        try {
            let val = value.replace(/[^A-Za-z_ .]/gi, "");
            if (val === undefined || val === null)
                return
            setSpecialityName(val);
        } catch (e) {
            recordError(e)
        }
    };


    return (
        <Container>

            <HeaderSearchBar
                inputTextPlaceholder={"Search by specialities"}
                headerText={"Select Speciality"}
                navigation={props.navigation}
                inputTextValue={specialitiesName}
                onChange={(name) => onChange(name)}

            />

            {_doctors !== undefined && _doctors !== null && _doctors.length > 0 ? (
                <View style={{
                    justifyContent: 'center',
                    marginTop: 10,
                    flex: 1,
                    marginHorizontal: 10,
                }}>
                    <FlatList
                        scrollEnabled
                        showsVerticalScrollIndicator={false}
                        keyExtractor={keyExtractor}
                        numColumns={2}
                        style={{ flex: 1 }}
                        data={_doctors}
                        renderItem={(item) => renderItem(item)}
                    />
                </View>
            ) :
                specialitiesName !== undefined && specialitiesName !== null && specialitiesName.length > 0 && _doctors !== undefined && _doctors !== null && _doctors.length === 0 ?
                    (
                        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Image source={Images.notFound} style={{ width: 80, height: 85 }} />
                            <Text style={styles.notFound}>Search Failed!</Text>
                            <Text style={styles.notFoundDetails}>Try different words to get better results</Text>
                        </View>
                    ) :
                    (
                        <View
                            style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
                            <Text>No results found</Text>
                        </View>
                    )
            }

            <View style={styles.bottomContainer}>
                {/*<TouchableOpacity style={styles.buttonView}>*/}
                {/*    <Image source={Images.sortBy} style={{width: 15, height: 11, marginHorizontal: 10}}/>*/}
                {/*    <Text style={styles.sortStyle}>SORT BY</Text>*/}
                {/*</TouchableOpacity>*/}

                <TouchableOpacity style={styles.buttonView}>
                    {/*<View style={styles.lineStyle}/>*/}
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={Images.filter} style={{ width: 12, height: 10, marginHorizontal: 10 }} />
                        <Text style={styles.filter}>FILTER</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </Container>
    );
};
export default SelectSpeciality;

const styles = StyleSheet.create({
    header: {
        height: hp("25%"),
        backgroundColor: "#1E2081",
        justifyContent: "center",
        paddingHorizontal: 8,
    },
    heading: {
        flexDirection: "row",
        marginLeft: 18,
        alignItems: "center",
        justifyContent: "flex-start",
    },
    headerText: {
        color: "#fff",
        fontSize: hp("2.9%"),
        marginLeft: wp("4%"),
    },
    headerBackIcon: {
        fontSize: hp("3.5%"),
        color: "#fff",
    },
    headerSearch: {
        height: hp("6.5%"),
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: wp("4%"),
        marginTop: 20,
        opacity: 0.80,
        marginHorizontal: "4%",
        backgroundColor: "rgba(255,255,255,0.2)",
        borderRadius: hp("0.8%"),
    },
    searchBarText: {
        color: "#ffffff",
        fontSize: FONT_SIZE_14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    headerSearchIcon: {
        fontSize: 24,
        marginRight: 10,
        color: "white",
    },
    resultsView: {
        marginTop: 19,
        marginLeft: 26,
    },
    resultsText: {
        color: "#041A32",
        fontSize: 17,
    },
    notFound: {
        color: "#041A32",
        fontSize: 18,
        marginBottom: 13,
        marginTop: 20,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    notFoundDetails: {
        color: "#A0A9BE",
        fontSize: 14,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM
    },
    listView: { marginTop: 20 },
    listBtn: {
        borderRadius: 10,
        borderWidth: 2,
        backgroundColor: "#fff",
        width: 60,
        borderColor: "#C70315",
        justifyContent: "center",
        alignItems: "center",
        height: 30,
    },
    listBtnText: { color: "#041A32" },
    cardTextStyle: {
        textAlign: 'center',
        marginHorizontal: 10,
        fontSize: FONT_SIZE_15,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: 'black'
    },
    cardImageStyle: {
        width: 46,
        height: 46,
        borderRadius: 23
    },
    cardContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f1f2f2',
        marginHorizontal: 14,
        marginBottom: 15,
        marginTop: 0,
        padding: 8,
    },



});

