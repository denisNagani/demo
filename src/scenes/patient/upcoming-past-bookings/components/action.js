import { DOCTOR_SLOT, FOLLOW_UP } from "../../../../utils/constant";


export const setUpcomingPastActive = (payload) => {
    return {
        type: 'UPCOMING_PAST_ACTIVE',
        payload: payload,
    };
};

export const setFollowUp = (payload) => {
    return {
        type: FOLLOW_UP,
        payload: payload,
    };
};
