/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
import { PermissionsAndroid, Platform } from "react-native";
import { check, PERMISSIONS, request, RESULTS } from "react-native-permissions";
import { errorLog, successLog } from "../../../../utils/fireLog";
import { successPayload } from "../../../../utils/helper";
import { showModal } from "../../../../actions/modal";
import moment from "moment";
import RNFetchBlob from "rn-fetch-blob";
import { v4 as uuidv4 } from 'uuid';
import { hide, show } from "../../../../utils/loader/action";

let RNFS = require('react-native-fs');

/**
 * @name requestCameraAndAudioPermission
 * @description Function to request permission for Audio and Camera
 */

// const downloadFile = (url, dispatch) => {
//     // let dateTime = moment(new Date()).format('HH:mm:ss')
//     let dateTime = new Date().getTime()
//     let path;
//     Platform.OS === 'android'
//         ? path = `${RNFS.ExternalStorageDirectoryPath}/Download/prescription_${dateTime}.pdf`
//         : path = `${RNFS.DocumentDirectoryPath}/prescription_${dateTime}.pdf`
//     RNFS.downloadFile({
//         fromUrl: url,
//         toFile: path,
//     }).promise.then((r) => {
//         successLog(`${path}/Download/prescription_${dateTime}.pdf`)
//         dispatch(showModal(successPayload("PDF Downloaded Successfully",
//             `Download location is ${`${path}/Download/`}`)));
//     }).catch(err => {
//         console.log("error downloading prescription " + JSON.stringify(err));
//     });
// }

const downloadFile = (url, dispatch) => {
    dispatch(show())
    const dirs = RNFetchBlob.fs.dirs.DownloadDir;
    const localPath = dirs + `/${uuidv4()}.pdf`
    RNFetchBlob
        .config({
            path: localPath,
            addAndroidDownloads: {
                useDownloadManager: true,
                path: localPath,
                notification: true,
                mediaScannable: true,
            }
        })
        .fetch('GET', url)
        .progress((received, total) => {
            let percentage = (received / total) * 100
            let roundPercentage = Math.round(percentage)
            console.log('progress', roundPercentage)
        })
        .then((res) => {
            dispatch(hide())
            dispatch(showModal(successPayload("PDF Downloaded Successfully",
                `Download location is ${`${localPath}`}`)));
        })
        .catch((err) => {
            dispatch(hide())
            console.log('err  ', err)
        })
}

export const requestReadWritePermissionAndDownload = (url) => {
    successLog('checkPermissionAndDownload')
    return async (dispatch) => {
        // dispatch(show());
        try {
            if (Platform.OS === "android") {
                if (Platform.Version > 32) {
                    downloadFile(url, dispatch)
                } else {
                    const granted = await PermissionsAndroid.requestMultiple([
                        PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                    ]);

                    if (granted["android.permission.READ_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED &&
                        granted["android.permission.WRITE_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED) {
                        downloadFile(url, dispatch)
                    } else {
                        console.log("Permission denied");
                    }
                }

            } else {
                const requestPerm = await request(PERMISSIONS.IOS.MEDIA_LIBRARY);
                const res = await check(PERMISSIONS.IOS.MEDIA_LIBRARY);
                console.log(res);
                if (res === RESULTS.GRANTED) {
                    downloadFile(url, dispatch)
                } else if (res === RESULTS.DENIED) {
                    const res2 = await request(PERMISSIONS.IOS.MEDIA_LIBRARY);
                    res2 === RESULTS.GRANTED
                        ? downloadFile(url, dispatch)
                        : console.log("not granted CAMERA")
                } else {
                    downloadFile(url, dispatch)
                }
            }

        } catch (err) {
            errorLog(err)
        }
    };


}

export const requestStoragePermission = async (callback) => {
    try {
        if (Platform.OS === "android") {
            const granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
            ]);

            if (granted["android.permission.READ_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED &&
                granted["android.permission.WRITE_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED) {
                callback(200)
            } else {
                callback(400)
            }

        } else {
            const res = await check(PERMISSIONS.IOS.MEDIA_LIBRARY);
            console.log(res);
            if (res === RESULTS.GRANTED) {
                callback(200)
            } else if (res === RESULTS.DENIED) {
                const res2 = await request(PERMISSIONS.IOS.MEDIA_LIBRARY);
                res2 === RESULTS.GRANTED
                    ? callback(200)
                    : callback(400)
            } else {
                callback(400)
            }
        }

    } catch (err) {
        console.log('err ', err);
    }
}
