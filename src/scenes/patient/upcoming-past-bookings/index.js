import React, { Component } from "react";
import { FlatList, Image, Linking, Modal, Platform, SafeAreaView, ScrollView, Text, TouchableOpacity, View, } from "react-native";
import { ListItem } from "react-native-elements";
import { Accordion, Button, Container, Icon } from "native-base";
import styles from "./style";
import moment from "moment";
import Images from "../../../assets/images";
import { bindActionCreators } from "redux";
import {
    getPastBookings,
    getUpcomingBookings,
    setCallHeader,
    setSelectedAppointment,
    updateAppointmentPatient
} from "../../../actions/bookings";
import { connect } from "react-redux";
import database from "@react-native-firebase/database";
import { hide, show } from "../../../utils/loader/action";
import { addUserLogToFirebase, checkUserProfile, errorPayload, getSpecialitiesNames } from "../../../utils/helper";
import { setTcModal, showModal } from "../../../actions/modal";
import { JOINED, WAITING } from "../../../utils/constant";
import { methodLog, recordError, successLog } from "../../../utils/fireLog";
import HeaderSehet from "../../../components/header-common";
import SegmentCustom from "./components/SegmentCustom";
import { ifNotValid, ifValid, isValid } from "../../../utils/ifNotValid";
import common from "../../../styles/common";
import { setUpcomingPastActive, setFollowUp } from "./components/action";
import { sendNotification } from "../../../actions/notification";
import { FONT_FAMILY_SF_PRO_BOLD, FONT_FAMILY_SF_PRO_MEDIUM, FONT_FAMILY_SF_PRO_REGULAR, FONT_FAMILY_SF_PRO_SemiBold, FONT_SIZE_13, FONT_SIZE_16 } from "../../../styles/typography";
import { setUpcomingPastDetailView } from "../appointment-detail/action";
import { requestReadWritePermissionAndDownload } from "./components/checkPermission";
import { getSkipped } from "../../../services/auth";
import { strings, TranslationConfig } from "../../../utils/translation";
import PowerTranslator from "../../../components/PowerTranslator";
import { setSelectedDoctor } from "../../../actions/appointment";
import { selectedSpecialityObj } from "../../../actions/sehet/speciality-action";
import { rowStyle } from "../../../styles/commonStyle";
import Line from "../../../components/line";
import { PERMISSIONS, RESULTS, checkMultiple, openSettings, requestMultiple } from "react-native-permissions";
import RNFetchBlob from "rn-fetch-blob";
import { v4 as uuidv4 } from 'uuid';

let RNFS = require('react-native-fs');

class UpcomingPastBookings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            connect: false,
            isLoading: false,
            modal: false,
            id: null,
            roomName: null,
            doctor: null,
            appointmentDate: null,
            activeButton: 'PAST',
            isFetching: false,
        };
    }

    async componentDidMount() {
        successLog("UpcomingBookings screen loaded")
        const isSkipped = await getSkipped();
        if (isSkipped === 'true') {
            this.goToLogin()
        } else {
            this.props.getUpcomingBookings();
            this.props.getPastBookings();
        }
        TranslationConfig(this.props.lang)
    }


    onRefresh() {
        this.setState({ isFetching: true });
        this.props.getUpcomingBookings();
        this.props.getPastBookings();
        this.setState({ isFetching: false })
    }


    goToLogin = () => {
        const payload = {
            text: `${strings.please_login_to_continue}`,
            subtext: "",
            iconName: "closecircleo",
            modalVisible: true,
        }
        this.props.showModal(payload)
        this.props.navigation.navigate('PatientLogin')
    }

    _tAndc = (id, roomName, doctor, appointmentDate) => {
        methodLog("join call")
        let payload = {
            text: "Can't join call for future appointments!!",
            subtext: "",
            iconName: "closecircleo",
            modalVisible: true,
        };
        this.setState({ modal: !this.state.modal });


        // if (moment(new Date()).format("YYYY-MM-DD") !=
        //     moment(appointmentDate).format("YYYY-MM-DD")) {
        //     this.props.showModal(payload);
        //     return;
        // }

        if (roomName === null || roomName === undefined) {
            payload = { ...payload, text: "room url not found" }
            this.props.showModal(payload);
            return;
        }
        this.props.show();
        this.props.setSelectedAppointment(id);
        this.props.setCallHeader(
            doctor !== undefined ? `${doctor.fullName}` : ""
        );

        this.state.connect = true;
        try {
            database()
                .ref(`/doctor/${id}`)
                .on("value", (snapshot) => {
                    console.log(JSON.stringify(snapshot));
                    if (snapshot.val() !== null && snapshot.val().status === "APPROVED") {
                        if (this.state.connect) {
                            this.state.connect = false;
                            this.props.hide();
                            let { fullName, image } = checkUserProfile(this.props.userProfile);
                            // add status joined
                            addUserLogToFirebase(id, { event: JOINED, name: `${fullName} (Patient)` }).then(r => {
                                // move if already approved to call.
                                this.addUserInList(id).then((r) => this.redirect(roomName, id));
                            })

                        }
                    } else {
                        if (this.state.connect) {
                            this.state.connect = false;
                            let obj = { [id]: { status: "WAITING" } };
                            //// put user in waiting list.
                            database()
                                .ref("/doctor")
                                .update(obj)
                                .then((obj) => {
                                    let { fullName, image } = checkUserProfile(this.props.userProfile);
                                    /// put user log in array
                                    addUserLogToFirebase(id, {
                                        event: WAITING,
                                        name: `${fullName} (Patient)`
                                    }).then(r => {
                                        //// put user details in list.
                                        this.addUserInList(id).then((_) => {
                                            this.props.hide();
                                            this.props.updateAppointmentPatient(
                                                id,
                                                this.props.navigation,
                                                "WAITING",
                                                roomName,
                                                doctor
                                            );
                                        });
                                    })

                                });
                        }
                    }
                });
        } catch (e) {
            recordError(e)
        }
    };

    addUserInList = async (id) => {
        methodLog("addUserInList")
        let { fullName, image } = checkUserProfile(this.props.userProfile);
        console.log(fullName + image);
        await database()
            .ref(`/users/${id}`)
            .update({ patient: { fullName: ifValid(fullName) ? fullName : '', image: ifValid(image) ? image : '' } })
            .then((obj) => {
            });
    };

    onModal = () => {
        methodLog("onModal")
        this.setState({ modal: !this.state.modal });
    };

    redirect = (roomName, id) => {
        methodLog("redirect")
        this.props.navigation.navigate("JoinCall", {
            channel: roomName,
            user: "PATIENT_DIRECT",
            bookingId: id,
        });
    };

    keyExtractor = (item, index) => index.toString();

    renderItem = ({ item }) => {
        // if (ifNotValid(item))
        //     return <View/>
        let doctorId = ifNotValid(item.doctorId) ? {} : item.doctorId;

        // if (ifNotValid(doctorId))
        //     return <View/>

        let fullName = doctorId.fullName
        ifNotValid(fullName) ? fullName = "-" : fullName

        let hinFullName = doctorId.hinFullName
        ifNotValid(hinFullName) ? hinFullName = fullName : hinFullName

        let imageUrl = doctorId.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = Images.user
        else imageUrl = { uri: imageUrl }

        let isInvalid = false;
        let day = item.appointmentDate
        if (ifValid(day)) day = moment(item.appointmentDate).format("dddd")
        else isInvalid = true;

        let buttonEnabled = false;
        if (ifValid(item.utcDate)) {
            let hours = moment().diff(moment(item.utcDate), 'hours');
            successLog("HOSRUS  + " + hours)
            if (hours === 0) {
                buttonEnabled = true
            }
        }
        let date = item.appointmentDate
        if (ifValid(date)) date = moment(item.appointmentDate).format("D MMM")
        else isInvalid = true

        let time = item.startTime
        if (ifValid(time)) time = moment(item.startTime, ["h:mm A"]).format("hh:mm A");
        else isInvalid = true

        let callType = item?.callType

        return (
            <ListItem
                containerStyle={{ backgroundColor: '#F9F9F9' }}
                style={{ margin: 1 }}
                onPress={() => {
                    this.props.setUpcomingPastDetailView('UPCOMING')
                    this.props.navigation.navigate("AppointmentDetails", { data: item })
                }}
                title={<PowerTranslator hinText={hinFullName} enText={fullName} numberOfLines={1} />}
                titleStyle={{
                    fontSize: FONT_SIZE_16,
                    paddingBottom: 5,
                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                    color: "#041A32",
                }}
                subtitle={
                    <View>
                        {
                            isInvalid === true ?
                                <Text>...</Text> :
                                <>
                                    <Text style={styles.subTitle}>
                                        <Icon style={[styles.subIcon]} type="AntDesign"
                                            name="calendar" /> {day}, {date}, <Icon style={styles.subIcon} type="Feather"
                                                name="clock" /> {time}
                                    </Text>
                                    {callType == "Quick" && <View style={{
                                        backgroundColor: '#038BEF',
                                        alignSelf: 'flex-start',
                                        paddingVertical: 3,
                                        paddingHorizontal: 5,
                                        marginVertical: 3,
                                        borderRadius: 8
                                    }}>
                                        <Text style={{ fontSize: 12, color: '#FFF' }}>{"Quick Call"}</Text>
                                    </View>}
                                </>
                        }

                    </View>
                }
                leftAvatar={
                    <Image source={imageUrl} style={{ width: 57, height: 57, borderRadius: 28.5 }} />
                }
                bottomDivider
                rightElement={
                    buttonEnabled
                        ? <TouchableOpacity
                            onPress={() => {

                                // user can't join future calls
                                if (!moment(item.appointmentDate).isSame(moment(), 'day')) {
                                    this.props.showModal(errorPayload('Please check appointment date '))
                                    return;
                                }


                                const body = {
                                    doctorId: doctorId?._id,
                                    type: 'JOINCALL',
                                    appointmentId: item?._id
                                }
                                this.props.sendNotification(body)
                                this.props.setTcModal({
                                    visible: true,
                                    appointmentId: item._id,
                                    roomName: item.doctorId.roomName,
                                    doctor: item.doctorId,
                                    appointmentDate: item.appointmentDate,
                                    intakeInfo: item.intakeInfo
                                })
                                // this.setState({
                                //     modal: !this.state.modal,
                                //     id: item._id,
                                //     roomName: item.doctorId.roomName,
                                //     doctor: item.doctorId,
                                //     appointmentDate: item.appointmentDate,
                                // })
                            }
                            }
                            style={[common.intakeBtn, { backgroundColor: "#FF5E38" }]}>
                            <Text style={[common.intakeBtnText, { color: '#fff', paddingHorizontal: 4 }]}>{strings.Join_Call}</Text>
                        </TouchableOpacity>
                        : item?.appointmentType != "RESCHEDULED"
                            ? <TouchableOpacity
                                onPress={() => {
                                    let speciality = ifValid(item?.doctorId?.specialityId) ? item?.doctorId?.specialityId : item?.specialization;
                                    if (ifNotValid(speciality)) speciality = ""
                                    let doc = {
                                        specialization: speciality,
                                        gender: item?.doctorId?.gender,
                                        imageUrl: item?.doctorId?.imageUrl,
                                        experience: item?.doctorId?.experience,
                                        fullName: item?.doctorId?.fullName,
                                        _id: item?.doctorId?._id,
                                        specialityId: item?.doctorId?.specialityId,
                                    }

                                    this.props.setSelectedDoctor(doc)

                                    const spec = doc?.doctorId?.specialityId;
                                    let m_price = 0
                                    this.props.selectedSpecialityObj({ ...spec, price: m_price, m_price: m_price, dateTime: new Date() })
                                    this.props.setFollowUp({
                                        isFollowUp: false,
                                        from_reschedule: true,
                                        appointment_id: item?._id,
                                        appointmentDate: item?.appointmentDate,
                                        patientId: item?.patientId?._id,
                                        doctorId: item?.doctorId?._id,
                                        slotId: item?.slotId?.[0]?._id,
                                    })
                                    this.props.navigation.navigate("BookAppointmentScreenFlow2", {
                                        doc
                                    })
                                }}
                                style={[common.intakeBtn, { backgroundColor: "#038BEF" }]}>
                                <Text style={[common.intakeBtnText, { color: '#fff', paddingHorizontal: 4 }]}>{strings.reschedule_call}</Text>
                            </TouchableOpacity>
                            : null

                }
            />
        );
    };

    openMaps = (lat, long) => {
        const location = `${lat},${long}`
        const url = Platform.select({
            ios: `maps:${location}`,
            android: `geo:${location}?center=${location}&q=${location}&z=16`,
        });
        Linking.openURL(url);
    }

    renderItemUpcoming = ({ item, index }) => {
        let doctorId = ifNotValid(item.doctorId) ? {} : item.doctorId;

        let fullName = isValid(doctorId.fullName) ? doctorId.fullName : "-"

        let hinFullName = isValid(doctorId.hinFullName) ? doctorId.hinFullName : "-"
        let specialityNameArray = doctorId?.specialityId?.length > 0 ? doctorId?.specialityId : "-"

        let imageUrl = isValid(doctorId.imageUrl) ? { uri: doctorId.imageUrl } : Images.user;

        let isInvalid = false;
        let day = item.appointmentDate
        if (ifValid(day)) day = moment(item.appointmentDate).format("ddd")
        else isInvalid = true;

        let buttonEnabled = false;
        if (ifValid(item.utcDate)) {
            let hours = moment().diff(moment(item.utcDate), 'hours');
            successLog("HOSRUS  + " + hours)
            if (hours === 0) {
                buttonEnabled = true
            }
        }
        let date = item.appointmentDate
        if (ifValid(date)) date = moment(item.appointmentDate).format("D MMM YYYY")
        else isInvalid = true

        let time = item.startTime
        if (ifValid(time)) time = moment(item.startTime, ["h:mm A"]).format("hh:mm A");
        else isInvalid = true

        let callType = item?.callType
        let appointType = item?.callType == 'Online'
            ? "(Online)"
            : item?.callType == "Offline"
                ? "(Clinic Visit)"
                : ""

        let intakeForm = item?.intakeInfo
        let clinitAddress = item?.doctorId?.clinicLocation?.address
        let clinicLatLong = item?.doctorId?.clinicLocation?.latlong

        return <TouchableOpacity style={{
            backgroundColor: '#FFF',
            marginHorizontal: 20,
            marginVertical: 10,
            padding: 15,
            borderWidth: 1,
            borderColor: '#E8E8E8'
        }}
            onPress={() => {
                this.props.setUpcomingPastDetailView('UPCOMING')
                this.props.navigation.navigate("AppointmentDetails", { data: item })
            }}
        >
            <View>
                <View style={rowStyle}>
                    <Text style={{
                        fontSize: 16,
                        paddingBottom: 5,
                        fontWeight: 'bold',
                        color: "#000",
                    }}>{`Appointment ${index + 1}`}</Text>
                    <Text style={{
                        fontSize: 16,
                        paddingBottom: 5,
                        color: "#9FA9BE",
                        flex: 1,
                    }}>{` ${appointType}`}</Text>

                    {
                        buttonEnabled && callType == "Online"
                            ? <TouchableOpacity
                                onPress={() => {

                                    // user can't join future calls
                                    if (!moment(item.appointmentDate).isSame(moment(), 'day')) {
                                        this.props.showModal(errorPayload('Please check appointment date '))
                                        return;
                                    }


                                    const body = {
                                        doctorId: doctorId?._id,
                                        type: 'JOINCALL',
                                        appointmentId: item?._id
                                    }
                                    this.props.sendNotification(body)
                                    this.props.setTcModal({
                                        visible: true,
                                        appointmentId: item._id,
                                        roomName: item.doctorId.roomName,
                                        doctor: item.doctorId,
                                        appointmentDate: item.appointmentDate,
                                        intakeInfo: item.intakeInfo
                                    })
                                    // this.setState({
                                    //     modal: !this.state.modal,
                                    //     id: item._id,
                                    //     roomName: item.doctorId.roomName,
                                    //     doctor: item.doctorId,
                                    //     appointmentDate: item.appointmentDate,
                                    // })
                                }
                                }
                                style={[common.intakeBtn, { backgroundColor: "#FF5E38" }]}>
                                <Text style={[common.intakeBtnText, { color: '#fff', paddingHorizontal: 4 }]}>{strings.Join_Call}</Text>
                            </TouchableOpacity>
                            : item?.appointmentType != "RESCHEDULED"
                                ? <TouchableOpacity
                                    onPress={() => {
                                        let speciality = ifValid(item?.doctorId?.specialityId) ? item?.doctorId?.specialityId : item?.specialization;
                                        if (ifNotValid(speciality)) speciality = ""
                                        let doc = {
                                            specialization: speciality,
                                            gender: item?.doctorId?.gender,
                                            imageUrl: item?.doctorId?.imageUrl,
                                            experience: item?.doctorId?.experience,
                                            fullName: item?.doctorId?.fullName,
                                            _id: item?.doctorId?._id,
                                            specialityId: item?.doctorId?.specialityId,
                                        }

                                        this.props.setSelectedDoctor(doc)

                                        const spec = doc?.doctorId?.specialityId;
                                        let m_price = 0
                                        this.props.selectedSpecialityObj({ ...doc, price: m_price, m_price: m_price, dateTime: new Date(), bookNow: false })
                                        this.props.setFollowUp({
                                            isFollowUp: false,
                                            from_reschedule: true,
                                            appointment_id: item?._id,
                                            appointmentDate: item?.appointmentDate,
                                            patientId: item?.patientId?._id,
                                            doctorId: item?.doctorId?._id,
                                            slotId: item?.slotId?.[0]?._id,
                                        })
                                        this.props.navigation.navigate("BookAppointmentScreenFlow2", {
                                            doc
                                        })
                                    }}
                                    style={[common.intakeBtn, { backgroundColor: "#038BEF" }]}>
                                    <Text style={[common.intakeBtnText, { color: '#fff', paddingHorizontal: 4 }]}>{strings.reschedule_call}</Text>
                                </TouchableOpacity>
                                : null
                    }

                </View>

                <View style={[rowStyle, { marginTop: 10 }]}>
                    <View style={{ flex: 1 }}>
                        <Text style={{
                            fontSize: 14,
                            paddingBottom: 5,
                            color: "#9FA9BE",
                        }}>{"Patient's Name"}</Text>
                        <Text style={{
                            fontSize: 15,
                            paddingBottom: 5,
                            color: "#000",
                        }}>{intakeForm?.name}</Text>
                    </View>

                    <View style={{}}>
                        <View style={rowStyle}>
                            <Icon style={[styles.subIcon]} type="AntDesign" name="calendar" />
                            <Text style={[styles.subTitle, { color: '#586278' }]}> {day}, {date}</Text>
                        </View>
                        <View style={[rowStyle, { justifyContent: 'flex-end', marginTop: 5, }]}>
                            <Icon style={styles.subIcon} type="Feather" name="clock" />
                            <Text style={[styles.subTitle, { color: '#586278' }]}> {time}</Text>
                        </View>
                    </View>
                </View>

                <Line style={{ borderColor: '#9FA9BE', marginVertical: 8 }} />

                <View style={[rowStyle, { marginTop: 10, marginBottom: 5 }]}>
                    <View style={{ flex: 1 }}>
                        <View style={rowStyle}>
                            <Image source={imageUrl} style={{ width: 57, height: 57, borderRadius: 28.5 }} />
                            <View style={{ flex: 1, marginHorizontal: 5 }}>
                                <PowerTranslator hinText={hinFullName} enText={fullName} numberOfLines={1} style={{ fontWeight: '500', fontSize: 15 }} />
                                <PowerTranslator
                                    hinText={getSpecialitiesNames(specialityNameArray, "hindiName")}
                                    enText={getSpecialitiesNames(specialityNameArray, "name")}
                                    style={[styles.subTitle, { color: '#586278' }]}
                                />
                            </View>
                        </View>
                    </View>
                    {callType == "Quick" && <View style={{
                        alignSelf: 'flex-start',
                        paddingVertical: 3,
                        paddingHorizontal: 5,
                        marginVertical: 3,
                        borderRadius: 8
                    }}>
                        <Text style={{ fontSize: 15, color: '#038BEF', }}>{"Quick Call"}</Text>
                    </View>}
                </View>


                {
                    callType == "Offline" && <>
                        <Line style={{ borderColor: '#9FA9BE', marginVertical: 8 }} />
                        <Accordion
                            style={{
                                borderWidth: 0
                            }}
                            dataArray={[{
                                title: "Clinit Address",
                                content: {
                                    address: clinitAddress,
                                    latlong: clinicLatLong
                                }
                            }]}
                            renderHeader={(item, expanded) => {
                                return <View style={{
                                    flexDirection: "row",
                                    paddingVertical: 5,
                                    paddingHorizontal: 8,
                                    justifyContent: "space-between",
                                    alignItems: "center",
                                }}>
                                    <Text style={{ fontWeight: "500", width: '93%', fontSize: 16, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }}>
                                        {item.title}
                                    </Text>
                                    {expanded
                                        ? <Icon style={{ fontSize: 22, color: '#038BEF' }} name="upcircleo" type='AntDesign' />
                                        : <Icon style={{ fontSize: 22, color: '#038BEF' }} name="downcircleo" type='AntDesign' />}
                                </View>
                            }}
                            renderContent={(item) => {
                                return (
                                    <View style={{ paddingHorizontal: 8 }}>
                                        <Text
                                            style={{
                                                paddingVertical: 10,
                                                fontSize: 15,
                                                // fontStyle: "italic",
                                            }}
                                        >
                                            {item?.content?.address}
                                        </Text>
                                        <TouchableOpacity
                                            style={rowStyle}
                                            onPress={() => {
                                                this.openMaps(item?.content?.latlong?.lat, item?.content?.latlong?.lng)
                                            }}
                                        >
                                            <Icon style={{ fontSize: 22, color: '#038BEF', paddingRight: 5 }} name="map-pin" type='Feather' />
                                            <Text style={{
                                                fontSize: 15,
                                                color: '#038BEF'
                                            }}>View On Maps</Text>
                                        </TouchableOpacity>
                                    </View>
                                );
                            }}
                        />
                    </>
                }
            </View>

        </TouchableOpacity>
    }

    renderItemPast = ({ item }) => {
        let doctorId = ifNotValid(item.doctorId) ? {} : item.doctorId;

        let fullName = doctorId.fullName
        ifNotValid(fullName) ? fullName = "-" : fullName

        let hinFullName = doctorId.hinFullName
        ifNotValid(hinFullName) ? hinFullName = fullName : hinFullName

        let imageUrl = doctorId.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = Images.user
        else imageUrl = { uri: imageUrl }

        let day = item.appointmentDate
        ifValid(day) ? day = moment(item.appointmentDate).format("dddd") : '...'

        let date = item.appointmentDate
        ifValid(date) ? date = moment(item.appointmentDate).format("D MMM") : '...'

        let time = item.startTime
        ifValid(time) ? time = moment(item.startTime, ["h:mm A"]).format("hh:mm A") : '...'

        let pdfUploaded = ifValid(item.prescriptionId) ? item.prescriptionId.pdfUploaded : undefined;

        var app_date = item?.appointmentDate;
        var today = moment().format("YYYY-MM-DD");
        var diff = moment(today).diff(app_date, 'days')

        let dateCheck = (diff > 3)
        let d_b = item?.followUp == undefined ? true : item?.followUp
        let canFollowUp = (dateCheck || d_b) === true ? false : true

        return (
            <ListItem
                containerStyle={{ backgroundColor: '#F9F9F9' }}
                style={{ margin: 1 }}
                onPress={() => {
                    this.props.navigation.navigate("AppointmentDetails", { data: item })
                    this.props.setUpcomingPastDetailView('PAST')
                }}
                title={<PowerTranslator hinText={hinFullName} enText={fullName} numberOfLines={1} />}
                titleStyle={{
                    fontSize: FONT_SIZE_16,
                    paddingBottom: 5,
                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                    color: "#041A32",

                }}
                subtitle={
                    <View>
                        <Text style={styles.subTitle}>
                            <Icon style={[styles.subIcon]} type="AntDesign" name="calendar" /> {day}, {date} <Icon
                                style={styles.subIcon} type="Feather" name="clock" /> {time}
                        </Text>
                        <TouchableOpacity
                            onPress={() => {
                                if (ifNotValid(pdfUploaded)) {
                                    this.props.showModal(errorPayload("Prescription not added yet"))
                                    return;
                                }
                                this.props.requestReadWritePermissionAndDownload(pdfUploaded)
                            }}>
                            <Text style={{
                                color: ifValid(pdfUploaded) ? '#038BEF' : 'gray',
                                fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                                fontSize: 15
                            }}>{strings.prescription_download} <Icon name={'download'} type={"Feather"}
                                style={{ color: '#FF5E38', fontSize: 22 }} />
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                if (canFollowUp) {
                                    this.followUpPressed(item)
                                } else {
                                    this.props.showModal(errorPayload("You can not follow up this appointment"))
                                }
                            }}
                        >
                            <Text
                                style={{
                                    // borderWidth: 1,
                                    color: canFollowUp ? '#038BEF' : 'gray',
                                    paddingVertical: 5,
                                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                                    fontSize: 15
                                }}
                            >Follow Up</Text>
                        </TouchableOpacity>
                    </View>
                }
                leftAvatar={
                    <Image source={imageUrl} style={{ width: 57, height: 57, borderRadius: 28.5 }} />
                }
                bottomDivider
            // rightElement={
            //     <TouchableOpacity
            //         style={[common.intakeBtn, {backgroundColor: "#C70315"}]}>
            //         <Text style={[common.intakeBtnText, {color: '#fff', paddingHorizontal: 4}]}>Follow Up</Text>
            //     </TouchableOpacity>
            // }
            />
        );
    };

    renderItemPastNew = ({ item, index }) => {
        let doctorId = ifNotValid(item.doctorId) ? {} : item.doctorId;

        let fullName = doctorId.fullName
        ifNotValid(fullName) ? fullName = "-" : fullName

        let hinFullName = doctorId.hinFullName
        ifNotValid(hinFullName) ? hinFullName = fullName : hinFullName

        let imageUrl = doctorId.imageUrl;
        if (ifNotValid(imageUrl)) imageUrl = Images.user
        else imageUrl = { uri: imageUrl }

        let day = item.appointmentDate
        ifValid(day) ? day = moment(item.appointmentDate).format("dddd") : '...'

        let date = item.appointmentDate
        ifValid(date) ? date = moment(item.appointmentDate).format("D MMM") : '...'

        let time = item.startTime
        ifValid(time) ? time = moment(item.startTime, ["h:mm A"]).format("hh:mm A") : '...'

        let pdfUploaded = isValid(item.prescriptionId)
            ? item.prescriptionId?.imageURL?.length > 0 || isValid(item.prescriptionId?.pdfUploaded)
            : false
        let pdfUploadedData = item?.prescriptionId?.pdfUploaded;
        let imageURLData = item?.prescriptionId?.imageURL;

        var app_date = item?.appointmentDate;
        var today = moment().format("YYYY-MM-DD");
        var diff = moment(today).diff(app_date, 'days')

        let dateCheck = (diff > 3)
        let d_b = item?.followUp == undefined ? true : item?.followUp
        let canFollowUp = (dateCheck || d_b) === true ? false : true

        let specialityNameArray = doctorId?.specialityId?.length > 0 ? doctorId?.specialityId : "-"

        let callType = item?.callType
        let appointType = item?.callType == 'Online'
            ? "(Online)"
            : item?.callType == "Offline"
                ? "(Clinic Visit)"
                : ""

        let patienName = item?.patientId?.fullName

        let intakeForm = isValid(item?.intakeInfo) ? item?.intakeInfo : { name: patienName }
        let clinitAddress = item?.doctorId?.clinicLocation?.address
        let clinicLatLong = item?.doctorId?.clinicLocation?.latlong

        return (
            <View>
                <TouchableOpacity style={{
                    backgroundColor: '#FFF',
                    marginHorizontal: 20,
                    marginVertical: 10,
                    padding: 15,
                    borderWidth: 1,
                    borderColor: '#E8E8E8'
                }}
                    onPress={() => {
                        this.props.navigation.navigate("AppointmentDetails", { data: item })
                        this.props.setUpcomingPastDetailView('PAST')
                    }}
                >
                    <View>
                        <View style={rowStyle}>
                            <Text style={{
                                fontSize: 16,
                                paddingBottom: 5,
                                fontWeight: 'bold',
                                color: "#000",
                            }}>{`Appointment ${index + 1}`}</Text>
                            <Text style={{
                                fontSize: 16,
                                paddingBottom: 5,
                                color: "#9FA9BE",
                                flex: 1,
                            }}>{` ${appointType}`}</Text>

                        </View>

                        <View style={[rowStyle, { marginTop: 10 }]}>
                            <View style={{ flex: 1 }}>
                                <Text style={{
                                    fontSize: 14,
                                    paddingBottom: 5,
                                    color: "#9FA9BE",
                                }}>{"Patient's Name"}</Text>
                                <Text style={{
                                    fontSize: 15,
                                    paddingBottom: 5,
                                    color: "#000",
                                }}>{intakeForm?.name}</Text>
                            </View>

                            <View style={{}}>
                                <View style={rowStyle}>
                                    <Icon style={[styles.subIcon]} type="AntDesign" name="calendar" />
                                    <Text style={[styles.subTitle, { color: '#586278' }]}> {day}, {date}</Text>
                                </View>
                                <View style={[rowStyle, { justifyContent: 'flex-end', marginTop: 5, }]}>
                                    <Icon style={styles.subIcon} type="Feather" name="clock" />
                                    <Text style={[styles.subTitle, { color: '#586278' }]}> {time}</Text>
                                </View>
                            </View>
                        </View>

                        <Line style={{ borderColor: '#9FA9BE', marginVertical: 8 }} />

                        <View style={[rowStyle, { marginTop: 10, marginBottom: 5 }]}>
                            <View style={{ flex: 1 }}>
                                <View style={rowStyle}>
                                    <Image source={imageUrl} style={{ width: 57, height: 57, borderRadius: 28.5 }} />
                                    <View style={{ flex: 1, marginHorizontal: 5 }}>
                                        <PowerTranslator hinText={hinFullName} enText={fullName} numberOfLines={1} style={{ fontWeight: '500', fontSize: 15 }} />
                                        <PowerTranslator
                                            hinText={getSpecialitiesNames(specialityNameArray, "hindiName")}
                                            enText={getSpecialitiesNames(specialityNameArray, "name")}
                                            style={[styles.subTitle, { color: '#586278', flex: 1, }]}
                                        />
                                    </View>
                                </View>
                            </View>
                            {callType == "Quick" && <View style={{
                                alignSelf: 'flex-start',
                                paddingVertical: 3,
                                paddingHorizontal: 5,
                                marginVertical: 3,
                                borderRadius: 8
                            }}>
                                <Text style={{ fontSize: 15, color: '#038BEF', }}>{"Quick Call"}</Text>
                            </View>}
                        </View>

                        <Line style={{ borderColor: '#9FA9BE', marginTop: 8 }} />

                        {
                            appointType == "Offline" && <>
                                <Accordion
                                    style={{
                                        borderWidth: 0,
                                        marginTop: 3
                                    }}
                                    dataArray={[{
                                        title: "Clinit Address",
                                        content: {
                                            address: clinitAddress,
                                            latlong: clinicLatLong
                                        }
                                    }]}
                                    renderHeader={(item, expanded) => {
                                        return <View style={{
                                            flexDirection: "row",
                                            paddingVertical: 5,
                                            paddingHorizontal: 8,
                                            justifyContent: "space-between",
                                            alignItems: "center",
                                        }}>
                                            <Text style={{ fontWeight: "500", width: '93%', fontSize: 16, fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }}>
                                                {item.title}
                                            </Text>
                                            {expanded
                                                ? <Icon style={{ fontSize: 22, color: '#038BEF' }} name="upcircleo" type='AntDesign' />
                                                : <Icon style={{ fontSize: 22, color: '#038BEF' }} name="downcircleo" type='AntDesign' />}
                                        </View>
                                    }}
                                    renderContent={(item) => {
                                        return (
                                            <View style={{ paddingHorizontal: 8 }}>
                                                <Text
                                                    style={{
                                                        paddingVertical: 10,
                                                        fontSize: 15,
                                                        // fontStyle: "italic",
                                                    }}
                                                >
                                                    {item?.content?.address}
                                                </Text>
                                                <TouchableOpacity
                                                    style={rowStyle}
                                                    onPress={() => {
                                                        this.openMaps(item?.content?.latlong?.lat, item?.content?.latlong?.lng)
                                                    }}
                                                >
                                                    <Icon style={{ fontSize: 22, color: '#038BEF', paddingRight: 5 }} name="map-pin" type='Feather' />
                                                    <Text style={{
                                                        fontSize: 15,
                                                        color: '#038BEF'
                                                    }}>View On Maps</Text>
                                                </TouchableOpacity>
                                            </View>
                                        );
                                    }}
                                />
                                <Line style={{ borderColor: '#9FA9BE', marginTop: 8 }} />
                            </>
                        }
                    </View>


                    <View style={{
                        marginTop: 10,
                        marginHorizontal: 10
                    }}>
                        <TouchableOpacity
                            onPress={() => {
                                if (pdfUploaded == false) {
                                    this.props.showModal(errorPayload("Prescription not added yet"))
                                    return;
                                }
                                if (imageURLData?.length > 0) {
                                    this.onPressDownloadImageprescriptions(imageURLData)
                                }
                                if (isValid(pdfUploadedData)) {
                                    this.props.requestReadWritePermissionAndDownload(pdfUploadedData)
                                }
                            }}>
                            <Text style={{
                                color: pdfUploaded == true ? '#038BEF' : 'gray',
                                fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                                fontSize: 15
                            }}>{strings.prescription_download} <Icon name={'download'} type={"Feather"}
                                style={{ color: '#FF5E38', fontSize: 22 }} />
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => {
                                if (canFollowUp) {
                                    this.followUpPressed(item)
                                } else {
                                    this.props.showModal(errorPayload("You can not follow up this appointment"))
                                }
                            }}
                        >
                            <Text
                                style={{
                                    // borderWidth: 1,
                                    color: canFollowUp ? '#038BEF' : 'gray',
                                    paddingVertical: 5,
                                    fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
                                    fontSize: 15
                                }}
                            >Follow Up</Text>
                        </TouchableOpacity>
                    </View>

                </TouchableOpacity>
            </View>
        );
    };


    noBookings = () => {
        return (
            <View style={styles.noBookingsBlock}>
                <Image source={Images.no_bookings} style={styles.noBookingsImage} />
                <Text style={styles.noBookingsTitle}>{strings.No_appointments}</Text>
                <Text style={styles.noBookingsSubTitle}>
                    {strings.No_appointments2}
                </Text>
            </View>
        );
    };

    followUpPressed = (item) => {
        console.log("follow pressed appointment " + JSON.stringify(item));
        let speciality = ifValid(item?.doctorId?.specialityId) ? item?.doctorId?.specialityId : item?.specialization;
        if (ifNotValid(speciality)) speciality = ""
        let doc = {
            specialization: speciality,
            gender: item?.doctorId?.gender,
            imageUrl: item?.doctorId?.imageUrl,
            experience: item?.doctorId?.experience,
            fullName: item?.doctorId?.fullName,
            _id: item?.doctorId?._id,
            specialityId: item?.doctorId?.specialityId,
        }

        this.props.setSelectedDoctor(doc)

        const spec = doc?.doctorId?.specialityId;
        let m_price = 0
        this.props.selectedSpecialityObj({ ...spec, price: m_price, m_price: m_price, dateTime: new Date(), bookNow: false })
        this.props.setFollowUp({
            isFollowUp: true,
            appointment_id: item?._id
        })
        this.props.navigation.navigate("BookAppointmentScreenFlow2", {
            doc
        })
    }

    onPressDownloadImageprescriptions = async (images) => {
        console.log('images here ', images);

        if (isValid(images)) {
            this.props.show()
            if (Platform.Version > 32) {
                this.downloadImages(images)
            } else {
                const typess = [PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE, PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE]
                const permiss = await checkMultiple(typess)

                console.log('permiss ', permiss);

                if (permiss["android.permission.READ_EXTERNAL_STORAGE"] === RESULTS.GRANTED && permiss["android.permission.WRITE_EXTERNAL_STORAGE"] === RESULTS.GRANTED) {
                    this.downloadImages(images)
                } else if (permiss["android.permission.READ_EXTERNAL_STORAGE"] === RESULTS.BLOCKED && permiss["android.permission.WRITE_EXTERNAL_STORAGE"] === RESULTS.BLOCKED) {
                    this.props.hide()
                    this.showSettingsAlert()
                } else {
                    requestMultiple(typess)
                        .then(() => this.downloadImages(images))
                        .catch(err => {
                            console.log(err);
                            this.props.hide()
                        })
                }
            }
        }
    }

    showSettingsAlert = () => {
        Alert.alert(
            "Permission required!",
            "Please click open settings and allow storage permission under Permissions",
            [
                {
                    text: "Open Settings",
                    onPress: () => openSettings(),
                    style: "cancel",
                },
            ],
            {
                cancelable: true,
            }
        );
    }

    downloadImages = (images) => {
        if (images?.length > 0) {
            const prms = [];

            images?.map(downUrl => {
                const promise = new Promise((resolve, reject) => {
                    const dirs = RNFetchBlob.fs.dirs.DownloadDir;
                    const localPath = dirs + `/${uuidv4()}.jpg`
                    RNFetchBlob
                        .config({
                            path: localPath,
                            addAndroidDownloads: {
                                useDownloadManager: true,
                                path: localPath,
                                notification: true,
                                mediaScannable: true,
                            }
                        })
                        .fetch('GET', downUrl)
                        .progress((received, total) => {
                            let percentage = (received / total) * 100
                            let roundPercentage = Math.round(percentage)
                            console.log('progress', roundPercentage)
                        })
                        .then((res) => {
                            resolve(res?.path())
                        })
                        .catch((err) => {
                            console.log('err  ', err)
                            reject(err)
                        })
                })
                prms.push(promise)
            })

            Promise.all(prms)
                .then(results => {
                    this.props.hide()
                    const payload = {
                        text: `Prescription downloaded on your device`,
                        subtext: "",
                        iconName: "checkcircleo",
                        modalVisible: true,
                    }
                    this.props.showModal(payload)
                    console.log('results ', results);
                })
                .catch(err => {
                    console.log(err);
                    this.props.hide()
                })
        }
    }

    render() {
        let bookings = false;
        let upcomingBookings = this.props.upcomingBookings;
        let pastBookings = this.props.pastBookings;
        ifValid(upcomingBookings) && upcomingBookings.length > 0 ? bookings = true : bookings = true
        let { activeButton } = this.state;
        return (
            <Container style={{ backgroundColor: '#F9F9F9' }}>
                <HeaderSehet headerText={strings.my_appointments} navigation={this.props.navigation} changeTheme={true} />

                <SegmentCustom />
                {bookings ? (
                    <View style={{ flex: 1, backgroundColor: '#F9F9F9' }}>
                        <FlatList
                            style={{ backgroundColor: '#F9F9F9' }}
                            refreshing={this.state.isFetching}
                            scrollEnabled
                            onRefresh={() => this.onRefresh()}
                            keyExtractor={this.keyExtractor}
                            data={this.props.activeUpcomingOrPast === "UPCOMING" ? this.props.upcomingBookings : this.props.pastBookings}
                            renderItem={this.props.activeUpcomingOrPast === "UPCOMING" ? this.renderItemUpcoming : this.renderItemPastNew}
                            ListEmptyComponent={this.noBookings}
                        // renderItem={this.renderItem}
                        />
                    </View>
                ) : (
                    this.noBookings()
                )}

                {/* <Modal visible={this.state.modal} animationType="slide">
                    <SafeAreaView style={styles.modalContainer}>
                        <View style={styles.modalheader}>
                            <Icon
                                name="close"
                                type="AntDesign"
                                style={styles.modalheaderBackIcon}
                                onPress={this.onModal}
                            />
                            <Text style={styles.modalheading}>
                                Consent For Telehealth Consultation
                            </Text>
                        </View>
                        <ScrollView contentContainerStyle={styles.modal}>
                            <Text style={styles.modalText}>1. I understand that my Physical recommends engaging in
                                telehealth services with me to provide treatment</Text>
                            <Text style={styles.modalText}>2. I understand this is out of necessity and an abundance of
                                caution and has originated due to the Coronavirus (Covid-19) pandemic. This will
                                continue until such time that we are able to meet in person, or could continue,
                                depending on the particular circumstance.</Text>
                            <Text style={styles.modalText}>3. I understand that telehealth treatment has potential
                                benefits including, but not limited to, easier access to care.</Text>
                            <Text style={styles.modalText}>4. I understand that telehealth has been found to be
                                effective in treating a wide range of disorders, and there are potential benefits
                                including, but not limited to easier access to care. I understand; however, there is no
                                guarantee that all treatment of all patients will be effective.</Text>
                            <Text style={styles.modalText}>5. I understand that it is my obligation to notify my
                                Physical of my location at the beginning of each treatment session. If for some reason,
                                I change locations during the session, it is my obligation to notify my [insert
                                discipline] of the change in location.</Text>
                            <Text style={styles.modalText}>6. I understand that it is my obligation to notify my
                                Physical of any other persons in the location, either on or off camera and who can hear
                                or see the session. I understand that I am responsible to ensure privacy at my location.
                                I will notify my [insert discipline] at the outset of each session and am aware that
                                confidential information may be discussed.</Text>
                            <Text style={styles.modalText}>7. I understand that it is my obligation to ensure that any
                                virtual assistant artificial intelligence devices, including but not limited to Alexa or
                                Echo, will be disabled or will not be in the location where information can be
                                heard.</Text>
                            <Text style={styles.modalText}>8. I agree that I will not record either through audio or
                                video any of the session, unless I notify my Physical and this is agreed upon.</Text>
                            <Text style={styles.modalText}>9. I understand there are potential risks to using telehealth
                                technology, including but not limited to, interruptions, unauthorized access, and
                                technical difficulties. I understand
                                some of these technological challenges include issues with software, hardware, and
                                internet connection which may result in interruption.</Text>
                            <Text style={styles.modalText}>10. I understand that my [insert discipline] is not
                                responsible for any technological problems of which my Physical has no control over. I
                                further understand that my [insert discipline] does not guarantee that technology will
                                be available or work as expected.</Text>
                            <Text style={styles.modalText}>11. I understand that I am responsible for information
                                security on my device, including but not limited to, computer, tablet, or phone, and in
                                my own location.</Text>
                            <Text style={styles.modalText}>12. I understand that my [insert discipline] or I (or, if
                                applicable, my guardian or conservator), can discontinue the telehealth consult/visit if
                                it is determined by either me or my [insert discipline] that the videoconferencing
                                connections or protections are not adequate for the situation.</Text>
                            <Text style={styles.modalText}>13. I have had a conversation with my [insert discipline],
                                during which time I have had the opportunity to ask questions concerning services via
                                telehealth. My questions have been answered, and the risks, benefits, and any practical
                                alternatives have been discussed with me.</Text>
                            <Text style={styles.modalText}>14. Sehet App is the technology service we will
                                use to conduct telehealth videoconferencing appointments. My [insert discipline] has
                                discussed the use of this platform. Prior to each session, I will receive an email link
                                to enter the “waiting room” until the session begins. There are no passwords or log in
                                required.</Text>

                            <Text style={styles.modalTextSub}>By signing this document, I acknowledge:</Text>

                            <Text style={styles.modalText}>1. Pathway Healthcare App is NOT an emergency service. In the
                                event of an emergency, I will use a phone to call 9-1-1 and/or other appropriate
                                emergency contact.</Text>
                            <Text style={styles.modalText}>2. I recognize my [insert discipline] may need to notify
                                emergency personnel in the event he/she feels there is a safety concern, including but
                                not limited to, a risk to self/others or my Physical is concerned that immediate medical
                                attention is needed.</Text>
                            <Text style={styles.modalText}>3. Though my [insert discipline] and I may be in virtual
                                contact through telehealth services, neither [Name of the Telehealth Service to be used]
                                or my [insert discipline] provides any medical or emergency or urgent healthcare
                                services or advice. I understand should medical services be required, I will contact my
                                physician. If emergency services are needed, I understand I should call 9-1-1.</Text>
                            <Text style={styles.modalText}>4. The Sehet App facilitates videoconferencing
                                and this technology platform is not, itself, a source of healthcare, medical advice, or
                                care.</Text>
                            <Text style={styles.modalText}>5. I understand that the same fee rates apply for telehealth
                                as apply for in-person treatment. Some insurers are waiving co-pays during this time. It
                                is my obligation to contact my insurer before engaging in telehealth to determine if
                                there are applicable co-pays or fees which I am responsible for. Insurance or other
                                managed care providers may not cover telehealth sessions. I understand that if my
                                insurance, HMO, third-party payor, or other managed care provider do not cover the
                                telehealth sessions, I will be solely responsible for the entire fee of the
                                session.</Text>
                            <Text style={styles.modalText}>6. During these times of the impact of Coronavirus (Covid-19)
                                my [insert discipline] may not have access to all of my medical/treatment records. My
                                [insert discipline] has made reasonable efforts to obtain records, but I understand and
                                agree this may not be reasonably possible.</Text>
                            <Text style={styles.modalText}>7. To maintain confidentiality, I will not share my
                                telehealth appointment link or information with anyone not authorized to attend the
                                session.</Text>
                            <Text style={styles.modalText}>8. I understand that either I or my [insert discipline] can
                                discontinue the telehealth services if those services do not appear to benefit me
                                therapeutically or for other reasons which will be explained to me. I understand there
                                may be no other treatment alternative available.</Text>
                            <Text style={styles.modalText}>6. During these times of the impact of Coronavirus (Covid-19)
                                my [insert discipline] may not have access to all of my medical/treatment records. My
                                [insert discipline] has made reasonable efforts to obtain records, but I understand and
                                agree this may not be reasonably possible.</Text>
                            <Text style={styles.modalTextSub}>I have read and understand the information provided above
                                regarding telehealth, have discussed it with my physician, and I hereby give informed
                                consent to the use of telehealth.</Text>

                        </ScrollView>
                        <View style={{ width: '100%' }}>
                            <Button
                                primary
                                onPress={() =>
                                    this._tAndc(
                                        this.state.id,
                                        this.state.roomName,
                                        this.state.doctor,
                                        this.state.appointmentDate
                                    )
                                }
                                style={styles.modal1Close}
                            >
                                <Text uppercase={false} style={{ color: "#fff" }}>
                                    Accept & Continue
                                </Text>
                            </Button>
                        </View>
                    </SafeAreaView>
                </Modal> */}

            </Container>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            getUpcomingBookings, getPastBookings, updateAppointmentPatient,
            setSelectedAppointment, show, hide, setUpcomingPastDetailView,
            setCallHeader, setUpcomingPastActive, showModal, requestReadWritePermissionAndDownload,
            sendNotification, setSelectedDoctor, selectedSpecialityObj, setFollowUp, setTcModal
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    upcomingBookings: state.bookings.upcomingBookings,
    pastBookings: state.bookings.pastBookings,
    userProfile: state.profile.userProfile,
    activeUpcomingOrPast: state.upcomingPast.active,
    lang: state.profile.selected_language
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UpcomingPastBookings);
