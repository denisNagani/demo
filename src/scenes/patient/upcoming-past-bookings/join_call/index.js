/* eslint-disable prettier/prettier */
import React, { Component } from "react";
import { BackHandler, NativeModules, View, } from "react-native";
import { RtcEngine } from "react-native-agora";
import database from "@react-native-firebase/database";
import { bindActionCreators } from "redux";
import { completeAppointment, getBookingHistory, getUpcomingBookings, } from "../../../../actions/bookings";
import { connect } from "react-redux";
import { requestCameraAndAudioPermission } from "../../../../services/permission";
import RNBeep from "react-native-a-beep";
import { addUserLogToFirebase, checkUserProfile, handleBackPress } from "../../../../utils/helper";
import { startRecording, stopRecording } from "../../../../actions/appointment";
import VideoCommon from "../../../../components/video-common";
import { showModal } from "../../../../actions/modal";
import { getRoomURLForGuest } from "../../../../actions/share-room";
import { agora_id } from "../../../../config";
import { DISCONNECTED } from "../../../../utils/constant";
import { methodLog, recordError, successLog } from "../../../../utils/fireLog";
import { isValid } from "../../../../utils/ifNotValid";

const { Agora } = NativeModules; //Define Agora object as a native module

const { FPS30, AudioProfileDefault, AudioScenarioDefault, Adaptative } = Agora; //Set defaults for Stream

const config = {
    //Setting config of the app
    appid: agora_id, //Enter the App ID generated from the Agora Website
    channelProfile: 0, //Set channel profile as 0 for RTC
    videoEncoderConfig: {
        //Set Video feed encoder settings
        width: 720,
        height: 1080,
        bitrate: 1,
        frameRate: FPS30,
        orientationMode: Adaptative,
    },
    audioProfile: AudioProfileDefault,
    audioScenario: AudioScenarioDefault,
};

class JoinCall extends Component {
    constructor(props) {
        super(props);
        this.state = {
            peerIds: [], //Array for storing connected peers
            uid: Math.floor(Math.random() * 100), //Generate a UID for local user
            appid: config.appid,
            channelName: "", //Channel Name for the current session
            joinSucceed: false, //State variable for storing success
            audMute: false,
            vidMute: false,
            showList: false,
            userList: [],
            minutes: 0,
            seconds: 0,
            usersListFirebase: [],
            fullScreen: false,
            peerIdSelected: null,
        };
        this.state.channelName = this.props.navigation.state.params.channel;
        console.log(this.state.channelName);
        requestCameraAndAudioPermission().then((_) => {
            console.log("requested!");
            this.startCall();
        });
        this.backPressSubscriptions = new Set();
    }

    componentWillUnmount() {
        successLog("JoinCall screen loaded")
        let { fullName, image } = checkUserProfile(this.props.userProfile);
        /// put user log in array
        addUserLogToFirebase(this.props.selectedAppointment, {
            event: DISCONNECTED,
            name: `${fullName} (Patient)`
        }).then(_ => {
        })
        this.backHandler.remove();
        clearInterval(this.interval);
        this.endCall();
        database()
            .ref(`/doctor/${this.props.navigation.state.params.bookingId}`)
            .off("value");
    }

    tick = () => {
        this.setState({
            counter: this.state.counter + 1,
        });
    };

    checkCallStatus = () => {
        methodLog("checkCallStatus")
        try {
            database().ref(`/doctor/${this.props.navigation.state.params.bookingId}`)
                .on("value", (snapshot) => {
                    if (snapshot.val() !== null && snapshot.val().status === "FINISHED") {
                        const payload = {
                            text: "Appointment is ended by the doctor!!",
                            iconName: "closecircleo",
                            modalVisible: true,
                            subText: "",
                        };
                        this.props.showModal(payload)
                        this.endCall();
                    }
                });
        } catch (e) {
            recordError(e)
        }
    };

    getUsersList = () => {
        methodLog("getUsersList")
        let users = [];
        try {
            const result = database()
                .ref(`/users/${this.props.selectedAppointment}/`)
                .on("value", (snapshot) => {
                    if (snapshot.val() !== null) {
                        let data = snapshot.val();
                        users = Object.values(data);
                        this.setState({
                            usersListFirebase: users,
                        });
                    }
                });
        } catch (e) {
            recordError(e)
        }
    };

    componentDidMount() {
        successLog("Join call screen loaded")
        try {
            this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
                handleBackPress(this.props);
                return true;
            });

            this.checkCallStatus();
            this.getUsersList();
            this.interval = setInterval(() => {
                const { seconds, minutes } = this.state;
                if (seconds < 60) {
                    this.setState(({ seconds }) => ({
                        seconds: seconds + 1,
                    }));
                } else {
                    this.setState(({ minutes }) => ({
                        minutes: minutes + 1,
                        seconds: 0,
                    }));
                }
            }, 1000);

            RtcEngine.on("userJoined", (data) => {
                const { peerIds } = this.state; //Get currrent peer IDs
                if (peerIds.indexOf(data.uid) === -1) {
                    //If new user has joined
                    RNBeep.beep(false);
                    this.setState({
                        peerIds: [...peerIds, data.uid], //add peer ID to state array
                    });
                }
            });
            RtcEngine.on("userOffline", (data) => {
                //If user leaves
                RNBeep.beep(false);
                this.setState({
                    peerIds: this.state.peerIds.filter((uid) => uid !== data.uid), //remove peer ID from state array
                });
            });
            RtcEngine.on("joinChannelSuccess", (data) => {
                //If Local user joins RTC channel
                RtcEngine.startPreview(); //Start RTC preview
                RNBeep.beep(false);
                this.setState({
                    joinSucceed: true, //Set state variable to true
                });
            });
            RtcEngine.init(config); //Initialize the RTC engine

        } catch (e) {
            recordError(e)
        }
    }


    startCall = () => {
        methodLog("startCall")
        try {
            RNBeep.beep(false);
            /// this.props.startRecording(this.state.channelName, this.state.uid, this.props.navigation.state.params.bookingId);
            console.log("INSTARRT " + this.state.uid + "   " + this.state.channelName);
            // const channelUID = isValid(this.props.userProfile?.index) ? this.props.userProfile?.index : this.state.uid;
            const channelUID = this.state.uid;
            RtcEngine.joinChannel(this.state.channelName, channelUID); //Join Channel
            RtcEngine.enableAudio(); //Enable the audio
        } catch (e) {
            recordError(e)
        }
    };

    endCall = () => {
        try {
            if (this.props.startRecordingResponse !== undefined && this.props.startRecordingResponse !== null) {
                let body = {
                    cname: this.state.channelName,
                    uid: this.state.uid.toString(),
                    resourceId: this.props.startRecordingResponse.resourceId,
                    sid: this.props.startRecordingResponse.sid,
                    appointmentId: this.props.navigation.state.params.bookingId,
                    clientRequest: {},
                };
                // this.props.stopRecording(body);
            }

            clearInterval(this.interval);
            this.props.getUpcomingBookings();
            this.props.getBookingHistory();
            this.removeFromList();
            RtcEngine.leaveChannel();
            this.setState({
                peerIds: [],
                joinSucceed: false,
            });

            let user = this.props.navigation.state.params.user;
            let id = this.props.navigation.state.params.id;
            if (user && user === "PATIENT") this.props.navigation.pop(1);
            else if (user && user === "PATIENT_DIRECT") this.props.navigation.goBack();
            else {
                this.props.navigation.goBack();
            }
        } catch (e) {
            recordError(e)
        }
    };

    removeFromList = async () => {
        await database()
            .ref(`/users/${this.props.selectedAppointment}/patient`)
            .remove();
    };

    toggleVideo = () => {
        try {
            let mute = this.state.vidMute;
            RtcEngine.muteLocalVideoStream(!mute);
            this.setState({
                vidMute: !mute,
            });
        } catch (e) {
            recordError(e)
        }
    };

    toggleAudio = () => {
        try {
            let mute = this.state.audMute;
            RtcEngine.muteLocalAudioStream(!mute);
            this.setState({
                audMute: !mute,
            });
        } catch (e) {
            recordError(e)
        }
    };

    makeFullScreen = (peerId) => {
        try {
            let fullScreen = this.state.fullScreen;
            this.setState({
                fullScreen: !fullScreen, peerIdSelected: peerId
            });
        } catch (e) {
            recordError(e)
        }
    };

    switchCamera = () => {
        RtcEngine.switchCamera()
            .then(() => {
                console.log(" switchCamera called");
            }).catch((err) => console.log(" switchCamera called err " + err))
    }

    videoView() {
        return (
            <View style={{ flex: 1 }}>
                <VideoCommon
                    selectedHeader={this.props.selectedHeader}
                    showList={this.state.showList}
                    peerIds={this.state.peerIds}
                    audMute={this.state.audMute}
                    vidMute={this.state.vidMute}
                    joinSucceed={this.state.joinSucceed}
                    minutes={this.state.minutes}
                    seconds={this.state.seconds}
                    usersListFirebase={this.state.usersListFirebase}
                    onPressShowList={() => {
                        this.setState({
                            showList: !this.state.showList
                        })
                    }}
                    endCall={() => this.endCall()}
                    toggleAudio={() => this.toggleAudio()}
                    toggleVideo={() => this.toggleVideo()}
                    switchCamera={() => this.switchCamera()}
                    fullScreen={this.state.fullScreen}
                    peerIdSelected={this.state.peerIdSelected}
                    makeFullScreen={(id) => this.makeFullScreen(id)}
                // copyUrl={()=> {
                // 	this.props.getRoomURLForGuest(this.props.navigation.state.params.bookingId)
                // }}
                />
            </View>
        );
    }

    render() {
        return this.videoView();
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            startRecording,
            stopRecording,
            completeAppointment,
            getUpcomingBookings,
            getBookingHistory, showModal, getRoomURLForGuest
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    userProfile: state.profile.userProfile,
    selectedAppointment: state.bookings.selectedAppointment,
    selectedHeader: state.bookings.selectedHeader,
    startRecordingResponse: state.appointment.startRecording,
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(JoinCall);
