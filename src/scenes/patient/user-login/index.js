import React, { Component } from "react";
import { connect } from "react-redux";
import { Dimensions, Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, SafeAreaView, ImageBackground, PermissionsAndroid, } from "react-native";
import { Button, Picker, Icon } from "native-base";
import { showModal } from "../../../actions/modal";
import { hide, show } from "../../../utils/loader/action";
import Images from "../../../assets/images";
import validate from "../../../utils/validation_wrapper";
import moment from "moment-timezone";
import { bindActionCreators } from "redux";
import { callLoginApi } from "../../../actions/login";
import { methodLog, successLog } from "../../../utils/fireLog";
import styles from "../book-appointment/style";
import loginstyles from "../../authentication/signin/style";
import { FONT_FAMILY_SF_PRO_MEDIUM, FONT_SIZE_16, LINE_HEIGHT_24, FONT_FAMILY_SF_PRO_SemiBold } from "../../../styles/typography";
import { GoogleSignin } from '@react-native-community/google-signin';
import auth from '@react-native-firebase/auth';
import { getFcmToken, setFcmToken, setSkipped, setAppLangugae, getAppLangugae } from "../../../services/auth";
import messaging from '@react-native-firebase/messaging'
import TAndCCommon from "../../../components/t_and_c_common";
import PrivacyPolicyCommon from "../../../components/privacy_policy_common";
import PatientConsent from "../../../components/PatientConsent";
import Refund from "../../../components/Refund";
import { ifValid } from "../../../utils/ifNotValid";
import http from "../../../services/http";
import { client_id, client_secret, webClientId } from "../../../config";
import { AppleButton, appleAuth } from '@invertase/react-native-apple-authentication';
import ImageContainer from "../../../components/ImageContainer";
import { Set_App_Language } from "../../../actions/profile";
import { changeLaguage, strings } from "../../../utils/translation";
import MediaPreview from "../../../components/ChatScreen/component/MediaPreview";
import { setActiveTab } from '../CareForm/action'
import DeviceInfo from 'react-native-device-info'

GoogleSignin.configure({
    webClientId: webClientId,
});
import { from } from "form-data";
import { getContactMobileNumbers } from "../../../services";
import { callNumber } from "../../../utils/helpline-no";
import RNCallKeepComponent from "../../../components/RNCallKeepComponent";
import { PERMISSIONS, request } from "react-native-permissions";

class UserLoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            emailError: null,
            password: "",
            passwordError: null,
            secureTextEntry: true,
            offsetDifference: "",
            modal1: false,
            modal2: false,
            modal3: false,
            modal4: false,
            contactNo: []
        };
        this.state.offsetDifference = this.getUtcOffset(new Date());

        this.state.majorVersionIOS = parseInt(Platform.Version, 10);
    }

    componentDidMount() {
        this.checkAndStoreFCMToken()
        this.checkLanguage()
        this.getContacts()
        if (Platform.OS === "ios") {
            this.requestUserPermission()
        } else {
            this.requestUserPermissionAndroid()
        }
    }

    async getContacts() {
        const nos = await getContactMobileNumbers()
        this.setState({
            contactNo: nos
        })
    }

    async checkLanguage() {
        const value = await getAppLangugae()
        if (value != null) {
            changeLaguage(value)
            this.props.Set_App_Language(value)
        }
        else {
            changeLaguage('en')
            this.props.Set_App_Language('en')
        }
    }


    async requestUserPermission() {
        const authorizationStatus = await messaging().requestPermission();
        if (authorizationStatus) {
            console.log('Permission status:', authorizationStatus);
        } else {
            console.log('Permission status:', authorizationStatus);
        }
    }

    async requestUserPermissionAndroid() {
        request(PERMISSIONS.ANDROID.POST_NOTIFICATIONS)
            .then((status) => {
                console.log('requestUserPermissionAndroid ', status);
            })
            .catch(err => {
                console.log('requestUserPermissionAndroid ', err);
            })
    }

    async checkAndStoreFCMToken() {
        const fcm_token = await getFcmToken()
        console.log("my current token " + fcm_token);
        if (!fcm_token) {
            const tokenfcm = await messaging().getToken();
            console.log("my new token " + tokenfcm);
            setFcmToken(tokenfcm);
        }
        messaging()
            .subscribeToTopic('campaign')
            .then(() => console.log('subscribed to campaign'))
            .catch(() => console.log('err subscribeToTopic'))
    }

    changeLang = async (value) => {
        changeLaguage(value)
        this.props.Set_App_Language(value)
        await setAppLangugae(value)
    }

    updateSecureTextEntry = () => {
        this.setState({
            secureTextEntry: !this.state.secureTextEntry,
        });
    };

    isValid = () => {
        const emailError = validate("email", this.state.email);
        const passwordError = validate("password", this.state.password);
        this.setState({ emailError, passwordError });
        return !emailError && !passwordError;
    };

    _back() {
        this.props.navigation.navigate("Landing");
    }

    _onChangeValue(key, value) {
        this.setState({ [key]: value });
    }

    _onBlur(key) {
        this.setState({
            [`${key}Error`]: validate(key, this.state[key]),
        });
    }

    _resetData() {
        this.setState({
            email: null,
            emailError: null,
            password: null,
            passwordError: null,
        });
    }

    getUtcOffset = (date) => {
        return moment(date)
            .subtract(moment(date).utcOffset(), "minutes")
            .utc();
    };


    _onGoogleButtonPress = async () => {
        try {
            methodLog("_onGoogleButtonPress")
            // Get the users ID token
            const { idToken } = await GoogleSignin.signIn();

            // Create a Google credential with the token
            const googleCredential = auth.GoogleAuthProvider.credential(idToken);

            // Sign-in the user with the credential
            return auth().signInWithCredential(googleCredential);
        } catch (e) {
            console.log(e)
        }
    };

    onModal1 = () => {
        this.setState({ modal1: !this.state.modal1 });
    };

    onModal2 = () => {
        this.setState({ modal2: !this.state.modal2 });
    };
    onModal3 = () => {
        this.setState({ modal3: !this.state.modal3 });
    };
    onModal4 = () => {
        this.setState({ modal4: !this.state.modal4 });
    };

    onAppleButtonPress = async () => {
        try {
            console.log("inside apple sign in");
            // Start the sign-in request
            const appleAuthRequestResponse = await appleAuth.performRequest({
                requestedOperation: appleAuth.Operation.LOGIN,
                requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
            });
            console.log("appleAuthRequestResponse " + JSON.stringify(appleAuthRequestResponse));

            // Ensure Apple returned a user identityToken
            if (!appleAuthRequestResponse.identityToken) {
                console.log('Apple Sign-In failed - no identify token returned');
            }

            // Create a Firebase credential from the response
            const { identityToken, nonce } = appleAuthRequestResponse;
            const appleCredential = auth.AppleAuthProvider.credential(identityToken, nonce);

            // Sign the user in with the credential
            return auth().signInWithCredential(appleCredential);
        } catch (error) {
            console.log("error in apple sign in  " + JSON.stringify(error));
        }
    }

    render() {
        let { width, height } = Dimensions.get("window");
        let imageHeight = height / 1.7
        const imaegeHeigh = Platform.OS === 'ios' ? 320 : 300
        return (
            <ImageContainer>
                <ScrollView style={{ flex: 1, height: height + 50 }}>
                    <>
                        <SafeAreaView style={{
                            flex: 1,
                            flexDirection: 'row',
                            justifyContent: 'space-between',
                            alignItems: 'center',
                        }}>
                            <View>
                                <Picker
                                    mode="dialog"
                                    selectedValue={this.props.selected_language}
                                    onValueChange={(value) => {
                                        if (value == "en") {
                                            this.changeLang(value)
                                        } else if (value == "hin") {
                                            this.changeLang(value)
                                        } else if (value == "ma") {
                                            this.changeLang(value)
                                        }
                                    }}
                                    textStyle={{ color: '#FFF', fontSize: 16 }}
                                    style={{
                                        width: 120,
                                        // height: 44,
                                        // marginRight: '40%',
                                        marginLeft: 14, color: '#FFF',
                                    }}
                                >
                                    <Picker.Item label="English" value="en" />
                                    <Picker.Item label="हिन्दी" value="hin" />
                                </Picker>

                            </View>
                            <Text onPress={() => {
                                setSkipped(true)
                                this.props.navigation.navigate("Home")
                            }}
                                style={innerStyle.skipStyle}
                            >{strings.skip}</Text>

                        </SafeAreaView>
                        <View
                            style={{
                                alignItems: 'center',
                                paddingHorizontal: 40,
                                paddingVertical: 25,
                            }}>
                            <Image
                                resizeMode="contain"
                                source={Images.home_logo}
                                style={{ width: '60%', height: imageHeight - imaegeHeigh }} />
                        </View>

                        <View style={{
                            // height: height / 3,
                            marginHorizontal: 20,
                            marginVertical: 10,
                            // justifyContent: 'space-around'
                        }}>

                            <Text style={{ color: 'white', fontSize: 32, textAlign: 'center', marginBottom: 30, fontFamily: FONT_FAMILY_SF_PRO_SemiBold }}>
                                {/* Consult Best Doctors{'\n'} in India */}
                                {strings.tagLine}
                            </Text>

                            <Button danger style={[styles.footerBtn, { marginBottom: 20 }]}
                                onPress={() => this.props.navigation.navigate("LoginScreenMobile")}>
                                {/*<View style={{width: '25%',}}/>*/}
                                <View style={{ width: '80%', }}>
                                    <Text style={{ color: 'white', textAlign: 'center', fontSize: 15 }}>{strings.loginMobile}</Text>
                                </View>
                            </Button>

                            <Button danger style={[styles.footerBtn, { backgroundColor: 'white', borderWidth: 0.3, marginBottom: 20 }]} onPress={() => {
                                this.props.show()
                                this._onGoogleButtonPress()
                                    .then(async (_) => {
                                        console.log('Signed in with Google! res ' + JSON.stringify(_))
                                        if (ifValid(_)) {
                                            const deviceId = DeviceInfo.getUniqueId()
                                            const token = await getFcmToken()
                                            const fcmToken = JSON.parse(token)
                                            let payload = {
                                                email: _.user.providerData[0].email,
                                                msgMode: 'EMAIL',
                                                uuid: _.user.providerData[0].uid
                                            }
                                            //make register and login calls
                                            http.postLogin('/auth/verifyGoogleAndSignUp', payload, this.props.dispatch).then(res => {
                                                // console.log('/auth/verifyGoogleAndSignUp ' + JSON.stringify(res))
                                                if (res.data.status === 200) {
                                                    console.log("success logged in");
                                                    let appPlatForm = Platform.OS
                                                    const tokenObj = {
                                                        token: fcmToken,
                                                        deviceId: deviceId,
                                                        platform: appPlatForm
                                                    }
                                                    let loginpayload = `username=${payload.email}&password=${payload.uuid}&fcmToken=${JSON.stringify(tokenObj)}&offset=${moment(
                                                        new Date()
                                                    ).format()}&grant_type=password&client_id=${client_id}&client_secret=${client_secret}&msgMode=EMAIL`;
                                                    console.log("props bookingFlow ", this.props.bookingFlow1or2);
                                                    this.props.callLoginApi(loginpayload, this.props.navigation, this.props.bookingFlow1or2, payload?.email)
                                                    // alert(JSON.stringify(loginpayload))
                                                    this.props.hide()
                                                } else {
                                                    console.log("login api failed...");
                                                    this.props.hide()
                                                }
                                            }).catch(err => {
                                                console.log("/auth/verifyGoogleAndSignUp error ", JSON.stringify(err));
                                                this.props.hide()
                                            })
                                        } else {
                                            this.props.hide()
                                            console.log("res not valid");
                                        }
                                    }).catch(err => {
                                        console.log(err);
                                        this.props.hide()
                                    })
                            }}>

                                <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
                                    <Image source={Images.google} style={{
                                        width: 33, height: 33,
                                        borderRadius: 33,
                                        marginRight: 10,
                                    }} />
                                    <Text style={{ color: '#041A32', textAlign: 'center', fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, fontSize: 15 }}>
                                        {strings.loginGoogle}
                                    </Text>
                                </View>
                            </Button>

                            {
                                Platform.OS === "ios" && (
                                    <AppleButton
                                        style={{ height: 45, marginBottom: 20 }}
                                        buttonStyle={AppleButton.Style.BLACK}
                                        buttonType={AppleButton.Type.SIGN_IN}
                                        onPress={() => {
                                            this.props.show()
                                            this.onAppleButtonPress()
                                                .then(async (_) => {
                                                    console.log('Apple sign-in complete! ' + JSON.stringify(_))

                                                    if (ifValid(_)) {
                                                        const deviceId = DeviceInfo.getUniqueId()
                                                        const token = await getFcmToken()
                                                        const fcmToken = JSON.parse(token)
                                                        let payload = {
                                                            email: _.user.providerData[0].email,
                                                            msgMode: 'EMAIL',
                                                            uuid: _.user.providerData[0].uid
                                                        }
                                                        //make register and login calls
                                                        http.postLogin('/auth/verifyGoogleAndSignUp', payload, this.props.dispatch).then(res => {
                                                            // console.log('/auth/verifyGoogleAndSignUp ' + JSON.stringify(res))
                                                            if (res.data.status === 200) {
                                                                console.log("success logged in");
                                                                let appPlatForm = Platform.OS
                                                                const tokenObj = {
                                                                    token: fcmToken,
                                                                    deviceId: deviceId,
                                                                    platform: appPlatForm
                                                                }
                                                                let loginpayload = `username=${payload.email}&password=${payload.uuid}&deviceId=${deviceId}&fcmToken=${JSON.stringify(tokenObj)}&offset=${moment(
                                                                    new Date()
                                                                ).format()}&grant_type=password&client_id=${client_id}&client_secret=${client_secret}&msgMode=EMAIL`;
                                                                console.log("props bookingFlow ", this.props.bookingFlow1or2);
                                                                this.props.callLoginApi(loginpayload, this.props.navigation, this.props.bookingFlow1or2, payload?.email)
                                                                // alert(JSON.stringify(loginpayload))
                                                                this.props.hide()
                                                            } else {
                                                                console.log("login api failed...");
                                                                this.props.hide()
                                                            }
                                                        }).catch(err => {
                                                            console.log("/auth/verifyGoogleAndSignUp error ", JSON.stringify(err));
                                                            this.props.hide()
                                                        })
                                                    } else {
                                                        this.props.hide()
                                                        console.log("res not valid");
                                                    }
                                                    // alert(JSON.stringify(data))
                                                })
                                                .catch(err => {
                                                    this.props.show()
                                                    console.log(JSON.stringify(err))
                                                    alert(JSON.stringify("fat gaya " + err))
                                                })
                                        }}
                                    />
                                )
                            }

                            <View
                                style={{
                                    alignItems: 'center',
                                    flexDirection: 'row',
                                    justifyContent: 'center'
                                }}>
                                <Text style={loginstyles.loginHere}>{strings.ifDoctor}</Text>
                                <TouchableOpacity onPress={() => this.props.navigation.navigate('LoginScreen')}><Text
                                    style={loginstyles.loginLink}> {strings.login}</Text></TouchableOpacity>
                                <Text style={loginstyles.loginHere}> {strings.here} </Text>
                            </View>


                            {/* <Text>cdcdc</Text>
                            <Text>cdcdc</Text>
                            <Text>cdcdc</Text> */}
                        </View>

                        <View>
                            <TAndCCommon visible={this.state.modal1} onPress={() => this.onModal1()} />
                            <PrivacyPolicyCommon visible={this.state.modal2} onPress={() => this.onModal2()} />
                            <PatientConsent visible={this.state.modal3} onPress={() => this.onModal3()} />
                            <Refund visible={this.state.modal4} onPress={() => this.onModal4()} />
                        </View>

                    </>
                </ScrollView>

                <Text style={[styles.mainText, {
                    // flex: 1,
                    textAlign: 'center',
                    position: 'absolute',
                    bottom: 0,
                    height: 45,
                    // marginVertical: 10,
                    marginHorizontal: 20,
                }]}>
                    <Text style={styles.tAndCText}>By continuing, you agree that you have read and accept
                        our </Text>
                    <Text style={styles.tAndCLink} onPress={this.onModal1}> T&C , </Text>
                    <Text style={styles.tAndCLink} onPress={this.onModal2}> Privacy Policy.</Text>
                    <Text style={styles.tAndCLink} onPress={this.onModal3}> Patient Consent .</Text>
                    <Text style={styles.tAndCText}> and</Text>
                    <Text style={styles.tAndCLink} onPress={this.onModal4}> Refund Policy.</Text>
                </Text>
            </ImageContainer>
        );
    }
}

const mapDispatchToProps = (dispatch) =>
    bindActionCreators(
        {
            showModal, callLoginApi, show, hide, Set_App_Language, setActiveTab
        },
        dispatch
    );

const mapStateToProps = (state) => ({
    bookingFlow1or2: state.appointment.bookingFlow1or2,
    selected_language: state.profile.selected_language,
});


export default connect(
    mapStateToProps,
    mapDispatchToProps
)(UserLoginScreen);


const innerStyle = StyleSheet.create({
    skipStyle: {
        color: '#FF5E38',
        fontSize: 16,
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        marginRight: 25,
        // alignSelf: 'flex-end'
        // marginTop: 20
        // position: 'absolute',
        // top: 5,
        // right: 15
    }
})


{/* <Button danger style={[styles.footerBtn, { backgroundColor: 'white', borderWidth: 0.3 }]} onPress={() => {
                        this.props.show()
                        this._onGoogleButtonPress().then((_) => {
                            console.log('Signed in with Google! res ' + JSON.stringify(_))
                            if (ifValid(_)) {
                                let payload = {
                                    email: _.user.providerData[0].email,
                                    msgMode: 'EMAIL',
                                    uuid: _.user.providerData[0].uid
                                }
                                //make register and login calls
                                http.postLogin('/auth/verifyGoogleAndSignUp', payload, this.props.dispatch).then(res => {
                                    // console.log('/auth/verifyGoogleAndSignUp ' + JSON.stringify(res))
                                    if (res.data.status === 200) {
                                        console.log("success logged in");
                                        let loginpayload = `username=${payload.email}&password=${payload.uuid}&offset=${moment(
                                            new Date()
                                        ).format()}&grant_type=password&client_id=${client_id}&client_secret=${client_secret}`;
                                        console.log("props bookingFlow ", this.props.bookingFlow1or2);
                                        this.props.callLoginApi(loginpayload, this.props.navigation, this.props.bookingFlow1or2)
                                        this.props.hide()
                                    } else {
                                        console.log("login api failed...");
                                        this.props.hide()
                                    }
                                }).catch(err => {
                                    console.log("/auth/verifyGoogleAndSignUp error ", JSON.stringify(err));
                                    this.props.hide()
                                })
                            } else {
                                this.props.hide()
                                console.log("res not valid");
                            }
                        }).catch(err => {
                            console.log(err);
                            this.props.hide()
                        })
                    }}>
                        <Image source={Images.google} style={{
                            width: 33, height: 33, left: 4,
                            position: 'absolute'
                        }} />
                        <View style={{ width: '100%', }}>
                            <Text style={{ color: 'black', textAlign: 'center', fontFamily: FONT_FAMILY_SF_PRO_MEDIUM }}>Login
                                with Google</Text>
                        </View>
                    </Button> */
}

// <View style={{
//                                 flex: 1,
//                                 marginTop: 25
//                             }}>
//                                 <Button danger
//                                     style={[styles.footerBtn, { backgroundColor: '#038BEF', borderWidth: 0.3, marginBottom: 5 }]}
//                                     onPress={() => {
//                                         this.props.setActiveTab(0)
//                                         this.props.navigation.navigate('CareForm')
//                                     }}>

//                                     <View style={{ width: '100%', justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
//                                         <Image source={Images.covid_logo} style={{
//                                             width: 33, height: 33,
//                                             borderRadius: 33,
//                                             marginRight: 10,
//                                         }} />
//                                         <Text style={{ color: '#FFF', textAlign: 'center', fontFamily: FONT_FAMILY_SF_PRO_MEDIUM, fontSize: 15 }}>
//                                             {/* {strings.loginGoogle} */}
//                                             Covid-Care
//                                         </Text>
//                                     </View>
//                                 </Button>

//                                 <Text style={{
//                                     textAlign: 'center',
//                                     color: '#FFF',
//                                     fontSize: 15
//                                 }}>
//                                     Helpline No :
//                                     {
//                                         this.state.contactNo.map(item => (
//                                             <>
//                                                 <Text
//                                                     style={{
//                                                         color: '#038BEF'
//                                                     }}
//                                                     onPress={() => {
//                                                         callNumber(`${item}`)
//                                                     }}
//                                                 > {
//                                                         item !== "" && item
//                                                     }</Text>
//                                             </>
//                                         ))
//                                     }
//                                 </Text>
//                             </View>
