import { StyleSheet, Dimensions } from "react-native";
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp,
} from "react-native-responsive-screen";

let { width, height } = Dimensions.get("window");

export default (styles = StyleSheet.create({
	_passwordIcon: {
		fontSize: 20,
		color: "#A0A9BE",
	},
	_passwordIconOn: {
		color: "#1B80F3",
		fontSize: 20,
	},
	toast: {
		width: "80%",
	},
	container: {
		padding: "5%",
	},
	imageBg: {
		height: hp("100%"),
	},
	backIcon: {
		color: "#fff",
		marginTop: hp("1%"),
		fontSize: hp("3.5%"),
		marginLeft: 10,
	},
	section1: {
		marginTop: hp("6%"),
		marginLeft: wp("4%"),
	},
	welcomeText: {
		fontSize: 24,
		color: "#fff",
	},
	section2: {
		marginTop: hp("12%"),
		paddingRight: wp("5%"),
	},
	section3: {
		marginTop: hp("16%"),
		alignItems: "center",
		justifyContent: "space-between",
	},
	section3_sm: {
		alignItems: "center",
		justifyContent: "space-between",
	},
	image: {
		flex: 1,
		resizeMode: "cover",
		justifyContent: "center",
	},
	hyperlink: {
		color: "#1B80F3",
	},
	help: {
		color: "#A0A9BE",
		textDecorationLine: "underline",
		marginLeft: 5,
	},
	footer: {
		marginTop: 10,
		flexDirection: "row",
		alignItems: "center",
	},
	danger: {
		backgroundColor: "#c70315",
		borderRadius: 10,
		marginBottom: hp("12%"),
		width: wp("82%"),
		height: hp("6%"),
		alignSelf: "center",
	},
	disabled: {
		backgroundColor: "#A0A9BE",
		borderRadius: 10,
		marginBottom: hp("12%"),
		width: wp("82%"),
		height: hp("6%"),
		alignSelf: "center",
	},
	forgotPassword: {
		flexDirection: "row",
		justifyContent: "flex-end",
		color: "#041A32",
		marginBottom: hp("3%"),
	},

	footerBtn: {
		justifyContent: "center",
		backgroundColor:'#FF5E38',
		alignItems: "center",
		width: wp("90%"),
		borderRadius: 9,
	},
	buttonText: {
		color: 'white',
		fontWeight: 'bold',
		textAlign: 'center',
		flex: 2
	},

	iconLeft: {
		marginLeft: 10,
	},

	buttonIconStyle: {
		position: "absolute",
		left: 10,
		backgroundColor: 'transparent'
	},

	iconRight: {
		marginRight: 10,
	},

}));
