import AsyncStorage from '@react-native-community/async-storage'

export const setToken = async (token) => {
    await AsyncStorage.setItem('accessToken', token.accessToken);
    await AsyncStorage.setItem('refreshToken', token.refreshToken);
    await AsyncStorage.setItem('role', token.user.role);
}

export const accessToken = async () => {
    return await AsyncStorage.getItem('accessToken')
}

export const getRefToken = async () => {
    return await AsyncStorage.getItem('refreshToken')
}
export const getRole = async () => {
    return await AsyncStorage.getItem('role')
}

export const setSkipped = async (value) => {
    await AsyncStorage.setItem('skipped', JSON.stringify(value))
}

export const getSkipped = async () => {
    return await AsyncStorage.getItem('skipped')
}

export const setLoggedIn = async (value) => {
    await AsyncStorage.setItem('loggedIn', JSON.stringify(value))
}

export const getLoggedIn = async () => {
    return await AsyncStorage.getItem('loggedIn')
}

export const setFromCheckout = async (value) => {
    return await AsyncStorage.setItem('fromCheckout', JSON.stringify(value))
}

export const getFromCheckout = async () => {
    return await AsyncStorage.getItem('fromCheckout')
}

export const getFcmToken = async () => {
    return await AsyncStorage.getItem('fcmToken')
}

export const setFcmToken = async (value) => {
    return await AsyncStorage.setItem('fcmToken', JSON.stringify(value))
}

export const setScheduleNotificationCheck = async (value) => {
    return await AsyncStorage.setItem('isScheduledNotif', JSON.stringify(value))
}

export const getScheduleNotificationCheck = async () => {
    return await AsyncStorage.getItem('isScheduledNotif')
}

export const getGetStarted = async () => {
    return await AsyncStorage.getItem('getStarted')
}

export const setGetStarted = async (value) => {
    return await AsyncStorage.setItem('getStarted', JSON.stringify(value))
}

export const getAppLangugae = async () => {
    return await AsyncStorage.getItem('app_language')
}

export const setAppLangugae = async (value) => {
    return await AsyncStorage.setItem('app_language', value)
}

export const getVideoNotification = async () => {
    return await AsyncStorage.getItem('isVideoNotification')
}

export const setIsVideoNotification = async (value) => {
    return await AsyncStorage.setItem('isVideoNotification', value)
}

export const getIncomingCallScreen = async () => {
    return await AsyncStorage.getItem('isIncomingCallScreen')
}

export const setIncomingCallScreen = async (value) => {
    return await AsyncStorage.setItem('isIncomingCallScreen', value)
}

export const getCallerData = async () => {
    return await AsyncStorage.getItem('callerData')
}

export const setCallerData = async (value) => {
    return await AsyncStorage.setItem('callerData', JSON.stringify(value))
}

export const getOfferFlag = async () => {
    return await AsyncStorage.getItem('offer')
}

export const setOfferFlag = async (value) => {
    return await AsyncStorage.setItem('offer', JSON.stringify(value))
}

export const getOfferId = async () => {
    return await AsyncStorage.getItem('offerId')
}
export const setOfferId = async (value) => {
    return await AsyncStorage.setItem('offerId', JSON.stringify(value))
}