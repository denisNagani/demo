import { base_url } from "../config";
import axios from "axios";
import { accessToken, setGetStarted } from "./auth";
import { showModal } from "../actions/modal";
import AsyncStorage from "@react-native-community/async-storage";
import NavigationService from '../components/startScreen/RootNavigation';
import { hide } from "../utils/loader/action";
import { errorPayload } from "../utils/helper";
import { setApplyCouponModal, setSelectPlanModal } from "../actions/sehet/speciality-action";
import { isValid } from "../utils/ifNotValid";

const header = {
    accept: "application/json",
    "content-type": "application/json",

};

const loginHeader = {
    'accept': "application/json",
    "content-type": "application/x-www-form-urlencoded",
};


class httpService {
    constructor() {
        this.setInterceptor();
    }

    setInterceptor = () => {
        let service = axios.create({
            Accept: "application/json",
            "Content-Type": "application/json",
        });
        service.interceptors.request.use(
            async (config) => {
                let access_token = await accessToken();
                config.headers.Authorization = "Bearer " + access_token;
                return config;
            },
            (error) => {
                return Promise.reject(error);
            }
        );
        this.service = service;
    };

    get(endpoint, dispatch) {
        let url = base_url + endpoint;
        console.log("URL === ", url);
        return this.service
            .get(url)
            .then((response) => response.data)
            .catch((error) => this.handleCatch(error, dispatch));
    }


    postLogin(endpoint, payload, dispatch, navigation) {
        let url = base_url + endpoint;
        return axios
            .post(url, payload, loginHeader)
            .then((response) => response)
            .catch((error) => this.handleCatch(error, dispatch, navigation));
    }


    handleCatch(error, dispatch, navigation) {
        console.log('inside handleCatch', error.response.data)
        if (error?.response?.data?.status === 400) {
            dispatch(showModal(errorPayload(error?.response?.data?.message)))
            dispatch(setApplyCouponModal(false))
            dispatch(setSelectPlanModal(false))
            dispatch(hide())
        } else {
            let payload = {
                text: JSON.stringify(error),
                subtext: "",
                iconName: "closecircleo",
                modalVisible: true,
            };
            if (error.response.data !== undefined) {
                payload = { ...payload, text: JSON.stringify(error.response.data) };
            }

            if (error.response.data.message !== undefined) {
                console.log(error.response.data.message);
                if (typeof error.response.data.message == "object") {
                    payload = { ...payload, text: 'Something went wrong..' };
                } else {
                    payload = { ...payload, text: error.response.data.message };
                }
            }
            if (error.response.data.error !== undefined) {
                console.log(error.response.data.error);
                payload = { ...payload, text: error.response.data.error };
            }

            if (error.response.status == 401) {
                if (NavigationService !== undefined && NavigationService !== null) {
                    payload = { ...payload, text: "Session expired" };
                    dispatch(showModal(payload));
                    AsyncStorage.clear();
                    setGetStarted(true)
                    NavigationService.navigate('Land');
                    return;
                } else {
                    console.log("NavigationService is empty ")
                }
            }
            dispatch(showModal(payload));
            dispatch(hide())
        }
        return error.response.data;
    }

    post(endpoint, payload, dispatch) {
        let url = base_url + endpoint;
        return this.service
            .request({ method: "POST", url: url, data: payload })
            .then((response) => response.data)
            .catch((error) => this.handleCatch(error, dispatch));
    }

    put(endpoint, payload, dispatch) {
        let url = base_url + endpoint;
        return this.service
            .request({ method: "PUT", url: url, data: payload })
            .then((response) => response.data)
            .catch((error) => this.handleCatch(error, dispatch));
    }

    delete(endpoint) {
        let url = base_url + endpoint;
        return this.service
            .delete(url, header)
            .then((response) => response.data)
            .catch((error) => this.handleCatch(error));
    }
}

export default new httpService();
