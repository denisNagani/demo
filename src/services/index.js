import analytics, { firebase } from "@react-native-firebase/analytics";
import firestore from "@react-native-firebase/firestore";

export const sendOTPAnalytics = async (logName, phoneNumber, status, details) => {
    console.log("firebase event data " + JSON.stringify({
        logName, phoneNumber, status, details
    }));
    await analytics().logEvent(logName, {
        mobile_number: phoneNumber,
        status: status,
        details: details
    })
}

export const loggedInAnalytics = async (logName, phoneNumber) => {
    console.log("event loggedInAnalytics " + JSON.stringify({
        logName, phoneNumber
    }));
    await analytics().logEvent(logName, {
        mobile_number: phoneNumber,
    })
}

export const getContactNumber = async () => {
    const res = await firestore()
        .collection("support")
        .doc("help_number")
        .get()
    const { phoneNo } = res.data()
    return phoneNo
}

export const getContactMobileNumbers = async () => {
    const res = await firestore()
        .collection("support")
        .doc("support_help")
        .get()
    const { contactNo } = res.data()
    return contactNo
}

export const fireReport = async (mobileNo) => {
    try {
        firestore()
            .collection('OptReport')
            .doc(mobileNo)
            .get()
            .then(documentSnapshot => {
                console.log('Number exists: ', documentSnapshot.exists);
                if (documentSnapshot.exists) {
                    const increment = firebase.firestore.FieldValue.increment(1);
                    firestore()
                        .collection('OptReport')
                        .doc(mobileNo)
                        .update({
                            count: increment
                        })
                        .then(() => {
                            console.log(" Otp verfiy count increased");
                        })
                        .catch(err => {
                            console.log("increased error " + JSON.stringify(err));
                        })

                } else {
                    firestore()
                        .collection('OptReport')
                        .doc(mobileNo)
                        .set({
                            mobileNo: mobileNo,
                            count: 1
                        })
                        .then(() => {
                            console.log('User added!');
                        }).catch(err => {
                            console.log('error in adding user ' + JSON.stringify(err));
                        })
                }
            });
    } catch (error) {
        console.log("error Otp Report " + JSON.stringify(error));
    }
}

export const getWhatsAppSupportData = async () => {
    const res = await firestore()
        .collection("support")
        .doc("whatsAppSupport")
        .get()
    const doc = res.data()
    return doc
}