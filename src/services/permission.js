/* eslint-disable prettier/prettier */
/* eslint-disable quotes */
import { PermissionsAndroid, Platform } from "react-native";
import { check, PERMISSIONS, request, RESULTS } from "react-native-permissions";
import Geolocation from 'react-native-geolocation-service';
import { store } from "../store/index";
import { setLocationData } from "../actions/sehet/user-action";

/**
 * @name requestCameraAndAudioPermission
 * @description Function to request permission for Audio and Camera
 */
export const requestCameraAndAudioPermission = async () => {

    try {
        if (Platform.OS === "android") {

            const granted = await PermissionsAndroid.requestMultiple([
                PermissionsAndroid.PERMISSIONS.CAMERA,
                PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
            ]);
            if (
                granted["android.permission.RECORD_AUDIO"] ===
                PermissionsAndroid.RESULTS.GRANTED &&
                granted["android.permission.CAMERA"] ===
                PermissionsAndroid.RESULTS.GRANTED
            ) {
                console.log("You can use the cameras & mic");
            } else {
                console.log("Permission denied");
            }

        } else {
            const res = await check(PERMISSIONS.IOS.CAMERA);
            if (res === RESULTS.GRANTED) {
                console.log("granted CAMERA")
            } else if (res === RESULTS.DENIED) {
                const res2 = await request(PERMISSIONS.IOS.CAMERA);
                res2 === RESULTS.GRANTED ? console.log("granted CAMERA")
                    : console.log("not granted CAMERA")
            }
            const micro = await check(PERMISSIONS.IOS.MICROPHONE);
            if (micro === RESULTS.GRANTED) {
                console.log("granted micro")
            } else if (micro === RESULTS.DENIED) {
                const micro = await request(PERMISSIONS.IOS.MICROPHONE);
                micro === RESULTS.GRANTED ? console.log("granted micro")
                    : console.log("not granted micro")
            }
        }


    } catch (err) {
        console.warn(err);
    }
}

export const getLocationCordinates = (callback) => {
    Geolocation.getCurrentPosition(
        //Will give you the current location
        async (position) => {
            //getting the Longitude from the location json
            const currentLongitude = position?.coords?.longitude;
            //getting the Latitude from the location json
            const currentLatitude = position?.coords?.latitude;

            const cords = {
                latitude: currentLatitude,
                longitude: currentLongitude
            }
            store.dispatch(setLocationData(cords))
            callback(cords)
        },
        (error) => {
            console.log('eror getting Geolocation.getCurrentPosition ', error);
        },
        {
            enableHighAccuracy: false,
        },
    );
}

export const askForMyCurrentLocation = async (callback) => {
    if (Platform.OS === 'android') {
        const response = await askLocationPermissionAndroid()
        console.warn('per res ', response);
        if (response === 'granted') {
            getLocationCordinates(callback)
        } else if (response === 'denied') {
            console.log('permission is denied');
        } else if (response === 'never_ask_again') {
            console.log('permission is never_ask_again');
        }
    } else {
        const response = await Geolocation.requestAuthorization("whenInUse")
        if (response === 'granted') {
            getLocationCordinates(callback)
        } else if (response === 'denied') {
            console.log('permission is denied');
        } else if (response === 'restricted') {
            console.log('permission is restricted');
        }
    }
}

export const askLocationPermissionAndroid = async () => {
    const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
            title: 'Location Access Required',
            message: 'This App needs to access your location to show you neaby doctors',
        },
    );
    return granted
}