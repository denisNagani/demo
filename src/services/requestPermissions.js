import { Alert, Platform } from "react-native";
import { PERMISSIONS, check, openSettings, request } from "react-native-permissions";

export const requestPermissionStorage = async () => {
    if (Platform.OS === 'android') {
        if (Platform.Version > 32) {
            const status = await request(PERMISSIONS.ANDROID.READ_MEDIA_IMAGES)
            if (status == 'granted') {
                return true
            } else if (status == 'blocked') {
                showSettingsAlert("Photos and Videos")
            } else {
                return true
            }
        } else {
            return true
        }
    } else {
        return true
    }
}

export const requestPermissionCamera = async () => {
    if (Platform.OS === 'android') {
        // if (Platform.Version > 32) {
        const status = await request(PERMISSIONS.ANDROID.CAMERA)
        if (status == 'granted') {
            return true
        } else if (status == 'blocked') {
            showSettingsAlert("Camera")
        } else {
            return true
        }
        // } else {
        //     return true
        // }
    } else {
        return true
    }
}

const showSettingsAlert = (permission_name) => {
    Alert.alert(
        "Permission required!",
        `Please click open settings and allow ${permission_name} permission under Permissions`,
        [
            {
                text: "Open Settings",
                onPress: () => openSettings(),
                style: "cancel",
            },
        ],
        {
            cancelable: true,
        }
    );
}