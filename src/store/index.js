import { applyMiddleware, compose, createStore } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers/index';


const logger = createLogger();
let middleware = [];
middleware = [...middleware, thunk];
// middleware = [...middleware, thunk];

const store = createStore(
    rootReducer,
    compose(applyMiddleware(...middleware))
);
const getStore = () => {
    return store;
}


export { store, getStore };


// const rootReducer = (state, action) => appReducers(state, action);

// const logger = createLogger();

// let middleware = [];
// middleware = [...middleware, thunk, logger];

// export default createStore(
//   rootReducer,
//   compose(applyMiddleware(...middleware))
// );





