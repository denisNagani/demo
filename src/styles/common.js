import { Dimensions, StyleSheet } from "react-native";
import {
    FONT_FAMILY_MEDIUM,
    FONT_FAMILY_SF_PRO_MEDIUM,
    FONT_SIZE_14,
    FONT_SIZE_15,
    FONT_SIZE_16,
    FONT_SIZE_18,
    FONT_SIZE_20
} from "./typography";

import { heightPercentageToDP as hp, widthPercentageToDP as wp, } from "react-native-responsive-screen";
import { Platform } from "react-native";

let dimensions = {
    //get dimensions of the device to use in view styles
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
};

export default StyleSheet.create({
    max: {
        flex: 1,
        backgroundColor: "white",
    },
    buttonHolder: {
        height: 76,
        alignItems: "center",
        backgroundColor: "#FFFFFF",
        flexDirection: "row",
        justifyContent: "space-evenly",
    },
    button: {},
    iconBottom: { width: 22, height: 22, resizeMode: "contain" },
    iconStyle: { color: "#041A32" },
    boxStyleAmount: {
        borderRadius: 6, borderColor: '#B6BECB', borderWidth: 0.8, paddingTop: 5, paddingLeft: 8, paddingRight: 5,
    },
    titleStyle_: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        color: 'black',
        fontSize: FONT_SIZE_14
    },
    buttonText: {
        color: "#fff",
    },
    fullView: {
        width: dimensions.width,
        backgroundColor: "white",
        height: dimensions.height - 174,
    },
    halfViewRow: {
        flex: 1 / 2,
        flexDirection: "row",
    },
    full: {
        flex: 1,
    },
    half: {
        flex: 1 / 2,
    },
    localVideoStyle: {
        width: 120,
        height: 150,
        position: "absolute",
        left: 20,
        bottom: 0,
        zIndex: 100,
    },
    userList: {
        flex: 1,
        position: "absolute",
        left: 0,
        bottom: 0,
        top: 0,
        right: 0,
        zIndex: 100,
        backgroundColor: "white",
    },
    noUserText: { paddingHorizontal: 10, paddingVertical: 5, color: "#0093E9" },

    lineStyle: { borderBottomColor: "#B6BECB", borderBottomWidth: 0.5 },

    drawerItemContainer: {
        marginLeft: 25,
        marginTop: hp("4.5%"),
        flexDirection: "row",
        alignItems: 'center'
    },
    drawerImage: { width: hp("2.5%"), height: hp("2.5%") },
    drawerImageSocial: { width: hp("3.5%"), height: hp("3.5%") },
    drawerItemText: { color: "#484F5F", fontSize: FONT_SIZE_15, marginLeft: 8 },

    drawerProfileContainer: {
        flexDirection: "row",
        marginLeft: 30,
        marginTop: 45,
    },
    drawerThumbnail: {
        width: wp("14%"),
        height: wp("14%"),
        marginTop: 10,
        borderRadius: wp("7%"),
    },
    drawerProfileName: {
        color: "#041A32",
        fontSize: FONT_SIZE_20,
        fontFamily: FONT_FAMILY_MEDIUM,
        paddingRight: wp("15%"),
    },
    drawerProfileSmallText: {
        color: "#1B80F3",
        fontSize: 14
    },
    lineStyleDrawer: { marginTop: 30, opacity: 0.13 },
    header: {
        backgroundColor: "#FFFFFF",
        height: hp("10%"),
        flexDirection: "row",
        alignItems: "center",
    },
    headerBackIcon: {
        flex: 1,
        fontSize: hp("5%"),
        color: "#041A32",
        marginLeft: wp("5%"),
    },
    heading: {
        flex: 8,
        color: "#041A32",
        fontSize: hp("2.9%"),
    },
    drawerFollowUsContainer: {
        marginTop: 20,
        flex: 2.5,
        alignSelf: 'center',
        justifyContent: 'center',
    },
    drawerFollowIcons: {

        marginTop: 20,
        marginBottom: 15,
        flexDirection: 'row',
        justifyContent: 'space-around'

    },
    joinCallheader: {
        height: Platform.OS === "ios" ? 95 : 80,
        backgroundColor: "#FFFFFF",
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
    },
    joinCallheading: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginLeft: 20,
    },
    joinCallheaderText: {
        color: "#041A32",
        fontSize: 18,
        marginLeft: 20,
    },
    joinCallheaderBackIcon: {
        fontSize: 35,
        color: "#fff",
    },
    skip: {
        marginRight: 20,
        color: "#1B80F3",
    },
    cardContainer: {
        alignItems: "center",
        marginTop: 30,
    },
    cardHeader: {
        flexDirection: "row",
    },
    cardHeaderIcon: {
        marginLeft: 10,
    },
    cardItem: {
        flexDirection: "row",
    },
    videoList: { width: 54, height: 54, borderRadius: 27 },
    videoListItemContainer: {
        marginLeft: 30,
        marginTop: 10,
        flexDirection: "row",
    },
    videoUserButtons: { flexDirection: "row", marginTop: 8 },
    videoUsersButtons: { width: 25, height: 25, marginRight: 12 },
    userListCount: { position: "absolute", top: -5, right: -4 },
    iconHeaderCall: { width: 18, height: 20, resizeMode: "contain" },
    waitingBoxContainer: {
        flex: 1,
        backgroundColor: "#041A32",
        alignItems: "center",
        justifyContent: "center",
    },
    waitingBox: {
        paddingLeft: 10,
        width: 154,
        height: 152,
        borderColor: "#A0A9BE",
        borderWidth: 1,
        borderRadius: 10,
        justifyContent: "space-around",
    },
    offlineStyle: {
        color: "#FFFFFF",
        backgroundColor: "#FF5F5F",
        borderRadius: 10,
        alignSelf: "flex-start",
        paddingHorizontal: 8,
    },
    offlineImageStyle: { resizeMode: "contain", width: 16, height: 14 },
    successModalMainTxt: {
        color: "#222222",
        fontSize: 14,
        fontWeight: "600",
        letterSpacing: 0,
        lineHeight: 18,
        marginTop: 15,
        marginBottom: 8,
    },
    successModalSubTxt: {
        color: "#979797",
        fontSize: 12,
        fontWeight: "500",
        letterSpacing: 0,
        lineHeight: 15,
        textAlign: "center",
    },

    modal: {
        justifyContent: "center",
        paddingHorizontal: wp("5%"),
        paddingVertical: hp("2%"),
    },
    modalContainer: {
        flex: 1,
    },
    modalTextHeader: {
        fontSize: 24,
        marginTop: 27,
        marginBottom: 6,
        color: "#041A32",
    },
    modalTextSubHeader: {
        fontSize: 14,
        color: "#041A32",
        marginTop: 20,
        marginBottom: 6,
    },
    modalText: {
        fontSize: 13,
        color: "#4E5C76",
    },
    modal1Close: {
        alignItems: "center",
        justifyContent: "center",
    },
    modalheader: {
        // backgroundColor: "#1B80F3",
        height: hp("9%"),
        flexDirection: "row",
        alignItems: "center",
    },
    modalheaderBackIcon: {
        flex: 1,
        fontSize: hp("3.5%"),
        color: "#1B80F3",
        marginLeft: wp("5%"),
    },
    modalheading: {
        flex: 8,
        color: "#041A32",
        fontSize: hp("2.9%"),
    },
    redColor: {
        color: "red",
    },
    intakeBtn: {
        backgroundColor: "#F9F9F9",
        borderRadius: 7,
        margin: 2,
        padding: 5,
    },
    paddingVertical4: {},
    intakeBtnText: {
        textAlign: "center",
        color: "#fff",
        fontSize: 14,
    },
    labelColorModal: {
        color: 'gray'
    },

    rejectBtn: {
        marginLeft: 10,
        width: wp("12%"),
        justifyContent: "center",
        borderRadius: 5,
    },
    rejectBtnText: {
        fontSize: 12,
        color: "#fff",
    },
    bellStyle: {
        width: 18,
        height: 22,
    },
    titleProfile: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_18,
        color: 'white'
    },
    subTitleProfile: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_16,
        color: '#041A32',
        opacity: 0.5,
    },
    headerBellAndText: {
        flex: 1,
        flexDirection: 'row',
        // justifyContent: 'space-between',
        justifyContent: 'center',
        alignItems: 'center'
    },

    findDoctors: {
        alignItems: "center",
        backgroundColor: "#f2f2f2",
        justifyContent: 'center',
        width: wp("28%"),
        flex: 1,
        margin: 5,
        padding: 10,
        alignSelf: "center",
        borderRadius: 5,
    },
    specialitiesTitle: {
        fontFamily: FONT_FAMILY_SF_PRO_MEDIUM,
        fontSize: FONT_SIZE_14,
        textAlign: 'center',
        color: '#041A32',
        paddingTop: 5,
    },
    headerCommonBackIcon: {
        fontSize: hp("3.5%"),
        color: "#fff",
    },
    headerHeading: {
        flexDirection: "row",
        marginLeft: 18,
        alignItems: "center",
        justifyContent: "flex-start",
    },

    footer: {
        height: 80,
        backgroundColor: "#ffffff",
        justifyContent: "center",
        alignItems: "center",
    },

    footerBtn: {
        justifyContent: "center",
        backgroundColor: '#FF5E38',
        alignItems: "center",
        width: wp("90%"),
        margin: 15,
        borderRadius: 5,
    },

    itemBorderStyle: {
        borderBottomWidth: 0.5,
        borderTopWidth: 0.5,
        borderRightWidth: 0.5,
        borderLeftWidth: 0.5,
        marginLeft: 0,
    }
});
