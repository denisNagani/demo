import { scaleFont } from './mixins';
import { Dimensions } from "react-native";

// FONT FAMILY
export const FONT_FAMILY_REGULAR = 'SF Pro Display';
export const FONT_FAMILY_SF_PRO_MEDIUM = 'SFProDisplay-Medium';
export const FONT_FAMILY_SF_PRO_REGULAR = 'SFProDisplay-Regular';
export const FONT_FAMILY_SF_PRO_BOLD = 'SourceSansPro-Bold';
export const FONT_FAMILY_SF_PRO_LIGHT = 'SourceSansPro-Light';
export const FONT_FAMILY_SF_PRO_SemiBold = 'SourceSansPro-SemiBold';
export const FONT_FAMILY_BOLD = 'OpenSans-Bold';

// FONT WEIGHT
export const FONT_WEIGHT_REGULAR = '400';
export const FONT_WEIGHT_BOLD = '700';
export const DEVICE_HEIGHT = Dimensions.get('window').height;
export const DEVICE_WIDTH = Dimensions.get('window').width;

// FONT SIZE
export const FONT_SIZE_11 = 11;
export const FONT_SIZE_12 = 12;
export const FONT_SIZE_13 = 13;
export const FONT_SIZE_14 = 14;
export const FONT_SIZE_15 = 15;
export const FONT_SIZE_16 = 16;
export const FONT_SIZE_17 = 17;
export const FONT_SIZE_18 = 18;
export const FONT_SIZE_19 = 19;
export const FONT_SIZE_20 = 20;
export const FONT_SIZE_26 = 26;
export const FONT_SIZE_30 = 30;

// LINE HEIGHT
export const LINE_HEIGHT_24 = 24;
export const LINE_HEIGHT_20 = 20;
export const LINE_HEIGHT_16 = 16;

// FONT STYLE
export const FONT_REGULAR = {
  fontFamily: FONT_FAMILY_REGULAR,
  fontWeight: FONT_WEIGHT_REGULAR,
};

export const FONT_BOLD = {
  fontFamily: FONT_FAMILY_BOLD,
  fontWeight: FONT_WEIGHT_BOLD,
};
