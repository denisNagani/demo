//import crashlytics from "@react-native-firebase/crashlytics";

export const successLog = (message, status = "") => {
    let _status = status === undefined || status === null ? "" : status;
    let result = `${message} ${_status === "" ? "" : "success status=" + _status}`
    console.log(result)
   // crashlytics().log(result);
};

export const errorLog = (msg = "", error = "") => {
    let e = error === undefined || error === null ? "" : error
    //crashlytics().log(msg + " error " + JSON.stringify(e));
};

export const methodLog = msg => {
    console.log(msg)
   // crashlytics().log(`${msg} method called`);
};

export const recordError = error => {
    console.log(error)
    successLog(error)
  //  crashlytics().recordError(error);
};
