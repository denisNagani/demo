import moment from "moment-timezone";
import { Alert } from "react-native";
import database from "@react-native-firebase/database";
import { ifValid } from "./ifNotValid";
import { successLog } from "./fireLog";
// import { fireEnv } from "../config/index";
import RNFetchBlob from 'rn-fetch-blob'
const { fs } = RNFetchBlob;

export const isoFormat = (date) => {
    return moment(date).format();
}

export const setZeroTime = (date) => {
    return moment(date).set({ hour: 0, minute: 0, second: 0, millisecond: 0 })
}

export const errorPayload = (text = "", subtext = "") => {
    return {
        text: ifValid(text) ? text : '',
        subText: ifValid(subtext) ? subtext : '',
        iconName: "closecircleo",
        modalVisible: true,
        iconColor: "#C70315",
    }
}



export const successPayload = (text = "", subtext = "") => {
    return {
        text: ifValid(text) ? text : '',
        subText: ifValid(subtext) ? subtext : '',
        iconName: "checkcircleo",
        modalVisible: true,
        iconColor: "#FF5E38",
    }
}

export const handleBackPress = (props) => {
    Alert.alert(
        "",
        "Are you sure you want to disconnect this call",
        [
            {
                text: "No",
                onPress: () => {
                    console.log("Cancel Pressed");
                },
                style: "cancel"
            },
            { text: "Yes", onPress: () => props.navigation.goBack() }
        ],
        { cancelable: false }
    );
}

export const checkUserProfile = (userProfile) => {
    let fullName = "...", image = "";
    console.log(userProfile)
    if (ifValid(userProfile) && ifValid(userProfile.userId)) {
        const user = userProfile.userId;
        image = ifValid(user.imageUrl) ? user.imageUrl : "";
        fullName = ifValid(user.fullName) ? user.fullName : '';
    }
    return { image, fullName }
}


export const addUserLogToFirebase = async (id, obj = {}) => {
    return await database()
        .ref(`/logs/${id}`)
        .push({ ...obj, timeStamp: JSON.stringify(moment().unix()) })
        .then(_ => {
            console.log("LOG ADDED")
        });
}

export const calculate_age = dob => {
    dob = moment(dob).format('YYYY/MM/DD')
    successLog(dob)
    var today = new Date();
    var birthDate = new Date(dob);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

export const Capitalize = (str) => {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

export const getCardsWithbgColor = (cardsArray, colorsArray) => {
    let pos = 0;
    let interval = 1;
    let disp_index = 0;

    if (colorsArray?.length > 0) {
        while (pos < cardsArray?.length) {
            cardsArray[pos].bgColor = colorsArray[disp_index]
            pos += interval;
            if ((colorsArray?.length - 1) !== disp_index) {
                disp_index = disp_index + 1
            } else {
                disp_index = 0
            }
        }
    }
    return cardsArray
}

export const convertDateToText = (momentDate) => {
    var today = moment();
    var yesterday = moment().subtract(1, 'day');
    return moment(momentDate).isSame(today, 'day')
        ? "Today"
        : moment(momentDate).isSame(yesterday, 'day')
            ? "Yesterday"
            : moment(momentDate).format('ll')
}

export const bytesToSize = (bytes) => {
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes == 0) return '0 Byte';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
    return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

export const calculateDrConsultationFee = (array) => {
    const maxObj = array.reduce((prev, current) => (
        parseInt(prev?.price) > parseInt(current?.price))
        ? prev
        : current
    )
    return maxObj
}

export const getSpecialitiesNames = (array, type) => {
    return array?.map(item => item?.[type])?.toString()
}

export const downloadPdfFromBase64 = async (baseUri, locationPath) => {
    try {
        let pdfLocation
        if (locationPath) {
            pdfLocation = locationPath
        } else {
            let date = new Date()
            let cur_date = Math.floor(date.getTime() + date.getSeconds() / 2)
            pdfLocation = fs.dirs.DownloadDir + `/pdf_${cur_date}.pdf`;
        }

        const response = await fs.writeFile(pdfLocation, baseUri, 'base64')
        console.log('dowloadPdfOndevice ', response);
        return response
    } catch (error) {
        console.log('dowloadPdfFromBase64 error ', error);
        return null
    }
}