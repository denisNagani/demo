export const ifNotValid = (value) =>
    value == undefined || value == null

export const ifValid = (value) =>
    value != undefined && value != null

export const ifEmpty = (value) =>
    value != undefined && value != null && value == ""

export const ifNotEmpty = (value) =>
    value != undefined && value != null && value != ""

export const isValid = (value) => {
    return value !== '' && value !== null && value !== undefined
}