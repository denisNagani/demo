export function show() {
	return {
		type: "SHOW_LOADER",
		payload: true,
	};
}

export function hide() {
	return {
		type: "HIDE_LOADER",
		payload: false,
	};
}
