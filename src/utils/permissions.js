import React from 'react'
import { PermissionsAndroid, Platform } from 'react-native'
import { check, checkMultiple, request, requestMultiple, Permission, PERMISSIONS } from "react-native-permissions";

export const CheckReadWritePermission = async () => {
    try {
        const res = await checkMultiple(['android.permission.READ_EXTERNAL_STORAGE', 'android.permission.WRITE_EXTERNAL_STORAGE', 'android.permission.RECORD_AUDIO'])
        if (res["android.permission.READ_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED && res["android.permission.WRITE_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.GRANTED && res["android.permission.RECORD_AUDIO"] === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("permission granted");
            return true
        } else if (res["android.permission.READ_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.DENIED || res["android.permission.WRITE_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.DENIED || res["android.permission.RECORD_AUDIO"] === PermissionsAndroid.RESULTS.DENIED) {
            //ask permission
            AskReadWritePermissions()
        } else if (res["android.permission.READ_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.BLOCKED || res["android.permission.WRITE_EXTERNAL_STORAGE"] === PermissionsAndroid.RESULTS.BLOCKED || res["android.permission.RECORD_AUDIO"] === PermissionsAndroid.RESULTS.BLOCKED) {
            alert("Please allow permission manually from app settings to use this feature")
            return false
        }
    } catch (error) {
        console.log(error);
        return false
    }
}

export const CheckAudioPermission = async () => {
    try {
        const res = await check("android.permission.RECORD_AUDIO")
        console.log(res);
        if (res === 'granted') {
            console.log("permission granted");
            return true
        } else if (res === 'denied') {
            //ask permission
            AskAudioPermission()
        } else if (res === 'blocked') {
            alert("Please allow permission manually from app settings to use this feature")
            return false
        }
    } catch (error) {
        console.log(error);
    }
}

export const AskReadWritePermissions = async () => {
    try {
        const res = await requestMultiple(['android.permission.READ_EXTERNAL_STORAGE', 'android.permission.WRITE_EXTERNAL_STORAGE', 'android.permission.RECORD_AUDIO'])
        console.log(JSON.stringify(res));
        return true
    } catch (error) {
        console.log(error);
        return false
    }
}

export const AskAudioPermission = async () => {
    try {
        const res = await request("android.permission.RECORD_AUDIO")
        console.log(res);
        if (res === 'granted') {
            // return true
        } else {
            return false
        }
    } catch (error) {
        console.log(error);
        return false
    }
}

export const CheckReadExternalPermission = async () => {
    if (Platform.OS === 'android') {
        try {
            const res = await check('android.permission.READ_EXTERNAL_STORAGE')
            if (res === 'granted') {
                console.log("permission granted");
                return true
            } else if (res === 'denied') {
                //ask permission
                AskReadExternalPermission()
            } else if (res === 'blocked') {
                alert("Please allow permission manually from app settings to use this feature")
                return false
            }
        } catch (error) {
            console.log(error);
            return false
        }
    } else {
        return true
    }
}

export const AskReadExternalPermission = async () => {
    try {
        const res = await request("android.permission.READ_EXTERNAL_STORAGE")
        console.log(res);
        if (res === 'granted') {
            return true
        } else {
            return false
        }
    } catch (error) {
        console.log(error);
        return false
    }
}
