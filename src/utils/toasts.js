import { Toast } from 'native-base';

export const successToast = (message, position="bottom") => {
    Toast.show({
        text: message,
        textStyle: { color: '#fdc630' },
        position: position,
        duration: 3000
    })
};

export const successToastWithBack = (message, scope) => {
    Toast.show({
        text: message,
        textStyle: { color: '#fdc630' },
        position: "bottom",
        duration: 3000,
        onClose: function () {
            scope.props.navigation.goBack();
        }
    })
};
export const successToastWithRoute = (message, scope, route) => {
    Toast.show({
        text: message,
        textStyle: { color: '#fdc630' },
        position: "bottom",
        duration: 3000,
        onClose: function () {
            scope.props.navigation.navigate(route);
        }
    })
};
export const errorToast = (message, position= "bottom") => {
    Toast.show({
        text: message,
        textStyle: { color: '#fdc630' },
        position: position,
        buttonText: "Ok",
        duration: 3000
    })
};
export const errorToastWithBack = (message, scope) => {
    Toast.show({
        text: message,
        textStyle: { color: '#fdc630' },
        position: "bottom",
        duration: 3000,
        onClose: function () {
            scope.props.navigation.goBack();
        }
    })
}
export const errorToastWithRoute = (message, scope, route) => {
    Toast.show({
        text: message,
        textStyle: { color: '#fdc630' },
        position: "bottom",
        duration: 3000,
        onClose: function () {
            scope.props.navigation.navigate(route);
        }
    })
}
