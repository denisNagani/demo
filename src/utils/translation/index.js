import LocalizedStrings from 'react-native-localization';
import english from './en.js'
import hindi from './hin.js'
import { ProviderTypes, TranslatorConfiguration, TranslatorFactory } from 'react-native-power-translator';
import { translationApiKey } from '../../config/index';
import { ifValid } from '../../utils/ifNotValid';

export const strings = new LocalizedStrings({
    en: english,
    hin: hindi,
});

export const changeLaguage = (languageKey) => {
    strings.setLanguage(languageKey)
}

export const translateToHindi = async (txt) => {
    try {
        //Fill with your own details
        if (ifValid(txt)) {
            await TranslatorConfiguration.setConfig(ProviderTypes.Google, translationApiKey, 'hi');
            const translator = await TranslatorFactory.createTranslator();
            const translated = await translator.translate(txt)
            //Do something with the translated text
            return translated
        }
    } catch (error) {
        console.log("error in transaltion " + JSON.stringify(error));
        return txt
    }
}

export const TranslationConfig = async (lang) => {
    return TranslatorConfiguration.setConfig(ProviderTypes.Google, translationApiKey, lang)
}