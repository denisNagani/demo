export default (validation = {
	fullName: {
		presence: {
			message: "^Please enter name",
			allowEmpty: false,
		},
	},
	mobileNo: {
		presence: {
			message: "^Please enter mobile number",
			allowEmpty: false,
		}
	},
	email: {
		presence: {
			message: "^Please enter an email address",
		},
		email: {
			message: "^Please enter a valid email address",
		},
	},
	password: {
		presence: {
			message: "^Please enter a password",
		},
		length: {
			minimum: 5,
			// maximum: 8,
			message:
				"^Your password must be at least 5 characters long and less than 8 characters",
		},
	},
});
