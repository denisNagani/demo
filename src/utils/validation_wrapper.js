import validation from './validation-messages'
import validator from 'validate.js';

export default function validate(fieldName, value) {
  var formValues = {}
  formValues[fieldName] = value
  
  var formFields = {}
  formFields[fieldName] = validation[fieldName]

  const result = validator(formValues, formFields);
  if (result) {
    return result[fieldName][0]//result[fieldName].length>0 //result[fieldName][0]
  }
  return null
}